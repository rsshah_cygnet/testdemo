# Tuteme #


### How do I get set up? ###

* After taking clone first create a database for the project.
* Rename the the .env.example file as .env and setup your enviornment configuration settings there e.g Database details.
* Run `composer install` command from the root directory.(For this command, make sure you have composer installed, https://getcomposer.org/download)
* Run `npm install` command from the root directory.(For this command, make sure you have npm installed, https://docs.npmjs.com/cli/install)
* Now run the commands `php artisan migrate` and then `php artisan db:seed`.
* Now run the commands `gulp` command.
* Run command `php artisan serve` and you can check website from `http://localhost:8000/`.