// JavaScript Document
$ = jQuery.noConflict();

  $(document).ready(function(){
     
      $('#passTestMsg').hide();
      $('.rating-input').unbind('mouseenter mouseleave click');
       var user_id = $('#logged_in_user_id').val();
       setInterval('checkTutorJoinSession('+user_id+')',60000);
       //checkTutorJoinSession(user_id);

  })


  $(window).load(function() {
    
      $('#from_date_specific').daterangepicker({
          "singleDatePicker": true,
          showDropdowns: true,
          minDate:new Date(),
           maxDate: $('#admin_end_range_date_for_jquery').val(),
      });

      window.fbAsyncInit = function() {
        // init the FB JS SDK
        FB.init({
          appId      : '1422143877853101',                            
          status     : true,                                 
          xfbml      : true                                  
        });

      };

      // Load the SDK asynchronously
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/all.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
  })

  $('body').on('change','#month_piechart,#year_piechart',function(){
      month_piechart();
  })

  $('body').on('change','#quarter,#year_columnchart',function(){
      filterColumnChart();
  })

 
  $(document ).on( 'click', '#cancelSession', function()
  {
     var id = $('#session-id').val();
     $("#myModal").modal('show');
  });

  /* $(window).resize(function(){
       drawChart();
       drawChart2();
   });*/

  function submit_form()
  {
    var selected_tab = $("input[name='available']:checked").val();
    if(selected_tab == 2)
      available_every_day();
    else
      available_specific_day();
  }

  function formatDate(date) {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }

  function deleteTutorAvaliablity(id)
  {
    $.ajax({
      url: baseUrl,
      type: 'POST',
      data: "id="+id+"&_token="+$('#csrf_token_header').val(),
      success: function(data){
          // Success...
          $('.availablity_'+id).remove();

        },
        error: function(data){

        }
      });
  }

  function available_specific_day()
  {
    var from_time = $('#from_time_specific').val();
    var to_time = $('#to_time_specific').val();
    var from_date = $('#from_date_specific').val();

    $.ajax({
      url: baseUrl + '/tutor-availablity-everyday',
      type: 'POST',
      data: $( "#available_specific_day" ).serialize()+"&reference=1&to_date="+from_date,
      success: function(data){
                // Success...
               // Success...
               $('.has-error-text').hide();
               $('.form-group').removeClass(" has-error has-feedback ");
               $('form').removeClass("has-feedback");

               var data = $.parseJSON(data);
               if(typeof data.error != 'undefined')
               {
                $(".from_time_specific_error").show();
                $(".from_time_specific_error_value").html(data.error);
                return false;
              }


              $('.has-error-text').hide();
              $('.glyphicon-warning-sign').hide();
              $('.form-group').removeClass(" has-error has-feedback ");
              $('form').removeClass("has-feedback");

              string = '<li class="availablity_'+data.id+'"><div class="horizontal-custom d-i-b"><dl class="dl-horizontal selected-time-list "><dt>Date:</dt><dd>'+formatDate(from_date)+'</dd></dl><dl class="dl-horizontal selected-time-list from-to-time"><dt>From Time:</dt><dd>'+from_time+'</dd><dt>To Time:</dt><dd>'+to_time+'</dd></dl></div><a href="#" onclick="deleteTutorAvaliablity('+data.id+')" class="btn tutor-dashboard-trash-btn select-time-trash"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>';


              $('#available_specific_div').append(string);
              $('#from_time').val('');
              $('#to_time').val('');


            },
            error: function(data){
              $('.has-error-text').hide();
              $('.form-group').removeClass(" has-error has-feedback ");
              $('form').removeClass("has-feedback");

                // Error...
                var errors = data.responseJSON;

                $.each(errors, function(index, value) {
                  $( '#'+index+"_specific" ).closest('.form-group').addClass(" has-error has-feedback ");
                  $('.'+index+"_specific_error").show();
                  $('.'+index+"_specific_error_value").html(value);
                   //$(index+"_error").html(value);
                 });
              }
            });
  }

  function available_every_day()
  {
    $.ajax({
      url: baseUrl + '/tutor-availablity-everyday',
      type: 'POST',
      data: $( "#available_everyday" ).serialize()+"&reference=0",
      success: function(data){
                // Success...
                $('.has-error-text').hide();
                $('.form-group').removeClass(" has-error has-feedback ");
                $('form').removeClass("has-feedback");

                var data = $.parseJSON(data);
                if(typeof data.error != 'undefined')
                {
                  $(".from_time_error").show();
                  $(".from_time_error_value").html(data.error);
                  return false;
                }

                var from_time = $('#from_time').val();
                var to_time = $('#to_time').val();

                $('.has-error-text').hide();
                $('.glyphicon-warning-sign').hide();
                $('.form-group').removeClass(" has-error has-feedback ");
                $('form').removeClass("has-feedback");

                string = '<li class="availablity_'+data.id+'"><dl class="dl-horizontal selected-time-list d-i-b"><dt>From Time:</dt><dd>'+from_time+'</dd><dt>To Time:</dt><dd>'+to_time+'</dd></dl><a href="deleteTutorAvaliablity('+data.id+')" class="btn tutor-dashboard-trash-btn select-time-trash"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>';

                $('#available_everyday_div').append(string);
                $('#from_time').val('');
                $('#to_time').val('');



              },
              error: function(data){
                $('.has-error-text').hide();
                $('.form-group').removeClass(" has-error has-feedback ");
                $('form').removeClass("has-feedback");

                // Error...
                var errors = data.responseJSON;

                $.each(errors, function(index, value) {
                  $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
                  $('.'+index+"_error").show();
                  $('.'+index+"_error_value").html(value);
                   //$(index+"_error").html(value);
                 });
              }
            });
  }

 function month_piechart() {
        var month = $('#month_piechart').val();
        var year = $('#year_piechart').val();
        if(year == ""){
           $('#piechart_error').html('Please select year');
           $('#piechart_error').show();
            return false;
        }else{
          $('#piechart_error').hide();
        }
        if(month == ""){
         $('#piechart_error').html('Please select month');
         $('#piechart_error').show();
            return false;
        }else{
          $('#piechart_error').hide();
        }
        $.ajax({
            type: 'POST',
            url: baseUrl + '/tutor-piechart',
            data: {month: month,year: year,_token: $('#csrf_token_header').val()},
            success: function(data){
                var status = data.status;
                if(status == "success"){
                    $('#pie_chart_portion').html(data.html);    

                }else{
                    var error_html = "<center><p>There are no any session request found for this time interval</p></center>";
                    $('#pie_chart_portion').html(error_html);   
                }
                
            }
        });
    }

  function filterColumnChart() {
        var quarter = $('#quarter').val();
        var year = $('#year_columnchart').val();
        if(year == ""){
           $('#column_chart_error').html('Please select year');
           $('#column_chart_error').show();
            return false;
        }else{
          $('#column_chart_error').hide();
        }
        if(quarter == ""){
         $('#column_chart_error').html('Please select quarter');
         $('#column_chart_error').show();
            return false;
        }else{
          $('#column_chart_error').hide();
        }
        $.ajax({
            type: 'POST',
            url: baseUrl + '/tutor-columnchart',
            data: {quarter: quarter,year: year,_token: $('#csrf_token_header').val()},
            success: function(data){
                var status = data.status;
                //if(status == "success"){
                    $('#column_chart_portion').html(data.html);    
                //}
                
            }
        });
    }


    $(document ).on( 'click', '.report_tutor', function()
        {
          var id = $('#session_id').val();
          $("#report_tutor_modal").modal('show');
          $('#ReportTutorForm').on('submit', function (e)
          {  e.preventDefault();
            $.ajax({
                  type: 'POST',
                  url: '/tutor-report',
                  data: {id:id,_token: $('#csrf_token_header').val(),report: $("#reporttext").val()},
                  success: function(data)
                  {
                    if(data.res == 201)
                    {
                      $('.error').html(data.msg);
                    }
                    else
                    {
                     alert(data);
                     loadTutorMysessions();
                      $('.modal-backdrop').remove();
                      $('.tutor-dashboard').removeClass('modal-open');
                    }
                  }
              });
          });
        });

        $(document ).on( 'click', '.session_feedback', function()
        { 
          var id = $('#session_id').val();
          $("#session_feedback_modal").modal('show');
          $('#SessionFeedbackForm').on('submit', function (e)
          {  
            e.preventDefault();
            $.ajax({
                  type: 'POST',
                  url: '/tutor-session-feedback',
                  data: {id:id,_token: $('#csrf_token_header').val(),report: $("#feedbackText").val()},
                  success: function(data)
                  {
                    if(data.res == 201)
                    {
                      $('.error').html(data.msg);
                    }
                    else
                    {
                     alert(data);
                     loadTutorMysessions();
                      $('.modal-backdrop').remove();
                      $('.tutor-dashboard').removeClass('modal-open');
                    }
                  }
              });
          });
        });


        $(document).on( 'click', '#declineSession', function()
        {
          var userId = $('#logged_in_user_id').val();
          var id = $('#session-id').val();
          var logged_as = $('#logged_in_as').val();
          if(logged_as == "tutor"){
            var role = '1';
          }else{
            var role = '2';
          }
          $.ajax({
                    type: 'POST',
                    url: '/tutor-cancel-session',
                    data: {id:id,role:role,userId:userId,_token: $('#csrf_token_header').val()},
                    success: function(data)
                    {
                      loadTutorMysessions();
                      $('.modal-backdrop').remove();
                      $('.tutor-dashboard').removeClass('modal-open');
                    }
                });
      });

      function FBShareTutor(){
            var product_name   =  'Tuteme';
            var description    =  $('#share_description').val();
            var share_image    =  'http://192.192.8.51:8893/images/logo-design.png';
            var share_url      =  'http://192.192.8.51:8893/'; 
            var share_capt     =  'https://tuteme.com';
              FB.ui({
                  method: 'feed',
                  name: product_name,
                  link: share_url,
                  picture: share_image,
                  caption: share_capt,
                  description: description

              }, function(response) {
                  if(response && response.post_id){
                    var session_id = $('#session_id').val();
                    $.ajax({
                            type: 'POST',
                            url: baseUrl + '/update_share_count_tutor',
                            data: {session_id:session_id,_token: $('#csrf_token_header').val()},
                            success: function(data){
                              $('#fb_share_tutor_btn').hide();
                              return true;  
                            }
                        });
                  }
                  else{}
              });
          } 

           function loadSessionCalanderForTutor(currentDate,finalSessionData){
               var html='';
                   html+='<div class="color-legend"><ul class="list-inline box-legend-calender"><li><span class="small-box upcomming-session"></span>Upcoming Session</li><li><span class="small-box successful-session"></span>Successful Session</li><li><span class="small-box cancel-session-tutee"></span>Session cancelled by Tutee</li><li><span class="small-box cancel-session-tutor"></span>Session cancelled by Tutor</li></ul></div>';



                  var date = new Date();
                  var d = date.getDate(),
                      m = date.getMonth(),
                      y = date.getFullYear();

            $('#calendar').fullCalendar({
                eventAfterAllRender: function (event, element) {
                    if($('.fc-header-toolbar').parent().find('.color-legend').length <= 0){
                        $('.fc-header-toolbar').after(html)
                    }
                },
                    header: {
                        left: '',
                        center: 'prev,title,next,today',
                        right: ''
                    },
                    defaultDate: currentDate,
                    navLinks: true, // can click day/week names to navigate views
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    eventClick: function(calEvent, jsEvent, view) {
                      var status = calEvent.status;
                      var id = calEvent.id;
                      showSessionPopup(id,status);
                      //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                    },
                     events : JSON.parse(finalSessionData)
                   
            });
             
        }

           function showSessionPopup(id,status) {

              $.ajax({
                  type: 'POST',
                  url: baseUrl + '/get-session-data-tutor',
                  data: {status: status,id:id,_token: $('#csrf_token_header').val()},
                  success: function(data){
                      var status = data.status;
                      if(status == 1){
                         $('#session_modal_div').html(data.html);
                         $('.rating').rating();
                            $('#success_session_modal').modal('show');
                       }else if(status == 2){
                         $('#session_modal_div').html(data.html);
                            $('#upcomming_session_modal').modal('show');
                            var user_id = $('#logged_in_user_id').val();
                       checkTutorJoinSession(user_id); 
                       }else if(status == 3){
                         $('#session_modal_div').html(data.html);
                            $('#cancelled_by_tutor_session_modal').modal('show');
                       }else if(status == 4){
                            $('#session_modal_div').html(data.html);
                            $('#cancelled_by_tutee_session_modal').modal('show');
                       }

                      $('.rating-input').unbind('mouseenter mouseleave click');
                      return false;
                  }
              });
          }


          $(document ).on( 'click', '#tutor_session_accept', function()
          { 
              var session_id = $(this).attr('data-id');
              $.ajax({
                    type: 'GET',
                    url: baseUrl + '/tutor-session-request/accept/'+session_id,
                    success: function(data)
                    {

                       var data = $.parseJSON(data);
                       var status = data.status;
                      if(status == "success"){
                          $('#Msg').hide();
                          $('#Msg').show();
                          $('#Msg').empty();
                          $('#Msg').addClass("alert alert-success").append(data.msg);
                            //return false;
                      }else{
                            $('#Msg').hide();
                            $('#Msg').show();
                            $('#Msg').empty();
                            $('#Msg').addClass("alert alert-danger").append(data.msg);
                            //return false;
                      }
                     setTimeout(function(){loadTutorSessionRequest();}, 3000);
                     
                    }
                })
          });


          $('body').on('click','#cancel_request_tutor',function(){
              var session_id = $('#cancelled_session_id').val();
              var url = "/tutor-session-request/decline/"+session_id;
              $.ajax({
                type: 'POST',
                url: url,
               data: {id:session_id,_token: $('#csrf_token_header').val()},
                success: function(data){
                   var data = $.parseJSON(data);
                    var status = data.status;
                    //$('.hide_modal').click();

                    if(status == "success"){
                          $('#Msg').hide();
                          $('#Msg').show();
                          $('#Msg').empty();
                          $('#Msg').addClass("alert alert-success").append(data.msg);
                          //return false;
                    }else{
                          $('#Msg').hide();
                          $('#Msg').show();
                          $('#Msg').empty();
                          $('#Msg').addClass("alert alert-danger").append(data.msg);
                          //return false;
                    }
                   
                    setTimeout(function(){loadTutorSessionRequest();}, 3000);
                   $("#myModal").modal('hide');
                   $('.modal-backdrop').remove();
                   $('.tutor-dashboard').removeClass('modal-open');
                }
            });
          })

          function handleModalBoxEvent(data)
          {
            jQuery( ".modal-body" ).html(data);
            $("#myModal").modal('show');
          }

          $( document ).on( 'click', '#decline-request', function()
          {
            var id = $(this).attr('id2');
            showModalFormTutor(id);
          });

         function showModalFormTutor(id)
         {
          jQuery.ajax(
          {
            type:   "GET",
            url: baseUrl + '/tutor-session-request/decline/model/'+id,
            async:  true,
            success: function(data)
            {
              handleModalBoxEvent(data);
            }
          });
        }


        $(document).on('click', '#active1', function (e) {
              var startDate = $.session.get('startDate');
              var endDate = $.session.get('endDate');
               var token = $('#csrf_token_header').val();
              $.ajax({
                url: 'payment/ajax/tab/tab2',
                type: 'POST',
                data:  "&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
                dataType: 'html',
              }).success(function (data) {
                  data = $.parseJSON(data);
                  jQuery("#home").html(data.successfulSessionHTML);

                   $("#active1").addClass("active");
              $("#active3").removeClass("active");
              $("#active2").removeClass("active");    

              $("#home").addClass("active in");        
              $("#menu1").removeClass("active in");        
              $("#menu2").removeClass("active in");

              $('#menu1-collapse').removeClass('in');
              $('#menu2-collapse').removeClass('in');
              $('#home-collapse').addClass('in');  
              }).fail(function () {
                alert('Tutor could not be loaded.');
              });
          });


        $(document).on('click', '#active3', function (e) {
            var startDate = $.session.get('startDate');
            var endDate = $.session.get('endDate');
            var token = $('#csrf_token_header').val();
            $.ajax({
              url: 'payment/ajax/tab/tab2',
              type: 'POST',
              data:  "&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
              dataType: 'html',
            }).success(function (data) {
                data = $.parseJSON(data);
                jQuery("#menu2").html(data.cancelSessionByTuteeHTML);
                $("#active3").addClass("active");
            $("#active1").removeClass("active");
            $("#active2").removeClass("active");    

            $("#home").removeClass("active in");        
            $("#menu1").removeClass("active in");        
            $("#menu2").addClass("active in");

            $('#menu1-collapse').removeClass('in');
            $('#menu2-collapse').addClass('in');
            $('#home-collapse').removeClass('in');  
            }).fail(function () {
              alert('Tutor could not be loaded.');
            });
        });


        $(document).on('click', '#active2', function (e) {
            var startDate = $.session.get('startDate');
            var endDate = $.session.get('endDate');
             var token = $('#csrf_token_header').val();
            $.ajax({
              url: 'payment/ajax/tab/tab2',
              type: 'POST',
              data:  "&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
              dataType: 'html',
            }).success(function (data) {
                data = $.parseJSON(data);
                jQuery("#menu1").html(data.cancelSessionByTutorHTML);

             $("#active2").addClass("active");
            $("#active1").removeClass("active");
            $("#active3").removeClass("active");    

            $("#home").removeClass("active in");        
            $("#menu1").addClass("active in");        
            $("#menu2").removeClass("active in");

            $('#menu1-collapse').addClass('in');
            $('#menu2-collapse').removeClass('in');
            $('#home-collapse').removeClass('in');  
            }).fail(function () {
              alert('Tutor could not be loaded.');
            });
        });


          $('body').on('click','.js-tabcollapse-panel-heading',function(){
                var tab_attr = $(this).attr('href');
                if(tab_attr == '#home-collapse'){
                    $('#active1').click();
                }else if(tab_attr == '#menu1-collapse'){
                    $('#active2').click();
                }else if(tab_attr == '#menu2-collapse'){
                    $('#active3').click();
                } 

                /*For my test page*/
                var type = $(this).attr('href');
                if(type == '#home-collapse'){
                    $('#tab-home').click();
                    $('#home-collapse').addClass('in');
                    $('#menu1-collapse').removeClass('in');
                }else if(type == '#menu1-collapse'){
                    $('#tab-menu1').click();
                    $('#home-collapse').removeClass('in');
                    $('#menu1-collapse').addClass('in');
                }
          })

        function tutorPaymentPagination(page)
          {
             var tab1 = $('#active1').attr('class');
             var tab2 = $('#active2').attr('class');
             var tab3 = $('#active3').attr('class');
             var filter = $('#filter').val();

            
            if(tab1 == 'active'){
                var type = 'tab1';
            }else if(tab2 == 'cancel-tutor active'){
                var type = 'tab2';
            }else if(tab3 == 'cancel-tutee active'){
                var type = 'tab3';
            }
            
            var startDate = $.session.get('startDate');
            var endDate = $.session.get('endDate');
            var token = $('#csrf_token_header').val();
            $.ajax({
              url: 'payment/ajax/' + type,
              type: 'POST',
              data: "&page="+page+"&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
              dataType: 'html',
            }).success(function (data) {
                data = $.parseJSON(data);
                jQuery("#home").html(data.successfulSessionHTML);
                jQuery("#menu2").html(data.cancelSessionByTuteeHTML);
                jQuery("#menu1").html(data.cancelSessionByTutorHTML);

                if($('#active1').hasClass('active')){
                    $('#home-collapse').html(data.successfulSessionHTML);    
                }
                if($('#active2').hasClass('active')){
                    $('#menu1-collapse').html(data.cancelSessionByTutorHTML);
                }
                if($('#active1').hasClass('active')){
                    $('#menu2-collapse').html(data.cancelSessionByTuteeHTML);  
                }
               
                
            
            }).fail(function () {
              alert('Some error occured.');
            });
          }


      $(document).ready(function(){

          var tutor_payment = $('#is_tutor_payment').val();
          if(tutor_payment != undefined && tutor_payment == 'tutor_payment'){
                $.session.clear();
                $(function() {

                  $('input[name="datefilter"]').daterangepicker({
                      autoUpdateInput: false,
                      locale: {
                          cancelLabel: 'Clear'
                      }
                  });

                  $('input[name="datefilter"]').daterangepicker();
                  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                    var startDate = picker.startDate.format('YYYY-MM-DD');
                    var endDate = picker.endDate.format('YYYY-MM-DD');
                    $.session.set('startDate', startDate);
                    $.session.set('endDate', endDate);
                    //showModalForm(startDate, endDate);
                     tutorPaymentPagination(1);
                  });
                   $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                    $('#payment-date').val('');
                    $.session.clear();
                    var startDate = picker.startDate.format('YYYY-MM-DD');
                    var endDate = null;
                   // showModalForm(startDate, endDate);
                    tutorPaymentPagination(1);
                  });
                    });
          }    


          var is_tutor_test = $('#is_tutor_test').val();
          if(is_tutor_test != undefined && is_tutor_test == 'tutor_my_test'){
              $.session.clear();
              $(function() {
              
                $('input[name="datefilter"]').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });

                $('input[name="datefilter"]').daterangepicker();
                $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                  var startDate = picker.startDate.format('YYYY-MM-DD');
                  var endDate = picker.endDate.format('YYYY-MM-DD');
                  $.session.set('startDate', startDate);
                  $.session.set('endDate', endDate);
                  showModalFormMytest(startDate, endDate);
                });
                $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                  $.session.clear();
                  $('#payment-date').val('');
                
                  var startDate = null;
                  var endDate = null;
                  showModalFormMytest(startDate, endDate);
                  //showModalFormCancelButton(startDate, endDate);
                });
              });
          }
         
          
          });

        $(document).on('click', '.pagination a', function (e) {
            e.preventDefault();
            var tutor_payment = $('#is_tutor_payment').val();
            if(tutor_payment != undefined && tutor_payment == 'tutor_payment'){
                tutorPaymentPagination($(this).attr('href').split('page=')[1]);    
            }
            var is_tutor_test = $('#is_tutor_test').val();
            if(is_tutor_test != undefined && is_tutor_test == 'tutor_my_test'){
                tutorMytestPagination($(this).attr('href').split('page=')[1]);  
            }
            e.preventDefault();
        });


        $('body').on('click','#reset',function (e)
        {
          loadTutorPayment();
        });


        function tutorMytestPagination(page)
        {
           var tab1 = $('#tab-home').attr('class');
           var tab2 = $('#tab-menu1').attr('class');
          
          if(tab1 == 'active'){
              var type = 'tab1';
          }else if(tab2 == 'pending-test active'){
              var type = 'tab2';
          }
          var startDate = $.session.get('startDate');
          var endDate = $.session.get('endDate');
          var token = $('#csrf_token_header').val();
          $.ajax({
            url: 'mytest/ajax/' + type,
            type: 'POST',
            data: "&page="+page+"&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
            dataType: 'html',
          }).success(function (data) {
              data = $.parseJSON(data);
              jQuery("#home").html(data.appearedTestHTML);
              jQuery("#menu1").html(data.pendingTestHTML);

               $('#menu1-collapse').html(data.pendingTestHTML);
               $('#home-collapse').html(data.appearedTestHTML);
          }).fail(function () {
            alert('Some error occured.');
          });
        }


        $(document).on('click', '#tab-home', function (e) {
          $('.date-block').show();
            var startDate = $.session.get('startDate');
            var endDate = $.session.get('endDate');
            var token = $('#csrf_token_header').val();
            $.ajax({
              url:  'mytest/ajax/tab/tab1',
              type: 'POST',
              data:  "&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
              dataType: 'html',
            }).success(function (data) {
                data = $.parseJSON(data);
                jQuery("#home").html(data.appearedTestHTML);
                $('#home-collapse').html(data.appearedTestHTML);

                $("#tab-menu1").removeClass("active");
                $("#tab-home").addClass("active");    

                $("#home").addClass("active in");        
                $("#menu1").removeClass("active"); 

                $('#home-collapse').addClass('in');   
                $('#menu1-collapse').removeClass('in');

            }).fail(function () {
              alert('Tutor could not be loaded.');
            });
        });

    $(document).on('click', '#tab-menu1', function (e) {
         $('.date-block').hide();
        var startDate = $.session.get('startDate');
        var endDate = $.session.get('endDate');
       var token = $('#csrf_token_header').val();
        $.ajax({
          url: 'mytest/ajax/tab/tab1',
          type: 'POST',
          data:  "&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
          dataType: 'html',
        }).success(function (data) {
            data = $.parseJSON(data);
            jQuery("#menu1").html(data.pendingTestHTML);
            $('#menu1-collapse').html(data.pendingTestHTML);

             $('#home-collapse').removeClass('in');
            $('#menu1-collapse').addClass('in');
            
            $("#tab-menu1").addClass("active");
            $("#tab-home").removeClass("active"); 
            $("#home").removeClass("active");        
            $("#menu1").addClass("active in"); 

        }).fail(function () {
          alert('Tutor could not be loaded.');
        });
    });


    $('body').on('click','#give-test',function(e){
            e.preventDefault();
            var curriculumn_id = $('#curriculum_id').val();
            var token = $('#csrf_token_header').val();
            var url = baseUrl + "/test/check";
            jQuery.ajax(
            {
                type:   "POST",
                url:    url,
                dataType : 'html',
                data: $("#appear-test-form").serialize()+"&_token="+token,
                success: function(data)
                {
                    var testResult  = $.parseJSON(data);
                    var test        = testResult.status;
                    var grade       = testResult.grade;
                    var subject     = testResult.subject;
                    var topic       =  testResult.topic;
                    var education_system       =  testResult.education_system;
                    var level       =  testResult.level;
                    var program     =  testResult.program;
                    
                    if (curriculumn_id == '')
                    {
                        $('#passTestMsg').empty();
                        $('#passTestMsg').show();
                        $('#passTestMsg').append('Please select Curriculum.').fadeOut(2000);
                    }
                    else if(test == 'pass'){
                        $('#passTestMsg').empty();
                        $('#passTestMsg').show();
                        $('#passTestMsg').append('You have already passed this test.').fadeOut(2000);
                    }else if(grade == 'grade_required'){
                        $('#passTestMsg').empty();
                        $('#passTestMsg').show();
                        $('#passTestMsg').append('Please select grade.').fadeOut(2000);
                    }else if(subject == 'subject_required'){
                        $('#passTestMsg').empty();
                        $('#passTestMsg').show();
                        $('#passTestMsg').append('Please select subject.').fadeOut(2000);
                    }
                    else if(topic == 'topic_required'){
                        $('#passTestMsg').empty();
                        $('#passTestMsg').show();
                        $('#passTestMsg').append('Please select topic.').fadeOut(2000);
                    }
                    else if(education_system == 'education_system_required'){
                        $('#passTestMsg').empty();
                        $('#passTestMsg').show();
                        $('#passTestMsg').append('Please select education system.').fadeOut(2000);
                    }
                    else if(level == 'level_required'){
                        $('#passTestMsg').empty();
                        $('#passTestMsg').show();
                        $('#passTestMsg').append('Please select level.').fadeOut(2000);
                    }
                    else if(program == 'program_required'){
                        $('#passTestMsg').empty();
                        $('#passTestMsg').show();
                        $('#passTestMsg').append('Please select program.').fadeOut(2000);
                    }
                    else{
                        $('#appear-test-form').submit();
                    }
                }
            });
        });


      function showModalFormMytest(startDate, endDate){
        
            var startDate = $.session.get('startDate');
            var endDate = $.session.get('endDate');
            var token = $('#csrf_token_header').val();
            jQuery.ajax(
            {
                type:       "GET",
                url: baseUrl + '/tutor/mytest/ajax/tab1',
                data:       {startDate: startDate, endDate: endDate},
                success: function(data){    
                    var result = data;
                    jQuery("#home").html(result.appearedTestHTML);
                    $("#tab-menu1").removeClass("active");
                    $("#tab-home").addClass("active");    

                    $("#home").addClass("active in");        
                    $("#menu1").removeClass("active"); 

                    $('#home-collapse').addClass('in');   
                    $('#menu1-collapse').removeClass('in');
                }
            }); 
        }