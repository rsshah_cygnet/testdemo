// JavaScript Document
$ = jQuery.noConflict();
// when the page is ready, initialize everything
$(document).ready(function() {

      $('body').on('click','#cancel_shareexp',function(){
            $('#share_exp_textarea').val('');
            $('.custom_error_msg').hide();
            $('.custom_success_msg').hide();
      }) 

       $('body').on('click','#cancel_changepassbtn',function(){
            $('#oldpsd').val('');
            $('#newpsd').val('');
            $('#confirmpsd').val('');
            $('.has-error-text').hide();
      })  

      $('.fc-day-number').prop('disabled',true); 
      $(document).on('click','.fc-prev-button',function(){
         $('.fc-day-number').prop('disabled',true);
      })
   
    refreshNotifications();
    $('.educationDetailPart .select-curriculum').change(function(e) {
        if ($(this).val() == "") {
            $('.educationDetailPart .select-accordion').slideUp();
        } else {
            $('.educationDetailPart .select-accordion').slideDown();
        }
    });
    $('.PaymentPart .select-curriculum').change(function(e) {
        if ($(this).val() == "") {
            $('.PaymentPart .select-accordion').slideUp();
        } else {
            $('.PaymentPart .select-accordion').slideDown();
        }
    });

    $('.dynamic_dropdown').on('change',function(){
        trrigerChangeEdit();
    })

  

   /*  $('body').on('click','.language-drop',function(){
        if(!$('.bootstrap-select').hasClass('open')){

            $('.bootstrap-select').addClass('open');alert('sdf');
        }else{
            alert('dsf');
        }
    })*/

      var is_notification_page = $('#is_notification_page').val();
      if(is_notification_page != undefined && is_notification_page == 'notification') {
            loadNotificationList();
      } 

    var page_type_edit = $('#is_edit').val();
    if(page_type_edit != undefined && page_type_edit == 'edit_profile') {

        var qualificationStatus = $('#qualification_id option:selected').val();
        if(qualificationStatus == 1 || qualificationStatus == 2 || qualificationStatus == 3)
        {
            $('.user_curriculum_div').show();
            $('.user_educational_curriculum').attr('required','required');
            $('.user_subject_name_div').hide();
        }
        else{
            $('.user_curriculum_div').hide();
            $('.user_educational_curriculum').removeAttr('required');
            $('#curriculum_id').val('').trigger('change');
            $('.user_subject_name_div').show();
        }

        $('.datepicker').daterangepicker({
            "singleDatePicker": true,
            showDropdowns: true,
            maxDate: new Date(),
        });

        //getCurriculumHierarchy("curriculum_id");
        trrigerChangeEdit();
        var level_trriger_edit = true;
        var grade_trriger_edit = true;
        var education_trriger_edit = true;
        var subject_trriger_edit = true;
        var program_trriger_edit = true;
        var topic_trriger_edit = true;


        function trrigerChangeEdit(){
            setTimeout(function(){

                var curriculum = $('#curriculum_id').length;
                var edit_eductional_systems = $('#eductional_systems_id').length;
                var edit_levels = $('#level_id').length;
                var edit_program = $('#program_id').length;
                var edit_grade = $('#grade_id').length;
                var edit_subject = $('#subject_id').length;
                var edit_topic = $('#topic_id').length;



                var selected_education_edit = $('#edit_education_id').val();
                var selected_level_edit = $('#edit_level_id').val();
                var selected_program_edit = $('#edit_program_id').val();
                var selected_grade_edit = $('#edit_grade_id').val();
                var selected_subject_edit = $('#edit_subject_id').val();
                var selected_topic_edit = $('#edit_topic_id').val();

                if(selected_education_edit != 0 && selected_education_edit !="" && selected_education_edit != "null" && edit_eductional_systems > 0 && education_trriger_edit == true){
                    $('#eductional_systems_id').val(selected_education_edit).trigger("change");
                    education_trriger_edit = false;trrigerChangeEdit()
                }

                if(selected_level_edit != 0 && selected_level_edit != "null" && edit_levels > 0 && selected_level_edit != "" && level_trriger_edit == true){
                    $('#level_id').val(selected_level_edit).trigger("change");
                    level_trriger_edit = false;trrigerChangeEdit()

                }

                if(selected_program_edit != 0 && selected_program_edit !="" && selected_program_edit != "null" && edit_program > 0 && program_trriger_edit == true){
                    $('#program_id').val(selected_program_edit).trigger("change");
                    program_trriger_edit = false;trrigerChangeEdit()
                }

                if(selected_grade_edit != 0 && selected_grade_edit !="" && selected_grade_edit != "null" && edit_grade > 0 && grade_trriger_edit == true){
                    $('#grade_id').val(selected_grade_edit).trigger("change");
                    grade_trriger_edit = false;trrigerChangeEdit()
                }

                if(selected_subject_edit != 0 && selected_subject_edit !="" && selected_subject_edit != "null" && edit_subject > 0 && subject_trriger_edit == true){
                    $('#subject_id').val(selected_subject_edit).trigger("change");
                    subject_trriger_edit = false;trrigerChangeEdit()
                }

                if(selected_topic_edit != 0 && selected_topic_edit !="" && selected_topic_edit != "null" && edit_topic > 0 && topic_trriger_edit == true){
                    $('#topic_id').val(selected_topic_edit).trigger("change");
                    topic_trriger_edit = false;trrigerChangeEdit()
                }
            }, 350);
        }

    }
});

function browse_photo()
{
    $('#user_photo').click();
}

function changeCurriculumn()
{

    var grade_type = $('#qualification_id option:selected').val();
    $('.curriculum_id_error').hide();
    if(grade_type == 1 || grade_type == 2 || grade_type == 3)
    {
        $('.user_curriculum_div').show();
        $('.user_educational_curriculum').attr('required','required');
        $('#subject_name').removeAttr('required');
        $('.user_subject_name_div').hide();

    }
    else
    {
        $('.user_curriculum_div').hide();
        $('.user_educational_curriculum').removeAttr('required');
        $('#subject_name').attr('required','required');
        $('#curriculum_id').val('').trigger('change');
        $('.user_subject_name_div').show();
    }
}


    function upload_photos()
    {
        var file_data = $('#user_photo').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajaxSetup({
            headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
        });
        $('.file_error').hide();
        $.ajax({
            //url: "{{route("tutee.register_user_photo") }}", // point to server-side PHP script
            url: baseUrl + "/tuteeprofile/register_user_photo",
            data: form_data,
            type: 'POST',
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData: false,
            success: function (data) {

                if (data.fail) {
                  // $('#user_img').attr('src','uploads/user/default.jpg');
                  alert(data.errors['file']);
              }
              else {
                filename = data;
                $('#user_img').attr('src','/uploads/user/'+filename);
                $('#photo_hidden').val(filename);

              }
          },
          error: function (xhr, status, error) {
            var json = JSON.parse(xhr.responseText);
                var error = json.file; // or json["Data"]
                $('.file_error').show();
                $('.file_error_value').html(error);
            }
        });
    }

function getEducational(id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/getEducationalsystem',
        data: {
            id: id,
            type: 'topic',
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            //education system dropdown
            if (!jQuery.isEmptyObject(options.education)) {
                $('#eductional_systems_id').empty();
                var html = "<option value=''>Select</option>";
                $.each(options.education, function(key, element) {
                    html += "<option value='" + element['id'] + "' data-name='" + element['name'] + "' data-name='" + element.name + "'>" + element['name'] + "</option>"
                });
                $('#eductional_systems_id').append(html);
                $('.education_div').show();
            } else {
                $('.education_div').hide();
                $('#eductional_systems_id').val('').trigger("change");
            }
            //program dropdown
            if (!jQuery.isEmptyObject(options.program)) {
                $('#program_id').empty();
                var html = "<option value=''>Select</option>";
                $.each(options.program, function(key, element) {
                    html += "<option value='" + element['id'] + "' data-name='" + element['program_name'] + "' data-name='" + element.name + "'>" + element['program_name'] + "</option>"
                });
                $('#program_id').append(html);
                $('.program_div').show();
            } else {
                $('.program_div').hide();
                $('#program_id').val('').trigger("change");
            }
            //subject dropdown
            if (!jQuery.isEmptyObject(options.subject)) {
                $('#subject_id').empty();
                var html = "<option value=''>Select</option>";
                $.each(options.subject, function(key, element) {
                    html += "<option value='" + element['id'] + "' data-name='" + element['subject_name'] + "' data-name='" + element.name + "'>" + element['subject_name'] + "</option>"
                });
                $('#subject_id').append(html);
                // $('#subject_id').attr('required','required');
                $('.subject_div').show();
            } else {
                $('.subject_div').hide();
                // $('#subject_id').removeAttr('required');
                $('#subject_id').val('').trigger("change");
            }
            //grade dropdown
            if (!jQuery.isEmptyObject(options.grade)) {
                $('#grade_id').empty();
                var html = "<option value=''>Select</option>";
                $.each(options.grade, function(key, element) {
                    html += "<option value='" + element['id'] + "' data-type='" + element['grade_type'] + "' data-name='" + element['grade_name'] + "' data-name='" + element.name + "'>" + element['grade_name'] + "</option>"
                });
                $('#grade_id').append(html);
                // $('#grade_id').attr('required','required');
                $('.grade_div').show();
            } else {
                $('.grade_div').hide();
                // $('#grade_id').removeAttr('required');
                $('#grade_id').val('').trigger("change");
            }
            //level dropdown
            if (!jQuery.isEmptyObject(options.level)) {
                $('#level_id').empty();
                var html = "<option value=''>Select</option>";
                $.each(options.level, function(key, element) {
                    html += "<option value='" + element['id'] + "' data-name='" + element['levels_name'] + "' data-name='" + element.name + "'>" + element['levels_name'] + "</option>"
                });
                $('#level_id').append(html);
                $('.level_div').show();
            } else {
                $('.level_div').hide();
                $('#level_id').val('').trigger("change");
            }
            // $('.select2-container').css('width','100%');
        }
    });
}


function getCurriculumsystemForSessionReserve(current_dropdown_id) {
    var grade_type = $('#grade_id option:selected').attr('data-type');
    var type = '';
    var tutor_id = $('#resonseData').val();
    if (typeof grade_type != 'undefined' && grade_type == '0'){
        var type = 'topic';
    }
    
    //var current_dropdown_id = $this.attr('id');
    var dropdown_array = [];
    dropdownIDsArray = $.map($("div[id*=custom_dropdowns]").find("select"), function(item, index) {
        var next = $(item).attr("id");
        dropdown_array.push(next);
        //alert(next);
    });
    if (typeof dropdown_array != 'undefined' && !jQuery.isEmptyObject(dropdown_array)) {
        var current_dropdown_position = dropdown_array.indexOf(current_dropdown_id);
        // alert(current_dropdown_position);
        $.each(dropdown_array, function(key, value) {
            if (key > current_dropdown_position) {
                if (value == "program_id") {
                    $('.program_div').remove();
                }
                if (value == "level_id") {
                    $('.level_div').remove();
                }
                if (value == "eductional_systems_id") {
                    $('.education_div').remove();
                }
                if (value == "grade_id") {
                    $('.grade_div').remove();
                }
                if (value == "subject_id") {
                    $('.subject_div').remove();
                }
                if (value == "topic_id") {
                    $('.topic_div').remove();
                }
            }
        });
    }
    var curriculum_id = $('#curriculum_id').val();
    var eductional_systems_id = $('#eductional_systems_id').val();
    var program_id = $('#program_id').val();
    var subject_id = $('#subject_id').val();
    var grade_id = $('#grade_id').val();
    var level_id = $('#level_id').val();
    var topic_id = $('#topic_id').val();
    if (typeof curriculum_id == 'undefined' || curriculum_id == "" || curriculum_id == 0) {
        return false;
    }
    var curriculum = $('#curriculum_id').length;
    var eductional_systems = $('#eductional_systems_id').length;
    var program = $('#program_id').length;
    var subject = $('#subject_id').length;
    var grade = $('#grade_id').length;
    var level = $('#level_id').length;
    var topic = $('#topic_id').length;
    var total_lenght = parseInt(curriculum) + parseInt(eductional_systems) + parseInt(program) + parseInt(subject) + parseInt(grade) + parseInt(level);
    if (total_lenght > 0) {
        var step = parseInt(total_lenght) - parseInt(1);
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + '/getCurriculumsystemForSessionReserve',
        data: {
            step: step,
            curriculum_id: curriculum_id,
            eductional_systems_id: eductional_systems_id,
            program_id: program_id,
            subject_id: subject_id,
            grade_id: grade_id,
            level_id: level_id,
            topic_id: topic_id,
            type: type,
            tutor_id:tutor_id,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            //education system dropdown
            if (type != "eductional_systems") {
                if (typeof options.education != 'undefined' && !jQuery.isEmptyObject(options.education)) {
                    if (eductional_systems == 0) {
                        var id = "eductional_systems_id";
                        var education_html = '<div class="clearfix form-group education_div"><label class="col-xs-12 col-sm-12 col-md-6 label-title">Education System</label><div class="col-xs-12 col-sm-12 col-md-6"><select name="education_id" id="eductional_systems_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumsystemForSessionReserve(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(education_html);
                    }
                    $('#eductional_systems_id').empty();
                    var html = "<option value=''>Select Educational System</option>";
                    $.each(options.education, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.name + "'>" + element.name + "</option>"
                    });
                    $('#eductional_systems_id').append(html);
                    $('.education_div').show();
                } else {
                    //  $('#eductional_systems_id').val('').trigger("change");
                    //   $('.education_div').remove();
                }
            }
            //program dropdown
            if (type != "programs") {
                if (typeof options.program != 'undefined' && !jQuery.isEmptyObject(options.program)) {
                    if (program == 0) {
                        var id = "program_id";
                        var program_html = '<div class="clearfix form-group program_div"><label class="col-xs-12 col-sm-12 col-md-6 label-title">Program</label><div class="col-xs-12 col-sm-12 col-md-6"><select name="program_id" id="program_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumsystemForSessionReserve(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(program_html);
                    }
                    $('#program_id').empty();
                    var html = "<option value=''>Select Program</option>";
                    $.each(options.program, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.program_name + "'>" + element.program_name + "</option>"
                    });
                    $('#program_id').append(html);
                    $('.program_div').show();
                } else {
                    //$('#program_id').val('').trigger("change");
                    //$('.program_div').remove();
                }
            }
            //subject dropdown
            if (type != "subject") {
                if (typeof options.subject != 'undefined' && !jQuery.isEmptyObject(options.subject)) {
                    if (subject == 0) {
                        var id = "subject_id";
                        var subject_html = '<div class="clearfix form-group subject_div"><label class="col-xs-12 col-sm-12 col-md-6 label-title">Subject</label><div class="col-xs-12 col-sm-12 col-md-6"><select name="subject_id" id="subject_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumsystemForSessionReserve(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(subject_html);
                    }
                    $('#subject_id').empty();
                    var html = "<option value=''>Select Subject</option>";
                    $.each(options.subject, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.subject_name + "'>" + element.subject_name + "</option>"
                    });
                    $('#subject_id').append(html);
                    // $('#subject_id').attr('required','required');
                    $('.subject_div').show();
                } else {
                    // $('#subject_id').removeAttr('required');
                    // $('#subject_id').val('').trigger("change");
                    //$('.subject_div').remove();
                }
            }
            //grade dropdown
            if (type != "grade") {
                if (typeof options.grade != 'undefined' && !jQuery.isEmptyObject(options.grade)) {
                    if (grade == 0) {
                        var id = "grade_id";
                        var grade_html = '<div class="clearfix form-group grade_div"><label class="col-xs-12 col-sm-12 col-md-6 label-title">Grade</label><div class="col-xs-12 col-sm-12 col-md-6"><select name="grade_id" id="grade_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumsystemForSessionReserve(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(grade_html);
                    }
                    $('#grade_id').empty();
                    var html = "<option value=''>Select Grade</option>";
                    $.each(options.grade, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.grade_name + "' data-type='" + element.grade_type + "'>" + element.grade_name + "</option>"
                    });
                    $('#grade_id').append(html);
                    //$("#grade_id").select2();
                    // $('#grade_id').attr('required','required');
                    $('.grade_div').show();
                } else {
                    // $('#grade_id').removeAttr('required');
                    // $('#grade_id').val('').trigger("change");
                    // $('.grade_div').remove();
                }
            }
            //level dropdown
            if (type != "levels") {
                if (typeof options.level != 'undefined' && !jQuery.isEmptyObject(options.level)) {
                    if (level == 0) {
                        var id = "level_id";
                        var level_html = '<div class="clearfix form-group level_div"><label class="col-xs-12 col-sm-12 col-md-6 label-title">Level</label><div class="col-xs-12 col-sm-12 col-md-6"><select name="level_id" id="level_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumsystemForSessionReserve(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(level_html);
                    }
                    $('#level_id').empty();
                    var html = "<option value=''>Select Level</option>";
                    $.each(options.level, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.levels_name + "'>" + element.levels_name + "</option>"
                    });
                    $('#level_id').append(html);
                    $('.level_div').show();
                } else {
                    // $('#level_id').val('').trigger("change");
                    //$('.level_div').remove();
                }
            }
            //topic dropdown
            if (type != "topic") {
                if (typeof options.topic != 'undefined' && !jQuery.isEmptyObject(options.topic)) {
                    if (topic == 0) {
                        var id = "topic_id";
                        var topic_html = '<div class="clearfix form-group topic_div"><label class="col-xs-12 col-sm-12 col-md-6 label-title">Topic</label><div class="col-xs-12 col-sm-12 col-md-6"><select name="topic_id" id="topic_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumsystemForSessionReserve(id,' + options.step + ')"></select><span class="text-right display-block has-error-text topic_id_error topic_id_error_value" style="display: none">Pleas add atleat one curriculum</span></div></div>';
                        $('#custom_dropdowns').append(topic_html);
                    }
                    $('#topic_id').empty();
                    var disable_dropdown = '';
                    var first_option = "Select Topic";
                    if (typeof grade_type != 'undefined' && grade_type == '0'){
                        var disable_dropdown = "disabled='disabled'";
                        var first_option = "All topics";
                    }
                    var html = "<option value=''>"+first_option+"</option>";
                    $.each(options.topic, function(key, element) {
                        html += "<option "+disable_dropdown+" value='" + element.id + "' data-name='" + element.topic_name + "'>" + element.topic_name + "</option>"
                    });
                    $('#topic_id').append(html);
                    $('.topic_div').show();
                } else {
                    // $('#topic_id').val('').trigger("change");
                    //$('.topic_div').remove();
                }
            }
        }
    });
}

/*becometutor function start*/
function getCurriculumHierarchyForBecomeTutor(current_dropdown_id) {
    var grade_type = $('#grade_id_become option:selected').attr('data-type');
    var is_registration = $('#is_registration').val();
    var is_registration_length = $('#is_registration').length;
    var type = '';
    var page_type = '';
    /*if (typeof grade_type != 'undefined' && grade_type == '0'){
        var type = 'topic';
    }*/
    
    //var current_dropdown_id = $this.attr('id');
    if (typeof is_registration != 'undefined' && is_registration == 'register' && is_registration_length > 0){
        var page_type = 'register';
    }

    var dropdown_array = [];
    dropdownIDsArray = $.map($("div[id*=custom_dropdowns_become]").find("select"), function(item, index) {
        var next = $(item).attr("id");
        dropdown_array.push(next);
        //alert(next);
    });
    if (typeof dropdown_array != 'undefined' && !jQuery.isEmptyObject(dropdown_array)) {
        var current_dropdown_position = dropdown_array.indexOf(current_dropdown_id);
        // alert(current_dropdown_position);
        $.each(dropdown_array, function(key, value) {
            if (key > current_dropdown_position) {
                if (value == "program_id_become") {
                    $('.program_div_become').remove();
                }
                if (value == "level_id_become") {
                    $('.level_div_become').remove();
                }
                if (value == "eductional_systems_id_become") {
                    $('.education_div_become').remove();
                }
                if (value == "grade_id_become") {
                    $('.grade_div_become').remove();
                }
                if (value == "subject_id_become") {
                    $('.subject_div_become').remove();
                }
                if (value == "topic_id_become") {
                    $('.topic_div_become').remove();
                }
            }
        });
    }
    var curriculum_id = $('#curriculum_id_become').val();
    var eductional_systems_id = $('#eductional_systems_id_become').val();
    var program_id = $('#program_id_become').val();
    var subject_id = $('#subject_id_become').val();
    var grade_id = $('#grade_id_become').val();
    var level_id = $('#level_id_become').val();
    var topic_id = $('#topic_id_become').val();
    if (typeof curriculum_id == 'undefined' || curriculum_id == "" || curriculum_id == 0) {
        return false;
    }
    var curriculum = $('#curriculum_id_become').length;
    var eductional_systems = $('#eductional_systems_id_become').length;
    var program = $('#program_id_become').length;
    var subject = $('#subject_id_become').length;
    var grade = $('#grade_id_become').length;
    var level = $('#level_id_become').length;
    var topic = $('#topic_id_become').length;
    var total_lenght = parseInt(curriculum) + parseInt(eductional_systems) + parseInt(program) + parseInt(subject) + parseInt(grade) + parseInt(level);
    if (total_lenght > 0) {
        var step = parseInt(total_lenght) - parseInt(1);
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + '/getCurriculumsystem',
        data: {
            step: step,
            curriculum_id: curriculum_id,
            eductional_systems_id: eductional_systems_id,
            program_id: program_id,
            subject_id: subject_id,
            grade_id: grade_id,
            level_id: level_id,
            topic_id: topic_id,
            type: type,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            //education system dropdown
            if (type != "eductional_systems") {
                if (typeof options.education != 'undefined' && !jQuery.isEmptyObject(options.education)) {
                    if (eductional_systems == 0) {
                        var id = "eductional_systems_id_become";
                        var education_html = '<div class="clearfix form-group education_div_become"><label class="col-xs-12 col-sm-6 label-title">Education System</label><div class="col-xs-12 col-sm-6"><select name="education_id" id="eductional_systems_id_become" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchyForBecomeTutor(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_become').append(education_html);
                    }
                    $('#eductional_systems_id_become').empty();
                    var html = "<option value=''>Select Educational System</option>";
                    $.each(options.education, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.name + "'>" + element.name + "</option>"
                    });
                    $('#eductional_systems_id_become').append(html);
                    $('.education_div_become').show();
                } else {
                    //  $('#eductional_systems_id_become').val('').trigger("change");
                    //   $('.education_div_become').remove();
                }
            }
            //program dropdown
            if (type != "programs") {
                if (typeof options.program != 'undefined' && !jQuery.isEmptyObject(options.program)) {
                    if (program == 0) {
                        var id = "program_id_become";
                        var program_html = '<div class="clearfix form-group program_div_become"><label class="col-xs-12 col-sm-6 label-title">Program</label><div class="col-xs-12 col-sm-6"><select name="program_id" id="program_id_become" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchyForBecomeTutor(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_become').append(program_html);
                    }
                    $('#program_id_become').empty();
                    var html = "<option value=''>Select Program</option>";
                    $.each(options.program, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.program_name + "'>" + element.program_name + "</option>"
                    });
                    $('#program_id_become').append(html);
                    $('.program_div_become').show();
                } else {
                    //$('#program_id_become').val('').trigger("change");
                    //$('.program_div_become').remove();
                }
            }
            //subject dropdown
            if (type != "subject" && page_type != 'register') {
                if (typeof options.subject != 'undefined' && !jQuery.isEmptyObject(options.subject)) {
                    if (subject == 0) {
                        var id = "subject_id_become";
                        var subject_html = '<div class="clearfix form-group subject_div_become"><label class="col-xs-12 col-sm-6 label-title">Subject</label><div class="col-xs-12 col-sm-6"><select name="subject_id" id="subject_id_become" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchyForBecomeTutor(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_become').append(subject_html);
                    }
                    $('#subject_id_become').empty();
                    var html = "<option value=''>Select Subject</option>";
                    $.each(options.subject, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.subject_name + "'>" + element.subject_name + "</option>"
                    });
                    $('#subject_id_become').append(html);
                    // $('#subject_id_become').attr('required','required');
                    $('.subject_div_become').show();
                } else {
                    // $('#subject_id_become').removeAttr('required');
                    // $('#subject_id_become').val('').trigger("change");
                    //$('.subject_div_become').remove();
                }
            }else if (page_type == 'register') {
                if (typeof options.subject != 'undefined' && !jQuery.isEmptyObject(options.subject)) {
                    if (subject == 0) {
                        var id = "subject_id_become";
                        var subject_html = '<div class="clearfix form-group subject_div_become"><label class="col-xs-12 col-sm-6 label-title">Subject</label><div class="col-xs-12 col-sm-6"><select name="subject_id" id="subject_id_become" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchyForBecomeTutor(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_become').append(subject_html);
                    }
                    $('#subject_id_become').empty();
                    var html = "<option value=''>Select Subject</option>";
                    $.each(options.subject, function(key, element) {
                        if(key == '0'){
                            html += "<option selected='selected' value='" + element.id + "' data-name='" + element.subject_name + "'>" + element.subject_name + "</option>"    
                        }else{
                            html += "<option value='" + element.id + "' data-name='" + element.subject_name + "'>" + element.subject_name + "</option>"
                        }
                        
                    });
                    $('#subject_id_become').append(html);
                    $('.subject_div_become').hide();
                }
            }

            //grade dropdown
            if (type != "grade") {
                if (typeof options.grade != 'undefined' && !jQuery.isEmptyObject(options.grade)) {
                    if (grade == 0) {
                        var id = "grade_id_become";
                        var grade_html = '<div class="clearfix form-group grade_div_become"><label class="col-xs-12 col-sm-6 label-title">Grade</label><div class="col-xs-12 col-sm-6"><select name="grade_id" id="grade_id_become" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchyForBecomeTutor(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_become').append(grade_html);
                    }
                    $('#grade_id_become').empty();
                    var html = "<option value=''>Select Grade</option>";
                    $.each(options.grade, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.grade_name + "' data-type='" + element.grade_type + "'>" + element.grade_name + "</option>"
                    });
                    $('#grade_id_become').append(html);
                    //$("#grade_id_become").select2();
                    // $('#grade_id_become').attr('required','required');
                    $('.grade_div_become').show();
                } else {
                    // $('#grade_id_become').removeAttr('required');
                    // $('#grade_id_become').val('').trigger("change");
                    // $('.grade_div_become').remove();
                }
            }
            //level dropdown
            if (type != "levels") {
                if (typeof options.level != 'undefined' && !jQuery.isEmptyObject(options.level)) {
                    if (level == 0) {
                        var id = "level_id_become";
                        var level_html = '<div class="clearfix form-group level_div_become"><label class="col-xs-12 col-sm-6 label-title">Level</label><div class="col-xs-12 col-sm-6"><select name="level_id" id="level_id_become" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchyForBecomeTutor(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_become').append(level_html);
                    }
                    $('#level_id_become').empty();
                    var html = "<option value=''>Select Level</option>";
                    $.each(options.level, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.levels_name + "'>" + element.levels_name + "</option>"
                    });
                    $('#level_id_become').append(html);
                    $('.level_div_become').show();
                } else {
                    // $('#level_id_become').val('').trigger("change");
                    //$('.level_div_become').remove();
                }
            }
            //topic dropdown
            if (type != "topic" && page_type != 'register') {
                if (typeof options.topic != 'undefined' && !jQuery.isEmptyObject(options.topic)) {
                    if (topic == 0) {
                        var id = "topic_id_become";
                        var topic_html = '<div class="clearfix form-group topic_div_become"><label class="col-xs-12 col-sm-6 label-title">Topic</label><div class="col-xs-12 col-sm-6"><select name="topic_id" id="topic_id_become" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchyForBecomeTutorForPayment(id,' + options.step + ')"></select><span class="text-right display-block has-error-text topic_id_error topic_id_error_value" style="display: none">Pleas add atleat one curriculum</span></div></div>';
                        $('#custom_dropdowns_become').append(topic_html);
                    }
                    $('#topic_id_become').empty();
                    var disable_dropdown = '';
                    var first_option = "Select Topic";
                    if (typeof grade_type != 'undefined' && grade_type == '0'){
                        var disable_dropdown = "disabled='disabled'";
                        var first_option = "All topics";
                    }
                    var html = "<option value=''>"+first_option+"</option>";
                    $.each(options.topic, function(key, element) {
                        html += "<option "+disable_dropdown+" value='" + element.id + "' data-name='" + element.topic_name + "'>" + element.topic_name + "</option>"
                    });
                    $('#topic_id_become').append(html);
                    $('.topic_div_become').show();
                } else {
                    // $('#topic_id_become').val('').trigger("change");
                    //$('.topic_div_become').remove();
                }
            }
        }
    });
}
/*become tutor function end*/

function getCurriculumHierarchy(current_dropdown_id) {
    $('.curriculum_id_error').hide();
    var grade_type = $('#grade_id option:selected').attr('data-type');
    var is_registration = $('#is_registration').val();
    var is_registration_length = $('#is_registration').length;
    var type = '';
    var page_type = '';
    /*if (typeof grade_type != 'undefined' && grade_type == '0'){
        var type = 'topic';
    }*/
    
    //var current_dropdown_id = $this.attr('id');
    if (typeof is_registration != 'undefined' && is_registration == 'register' && is_registration_length > 0){
        var page_type = 'register';
    }

    var dropdown_array = [];
    dropdownIDsArray = $.map($("div[id*=custom_dropdowns]").find("select"), function(item, index) {
        var next = $(item).attr("id");
        dropdown_array.push(next);
        //alert(next);
    });
    if (typeof dropdown_array != 'undefined' && !jQuery.isEmptyObject(dropdown_array)) {
        var current_dropdown_position = dropdown_array.indexOf(current_dropdown_id);
        // alert(current_dropdown_position);
        $.each(dropdown_array, function(key, value) {
            if (key > current_dropdown_position) {
                if (value == "program_id") {
                    $('.program_div').remove();
                }
                if (value == "level_id") {
                    $('.level_div').remove();
                }
                if (value == "eductional_systems_id") {
                    $('.education_div').remove();
                }
                if (value == "grade_id") {
                    $('.grade_div').remove();
                }
                if (value == "subject_id") {
                    $('.subject_div').remove();
                }
                if (value == "topic_id") {
                    $('.topic_div').remove();
                }
            }
        });
    }
    var curriculum_id = $('#curriculum_id').val();
    var eductional_systems_id = $('#eductional_systems_id').val();
    var program_id = $('#program_id').val();
    var subject_id = $('#subject_id').val();
    var grade_id = $('#grade_id').val();
    var level_id = $('#level_id').val();
    var topic_id = $('#topic_id').val();
    if (typeof curriculum_id == 'undefined' || curriculum_id == "" || curriculum_id == 0) {
        return false;
    }
    var curriculum = $('#curriculum_id').length;
    var eductional_systems = $('#eductional_systems_id').length;
    var program = $('#program_id').length;
    var subject = $('#subject_id').length;
    var grade = $('#grade_id').length;
    var level = $('#level_id').length;
    var topic = $('#topic_id').length;
    var total_lenght = parseInt(curriculum) + parseInt(eductional_systems) + parseInt(program) + parseInt(subject) + parseInt(grade) + parseInt(level);
    if (total_lenght > 0) {
        var step = parseInt(total_lenght) - parseInt(1);
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + '/getCurriculumsystem',
        data: {
            step: step,
            curriculum_id: curriculum_id,
            eductional_systems_id: eductional_systems_id,
            program_id: program_id,
            subject_id: subject_id,
            grade_id: grade_id,
            level_id: level_id,
            topic_id: topic_id,
            type: type,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            //education system dropdown
            if (type != "eductional_systems") {
                if (typeof options.education != 'undefined' && !jQuery.isEmptyObject(options.education)) {
                    if (eductional_systems == 0) {
                        var id = "eductional_systems_id";
                        var education_html = '<div class="clearfix form-group education_div"><label class="col-xs-12 col-sm-6 label-title">Education System</label><div class="col-xs-12 col-sm-6"><select name="education_id" id="eductional_systems_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(education_html);
                    }
                    $('#eductional_systems_id').empty();
                    var html = "<option value=''>Select Educational System</option>";
                    $.each(options.education, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.name + "'>" + element.name + "</option>"
                    });
                    $('#eductional_systems_id').append(html);
                    $('.education_div').show();
                } else {
                    //  $('#eductional_systems_id').val('').trigger("change");
                    //   $('.education_div').remove();
                }
            }
            //program dropdown
            if (type != "programs") {
                if (typeof options.program != 'undefined' && !jQuery.isEmptyObject(options.program)) {
                    if (program == 0) {
                        var id = "program_id";
                        var program_html = '<div class="clearfix form-group program_div"><label class="col-xs-12 col-sm-6 label-title">Program</label><div class="col-xs-12 col-sm-6"><select name="program_id" id="program_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(program_html);
                    }
                    $('#program_id').empty();
                    var html = "<option value=''>Select Program</option>";
                    $.each(options.program, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.program_name + "'>" + element.program_name + "</option>"
                    });
                    $('#program_id').append(html);
                    $('.program_div').show();
                } else {
                    //$('#program_id').val('').trigger("change");
                    //$('.program_div').remove();
                }
            }
            //subject dropdown
            if (type != "subject" && page_type != 'register') {
                if (typeof options.subject != 'undefined' && !jQuery.isEmptyObject(options.subject)) {
                    if (subject == 0) {
                        var id = "subject_id";
                        var subject_html = '<div class="clearfix form-group subject_div"><label class="col-xs-12 col-sm-6 label-title">Subject</label><div class="col-xs-12 col-sm-6"><select name="subject_id" id="subject_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(subject_html);
                    }
                    $('#subject_id').empty();
                    var html = "<option value=''>Select Subject</option>";
                    $.each(options.subject, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.subject_name + "'>" + element.subject_name + "</option>"
                    });
                    $('#subject_id').append(html);
                    // $('#subject_id').attr('required','required');
                    $('.subject_div').show();
                } else {
                    // $('#subject_id').removeAttr('required');
                    // $('#subject_id').val('').trigger("change");
                    //$('.subject_div').remove();
                }
            }else if (page_type == 'register') {
                if (typeof options.subject != 'undefined' && !jQuery.isEmptyObject(options.subject)) {
                    if (subject == 0) {
                        var id = "subject_id";
                        var subject_html = '<div class="clearfix form-group subject_div"><label class="col-xs-12 col-sm-6 label-title">Subject</label><div class="col-xs-12 col-sm-6"><select name="subject_id" id="subject_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(subject_html);
                    }
                    $('#subject_id').empty();
                    var html = "<option value=''>Select Subject</option>";
                    $.each(options.subject, function(key, element) {
                        if(key == '0'){
                            html += "<option selected='selected' value='" + element.id + "' data-name='" + element.subject_name + "'>" + element.subject_name + "</option>"    
                        }else{
                            html += "<option value='" + element.id + "' data-name='" + element.subject_name + "'>" + element.subject_name + "</option>"
                        }
                        
                    });
                    $('#subject_id').append(html);
                    $('.subject_div').hide();
                }
            }

            //grade dropdown
            if (type != "grade") {
                if (typeof options.grade != 'undefined' && !jQuery.isEmptyObject(options.grade)) {
                    if (grade == 0) {
                        var id = "grade_id";
                        var grade_html = '<div class="clearfix form-group grade_div"><label class="col-xs-12 col-sm-6 label-title">Grade</label><div class="col-xs-12 col-sm-6"><select name="grade_id" id="grade_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(grade_html);
                    }
                    $('#grade_id').empty();
                    var html = "<option value=''>Select Grade</option>";
                    $.each(options.grade, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.grade_name + "' data-type='" + element.grade_type + "'>" + element.grade_name + "</option>"
                    });
                    $('#grade_id').append(html);
                    //$("#grade_id").select2();
                    // $('#grade_id').attr('required','required');
                    $('.grade_div').show();
                } else {
                    // $('#grade_id').removeAttr('required');
                    // $('#grade_id').val('').trigger("change");
                    // $('.grade_div').remove();
                }
            }
            //level dropdown
            if (type != "levels") {
                if (typeof options.level != 'undefined' && !jQuery.isEmptyObject(options.level)) {
                    if (level == 0) {
                        var id = "level_id";
                        var level_html = '<div class="clearfix form-group level_div"><label class="col-xs-12 col-sm-6 label-title">Level</label><div class="col-xs-12 col-sm-6"><select name="level_id" id="level_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(level_html);
                    }
                    $('#level_id').empty();
                    var html = "<option value=''>Select Level</option>";
                    $.each(options.level, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.levels_name + "'>" + element.levels_name + "</option>"
                    });
                    $('#level_id').append(html);
                    $('.level_div').show();
                } else {
                    // $('#level_id').val('').trigger("change");
                    //$('.level_div').remove();
                }
            }
            //topic dropdown
            if (type != "topic" && page_type != 'register') {
                if (typeof options.topic != 'undefined' && !jQuery.isEmptyObject(options.topic)) {
                    if (topic == 0) {
                        var id = "topic_id";
                        var topic_html = '<div class="clearfix form-group topic_div"><label class="col-xs-12 col-sm-6 label-title">Topic</label><div class="col-xs-12 col-sm-6"><select name="topic_id" id="topic_id" class="form-control custom-select dynamic_dropdown" onchange="getCurriculumHierarchyForPayment(id,' + options.step + ')"></select><span class="text-right display-block has-error-text topic_id_error topic_id_error_value" style="display: none">Pleas add atleat one curriculum</span></div></div>';
                        $('#custom_dropdowns').append(topic_html);
                    }
                    $('#topic_id').empty();
                    var disable_dropdown = '';
                    var first_option = "Select Topic";
                    if (typeof grade_type != 'undefined' && grade_type == '0'){
                        var disable_dropdown = "disabled='disabled'";
                        var first_option = "All topics";
                    }
                    var html = "<option value=''>"+first_option+"</option>";
                    $.each(options.topic, function(key, element) {
                        html += "<option "+disable_dropdown+" value='" + element.id + "' data-name='" + element.topic_name + "'>" + element.topic_name + "</option>"
                    });
                    $('#topic_id').append(html);
                    $('.topic_div').show();
                } else {
                    // $('#topic_id').val('').trigger("change");
                    //$('.topic_div').remove();
                }
            }
        }
    });
}

function getCurriculumHierarchyForPayment(current_dropdown_id) {
    var grade_type = $('#grade_id_payment option:selected').attr('data-type');
    var type = '';
    /* if (typeof grade_type != 'undefined' && grade_type == '0'){
        var type = 'topic';
    }*/
    //var current_dropdown_id = $this.attr('id');
    var dropdown_array = [];
    dropdownIDsArray = $.map($("div[id*=custom_dropdowns_payment]").find("select"), function(item, index) {
        var next = $(item).attr("id");
        dropdown_array.push(next);
        //alert(next);
    });
    if (typeof dropdown_array != 'undefined' && !jQuery.isEmptyObject(dropdown_array)) {
        var current_dropdown_position = dropdown_array.indexOf(current_dropdown_id);
        $.each(dropdown_array, function(key, value) {
            if (key > current_dropdown_position) {
                if (value == "program_id_payment") {
                    $('.program_div_payment').remove();
                }
                if (value == "level_id_payment") {
                    $('.level_div_payment').remove();
                }
                if (value == "eductional_systems_id_payment") {
                    $('.education_div_payment').remove();
                }
                if (value == "grade_id_payment") {
                    $('.grade_div_payment').remove();
                }
                if (value == "subject_id_payment") {
                    $('.subject_div_payment').remove();
                }
                if (value == "topic_id_payment") {
                    $('.topic_div_payment').remove();
                }
            }
        });
    }
    var curriculum_id = $('#curriculum_id_payment').val();
    var eductional_systems_id = $('#eductional_systems_id_payment').val();
    var program_id = $('#program_id_payment').val();
    var subject_id = $('#subject_id_payment').val();
    var grade_id = $('#grade_id_payment').val();
    var level_id = $('#level_id_payment').val();
    var topic_id = $('#topic_id_payment').val();
    if (typeof curriculum_id == 'undefined' || curriculum_id == "" || curriculum_id == 0) {
        return false;
    }
    var curriculum = $('#curriculum_id_payment').length;
    var eductional_systems = $('#eductional_systems_id_payment').length;
    var program = $('#program_id_payment').length;
    var subject = $('#subject_id_payment').length;
    var grade = $('#grade_id_payment').length;
    var level = $('#level_id_payment').length;
    var topic = $('#topic_id_payment').length;
    var total_lenght = parseInt(curriculum) + parseInt(eductional_systems) + parseInt(program) + parseInt(subject) + parseInt(grade) + parseInt(level);
    if (total_lenght > 0) {
        var step = parseInt(total_lenght) - parseInt(1);
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + '/getCurriculumsystem',
        data: {
            step: step,
            curriculum_id: curriculum_id,
            eductional_systems_id: eductional_systems_id,
            program_id: program_id,
            subject_id: subject_id,
            grade_id: grade_id,
            level_id: level_id,
            topic_id: topic_id,
            type: type,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            //education system dropdown
            if (type != "eductional_systems") {
                if (typeof options.education != 'undefined' && !jQuery.isEmptyObject(options.education)) {
                    if (eductional_systems == 0) {
                        var id = "eductional_systems_id_payment";
                        var education_html = '<div class="clearfix form-group education_div_payment"><label class="col-xs-12 col-sm-6 label-title">Education System</label><div class="col-xs-12 col-sm-6"><select name="education_id_payment" id="eductional_systems_id_payment" class="form-control custom-select dynamic_dropdown_payment" onchange="getCurriculumHierarchyForPayment(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_payment').append(education_html);
                    }
                    $('#eductional_systems_id_payment').empty();
                    var html = "<option value=''>Select Educational System</option>";
                    $.each(options.education, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.name + "'>" + element.name + "</option>"
                    });
                    $('#eductional_systems_id_payment').append(html);
                    $('.education_div_payment').show();
                } else {
                    //  $('#eductional_systems_id').val('').trigger("change");
                    //   $('.education_div').remove();
                }
            }
            //program dropdown
            if (type != "programs") {
                if (typeof options.program != 'undefined' && !jQuery.isEmptyObject(options.program)) {
                    if (program == 0) {
                        var id = "program_id_payment";
                        var program_html = '<div class="clearfix form-group program_div_payment"><label class="col-xs-12 col-sm-6 label-title">Program</label><div class="col-xs-12 col-sm-6"><select name="program_id_payment" id="program_id_payment" class="form-control custom-select dynamic_dropdown_payment" onchange="getCurriculumHierarchyForPayment(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_payment').append(program_html);
                    }
                    $('#program_id_payment').empty();
                    var html = "<option value=''>Select Program</option>";
                    $.each(options.program, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.program_name + "'>" + element.program_name + "</option>"
                    });
                    $('#program_id_payment').append(html);
                    $('.program_div_payment').show();
                } else {
                    //$('#program_id').val('').trigger("change");
                    //$('.program_div').remove();
                }
            }
            //subject dropdown
            if (type != "subject") {
                if (typeof options.subject != 'undefined' && !jQuery.isEmptyObject(options.subject)) {
                    if (subject == 0) {
                        var id = "subject_id_payment";
                        var subject_html = '<div class="clearfix form-group subject_div_payment"><label class="col-xs-12 col-sm-6 label-title">Subject</label><div class="col-xs-12 col-sm-6"><select name="subject_id_payment" id="subject_id_payment" class="form-control custom-select dynamic_dropdown_payment" onchange="getCurriculumHierarchyForPayment(id,' + options.step + ')" required="required"></select><span class="text-right display-block has-error-text subject_id_payment_error subject_id_payment_error_value" style="display: none">Pleas add atleat one curriculum</span></div></div>';
                        $('#custom_dropdowns_payment').append(subject_html);
                    }
                    $('#subject_id_payment').empty();
                    var html = "<option value=''>Select Subject</option>";
                    $.each(options.subject, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.subject_name + "'>" + element.subject_name + "</option>"
                    });
                    $('#subject_id_payment').append(html);
                    // $('#subject_id').attr('required','required');
                    $('.subject_div_payment').show();
                } else {
                    // $('#subject_id').removeAttr('required');
                    // $('#subject_id').val('').trigger("change");
                    //$('.subject_div').remove();
                }
            }
            //grade dropdown
            if (type != "grade") {
                if (typeof options.grade != 'undefined' && !jQuery.isEmptyObject(options.grade)) {
                    if (grade == 0) {
                        var id = "grade_id_payment";
                        var grade_html = '<div class="clearfix form-group grade_div_payment"><label class="col-xs-12 col-sm-6 label-title">Grade</label><div class="col-xs-12 col-sm-6"><select name="grade_id_payment" id="grade_id_payment" class="form-control custom-select dynamic_dropdown_payment" onchange="getCurriculumHierarchyForPayment(id,' + options.step + ')" required="required"></select><span class="text-right display-block has-error-text grade_id_payment_error grade_id_payment_error_value" style="display: none">Pleas add atleat one curriculum</span></div></div>';
                        $('#custom_dropdowns_payment').append(grade_html);
                    }
                    $('#grade_id_payment').empty();
                    var html = "<option value=''>Select Grade</option>";
                    $.each(options.grade, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.grade_name + "' data-type='" + element.grade_type + "'>" + element.grade_name + "</option>"
                    });
                    $('#grade_id_payment').append(html);
                    //$("#grade_id").select2();
                    // $('#grade_id').attr('required','required');
                    $('.grade_div_payment').show();
                } else {
                    // $('#grade_id').removeAttr('required');
                    // $('#grade_id').val('').trigger("change");
                    // $('.grade_div').remove();
                }
            }
            //level dropdown
            if (type != "levels") {
                if (typeof options.level != 'undefined' && !jQuery.isEmptyObject(options.level)) {
                    if (level == 0) {
                        var id = "level_id_payment";
                        var level_html = '<div class="clearfix form-group level_div_payment"><label class="col-xs-12 col-sm-6 label-title">Level</label><div class="col-xs-12 col-sm-6"><select name="level_id_payment" id="level_id_payment" class="form-control custom-select dynamic_dropdown_payment" onchange="getCurriculumHierarchyForPayment(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns_payment').append(level_html);
                    }
                    $('#level_id_payment').empty();
                    var html = "<option value=''>Select Level</option>";
                    $.each(options.level, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.levels_name + "'>" + element.levels_name + "</option>"
                    });
                    $('#level_id_payment').append(html);
                    $('.level_div_payment').show();
                } else {
                    // $('#level_id').val('').trigger("change");
                    //$('.level_div').remove();
                }
            }
            //topic dropdown
            if (type != "topic") {
                if (typeof options.topic != 'undefined' && !jQuery.isEmptyObject(options.topic)) {
                    if (topic == 0) {
                        var id = "topic_id_payment";
                        var topic_html = '<div class="clearfix form-group topic_div_payment"><label class="col-xs-12 col-sm-6 label-title">Topic</label><div class="col-xs-12 col-sm-6"><select name="topic_id_payment" id="topic_id_payment" class="form-control custom-select dynamic_dropdown_payment" onchange="getCurriculumHierarchyForPayment(id,' + options.step + ')"></select><span class="text-right display-block has-error-text topic_id_payment_error topic_id_payment_error_value" style="display: none">Pleas add atleat one curriculum</span></div></div>';
                        $('#custom_dropdowns_payment').append(topic_html);
                    }
                    $('#topic_id_payment').empty();
                    /*var html = "<option value=''>Select Topic</option>";
                    $.each(options.topic, function(key, element) {
                        html += "<option value='" + element.id + "' data-name='" + element.topic_name + "'>" + element.topic_name + "</option>"
                    });*/
                    var disable_dropdown = '';
                    var first_option = "Select Topic";
                    if (typeof grade_type != 'undefined' && grade_type == '0'){
                        var disable_dropdown = "disabled='disabled'";
                        var first_option = "All topics";
                    }
                    var html = "<option value=''>"+first_option+"</option>";
                    $.each(options.topic, function(key, element) {
                        html += "<option "+disable_dropdown+" value='" + element.id + "' data-name='" + element.topic_name + "'>" + element.topic_name + "</option>"
                    });

                    $('#topic_id_payment').append(html);
                    $('.topic_div_payment').show();
                } else {
                    // $('#topic_id').val('').trigger("change");
                    //$('.topic_div').remove();
                }
            }
        }
    });
}

function getState(id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/getState',
        data: {
            id: id,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            //     //education system dropdown
            $('#state_id').empty();
            var html = "<option value=''>State</option>";
            if (!jQuery.isEmptyObject(options)) {
                $.each(options, function(key, element) {
                    html += "<option value='" + key + "' data-name='" + element + "'>" + element + "</option>"
                });
                
            }else{
             $('#city_id').empty();
             var html_city = "<option value=''>City</option>";
             $('#city_id').append(html_city);
         }
         $('#state_id').append(html);
     }
 });
}

function getCity(state_id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/getCity',
        data: {
            id: state_id,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            //     //education system dropdown
            $('#city_id').empty();
            var html = "<option value=''>City</option>";
            if (!jQuery.isEmptyObject(options)) {
              
                $.each(options, function(key, element) {
                    html += "<option value='" + key + "' data-name='" + element+ "'>" + element + "</option>"
                });
            }
            $('#city_id').append(html);
        }
    });
}

function tutorJoinSession(id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/tutorJoinSession',
        data: {
            id: id,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            data = $.parseJSON(data);
            if (data.error) 
            {
                alert(data.error);
            }
            else 
            {
                window.location.href = data.link;
            }
        }
    });

    //window.location.href = baseUrl + "/tuteeJoinSession/" + id;
}

function tuteeJoinSession(id) {

    $.ajax({
        type: 'POST',
        url: baseUrl + '/send_notification_for_joinsession',
        data: {
            id: id,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
           
        }
    });
    window.location.href = baseUrl + "/tuteeJoinSession/" + id;
}

function checkTutorJoinSession(id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/checkTutorJoinSession',
        data: {
            id: id,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            var final_data = [];
            final_data = $.parseJSON(data);
            $('.tutor-join-session').addClass('hidden');
            $.each(final_data, function(key, element) {
                $('.tutor-join-session-' + element.id).removeClass('hidden');
            });
        }
    });
}

function checkTuteeJoinSession(id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/checkTuteeJoinSession',
        data: {
            id: id,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            data = $.parseJSON(data);
            $('.tutee-join-session').addClass('hidden');
            $.each(data, function(key, element) {
                $('.tutee-join-session-' + element.id).removeClass('hidden');
            });
        }
    });
}

function readNotifications(header_notification_limit) {
    var limit = header_notification_limit;
    $.ajax({
        type: 'POST',
        url: baseUrl + '/read_notification',
        data: {
            limit: limit,
            _token: $('#csrf_token_header').val()
        },
        success: function(data) {
            
        }
    });
}


    $('body').on('click','#oldpsd-checkbox', function()
    {
       // alert($(this).attr('data-status'));
       if($(this).attr('data-status') == "unchecked")
       {
          $(this).attr('data-status', 'checked');
          $('#oldpsd').attr('type',"text");
       }
        else
        {
          $(this).attr('data-status', 'unchecked');
          $('#oldpsd').attr('type',"password");
        }
    });

    $('body').on('click','#newpsd-checkbox',function()
    {
       // alert($(this).attr('data-status'));
       if($(this).attr('data-status') == "unchecked")
       {
          $(this).attr('data-status', 'checked');
          $('#newpsd').attr('type',"text");
       }
        else
        {
          $(this).attr('data-status', 'unchecked');
          $('#newpsd').attr('type',"password");
        }
    });


    $('body').on('click','#confirmpsd-checkbox',function()
    {
       // alert($(this).attr('data-status'));
       if($(this).attr('data-status') == "unchecked")
       {
          $(this).attr('data-status', 'checked');
          $('#confirmpsd').attr('type',"text");
       }
        else
        {
          $(this).attr('data-status', 'unchecked');
          $('#confirmpsd').attr('type',"password");
        }
    });


/*Left side bar ajax page reload changes start from here*/
    $('body').on('click','#share_exp_tutee',function(){
       loadShareExperience();
    })

    function loadShareExperience(){
     // $('#scroll').scrollTop(1000000);
         $.ajax({
            type: 'GET',
            url: baseUrl + '/share-experience',
            success: function(data) {
                var status = data.status;
                if(status == "success"){
                  /*  $('html, body').animate({
                    scrollTop: $(".mid-section").offset().top
                     }, 10);*/
                    $('.mid-section').html(data.html);
                    loadCommonscript();
                   
                    // var newurl = window.location.protocol + "//" + window.location.host + "share-experience";
                      history.pushState(null, null,'/share-experience');
                      
                   
                }
                
            }
        });
    }

    $('body').on('click', '.exp_submit', function()
    {
        var exp_desc = $('.text_exp').val();
        if($.trim(exp_desc) == ""){
            $('.custom_error_msg').html('Experience description is required');
            $('.custom_error_msg').show();
            return false;
        }else{
            $('.custom_error_msg').show();
        }
            $.ajax({
                  type: 'POST',
                   url: baseUrl + '/share-experience/store',
                  data: {experience:exp_desc,_token: $('#csrf_token_header').val()},
                  success: function(data)
                  {
                    $('.custom_success_msg').html('Thank you for sharing your experience !!!');
                    $('.custom_success_msg').show();
                    $('.text_exp').val('');
                    $('.custom_success_msg').delay(5000).fadeOut('slow');
                      $('.custom_error_msg').hide();
                  }
              });
    });

    /*Tutee Dashboard start*/
        $('body').on('click','#tutee_dashboard_link',function(){
           loadTuteeDashboard();
        })

        function loadTuteeDashboard(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutee-dashboard',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      /*  $('html, body').animate({
                        scrollTop: $(".mid-section").offset().top
                         }, 10);*/
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                          $('.suggested-tutor-scroll').each(function(){
                             var get_width = $('.suggested-tutor-list').length * $('.suggested-tutor-list').innerWidth();

                             $('.suggested-tutor-scroll').width(get_width); 
                             $('.suggested-tutor-list-wrapper').perfectScrollbar();
                         }); 

                          
                          $('[data-toggle="tooltip"]').tooltip();

                          var record= data.user;
                          google.charts.load('current', {packages: ['corechart', 'bar']});
                            google.charts.setOnLoadCallback(function(){ drawChart(record) });

                           $(window).on('resize',function(){
                             $("[data-toggle = 'tooltip']").tooltip('hide'); 
                         });
                            var user_id = $('#logged_in_user_id').val();
                            checkTuteeJoinSession(user_id);
                           // refreshNotifications();
                         history.pushState(null, null,'/tutee-dashboard');
                          
                       
                    }
                    
                }
            });
        }

        function drawChart(data_user) {

                var record=data_user; 
          
                 var data = new google.visualization.DataTable();
                 data.addColumn('string', 'Session Type');
                 data.addColumn('number', 'Total');
                 for(var k in record){
                      var v = record[k];

                       data.addRow([k,v]);
                       //console.log(v);
                    }
                 var options = {
                    title: '',
                    is3D: false,
                    width: '90%',
                    height: '90%',
                    chartArea: {
                        left: "3%",
                        top: "3%",
                        height: "80%",
                        width: "80%"
                    },
                    /*legend:{position: 'top'},
        */
                  };
                  var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
                  chart.draw(data, options);
        }

        function loadCommonscript(){
            refreshNotifications();

             $('.rating').rating();
             var windowWidth;
             $('.rating-input').unbind('mouseenter mouseleave click');
             

            $('.banner-section').height($(window).height());
            
            $('.left-aside-dashboard').mouseenter(function(){
                if($(window).width() >= 1201){
                    $('body').addClass('left-side-expand');
                }
            }).mouseleave(function(){
                if($(window).width() >= 1201){
                    $('body').removeClass('left-side-expand');

                }
            });
            $('body').toggleClass('left-side-expand');

             $(window).on('load resize',function(){
                $('.banner-section').height($(window).height());
                
                $('.left-aside-dashboard').mouseenter(function(){
                    if($(window).width() >= 1201){
                        $('body').addClass('left-side-expand');
                    }
                }).mouseleave(function(){
                    if($(window).width() >= 1201){
                        $('body').removeClass('left-side-expand');

                    }
                });
            });

               $('.tutor-box-radio-box input[name=available]').on('change', function() {
                 if($('input[name=available]:checked').val() == 2){
                     $('.available-specific-day').stop().fadeOut('slow');

                     setTimeout(function(){ $('.available-everyday').stop().fadeIn();}, 600);

                 }
                 else{
                     $('.available-everyday').stop().fadeOut('slow');
                     setTimeout(function(){ $('.available-specific-day').stop().fadeIn();}, 600);
                 }
             }); 




                 //Date range picker with time picker
                 if($('#reservation, #payment-date').length){
                    if($(window).width() <= 991){
                        $('#reservation').daterangepicker({
                            "opens": "top",
                            "drops": "up",
                            "parentEl": ".datetime-range"
                        });
                        $('#payment-date').daterangepicker({
                            "opens": "top",
                            "drops": "down",
                            "parentEl": ".datetime-range"
                        });
                        
                    }else{
                       $('#reservation').daterangepicker({
                        "opens": "right",
                    });
                       $('#payment-date').daterangepicker({
                        "opens": "left",
                    });
                   }
               }
               if($('.perfect-scroll').length){
                $('.perfect-scroll').perfectScrollbar();
            }
            //tab to accordion
            if($('#userDetail').length){
                $('#userDetail').tabCollapse();
            }
            if($('.datepicker').length){    
                $('.datepicker').daterangepicker({
                 "singleDatePicker": true,
                 showDropdowns: true
             });
            }
            
                if($('.time-picker').length > 0){
                    $('.time-picker').datetimepicker({
                        format: 'LT'
                    });
                }

                $('.filter-mobile').click(function(){
                    $('body').addClass('open-drawar');
                })
                $('.close-drawer').click(function(){
                    $('body').removeClass('open-drawar');
                })
                
                /*inline calender start form here*/
                if($('.calender-inline').length > 0){
                   $('.calender-inline').datetimepicker({
                     inline: true,
                     format: 'DD/MM/YYYY'  
                 });
               }
               /*inline calender end from here*/


               $('.rating').on('change', function(){
                var rating_val = $(this).val();
                $('.rating-outof').text( "("+rating_val+"/5)" );
            });


               $('.dropdown-menu').on('click touchstart tap', function(e){
                    //The event won't be propagated to the document NODE and 
                    // therefore events delegated to document won't be fired
                    e.stopPropagation();
                });



               $('.dropdown-menu .close').click(function(){
                   $(".dropdown-menu").dropdown('toggle');
                   return false;
               });
               $('.back-button').click(function(){
                $('.conform-wrapper').slideUp();
                $('.reserve-tutee-wrapper').slideDown();
            });
               $('.reserve-tutee-wrapper .tutee-primary-btn').click(function(){
                $('.conform-wrapper').slideDown();
                $('.reserve-tutee-wrapper').slideUp();
            });

            if($('.selectpicker').length){   
                $('.selectpicker').selectpicker();
            }

               /*tooltip for find tutor page*/
               //$('[data-toggle="tooltip"]').tooltip();

               $(window).on('resize',function(){
                 $("[data-toggle = 'tooltip']").tooltip('hide'); 
             });
            fixedHeader();
            if($('#PaymentTab').length > 0){
                $('#PaymentTab').tabCollapse();    
            }
            if($('.landing-page-main').length > 0){


                $('.landing-page-main .navbar-nav li a[href^="#"] , .landing-page-main .about-us-content-wrapper .contact-btn-wrapper').click(function(event){
                    event.preventDefault();
                    var target = this.hash,
                    $target = $(target);
                    $('.landing-page-main #mainnav').collapse('hide');
                    var heightGet = $('.landing-page-main .logo').height();

                    $('html, body').stop().animate({
                        'scrollTop': $target.offset().top - heightGet
                    }, 900, 'swing', function () {
                       // window.location.hash = target;

                   });
                    $('.landing-page-main #mainnav').collapse('hide');
                });

                window.addEventListener('mousewheel', function(event){
                    $('#menu-navigation li').removeClass('active');
                    var about = document.getElementById('aboutUs').getBoundingClientRect();
                    var howItWorks = document.getElementById('howItWork').getBoundingClientRect();
                    var ContactUs = document.getElementById('contactUS').getBoundingClientRect();

                    if(ContactUs.top <= $('.landing-page-main .logo').height() )
                    {
                        $('#menu-navigation li.contact').first().addClass('active');
                    }
                    else if(howItWorks.top <= $('.landing-page-main .header-landing-page').height() )
                    {
                        $('#menu-navigation li.howitwork').first().addClass('active');
                    }
                    else if(about.top <= $('.landing-page-main .header-landing-page').height() )
                    {
                        $('#menu-navigation li.about').first().addClass('active');
                    }
                }, false);

            }

            $(window).on('load resize',function(){

            $(window).scroll(fixedHeader);
            $(window).on("load resize",function(e){
                if($(window).height()<= 640){

                    $('.landing-input input').focus(function(){
                        $('body').addClass('inputfocus');

                    });
                    $('.landing-input input').blur(function(){
                        $('body').removeClass('inputfocus');
                    });

                }
            });    
            if ($(window).width() != windowWidth) {

                    windowWidth = $(window).width();
                    $('.banner-section').height($(window).height());
                    
                    $('.left-aside-dashboard').mouseenter(function(){
                        if($(window).width() >= 1201){
                            $('body').addClass('left-side-expand');
                        }
                    }).mouseleave(function(){
                        if($(window).width() >= 1201){
                            $('body').removeClass('left-side-expand');

                        }
                    });

                }
                
                
            });

                if($('body').hasClass('left-side-expand')){
                    $('body').removeClass('left-side-expand');    
                } 
        }

        
        $('body').on('click','#tutee_session_request',function(){
           loadTuteeSessionRequest();
        })

        function loadTuteeSessionRequest(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutee-session-request',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         history.pushState(null, null,'/tutee-session-request');
                          
                       
                    }
                    
                }
            });
        }

        $('body').on('click','#tutee_free_session',function(){
           loadTuteeFreeSession();
        })

        function loadTuteeFreeSession(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/free-session',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         history.pushState(null, null,'/free-session');
                    }
                    
                }
            });
        }


         $('body').on('click','#settings',function(){
           loadSettings();
        })

        function loadSettings(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/change-password',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         history.pushState(null, null,'/change-password');
                    }
                    
                }
            });
        }

        $('body').on('click','#change_pass_submit',function(){
            $.ajax({
                type: 'POST',
                url: baseUrl + '/password/change',
                data : $( "#change_password_form" ).serialize(),
                success: function(data) {
                    $('.has-error-text').hide();
                     $('.glyphicon-warning-sign').hide();
                     $('.form-group').removeClass(" has-error has-feedback ");
                     $('form').removeClass("has-feedback");
                    var data = $.parseJSON(data);  
                    var status = data.status;
                    if(status == "success"){
                            $('#Msg').hide();
                            $('#Msg').show();
                            $('#Msg').empty();
                            $('#Msg').removeClass("alert alert-danger");
                            $('#Msg').addClass("alert alert-success").append(data.msg);
                            $('#oldpsd').val('');
                            $('#newpsd').val('');
                            $('#confirmpsd').val('');
                      }else{
                            $('#Msg').hide();
                            $('#Msg').show();
                            $('#Msg').empty();
                             $('#Msg').removeClass("alert alert-success");
                            $('#Msg').addClass("alert alert-danger").append(data.msg);
                            //return false;
                      }
                        
                },
                error: function(data){
                   
                     $('.has-error-text').hide();
                    $('.glyphicon-warning-sign').hide();
                    $('.form-group').removeClass(" has-error has-feedback ");
                    $('form').removeClass("has-feedback");


                    var errors = data.responseJSON;


                    $.each(errors, function(index, value) {
                        $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
                        $('.'+index+"_error").show();
                        $('.'+index+"_error_value").html(value);
                       //$(index+"_error").html(value);
                    });
                    return false;
                    
                  }
            });
        })
    
        $('body').on('click','#tutee_my_preference',function(){
           loadTuteeMypreference();
        })

        function loadTuteeMypreference(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutee-preference',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         history.pushState(null, null,'/tutee-preference');
                    }
                    
                }
            });
        }

        $('body').on('click','#tutee_my_sessions',function(){
           loadTuteeMysessions();
        })

        function loadTuteeMysessions(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutee-sessions',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);

                         
                         loadSessionCalander(data.currentDate,data.finalSessionData);
                         loadCommonscript();
                         $('.fc-day-number').prop('disabled',true); 
                         history.pushState(null, null,'/tutee-sessions');
                    }
                    
                }
            });
        }

        $('body').on('click','#tutee_find_tutor',function(){
           loadFindTutor();
        })

        function loadFindTutor(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/find-a-tutor',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         AjaxData(1); 
                         loadCommonscript();
                         var reservation_serach = $('#search_reservation').val();
                        if(reservation_serach != ""){
                            $("#reservation").val(reservation_serach);  
                        }else{
                          $("#reservation").val('');
                        }
                         history.pushState(null, null,'/find-a-tutor');
                    }
                    
                }
            });
        }

        function loadTutorDetails(id){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutorDetail/'+id,
                  //data: {id:id,_token: $('#csrf_token_header').val()},
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                        loadCommonscript();
                         var gb;
                          var subject_id;
                          var topic_id;  
                          $('#curriculum_id').val('');
                          $('.navbar-toggle').hide();
                          $("#prev").hide(function()
                          {
                           });

                          var Total = $('#total_reviews').val();
                          if(Total <= 10)
                          {
                            $("#next").hide();
                          }

                           var strVale = $('#enabledDates').val();
                          var date_arr = strVale.split('","');
                          $('#calender-inline').datetimepicker({
                               inline: true,
                               format: 'DD/MM/YYYY',
                               enabledDates : date_arr,
                               minDate: new Date()
                            //   defaultDate: '08/06/2017',

                          });
                        history.pushState(null, null,'/tutorDetail/'+id);
                    }
                    
                }
            });
        }

        $('body').on('click','#tutee_profile',function(){
           loadProfile();
        })

        function loadProfile(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/profile',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         if($('body').hasClass('left-side-expand')){
                                $('body').removeClass('left-side-expand');    
                            }
                         history.pushState(null, null,'/profile');
                    }
                    
                }
            });
        }

         $('body').on('click','#tutee_profile_edit',function(){
           loadEditProfile();
        })

        function loadEditProfile(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/profile/edit',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         if($('body').hasClass('left-side-expand')){
                                $('body').removeClass('left-side-expand');    
                            } 
                            var qualificationStatus = $('#qualification_id option:selected').val();
                            if(qualificationStatus == 1 || qualificationStatus == 2 || qualificationStatus == 3)
                            {
                                $('.user_curriculum_div').show();
                                $('.user_educational_curriculum').attr('required','required');
                                $('.user_subject_name_div').hide();
                            }
                            else{
                                $('.user_curriculum_div').hide();
                                $('.user_educational_curriculum').removeAttr('required');
                                $('#curriculum_id').val('').trigger('change');
                                $('.user_subject_name_div').show();
                            }

                            $('.datepicker').daterangepicker({
                                "singleDatePicker": true,
                                showDropdowns: true,
                                maxDate: new Date(),
                            });

                            /*$('body').on('change','.dynamic_dropdown',function(){alert('dd');
                                trrigerChangeEdit2();
                            })*/

                            getCurriculumHierarchy("curriculum_id");
                            trrigerChangeEdit2();
                           
                            var level_trriger_edit2 = true;
                            var grade_trriger_edit2 = true;
                            var education_trriger_edit2 = true;
                            var subject_trriger_edit2 = true;
                            var program_trriger_edit2 = true;
                            var topic_trriger_edit2 = true;


                            function trrigerChangeEdit2(){
                                setTimeout(function(){

                                    var curriculum = $('#curriculum_id').length;
                                    var edit_eductional_systems = $('#eductional_systems_id').length;
                                    var edit_levels = $('#level_id').length;
                                    var edit_program = $('#program_id').length;
                                    var edit_grade = $('#grade_id').length;
                                    var edit_subject = $('#subject_id').length;
                                    var edit_topic = $('#topic_id').length;

                                    var selected_education_edit = $('#edit_education_id').val();
                                    var selected_level_edit = $('#edit_level_id').val();
                                    var selected_program_edit = $('#edit_program_id').val();
                                    var selected_grade_edit = $('#edit_grade_id').val();
                                    var selected_subject_edit = $('#edit_subject_id').val();
                                    var selected_topic_edit = $('#edit_topic_id').val();

                                    if(selected_education_edit != 0 && selected_education_edit !="" && selected_education_edit != "null" && edit_eductional_systems > 0 && education_trriger_edit2 == true){
                                        $('#eductional_systems_id').val(selected_education_edit).trigger("change");
                                        education_trriger_edit2 = false; trrigerChangeEdit2();
                                    }

                                    if(selected_level_edit != 0 && selected_level_edit != "null" && edit_levels > 0 && selected_level_edit != "" && level_trriger_edit2 == true){
                                        $('#level_id').val(selected_level_edit).trigger("change");
                                        level_trriger_edit2 = false; trrigerChangeEdit2();

                                    }

                                    if(selected_program_edit != 0 && selected_program_edit !="" && selected_program_edit != "null" && edit_program > 0 && program_trriger_edit2 == true){
                                        $('#program_id').val(selected_program_edit).trigger("change");
                                        program_trriger_edit2 = false; trrigerChangeEdit2();
                                    }

                                    if(selected_grade_edit != 0 && selected_grade_edit !="" && selected_grade_edit != "null" && edit_grade > 0 && grade_trriger_edit2 == true){
                                        $('#grade_id').val(selected_grade_edit).trigger("change");
                                        grade_trriger_edit2 = false; trrigerChangeEdit2();
                                    }

                                    if(selected_subject_edit != 0 && selected_subject_edit !="" && selected_subject_edit != "null" && edit_subject > 0 && subject_trriger_edit2 == true){
                                        $('#subject_id').val(selected_subject_edit).trigger("change");
                                        subject_trriger_edit2 = false; trrigerChangeEdit2();
                                    }

                                    if(selected_topic_edit != 0 && selected_topic_edit !="" && selected_topic_edit != "null" && edit_topic > 0 && topic_trriger_edit2 == true){
                                        $('#topic_id').val(selected_topic_edit).trigger("change");
                                        topic_trriger_edit2 = false; trrigerChangeEdit2();
                                    }
                                }, 350);
                            }
   
                         history.pushState(null, null,'/profile/edit');
                    }
                    
                }
            });
        }

           $('body').on('submit','#edit_profile_form',function(ev){
              ev.preventDefault();
                 $.ajax({
                    url: baseUrl + '/profile/update',
                    type: 'POST',
                    data: $( "#edit_profile_form" ).serialize(),
                    success: function(data){
                            var data = $.parseJSON(data);
                            loadProfile();
                            $('#Msg').hide();
                            $('#Msg').show();
                            $('#Msg').empty();
                            $('#Msg').addClass("alert alert-success").append('Profile has been update successfully');

                          },
                          error: function(data){
                           $('.has-error-text').hide();
                           $('.form-group').removeClass(" has-error has-feedback ");
                           $('form').removeClass("has-feedback");
                           
                            var errors = data.responseJSON;
                            $.each(errors, function(index, value) {
                              $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
                              $('.'+index+"_error").show();
                              $('.'+index+"_error_value").html(value);
                               //$(index+"_error").html(value);
                             });
                            $('html, body').animate({
                                scrollTop: ($('.my-profile-section').offset().top)
                            },500); 
                          }
                        });

            })

           $('body').on('click','#tutee_my_payment',function(){
               loadTuteePayment();
            })

            function loadTuteePayment(){
                 $.ajax({
                    type: 'GET',
                    url: baseUrl + '/tutee/payment',
                    success: function(data) {
                        var status = data.status;
                        if(status == "success"){
                          
                            $('.mid-section').html(data.html);
                             loadCommonscript();
                             if($('body').hasClass('left-side-expand')){
                                    $('body').removeClass('left-side-expand');    
                                }
                                $.session.clear();
                                   $(function() {

                                  $('input[name="datefilter"]').daterangepicker({
                                      autoUpdateInput: false,
                                      locale: {
                                          cancelLabel: 'Clear'
                                      }
                                  });

                                  $('input[name="datefilter"]').daterangepicker();
                                  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                                    var startDate = picker.startDate.format('YYYY-MM-DD');
                                    var endDate = picker.endDate.format('YYYY-MM-DD');
                                    $.session.set('startDate', startDate);
                                    $.session.set('endDate', endDate);
                                    //showModalForm(startDate, endDate);
                                    tuteePaymentPagination();
                                  });
                                  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                                    $('#payment-date').val('');
                                    $.session.clear();
                                   
                                    var startDate = picker.startDate.format('YYYY-MM-DD');
                                    var endDate = null;
                                    //showModalForm(startDate, endDate);
                                    tuteePaymentPagination();
                                  });
                                });
                             history.pushState(null, null,'/tutee/payment');
                        }
                        
                    }
                });
            }


      /*Tutor dashboard start*/
      $('body').on('click','#tutor_dashboard_link',function(){
           loadTutorDashboard();
        })

        function loadTutorDashboard(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutor-dashboard',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                       // var record_for_column_chart = '';
                          var record= data.user;
                         var record_for_column_chart = JSON.parse(data.columnChartArray);
                          google.charts.load('current', {packages: ['corechart', 'bar']});
                          google.charts.setOnLoadCallback(function(){ drawChart(record) });
                          google.charts.setOnLoadCallback(function(){ drawChart2(record_for_column_chart) });
                           var user_id = $('#logged_in_user_id').val();
                           checkTutorJoinSession(user_id);
                          //$('.notification_count').text(data.notificationCount); 
                          history.pushState(null, null,'/tutor-dashboard');
                          
                    }
                    
                }
            });
        }


         function refreshNotifications(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/get_notifications',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                        $('.notification_main_li').html(data.html);
                        $('.notification_count').text(data.notificationCount);
                        //  notification_fire = true;
                    }
                    
                }
            });
        }

         function drawChart2(record_for_column_chart) {
             var record=record_for_column_chart; 
            var data = google.visualization.arrayToDataTable(record);
          
            var options = {
              chart: {
                title: 'Ratings',

                //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
              },
              bars: 'vertical',
              //vAxis: {format: 'decimal'},
             // height: 400,
              legend: {position: 'top'},
              colors: ['#f79237', '#228B22'],
                width: '100%',
                height: '100%',
              
                chartArea: {
                  /*left: "3%",
                  top: "3%",*/
                  height: "85%",
                  width: "85%",
                  
              },
            };

           // var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
           var chart =  new google.visualization.ColumnChart(document.getElementById('columnchart_material'));

            chart.draw(data, options);
      }
      /*Tutor dashboard end*/      

      /*tutor_my_availability*/
      $('body').on('click','#tutor_my_availability',function(){
           loadTutorMyavailability();
        })

        function loadTutorMyavailability(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutor-availablity',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         $('#from_date_specific').daterangepicker({
                              "singleDatePicker": true,
                              showDropdowns: true,
                              minDate:new Date(),
                               maxDate: $('#admin_end_range_date_for_jquery').val(),
                          });
                           
                          history.pushState(null, null,'/tutor-availablity');
                          
                    }
                    
                }
            });
        }
      /*tutor_my_availability*/

      /*Tutor my sessions*/

        $('body').on('click','#tutor_my_sessions',function(){
           loadTutorMysessions();
        })

        function loadTutorMysessions(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutor-sessions',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);

                         
                         loadSessionCalanderForTutor(data.currentDate,data.finalSessionData);
                         loadCommonscript();
                         $('.fc-day-number').prop('disabled',true); 
                         history.pushState(null, null,'/tutor-sessions');
                    }
                    
                }
            });
        }
      /*Tutor my sessions*/
       /*Tutor session request*/
       $('body').on('click','#tutor_session_request',function(){
           loadTutorSessionRequest();
        })

        function loadTutorSessionRequest(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutor-session-request',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         history.pushState(null, null,'/tutor-session-request');
                          
                       
                    }
                    
                }
            });
        }
     /*Tutor session request*/

     /*Tutor My payment start*/
     $('body').on('click','#tutor_my_payment',function(){
               loadTutorPayment();
     })

        function loadTutorPayment(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutor/payment',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         if($('body').hasClass('left-side-expand')){
                                $('body').removeClass('left-side-expand');    
                            }
                            $.session.clear();
                               $(function() {
                                  $('input[name="datefilter"]').daterangepicker({
                                      autoUpdateInput: false,
                                      locale: {
                                          cancelLabel: 'Clear'
                                      }
                                  });

                                  $('input[name="datefilter"]').daterangepicker();
                                  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                                    var startDate = picker.startDate.format('YYYY-MM-DD');
                                    var endDate = picker.endDate.format('YYYY-MM-DD');
                                    $.session.set('startDate', startDate);
                                    $.session.set('endDate', endDate);
                                    //showModalForm(startDate, endDate);
                                     tutorPaymentPagination(1);
                                  });
                                   $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                                    $('#payment-date').val('');
                                    $.session.clear();
                                    var startDate = picker.startDate.format('YYYY-MM-DD');
                                    var endDate = null;
                                   // showModalForm(startDate, endDate);
                                    tutorPaymentPagination(1);
                                  });
                                });

                         history.pushState(null, null,'/tutor/payment');
                    }
                    
                }
            });
        }
     /*Tutor my payment end*/

     /*Tutor My test start*/
     $('body').on('click','#tutor_my_test',function(){
               loadTutorMytest();
     })

        function loadTutorMytest(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/tutor/mytest',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                         loadCommonscript();
                         if($('body').hasClass('left-side-expand')){
                                $('body').removeClass('left-side-expand');    
                            }
                            $.session.clear();
                                $(function() {
              
                                    $('input[name="datefilter"]').daterangepicker({
                                        autoUpdateInput: false,
                                        locale: {
                                            cancelLabel: 'Clear'
                                        }
                                    });

                                    $('input[name="datefilter"]').daterangepicker();
                                    $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                                      var startDate = picker.startDate.format('YYYY-MM-DD');
                                      var endDate = picker.endDate.format('YYYY-MM-DD');
                                      $.session.set('startDate', startDate);
                                      $.session.set('endDate', endDate);
                                      showModalFormMytest(startDate, endDate);
                                    });
                                    $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                                    
                                      $.session.clear();
                                      $('#payment-date').val('');
                                    
                                      var startDate = null;
                                      var endDate = null;
                                      showModalFormMytest(startDate, endDate);
                                      //showModalFormCancelButton(startDate, endDate);
                                    });
                                  });
                                $('#passTestMsg').hide();
                         history.pushState(null, null,'/tutor/mytest');
                    }
                    
                }
            });
        }
     /*Tutor my test end*/

     /*Notification page start*/

    function loadMoreNotifications(button) 
    {   

        var token = $('#csrf_token_header').val();

        var hidden_limit = $('#hidden_limit').val();
        var totalNext = $('#totalNext').val();
        var totalPrev = $('#totalPrev').val();

        if(button == 'p')
        {  
            
          $('#hidden_limit').val(parseInt(hidden_limit) - 10);
          $('#totalPrev').val(parseInt(totalPrev) - 10);
          $('#totalNext').val(parseInt(totalNext) + 10);
        }
        else
        { 
          $('#hidden_limit').val(parseInt(hidden_limit) + 10); 
          $('#totalNext').val(parseInt(totalNext) - 10);
          $('#totalPrev').val(parseInt(totalPrev) + 10);
        }

        $.ajax({ 
        url: baseUrl + '/getNotificationList',
        type: 'POST',
        dataType : 'html',
        data: $("#loadMoreNotification").serialize()+"&_token="+token,
        success: function(data)
        {  
           $('#notificationslistDiv').html(data);
           var totalNext = $('#totalNext').val();
           var totalPrev = $('#totalPrev').val();

        if(totalPrev >= 0)
        $(".prev").show();

        if(button == 'p')
        {  
            if(totalPrev <= 0)
            { 
              $(".prev").addClass('hidden');
              $(".next").removeClass('hidden');
            }

            if(data == "" && button != "p")
            { 
              $(".prev").addClass('hidden');
              $(".next").removeClass('hidden');
            }
        }
        
        else
        {   
            if(totalNext <= 0)
            { 
              $(".next").addClass('hidden');
              $(".prev").removeClass('hidden');
            }

            if(data == "" && button != "n")
            { 
              $(".next").addClass('hidden');
              $(".prev").removeClass('hidden');
            }
        }
    }

        });

    }

    function loadNotificationList(){
        var token = $('#csrf_token_header').val();
        
        $.ajax({ 
            url: baseUrl + '/getNotificationList',
            type: 'POST',
            dataType : 'html',
            data: $("#loadMoreNotification").serialize()+"&_token="+token,
            success: function(data)
            {   
                $('#notificationslistDiv').html(data);
            }

        });
        
        var Total = $('#total_notifications').val();
        if(Total <= 10)
        { 
         $(".next").removeClass('hidden');
        }
    }

    function loadNotificationPage(){
             $.ajax({
                type: 'GET',
                url: baseUrl + '/notification',
                success: function(data) {
                    var status = data.status;
                    if(status == "success"){
                      
                        $('.mid-section').html(data.html);
                        loadNotificationList();
                         loadCommonscript();
                         history.pushState(null, null,'/notification');
                          
                       
                    }
                    
                }
            });
        }
    /*Notification page end*/    
/*Left side bar ajax page reload changes end*/