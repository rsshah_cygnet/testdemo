// JavaScript Document
$ = jQuery.noConflict();

$(document).ready(function(){
  
    var token = $('#csrf_token_header').val();
    window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
      appId      : '1422143877853101',
      status     : true,
      xfbml      : true
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

 

  $('.rating-input').unbind('mouseenter mouseleave click');
         var user_id = $('#logged_in_user_id').val();
         setInterval('checkTuteeJoinSession('+user_id+')',60000);
    });

$('body').on('change','#month_piechart,#year_piechart',function(){
    month_piechart();
})

$('body').on('change','#session_modal_div .rating',function(){
    var rating_val = $('#rating-empty-clearable-tutee').val();
    $('#rating-outof').text( "("+rating_val+"/5)" );
});

$('body').on('click','.notification_main_li',function(){
        if($('.notification_main_li').hasClass('open')){
            $('.notification_main_li').removeClass('open');
        }else{
            $('.notification_main_li').addClass('open');
        }
    })


function checkTuteeJoinSession(id) {
           $.ajax({
            type: 'POST',
            url: baseUrl + '/checkTuteeJoinSession',
            data: {
                id: id,
                _token: $('#csrf_token_header').val()
            },
            success: function(data) {
                data = $.parseJSON(data);
                $('.tutee-join-session').addClass('hidden');
                $.each(data, function(key, element) {
                    $('.tutee-join-session-' + element.id).removeClass('hidden');
                });
            }
        });
    }

function month_piechart() {
      var month = $('#month_piechart').val();
      var year = $('#year_piechart').val();
      if(year == ""){
         $('#piechart_error').html('Please select year');
         $('#piechart_error').show();
          return false;
      }else{
        $('#piechart_error').hide();
      }
      if(month == ""){
       $('#piechart_error').html('Please select month');
       $('#piechart_error').show();
          return false;
      }else{
        $('#piechart_error').hide();
      }
      $.ajax({
          type: 'POST',
          url: baseUrl + '/tutee-piechart',
          data: {month: month,year: year,_token: $('#csrf_token_header').val()},
          success: function(data){
              var status = data.status;
              if(status == "success"){
                  $('#pie_chart_portion').html(data.html);
              }else{
                  var error_html = "<center><p>There are no any session request found for this time interval</p></center>";
                  $('#pie_chart_portion').html(error_html);
              }

          }
      });
  }

    /*Session request section*/
    function handleModalBoxEvent(data)
    {
      jQuery( ".modal-body" ).html(data);
      $("#myModal").modal('show');
    }

    $('body').on( 'click', '#decline-request', function()
    {
      var id = $(this).attr('id2');
      showModalForm(id);
    });


    function showModalForm(id)
     {
      //var url = "{!! route('decline.tutor.session.request.model', ['id'=> '-1'])!!}";
      var url = "tutee-session-request/decline/model/"+id;
      //url = url.replace('-1',id);

      jQuery.ajax(
      {
        type:   "GET",
        url:    url,
        async:  true,
        success: function(data)
        {
          handleModalBoxEvent(data);
        }
      });

    }

    $('body').on('click','#cancel_request_tutee',function(){
        var session_id = $('#cancelled_session_id').val();
        var url = "tutee-session-request-decline/"+session_id;
        $.ajax({
          type: 'GET',
          url: url,
          success: function(data){
               var data = $.parseJSON(data);  
              var status = data.status;
              //$('.hide_modal').click();

              if(status == "success"){
                    $('#Msg').hide();
                    $('#Msg').show();
                    $('#Msg').empty();
                    $('#Msg').addClass("alert alert-success").append(data.msg);
                    //return false;
              }else{
                    $('#Msg').hide();
                    $('#Msg').show();
                    $('#Msg').empty();
                    $('#Msg').addClass("alert alert-danger").append(data.msg);
                    //return false;
              }
              loadTuteeSessionRequest();
              $('.modal-backdrop').remove();
          }
      });
    })
    /*Session request section end*/

    /*My preference start*/
      $('body').on('click','.tutee_curriculumn_preference',function(){
            sessionPreference();
      })
           

        function sessionPreference(){
          //$('#tutee_session_preference').on('submit', function(ev){
          // ev.preventDefault();
          var lastValue = $('#custom_dropdowns_payment div select:last-child').attr('id');
          var type = '';
        //var current_dropdown_id = $this.attr('id');
        var dropdown_array = [];
        dropdownIDsArray = $.map($("div[id*=custom_dropdowns_payment]").find("select"), function (item, index) {
          var next = $(item).attr("id");
          dropdown_array.push(next);
                //alert(next);
              });

        var string ='';
        if(typeof dropdown_array != 'undefined' && !jQuery.isEmptyObject(dropdown_array)){
          count = dropdown_array.length;
          initialize = 1;
          $.each(dropdown_array, function(key, value) {
            if(initialize == count)
              delimeter = "";
            else
              delimeter = " | ";
                       // if(value != "subject_id_payment" &&  value != "topic_id_payment"){
                        string += $('#'+value+' option:selected').attr("data-name")+delimeter;
                        initialize++;
                       // }
                     });
        }

        var grade_type = $('#grade_id_payment option:selected').attr('data-type');
        var subject_id_payment = $('#subject_id_payment option:selected').attr('data-name');
        var topic_id_payment = $('#topic_id_payment option:selected').attr('data-name');
        var added_curriculum_number = $('#added_curriculum_number').val();

        if(grade_type == 0)
          type = "lower";
        else
          type = "higher";

        if(!topic_id_payment)
          topic_id_payment = 'N/A';

        hidden_user_id = $('#hidden_user_id').val();
        $.ajax({
          url: baseUrl + '/tutee-session-preference',
          type: 'POST',
          data: $( "#tutee_session_preference" ).serialize()+"&grade_type="+grade_type,
          success: function(data){
            // Success...
            if(typeof data.topic_error != 'undefined')
            {
              $('.topic_id_payment_error').html(data.topic_error);
              $('.topic_id_payment_error').show();
              return false;
            }
            else
            {
              $('.topic_id_payment_error').hide();
            }
            if(typeof data.subject_id_error != 'undefined')
            {
              $('.subject_id_payment_error').html(data.subject_id_error);
              $('.subject_id_payment_error').show();
              return false;
            }
            else
            {
              $('.subject_id_payment_error').hide();
            }
            if(typeof data.curriculumn_error != 'undefined')
            {
              $('.curriculumn_added_payment_error').html(data.curriculumn_error);
              $('.curriculumn_added_payment_error').show();
              return false;
            }
            else
            {
              $('.curriculumn_added_payment_error').hide();
            }

            $('#added_curriculum_number').val(parseInt(added_curriculum_number) + 1);
            $('.total-curriculum').html('Total '+ (parseInt(added_curriculum_number) + 1));


            var append_html = '<div class="curriculum-list_'+data.id+' clearfix"><div class="curriculum-record-wrapper"><div class="curriculum-record"><dl class="dl-horizontal"><dt>Curriculum</dt><b>'+string+'</b></div><div class="curriculum-record"><dl class="dl-horizontal dl-inline-block"><dt>Subject</dt><dd><b>'+subject_id_payment+' </b></dd></dl><dl class="dl-horizontal dl-inline-block"><dt> Topic</dt><dd><b>'+topic_id_payment+'</b></dd></dl></div></div><div class="curriculum-record-delete"><a href="javascript:void(0);"><img src="images/delete-button.png" onclick = "deleteTuteePreferenceCurriculum('+"'"+data.id+"'"+')" alt=""></a></div></div>';


            $('.curriculum-list-wrapper').append(append_html);

    //update added number of curriculum

    },
    error: function(data){
     $('.has-error-text').hide();
     $('.glyphicon-warning-sign').hide();
     $('.form-group').removeClass(" has-error has-feedback ");
     $('form').removeClass("has-feedback");

            // Error...
            var errors = data.responseJSON;

            $.each(errors, function(index, value) {
                //$( '#'+index+"_payment").closest('.form-group').addClass(" has-error has-feedback ");
                $('.'+index+"_error").show();
                $('.'+index+"_error_value").html(value);
               //$(index+"_error").html(value);
             });
          }
        });
      //});
  }
        //end session add curriculumn

      

     function submit_form()
     {
       $(window).scrollTop(0);
       //$('.tuteme_preference_save_hidden').click();
       tuteePreference();
       
     }


     function deleteTuteePreferenceCurriculum(id)
     {
      $.ajax({
        url: baseUrl + '/tutee-delete-preference',
        type: 'POST',
        data: "id="+id+"&_token="+$('#csrf_token_header').val(),
        success: function(data){
            // Success...
            $('.curriculum-list_'+id).remove();
            var added_curriculum_number = $('#added_curriculum_number').val();
            $('#added_curriculum_number').val(parseInt(added_curriculum_number) - 1);
            $('.total-curriculum').html('Total '+ (parseInt(added_curriculum_number) - 1));
            var string = '';

          },
          error: function(data){

          }
        });
}

     function tuteePreference(){
              //$('#tutee_preference').on('submit', function(ev){
              //ev.preventDefault();

              $.ajax({
                    url: baseUrl + '/tutee-preference',
                    type: 'POST',
                    data: $( "#tutee_preference" ).serialize(),
                    success: function(data){

                            // Success...
                            // Loop through all values in outputfromserver
                            var data = $.parseJSON(data);
                            
                            $('#hidden_id').val(data.id);
                            $('.has-error-text').hide();
                            $('.glyphicon-warning-sign').hide();
                            $('.form-group').removeClass(" has-error has-feedback ");
                            $('form').removeClass("has-feedback");
                             // location.reload();
                            $('#Msg').hide();
                            $('#Msg').show();
                            $('#Msg').empty();
                            $('#Msg').addClass("alert alert-success").append(data.msg);


                          },
                          error: function(data){
                           $('.has-error-text').hide();
                           $('.form-group').removeClass(" has-error has-feedback ");
                           $('form').removeClass("has-feedback");

                            // Error...
                            var errors = data.responseJSON;


                            $.each(errors, function(index, value) {
                              $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
                              $('.'+index+"_error").show();
                              $('.'+index+"_error_value").html(value);
                               //$(index+"_error").html(value);
                             });
                          }
                        });

            }
    /*My preference end*/

    /*My sessions page start*/
    
    function loadSessionCalander(currentDate,finalSessionData){

        var html='';
           html+='<div class="color-legend"><ul class="list-inline box-legend-calender"><li><span class="small-box upcomming-session"></span>Upcoming Session</li><li><span class="small-box successful-session"></span>Successful Session</li><li><span class="small-box cancel-session-tutee"></span>Session cancelled by Tutee</li><li><span class="small-box cancel-session-tutor"></span>Session cancelled by Tutor</li></ul></div>';

            var date = new Date();
            var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();

        $('#calendar').fullCalendar({
            eventAfterAllRender: function (event, element) {
                if($('.fc-header-toolbar').parent().find('.color-legend').length <= 0){
                    $('.fc-header-toolbar').after(html)
                }
            },
                header: {
                    left: '',
                    center: 'prev,title,next,today',
                    right: ''
                },
                defaultDate: currentDate,
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                eventClick: function(calEvent, jsEvent, view) {
                  var status = calEvent.status;
                  var id = calEvent.id;
                  showSessionPopup(id,status);
                  //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                },
                events : JSON.parse(finalSessionData)
        });
         
    }

    function showSessionPopup(id,status) {

              $.ajax({
                  type: 'POST',
                  url: baseUrl + '/get-session-data',
                  data: {status: status,id:id,_token: $('#csrf_token_header').val()},
                  success: function(data){
                      var status = data.status;
                      if(status == 1){
                         $('#session_modal_div').html(data.html);
                         $('.rating').rating();
                        $('#start_success .rating-input').unbind('mouseenter mouseleave click');
                         $('#success_session_modal').modal('show');
                            
                       }else if(status == 2){
                         $('#session_modal_div').html(data.html);
                           $('.rating').rating();
                            $('#upcomming_session_modal').modal('show');
                            $('#start_success .rating-input').unbind('mouseenter mouseleave click');
                            var user_id = $('#logged_in_user_id').val();
                       checkTuteeJoinSession(user_id);

                       }else if(status == 3){
                         $('#session_modal_div').html(data.html);
                            $('#cancelled_by_tutor_session_modal').modal('show');
                       }else if(status == 4){
                            $('#session_modal_div').html(data.html);
                            $('#cancelled_by_tutee_session_modal').modal('show');
                       }
                        FB.XFBML.parse(document.getElementById('shareButton'));
                      $('#start_success .rating-input').unbind('mouseenter mouseleave click');
                       $('#rating_tutor_modal').on('shown.bs.modal', function() {
                             $('.rating-input').bind('mouseenter mouseleave click');
                          })  
                      return false;
                  }
              });
          }


          function FBShareOp(){
              var product_name   =  'Tuteme';
              var description    =  $('#share_description').val();
              var share_image    =  'http://192.192.8.51:8893/images/logo-design.png';
              var share_url      =  'http://192.192.8.51:8893/';
              var share_capt     =  'https://tuteme.com';
                FB.ui({
                    method: 'feed',
                    name: product_name,
                    link: share_url,
                    picture: share_image,
                    caption: share_capt,
                    description: description

                }, function(response) {
                    if(response && response.post_id){
                      var session_id = $('#session_id').val();
                      $.ajax({
                              type: 'POST',
                              url: baseUrl + '/update_share_count',
                              data: {session_id:session_id,_token: $('#csrf_token_header').val()},
                              success: function(data){
                                $('#fb_share_tutee_btn').hide();
                                return true;  
                              }
                          });
                    }
                    else{}
                });
            }

      $(document).on( 'click', '#cancelSession', function()
      {
         var id = $('#session-id').val();
         $("#myModal").modal('show');
      });
      
      $(document).on( 'click', '#declineSession', function()
      { 
            var userId = $('#logged_in_user_id').val();
            var id = $('#session-id').val();
            var logged_as = $('#logged_in_as').val();
            if(logged_as == "tutor"){
              var role = '1';
            }else{
              var role = '2';
            }

            $.ajax({
                      type: 'POST',
                      url: '/tutee-cancel-session',
                      data: {id:id,role:role,userId:userId,_token: $('#csrf_token_header').val()},
                      success: function(data)
                      {
                         loadTuteeMysessions();
                          $('.modal-backdrop').remove();
                          $('.tutee-dashboard').removeClass('modal-open');
                      }
                  });
        }); 

        $(document ).on( 'click', '.rating_tutor', function()
        {
       
          $("#rating_tutor_modal").modal('show');
          $('.rating').rating();
          $('#RatingTutorForm').on('submit', function (e)
          {   
            var rating = $('#rating-empty-clearable-tutee').val();
            var id = $('#session_id').val();
            e.preventDefault();
            $.ajax({
                  type: 'POST',
                  url: '/tutor-rating',
                  data: {id:id,_token: $('#csrf_token_header').val(),rating:rating,review: $("#review").val()},
                  success: function(data)
                  {  
                    if(data.res == 201)
                    {
                      $('.error').html(data.msg);
                      $('.error').css('color','red');
                    }
                    else
                    {
                     alert(data);
                    loadTuteeMysessions();
                   $('.modal-backdrop').remove();
                     $('.tutee-dashboard').removeClass('modal-open');
                    }
                  }
              });
          });
        });   

        $(document ).on( 'click', '.report_tutee', function()
        {
          var id = $('#session_id').val();
          $("#report_tutee_modal").modal('show');
          $('#ReportTuteeForm').on('submit', function (e)
          {  e.preventDefault();
            $.ajax({
                  type: 'POST',
                  url: '/tutee-report',
                  data: {id:id,_token: $('#csrf_token_header').val(),report: $("#reporttext").val()},
                  success: function(data)
                  {
                    if(data.res == 201)
                    {
                      $('.error').html(data.msg);
                    }
                    else
                    {
                     alert(data);
                     loadTuteeMysessions();
                     $('.modal-backdrop').remove();
                     $('.tutee-dashboard').removeClass('modal-open');
                    }
                  }
              });
          });
        });


        $(document ).on( 'click', '.session_feedback', function()
        { 
          var id = $('#session_id').val();
          $("#session_feedback_modal").modal('show');
          $('#SessionFeedbackForm').on('submit', function (e)
          {  
            e.preventDefault();
            $.ajax({
                  type: 'POST',
                  url: '/tutee-session-feedback',
                  data: {id:id,_token: $('#csrf_token_header').val(),report: $("#feedbackText").val()},
                  success: function(data)
                  {
                    if(data.res == 201)
                    {
                      $('.error').html(data.msg);
                    }
                    else
                    {
                     alert(data);
                     loadTuteeMysessions();
                     $('.modal-backdrop').remove();
                     $('.tutee-dashboard').removeClass('modal-open');
                    }
                  }
              });
          });
        });
    /*My sessions page end*/

    /*Tutee my payment start*/
    function tuteePaymentPagination(page)
    {
       var tab1 = $('#tab1').attr('class');
         var tab2 = $('#tab2').attr('class');
         var tab3 = $('#tab3').attr('class');
         var filter = $('#filter').val();
      
      if(tab1 == 'active'){
        var type = 'tab1';
      }else if(tab2 == 'cancel-tutee active'){
        var type = 'tab2';
      }else if(tab3 == 'cancel-tutor active'){
        var type = 'tab3';
      }
      var startDate = $.session.get('startDate');
      var endDate = $.session.get('endDate');
        var token = $('#csrf_token_header').val();
        $.ajax({
          url: 'payment/ajax/' + type,
          type: 'POST',
          data: "&page="+page+"&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
          dataType: 'html',
        }).success(function (data) {
        data = $.parseJSON(data);
          jQuery("#home").html(data.successfulSessionHTML);
            jQuery("#menu1").html(data.cancelSessionByTuteeHTML);
            jQuery("#menu2").html(data.cancelSessionByTutorHTML);

            if($('#tab1').hasClass('active')){
              $('#home-collapse').html(data.successfulSessionHTML);  
            }
            if($('#tab2').hasClass('active')){
              $('#menu1-collapse').html(data.cancelSessionByTuteeHTML);
            }
            if($('#tab3').hasClass('active')){
              $('#menu2-collapse').html(data.cancelSessionByTutorHTML);
            }
            
        }).fail(function () {
          alert('Some error occured.');
        });
    }

    $(document).on('click', '#tab1', function (e) {
        var startDate = $.session.get('startDate');
        var endDate = $.session.get('endDate');
         var token = $('#csrf_token_header').val();
          $.ajax({
            url: 'payment/ajax/tab/tab2',
            type: 'POST',
            data:  "&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
            dataType: 'html',
          }).success(function (data) {
          data = $.parseJSON(data);
              jQuery("#home").html(data.successfulSessionHTML);
                    $("#tab1").addClass("active");
                    $("#tab2").removeClass("active");
                    $("#tab3").removeClass("active");   

                    $("#home").addClass("active in");        
                    $("#menu1").removeClass("active in");        
                    $("#menu2").removeClass("active in"); 

                    $('#home-collapse').addClass('in');   
                    $('#menu1-collapse').removeClass('in');
                    $('#menu2-collapse').removeClass('in');  
          }).fail(function () {
            alert('Tutor could not be loaded.');
          });
    });

  $(document).on('click', '#tab2', function (e) {
      var startDate = $.session.get('startDate');
      var endDate = $.session.get('endDate');
       var token = $('#csrf_token_header').val();
        $.ajax({
          url: 'payment/ajax/tab/tab2',
          type: 'POST',
          data:  "&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
          dataType: 'html',
        }).success(function (data) {
        data = $.parseJSON(data);
            jQuery("#menu1").html(data.cancelSessionByTuteeHTML);
               $("#tab2").addClass("active");
          $("#tab1").removeClass("active");
          $("#tab3").removeClass("active");    


          $("#home").removeClass("active in");        
          $("#menu1").addClass("active in");        
          $("#menu2").removeClass("active in");   

          $('#menu1-collapse').addClass('in');
          $('#menu2-collapse').removeClass('in');
          $('#home-collapse').removeClass('in');
        }).fail(function () {
          alert('Tutor could not be loaded.');
        });
    });

  $(document).on('click', '#tab3', function (e) {
    var startDate = $.session.get('startDate');
    var endDate = $.session.get('endDate');
     var token = $('#csrf_token_header').val();
      $.ajax({
        url: 'payment/ajax/tab/tab2',
        type: 'POST',
        data:  "&_token="+token+"&startDate="+startDate+"&endDate="+endDate,
        dataType: 'html',
      }).success(function (data) {
      data = $.parseJSON(data);
          jQuery("#menu2").html(data.cancelSessionByTutorHTML);
            $("#tab3").addClass("active");
        $("#tab1").removeClass("active");
        $("#tab2").removeClass("active");    

        $("#home").removeClass("active in");        
        $("#menu1").removeClass("active in");        
        $("#menu2").addClass("active in");

        $('#menu1-collapse').removeClass('in');
        $('#menu2-collapse').addClass('in');
        $('#home-collapse').removeClass('in');  
      }).fail(function () {
        alert('Tutor could not be loaded.');
      });
    });

    $('body').on('click','.js-tabcollapse-panel-heading',function(){
          var tab_attr = $(this).attr('href');
          if(tab_attr == '#home-collapse'){
              $('#tab1').click();
          }else if(tab_attr == '#menu1-collapse'){
              $('#tab2').click();
          }else if(tab_attr == '#menu2-collapse'){
              $('#tab3').click();
          } 
    })


     $(document).ready(function(){

        $('body').on('click','.custome_lang_dropdown',function(){
            if(!$('.bootstrap-select').hasClass('open')){
                $('.bootstrap-select').addClass('open');
            }
        })

        var tutee_payment = $('#is_tutee_payment').val();
        if(tutee_payment != undefined && tutee_payment == 'tutee_payment'){
              $.session.clear();
               $(function() {

              $('input[name="datefilter"]').daterangepicker({
                  autoUpdateInput: false,
                  locale: {
                      cancelLabel: 'Clear'
                  }
              });

              $('input[name="datefilter"]').daterangepicker();
              $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                var startDate = picker.startDate.format('YYYY-MM-DD');
                var endDate = picker.endDate.format('YYYY-MM-DD');
                $.session.set('startDate', startDate);
                $.session.set('endDate', endDate);
                //showModalForm(startDate, endDate);
                tuteePaymentPagination(1);
              });
              $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                $('#payment-date').val('');
                $.session.clear();
               
                var startDate = picker.startDate.format('YYYY-MM-DD');
                var endDate = null;
                //showModalForm(startDate, endDate);
                tuteePaymentPagination(1);
              });
            });        
        }    
        
        
        });
    /*Tutee my payment end*/