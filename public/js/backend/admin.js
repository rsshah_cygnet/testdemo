var Imperial = {
    /**
     * Domain
     *
     */
    Domain: {
        selectors: {
            domainSelection: jQuery(".accAj")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.domainSelection.select2();
        }
    },
    /**
     * Cmspage
     *
     */
    Cmspage: {
        selectors: {
            cmspageSelection: jQuery(".accAjCms")
        },
        init: function() {
            this.addHandlers();
            // Imperial.tinyMCE.init();
        },
        addHandlers: function() {
            this.selectors.cmspageSelection.select2();
        }
    },
    /**
     * Cmspage
     *
     */
    Levels: {
        selectors: {
            levelSelection: jQuery("#grade_id")
        },
        init: function() {
            this.addHandlers();
            // Imperial.tinyMCE.init();
        },
        addHandlers: function() {
            this.selectors.levelSelection.select2();
        }
    },
    /**
     * Programs
     *
     */
    Program: {
        selectors: {
            programSelection: jQuery("#grade_id")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.programSelection.select2();
        }
    },
    /**
     * Menus
     *
     */
    Menus: {
        selectors: {
            menuSelection: jQuery(".accAjMenu")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.menuSelection.select2();
        }
    },
    /**
     * BrandModel
     *
     */
    BrandModel: {
        selectors: {
            brandSelection: jQuery(".accAj")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.brandSelection.select2();
        }
    },
    /**
     * Campaign Managment
     *
     */
    Campaign: {
        selectors: {
            brandSelection: jQuery(".selectbrand"),
            modelSelection: jQuery(".selectmodel"),
            datepicker: jQuery(".datepicker"),
            domainSelection: jQuery(".selectdomain"),
            cmspageSelection: jQuery(".selectcms"),
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.brandSelection.select2();
            this.selectors.modelSelection.select2();
            this.selectors.domainSelection.select2();
            this.selectors.cmspageSelection.select2();
            this.selectors.datepicker.datepicker();
        }
    },
    /**
     * Special Managment
     *
     */
    SpecialManagement: {
        selectors: {
            yearSelection: jQuery(".selectyear"),
            brandSelection: jQuery(".selectbrand"),
            modelSelection: jQuery(".selectmodel"),
            typeSelection: jQuery(".selecttype"),
            domainSelection: jQuery(".selectdomain"),
            derivativeSelection: jQuery(".selectderivative"),
            datepicker: jQuery(".datepicker"),
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.yearSelection.select2();
            this.selectors.brandSelection.select2();
            this.selectors.modelSelection.select2();
            this.selectors.typeSelection.select2();
            this.selectors.domainSelection.select2();
            this.selectors.derivativeSelection.select2();
            this.selectors.datepicker.datepicker();
        }
    },
    /**
     * ProvinceRegionsModel
     *
     */
    ProvinceRegions: {
        selectors: {
            provinceRegionsSelection: jQuery(".accSd")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.provinceRegionsSelection.select2();
        }
    },
    /**
     * CmsPagesModel
     *
     */
    CmsPagesModel: {
        selectors: {
            cmsPageSelection: jQuery(".accSd,#grade_id")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.cmsPageSelection.select2();
        }
    },
    /**
     * Dealership
     *
     */
    Dealership: {
        selectors: {
            dealershipSelection: jQuery(".accAj")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.dealershipSelection.select2();
        }
    },
    /**
     * Blog
     *
     */
    Blog: {
        selectors: {
            tags: jQuery(".tags"),
            categories: jQuery(".categories"),
            toDisplay: jQuery(".toDisplay"),
            status: jQuery(".status"),
        },
        init: function() {
            this.addHandlers();
            Imperial.tinyMCE.init();
        },
        addHandlers: function() {
            this.selectors.tags.select2({
                tags: true,
            });
            this.selectors.categories.select2();
            this.selectors.toDisplay.select2();
            this.selectors.status.select2();
        }
    },
    /**
     * Support ticket
     *
     */
    SupportTicket: {
        selectors: {
            supportSelection: jQuery(".s")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.supportSelection.select2();
        }
    },
    /**
     * Customer Feedback Backend
     *
     */
    CustomerFeedbackResponse: {
        selectors: {
            feedbackSelection: jQuery(".st")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.feedbackSelection.select2();
        }
    },
    /**
     * Educational System
     *
     */
    EducationalSystem: {
        selectors: {
            EducationalSystemSelection: jQuery(".accAjj")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.EducationalSystemSelection.select2();
        }
    },
    /**
     * Menus
     *
     */
    CustomerFeedback: {
        selectors: {
            menuSelection: jQuery(".accAjcf")
        },
        init: function() {
            this.addHandlers();
        },
        addHandlers: function() {
            this.selectors.feedbackSelection.select2();
        }
    },
    /**
     * Specification
     *
     */
    Specification: {
        init: function() {
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e) { //on add input button click
                e.preventDefault();
                x++; //text box increment
                var t = '<div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12"><label for="property" class="col-lg-2 control-label required">Property</label><div class="col-lg-5"><input class="form-control" placeholder="Property" required=" required" name="property[]" type="text"></div><a href="#" class="remove_field btn btn-info"><i class="fa fa-fw fa-minus"></i> Remove</a></div>'
                $(wrapper).append(t); //add input box
            });
            $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
            $("#admin-specification").submit(function(event) {
                var values = $('input[name="property[]"]').map(function() {
                    return this.value;
                }).toArray();
                var hasDups = !values.every(function(v, i) {
                    return values.indexOf(v) == i;
                });
                if (hasDups) {
                    $("#error").append("<p>Property should be unique</p>");
                    $("#error").show()
                    return false
                }
            });
        }
    },
    /**
     * BrandModel
     *
     */
    Vehicle: {
        selectors: {
            brandSelection: jQuery(".dp")
        },
        init: function() {
            this.addHandlers();
            Imperial.tinyMCE.init();
            Imperial.dropZone.init();
            $("#specTab li:first-child a").tab('show');
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e) { //on add input button click
                e.preventDefault();
                x++; //text box increment
                var t = '<div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">\n\
                                <label for="colour" class="col-lg-2 control-label required">Colour</label>\n\
                                <div class="col-lg-3">\n\
                                    <input class="form-control" placeholder="Colour" required="required" name="colour[]" type="text">\n\
                                </div>\n\
                                <label for="photo" class="col-lg-2 control-label required">Photo</label>\n\
                                <div class="col-lg-3">\n\
                                    <input name="colour_file[]" type="file" required="required">\n\
                                </div>\n\
                                <a href="#" class="btn btn-danger btn-sm remove_field" role="button">Remove</a>\n\
                        </div>';
                $(wrapper).append(t); //add input box
            });
            $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
            $("#admin-specification").submit(function(event) {
                var values = $('input[name="property[]"]').map(function() {
                    return this.value;
                }).toArray();
                var hasDups = !values.every(function(v, i) {
                    return values.indexOf(v) == i;
                });
                if (hasDups) {
                    $("#error").append("<p>Property should be unique</p>");
                    $("#error").show()
                    return false
                }
            });
        },
        addHandlers: function() {
            this.selectors.brandSelection.select2();
        }
    },
    tinyMCE: {
        init: function() {
            tinymce.init({
                path_absolute: "/",
                selector: 'textarea',
                height: 200,
                theme: 'modern',
                plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak', 'searchreplace wordcount visualblocks visualchars code fullscreen', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                //                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
                image_advtab: true,
                relative_urls: false,
                file_browser_callback: function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
                    var cmsURL = "/" + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }
                    tinyMCE.activeEditor.windowManager.open({
                        file: cmsURL,
                        title: 'Filemanager',
                        width: x * 0.8,
                        height: y * 0.8,
                        resizable: "yes",
                        close_previous: "no"
                    });
                },
                content_css: ['//fonts.googleapis.com/css?family=Lato:300,300i,400,400i', '//www.tinymce.com/css/codepen.min.css']
            });
        }
    },
    dropZone: {
        init: function() {
            // modal window template
            //            var modalTemplate = '<div class="modal"><div class="modal-dialog"><!-- Modal content--><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>        <h4 class="modal-title">Modal Header</h4>      </div>      <div class="modal-body">       <div class="image-container"></div>      </div>      <div class="modal-footer">        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>      </div>    </div>  </div></div>';
            var maxImageWidth = 910,
                maxImageHeight = 440;
            Dropzone.autoDiscover = false;
            var vehicleDropzone = $("div#vehicle-dropzone").dropzone({
                //                autoProcessQueue: false,
                uploadMultiple: false,
                preventDuplicates: true,
                parallelUploads: 100,
                maxFilesize: 2,
                addRemoveLinks: true,
                dictRemoveFile: 'Remove',
                dictFileTooBig: 'Image is bigger than 2MB',
                url: "/admin/vehicle-management/addPhotos",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                sending: function(file, xhr, formData) {
                    if (typeof editVehicleId !== "undefined") {
                        formData.append('editVehicleId', editVehicleId);
                    } else {
                        formData.append('editVehicleId', "");
                    }
                },
                init: function() {
                    var context = this;
                    editVehicleId = (typeof editVehicleId !== "undefined") ? editVehicleId : "";
                    if (editVehicleId > 0) {
                        $.getJSON(baseUrl + '/admin/vehicle-management/' + editVehicleId + '/getPhotos', function(data) { // get the json response
                            $.each(data, function(key, value) { //loop through it
                                var mockFile = {
                                    name: value.name,
                                    size: value.size
                                }; // here we get the file name and size as response 
                                var drawnImage = context.createThumbnailFromUrl(mockFile, "/uploads/vehicle_images/" + editVehicleId + "/" + value.name, null, false);
                                context.options.addedfile.call(context, mockFile);
                                context.options.thumbnail.call(context, mockFile, "/uploads/vehicle_images/" + editVehicleId + "/" + value.name, drawnImage);
                                context.emit("complete", mockFile);
                                context.files.push(mockFile);
                            });
                        });
                    }
                    // Register for the thumbnail callback.
                    // When the thumbnail is created the image dimensions are set.
                    this.on("thumbnail", function(file) {
                        // Do the dimension checks you want to do
                        if (file.width == maxImageWidth && file.height == maxImageHeight) {
                            file.acceptDimensions();
                        } else {
                            file.rejectDimensions()
                        }
                    });
                },
                // Instead of directly accepting / rejecting the file, setup two
                // functions on the file that can be called later to accept / reject
                // the file.
                accept: function(file, done) {
                    file.acceptDimensions = done;
                    file.rejectDimensions = function() {
                        done("Invalid dimension.");
                    };
                    // Of course you could also just put the `done` function in the file
                    // and call it either with or without error in the `thumbnail` event
                    // callback, but I think that this is cleaner.
                },
                removedfile: function(file) {
                    //                    x = confirm('Do you want to delete?');
                    //                    if(!x)  return false;
                    editVehicleId = (typeof editVehicleId !== "undefined") ? editVehicleId : "";
                    $.ajax({
                        type: 'POST',
                        url: '/admin/vehicle-management/deletePhotos',
                        data: {
                            file_name: file.name,
                            editVehicleId: editVehicleId,
                            _token: $('#csrf-token').val()
                        },
                        dataType: 'html',
                        success: function(data) {
                            //                            var rep = JSON.parse(data);
                            //                            if(rep.code == 200)
                            //                            {
                            //                                photo_counter--;
                            //                                $("#photoCounter").text( "(" + photo_counter + ")");
                            //                            }
                        }
                    });
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                error: function(file, response) {
                    if ($.type(response) === "string") var message = response; //dropzone sends it's own error messages in string
                    else var message = response.message;
                    file.previewElement.classList.add("dz-error");
                    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                    _results = [];
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        node = _ref[_i];
                        _results.push(node.textContent = message);
                    }
                    return _results;
                },
                success: function(file, done) {
                    makeDropzoneSortable();
                }
            });
        }
    },
    emailTemplate: {
        init: function() {
            Imperial.emailTemplate.addHandlers();
            Imperial.tinyMCE.init();
        },
        // ! Imperial.emailTemplate.addHandlers
        addHandlers: function() {
            // to add placeholder in to active textarea
            $("#addPlaceHolder").on('click', function(event) {
                Imperial.emailTemplate.addPlaceHolder(event);
            });
            $("#showPreview").on('click', function(event) {
                Imperial.emailTemplate.showPreview(event);
            });
        },
        // ! Imperial.emailTemplate.addPlaceHolder
        addPlaceHolder: function(event) {
            var placeHolder = $('#placeHolder').val();
            if (placeHolder != '') {
                tinymce.activeEditor.execCommand('mceInsertContent', false, "[" + $('#placeHolder :selected').text() + "]");
            }
        },
        // ! Imperial.emailTemplate.showPreview
        showPreview: function(event) {
            console.log(tinyMCE.get('txtBody').getContent());
            jQuery(".modal-body").html(tinyMCE.get('txtBody').getContent());
            $(".model-wrapper").modal('show');
        },
    },
}
jQuery('#publish_date').datetimepicker({
    toolbarPlacement: 'top',
    showClose: true,
    sideBySide: true,
});
jQuery('#createDateButton').click(function() {
    $('#datetime').remove();
    $('.dateTimeParent').append("<input type='datetime-local' class='form-control' id='datetime' name='publish_datetime' autofocus='true'>");
});

function makeDropzoneSortable() {
    if ($("#vehicle-dropzone").hasClass('ui-sortable')) {
        $("#vehicle-dropzone").sortable('destroy');
    }
    $("#vehicle-dropzone").sortable({
        items: '.dz-preview',
        cursor: 'move',
        opacity: 0.5,
        containment: '#vehicle-dropzone',
        distance: 20,
        tolerance: 'pointer'
    });
}
// transform cropper dataURI output to a Blob which Dropzone accepts
//function dataURItoBlob(dataURI) {
//    var byteString = atob(dataURI.split(',')[1]);
//    var ab = new ArrayBuffer(byteString.length);
//    var ia = new Uint8Array(ab);
//    for (var i = 0; i < byteString.length; i++) {
//        ia[i] = byteString.charCodeAt(i);
//    }
//    return new Blob([ab], { type: 'image/jpeg' });
//}
function getBrandModels(brand_id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getBrandModels',
        data: {
            brand_id: brand_id,
            _token: $('#csrf-token').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            $('.brandModelDp').empty();
            var html = "<option value=''>Select Model</option>";
            $.each(options, function(key, element) {
                html += "<option value='" + key + "'>" + element + "</option>"
            });
            $('.brandModelDp').append(html);
            $('.brandModelDp').change();
        }
    });
}

function getDerivatives(model_id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getDerivatives',
        data: {
            model_id: model_id,
            _token: $('#csrf-token').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            $('.derivativeDp').empty();
            var html = "<option value=''>Select Derivative</option>";
            $.each(options, function(key, element) {
                html += "<option value='" + key + "'>" + element + "</option>"
            });
            $('.derivativeDp').append(html);
            $('.derivativeDp').change();
        }
    });
}

function sendNewslettermail(user_id) {
    $('.ajax-loader').show();
    $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/frontusers/send',
        data: {
            user_id: user_id,
            _token: $('#csrf-token').val()
        },
        success: function(data) {
            // var data = $.parseJSON(data);
            $('.ajax-loader').hide();
            if (data.status == "OK") {
                toastr.success('Mail sent successfully');
            } else {
                toastr.error('Something went wrong.Please try again.');
            }
        }
    });
}

function getCurriculumHierarchy(current_dropdown_id) {
    var type = $('#management_type').val();
    //var current_dropdown_id = $this.attr('id');
    var dropdown_array = [];
    dropdownIDsArray = $.map($("div[id*=custom_dropdowns]").find("select"), function(item, index) {
        var next = $(item).attr("id");
        dropdown_array.push(next);
        //alert(next);
    });
    if (typeof dropdown_array != 'undefined' && !jQuery.isEmptyObject(dropdown_array)) {
        var current_dropdown_position = dropdown_array.indexOf(current_dropdown_id);
        // alert(current_dropdown_position);
        $.each(dropdown_array, function(key, value) {
            if (key > current_dropdown_position) {
                if (value == "program_id") {
                    $('.program_div').remove();
                }
                if (value == "level_id") {
                    $('.level_div').remove();
                }
                if (value == "eductional_systems_id") {
                    $('.education_div').remove();
                }
                if (value == "grade_id") {
                    $('.grade_div').remove();
                }
                if (value == "subject_id") {
                    $('.subject_div').remove();
                }
            }
        });
    }
    var curriculum_id = $('#curriculum_id').val();
    var eductional_systems_id = $('#eductional_systems_id').val();
    var program_id = $('#program_id').val();
    var subject_id = $('#subject_id').val();
    var grade_id = $('#grade_id').val();
    var level_id = $('#level_id').val();
    if (typeof curriculum_id == 'undefined' || curriculum_id == "" || curriculum_id == 0) {
        return false;
    }
    if (current_dropdown_id == 'grade_id' && type == 'subject') {
        var grade_type = $('#grade_id option:selected').attr('data-type');
        if (grade_type == 1) {
            $('.number_of_max_questions_per_topic').show();
            $('.number_of_questions_per_topic').hide();
        } else {
            $('.number_of_max_questions_per_topic').hide();
            $('.number_of_questions_per_topic').show();
        }
    }
    var curriculum = $('#curriculum_id').length;
    var eductional_systems = $('#eductional_systems_id').length;
    var program = $('#program_id').length;
    var subject = $('#subject_id').length;
    var grade = $('#grade_id').length;
    var level = $('#level_id').length;
    var total_lenght = parseInt(curriculum) + parseInt(eductional_systems) + parseInt(program) + parseInt(subject) + parseInt(grade) + parseInt(level);
    if (total_lenght > 0) {
        var step = parseInt(total_lenght) - parseInt(1);
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getCurriculumsystem',
        data: {
            step: step,
            curriculum_id: curriculum_id,
            eductional_systems_id: eductional_systems_id,
            program_id: program_id,
            subject_id: subject_id,
            grade_id: grade_id,
            level_id: level_id,
            type: type,
            _token: $('#csrf-token').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            //education system dropdown
            if (type != "eductional_systems") {
                if (typeof options.education != 'undefined' && !jQuery.isEmptyObject(options.education)) {
                    if (eductional_systems == 0) {
                        var id = "eductional_systems_id";
                        var education_html = '<div class="form-group education_div"><label class="col-lg-2 control-label required" for="eductional_systems_id">Eductional system</label><div class="col-lg-10"><select name="eductional_systems_id" id="eductional_systems_id" class="form-control dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(education_html);
                    }
                    $('#eductional_systems_id').empty();
                    var html = "<option value=''>Select Educational system</option>";
                    $.each(options.education, function(key, element) {
                        html += "<option value='" + element.id + "'>" + element.name + "</option>"
                    });
                    $('#eductional_systems_id').append(html);
                    $("#eductional_systems_id").select2();
                    $('.education_div').show();
                } else {
                    //  $('#eductional_systems_id').val('').trigger("change");
                    //   $('.education_div').remove();
                }
            }
            //program dropdown
            if (type != "programs") {
                if (typeof options.program != 'undefined' && !jQuery.isEmptyObject(options.program)) {
                    if (program == 0) {
                        var id = "program_id";
                        var program_html = '<div class="form-group program_div"><label class="col-lg-2 control-label required" for="program_id">Program</label><div class="col-lg-10"><select name="program_id" id="program_id" class="form-control dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(program_html);
                    }
                    $('#program_id').empty();
                    var html = "<option value=''>Select program</option>";
                    $.each(options.program, function(key, element) {
                        html += "<option value='" + element.id + "'>" + element.program_name + "</option>"
                    });
                    $('#program_id').append(html);
                    $("#program_id").select2();
                    $('.program_div').show();
                } else {
                    //$('#program_id').val('').trigger("change");
                    //$('.program_div').remove();
                }
            }
            //subject dropdown
            if (type != "subject") {
                if (typeof options.subject != 'undefined' && !jQuery.isEmptyObject(options.subject)) {
                    if (subject == 0) {
                        var id = "subject_id";
                        var subject_html = '<div class="form-group subject_div"><label class="col-lg-2 control-label required" for="subject_id">Subject</label><div class="col-lg-10"><select name="subject_id" id="subject_id" class="form-control dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(subject_html);
                    }
                    $('#subject_id').empty();
                    var html = "<option value=''>Select subject</option>";
                    $.each(options.subject, function(key, element) {
                        html += "<option value='" + element.id + "'>" + element.subject_name + "</option>"
                    });
                    $('#subject_id').append(html);
                    $("#subject_id").select2();
                    // $('#subject_id').attr('required','required');
                    $('.subject_div').show();
                } else {
                    // $('#subject_id').removeAttr('required');
                    // $('#subject_id').val('').trigger("change");
                    //$('.subject_div').remove();
                }
            }
            //grade dropdown
            if (type != "grade") {
                if (typeof options.grade != 'undefined' && !jQuery.isEmptyObject(options.grade)) {
                    if (grade == 0) {
                        var id = "grade_id";
                        var grade_html = '<div class="form-group grade_div"><label class="col-lg-2 control-label required" for="grade_id">Grade</label><div class="col-lg-10"><select name="grade_id" id="grade_id" class="form-control dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(grade_html);
                    }
                    $('#grade_id').empty();
                    var html = "<option value=''>Select grade</option>";
                    $.each(options.grade, function(key, element) {
                        html += "<option value='" + element.id + "' data-type='" + element.grade_type + "'>" + element.grade_name + "</option>"
                    });
                    $('#grade_id').append(html);
                    $("#grade_id").select2();
                    // $('#grade_id').attr('required','required');
                    $('.grade_div').show();
                } else {
                    // $('#grade_id').removeAttr('required');
                    // $('#grade_id').val('').trigger("change");
                    // $('.grade_div').remove();
                }
            }
            //level dropdown
            if (type != "levels") {
                if (typeof options.level != 'undefined' && !jQuery.isEmptyObject(options.level)) {
                    if (level == 0) {
                        var id = "level_id";
                        var level_html = '<div class="form-group level_div"><label class="col-lg-2 control-label required" for="level_id">Level</label><div class="col-lg-10"><select name="level_id" id="level_id" class="form-control dynamic_dropdown" onchange="getCurriculumHierarchy(id,' + options.step + ')" required="required"></select></div></div>';
                        $('#custom_dropdowns').append(level_html);
                    }
                    $('#level_id').empty();
                    var html = "<option value=''>Select Level</option>";
                    $.each(options.level, function(key, element) {
                        html += "<option value='" + element.id + "'>" + element.levels_name + "</option>"
                    });
                    $('#level_id').append(html);
                    $("#level_id").select2();
                    $('.level_div').show();
                } else {
                    // $('#level_id').val('').trigger("change");
                    //$('.level_div').remove();
                }
            }
            $('.select2-container').css('width', '100%');
        }
    });
}

function getTopics(id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getTopics',
        data: {
            id: id,
            _token: $('#csrf-token').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            if (!jQuery.isEmptyObject(options.topic)) {
                $('#topic_id').empty();
                var html = "<option value=''>Select Topic</option>";
                $.each(options.topic, function(key, element) {
                    html += "<option value='" + key + "'>" + element + "</option>"
                });
                $('#topic_id').append(html);
                $('.topic_div').show();
                $('#no_topics').hide();
                $('#submit_question').prop('disabled', false);
            } else {
                $('.topic_div').hide();
                $('#no_topics').show();
                $('#submit_question').prop('disabled', true);
                $('#topic_id').val('').trigger("change");
            }
            $('.select2-container').css('width', '100%');
        }
    });
}
//For Blog datetimepicker for publish_datetime
jQuery('#datetimepicker1').datetimepicker({
    format: 'DD/MM/YYYY hh:mm a',
});

function filterSession() {
    var filter_type = $('#session_filter_type').val();
    var dateFrom = $('#date_start').val();
    var dateTo = $('#date_end').val();
    var session_report_type = $('#session_report_type').val();
    var current_date = $('#current_date').val();
    if (filter_type == "cancelled") {
        var field_name = '`session`.`cancelled_date_time`';
    } else if (filter_type == "upcoming") {
        var field_name = '`session`.`scheduled_date_time`';
    } else if (filter_type == "successful") {
        var field_name = '`session`.`scheduled_date_time`';
    }
    if (session_report_type == "") {
        if (dateFrom == "") {
            $('#search_error').html('Please select start date');
            $('#search_error').show();
            return false;
        } else {
            $('#search_error').hide();
        }
        if (dateTo == "") {
            $('#search_error').html('Please select end date');
            $('#search_error').show();
            return false;
        } else {
            $('#search_error').hide();
        }
        var new_from = addDays(new Date(dateFrom), -1);
        var new_end = addDays(new Date(dateTo), 1);
        dateFrom = "'" + new_from + "'";
        dateTo = "'" + new_end + "'";
    } else {
        var new_from = addDays(new Date(current_date), -(session_report_type));
        var new_end = current_date;
        dateFrom = "'" + new_from + "'";
        dateTo = "'" + new_end + "'";
        $('#date_start').val('');
        $('#date_end').val('');
    }
    var dateFilter = {
        groupOp: "AND",
        rules: [{
            "field": field_name,
            "op": "ge",
            "data": dateFrom
        }, {
            "field": field_name,
            "op": "le",
            "data": dateTo
        }]
    }
    jQuery('#list').jqGrid('setGridParam', {
        postData: {
            filters: JSON.stringify(dateFilter)
        }
    }).trigger("reloadGrid");
}

function addDays(theDate, days) {
    var date = new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
    var d = new Date(date);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = year + "-" + month + "-" + day;
    return date;
}

function fileExists(url) {
    var arr = ['.jpg', '.JPG', '.png', '.PNG', '.jpeg', '.JPEG', '.svg', '.SVG', '.gif', '.GIF', '.bmp', '.BMP'];
    if (url.match(/\.(jpeg|jpg|gif|png|bmp|svg|JPEG|JPG|GIF|PNG|BMP|SVG)$/) != null) {
        if (url) {
            var req = new XMLHttpRequest();
            req.open('GET', url, false);
            req.send();
            return req.status == 200;
        } else {
            return false;
        }
    }
}

function filterPiechart() {
    var month = $('#month').val();
    var year = $('#year').val();
    if (month == "") {
        $('#month_error').show();
        return false;
    } else {
        $('#month_error').hide();
    }
    if (year == "") {
        $('#year_error').show();
        return false;
    } else {
        $('#year_error').hide();
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/filterPieChart',
        data: {
            month: month,
            year: year,
            _token: $('#csrf-token').val()
        },
        success: function(data) {
            var status = data.status;
            if (status == "success") {
                $('#pie_chart_portion').html(data.html);
            } else {
                var error_html = "<center><p>There are no session details found for this time interval</p></center>";
                $('#pie_chart_portion').html(error_html);
            }
        }
    });
}

function getCity() {
    var country = $('#country_id').val();
    $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getCityBasedOncountry',
        data: {
            country: country,
            _token: $('#csrf-token').val()
        },
        success: function(data) {
            var options = $.parseJSON(data);
            if (typeof options != 'undefined' && !jQuery.isEmptyObject(options)) {
                $('#city_id').empty();
                var html = "<option value=''>Select City</option>";
                $.each(options, function(key, element) {
                    html += "<option value='" + element.id + "'>" + element.city_name + "</option>"
                });
                $('#city_id').append(html);
            }
        }
    });
}