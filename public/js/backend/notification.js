$(document).ready(function () {
    $(".multiple-select").select2();
    $(".select2").select2();
    // state-city dropdown - multi select
    $('#multiple_state_id').change(function (e) {
        var state = $(this).val();
        var url = $('#city_url').val();
        var slecetedCities = $("#multiple_city_id").val() ? $("#multiple_city_id").val() : [];

        $.ajax({
            type: 'POST',
            url: url,
            data: 'state_id=' + state,
            success: function (data) {
                cities = $.parseJSON(data);
                var html = '';
                $.each(cities, function (index, value) {
                    if ($.inArray(index, slecetedCities) != "-1") {
                        html += '<option value=' + index + ' selected="selected" >' + value + '</option>';
                    } else {
                        html += '<option value=' + index + '>' + value + '</option>';
                    }
                });
                $('#multiple_city_id').html(html);
            }
        });
    });
    $(".notifications-menu-btn").on('click',function(){
        $.ajax({
            type: 'GET',
            url: baseUrl + '/admin/notification/clearcurrentnotifications',
            dataType: "JSON",
            success: function(data){
                getNotifications();
            }
        });
    });
    getNotifications();
    setInterval(function () {
        getNotifications();
    }, 60000);
});
function getNotifications() {
    $.ajax({
        type: "GET",
        url: baseUrl + '/admin/notification/getlist',
        dataType: "JSON",
        success: function (result) {
            $(".notification-counter").text(result.count);
            $(".notification-menu-container").html(result.view);
        }
    });
}
