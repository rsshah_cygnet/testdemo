// Design global file
// // JavaScript Document
//$ = $.noConflict();
var O2H = {
	init: function () {
		$(window).resize(function () {
			O2H.loadResize();
		});

		O2H.loadResize();

		//Custom select function
		$(".select2").select2();
	},

	/* ! O2H.loadResize */
	loadResize: function () {
		//load resize script starts 
		O2H.jQGRidMobile();
		
		
		//script to get windows height and width
		var window_h = $(window).height();
		var window_w = $(window).width();

		//condition for the device width greater then and equal to 768
		if (window_w >= 768) {


		}

		//condition for the device width less then and equal to 768(mobile devices)
		else {

		}
		//load resize script ends 
	},

	/**
     jQGRidMobile
     function to make jqgrid responsive
     @version: 2016-07-18 updated (last update date of function)
     @return: what this function returns
		$param: paramters used name with their type
     */
	jQGRidMobile: function () {
		var $grid = $("#list"),
			newWidth = $grid.closest(".ui-jqgrid").parent().width();
		$grid.jqGrid("setGridWidth", newWidth, true);
	},




};

// when the page is ready, initialize everything
$(document).ready(function () {
	O2H.init();
});