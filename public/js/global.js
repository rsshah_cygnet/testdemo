// JavaScript Document
$ = jQuery.noConflict();
// Store the window width
var windowWidth;

$(document).on('click tap' , '.left-sidebar-toggle',function(){              
    if($(window).width() <= 1200){
        $(this).toggleClass('active-sidebar');
        $('body').toggleClass('left-side-expand');
    }
});
$(document).click(function(e){
    if($(window).width()<= 1200){
        if($(e.target).closest(".dashboard-page .navbar-toggle").length ==0 && !$(e.target).hasClass('active-sidebar')){
           $('body').removeClass('left-side-expand');
       }
   }
});
// when the page is ready, initialize everything
$(document).ready(function() {

 windowWidth = $(window).width();

 $('.suggested-tutor-scroll').each(function(){
   var get_width = $('.suggested-tutor-list').length * $('.suggested-tutor-list').innerWidth();

   $('.suggested-tutor-scroll').width(get_width); 
   $('.suggested-tutor-list-wrapper').perfectScrollbar();
});

 $(window).on('load resize',function(){
   /*banner section start fomr here*/
   $('.banner-section').height($(window).height());
   /*banner section end from here*/

    //  if($('body').hasClass('left-side-expand')){
    //     alert(0);
    //     $('body').removeClass('left-side-expand');     
    // }
    $('.left-aside-dashboard').mouseenter(function(){
        if($(window).width() >= 1201){
            $('body').addClass('left-side-expand');
        }
    }).mouseleave(function(){
        if($(window).width() >= 1201){
            $('body').removeClass('left-side-expand');

        }
    });
});

 $('.tutor-box-radio-box input[name=available]').on('change', function() {
   if($('input[name=available]:checked').val() == 2){
       $('.available-specific-day').stop().fadeOut('slow');

       setTimeout(function(){ $('.available-everyday').stop().fadeIn();}, 600);

   }
   else{
       $('.available-everyday').stop().fadeOut('slow');
       setTimeout(function(){ $('.available-specific-day').stop().fadeIn();}, 600);
   }
}); 




     //Date range picker with time picker
     if($('#reservation, #payment-date').length){
        if($(window).width() <= 991){
            $('#reservation').daterangepicker({
                "opens": "top",
                "drops": "up",
                "parentEl": ".datetime-range"
            });
            $('#payment-date').daterangepicker({
                "opens": "top",
                "drops": "down",
                "parentEl": ".datetime-range"
            });
            
        }else{
         $('#reservation').daterangepicker({
            "opens": "right",
        });
         $('#payment-date').daterangepicker({
            "opens": "left",
        });
     }
 }
 if($('.perfect-scroll').length){
    $('.perfect-scroll').perfectScrollbar();
}
//tab to accordion
if($('#userDetail').length){
    $('#userDetail').tabCollapse();
}
if($('.datepicker').length){    
    $('.datepicker').daterangepicker({
       "singleDatePicker": true,
       showDropdowns: true
   });
}
if($('.selectpicker').length){   
    $('.selectpicker').selectpicker();
}
    // $('.select-curriculum').change(function(){
    //    if($(this).val() == "lorem"){
    //        $('.select-accordion').slideDown();
    //    }
    //     else{
    //         $('.select-accordion').slideUp();
    //     }
    // });
    
    if($('.time-picker').length > 0){
        $('.time-picker').datetimepicker({
            format: 'LT'
        });
    }

    $('.filter-mobile').click(function(){
        $('body').addClass('open-drawar');
    })
    $('.close-drawer').click(function(){
        $('body').removeClass('open-drawar');
    })
    
    /*inline calender start form here*/
    if($('.calender-inline').length > 0){
     $('.calender-inline').datetimepicker({
       inline: true,
       format: 'DD/MM/YYYY'  
   });
 }
 /*inline calender end from here*/


 $('.rating').on('change', function(){
    var rating_val = $(this).val();
    $('.rating-outof').text( "("+rating_val+"/5)" );
});


 $('.dropdown-menu').on('click touchstart tap', function(e){
        //The event won't be propagated to the document NODE and 
        // therefore events delegated to document won't be fired
        e.stopPropagation();
    });



 $('.dropdown-menu .close').click(function(){
     $(".dropdown-menu").dropdown('toggle');
     return false;
 });

 /*$('body').on('click','.back-button',function(){
     $('.conform-wrapper').slideUp();
    $('.reserve-tutee-wrapper').slideDown();
 })
  $('body').on('click','.reserve-tutee-wrapper .tutee-primary-btn',function(){
     $('.conform-wrapper').slideDown();
    $('.reserve-tutee-wrapper').slideUp();
 })*/
 $('.back-button').click(function(){
    $('.conform-wrapper').slideUp();
    $('.reserve-tutee-wrapper').slideDown();
});

 $('.reserve-tutee-wrapper .tutee-primary-btn').click(function(){
    $('.conform-wrapper').slideDown();
    $('.reserve-tutee-wrapper').slideUp();
});

 /*tooltip for find tutor page*/
 $('[data-toggle="tooltip"]').tooltip();

 $(window).on('resize',function(){
   $("[data-toggle = 'tooltip']").tooltip('hide'); 
});

 /*Tab collapse for Payment tutor page*/
 if($('#PaymentTab').length > 0){
    $('#PaymentTab').tabCollapse();    
}
fixedHeader();

if($('.landing-page-main').length > 0){


    $('.landing-page-main .navbar-nav li a[href^="#"] , .landing-page-main .about-us-content-wrapper .contact-btn-wrapper').click(function(event){
        event.preventDefault();
        var target = this.hash,
        $target = $(target);
        $('.landing-page-main #mainnav').collapse('hide');
        var heightGet = $('.landing-page-main .logo').height();

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - heightGet
        }, 900, 'swing', function () {
           // window.location.hash = target;

       });
        $('.landing-page-main #mainnav').collapse('hide');
    });

    window.addEventListener('mousewheel', function(event){
        $('#menu-navigation li').removeClass('active');
        var about = document.getElementById('aboutUs').getBoundingClientRect();
        var howItWorks = document.getElementById('howItWork').getBoundingClientRect();
        var ContactUs = document.getElementById('contactUS').getBoundingClientRect();

        if(ContactUs.top <= $('.landing-page-main .logo').height() )
        {
            $('#menu-navigation li.contact').first().addClass('active');
        }
        else if(howItWorks.top <= $('.landing-page-main .header-landing-page').height() )
        {
            $('#menu-navigation li.howitwork').first().addClass('active');
        }
        else if(about.top <= $('.landing-page-main .header-landing-page').height() )
        {
            $('#menu-navigation li.about').first().addClass('active');
        }
    }, false);

}
});
$(window).on("load resize",function(e){
    if($(window).height()<= 640){

        $('.landing-input input').focus(function(){
            $('body').addClass('inputfocus');

        });
        $('.landing-input input').blur(function(){
            $('body').removeClass('inputfocus');
        });

    }
});

$(window).scroll(fixedHeader);
function fixedHeader() {
    var windowScroll = $(window).scrollTop();
    if (windowScroll >= 10) {
        $('.landing-page-header.navbar-fixed-top , .header-landing-page ').addClass('fixed_header');
    } else {
        $('.landing-page-header.navbar-fixed-top ,  .header-landing-page').removeClass('fixed_header');
    }
}




$(window).on('load resize',function(){


    if ($(window).width() != windowWidth) {

            // Update the window width for next time
            windowWidth = $(window).width();

            /*banner section start fomr here*/
            $('.banner-section').height($(window).height());
            /*banner section end from here*/

            // if($('body').hasClass('left-side-expand')){
            //     alert(0)
            //     $('body').removeClass('left-side-expand');     
            // }
            $('.left-aside-dashboard').mouseenter(function(){
                if($(window).width() >= 1201){
                    $('body').addClass('left-side-expand');
                }
            }).mouseleave(function(){
                if($(window).width() >= 1201){
                    $('body').removeClass('left-side-expand');

                }
            });

        }


    });


/*google chart start here*/

// $(window).resize(function(){
//     drawChart();
//     drawChart2();
// });

/*google chart end here*/


