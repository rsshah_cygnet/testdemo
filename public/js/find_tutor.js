 $(window).on('hashchange', function() {
    if (window.location.hash) {
      var page = window.location.hash.replace('#', '');
      if (page == Number.NaN || page <= 0) {
        return false;
      } else {
  //        getPosts(page);
      }
    }
  });

 $(document).ready(function() {
     $('.rating-input').unbind('mouseenter mouseleave click'); 

     $('body').on('click','#reserve_btn',function(){
          if(!$('.reserve-place').hasClass('open')){
            $('.reserve-place').addClass('open');
          }
     })

    $(document).on('click', '.pagination a', function (e) {
       e.preventDefault();
       var page_type = $('#is_find_tutor').val();
       if(page_type != "" && page_type == "find_tutor"){
             AjaxData($(this).attr('href').split('page=')[1]);
       }

      var tutee_payment = $('#is_tutee_payment').val();
      if(tutee_payment != undefined && tutee_payment == 'tutee_payment'){
          tuteePaymentPagination($(this).attr('href').split('page=')[1]);    
      }
      e.preventDefault();
    });

    $('body').on('click','#reset',function (e)
    {
      loadFindTutor();
    });

    $('#reservation').on('cancel.daterangepicker', function(ev, picker) {
      $('#reservation').val('');
    });
    var curriculum_length = $('#curriculum_id').length;
    if(curriculum_length > 0){
        getCurriculumHierarchy("#curriculum_id");
        trrigerChange();  
    }
    
    var page_type = $('#is_find_tutor').val();
     if(page_type != "" && page_type == "find_tutor"){
          $('body').on('change','.dynamic_dropdown',function(){
            trrigerChange();
          })   
     }
    

    var level_trriger = true;
    var grade_trriger = true;
    var education_trriger = true;
    var subject_trriger = true;
    var program_trriger = true;
    var topic_trriger = true;

    function trrigerChange(){
      if(page_type != "" && page_type != "find_tutor"){
        return false;
      }
      setTimeout(function(){

        var curriculum = $('#curriculum_id').length;
        var eductional_systems = $('#eductional_systems_id').length;
        var levels = $('#level_id').length;
        var program = $('#program_id').length;
        var grade = $('#grade_id').length;
        var subject = $('#subject_id').length;
        var topic = $('#topic_id').length;



        var selected_education = $('#search_education_id').val();
        var selected_level = $('#search_level_id').val();
        var selected_program = $('#search_program_id').val();
        var selected_grade = $('#search_grade_id').val();
        var selected_subject = $('#search_subject_id').val();
        var selected_topic = $('#search_topic_id').val();

        if(selected_education != 0 && selected_education !="" && selected_education != "null" && eductional_systems > 0 && education_trriger == true){
          $('#eductional_systems_id').val(selected_education).trigger("change");
          education_trriger = false;
        }

        if(selected_level != 0 && selected_level != "null" && levels > 0 && selected_level != "" && level_trriger == true){
          $('#level_id').val(selected_level).trigger("change");
          level_trriger = false;

        }

        if(selected_program != 0 && selected_program !="" && selected_program != "null" && program > 0 && program_trriger == true){
          $('#program_id').val(selected_program).trigger("change");
          program_trriger = false;
        }

        if(selected_grade != 0 && selected_grade !="" && selected_grade != "null" && grade > 0 && grade_trriger == true){
          $('#grade_id').val(selected_grade).trigger("change");
          grade_trriger = false;
        }

        if(selected_subject != 0 && selected_subject !="" && selected_subject != "null" && subject > 0 && subject_trriger == true){
          $('#subject_id').val(selected_subject).trigger("change");
          subject_trriger = false;
        }

        if(selected_topic != 0 && selected_topic !="" && selected_topic != "null" && topic > 0 && topic_trriger == true){
          $('#topic_id').val(selected_topic).trigger("change");
          topic_trriger = false;
        }
      }, 80);
    }

 });

 $(window).load(function()
  {
    var reservation_serach = $('#search_reservation').val();
    if(reservation_serach != ""){
        $("#reservation").val(reservation_serach);  
    }else{
      $("#reservation").val('');
    }
     var token = $('#tutor_find_token').val();
     var page_type = $('#is_find_tutor').val();
     if(page_type != "" && page_type == "find_tutor"){
        AjaxData(1); 
     }

       var current_date = new Date();
        var page_type = $('#page_type').val();
        if(page_type == 'tutor_detail'){

          var gb;
          var subject_id;
          var topic_id;  
          $('#curriculum_id').val('');
          $('.navbar-toggle').hide();
          $("#prev").hide(function()
          {
           });

          var Total = $('#total_reviews').val();
          if(Total <= 10)
          {
            $("#next").hide();
          }

           var strVale = $('#enabledDates').val();
          var date_arr = strVale.split('","');
          $('#calender-inline').datetimepicker({
               inline: true,
               format: 'DD/MM/YYYY',
               enabledDates : date_arr,
               minDate: new Date()
            //   defaultDate: '08/06/2017',

          });

         
            
        }

        $('body').on('click','#reserve_without_login',function(){
              var tutor_id = $('#tutorid').val();
             var token = $('#csrf_token_header').val();
              $.ajax({
                        type: 'POST',
                        url: '/tutor-check-login',
                        data: {tutor_id:tutor_id,_token: token},
                        success: function(data)
                        {
                           window.location = data;
                        }
                    });
          })
          
         
     
  })

  function AjaxData(page)
  {
    var token = $('#tutor_find_token').val();
    $.ajax({
          url: baseUrl + '/getTutorList',
          type: 'POST',
          data: $("#searchTutor").serialize()+"&page="+page+"&_token="+token,
          dataType: 'html',
      }).success(function (data) {
          $('#tutor-result').html(data);
          $('.rating').rating();
          $('.rating-input').unbind('mouseenter mouseleave click');
      }).fail(function () {
         alert('Tutor could not be loaded.');
      });
  }


    $('body').on('submit','#searchTutor',function (e)
    {
       e.preventDefault();
      $('#searchhidden').html('');
      var token = $('#tutor_find_token').val();

      $.ajax({
        url: baseUrl + '/find',
        type: 'POST',
        dataType : 'html',
        data: $("#searchTutor").serialize()+"&_token="+token+"&page=1",
        success: function (data)
        {
          $('#tutor-result').html(data);
          $('.rating').rating();
          $('.rating-input').unbind('mouseenter mouseleave click');
          $('.close-drawer').click();
        }
      });
    });


    function loadMore(button)
    {
          var total = $('#total_reviews').val();
          var token = $('#csrf_token_header').val();
          var userId = $("#tutorid").val();

          var hidden_limit = $('#hidden_limit').val();
          var totalNext = $('#totalNext').val();
          var totalPrev = $('#totalPrev').val();

            var totalPrev = $('#totalPrev').val();
            if(button == 'p')
            {

              $('#hidden_limit').val(parseInt(hidden_limit) - 10);
               $('#totalPrev').val(parseInt(totalPrev) - 10);
              $('#totalNext').val(parseInt(totalNext) + 10);
            }
            else
            {
              $('#hidden_limit').val(parseInt(hidden_limit) + 10);
              $('#totalNext').val(parseInt(totalNext) - 10);
              $('#totalPrev').val(parseInt(totalPrev) + 10);

            }

       $.ajax({
        //url: '{{ route("frontend.getreview") }}',
        url: baseUrl + '/viewreview',
        type: 'POST',
        dataType : 'html',
        data: $("#loadMoreForm").serialize()+"&_token="+token,
        success: function(data)
        {
          $('#tutor_rating_div').html(data);
          var hidden_limit = $('#hidden_limit').val();
          var totalNext = $('#totalNext').val();
          var totalPrev = $('#totalPrev').val();

          if(button == 'p')
          {
            if(totalPrev <= 0)
            {
              $("#prev").addClass('hidden');
              $("#next").removeClass('hidden');
            }

            if(data == "" && button != "p")
            {
              $("#prev").addClass('hidden');
              $("#next").removeClass('hidden');
            }
          }

          else
          {
            if(totalNext <= 0)
            {
              $("#next").addClass('hidden');
              $("#prev").removeClass('hidden');
            }

            if(data == "" && button != "n")
            {
              $("#next").addClass('hidden');
              $("#prev").removeClass('hidden');
            }
          }
              $('.rating').rating();
              $('.rating-input').unbind('mouseenter mouseleave click');
        },
        
    });
    }


     function showErrorOnReserve(message){
          $('.conform-wrapper').slideUp();
          $('.reserve-tutee-wrapper').slideDown();
          $(".msg").html(message);
          $('html,body').animate({
            scrollTop: $(".tutor-detail").offset().top},
            'slow');
           $(".msg").show();
          return false;
    }


    
      function reserveSession(){
         //e.preventDefault();
                var fromDate = $('.active').data('day');
                subject_id = $('#subject_id').val();
                topic_id = $('#topic_id').val();
                var token = $('#csrf_token_header').val();
                var tutorId =  $("#tutorid").val();

                $.ajax({
                url: baseUrl + '/tutor-checkPreference',
                type: 'POST',
                dataType : 'html',
                data: $("#sessionReserve").serialize()+"&_token="+token+"&fromDate="+fromDate+"&tutorId="+tutorId,
                success: function(data)
                {
                  var json_obj = $.parseJSON(data);
                  gb = json_obj;

                  if(json_obj.response == "success")
                  {
                     $(".fromdate").html("  "+json_obj.from_date);
                     $(".fromtime").html("  "+json_obj.start_time);
                     $(".hierarchy").html(json_obj.hierarchy);
                     $(".obj").html(json_obj.obj);
                     $(".fees").html('$'+json_obj.sessionFees);
                     $(".msg").hide();

                      var education_system_lenght = $('#eductional_systems_id').length;
                      var level_id_lenght = $('#level_id').length;
                      var grade_id_lenght = $('#grade_id').length;
                      var program_id_lenght = $('#program_id').length;
                      var subject_id_lenght = $('#subject_id').length;
                      var topic_id_lenght = $('#topic_id').length;

                      var education_system_id = $('#eductional_systems_id').val();
                      var level_id = $('#level_id').val();
                      var grade_id = $('#grade_id').val();
                      var program_id = $('#program_id').val();
                      var subject_id = $('#subject_id').val();
                      var topic_id = $('#topic_id').val();

                      if(education_system_lenght > 0){
                        if(education_system_id == "" || education_system_id == '0'){
                          showErrorOnReserve('please select educational system');
                        }
                        
                      }
                      if(level_id_lenght > 0){
                        if(level_id == "" || level_id == '0'){
                          showErrorOnReserve('please select level');
                        }
                        
                      }
                      if(grade_id_lenght > 0){
                        if(grade_id == "" || grade_id == '0'){
                          showErrorOnReserve('please select grade');
                        }
                        
                      }
                      if(program_id_lenght > 0){
                        if(program_id == "" || program_id == '0'){
                          showErrorOnReserve('please select program');
                        }
                        
                      }
                      if(subject_id_lenght > 0){
                        if(subject_id == "" || subject_id == '0'){
                          showErrorOnReserve('please select subject');
                        }
                        
                      }
                      if(topic_id_lenght > 0){
                        if(topic_id == "" || topic_id == '0'){
                          showErrorOnReserve('please select topic');
                        }
                        
                      }
                       $('html,body').animate({
                      scrollTop: $(".tutor-detail").offset().top},
                      'slow');
                  }
                  else
                  {
                    $('.conform-wrapper').slideUp();
                    $('.reserve-tutee-wrapper').slideDown();
                    $(".msg").html(json_obj.message);
                     $(".msg").show();
                    $('html,body').animate({
                      scrollTop: $(".tutor-detail").offset().top},
                      'slow');
                  }

              }
                });
      } 

      function confirmSession()
      {
         // e.preventDefault();
          var token = $('#csrf_token_header').val();
          var tutorId =  $("#tutorid").val();


          $.ajax({
          url:  baseUrl + '/tutor-conformPreference',
          type: 'POST',
          data:{tutorId:tutorId,gb:gb,_token:token,subject_id:subject_id,topic_id:topic_id},
          success: function(data)
          {
            var json_obj = $.parseJSON(data);
            if(json_obj.response == "success")
            {
              alert(json_obj.message);
              $(".tutee-dashboard-btn dropdown-toggle").addClass("hidden");
              $(".msg").hide();
              var tutorId =  $("#tutorid").val();
             loadTutorDetails(tutorId)
            }
            else
            {
              alert(json_obj.message);
              $(".msg").html(json_obj.message);
              $(".msg").show();
            }
          }
          });
      }