<?php
if(isset($_POST["submit"]) && !empty($_FILES["csv_file"])) 
{
	$file_type = strtolower(pathinfo($_FILES["csv_file"]["name"],PATHINFO_EXTENSION));
	if($file_type == "csv")
	{
		$target_dir = getcwd(). "/uploads/".date("d-m-Y")."/";
		mkdir($target_dir, 0755);
		$file_name = "file-" . date("d-m-Y");
		$target_file = $target_dir . $file_name . "." . $file_type;
		$updated_target_file = $target_dir . "updated-" . $file_name . "." . $file_type;
		
		if (move_uploaded_file($_FILES["csv_file"]["tmp_name"], $target_file)) 
		{
			chmod($target_file, 0755);
	        echo "The file ". basename( $_FILES["csv_file"]["name"]). " has been uploaded.";
	        
	        // read csv data and store it in $csv_data variable in array formats
	        $csv_data = array_map('str_getcsv', file($target_file));
	        
	        $date_invalid = array();
	        $columns_mismatch = array();
	        foreach ($csv_data as $key => $value) 
	        {
	        	if($key > 0) 
	        	{
	        		if(date('Y-m-d', strtotime(str_replace("/", "-", $value[1]))) == "1970-01-01")
	        		{
	        			array_push($date_invalid, array("line_no" => ($key+1), "value" => $value));
	        			unset($csv_data[$key]);
	        			continue;
	        		}
	        		if(count($value) != 14)
	        		{
	        			array_push($columns_mismatch, array("line_no" => ($key+1), "value" => $value));
	        			unset($csv_data[$key]);
	        			continue;
	        		}
	        	}
	        }
	        
	        // reindex csv data array
	        $csv_data_filtered = array_values($csv_data);

	        // write filtered csv records in new "updated" csv file
	        $fp = fopen($updated_target_file, 'w');
	        foreach ($csv_data_filtered as $v) {
	            fputcsv($fp, $v);
	        }
	        fclose($fp);

	        // write invalid records in $logdata variable
	        $logdata = "";
	        if(!empty($date_invalid))
	        {
	        	$logdata .= "\n".str_repeat("-",43)."\n";
	        	$logdata .= "Date is invalid in following ".count($date_invalid)." record(s):";
	        	$logdata .= "\n".str_repeat("-",43)."\n";
	        	foreach ($date_invalid as $k => $v) {
	        		$logdata .= "#".$v['line_no'].": \t".implode(",", $v['value']) . "\n";
	        	}
	        }
	        if(!empty($columns_mismatch))
	        {
	        	$logdata .= "\n".str_repeat("-",55)."\n";
	        	$logdata .= "Column count mismatch in following ".count($columns_mismatch)." record(s):";
	        	$logdata .= "\n".str_repeat("-",55)."\n";
	        	foreach ($columns_mismatch as $k => $v) {
	        		$logdata .= "#".$v['line_no'].": \t".implode(",", $v['value']) . "\n";
	        	}
	        }

	        // write contents of $logdata in a text file
	        $log_file = $target_dir.$file_name."-log.txt";
	        file_put_contents($log_file, $logdata);

	        // show log on webspage
	        echo str_replace("\n","<br/>", $logdata);
	    } 
	    else 
	    {
	        echo "Sorry, there was an error uploading your file.";
	    }		
	}
    else 
    {
        echo "Only CSV file is allowed.";
    }
}
?>