DROP FUNCTION IF EXISTS `insert_vehicle_stock`;
DELIMITER $$
CREATE FUNCTION insert_vehicle_stock(vehicle_id INT, tvs_id INT) RETURNS INT
    DETERMINISTIC
BEGIN
    DECLARE vehicle_stock_id int;
 	-- insert new vehicle stock in "vehicle_stocks" table from temporary table
 	INSERT INTO vehicle_stocks (vehicle_id, 
 								dealership_id, 
 								adoption_date, 
 								vehicle_number, 
 								vehicle_identification_number, 
 								colour_description, 
 								odometer_reading, 
 								mead_mcgrouther_code, 
 								listing_title, 
 								actual_cost, 
 								location_code, 
 								model_year, 
 								description, 
 								markup_percentage, 
 								gross_profit, 
 								created_by,
 								created_at
							)
	SELECT 	v.id AS vehicle_id,
			deal.id AS dealership_id,
			tvs.adoption_date,
			tvs.vehicle_number,
			tvs.vehicle_identification_number,
			tvs.colour_description,
			tvs.odometer_reading,
			tvs.mead_mcgrouther_code,
			tvs.listing_title,
			tvs.actual_cost,
			tvs.location_code,
			tvs.model_year,
			tvs.description,
			'0' AS markup_percentage,
			'0' AS gross_profit,
			tvs.created_by,
			NOW()
	FROM vehicle_stocks_csv_tmp as tvs
	JOIN vehicles as v
	ON v.id=vehicle_id
	JOIN dealerships as deal
	ON deal.dealership_code=tvs.company
	WHERE tvs.id=tvs_id;
 	SET vehicle_stock_id = LAST_INSERT_ID();
 RETURN (vehicle_id);
END
