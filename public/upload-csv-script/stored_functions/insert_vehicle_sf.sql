DROP FUNCTION IF EXISTS `insert_vehicle`;
DELIMITER $$
CREATE FUNCTION insert_vehicle(tvs_id INT) RETURNS INT
    DETERMINISTIC
BEGIN
    DECLARE vehicle_id int;
 	-- insert vehicle details in "vehicles" table
 	INSERT INTO vehicles (  brand_id, 
 							brand_model_id, 
 							derivative_id, 
 							vehicle_type_id, 
 							vehicle_type, 
 							selling_price,
 							added_by, 
 							active, 
 							created_by,
 							created_at
						)
	SELECT  b.id AS brand_id, 
			bm.id AS brand_model_id, 
			d.id AS derivative_id, 
			'1' AS 'vehicle_type_id', 
			tvs.new_used_demo AS vehicle_type , 
			'0' AS 'selling_price',
			'cron' AS added_by, 
			'1' AS 'active', 
			tvs.created_by,
			NOW()
	FROM vehicle_stocks_csv_tmp as tvs
	JOIN brands as b
	ON b.brand_name = tvs.manufacturer
	JOIN brand_models as bm
	ON bm.brand_model_name = tvs.model AND bm.brand_id = b.id
	JOIN derivatives as d
	ON d.brand_id=b.id AND d.brand_model_id=bm.id AND d.derivative_name=tvs.listing_title
	WHERE tvs.id=tvs_id
	GROUP BY b.id, bm.id, d.id;
 	SET vehicle_id = LAST_INSERT_ID();
 	RETURN (vehicle_id);
END