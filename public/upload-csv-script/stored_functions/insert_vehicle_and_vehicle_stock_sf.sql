DROP FUNCTION IF EXISTS `insert_vehicle_and_vehicle_stock`;
DELIMITER $$
CREATE FUNCTION insert_vehicle_and_vehicle_stock() RETURNS INT
    DETERMINISTIC
BEGIN
    DECLARE tvs_id INT;
    DECLARE vehicle_id INT;
    DECLARE vehicle_stock_id INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
    						FROM vehicle_stocks_csv_tmp AS tvs
    						WHERE tvs.vehicle_number NOT IN (
								SELECT vs.vehicle_number
								FROM vehicle_stocks AS vs
								JOIN vehicles AS v
								ON  v.id=vs.vehicle_id
								WHERE v.deleted_at IS NULL
							);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
 	OPEN cursor_i;
	 	read_loop: LOOP
	 		FETCH cursor_i INTO tvs_id;
	 		IF done THEN
	      		LEAVE read_loop;
	      	END IF;
	      	SET vehicle_id = 0;
	      	SET vehicle_stock_id = 0;
	      	IF tvs_id > 0 THEN
		      	SELECT insert_vehicle(tvs_id) INTO vehicle_id;
		      	IF vehicle_id > 0 THEN
		      		SELECT insert_vehicle_stock(vehicle_id, tvs_id) INTO vehicle_stock_id;
		      	END IF;
		    END IF;
	 	END LOOP;
  	CLOSE cursor_i;
  	RETURN(vehicle_stock_id);
END