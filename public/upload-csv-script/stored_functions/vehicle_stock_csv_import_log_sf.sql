DROP FUNCTION IF EXISTS `vehicle_stock_csv_import_log`;
DELIMITER $$
CREATE FUNCTION vehicle_stock_csv_import_log (
					last_message_text VARCHAR(255)
					 ) RETURNS TEXT
    DETERMINISTIC
BEGIN
	DECLARE fn_result TEXT;

    DECLARE log_m TEXT;

 	SELECT message INTO log_m 
 	FROM vehicle_stock_csv_import_logs
 	WHERE log_date = CURDATE();

	SET log_m = CONCAT_WS("", log_m, NOW(), ": ", last_message_text, "\n");
 	
 	INSERT INTO vehicle_stock_csv_import_logs 
 		(log_date, last_message, message) 
 	VALUES 
 		(CURDATE(), last_message_text, log_m) 
 	ON DUPLICATE KEY UPDATE  
 		last_message = VALUES(last_message), 
 		message = VALUES(message);

	RETURN (log_m);
END