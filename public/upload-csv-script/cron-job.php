<?php
//check if file is uploaded today
//$target_dir = getcwd(). "/uploads/";
$target_dir = getcwd(). "/uploads/".date("d-m-Y")."/";
//$target_file = $target_dir . "file-" . date("d-m-Y") . ".csv";
$target_file = $target_dir . "updated-file-" . date("d-m-Y") . ".csv";
if(file_exists($target_file))
{
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "imperialselect"; //"imperialselect"; //"testcsv";
	$tmp_vehicle_stock_table = "vehicle_stocks_csv_tmp";
	$cron_result_table = "cron_result_table";
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	//create temporary table in database 
	$sql = "CALL `create_vehicle_stock_csv_import_logs_table`(@sp_result); SELECT sp_result;";
	if ($conn->multi_query($sql) === TRUE) {
	    //echo "Table ".$tmp_vehicle_stock_table." created successfully";
	} else {
	    echo "Error creating table: " . $conn->error;
	}
	$conn->close();

	$conn = new mysqli($servername, $username, $password, $dbname);
	$sql = "CALL `create_vehicle_stocks_csv_tmp_table`(@sp_result); SELECT sp_result;";
	if ($conn->multi_query($sql) === TRUE) {
	    //echo "Table ".$tmp_vehicle_stock_table." created successfully";
	} else {
	    echo "Error creating table: " . $conn->error;
	}
	$conn->close();

	$conn = new mysqli($servername, $username, $password, $dbname);
	// load csv file data into db table
	$sql = "LOAD DATA LOCAL INFILE '".$target_file."'
			INTO TABLE ".$tmp_vehicle_stock_table."
			FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
			LINES TERMINATED BY '\n'
			IGNORE 1 ROWS
			(company,@adoption_date,vehicle_number,vehicle_identification_number,new_used_demo,manufacturer,model,colour_description,odometer_reading,mead_mcgrouther_code,listing_title,actual_cost,location_code,model_year)
			SET adoption_date = STR_TO_DATE(@adoption_date, '%d/%m/%Y');";
	$conn->query($sql);
	//$conn->query("SELECT vehicle_stock_csv_import_log('CSV data imported into temporary table')");
	$conn->close();
	
	$conn = new mysqli($servername, $username, $password, $dbname);
	// $sql = "CALL vehicle_stock_csv_import2(@sp_result); SELECT @sp_result;";
	$sql = "CALL vehicle_stock_csv_import_sp(@sp_result); SELECT @sp_result;";
 	$output = $conn->multi_query($sql);
 	$output_message = "";
 	if($output) {
	    do {
	        if ($result = $conn->store_result()) {
	            while ($row = $result->fetch_row()) {
	                $output_message .= " ".$row[0];
	            }
	            $result->free();
	        }
	    } while ($conn->next_result());
 	} else {
 		var_dump($conn->error);
 	}
 	
 	echo "<p>".$output_message."</p>";

	$conn->close();
}
?>