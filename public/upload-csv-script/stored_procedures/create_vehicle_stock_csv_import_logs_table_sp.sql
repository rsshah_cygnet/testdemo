-- @author: Parth Shah
-- @last_updated_on: 25-01-2017 07:33 PM
DROP PROCEDURE IF EXISTS `create_vehicle_stock_csv_import_logs_table`;
SET @OLD_SQL_MODE=@SQL_MODE;
SET SQL_MODE='';
DELIMITER $$
CREATE PROCEDURE create_vehicle_stock_csv_import_logs_table(OUT sp_result varchar(4000))
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
		@errno = MYSQL_ERRNO, @texts = MESSAGE_TEXT;
		SET sp_result = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @texts);
		ROLLBACK;
	END;
	-- 
	-- Table structure for table `vehicle_stock_csv_import_logs`
	--
	CREATE TABLE IF NOT EXISTS `vehicle_stock_csv_import_logs` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `log_date` DATE NOT NULL,
	  `message` text NOT NULL,
	  `last_message` varchar(255) NOT NULL,
	  `created_at` datetime NOT NULL,
	  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY(`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;
	
	ALTER TABLE `vehicle_stock_csv_import_logs` ADD UNIQUE(`log_date`);
	
	SET sp_result = "Table vehicle_stock_csv_import_logs has been created successfully";
END $$
DELIMITER ;
SET @SQL_MODE=@OLD_SQL_MODE;