-- @author: Parth Shah
-- @last_updated_on: 23-01-2017 07:33 PM
DROP PROCEDURE IF EXISTS `vehicle_stock_csv_import_sp`;
SET @OLD_SQL_MODE=@SQL_MODE;
SET SQL_MODE='';
DELIMITER $$
CREATE PROCEDURE vehicle_stock_csv_import_sp(OUT sp_result varchar(4000))
BEGIN
	DECLARE log_msg TEXT;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
		@errno = MYSQL_ERRNO, @texts = MESSAGE_TEXT;
		SET sp_result = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @texts);
		ROLLBACK;
	END;
	-- start sql transaction
	START TRANSACTION;
	-- insert vehicle dealer in "dealerships" table
    INSERT IGNORE INTO dealerships (dealership_code, status, created_by, dealership_name, address, banner_image, dealership_email, dealer_principal_email, dealer_principal_contact_no)
	SELECT DISTINCT tvs.company, tvs.status, tvs.created_by, '', '', '', '', '', ''
	FROM vehicle_stocks_csv_tmp AS tvs	
	LEFT JOIN vehicle_stocks AS vs
  	ON tvs.vehicle_number = vs.vehicle_number
  	LEFT JOIN vehicles AS v
  	ON  v.id=vs.vehicle_id
	WHERE ((vs.vehicle_number IS NULL) AND (v.deleted_at IS NULL));
	-- Add log
	-- SELECT vehicle_stock_csv_import_log('Dealerships added') INTO log_msg;
	-- insert vehicle brand in "brands" table
	INSERT INTO brands (brand_name, status, created_by)
	SELECT DISTINCT tvs.manufacturer, tvs.status, tvs.created_by
	FROM vehicle_stocks_csv_tmp as tvs	
	LEFT JOIN vehicle_stocks AS vs
  	ON tvs.vehicle_number = vs.vehicle_number
  	LEFT JOIN vehicles AS v
  	ON  v.id=vs.vehicle_id
	WHERE ((vs.vehicle_number IS NULL) 
		AND 
		(v.deleted_at IS NULL)) 
		AND 
		tvs.manufacturer NOT IN (SELECT brand_name FROM brands);
	-- Add log
	-- SELECT vehicle_stock_csv_import_log('Brands added') INTO log_msg;
	-- insert vehicle brand_model in "brand_models" table
	INSERT INTO brand_models (brand_id, brand_model_name, status, created_by)
	SELECT b.id, tvs.model, tvs.status, tvs.created_by
	FROM vehicle_stocks_csv_tmp as tvs
	JOIN brands as b
	ON b.brand_name = tvs.manufacturer	
	LEFT JOIN vehicle_stocks AS vs
  	ON tvs.vehicle_number = vs.vehicle_number
  	LEFT JOIN vehicles AS v
  	ON  v.id=vs.vehicle_id
	WHERE ((vs.vehicle_number IS NULL) 
		AND 
		(v.deleted_at IS NULL)) 
		AND
		NOT EXISTS (
			SELECT id 
	    	FROM brand_models 
	    	WHERE 	brand_id=b.id AND 
	    			brand_model_name=tvs.model
  		)
	GROUP BY b.id, tvs.model;
	-- Add log
	-- SELECT vehicle_stock_csv_import_log('Brand models added') INTO log_msg;
	-- insert vehicle brand_model derivatives in "derivatives" table
	INSERT INTO derivatives (brand_id, brand_model_id, derivative_name, status, created_by)
	SELECT b.id, bm.id, tvs.listing_title, tvs.status, tvs.created_by
	FROM vehicle_stocks_csv_tmp as tvs
	JOIN brands as b
	ON b.brand_name = tvs.manufacturer
	JOIN brand_models as bm
	ON bm.brand_model_name = tvs.model AND bm.brand_id = b.id	
	LEFT JOIN vehicle_stocks AS vs
  	ON tvs.vehicle_number = vs.vehicle_number
  	LEFT JOIN vehicles AS v
  	ON  v.id=vs.vehicle_id
	WHERE ((vs.vehicle_number IS NULL) 
		AND 
		(v.deleted_at IS NULL)) 
		AND
	  	NOT EXISTS (
		    SELECT id 
		    FROM derivatives 
		    WHERE 	brand_id=b.id AND 
		    		brand_model_id=bm.id AND 
		    		derivative_name=tvs.listing_title
	  	)
	GROUP BY b.id, bm.id, tvs.listing_title;
	-- Add log
	-- SELECT vehicle_stock_csv_import_log('Derivatives added') INTO log_msg;
	-- Remove vehicle stock records from "vehicle_stock" table which are not exist in csv (in temporary table)
	-- set vehicles as "sold" 
	UPDATE vehicles	
	SET sold=1, deleted_at=now() 
	WHERE id IN (
		SELECT vehicle_id 
		FROM vehicle_stocks 
		WHERE vehicle_number NOT IN (
			SELECT vehicle_number 
			FROM vehicle_stocks_csv_tmp
		)
	) AND (deleted_at IS NULL);
	-- Add log
	-- SELECT vehicle_stock_csv_import_log('Vehicle sold flag added for removed vehicles') INTO log_msg;
	-- insert vehicle and vehicle_stock records
	SELECT insert_vehicle_and_vehicle_stock();
	-- Add log
	-- SELECT vehicle_stock_csv_import_log('vehicle and vehicle stock added') INTO log_msg;
  	-- transaction completed successfuly commit all the queries
	COMMIT;
	SET sp_result = "Vehicle stock has been updated successfully";
	-- Add log
	-- SELECT vehicle_stock_csv_import_log(sp_result) INTO log_msg;
END $$
DELIMITER ;
SET @SQL_MODE=@OLD_SQL_MODE;