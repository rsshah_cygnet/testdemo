-- @author: Parth Shah
-- @last_updated_on: 25-01-2017 07:33 PM
DROP PROCEDURE IF EXISTS `create_vehicle_stocks_csv_tmp_table`;
SET @OLD_SQL_MODE=@SQL_MODE;
SET SQL_MODE='';
DELIMITER $$
CREATE PROCEDURE create_vehicle_stocks_csv_tmp_table(OUT sp_result varchar(4000))
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
		@errno = MYSQL_ERRNO, @texts = MESSAGE_TEXT;
		SET sp_result = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @texts);
		ROLLBACK;
	END;
	--
	-- Table structure for table `vehicle_stocks_csv_tmp`
	--
	CREATE TABLE IF NOT EXISTS `vehicle_stocks_csv_tmp` (
	  `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT,
	  `company` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
	  `vehicle_id` int(10) UNSIGNED DEFAULT NULL,
	  `dealership_id` int(10) UNSIGNED NOT NULL COMMENT 'Comapny field in CSV will add the dealership.',
	  `adoption_date` date NOT NULL COMMENT 'Adoption Date in CSV.',
	  `vehicle_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Vehicle Number in CSV.',
	  `vehicle_identification_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Vehicle Identification Number in CSV.',
	  `new_used_demo` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
	  `manufacturer` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `colour_description` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Colour Description in CSV.',
	  `odometer_reading` int(10) UNSIGNED NOT NULL COMMENT 'Last Odometer Reading in CSV.',
	  `mead_mcgrouther_code` bigint(20) UNSIGNED NOT NULL COMMENT 'Mead and McGrouther Code in CSV.',
	  `listing_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Description in CSV',
	  `actual_cost` decimal(15,2) UNSIGNED NOT NULL COMMENT 'Tot Stock in CSV. At which cost vehicle come to sale.',
	  `location_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Location Code in CSV',
	  `model_year` smallint(5) UNSIGNED NOT NULL COMMENT 'Model Year in CSV.',
	  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
	  `markup_percentage` decimal(5,2) UNSIGNED DEFAULT NULL,
	  `gross_profit` decimal(15,2) UNSIGNED DEFAULT NULL,
	  `status` enum('Active','InActive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
	  `created_by` int(10) UNSIGNED DEFAULT '1',
	  `updated_by` int(10) UNSIGNED DEFAULT NULL,
	  `created_at` timestamp NULL DEFAULT NULL,
	  `updated_at` timestamp NULL DEFAULT NULL,
	  `deleted_at` timestamp NULL DEFAULT NULL,
	  PRIMARY KEY(`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	--
	-- Empty existing table
	--
	TRUNCATE `vehicle_stocks_csv_tmp`;

	SET sp_result = "Table tmp_vehicle_stock has been created successfully";
END $$
DELIMITER ;
SET @SQL_MODE=@OLD_SQL_MODE;