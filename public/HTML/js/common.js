 $(document).ready(center_height,home_banner,desktop_slider,new_car,mobile_menu,car_detail, maximum_height_car);

//Windows resize jquery starts/////////////////////////////////////////////////////////////
$(window).resize(function (center_height,home_banner,desktop_slider,new_car,mobile_menu,car_detail, maximum_height_car) {
    /*center_height();
    home_banner();
    desktop_slider();
    new_car();
    mobile_menu();
    car_detail();*/
	
});
//Windows resize jquery End/////////////////////////////////////////////////////////////

var height = $(window).height();
function center_height(){
	var window_height = $(window).height()- $('header').outerHeight();
	$('.find-dealer-wrapper').css('height',window_height);
}

//Desktop home page banner
function home_banner(){
    
    //Home Page Banner height for different resolution 
	if ($(window).width() < 1281) {
       
		$('#banner').height(height);
	} else {
		//$('#banner').height(height - 20 + 'px');
        
		$('#banner').height(height - 200 + 'px');
         
	}
    if ($(window).height() <= 900) {
        
        $('#banner').height(height - 350 + 'px');
    }
    if ($(window).height() <= 800) {
        
        $('#banner').height(height - 280 + 'px');
    }
    
	if ($(window).height() <= 768) {
		$('#banner').height(height - 150 + 'px');
	}
    
    if ($(window).height() >= 632 && $(window).height() <= 638) {
        
        $('#banner').height(height - 120 + 'px');
	}
    if ($(window).height() <= 630) {
        
        $('#banner').height(height);
		//$('#banner').height(height - 120 + 'px');
	}

	
}

//Desktop banner bottom menu effects
function desktop_slider(){
    //Desktop Slider Bottom menu effect
	if ($(window).width() < 1367) {
		$(".banner-bottom li").on("mouseenter", function () {
			$(this).children(".link-hover").css('transform', 'translateY(10%)');
		}).on("mouseleave", function () {
			$(this).children(".link-hover").css('transform', 'translateY(98%)');
		});
	} else {
		$(".banner-bottom li").on("mouseenter", function () {
			$(this).children(".link-hover").css('transform', 'translateY(0px)');
		}).on("mouseleave", function () {
			$(this).children(".link-hover").css('transform', 'translateY(92%)');
		});
	}

    
    
}

//New Car page 
function new_car(){
        if ($(window).width() > 991) {
		$('#RefineOptRow').addClass('m-t-30');
		$('#RefineOptRow').addClass('min-width-refinerow');
		$('.refine-car').css('overflow', 'hidden');
		$('#RefineTitle').fadeOut();
		$('.detail-column').fadeOut();
		
		$('#RefineArrow').click(function () {
            
			$('#RefineColumn').removeClass('col-md-1 col-lg-1');
			$('#RefineColumn').addClass('col-md-4 col-lg-3');
			$('#SortingColumn').removeClass('col-md-11 col-lg-11');
			$('#SortingColumn').addClass('col-md-8 col-lg-9');
			$('#RefineArrowRow').fadeOut(100);
			$('#RefineTitle').fadeIn(500);
			$('#RefineOptRow').removeClass('m-t-30');
			$('#RefineOptRow').removeClass('min-width-refinerow');
			$('.refine-car').css('overflow', 'visible');
			$('#RefineSpecials').fadeIn(500);
			$('.car-model').removeClass('col-lg-3 col-m-3');
			$('.car-model').removeClass('col-lg-3 col-md-3');
			$('.car-model').addClass('col-lg-4 col-md-4');
			$('.car-list').removeClass('col-lg-3');
			$('.car-list').addClass('col-lg-4');
			$('.car-list').removeClass('col-lg-6');
			$('.car-list').addClass('col-lg-3');
			$('.car-list').removeClass('col-lg-3');
			$('.car-list').addClass('col-lg-4');
			$('#SortingColumn').removeClass('col-md-6 col-lg-6');
			$('#SortingColumn').addClass('col-md-11 col-lg-11');
			$('.detail-column').fadeOut();
			$("#SortingColumn, .detail-column").mCustomScrollbar("destroy");
			$('#RefineColumn').height();
			$('.detail-column').height();
            maximum_height_car();
		});

		$('#RefineClose').click(function () {
			$('#RefineColumn').addClass('col-md-1 col-lg-1');
			$('#RefineColumn').removeClass('col-md-4 col-lg-3');
			$('#SortingColumn').addClass('col-md-11 col-lg-11');
			$('#SortingColumn').removeClass('col-md-8 col-lg-9');
			$('#RefineArrowRow').fadeIn(500);
			$('#RefineTitle').fadeOut(100);
			$('#RefineOptRow').addClass('m-t-30');
			$('#RefineOptRow').addClass('min-width-refinerow');
			$('.refine-car').css('overflow', 'hidden');
			$('#RefineSpecials').fadeOut(100);
			$('.car-model').addClass('col-lg-3 col-md-3');
			$('.car-model').removeClass('col-lg-4 col-md-4');
			$('.car-list').addClass('col-lg-3');
			$('.car-list').removeClass('col-lg-4');
            maximum_height_car();
		});
		
		$('.car-detail-close').click(function () {
			$('#SortingColumn').addClass('col-md-11 col-lg-11');
			$('#SortingColumn').removeClass('col-md-6 col-lg-6 col-md-8 col-lg-9');
			$('.car-list').addClass('col-lg-3');
			$('.car-list').removeClass('col-lg-6');
			$('.car-model').removeClass('col-lg-3 col-md-3');
			$('.car-model').addClass('col-lg-4 col-md-4');
			$('#CarSelect .car-box').removeClass('car-seleted');
			//$('.detail-column').addClass('detail-column-transform');
			$('.detail-column').fadeOut(500);
			$('#SortingColumn').height(height - 20 + 'px');
			$("#SortingColumn, .detail-column").mCustomScrollbar("destroy");
			$('#RefineColumn').height();
			$('.detail-column').height();
			$('.car-model').addClass('col-lg-3 col-md-3');
			$('.car-model').removeClass('col-lg-4 col-md-4');
		});
		
		$('.car-image, .car-information h2 a, .car-information h2.car-price a').click(function () {
			$('#SortingColumn').removeClass('col-md-11 col-lg-11');
			$('#SortingColumn').addClass('col-md-6 col-lg-6');
			$('.car-list').removeClass('col-lg-3');
			$('.car-list').addClass('col-lg-6');
			$('#RefineColumn').addClass('col-md-1 col-lg-1');
			$('#RefineArrowRow').fadeIn(500);
			$('#RefineTitle').fadeOut(100);
			$('#RefineOptRow').addClass('m-t-30');
			$('#RefineOptRow').addClass('min-width-refinerow');
			$('.refine-car').css('overflow', 'hidden');
			$('#RefineSpecials').fadeOut(100);
			$('.car-model').addClass('col-lg-3 col-md-3');
			$('.car-model').removeClass('col-lg-4 col-md-4');
			$('#CarSelect .car-box').addClass('car-seleted');
			//$('.detail-column').addClass('detail-column-transform');
			$('.detail-column').fadeIn(500);
			$('#SortingColumn').height(height - 20 + 'px');
			$("#SortingColumn, .detail-column").mCustomScrollbar({
				mouseWheelPixels: 400
			});
			$('#RefineColumn').height(height);
			$('.detail-column').height(height);
		});

		$('.car-image, .car-information h2 a, .car-information h2.car-price a').click(function () {
			if ($('.car-list').hasClass('col-lg-4')) {
				$('.car-list').removeClass('col-lg-4 col-lg-6');
				$('.car-list').addClass('col-lg-6');
			}
		});
		
		
        
		
	}    
    }

// Mobile Menu button 

function mobile_menu(){
    $('.mobile-menu .btn').click(function () {
		$('.mobile-menu').css({
			'transform': 'translateY(20%)',
			'-webkit-transform': 'translateY(20%)',
			'-ms-transform': 'translateY(20%)'
		});
		$('.mobile-tab').show();
		$('.mobile-link li a.btn').css('border', 'none');
		$('.banner-top').css('margin-top', '30px');
		$('.mobile-link p').hide();
		$('.banner').addClass('landscape-menu');
	});
    
}

//Car detail page open page for small and mobile screen
function car_detail(){
    
	if ($(window).width() < 991) {
		$('.car-image, .car-information h2 a, .car-information h2.car-price a').click(function () {
			window.location = "new-car-detail.html";
		});
        $('.dealer-car-listing .car-image, .dealer-car-listing .car-information h2 a, .dealer-car-listing.car-information h2.car-price a').click(function () {
			window.location = "dealer-detail.html";
		});
	}

}

function maximum_height_car(){
    
    var maxHeight = -1;
    $('.car-list .car-box').each(function() {
        console.log($(this).height());
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
   });
    
    $('.car-list .car-box').each(function() {
     $(this).height(maxHeight +15);
   });
}




$(document).click(function () {
	$('#myNavbar').hide();
});
    
    
$(".blog-tab-content").easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion           
            width: 'auto', //auto or any custom width
            fit: true,   // 100% fits in a container
            closed: 'accordion', // Close the panels on start, the options 'accordion' and 'tabs' keep them closed in there respective view types
            tabidentify: 'blog-detail-tabs'
       
    });




//Document ready jquery starts/////////////////////////////////////////////////////////////
$(document).ready(function () {
    center_height();
    home_banner();
    desktop_slider();
    new_car();
    mobile_menu();
    car_detail();
    maximum_height_car();
    
    
    
    
   
    //Dealer Tab
     $(".dealer-tab-link").click(function(){
        $(".dealer-tab-block").toggleClass('tab-collapse');
    });
    
    
    
	// Custom Checkbox
	$('.icheck').iCheck({
		checkboxClass: 'icheckbox',
		radioClass: 'iradio',
		increaseArea: '20%'
	});

	//Accordion arrow change function op collapse
	$('.collapse').on('shown.bs.collapse', function (e) {
		//$('.accordion-heading i').toggleClass(' ');
		$(e.target).prev('.panel-heading').addClass('accordion-opened');
	});
	$('.collapse').on('hidden.bs.collapse', function (e) {
		$(e.target).prev('.panel-heading').removeClass('accordion-opened');
		//$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
	});

	//Select 2 js initialized
	$('select').select2();

	//responsive toggle menu initialized
	$('[data-menu]').menu();


	//Home Page Latest Specials Slider Start
	$("#latest-newcar, #latest-usedcar").owlCarousel({
		nav: true,
		dots: true,
		dotsData: false,
		navText: ["<img src='images/owl-prev-arrow.png'>", "<img src='images/owl-next-arrow.png'>"],
		items: 1
	});

	

	//Owl carousel variables Start
	var flag = false;
	var owl = $(".CarSlider");
	var totalItems;
	var totalItems = $('.CarSlider .owl-item').length;
	var currentIndex = $('.owl-item.active').index() + 1;


	//New car page Car color option carousel start
	$(".CarColorSlider").owlCarousel({
			items: 1,
			nav: false,
			dots: true
		})
		.on('changed.owl.carousel', function (e) {
			if (!flag) {
				flag = true;
				$(".CarThumbSlider").trigger('to.owl.carousel', [e.item.index, true]);
				flag = false;
			}
			$('.owl-item').removeClass('thumb-active');
		});
	$(".CarThumbSlider").owlCarousel({
			items: 5,
			nav: true,
			dots: false,
			navText: ["<img src='images/arrow-small-left.png'>", "<img src='images/arrow-small-right.png'>"],
			responsive: {
				0: {
					items: 2,
					slideBy: 1
				},
				768: {
					items: 5
					
				}
			}
		})
		.ready(function () {
			$('.CarThumbSlider .owl-item:first-child').addClass('thumb-active');
		})
		.on('click', '.owl-item', function () {
			$(".CarColorSlider").trigger('to.owl.carousel', [$(this).index(), true]);
			$(this).addClass('thumb-active');
		})
		.on('changed.owl.carousel', function (e) {
			if (!flag) {
				flag = true;
				$(".CarColorSlider").trigger('to.owl.carousel', [e.item.index, true]);
				flag = false;
			}
		});


	//Car Detail top Slider
	$('.CarSlider').owlCarousel({
		items: 1,
		nav: true,
		navText: ["<img src='images/arrow-white-left.png'>", "<img src='images/arrow-white-right.png'>"],
		animateOut: 'fadeOut'
	});

	//Car Detail top slider slide number jquery start
	$(".CarSlider .owl-prev, .CarSlider .owl-next").click(function () {
		$('.slide-num').html(currentIndex + "/" + totalItems);
	});
	$(".CarSlider .owl-item").on("touchstart mouseup", function () {
		var owl = $(".CarSlider");
		var totalItems;
		var totalItems = $('.top-slider .owl-item').length;
		var currentIndex = $('.owl-item.active').index() + 1;
		$('.slide-num').html(currentIndex + "/" + totalItems);
	});
	
	
	//Refine by Brand add active class on click
	$('.refine-by-brand a, .register-brand a').click(function(){
		$(this).toggleClass('active');
	})
	
    //Dealer scrollbar
    $(".dealer-container").mCustomScrollbar({});
        
    $('.back-top').on('click', function () {
        $(".dealer-container").mCustomScrollbar("scrollTo", "#target-element1");
        $(".detail-column").mCustomScrollbar("scrollTo", "#target-element");
});
    
   
    
    
    
});
//Document ready jquery End/////////////////////////////////////////////////////////////






//Windows load jquery starts/////////////////////////////////////////////////////////////
$(window).load(function () {
    
    
    //Blog page scroll for Desktop screens
		$('.resp-tabs-list.blog-detail-tabs, .blog-tab-container').height(height - 150 + 'px');
		$(".resp-tabs-list.blog-detail-tabs	, .blog-tab-container").mCustomScrollbar({
				mouseWheelPixels: 400
		});
    
    
        
    
    
	//Car Detail Page Top Slider
	var owl = $("#CarSlider");
	$('#CarSlider').owlCarousel();
	var totalItems = $('#CarSlider .owl-item').length;
	var currentIndex = $('.owl-item.active').index() + 1;
	$('.slide-num').html(currentIndex + "/" + totalItems);
    
    //Dealer Car Detail Page Top Slider
    var owl = $('.DealerCarSlider');
    owl.owlCarousel({
        loop: true,
        items: 1,
        thumbs: true,
        thumbImage: true,
        thumbContainerClass: 'owl-thumbs',
        nav:false,
        thumbItemClass: 'owl-thumb-item'
        
    });
    
    //Scroll Top
    
    
    
});
