var width = $(document).width();
$(window).resize(function (maximum_height_car, car_filter,car_detail){
});    
function maximum_height_car(){
    
    var maxHeight = -1;
    $('.special-car-list-box').each(function() {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
   });
    
    $('.special-car-list-box').each(function() {
     $(this).height(maxHeight +15);
   });
}
function car_filter(){
    
    /****New car listing start here****/
    if (width > 991) {
    $('.car-list-box-image a, .car-listing-name a').click(function () {
        $('.new-car-listing').addClass('col-md-3 col-lg-2');
        $(this).parent().parent('.special-car-list-box').addClass('car-seleted');
        $('.new-car-listing').removeClass('col-md-12 col-lg-12');
        $('.special-car-listing').removeClass('col-lg-3 col-md-4 col-xs-12 col-sm-6');
        $('.special-car-listing').addClass('col-lg-12 col-sm-12 col-md-12 col-xs-12');
        $('.new-car-detail').removeClass('hidden');
        $('.new-car-detail').addClass('visible');
        $('.arrow-close').show();
        $(".new-car-listing, .new-car-detail ").mCustomScrollbar({
				mouseWheelPixels: 400,
                set_height:"500px"
               
		});
    });
    $('.arrow-close').click(function () {
        $('.new-car-listing').addClass('col-md-12 col-lg-12');
        $('.new-car-listing').removeClass('col-md-3 col-lg-2');
        $('.special-car-listing').removeClass('col-lg-12 col-sm-12 col-md-12 col-xs-12');
        $('.special-car-listing').addClass('col-lg-3 col-md-4 col-xs-12 col-sm-6');
         $('.new-car-detail').addClass('hidden');
        $('.new-car-detail').removeClass('visible');
        $('.arrow-close').hide();
        $(".new-car-listing, .new-car-detail ").mCustomScrollbar("destroy");
        $('.new-car-listing .special-car-list-box').removeClass('car-seleted');
    });
    }
    
    /****New car listing end here****/
}

function car_detail(){
    
	if ($(window).width() < 991) {
		$('.car-list-box-image a, .car-listing-name a').click(function () {
			window.location = "new-car-detail.html";
		});
	}

}


/*********document ready start here*********/ 
$(document).ready(function () {
    car_filter();
    maximum_height_car();
    car_detail();
    /****select dropdown init start here****/
    if($('select').length > 0){
        $('select').select2();    
    }
    
    /****select dropdown init end here****/
    
    /****home page slider start here****/
    if($('#home-slider').length > 0){
    $('#home-slider').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        items:1,
        dots:true
        
    });
    }
    /****home page slider end here****/
    
    /****home page slider start here****/
    if($('#special-car-slider').length > 0){
    $('#special-car-slider').owlCarousel({
        loop:false,
        margin:0,
        nav:true,
        dots:false,
        thumbs: false,
         responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:7
        }
    }
        
    });
    }
    
    /****home page slider end here****/
    
    //responsive toggle menu initialized
	$('[data-menu]').menu();
    
    
    
    /****New car detail slider start  here****/
    if($('.new-car-detail-slider').length > 0){
    var owl = $('.new-car-detail-slider');
    owl.owlCarousel({
        loop: true,
        items: 1,
        margin:0,
        thumbs: true,
        thumbImage: true,
        nav:true,
        dots:true,
        thumbContainerClass: 'owl-thumbs',
        thumbItemClass: 'owl-thumb-item'
    });
    
    }
    
    /****New car detail slider end  here****/
    
    /****Detail slider start  here****/
    var flag = false;
    $(".detail-slider").owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        thumbs:false,
        loop:false
	})
    .on('changed.owl.carousel', function (e) {
			if (!flag) {
				flag = true;
				$(".detail-thumb-slider").trigger('to.owl.carousel', [e.item.index, true]);
				flag = false;
			}
			$('.detail-thumb-slider .owl-item').removeClass('thumb-active');
	});
    
    var owl = $('.detail-thumb-slider');
    owl.owlCarousel({
            loop: false,
            items: 3,
            margin:0,
            thumbs: false,
            nav:true,
            dots:false,
            responsive: {
                    0: {
                        items: 2,
                        slideBy: 1
                    },
                    768: {
                        items:3

                    }
                }

        })
        .ready(function () {
			$('.detail-thumb-slider .owl-item:first-child').addClass('thumb-active');
		})
        .on('click', '.owl-item', function () {
			$(".detail-slider").trigger('to.owl.carousel', [$(this).index(), true]);
			$(this).addClass('thumb-active');
		})
		.on('changed.owl.carousel', function (e) {
			if (!flag) {
				flag = true;
				$(".detail-slider").trigger('to.owl.carousel', [e.item.index, true]);
				flag = false;
			}
		});
    
       
    /****Car interior slider start  here****/
    if($('.car-interior-slider').length > 0){
    var owl = $('.car-interior-slider');
    owl.owlCarousel({
        loop: true,
        items: 1,
        margin:0,
        thumbs: false,
        nav:true,
        dots:false,
    });
    
    }
    
    /****Car interior slider end  here****/
    
     /****Car interior slider start  here****/
    if($('.safety-slider').length > 0){
    var owl = $('.safety-slider');
    owl.owlCarousel({
        loop: true,
        items: 1,
        margin:0,
        thumbs: false,
        nav:true,
        dots:false,
    });
    
    }
    
    
    
});
/*********document ready end here*********/

$(window).load(function() {
    
    var totalItems = $('#special-car-slider .item').length;
    var countitem = 0;
    $('#special-car-slider .owl-item').each(function(){
           if(!$(this).hasClass("active"))
           {
                countitem++;
           }                                  
    });
    $('.num').html('+'+countitem+'<span>more</span>');
});




