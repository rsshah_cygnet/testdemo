<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('notifications', function (Blueprint $table) {
                $table->dropForeign('notifications_user_id_foreign');
                $table->dropColumn('user_id');
                $table->integer('from_user_id')->unsigned()->index()->after('message');
                $table->integer('to_user_id')->unsigned()->index()->after('from_user_id');
                $table->integer('notification_type_id')->unsigned()->index()->after('to_user_id');
                $table->unsignedTinyInteger('is_read_admin')->default(0)->comment('0=unread , 1=read')->after('is_read');
                $table->unsignedTinyInteger('is_for_admin')->default(0)->comment('0=no , 1=yes')->after('is_read_admin');

                $table->foreign('from_user_id')
                        ->references('id')
                        ->on('front_user')
                        ->onDelete('cascade');

                $table->foreign('to_user_id')
                        ->references('id')
                        ->on('front_user')
                        ->onDelete('cascade');  
                        
                $table->foreign('notification_type_id')
                        ->references('id')
                        ->on('notification_type')
                        ->onDelete('cascade');  
                                  


         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications', function (Blueprint $table) {
                 $table->integer('user_id')->unsigned();

                   $table->foreign('user_id')
                        ->references('id')
                        ->on('users')
                        ->onDelete('cascade');  

                 $table->dropForeign('notifications_from_user_id_foreign');
                 $table->dropForeign('notifications_to_user_id_foreign');
                 $table->dropForeign('notifications_notification_type_id_foreign');
                  $table->dropColumn('is_read_admin');
                   $table->dropColumn('is_for_admin');
                    $table->dropColumn('from_user_id');
                     $table->dropColumn('to_user_id');
                      $table->dropColumn('notification_type_id');
         });

    }
}
