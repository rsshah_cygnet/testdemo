<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCountryIdFromCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('city', function (Blueprint $table) {
            /*
             * Drop Foreign key
             */
            $table->dropForeign('city_country_id_foreign');
            $table->dropColumn('country_id');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::table('city', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->index();
            $table->foreign('country_id')
                ->references('id')
                ->on('country')
                ->onDelete('cascade');

           
        });
        
    }
}
