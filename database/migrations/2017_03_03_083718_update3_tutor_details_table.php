<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update3TutorDetailsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('tutor_details', function (Blueprint $table) {
			$table->enum('is_completed', [0, 1])->after('test_date_time')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('tutor_details', function (Blueprint $table) {
			$table->dropColumn('is_completed');
		});
	}
}
