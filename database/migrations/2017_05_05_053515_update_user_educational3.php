<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserEducational3 extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement("ALTER TABLE `user_educational` CHANGE `punctuality` `punctuality` FLOAT(11) NOT NULL DEFAULT '0'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `user_educational` CHANGE `punctuality` `punctuality` INT NOT NULL DEFAULT '0'");
    }
}
