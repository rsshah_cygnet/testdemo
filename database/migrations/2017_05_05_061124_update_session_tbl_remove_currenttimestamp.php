<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionTblRemoveCurrenttimestamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement("ALTER TABLE `session` CHANGE `reserved_date_time` `reserved_date_time` TIMESTAMP NULL DEFAULT NULL");
         DB::statement("ALTER TABLE `session` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `session` CHANGE `reserved_date_time` `reserved_date_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP");
        DB::statement("ALTER TABLE `session` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP");
    }
}
