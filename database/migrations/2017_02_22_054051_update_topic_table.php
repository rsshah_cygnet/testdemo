<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('topic', function (Blueprint $table) {
           
            $table->unsignedTinyInteger('grade_type')->default(0)->comment('0=lower , 1=higher')->after('level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic', function (Blueprint $table) {
            // Drop Colomn
            $table->dropColumn('grade_type');
           
        });
    }
}
