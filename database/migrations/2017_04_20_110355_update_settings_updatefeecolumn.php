<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSettingsUpdatefeecolumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement("ALTER TABLE `settings` CHANGE `admin_commission_for_lower_level` `admin_commission_for_lower_level` FLOAT(10) UNSIGNED NOT NULL COMMENT '%'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `settings` CHANGE `admin_commission_for_lower_level` `admin_commission_for_lower_level` INT UNSIGNED NOT NULL COMMENT '%'");
    }
}
