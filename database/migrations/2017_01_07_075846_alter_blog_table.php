<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('blogs', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('publish_datetime');
            $table->longText('content');
            //$table->integer("domain_id")->unsigned()->index();
            $table->string('meta_title');
            $table->string('cannonical_link');
            $table->text('meta_description');
            $table->text('meta_keywords');
            $table->enum('status', ['Published', 'Draft', 'InActive']);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('blog_tags', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->enum('status', ['Active', 'InActive']);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('blog_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('status', ['Active', 'InActive']);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('blog_map_categories', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('blog_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();
        });

        Schema::create('blog_map_tags', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('blog_id')->unsigned()->index();
            $table->integer('tag_id')->unsigned()->index();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blogs');
        Schema::drop('blog_tags');
        Schema::drop('blog_categories');
        Schema::drop('blog_map_categories');
        Schema::drop('blog_map_tags');
    }
}
