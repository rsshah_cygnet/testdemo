<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTutorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_details', function (Blueprint $table) {
             $table->integer('no_of_total_answers')->after('no_of_total_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_details', function (Blueprint $table) {
             $table->dropColumn('no_of_total_answers');
        });
    }
}
