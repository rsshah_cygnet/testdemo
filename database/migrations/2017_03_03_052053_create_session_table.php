<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tutor_id')->unsigned()->index()->nullable();
            $table->integer('tutee_id')->unsigned()->index()->nullable();
            $table->integer('subject_id')->unsigned()->index()->nullable();
            $table->integer('topic_id')->unsigned()->index()->nullable();

            $table->timestamp('reserved_date_time')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('accepted_date_time')->default('0000-00-00 00:00');
            $table->timestamp('scheduled_date_time')->default('0000-00-00 00:00');
            $table->timestamp('tutee_refund_date_time')->default('0000-00-00 00:00');

            $table->float('ratings_given_by_tutee', 8, 2)->default(0);
            $table->float('session_fee', 8, 2)->default(0);
            $table->float('admin_commission')->default(0);
            $table->float('tutor_earning', 8, 2)->default(0);
            $table->float('penalty_amount', 8, 2)->default(0);
            $table->float('refund_amount', 8, 2)->default(0);
            
            $table->unsignedTinyInteger('status')->default(2)->comment('1=successful, 2=Upcoming, 3=cancelled by Tutor, 4=cancelled by Tutee');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
            $table->integer('modified_by')->unsigned()->index()->default(0);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('session');
    }
}
