<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Update5TutorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //A
        Schema::table('tutor_details', function (Blueprint $table) {
            DB::statement("ALTER TABLE `tutor_details` CHANGE `test_date_time` `test_date_time` TIMESTAMP NULL DEFAULT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_details', function (Blueprint $table) {
           DB::statement("ALTER TABLE `tutor_details` CHANGE `test_date_time` `test_date_time` TIMESTAMP NULL DEFAULT NULL;");
        });
    }
}
