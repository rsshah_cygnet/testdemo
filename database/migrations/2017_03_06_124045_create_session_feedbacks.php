<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSessionFeedbacks extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('session_feedbacks', function (Blueprint $table) {
			$table->increments('id');
			$table->longText('report_text');
			$table->integer('posted_by')->unsigned();
			$table->integer('session_id')->unsigned();
			$table->integer('modified_by')->unsigned();
			$table->timestamp('session_end_date_time');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('session_feedbacks');
	}
}
