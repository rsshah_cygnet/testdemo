<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update2SessionTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('session', function (Blueprint $table) {
			$table->integer('punctuality_ratings')->after('tutee_refund_date_time')->unsigned()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('session', function (Blueprint $table) {
			$table->dropColumn('punctuality_ratings');
		});
	}
}
