<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserEducationalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_educational', function (Blueprint $table) {
              $table->integer('qualification_id')->unsigned()->index()->nullable()->after('educational_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('user_educational', function (Blueprint $table) {
              $table->dropColumn('qualification_id');
        });
    }
}
