<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionAddShare extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('session', function (Blueprint $table) {
            $table->unsignedTinyInteger('share_by_tutee')->default(0)->comment('0=no, 1=yes');
            $table->unsignedTinyInteger('share_by_tutor')->default(0)->comment('0=no, 1=yes');
        });   
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('session', function (Blueprint $table) {
            $table->dropColumn('share_by_tutee');
             $table->dropColumn('share_by_tutor');
        });  
    }
}
