<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionCancelledPenaltyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('session_cancelled_penalty', function (Blueprint $table) {
            $table->renameColumn('percentage', 'penalty_percentage');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('session_cancelled_penalty', function (Blueprint $table) {
            $table->renameColumn('penalty_percentage', 'percentage');
         });
    }
}
