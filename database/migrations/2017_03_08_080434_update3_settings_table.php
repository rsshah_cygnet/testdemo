<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update3SettingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('settings', function (Blueprint $table) {
			$table->integer('tutor_availability_range')->after('higher_level_grade_fee')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('settings', function (Blueprint $table) {
			$table->dropColumn('tutor_availability_range');
		});
	}
}
