<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
           // $table->integer('domain_id')->unsigned()->index();
            $table->integer('cmspage_id')->unsigned()->index();
            $table->integer('parent_menu_id')->unsigned()->index()->comment("parent_menu_id = 0 identify the parent menu.");
            $table->enum('status', ['Active', 'InActive']);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            /**
             * Add Foreign/Unique/Index
             */
            $table->foreign('cmspage_id')
                ->references('id')
                ->on('cmspages')
                ->onDelete('cascade');
            
            /*$table->foreign('domain_id')
                ->references('id')
                ->on('domains')
                ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus', function (Blueprint $table) {
            /*
             * Drop Foreign key
             */
            $table->dropForeign('menus_cmspage_id_foreign');
            //$table->dropForeign('menus_domain_id_foreign');
        });
        
        Schema::drop('menus');
    }
}
