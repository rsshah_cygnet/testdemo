<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddScheduledEnumTypeToStatusInBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blogs', function (Blueprint $table) {
            DB::statement("ALTER TABLE `blogs` CHANGE `status` `status` ENUM('Published','Draft','InActive','Scheduled') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blogs', function (Blueprint $table) {
            DB::statement("ALTER TABLE `blogs` CHANGE `status` `status` ENUM('Published','Draft','InActive') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL; ");
        });
    }
}
