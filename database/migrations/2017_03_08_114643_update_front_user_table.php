<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFrontUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
    Schema::table('front_user', function (Blueprint $table) {
              $table->dropForeign('front_user_state_id_foreign');
              $table->dropForeign('front_user_city_id_foreign');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('front_user', function (Blueprint $table) {
        $table->foreign('state_id')
        ->references('id')
        ->on('state')
        ->onDelete('cascade');

        $table->foreign('city_id')
        ->references('id')
        ->on('city')
        ->onDelete('cascade');
        });
    }
}
