<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserReports extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('user_reports', function (Blueprint $table) {
			$table->increments('id');
			$table->text('reported_text');
			$table->integer('user_id')->unsigned()->comment('Currently loggedin user id');
			$table->integer('reported_user_id')->unsigned()->comment('Currently loggedin in user reported for this user id');
			$table->enum('reported_role', [1, 2])->comment('1 for tutor, 2 for tutee');
			$table->integer('modified_by')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('user_reports');
	}
}
