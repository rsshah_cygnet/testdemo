<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TuteeSessionPreference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('tutee_session_preference', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('front_user_id')->unsigned()->index()->nullable();
            $table->integer('curriculum_id')->unsigned()->index()->nullable();
            $table->integer('education_system_id')->unsigned()->index()->nullable();
            $table->integer('level_id')->unsigned()->index()->nullable();
            $table->integer('program_id')->unsigned()->index()->nullable();
            $table->integer('grade_id')->unsigned()->index()->nullable();
            $table->integer('subject_id')->unsigned()->index()->nullable();
            $table->integer('topic_id')->unsigned()->index()->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00.000000')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('tutee_session_preference');
    }
}
