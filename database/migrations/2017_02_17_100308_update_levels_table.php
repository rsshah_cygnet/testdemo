<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('levels', function (Blueprint $table) {
            $table->renameColumn('name', 'levels_name');
             $table->integer('program_id')->unsigned()->index()->nullable()->after('eductional_systems_id');
              $table->integer('subject_id')->unsigned()->index()->nullable()->after('program_id');
              $table->integer('grade_id')->unsigned()->index()->nullable()->after('subject_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levels', function (Blueprint $table) {
            $table->renameColumn('levels_name', 'name');
             $table->dropColumn('program_id');
              $table->dropColumn('subject_id');
              $table->dropColumn('grade_id');
        });
    }
}
