<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('state', function (Blueprint $table) {
              $table->renameColumn('name', 'state_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('state', function (Blueprint $table) {
              $table->renameColumn('state_name', 'name');
        });
    }
}
