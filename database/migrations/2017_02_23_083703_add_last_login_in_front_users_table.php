<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastLoginInFrontUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('front_user', function ($table) {
            $table->timestamp('last_login_date')->nullable();
            $table->timestamp('news_mail_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('front_user', function ($table) {
            $table->dropColumn('last_login_date');
            $table->dropColumn('news_mail_date');
        });
    }
}
