<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update5FrontUserTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('front_user', function (Blueprint $table) {
			$table->integer('session_share_count')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('front_user', function (Blueprint $table) {
			$table->dropColumn('session_share_count');
		});
	}
}
