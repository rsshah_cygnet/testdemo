<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('city', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->integer('country_id')->unsigned()->index();
        $table->integer('state_id')->unsigned()->index();

        
        $table->unsignedTinyInteger('status')->default(1);
        
        $table->foreign('country_id')
        ->references('id')
        ->on('country')
        ->onDelete('cascade');

        $table->foreign('state_id')
        ->references('id')
        ->on('state')
        ->onDelete('cascade');
        
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('city_country_id_foreign');
        $table->dropForeign('city_state_id_foreign');
        Schema::drop('city');
    }
}
