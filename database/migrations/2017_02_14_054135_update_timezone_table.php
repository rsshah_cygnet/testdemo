<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTimezoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timezone', function (Blueprint $table) {
            $table->renameColumn('name', 'timezone_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('timezone', function (Blueprint $table) {
            $table->renameColumn('timezone_name', 'name');
        });
    }
}
