<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update6FrontUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('front_user', function (Blueprint $table) {
            $table->unsignedTinyInteger('gender')->comment('0 - Male, 1 - Female')->default(0)->after('last_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('front_user', function (Blueprint $table) {
            $table->dropColumn('gender');
        });
    }
}
