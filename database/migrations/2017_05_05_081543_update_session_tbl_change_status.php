<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionTblChangeStatus extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          
        
             DB::statement("ALTER TABLE `session` CHANGE `status` `status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '5' COMMENT '1=successful, 2=Upcoming, 3=cancelled by Tutor, 4=cancelled by Tutee, 5 = pending, 6=joined only tutor, 7=none of joined session, 8=cancelled by tutee before accept'");
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          
    }
}
