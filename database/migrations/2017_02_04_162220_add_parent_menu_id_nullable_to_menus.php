<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddParentMenuIdNullableToMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menus', function (Blueprint $table) {
            // Change colomn as nullable
            DB::statement("ALTER TABLE `menus` CHANGE `parent_menu_id` `parent_menu_id` INT(10) UNSIGNED NULL COMMENT 'parent_menu_id = 0 identify the parent menu.';");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus', function (Blueprint $table) {
            // Change colomn as not nullable
            DB::statement("ALTER TABLE `menus` CHANGE `parent_menu_id` `parent_menu_id` INT(10) UNSIGNED NOT NULL COMMENT 'parent_menu_id = 0 identify the parent menu.';");
        });
    }
}
