<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInLevelsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         Schema::table('levels', function (Blueprint $table) {
           
            $table->integer('eductional_systems_id')->nullable()->change();
            $table->integer('curriculum_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
         Schema::table('levels', function (Blueprint $table) {
           
            $table->integer('eductional_systems_id')->change();
            $table->integer('curriculum_id')->change();
        });
    }
}
