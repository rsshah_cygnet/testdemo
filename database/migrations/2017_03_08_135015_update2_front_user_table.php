<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2FrontUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::statement('ALTER TABLE `front_user` CHANGE `status` `status` TINYINT(3) UNSIGNED NOT NULL DEFAULT "2" COMMENT "0-inactive, 1-active&proper registerred, 2-inproper register"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     
    }
}
