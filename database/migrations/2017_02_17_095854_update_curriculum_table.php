<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateCurriculumTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('curriculum', function (Blueprint $table) {
			$table->renameColumn('name', 'curriculum_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('curriculum', function (Blueprint $table) {
			$table->renameColumn('curriculum_name', 'name');
		});

	}
}
