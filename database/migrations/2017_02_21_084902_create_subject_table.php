<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject_name');
            $table->integer('curriculum_id')->unsigned()->index()->nullable();
            $table->integer('eductional_systems_id')->unsigned()->index()->nullable();
           // $table->integer('subject_id')->unsigned()->index()->nullable();
            $table->integer('program_id')->unsigned()->index()->nullable();
            $table->integer('grade_id')->unsigned()->index()->nullable();
            $table->integer('level_id')->unsigned()->index()->nullable();
            
            $table->unsignedTinyInteger('status')->default(1)->comment('0=inactive , 1=active');
            $table->integer('modified_by')->unsigned()->index()->default(0);
            $table->timestamps();

            $table->foreign('curriculum_id')
                ->references('id')
                ->on('curriculum')
                ->onDelete('cascade');

             $table->foreign('eductional_systems_id')
                ->references('id')
                ->on('eductional_systems')
                ->onDelete('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subject', function (Blueprint $table) {
            /*
             * Drop Foreign key
             */
            $table->dropForeign('subject_curriculum_id_foreign');
            $table->dropForeign('subject_eductional_systems_id_foreign');
        });
        Schema::drop('subject');
    }
}
