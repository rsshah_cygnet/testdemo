<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('topic_id')->unsigned()->index()->comment('reference to topic table');
        $table->longText('question')->nullable();
        $table->string('question_image')->nullable();
        $table->longText('option1')->nullable();
        $table->longText('option2')->nullable();
        $table->longText('option3')->nullable();
        $table->longText('option4')->nullable();
        $table->unsignedTinyInteger('correct_answer')->comment('1 - option1, 2 - option2, 3- option3, 4 - option4');        
        $table->unsignedTinyInteger('status')->default(1);
        $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('updated_at')->default('0000-00-00 00:00')->nullable();
        $table->integer('modified_by')->unsigned()->nullable();
    
    $table->softDeletes();

    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question');
    }
}
