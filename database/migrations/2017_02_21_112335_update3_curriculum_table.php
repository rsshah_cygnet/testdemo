<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update3CurriculumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('curriculum', function (Blueprint $table) {
        $table->unsignedTinyInteger('display_in_program')->default(1)->comment('0=no , 1=yes')->after('lower_level_fee');
        $table->unsignedTinyInteger('display_in_level')->default(1)->comment('0=no , 1=yes')->after('display_in_program');
        $table->unsignedTinyInteger('display_in_eductional_systems')->default(1)->comment('0=no , 1=yes')->after('display_in_level');
    });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('curriculum', function (Blueprint $table) {
        // Drop Colomn
        $table->dropColumn('display_in_program');
        $table->dropColumn('display_in_level');
        $table->dropColumn('display_in_eductional_systems');
    });
   }
}
