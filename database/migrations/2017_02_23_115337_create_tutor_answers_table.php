<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTutorAnswersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('tutor_answer', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('tutor_detail_id')->unsigned()->index()->comment('reference to tutor_detail table');
			$table->integer('question_id')->unsigned()->index()->comment('reference to question table');
			$table->unsignedTinyInteger('answer')->comment('1 - option1, 2 - option2, 3- option3, 4 - option4')->nullable;
			$table->unsignedTinyInteger('is_correct')->comment('0 - No , 1- Yes')->default(0)->nullable();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default('0000-00-00 00:00')->nullable();
			$table->integer('modified_by')->unsigned()->nullable();

			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('tutor_answer');
	}
}
