<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update2TopicTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('topic', function (Blueprint $table) {
			$table->integer('number_of_questions_per_topic')->after('grade_type');
			$table->integer('number_of_minutes_per_questions')->after('grade_type');
			$table->integer('passing_percentage')->after('grade_type');
			$table->integer('min_questions_in_test')->after('grade_type');
			$table->integer('reappearing_test')->after('grade_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('topic', function (Blueprint $table) {
			$table->dropColumn('number_of_questions_per_topic');
			$table->dropColumn('number_of_minutes_per_questions');
			$table->dropColumn('passing_percentage');
			$table->dropColumn('min_questions_in_test');
			$table->dropColumn('reappearing_test');
		});
	}
}
