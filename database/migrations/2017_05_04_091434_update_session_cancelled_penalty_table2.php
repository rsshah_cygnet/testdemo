<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionCancelledPenaltyTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement("ALTER TABLE `session_cancelled_penalty` CHANGE `penalty_percentage` `penalty_percentage` FLOAT(11) NULL DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `session_cancelled_penalty` CHANGE `penalty_percentage` `penalty_percentage` INT NULL DEFAULT NULL");
    }
}
