<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionAddReviewField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('session', function (Blueprint $table) {
             $table->text('session_review')->after('objective');
             $table->timestamp('end_date_time')->after('scheduled_date_time')->default('0000-00-00 00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('session', function (Blueprint $table) {
             $table->dropColumn('session_review');
              $table->dropColumn('end_date_time');
        });
    }
}
