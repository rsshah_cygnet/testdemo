<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEducationalSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eductional_systems', function (Blueprint $table) {
             $table->integer('levels_id')->unsigned()->index()->nullable()->after('curriculum_id');
              $table->integer('program_id')->unsigned()->index()->nullable()->after('levels_id');
              $table->integer('subject_id')->unsigned()->index()->nullable()->after('program_id');
              $table->integer('grade_id')->unsigned()->index()->nullable()->after('subject_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('eductional_systems', function (Blueprint $table) {
              $table->dropColumn('program_id');
              $table->dropColumn('subject_id');
              $table->dropColumn('grade_id');
               $table->dropColumn('levels_id');
        });
    }
}
