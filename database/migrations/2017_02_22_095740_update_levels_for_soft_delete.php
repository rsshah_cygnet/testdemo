<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLevelsForSoftDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('levels', function (Blueprint $table) {
             $table->softDeletes();
            $table->dropForeign('levels_eductional_systems_id_foreign');
             $table->dropForeign('levels_curriculum_id_foreign');

              /*$table->integer('eductional_systems_id')->nullable()->change();
            $table->integer('curriculum_id')->nullable()->change();*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('levels', function (Blueprint $table) {
                $table->dropSoftDeletes();

       $table->foreign('eductional_systems_id')
            ->references('id')->on('eductional_systems');

        $table->foreign('curriculum_id')
            ->references('id')->on('curriculum');
         });
    }
}
