<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionAddRatedByTutee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('session', function (Blueprint $table) {
            $table->unsignedTinyInteger('rated_by_tutee')->default(0)->comment('0=no, 1=yes');
         });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('session', function (Blueprint $table) {
            $table->dropColumn('rated_by_tutee');
         });  
    }
}
