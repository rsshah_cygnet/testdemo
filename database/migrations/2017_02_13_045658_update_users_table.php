<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
             $table->string('last_name')->after('first_name');
             $table->integer('contact_no')->after('email');
             $table->integer('country_id')->unsigned();
             $table->integer('city_id')->unsigned();
             $table->integer('modified_by')->unsigned()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('users', function (Blueprint $table) {
             $table->dropColumn('last_name');
             $table->dropColumn('contact_no');
             $table->dropColumn('country_id');
             $table->dropColumn('city_id');
             $table->dropColumn('modified_by');
        });
    }
}
