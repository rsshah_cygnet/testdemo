<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update6SettingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('settings', function (Blueprint $table) {
			$table->renameColumn('admin_commission', 'admin_commission_for_higher_level')->unsigned()->after('number_of_questions_per_topic')->comment('%');
			$table->integer('admin_commission_for_lower_level')->unsigned()->after('number_of_questions_per_topic')->comment('%');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('settings', function (Blueprint $table) {
			$table->renameColumn('admin_commission_for_higher_level', 'admin_commission');
			$table->dropColumn('admin_commission_for_lower_level');
		});
	}
}
