<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTestimonialTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('testimonials', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->enum('status', ['0', '1'])->default('0')->comment('0 = Inactive, 1 = Active ');
			$table->integer('created_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('testimonials');
	}
}
