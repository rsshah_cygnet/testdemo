<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateSettingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('settings', function (Blueprint $table) {
			$table->integer('number_of_questions_per_topic')->after('explanation4');
			$table->integer('number_of_minutes_per_questions')->after('explanation4');
			$table->integer('passing_percentage')->after('explanation4');
			$table->integer('min_questions_in_test')->after('explanation4');
			$table->integer('reappearing_test')->after('explanation4');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('settings', function (Blueprint $table) {
			$table->dropColumn('number_of_questions_per_topic');
			$table->dropColumn('number_of_minutes_per_questions');
			$table->dropColumn('passing_percentage');
			$table->dropColumn('min_questions_in_test');
			$table->dropColumn('reappearing_test');

		});
	}
}
