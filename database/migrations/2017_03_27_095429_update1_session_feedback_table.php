<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update1SessionFeedbackTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('session_feedbacks', function (Blueprint $table) {
			$table->integer('posted_for')->unsigned()->after('posted_by')->comment('Tutor Id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('session_feedbacks', function (Blueprint $table) {
			$table->dropColumn('posted_for');
		});
	}
}
