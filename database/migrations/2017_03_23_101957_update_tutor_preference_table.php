<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTutorPreferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::table('tutor_preference', function (Blueprint $table) {
              $table->renameColumn('stat_time', 'start_time');
              $table->unsignedTinyInteger('reference')->default(0)->comment(0 - 'available for everyday', 1 - 'for specific day')->after('end_time');
              $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_preference', function (Blueprint $table) {
              $table->renameColumn('start_time', 'stat_time');
              $table->dropColumn('reference');
              $table->dropColumn('deleted_at');

        });
    }
}
