<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmspagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cmspages', function (Blueprint $table) {    
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('page_slug')->unique();
            $table->text('description')->nullable();
            //$table->integer('domain_id')->unsigned()->index();
            $table->string('cannonical_link');
            $table->string('seo_title')->nullable();
            $table->string('seo_keyword')->nullable();
            $table->string('seo_description')->nullable();
            $table->enum('is_active', ['Active', 'Inactive']);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            
            /**
             * Add Foreign/Unique/Index
             */
            /*$table->foreign('domain_id')
                ->references('id')
                ->on('domains')
                ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('cmspages', function (Blueprint $table) {
            $table->dropForeign('cmspages_domain_id_foreign');
        });*/
        
        Schema::drop('cmspages');
    }
}
