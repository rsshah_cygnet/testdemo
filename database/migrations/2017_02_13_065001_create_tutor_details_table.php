<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('front_user_id')->unsigned()->index()->nullable();
            $table->integer('curriculum_id')->unsigned()->index()->nullable();
            $table->integer('education_system_id')->unsigned()->index()->nullable();
            $table->integer('level_id')->unsigned()->index()->nullable();
            $table->integer('program_id')->unsigned()->index()->nullable();
            $table->integer('grade_id')->unsigned()->index()->nullable();
            $table->integer('subject_id')->unsigned()->index()->nullable();
            $table->integer('topic_id')->unsigned()->index()->nullable();
            $table->enum('passed_status', ['1', '0'])->default('0')->comment('0 = inactive, 1 = active');
            $table->integer('no_of_total_questions')->default('0');
            $table->integer('no_of_correct_answers')->default('0');
            $table->integer('no_of_minutes')->default('0');
            $table->integer('no_of_marks')->default('0');
            $table->integer('no_of_percentage')->default('0');
            $table->enum('appeared', ['1', '0'])->default('0')->comment('0 = no, 1 = yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutor_details');
    }
}
