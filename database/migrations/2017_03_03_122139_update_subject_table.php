<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateSubjectTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('subject', function (Blueprint $table) {
			$table->integer('number_of_questions_per_topic')->after('level_id');
			$table->integer('number_of_minutes_per_questions')->after('level_id');
			$table->integer('passing_percentage')->after('level_id');
			$table->integer('min_questions_in_test')->after('level_id');
			$table->integer('reappearing_test')->after('level_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('subject', function (Blueprint $table) {
			$table->dropColumn('number_of_questions_per_topic');
			$table->dropColumn('number_of_minutes_per_questions');
			$table->dropColumn('passing_percentage');
			$table->dropColumn('min_questions_in_test');
			$table->dropColumn('reappearing_test');
		});
	}
}
