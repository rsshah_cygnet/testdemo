<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('message');
            $table->integer('user_id')->unsigned();
            $table->enum('type', ['success','warning','danger']);
            $table->unsignedTinyInteger('is_read')->default(0);
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

           Schema::table('notifications', function (Blueprint $table) {
            /*
             * Drop Foreign key
             */
            $table->dropForeign('notifications_user_id_foreign');
        });
        
        Schema::drop('notifications');
    }
}
