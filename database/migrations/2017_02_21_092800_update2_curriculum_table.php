<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2CurriculumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

     Schema::table('curriculum', function (Blueprint $table) {

        $table->integer('higher_level_fee')->unsigned()->nullable()->after('curriculum_name');
        $table->integer('lower_level_fee')->unsigned()->nullable()->after('higher_level_fee');
    });

 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('curriculum', function (Blueprint $table) {
            // Drop Colomn
            $table->dropColumn('higher_level_fee');
            $table->dropColumn('lower_level_fee');
        });
    }
}
