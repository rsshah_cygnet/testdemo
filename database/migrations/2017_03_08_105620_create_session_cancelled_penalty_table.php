<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionCancelledPenaltyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_cancelled_penalty', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('from')->nullable();
                $table->integer('to')->nullable();
                $table->integer('within')->nullable();
                $table->integer('percentage')->nullable();
                $table->unsignedTinyInteger('type')->default(1)->comment('1=between, 0=within');
                $table->unsignedTinyInteger('status')->default(1)->comment('1=active, 0=inactive');
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default('0000-00-00 00:00');
                $table->integer('modified_by')->unsigned()->index()->default(0);
                $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('session_cancelled_penalty');
    }
}
