<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2TutorDetailsTable extends Migration
{/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_details', function (Blueprint $table) {
              $table->timestamp('test_date_time')->after('appeared');
              
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_details', function (Blueprint $table) {
             $table->dropColumn('test_date_time');
        });
    }
}
