<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('grade', function (Blueprint $table) {
            
            $table->unsignedTinyInteger('grade_type')->default(0)->comment('0=lower , 1=higher')->after('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('grade', function (Blueprint $table) {
              $table->dropColumn('grade_type');
        });
    }
}
