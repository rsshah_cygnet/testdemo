<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


Schema::create('front_user', function (Blueprint $table) {
    $table->increments('id');
    $table->unsignedTinyInteger('role')->comment('1 - tutor, 2 - tutee, 3- both');
    $table->string('first_name');
    $table->string('last_name');
    $table->string('username');
    $table->string('email')->unique();
    $table->string('password', 60)->nullable();
    $table->integer('country_id')->unsigned()->index();
    $table->integer('state_id')->unsigned()->index();
    $table->integer('city_id')->unsigned()->index();
    $table->longText('nationality')->nullable();
    $table->integer('lang_id')->unsigned()->index()->comment('reference to language table');
    $table->string("photo");
    $table->date('dob');
    $table->integer('timezone_id')->unsigned()->index();
    $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
    $table->timestamp('updated_at')->default('0000-00-00 00:00');
    $table->integer('modified_by')->unsigned();
    $table->unsignedTinyInteger('status')->default(1);
    $table->softDeletes();

     $table->foreign('country_id')
        ->references('id')
        ->on('country')
        ->onDelete('cascade');


     $table->foreign('state_id')
        ->references('id')
        ->on('state')
        ->onDelete('cascade');

        $table->foreign('city_id')
        ->references('id')
        ->on('city')
        ->onDelete('cascade');

         $table->foreign('lang_id')
        ->references('id')
        ->on('language')
        ->onDelete('cascade');

        $table->foreign('timezone_id')
        ->references('id')
        ->on('timezone')
        ->onDelete('cascade');
});
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('front_user', function (Blueprint $table) {
            /*
             * Drop Foreign key
             */
            $table->dropForeign('front_user_country_id_foreign');
            $table->dropForeign('front_user_state_id_foreign');
            $table->dropForeign('front_user_city_id_foreign');
            $table->dropForeign('front_user_lang_id_foreign');
            $table->dropForeign('front_user_timezone_id_foreign');
        });

        Schema::drop('front_user');
    }
}
