<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update2SettingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('settings', function (Blueprint $table) {
			$table->integer('higher_level_grade_fee')->after('number_of_questions_per_topic')->comment('in $');
			$table->integer('lower_level_grade_fee')->after('number_of_questions_per_topic')->comment('in $');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('settings', function (Blueprint $table) {
			$table->dropColumn('higher_level_grade_fee');
			$table->dropColumn('lower_level_grade_fee');
		});
	}
}
