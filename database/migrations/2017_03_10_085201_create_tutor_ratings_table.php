<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTutorRatingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('tutor_ratings', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('tutor_id')->unsigned()->index()->nullable();
			$table->integer('tutee_id')->unsigned()->index()->nullable();
			$table->integer('rating')->unsigned()->index()->nullable();
			$table->text('review');
			$table->integer('modified_by')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('tutor_ratings');
	}
}
