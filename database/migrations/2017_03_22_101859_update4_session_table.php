<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update4SessionTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('session', function (Blueprint $table) {
			$table->timestamp('fee_credited_date_time')->default('0000-00-00 00:00')->after('scheduled_date_time');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('session', function (Blueprint $table) {
			$table->dropColumn('fee_credited_date_time');
		});
	}
}
