<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorPreferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_preference', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutor_id')->unsigned()->index();
            $table->date('start_date');
            $table->date('end_date');
            $table->time('stat_time');
            $table->time('end_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutor_preference');
    }
}
