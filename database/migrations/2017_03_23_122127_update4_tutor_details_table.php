<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update4TutorDetailsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('tutor_details', function (Blueprint $table) {
//ALTER TABLE  `tutor_details` CHANGE  `no_of_percentage`  `no_of_percentage` FLOAT( 11 ) NOT NULL DEFAULT  '0';
			//ALTER TABLE  `tutor_details` CHANGE  `no_of_percentage`  `no_of_percentage` INT( 11 ) NOT NULL DEFAULT  '0';
			DB::statement("ALTER TABLE  `tutor_details` CHANGE  `no_of_percentage`  `no_of_percentage` FLOAT( 11 ) NOT NULL DEFAULT  '0';");
			//$table->float('no_of_percentage')->default(0)->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('tutor_details', function (Blueprint $table) {
			//$table->integer('no_of_percentage')->default(0)->change();
			DB::statement("ALTER TABLE  `tutor_details` CHANGE  `no_of_percentage`  `no_of_percentage` INT( 11 ) NOT NULL DEFAULT  '0';");
		});
	}
}
