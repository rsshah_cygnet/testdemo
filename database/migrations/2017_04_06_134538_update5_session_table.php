<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Update5SessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('session', function (Blueprint $table) {
        $table->integer('meeting_id')->after('objective')->unsigned()->nullable();
        $table->text('tutor_session_url')->after('meeting_id')->nullable();
        $table->text('tutee_session_url')->after('tutor_session_url')->nullable();
        $table->text('record_url')->after('tutee_session_url')->nullable();
        $table->unsignedTinyInteger('is_get_record_url')->comment('0 - no, 1- yes')->default('0')->after('record_url');
         
        DB::statement("ALTER TABLE `session` CHANGE `accepted_date_time` `accepted_date_time` TIMESTAMP NULL DEFAULT NULL");
        DB::statement("ALTER TABLE `session` CHANGE `scheduled_date_time` `scheduled_date_time` TIMESTAMP NULL DEFAULT NULL");
        DB::statement("ALTER TABLE `session` CHANGE `end_date_time` `end_date_time` TIMESTAMP NULL DEFAULT NULL");
        DB::statement("ALTER TABLE `session` CHANGE `fee_credited_date_time` `fee_credited_date_time` TIMESTAMP NULL DEFAULT NULL");
        DB::statement("ALTER TABLE `session` CHANGE `cancelled_date_time` `cancelled_date_time` TIMESTAMP NULL DEFAULT NULL");
        DB::statement("ALTER TABLE `session` CHANGE `tutee_refund_date_time` `tutee_refund_date_time` TIMESTAMP NULL DEFAULT NULL");
        DB::statement("ALTER TABLE `session` CHANGE `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL");
        DB::statement("ALTER TABLE `session` CHANGE `conducted_date_time` `conducted_date_time` TIMESTAMP NULL DEFAULT NULL");
        DB::statement("ALTER TABLE `session` CHANGE `status` `status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '5' COMMENT '1=successful, 2=Upcoming, 3=cancelled by Tutor, 4=cancelled by Tutee, 5 = pending, 6=joined only tutor, 7=none of joined session';");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('session', function (Blueprint $table) {
            $table->dropColumn('meeting_id');
            $table->dropColumn('tutor_session_url');
            $table->dropColumn('tutee_session_url');
            $table->dropColumn('record_url');
            $table->dropColumn('is_get_record_url');
        });
    }
}
