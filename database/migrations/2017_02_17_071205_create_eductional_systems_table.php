<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEductionalSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eductional_systems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curriculum_id')->unsigned()->index();
            $table->string('name');
            $table->unsignedTinyInteger('status')->default(1)->comment('0=inactive , 1=active');
            $table->integer('modified_by')->unsigned()->index()->default(0);
            $table->timestamps();

            $table->foreign('curriculum_id')
                ->references('id')
                ->on('curriculum')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('eductional_systems', function (Blueprint $table) {
            /*
             * Drop Foreign key
             */
            $table->dropForeign('eductional_systems_curriculum_id_foreign');
        });
        Schema::drop('eductional_systems');
    }
}
