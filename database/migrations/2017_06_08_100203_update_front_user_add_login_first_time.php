<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFrontUserAddLoginFirstTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('front_user', function (Blueprint $table) {
            $table->unsignedTinyInteger('login_first_time')->comment('0 - yes, 1 - no')->default(0)->after('sign_with');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('front_user', function (Blueprint $table) {
            $table->dropColumn('login_first_time');
        });
    }
}
