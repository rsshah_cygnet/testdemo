<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TuteeBasicPreference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('tutee_basic_preference', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('front_user_id')->unsigned()->index();
            $table->string('lang_id')->nullable(); //we will store comma separated value
            $table->integer('from_rating')->unsigned()->index()->nullable();
            $table->integer('to_rating')->unsigned()->index()->nullable();
            $table->integer('from_punctual_rating')->unsigned()->index()->nullable();
            $table->integer('to_punctual_rating')->unsigned()->index()->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00.000000')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('tutee_basic_preference');
    }
}
