<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Update5SettingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('settings', function (Blueprint $table) {
			$table->float('admin_commission', 5, 2)->after('no_of_mark_per_question')->comment('(%)');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('settings', function (Blueprint $table) {
			$table->dropColumn('admin_commission');
		});
	}
}
