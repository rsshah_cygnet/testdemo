<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curriculum_id')->unsigned()->index();
            $table->integer('eductional_systems_id')->unsigned()->index();
            $table->string('name');
            $table->unsignedTinyInteger('status')->default(1)->comment('0=inactive , 1=active');
            $table->integer('modified_by')->unsigned()->index()->default(0);
            $table->timestamps();

            $table->foreign('curriculum_id')
                ->references('id')
                ->on('curriculum')
                ->onDelete('cascade');

             $table->foreign('eductional_systems_id')
                ->references('id')
                ->on('eductional_systems')
                ->onDelete('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('levels', function (Blueprint $table) {
            /*
             * Drop Foreign key
             */
            $table->dropForeign('levels_curriculum_id_foreign');
            $table->dropForeign('levels_eductional_systems_id_foreign');
        });
        Schema::drop('levels');
    }
}
