<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEducationalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_educational', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('front_user_id')->unsigned()->index()->nullable();
            $table->text('educational_details');
            $table->integer('curriculum_id')->unsigned()->index()->nullable();
            $table->integer('education_system_id')->unsigned()->index()->nullable();
            $table->integer('level_id')->unsigned()->index()->nullable();
            $table->integer('program_id')->unsigned()->index()->nullable();
            $table->integer('grade_id')->unsigned()->index()->nullable();
            $table->integer('subject_id')->unsigned()->index()->nullable();
            $table->integer('topic_id')->unsigned()->index()->nullable();
            $table->string('paypal_registered_email')->unique();
            $table->text('occupation_details');
            $table->integer('modified_by')->default('0');
            $table->integer('rating')->default('0');
            $table->integer('punctuality')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_educational');
    }
}
