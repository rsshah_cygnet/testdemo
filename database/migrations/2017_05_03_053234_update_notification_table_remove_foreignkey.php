<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotificationTableRemoveForeignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('notifications', function (Blueprint $table) {
                $table->dropForeign('notifications_from_user_id_foreign');
                 $table->dropForeign('notifications_to_user_id_foreign');
                 $table->dropForeign('notifications_notification_type_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('notifications', function (Blueprint $table) {
             $table->foreign('from_user_id')
                        ->references('id')
                        ->on('front_user')
                        ->onDelete('cascade');

                $table->foreign('to_user_id')
                        ->references('id')
                        ->on('front_user')
                        ->onDelete('cascade');  
                        
                $table->foreign('notification_type_id')
                        ->references('id')
                        ->on('notification_type')
                        ->onDelete('cascade');  
        });
    }
}
