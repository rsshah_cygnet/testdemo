<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionTable8 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('session', function (Blueprint $table) {
             $table->renameColumn('session_review', 'session_feedback_tutee');
             $table->text('session_feedback_tutor')->after('objective');
              $table->unsignedTinyInteger('feedback_by_tutee')->default(0)->comment('0=no, 1=yes');
            $table->unsignedTinyInteger('feedback_by_tutor')->default(0)->comment('0=no, 1=yes');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('session', function (Blueprint $table) {
             $table->dropColumn('session_feedback_tutor');
              $table->renameColumn('session_feedback_tutee', 'session_review');
               $table->dropColumn('feedback_by_tutee');
             $table->dropColumn('feedback_by_tutor');
        });
    }
}
