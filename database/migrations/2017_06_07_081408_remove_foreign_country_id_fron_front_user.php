<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveForeignCountryIdFronFrontUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('front_user', function (Blueprint $table) {
                $table->dropForeign('front_user_country_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('front_user', function (Blueprint $table) {
             $table->foreign('country_id')
                        ->references('id')
                        ->on('country')
                        ->onDelete('cascade');

        });
    }
}
