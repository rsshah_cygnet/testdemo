<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update3UserEducationalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_educational', function (Blueprint $table) {
              $table->string('paypal_registered_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('user_educational', function (Blueprint $table) {
              $table->dropColumn('paypal_registered_email');
        });
    }
}
