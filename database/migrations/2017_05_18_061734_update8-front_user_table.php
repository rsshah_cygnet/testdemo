<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update8FrontUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('front_user', function (Blueprint $table) {
            $table->unsignedTinyInteger('sign_with')->comment('0 - With web, 1 - With facebook')->default(0)->after('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('front_user', function (Blueprint $table) {
            $table->dropColumn('sign_with');
        });
    }
}
