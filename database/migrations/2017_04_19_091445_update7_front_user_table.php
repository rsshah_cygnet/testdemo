<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Update7FrontUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
              
            DB::statement("ALTER TABLE `front_user` DROP FOREIGN KEY `front_user_lang_id_foreign`");
               DB::statement("ALTER TABLE `front_user` CHANGE `lang_id` `lang_id` VARCHAR(100) NOT NULL COMMENT 'reference to language table'; ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement("ALTER TABLE `front_user` CHANGE `lang_id` `lang_id` INT(100) UNSIGNED NOT NULL COMMENT 'reference to language table'; ");
        
        Schema::table('front_user', function (Blueprint $table) {
        $table->foreign('lang_id')
        ->references('id')
        ->on('language')
        ->onDelete('cascade');
        });

    }
}
