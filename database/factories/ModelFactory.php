<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Access\User\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt('1234'),
        'status'=> 1,
        'confirmation_code' => str_random(10),
        'confirmed'=> 1,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Cmspage\Cmspage::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'page_slug' => rand(),
        'description' => implode(" ",$faker->paragraphs(10)),
        'seo_title' => $faker->word,
        'seo_keyword'=> implode(" ", $faker->words(3)),
        'seo_description' => implode(" ",$faker->paragraphs(3)),        
        'page_order' => 1,
        'is_active' => 'Active',
    ];
});

$factory->define(App\Models\Jobtypes\Jobtype::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'is_active' => 'Active',
    ];
});

$factory->define(App\Models\Salarytypes\Salarytype::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->numberBetween($min = 1, $max = 1000),
        'type' => 'Daily',
        'is_active' => 'Active',
    ];
});

$factory->define(App\Models\Abusewords\Abuseword::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name, 
        'name' => $faker->name,        
    ];
});
