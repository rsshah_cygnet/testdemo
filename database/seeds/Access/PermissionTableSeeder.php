<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class PermissionTableSeeder
 */
class PermissionTableSeeder extends Seeder {
	public function run() {
		if (env('DB_CONNECTION') == 'mysql') {
			DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		}

		if (env('DB_CONNECTION') == 'mysql') {
			DB::table(config('access.permissions_table'))->truncate();
			DB::table(config('access.permission_role_table'))->truncate();
			DB::table(config('access.permission_user_table'))->truncate();
		} elseif (env('DB_CONNECTION') == 'sqlite') {
			DB::statement('DELETE FROM ' . config('access.permissions_table'));
			DB::statement('DELETE FROM ' . config('access.permission_role_table'));
			DB::statement('DELETE FROM ' . config('access.permission_user_table'));
		} else {
			//For PostgreSQL or anything else
			DB::statement('TRUNCATE TABLE ' . config('access.permissions_table') . ' CASCADE');
			DB::statement('TRUNCATE TABLE ' . config('access.permission_role_table') . ' CASCADE');
			DB::statement('TRUNCATE TABLE ' . config('access.permission_user_table') . ' CASCADE');
		}

		/**
		 * Don't need to assign any permissions to administrator because the all flag is set to true
		 * in RoleTableSeeder.php
		 */

		/**
		 * Misc Access Permissions
		 */
		$permission_model = config('access.permission');
		$viewBackend = new $permission_model;
		$viewBackend->name = 'view-backend';
		$viewBackend->display_name = 'View Backend';
		$viewBackend->system = true;
		$viewBackend->group_id = 1;
		$viewBackend->sort = 1;
		$viewBackend->created_at = Carbon::now();
		$viewBackend->updated_at = Carbon::now();
		$viewBackend->save();

		$permission_model = config('access.permission');
		$viewAccessManagement = new $permission_model;
		$viewAccessManagement->name = 'view-access-management';
		$viewAccessManagement->display_name = 'View Access Management';
		$viewAccessManagement->system = true;
		$viewAccessManagement->group_id = 1;
		$viewAccessManagement->sort = 2;
		$viewAccessManagement->created_at = Carbon::now();
		$viewAccessManagement->updated_at = Carbon::now();
		$viewAccessManagement->save();

		/**
		 * Access Permissions
		 */

		/**
		 * Admin User Permissions
		 */

		//for view user permission go to line no 1703

		$permission_model = config('access.permission');
		$createUsers = new $permission_model;
		$createUsers->name = 'create-users';
		$createUsers->display_name = 'Create Users';
		$createUsers->system = true;
		$createUsers->group_id = 2;
		$createUsers->sort = 3;
		$createUsers->created_at = Carbon::now();
		$createUsers->updated_at = Carbon::now();
		$createUsers->save();

		$permission_model = config('access.permission');
		$editUsers = new $permission_model;
		$editUsers->name = 'edit-users';
		$editUsers->display_name = 'Edit Users';
		$editUsers->system = true;
		$editUsers->group_id = 2;
		$editUsers->sort = 4;
		$editUsers->created_at = Carbon::now();
		$editUsers->updated_at = Carbon::now();
		$editUsers->save();

		$permission_model = config('access.permission');
		$deleteUsers = new $permission_model;
		$deleteUsers->name = 'delete-users';
		$deleteUsers->display_name = 'Delete Users';
		$deleteUsers->system = true;
		$deleteUsers->group_id = 2;
		$deleteUsers->sort = 5;
		$deleteUsers->created_at = Carbon::now();
		$deleteUsers->updated_at = Carbon::now();
		$deleteUsers->save();

		$permission_model = config('access.permission');
		$changeUserPassword = new $permission_model;
		$changeUserPassword->name = 'change-user-password';
		$changeUserPassword->display_name = 'Change User Password';
		$changeUserPassword->system = true;
		$changeUserPassword->group_id = 2;
		$changeUserPassword->sort = 6;
		$changeUserPassword->created_at = Carbon::now();
		$changeUserPassword->updated_at = Carbon::now();
		$changeUserPassword->save();

		$permission_model = config('access.permission');
		$deactivateUser = new $permission_model;
		$deactivateUser->name = 'deactivate-users';
		$deactivateUser->display_name = 'Deactivate Users';
		$deactivateUser->system = true;
		$deactivateUser->group_id = 2;
		$deactivateUser->sort = 7;
		$deactivateUser->created_at = Carbon::now();
		$deactivateUser->updated_at = Carbon::now();
		$deactivateUser->save();

		$permission_model = config('access.permission');
		$reactivateUser = new $permission_model;
		$reactivateUser->name = 'reactivate-users';
		$reactivateUser->display_name = 'Re-Activate Users';
		$reactivateUser->system = true;
		$reactivateUser->group_id = 2;
		$reactivateUser->sort = 8;
		$reactivateUser->created_at = Carbon::now();
		$reactivateUser->updated_at = Carbon::now();
		$reactivateUser->save();

		$permission_model = config('access.permission');
		$undeleteUser = new $permission_model;
		$undeleteUser->name = 'undelete-users';
		$undeleteUser->display_name = 'Restore Users';
		$undeleteUser->system = true;
		$undeleteUser->group_id = 2;
		$undeleteUser->sort = 9;
		$undeleteUser->created_at = Carbon::now();
		$undeleteUser->updated_at = Carbon::now();
		$undeleteUser->save();

		$permission_model = config('access.permission');
		$permanentlyDeleteUser = new $permission_model;
		$permanentlyDeleteUser->name = 'permanently-delete-users';
		$permanentlyDeleteUser->display_name = 'Permanently Delete Users';
		$permanentlyDeleteUser->system = true;
		$permanentlyDeleteUser->group_id = 2;
		$permanentlyDeleteUser->sort = 10;
		$permanentlyDeleteUser->created_at = Carbon::now();
		$permanentlyDeleteUser->updated_at = Carbon::now();
		$permanentlyDeleteUser->save();

		$permission_model = config('access.permission');
		$resendConfirmationEmail = new $permission_model;
		$resendConfirmationEmail->name = 'resend-user-confirmation-email';
		$resendConfirmationEmail->display_name = 'Resend Confirmation E-mail';
		$resendConfirmationEmail->system = true;
		$resendConfirmationEmail->group_id = 2;
		$resendConfirmationEmail->sort = 11;
		$resendConfirmationEmail->created_at = Carbon::now();
		$resendConfirmationEmail->updated_at = Carbon::now();
		$resendConfirmationEmail->save();

		/**
		 * Role Management
		 */

		$permission_model = config('access.permission');
		$createRoles = new $permission_model;
		$createRoles->name = 'view-roles';
		$createRoles->display_name = 'View Roles';
		$createRoles->system = true;
		$createRoles->group_id = 20;
		$createRoles->sort = 12;
		$createRoles->created_at = Carbon::now();
		$createRoles->updated_at = Carbon::now();
		$createRoles->save();

		//for create role permission goto line 1690

		$permission_model = config('access.permission');
		$editRoles = new $permission_model;
		$editRoles->name = 'edit-roles';
		$editRoles->display_name = 'Edit Roles';
		$editRoles->system = true;
		$editRoles->group_id = 20;
		$editRoles->sort = 13;
		$editRoles->created_at = Carbon::now();
		$editRoles->updated_at = Carbon::now();
		$editRoles->save();

		$permission_model = config('access.permission');
		$deleteRoles = new $permission_model;
		$deleteRoles->name = 'delete-roles';
		$deleteRoles->display_name = 'Delete Roles';
		$deleteRoles->system = true;
		$deleteRoles->group_id = 20;
		$deleteRoles->sort = 14;
		$deleteRoles->created_at = Carbon::now();
		$deleteRoles->updated_at = Carbon::now();
		$deleteRoles->save();

		/**
		 * Permission Group
		 */
		$permission_model = config('access.permission');
		$createPermissionGroups = new $permission_model;
		$createPermissionGroups->name = 'create-permission-groups';
		$createPermissionGroups->display_name = 'Create Permission Groups';
		$createPermissionGroups->system = true;
		$createPermissionGroups->group_id = 19;
		$createPermissionGroups->sort = 15;
		$createPermissionGroups->created_at = Carbon::now();
		$createPermissionGroups->updated_at = Carbon::now();
		$createPermissionGroups->save();

		$permission_model = config('access.permission');
		$editPermissionGroups = new $permission_model;
		$editPermissionGroups->name = 'edit-permission-groups';
		$editPermissionGroups->display_name = 'Edit Permission Groups';
		$editPermissionGroups->system = true;
		$editPermissionGroups->group_id = 19;
		$editPermissionGroups->sort = 16;
		$editPermissionGroups->created_at = Carbon::now();
		$editPermissionGroups->updated_at = Carbon::now();
		$editPermissionGroups->save();

		$permission_model = config('access.permission');
		$deletePermissionGroups = new $permission_model;
		$deletePermissionGroups->name = 'delete-permission-groups';
		$deletePermissionGroups->display_name = 'Delete Permission Groups';
		$deletePermissionGroups->system = true;
		$deletePermissionGroups->group_id = 19;
		$deletePermissionGroups->sort = 17;
		$deletePermissionGroups->created_at = Carbon::now();
		$deletePermissionGroups->updated_at = Carbon::now();
		$deletePermissionGroups->save();

		$permission_model = config('access.permission');
		$sortPermissionGroups = new $permission_model;
		$sortPermissionGroups->name = 'sort-permission-groups';
		$sortPermissionGroups->display_name = 'Sort Permission Groups';
		$sortPermissionGroups->system = true;
		$sortPermissionGroups->group_id = 19;
		$sortPermissionGroups->sort = 18;
		$sortPermissionGroups->created_at = Carbon::now();
		$sortPermissionGroups->updated_at = Carbon::now();
		$sortPermissionGroups->save();

		/**
		 * Permission
		 */

		//for view permission go to line no 1722

		$permission_model = config('access.permission');
		$createPermissions = new $permission_model;
		$createPermissions->name = 'create-permissions';
		$createPermissions->display_name = 'Create Permissions';
		$createPermissions->system = true;
		$createPermissions->group_id = 4;
		$createPermissions->sort = 19;
		$createPermissions->created_at = Carbon::now();
		$createPermissions->updated_at = Carbon::now();
		$createPermissions->save();

		$permission_model = config('access.permission');
		$editPermissions = new $permission_model;
		$editPermissions->name = 'edit-permissions';
		$editPermissions->display_name = 'Edit Permissions';
		$editPermissions->system = true;
		$editPermissions->group_id = 4;
		$editPermissions->sort = 20;
		$editPermissions->created_at = Carbon::now();
		$editPermissions->updated_at = Carbon::now();
		$editPermissions->save();

		$permission_model = config('access.permission');
		$deletePermissions = new $permission_model;
		$deletePermissions->name = 'delete-permissions';
		$deletePermissions->display_name = 'Delete Permissions';
		$deletePermissions->system = true;
		$deletePermissions->group_id = 4;
		$deletePermissions->sort = 21;
		$deletePermissions->created_at = Carbon::now();
		$deletePermissions->updated_at = Carbon::now();
		$deletePermissions->save();

		/**
		 * CMS Pages Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-cms-pages';
		$permissions->display_name = 'View CMS Pages';
		$permissions->system = true;
		$permissions->group_id = 5;
		$permissions->sort = 22;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-cms-pages';
		$permissions->display_name = 'Create CMS Pages';
		$permissions->system = true;
		$permissions->group_id = 5;
		$permissions->sort = 23;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-cms-pages';
		$permissions->display_name = 'Edit CMS Pages';
		$permissions->system = true;
		$permissions->group_id = 5;
		$permissions->sort = 24;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-cms-pages';
		$permissions->display_name = 'Delete CMS Pages';
		$permissions->system = true;
		$permissions->group_id = 5;
		$permissions->sort = 25;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Email Templates
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-emailtemplate';
		$permissions->display_name = 'View Email Template';
		$permissions->system = true;
		$permissions->group_id = 7;
		$permissions->sort = 26;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-emailtemplate';
		$permissions->display_name = 'Create Email Template';
		$permissions->system = true;
		$permissions->group_id = 7;
		$permissions->sort = 27;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-emailtemplate';
		$permissions->display_name = 'Edit Email Template';
		$permissions->system = true;
		$permissions->group_id = 7;
		$permissions->sort = 28;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-emailtemplate';
		$permissions->display_name = 'Delete Email Template';
		$permissions->system = true;
		$permissions->group_id = 7;
		$permissions->sort = 29;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Settings
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-settings';
		$permissions->display_name = 'Edit Setting';
		$permissions->system = true;
		$permissions->group_id = 8;
		$permissions->sort = 30;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/* Umang Start From here */
		/**
		 * Front User Permissions
		 */

		$permission_model = config('access.permission');
		$createUsers = new $permission_model;
		$createUsers->name = 'delete-front-users';
		$createUsers->display_name = 'Delete Front Users';
		$createUsers->system = true;
		$createUsers->group_id = 9;
		$createUsers->sort = 31;
		$createUsers->created_at = Carbon::now();
		$createUsers->updated_at = Carbon::now();
		$createUsers->save();

		$permission_model = config('access.permission');
		$editUsers = new $permission_model;
		$editUsers->name = 'status-front-users';
		$editUsers->display_name = 'Status Front Users';
		$editUsers->system = true;
		$editUsers->group_id = 9;
		$editUsers->sort = 32;
		$editUsers->created_at = Carbon::now();
		$editUsers->updated_at = Carbon::now();
		$editUsers->save();

		/**
		 * Curriculam  management Permissions
		 */

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-curriculam-management';
		$permissions->display_name = 'View Curriculam Management';
		$permissions->system = true;
		$permissions->group_id = 10;
		$permissions->sort = 33;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-curriculam';
		$permissions->display_name = 'Create Curriculam';
		$permissions->system = true;
		$permissions->group_id = 10;
		$permissions->sort = 34;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-curriculam';
		$permissions->display_name = 'Edit Curriculam';
		$permissions->system = true;
		$permissions->group_id = 10;
		$permissions->sort = 35;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-curriculam';
		$permissions->display_name = 'Delete Curriculam';
		$permissions->system = true;
		$permissions->group_id = 10;
		$permissions->sort = 36;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'status-curriculam';
		$permissions->display_name = 'Status Curriculam';
		$permissions->system = true;
		$permissions->group_id = 10;
		$permissions->sort = 37;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Educational System Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-education-system-management';
		$permissions->display_name = 'View Education System Management';
		$permissions->system = true;
		$permissions->group_id = 11;
		$permissions->sort = 38;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-education-system';
		$permissions->display_name = 'Create Education System';
		$permissions->system = true;
		$permissions->group_id = 11;
		$permissions->sort = 39;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-education-system';
		$permissions->display_name = 'Edit Education System';
		$permissions->system = true;
		$permissions->group_id = 11;
		$permissions->sort = 40;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-education-system';
		$permissions->display_name = 'Delete Education System';
		$permissions->system = true;
		$permissions->group_id = 11;
		$permissions->sort = 41;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'status-education-system';
		$permissions->display_name = 'Status Education System';
		$permissions->system = true;
		$permissions->group_id = 11;
		$permissions->sort = 42;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Level Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-level-management';
		$permissions->display_name = 'View Level Management';
		$permissions->system = true;
		$permissions->group_id = 12;
		$permissions->sort = 43;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-level';
		$permissions->display_name = 'Create Level';
		$permissions->system = true;
		$permissions->group_id = 12;
		$permissions->sort = 44;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-level';
		$permissions->display_name = 'Edit Level';
		$permissions->system = true;
		$permissions->group_id = 12;
		$permissions->sort = 45;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-level';
		$permissions->display_name = 'Delete Level';
		$permissions->system = true;
		$permissions->group_id = 12;
		$permissions->sort = 46;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'status-level';
		$permissions->display_name = 'Status Level';
		$permissions->system = true;
		$permissions->group_id = 12;
		$permissions->sort = 47;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Program Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-program-management';
		$permissions->display_name = 'View Program Management';
		$permissions->system = true;
		$permissions->group_id = 13;
		$permissions->sort = 48;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-program';
		$permissions->display_name = 'Create Program';
		$permissions->system = true;
		$permissions->group_id = 13;
		$permissions->sort = 49;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-program';
		$permissions->display_name = 'Edit Program';
		$permissions->system = true;
		$permissions->group_id = 13;
		$permissions->sort = 50;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-program';
		$permissions->display_name = 'Delete Program';
		$permissions->system = true;
		$permissions->group_id = 13;
		$permissions->sort = 51;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'status-program';
		$permissions->display_name = 'Status Program';
		$permissions->system = true;
		$permissions->group_id = 13;
		$permissions->sort = 52;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Topic Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-topic-management';
		$permissions->display_name = 'View Topic Management';
		$permissions->system = true;
		$permissions->group_id = 14;
		$permissions->sort = 53;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-topic';
		$permissions->display_name = 'Create Topic';
		$permissions->system = true;
		$permissions->group_id = 14;
		$permissions->sort = 54;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-topic';
		$permissions->display_name = 'Edit Topic';
		$permissions->system = true;
		$permissions->group_id = 14;
		$permissions->sort = 55;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-topic';
		$permissions->display_name = 'Delete Topic';
		$permissions->system = true;
		$permissions->group_id = 14;
		$permissions->sort = 56;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'status-topic';
		$permissions->display_name = 'Status Topic';
		$permissions->system = true;
		$permissions->group_id = 14;
		$permissions->sort = 57;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Grade Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-grade-management';
		$permissions->display_name = 'View Grade Management';
		$permissions->system = true;
		$permissions->group_id = 15;
		$permissions->sort = 58;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-grade';
		$permissions->display_name = 'Create Grade';
		$permissions->system = true;
		$permissions->group_id = 15;
		$permissions->sort = 59;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-grade';
		$permissions->display_name = 'Edit Grade';
		$permissions->system = true;
		$permissions->group_id = 15;
		$permissions->sort = 60;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-grade';
		$permissions->display_name = 'Delete Grade';
		$permissions->system = true;
		$permissions->group_id = 15;
		$permissions->sort = 61;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'status-grade';
		$permissions->display_name = 'Status Grade';
		$permissions->system = true;
		$permissions->group_id = 15;
		$permissions->sort = 62;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Subject Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-subject-management';
		$permissions->display_name = 'View Subject Management';
		$permissions->system = true;
		$permissions->group_id = 16;
		$permissions->sort = 63;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'create-subject';
		$permissions->display_name = 'Create Subject';
		$permissions->system = true;
		$permissions->group_id = 16;
		$permissions->sort = 64;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'edit-subject';
		$permissions->display_name = 'Edit Subject';
		$permissions->system = true;
		$permissions->group_id = 16;
		$permissions->sort = 65;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-subject';
		$permissions->display_name = 'Delete Subject';
		$permissions->system = true;
		$permissions->group_id = 16;
		$permissions->sort = 66;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'status-subject';
		$permissions->display_name = 'Status Subject';
		$permissions->system = true;
		$permissions->group_id = 16;
		$permissions->sort = 67;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * ContactUs Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-contactus-management';
		$permissions->display_name = 'View ContactUs Management';
		$permissions->system = true;
		$permissions->group_id = 17;
		$permissions->sort = 68;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'delete-contactus';
		$permissions->display_name = 'Delete ContactUs';
		$permissions->system = true;
		$permissions->group_id = 17;
		$permissions->sort = 69;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'read-contactus';
		$permissions->display_name = 'Read ContactUs';
		$permissions->system = true;
		$permissions->group_id = 17;
		$permissions->sort = 70;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * News Letter Management
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-news-letter-management';
		$permissions->display_name = 'View News Letter Management';
		$permissions->system = true;
		$permissions->group_id = 18;
		$permissions->sort = 71;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'send-mail-to-all-news-letter';
		$permissions->display_name = 'Send Mail To all News Letter';
		$permissions->system = true;
		$permissions->group_id = 18;
		$permissions->sort = 72;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'send-mail-news-letter';
		$permissions->display_name = 'Send Mail News Letter';
		$permissions->system = true;
		$permissions->group_id = 18;
		$permissions->sort = 73;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Access Permissions
		 *
		 * Roles (create roles)
		 */
		$permission_model = config('access.permission');
		$createRoles = new $permission_model;
		$createRoles->name = 'create-roles';
		$createRoles->display_name = 'Create Roles';
		$createRoles->system = true;
		$createRoles->group_id = 20;
		$createRoles->sort = 74;
		$createRoles->created_at = Carbon::now();
		$createRoles->updated_at = Carbon::now();
		$createRoles->save();

		/**
		 * Access Permissions
		 *
		 * User (View Users)
		 */
		$permission_model = config('access.permission');
		$createRoles = new $permission_model;
		$createRoles->name = 'view-users';
		$createRoles->display_name = 'View Users';
		$createRoles->system = true;
		$createRoles->group_id = 2;
		$createRoles->sort = 75;
		$createRoles->created_at = Carbon::now();
		$createRoles->updated_at = Carbon::now();
		$createRoles->save();

		/**
		 * Access Permissions
		 *
		 * Permission (View Permission)
		 */
		$permission_model = config('access.permission');
		$createRoles = new $permission_model;
		$createRoles->name = 'view-permissions';
		$createRoles->display_name = 'View Permission';
		$createRoles->system = true;
		$createRoles->group_id = 2;
		$createRoles->sort = 76;
		$createRoles->created_at = Carbon::now();
		$createRoles->updated_at = Carbon::now();
		$createRoles->save();

		/**
		 * Settings
		 */
		$permission_model = config('access.permission');
		$permissions = new $permission_model;
		$permissions->name = 'view-settings';
		$permissions->display_name = 'View Setting';
		$permissions->system = true;
		$permissions->group_id = 8;
		$permissions->sort = 77;
		$permissions->created_at = Carbon::now();
		$permissions->updated_at = Carbon::now();
		$permissions->save();

		/**
		 * Front User Permissions
		 */

		$permission_model = config('access.permission');
		$createUsers = new $permission_model;
		$createUsers->name = 'view-front-users';
		$createUsers->display_name = 'View Front Users';
		$createUsers->system = true;
		$createUsers->group_id = 9;
		$createUsers->sort = 78;
		$createUsers->created_at = Carbon::now();
		$createUsers->updated_at = Carbon::now();
		$createUsers->save();

		/**
		 * Permission Group
		 */
		$permission_model = config('access.permission');
		$createPermissionGroups = new $permission_model;
		$createPermissionGroups->name = 'view-permission-groups';
		$createPermissionGroups->display_name = 'View Permission Groups';
		$createPermissionGroups->system = true;
		$createPermissionGroups->group_id = 19;
		$createPermissionGroups->sort = 79;
		$createPermissionGroups->created_at = Carbon::now();
		$createPermissionGroups->updated_at = Carbon::now();
		$createPermissionGroups->save();

		if (env('DB_CONNECTION') == 'mysql') {
			DB::statement('SET FOREIGN_KEY_CHECKS=1;');
		}
	}
}