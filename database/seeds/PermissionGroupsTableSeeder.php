<?php

use Illuminate\Database\Seeder;

class PermissionGroupsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run() {
		\DB::table('permission_groups')->delete();

		\DB::table('permission_groups')->insert(array(
			0 => array(
				'id' => '1',
				'parent_id' => NULL,
				'name' => 'Access',
				'sort' => '1',
				'created_at' => '2016-03-17 07:10:39',
				'updated_at' => '2016-03-17 07:10:39',
			),
			1 => array(
				'id' => '2',
				'parent_id' => '1',
				'name' => 'User',
				'sort' => '2',
				'created_at' => '2016-03-17 07:10:39',
				'updated_at' => '2016-03-17 07:10:39',
			),
			2 => array(
				'id' => '3',
				'parent_id' => '1',
				'name' => 'Role',
				'sort' => '3',
				'created_at' => '2016-03-17 07:10:39',
				'updated_at' => '2016-03-17 07:10:39',
			),
			3 => array(
				'id' => '4',
				'parent_id' => '1',
				'name' => 'Permission',
				'sort' => '4',
				'created_at' => '2016-03-17 07:10:39',
				'updated_at' => '2016-03-17 07:10:39',
			),
			4 => array(
				'id' => '5',
				'parent_id' => NULL,
				'name' => 'CMS Pages Management',
				'sort' => '5',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			5 => array(
				'id' => '6',
				'parent_id' => NULL,
				'name' => 'Master Management',
				'sort' => '6',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			6 => array(
				'id' => '7',
				'parent_id' => NULL,
				'name' => 'Email Templates',
				'sort' => '7',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			7 => array(
				'id' => '8',
				'parent_id' => NULL,
				'name' => 'Settings',
				'sort' => '8',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			8 => array(
				'id' => '9',
				'parent_id' => NULL,
				'name' => 'Front User',
				'sort' => '9',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			9 => array(
				'id' => '10',
				'parent_id' => NULL,
				'name' => 'Curriculam  Management',
				'sort' => '10',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			10 => array(
				'id' => '11',
				'parent_id' => NULL,
				'name' => 'Educational System Management',
				'sort' => '11',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			11 => array(
				'id' => '12',
				'parent_id' => NULL,
				'name' => 'Level Management',
				'sort' => '12',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			12 => array(
				'id' => '13',
				'parent_id' => NULL,
				'name' => 'Program Management',
				'sort' => '13',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			13 => array(
				'id' => '14',
				'parent_id' => NULL,
				'name' => 'Topic Management',
				'sort' => '14',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			14 => array(
				'id' => '15',
				'parent_id' => NULL,
				'name' => 'Grade Management',
				'sort' => '15',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			15 => array(
				'id' => '16',
				'parent_id' => NULL,
				'name' => 'Subject Management',
				'sort' => '16',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			16 => array(
				'id' => '17',
				'parent_id' => NULL,
				'name' => 'ContactUs Management',
				'sort' => '17',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			17 => array(
				'id' => '18',
				'parent_id' => NULL,
				'name' => 'News Letter Management',
				'sort' => '18',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			18 => array(
				'id' => '19',
				'parent_id' => NULL,
				'name' => 'Permission Group',
				'sort' => '19',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),
			19 => array(
				'id' => '20',
				'parent_id' => NULL,
				'name' => 'Role Management',
				'sort' => '20',
				'created_at' => '2017-01-10 00:00:00',
				'updated_at' => '2017-01-10 00:00:00',
			),

		));
	}
}
