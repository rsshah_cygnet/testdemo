<?php

use Illuminate\Database\Seeder;

class MediaCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('media_categories')->delete();
        
        \DB::table('media_categories')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'Slider',
                'status' => '1',
                'created_at' => '2016-03-17 00:00:00',
                'updated_at' => '2016-03-17 00:00:00',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => 'General',
                'status' => '1',
                'created_at' => '2016-03-17 00:00:00',
                'updated_at' => '2016-03-17 00:00:00',
            ),
        ));
        
        
    }
}
