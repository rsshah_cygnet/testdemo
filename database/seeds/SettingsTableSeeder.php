<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder {
	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run() {
		\DB::table('settings')->delete();
		\DB::table('settings')->insert(array(
			0 => array(
				'id' => '1',
				'logo' => '',
				'favicon' => '',
				'seo_title' => 'Tuteme | Tuteme',
				'seo_keyword' => 'new cars for sale, new cars, cars, new vehicles for sale, used cars for sale, used cars, second hand cars for sale, car for sale, vehicle for sale',
				'seo_description' => 'Imperial Select is a new and used car dealership with over 40 branches – With an extensive range of quality selected cars for sale.',
				'company_contact' => '',
				'company_address' => '',
				'from_name' => '',
				'from_email' => '',
				'facebook' => '',
				'linkedin' => '',
				'twitter' => '',
				'google' => '',
				'copyright_text' => '',
				'footer_text' => '',
				'google_analytics' => '',
				'home_video1' => '',
				'home_video2' => NULL,
				'home_video3' => '',
				'home_video4' => '',
				'explanation1' => '',
				'explanation2' => '',
				'explanation3' => '',
				'explanation4' => '',
				'created_at' => '2016-03-04 06:10:59',
				'updated_at' => '2016-03-04 07:11:24',
			),
		));
	}
}
