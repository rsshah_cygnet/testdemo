<?php

use Illuminate\Database\Seeder;

class ProvinceRegionsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        /*\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('province_regions')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \DB::table('province_regions')->insert(array(
            0 => array('id' => '1', 'province_id' => '1', 'region_name' => 'Port Elizabeth'),
            1 => array('id' => '2', 'province_id' => '2', 'region_name' => 'Bloemfontein'),
            2 => array('id' => '3', 'province_id' => '3', 'region_name' => 'Alberton'),
            3 => array('id' => '4', 'province_id' => '3', 'region_name' => 'Boksburg'),
            4 => array('id' => '5', 'province_id' => '3', 'region_name' => 'Centurion'),
            5 => array('id' => '6', 'province_id' => '3', 'region_name' => 'East Rand'),
            6 => array('id' => '7', 'province_id' => '3', 'region_name' => 'Edenvale'),
            7 => array('id' => '8', 'province_id' => '3', 'region_name' => 'Florida'),
            8 => array('id' => '9', 'province_id' => '3', 'region_name' => 'Fountains'),
            9 => array('id' => '10', 'province_id' => '3', 'region_name' => 'Germiston'),
            10 => array('id' => '11', 'province_id' => '3', 'region_name' => 'Glen'),
            11 => array('id' => '12', 'province_id' => '3', 'region_name' => 'Isando'),
            12 => array('id' => '13', 'province_id' => '3', 'region_name' => 'Kempton Park'),
            13 => array('id' => '14', 'province_id' => '3', 'region_name' => 'Midrand'),
            14 => array('id' => '15', 'province_id' => '3', 'region_name' => 'Northcliff'),
            15 => array('id' => '16', 'province_id' => '3', 'region_name' => 'Pretoria'),
            16 => array('id' => '17', 'province_id' => '3', 'region_name' => 'Roodepoort'),
            17 => array('id' => '18', 'province_id' => '3', 'region_name' => 'Strijdompark (Malibongwe)'),
            18 => array('id' => '19', 'province_id' => '3', 'region_name' => 'West Rand'),
            19 => array('id' => '20', 'province_id' => '4', 'region_name' => 'Durban'),
            20 => array('id' => '21', 'province_id' => '4', 'region_name' => 'Durban Central'),
            21 => array('id' => '22', 'province_id' => '4', 'region_name' => 'Gateway'),
            22 => array('id' => '23', 'province_id' => '4', 'region_name' => 'Hillcrest'),
            23 => array('id' => '24', 'province_id' => '4', 'region_name' => 'Pinetown'),
            24 => array('id' => '25', 'province_id' => '4', 'region_name' => 'Renault Pinetown'),
            25 => array('id' => '26', 'province_id' => '5', 'region_name' => 'Polokwane'),
            26 => array('id' => '27', 'province_id' => '6', 'region_name' => 'Nelspruit'),
            27 => array('id' => '28', 'province_id' => '9', 'region_name' => 'Brackenfell'),
            28 => array('id' => '29', 'province_id' => '9', 'region_name' => 'Cape Town'),
            29 => array('id' => '30', 'province_id' => '9', 'region_name' => 'Diep River'),
            30 => array('id' => '31', 'province_id' => '9', 'region_name' => 'George'),
            31 => array('id' => '32', 'province_id' => '9', 'region_name' => 'Helderberg'),
            32 => array('id' => '33', 'province_id' => '9', 'region_name' => 'Milnerton'),
            33 => array('id' => '34', 'province_id' => '9', 'region_name' => 'Stellenbosch'),
            34 => array('id' => '35', 'province_id' => '9', 'region_name' => 'Table View'),
            35 => array('id' => '36', 'province_id' => '9', 'region_name' => 'Tokai'),
            36 => array('id' => '37', 'province_id' => '9', 'region_name' => 'Tygerberg'),
            37 => array('id' => '38', 'province_id' => '9', 'region_name' => 'Tygervalley')
        ));*/
    }
}
