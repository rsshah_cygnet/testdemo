<?php

use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 10 ; $i++)
        {
        	DB::table('blogs')->insert([

        			'name' => $faker->unique()->sentence($nbWords = 10),
        			'content' => $faker->paragraph($nbSentences = 6),
        			'publish_datetime' => $faker->dateTime($min = 'now'),
                    'featured_image' => 'user.jpg',
        			'domain_id' => $faker->numberBetween($min = 6, $max = 10),
        			'meta_title' => $faker->sentence($nbWords = 5),
        			'cannonical_link' => $faker->url,
        			'meta_description' => $faker->paragraph($nbSentences = 5),
        			'meta_keywords' => $faker->word,
        			'status' => 'Published',
        			'created_by' => 1,
        			'created_at' => $faker->dateTime($max = 'now'),
        		]);
        }
    }
}
