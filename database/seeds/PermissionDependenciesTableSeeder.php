<?php

use Illuminate\Database\Seeder;

class PermissionDependenciesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run() {
		\DB::table('permission_dependencies')->delete();

		\DB::table('permission_dependencies')->insert(array(
			0 => array(
				'id' => '1',
				'permission_id' => '2',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			/**
			 * Admin User Permissions
			 */

			1 => array(
				'id' => '2',
				'permission_id' => '3',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			2 => array(
				'id' => '3',
				'permission_id' => '4',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			3 => array(
				'id' => '4',
				'permission_id' => '5',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			4 => array(
				'id' => '5',
				'permission_id' => '6',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			5 => array(
				'id' => '6',
				'permission_id' => '7',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			6 => array(
				'id' => '7',
				'permission_id' => '8',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			7 => array(
				'id' => '8',
				'permission_id' => '9',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			8 => array(
				'id' => '9',
				'permission_id' => '10',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			9 => array(
				'id' => '10',
				'permission_id' => '11',
				'dependency_id' => '75',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * Role Management
			 */
			10 => array(
				'id' => '11',
				'permission_id' => '13',
				'dependency_id' => '12',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			11 => array(
				'id' => '12',
				'permission_id' => '74',
				'dependency_id' => '12',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			12 => array(
				'id' => '13',
				'permission_id' => '14',
				'dependency_id' => '12',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * Permission
			 */
			13 => array(
				'id' => '14',
				'permission_id' => '19',
				'dependency_id' => '76',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			14 => array(
				'id' => '15',
				'permission_id' => '20',
				'dependency_id' => '76',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			15 => array(
				'id' => '16',
				'permission_id' => '21',
				'dependency_id' => '76',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * CMS Pages Management
			 */
			16 => array(
				'id' => '17',
				'permission_id' => '23',
				'dependency_id' => '22',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			17 => array(
				'id' => '18',
				'permission_id' => '24',
				'dependency_id' => '22',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			18 => array(
				'id' => '19',
				'permission_id' => '25',
				'dependency_id' => '22',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * Email Templates
			 */
			19 => array(
				'id' => '20',
				'permission_id' => '27',
				'dependency_id' => '26',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			20 => array(
				'id' => '21',
				'permission_id' => '28',
				'dependency_id' => '26',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			21 => array(
				'id' => '22',
				'permission_id' => '29',
				'dependency_id' => '26',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * Curriculam  management Permissions
			 */
			22 => array(
				'id' => '23',
				'permission_id' => '34',
				'dependency_id' => '33',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			23 => array(
				'id' => '24',
				'permission_id' => '35',
				'dependency_id' => '33',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			24 => array(
				'id' => '25',
				'permission_id' => '36',
				'dependency_id' => '33',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			25 => array(
				'id' => '26',
				'permission_id' => '37',
				'dependency_id' => '33',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * Educational System Management
			 */
			26 => array(
				'id' => '27',
				'permission_id' => '39',
				'dependency_id' => '38',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			27 => array(
				'id' => '28',
				'permission_id' => '40',
				'dependency_id' => '38',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			28 => array(
				'id' => '29',
				'permission_id' => '41',
				'dependency_id' => '38',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			29 => array(
				'id' => '30',
				'permission_id' => '42',
				'dependency_id' => '38',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			/**
			 * Level Management
			 */
			30 => array(
				'id' => '31',
				'permission_id' => '44',
				'dependency_id' => '43',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			31 => array(
				'id' => '32',
				'permission_id' => '45',
				'dependency_id' => '43',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			32 => array(
				'id' => '33',
				'permission_id' => '46',
				'dependency_id' => '43',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			33 => array(
				'id' => '34',
				'permission_id' => '47',
				'dependency_id' => '43',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			/**
			 * Program Management
			 */
			34 => array(
				'id' => '35',
				'permission_id' => '49',
				'dependency_id' => '48',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			35 => array(
				'id' => '36',
				'permission_id' => '50',
				'dependency_id' => '48',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			36 => array(
				'id' => '37',
				'permission_id' => '51',
				'dependency_id' => '48',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			37 => array(
				'id' => '38',
				'permission_id' => '52',
				'dependency_id' => '48',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * Topic Management
			 */
			38 => array(
				'id' => '39',
				'permission_id' => '54',
				'dependency_id' => '53',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			39 => array(
				'id' => '40',
				'permission_id' => '55',
				'dependency_id' => '53',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			40 => array(
				'id' => '41',
				'permission_id' => '56',
				'dependency_id' => '53',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			41 => array(
				'id' => '42',
				'permission_id' => '57',
				'dependency_id' => '53',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * Grade Management
			 */
			42 => array(
				'id' => '43',
				'permission_id' => '59',
				'dependency_id' => '58',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			43 => array(
				'id' => '44',
				'permission_id' => '60',
				'dependency_id' => '58',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			44 => array(
				'id' => '45',
				'permission_id' => '61',
				'dependency_id' => '58',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			45 => array(
				'id' => '46',
				'permission_id' => '62',
				'dependency_id' => '58',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			/**
			 * Subject Management
			 */
			46 => array(
				'id' => '47',
				'permission_id' => '64',
				'dependency_id' => '63',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			47 => array(
				'id' => '48',
				'permission_id' => '65',
				'dependency_id' => '63',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			48 => array(
				'id' => '49',
				'permission_id' => '66',
				'dependency_id' => '63',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			49 => array(
				'id' => '50',
				'permission_id' => '67',
				'dependency_id' => '63',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			/**
			 * ContactUs Management
			 */
			50 => array(
				'id' => '51',
				'permission_id' => '69',
				'dependency_id' => '68',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			51 => array(
				'id' => '52',
				'permission_id' => '70',
				'dependency_id' => '68',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			/**
			 * News Letter Management
			 */
			52 => array(
				'id' => '53',
				'permission_id' => '67',
				'dependency_id' => '72',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			53 => array(
				'id' => '54',
				'permission_id' => '67',
				'dependency_id' => '73',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			/**
			 * Settings
			 */
			54 => array(
				'id' => '55',
				'permission_id' => '30',
				'dependency_id' => '77',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			/**
			 * Front User Permissions
			 */
			55 => array(
				'id' => '56',
				'permission_id' => '31',
				'dependency_id' => '78',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			56 => array(
				'id' => '57',
				'permission_id' => '32',
				'dependency_id' => '78',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			/**
			 * Permission Group
			 */
			57 => array(
				'id' => '58',
				'permission_id' => '15',
				'dependency_id' => '79',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			58 => array(
				'id' => '59',
				'permission_id' => '16',
				'dependency_id' => '79',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			59 => array(
				'id' => '60',
				'permission_id' => '17',
				'dependency_id' => '79',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			60 => array(
				'id' => '61',
				'permission_id' => '18',
				'dependency_id' => '79',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			61 => array(
				'id' => '62',
				'permission_id' => '33',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			62 => array(
				'id' => '63',
				'permission_id' => '38',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			63 => array(
				'id' => '64',
				'permission_id' => '43',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			64 => array(
				'id' => '65',
				'permission_id' => '48',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			65 => array(
				'id' => '66',
				'permission_id' => '53',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			66 => array(
				'id' => '67',
				'permission_id' => '58',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
			67 => array(
				'id' => '68',
				'permission_id' => '63',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),

			68 => array(
				'id' => '69',
				'permission_id' => '22',
				'dependency_id' => '1',
				'created_at' => '2016-03-17 07:10:40',
				'updated_at' => '2016-03-17 07:10:40',
			),
		));
	}
}
