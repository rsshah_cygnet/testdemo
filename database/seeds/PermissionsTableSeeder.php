<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => '1',
                'group_id' => '1',
                'name' => 'view-backend',
                'display_name' => 'View Backend',
                'system' => '1',
                'sort' => '1',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            1 => 
            array (
                'id' => '2',
                'group_id' => '1',
                'name' => 'view-access-management',
                'display_name' => 'View Access Management',
                'system' => '1',
                'sort' => '2',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            2 => 
            array (
                'id' => '3',
                'group_id' => '2',
                'name' => 'create-users',
                'display_name' => 'Create Users',
                'system' => '1',
                'sort' => '5',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            3 => 
            array (
                'id' => '4',
                'group_id' => '2',
                'name' => 'edit-users',
                'display_name' => 'Edit Users',
                'system' => '1',
                'sort' => '6',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            4 => 
            array (
                'id' => '5',
                'group_id' => '2',
                'name' => 'delete-users',
                'display_name' => 'Delete Users',
                'system' => '1',
                'sort' => '7',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            5 => 
            array (
                'id' => '6',
                'group_id' => '2',
                'name' => 'change-user-password',
                'display_name' => 'Change User Password',
                'system' => '1',
                'sort' => '8',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            /*6 => 
            array (
                'id' => '7',
                'group_id' => '2',
                'name' => 'deactivate-users',
                'display_name' => 'Deactivate Users',
                'system' => '1',
                'sort' => '9',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            7 => 
            array (
                'id' => '8',
                'group_id' => '2',
                'name' => 'reactivate-users',
                'display_name' => 'Re-Activate Users',
                'system' => '1',
                'sort' => '11',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            8 => 
            array (
                'id' => '9',
                'group_id' => '2',
                'name' => 'undelete-users',
                'display_name' => 'Restore Users',
                'system' => '1',
                'sort' => '13',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            9 => 
            array (
                'id' => '10',
                'group_id' => '2',
                'name' => 'permanently-delete-users',
                'display_name' => 'Permanently Delete Users',
                'system' => '1',
                'sort' => '14',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            10 => 
            array (
                'id' => '11',
                'group_id' => '2',
                'name' => 'resend-user-confirmation-email',
                'display_name' => 'Resend Confirmation E-mail',
                'system' => '1',
                'sort' => '15',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            11 => 
            array (
                'id' => '12',
                'group_id' => '3',
                'name' => 'create-roles',
                'display_name' => 'Create Roles',
                'system' => '1',
                'sort' => '2',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            12 => 
            array (
                'id' => '13',
                'group_id' => '3',
                'name' => 'edit-roles',
                'display_name' => 'Edit Roles',
                'system' => '1',
                'sort' => '3',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            13 => 
            array (
                'id' => '14',
                'group_id' => '3',
                'name' => 'delete-roles',
                'display_name' => 'Delete Roles',
                'system' => '1',
                'sort' => '4',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            14 => 
            array (
                'id' => '15',
                'group_id' => '4',
                'name' => 'create-permission-groups',
                'display_name' => 'Create Permission Groups',
                'system' => '1',
                'sort' => '1',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            15 => 
            array (
                'id' => '16',
                'group_id' => '4',
                'name' => 'edit-permission-groups',
                'display_name' => 'Edit Permission Groups',
                'system' => '1',
                'sort' => '2',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            16 => 
            array (
                'id' => '17',
                'group_id' => '4',
                'name' => 'delete-permission-groups',
                'display_name' => 'Delete Permission Groups',
                'system' => '1',
                'sort' => '3',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            17 => 
            array (
                'id' => '18',
                'group_id' => '4',
                'name' => 'sort-permission-groups',
                'display_name' => 'Sort Permission Groups',
                'system' => '1',
                'sort' => '4',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            18 => 
            array (
                'id' => '19',
                'group_id' => '4',
                'name' => 'create-permissions',
                'display_name' => 'Create Permissions',
                'system' => '1',
                'sort' => '5',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            19 => 
            array (
                'id' => '20',
                'group_id' => '4',
                'name' => 'edit-permissions',
                'display_name' => 'Edit Permissions',
                'system' => '1',
                'sort' => '6',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),
            20 => 
            array (
                'id' => '21',
                'group_id' => '4',
                'name' => 'delete-permissions',
                'display_name' => 'Delete Permissions',
                'system' => '1',
                'sort' => '7',
                'created_at' => '2016-03-17 07:10:40',
                'updated_at' => '2016-03-17 07:10:40',
            ),*/
        ));
        
        
    }
}
