<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $this->call('PermissionGroupsTableSeeder');
            $this->call('PermissionDependenciesTableSeeder');
            $this->call('SettingsTableSeeder');
            $this->call('PermissionsTableSeeder');

        }

        $this->call(AccessTableSeeder::class);
        $this->call(CmspagesTableSeeder::class);
        $this->call(EmailTemplatePlaceholderTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(CountryTableSeeder::class);
       // $this->call(ProvinceRegionsTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(PermissionUserTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(TimezoneTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

        Model::reguard();
    }
}
