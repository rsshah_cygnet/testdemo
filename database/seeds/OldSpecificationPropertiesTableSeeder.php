<?php

use Illuminate\Database\Seeder;

class OldSpecificationPropertiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('old_specification_properties')->delete();

        \DB::table('old_specification_properties')->insert(array(
            0 =>
                array(
                    'id' => '1',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Bakie',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            1 =>
                array(
                    'id' => '2',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Sedan',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            2 =>
                array(
                    'id' => '3',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Hatchback',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            3 =>
                array(
                    'id' => '4',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Minibus',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            4 =>
                array(
                    'id' => '5',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Multi Purpose Vehicle(MPV)',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            5 =>
                array(
                    'id' => '6',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Panel Van',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            6 =>
                array(
                    'id' => '7',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Light Commercial Vehicle(1 ton)',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            7 =>
                array(
                    'id' => '8',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Sports Utility Vehicle(SUV)',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            8 =>
                array(
                    'id' => '9',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Camper',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            9 =>
                array(
                    'id' => '10',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Coupe',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            10 =>
                array(
                    'id' => '11',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Hybrid',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            11 =>
                array(
                    'id' => '12',
                    'old_specification_id' => '1',
                    'old_specification_property' => 'Motorcycle',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            12 =>
                array(
                    'id' => '13',
                    'old_specification_id' => '2',
                    'old_specification_property' => 'Manual',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            13 =>
                array(
                    'id' => '14',
                    'old_specification_id' => '2',
                    'old_specification_property' => 'Automatic',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            14 =>
                array(
                    'id' => '15',
                    'old_specification_id' => '3',
                    'old_specification_property' => 'Petrol',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            15 =>
                array(
                    'id' => '16',
                    'old_specification_id' => '3',
                    'old_specification_property' => 'Diesel',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            16 =>
                array(
                    'id' => '17',
                    'old_specification_id' => '4',
                    'old_specification_property' => 'Yes',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            17 =>
                array(
                    'id' => '18',
                    'old_specification_id' => '4',
                    'old_specification_property' => 'No',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            18 =>
                array(
                    'id' => '19',
                    'old_specification_id' => '4',
                    'old_specification_property' => 'Partial',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            19 =>
                array(
                    'id' => '20',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Air Conditioner',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            20 =>
                array(
                    'id' => '21',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Airbags',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            21 =>
                array(
                    'id' => '22',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Power Steering',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            22 =>
                array(
                    'id' => '23',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Electrical Window(front)',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            23 =>
                array(
                    'id' => '24',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Central Locking',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            24 =>
                array(
                    'id' => '25',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Sunroof',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            25 =>
                array(
                    'id' => '26',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'CD Shuttle',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            26 =>
                array(
                    'id' => '27',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Alloy Wheels',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            27 =>
                array(
                    'id' => '28',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'USB Port',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            28 =>
                array(
                    'id' => '29',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Roll Bar',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            29 =>
                array(
                    'id' => '30',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Reverse Camera',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            30 =>
                array(
                    'id' => '31',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Sport Suspension',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            31 =>
                array(
                    'id' => '32',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Bluetooth Enabled',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            32 =>
                array(
                    'id' => '33',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Bin Lining',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            33 =>
                array(
                    'id' => '34',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'MP3-CD',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            34 =>
                array(
                    'id' => '35',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'EBD',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            35 =>
                array(
                    'id' => '36',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Climate Control',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            36 =>
                array(
                    'id' => '37',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Automatic Headlights',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            37 =>
                array(
                    'id' => '38',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Telescope Steering',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            38 =>
                array(
                    'id' => '39',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Front Bucket Seats',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            39 =>
                array(
                    'id' => '40',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Premium Speakers',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            40 =>
                array(
                    'id' => '41',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Memory Seats',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            41 =>
                array(
                    'id' => '42',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Keyless Entry System',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            42 =>
                array(
                    'id' => '43',
                    'old_specification_id' => '5',
                    'old_specification_property' => 'Xenon lights',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
        ));
    }
}
