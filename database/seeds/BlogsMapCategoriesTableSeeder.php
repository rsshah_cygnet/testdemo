<?php

use Illuminate\Database\Seeder;

class BlogsMapCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 85; $i <= 94; $i++)
        {
        	DB::table('blog_map_categories')->insert([

        			'blog_id' => $i,
        			'category_id' => $faker->numberBetween($min = 3, $max = 7),
        		]);
        }
    }
}
