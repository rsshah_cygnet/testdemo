<?php

use Illuminate\Database\Seeder;

class DealershipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<=10;$i++)
        {
        	DB::table('dealerships')->insert([
        	'dealership_code' => $faker->sentence,
        	'dealership_name' => $faker->sentence,
        	'province_region_id' => $faker->numberBetween($min = 1, $max = 30),
        	'address' => $faker->sentence,
        	'banner_image' => $faker->word.".png",
        	'dealership_email' => $faker->unique()->sentence,
        	'dealer_principal_email' => $faker->unique()->sentence,
        	'dealer_principal_contact_no' => $faker->phoneNumber,
        	'status' => $faker->randomElement(array('Active','InActive'))
        	]);

        }
    }
}
