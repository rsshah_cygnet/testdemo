<?php

use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class EmailTemplatePlaceholderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        if (env('DB_CONNECTION') == 'mysql') {
            DB::table(config('backend.email_template_placeholder_table'))->truncate();
        }

        $data = [
            [
                'name'              => 'name',               
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'email',               
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'contact-details',               
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            
        ];

         DB::table(config('backend.email_template_placeholder_table'))->insert($data);
    }
}
