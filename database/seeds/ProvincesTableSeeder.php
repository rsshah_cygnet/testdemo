<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('provinces')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \DB::table('provinces')->insert(array(
            0 => array('id' => '1', 'name' => 'Eastern Cape'),
            1 => array('id' => '2', 'name' => 'Free State'),
            2 => array('id' => '3', 'name' => 'Gauteng'),
            3 => array('id' => '4', 'name' => 'KwaZulu-Natal'),
            4 => array('id' => '5', 'name' => 'Limpopo'),
            5 => array('id' => '6', 'name' => 'Mpumalanga'),
            6 => array('id' => '7', 'name' => 'North West'),
            7 => array('id' => '8', 'name' => 'Northern Cape'),
            8 => array('id' => '9', 'name' => 'Western Cape')
        ));
    }
}
