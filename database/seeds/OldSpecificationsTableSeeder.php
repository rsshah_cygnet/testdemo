<?php

use Illuminate\Database\Seeder;

class OldSpecificationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('old_specifications')->delete();

        \DB::table('old_specifications')->insert(array(
            0 =>
                array(
                    'id' => '1',
                    'old_specification' => 'Vehicle Type',
                    'input_type' => 'dropdown',
                    'status' => 'Active',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            1 =>
                array(
                    'id' => '2',
                    'old_specification' => 'Transmission Type',
                    'input_type' => 'dropdown',
                    'status' => 'Active',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            2 =>
                array(
                    'id' => '3',
                    'old_specification' => 'Fuel Type',
                    'input_type' => 'dropdown',
                    'status' => 'Active',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            3 =>
                array(
                    'id' => '4',
                    'old_specification' => 'Full Service History(FSH)',
                    'input_type' => 'dropdown',
                    'status' => 'Active',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
            4 =>
                array(
                    'id' => '5',
                    'old_specification' => 'Features',
                    'input_type' => 'checkbox',
                    'status' => 'Active',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2016-03-17 00:00:00',
                    'updated_at' => '2016-03-17 00:00:00',
                    'deleted_at' => NULL,
                ),
        ));
    }
}
