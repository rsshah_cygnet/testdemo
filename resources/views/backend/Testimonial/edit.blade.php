@extends ('backend.layouts.master')

@section ('title', 'Blog Management')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
    <h1>Blog Management<small> EDIT</small></h1>
</div>
</div>
</div>
@endsection

@section('after-styles-end')
	<style type="text/css">
		.select2-selection__choice
		{
			color: #000 !important;
		}
	</style>
@endsection

@section('content')
<div class="box box-danger">

	<div class="box-body">
		{!!Form::model($item, ['route' => ['admin.testimonial.update', $item->id],
			'class' => 'form-horizontal',
			'role' 		=> 'form',
			'method' 	=> 'PATCH',
			'id' 		=> 'edit-blog',
			'enctype' => 'multipart/form-data']) !!}

		{{-- Brand Model Form --}}
		@include('backend.blog.form')
	</div>

	<div class="box-footer">
		<a href="{{ route('admin.blog.index') }}" class="btn btn-red">
			Cancel
		</a>
		{!! Form::submit('Update', array("class"=>"btn btn-info pull-right")) !!}
	</div>

</div>

	{!! Form::close() !!}
@stop
