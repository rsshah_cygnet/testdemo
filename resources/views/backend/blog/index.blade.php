 @extends('backend.layouts.master')

@section('title', 'Blog Management' . ' | ' . trans('labels.backend.settings.edit'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Blog Management<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.blog.includes.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
     <div class="box box-danger">
        <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.blog.data', ['page'=>'1']) !!}"
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;InActive:InActive;Draft:Draft;Published:Published;Scheduled:Scheduled",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
            dateSearchOptions           = getSearchOption('date');

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value =
        {
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [

                {title: false, name: 'Blog',        index: 'blogs.name',searchoptions:stringSearchOptions,width:350},
                {title: false, name: 'Published Date and Time',  index: 'blogs.publish_datetime'},
                {title: false, name: 'Status',  index: 'blogs.status',  stype: 'select',searchoptions: selectStatusSearchOptions},
                {title: false, name: 'Created By',  index: 'users.first_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Created At',  index: 'blogs.created_at',
                searchoptions:dateSearchOptions},
                {title: false, name: 'act',         index: 'act', sortable: false, search: false,width:100},
            ],
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop