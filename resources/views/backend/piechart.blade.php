             <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
           <div class="box box-warning dashboard-section-box">
            <div class="box-body">
              <div id="piechart_3d" style="width: 100%; height: 300px;"></div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="box box-warning dashboard-section-box">
            <div class="box-body">
              <h3 class="box-title"><b>Successful Session Data($)</b></h3>
              <ul class="list-unstyled">
                <li><strong>Amount Paid by Tutee:</strong> {{round($sessionsData['amount_paid_by_tutee'],1)}}</li>
                <li><strong>Admin Commission:</strong> {{round($sessionsData['amount_received_by_admin'],1)}}</li>
                <li><strong>Amount Paid to Tutor:</strong> {{round($sessionsData['amount_paid_to_tutor'],1)}}</li>
              </ul>
              <br>

              <h3 class="box-title"><b>Sessions Cancelled by Tutor Data($)</b></h3>
              <ul class="list-unstyled">
                <li><strong>Amount Refund to Tutee:</strong> {{round($sessionsData['tutee_refund_amount'],1)}}</li>
              </ul>
              <br>

              <h3 class="box-title"><b>Sessions Cancelled by Tutee Data($)</b></h3>
              <ul class="list-unstyled">
                <li><strong>Penalty Amount Paid by Tutee:</strong> {{round($sessionsData['tutee_penalty_amount'],1)}}</li>
                <li><strong>Amount Paid to Tutor:</strong> {{round($sessionsData['tutor_earning_for_cancelled_by_tutee'],1)}}</li>
                <li><strong>Amount is Kept with Admin:</strong> {{round($sessionsData['amount_kept_with_admin_for_cancelled_by_tutee'],1)}}</li>
              </ul>
              <br>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
    
       var record={!! json_encode($user) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
       data.addColumn('string', 'Session Type');
       data.addColumn('number', 'Total');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
             //console.log(v);
          }
        var options = {
          title: 'Session overview',
          is3D: false,
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>