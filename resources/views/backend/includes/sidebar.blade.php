<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <!-- Optionally, you can add icons to the links -->
            <li class="{{ active_class(if_route(array('admin.dashboard')))}}">
                <a href="{!! route('admin.dashboard') !!}">
                    <img src="{{url("/")}}/images/dashboard.svg" />
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>


            @permission('view-access-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/access/*'))) }}">
                <a href="{!!url('admin/access/users')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>{{ trans('menus.backend.access.title') }}</span>
                </a>
            </li>
            @endauth


            @permission('view-access-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/users*'))) }}">
                <a href="{!!url('admin/users')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>{{ trans('menus.backend.access.user') }}</span>
                </a>
            </li>
            @endauth
<?php
$userrExamModules = ['admin/question*', 'admin/examsettings*'];
?>
            <li class="{{ active_class(if_uri_pattern($userrExamModules)) }} treeview">
                <a href="#"><img src="{{url("/")}}/images/menu.svg" />
                    <span>Exam Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(if_uri_pattern($userrExamModules), $activeClass = 'menu-open' )  }}" style="display: none; {{ active_class(if_uri_pattern($userrExamModules), 'display: block;') }}">
                    <!-- @permission('view-used-car-specifications') -->
                    <li class="{{ active_class(if_uri_pattern(array('admin/question*'))) }}">
                        <a href="{!!url('admin/question')!!}">
                            <span>{{ trans('menus.backend.question.questionm') }}</span>
                        </a>
                    </li>
                 <!--    @endauth -->
                   <!-- @permission('view-used-car-specifications') -->
                    <li class="{{ active_class(if_uri_pattern(array('admin/examsettings*'))) }}">
                        <a href="{!! url('admin/examsettings') !!}">
                        <span>
                            Exam Settings
                        </span></a>
                    </li>
                    <!-- @endauth -->
                 </ul>
            </li>
            <!--  @permission('view-access-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/question*'))) }}">
                <a href="{!!url('admin/question')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>{{ trans('menus.backend.question.questionm') }}</span>
                </a>
            </li>
            @endauth -->

             <!--@permission('view-contactus-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/contactus*'))) }}">
                <a href="{!!url('admin/contactus')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>{{ trans('menus.backend.contactus.main') }}</span>
                </a>
            </li>
            @endauth-->

          @permission('view-emailtemplate')
             <li class="{{ active_class(if_uri_pattern(array('admin/emailtemplate*'))) }}">
                <a href="{!!url('admin/emailtemplate')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>{{ trans('menus.backend.emailtemplate.management') }}</span>
                </a>
           @endauth

           @permission('view-cms-pages')
            <li class="{{ active_class(if_uri_pattern(array('admin/cmspages*'))) }}">
                <a href="{!!url('admin/cmspages')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>CMS Pages Management</span>
                </a>
            </li>
            @endauth

<?php
$usercurriculumModules = ['admin/educational-system*', 'admin/curriculum*', 'admin/levels*', 'admin/programs*', 'admin/topic*', 'admin/grade*', 'admin/subject*', 'admin/curriculum-management*'];
?>


           <li class="{{ active_class(if_uri_pattern($usercurriculumModules)) }} treeview">
                <a href="{!!url('admin/curriculum-management')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>Curriculum Management</span>
                </a>
            </li>


              <!--@permission('view-education-system-management')
             <li class="{{ active_class(if_uri_pattern(array('admin/educational-system*'))) }}">
                <a href="{!!url('admin/educational-system')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>{{ trans('menus.backend.educationalsystem.management') }}</span>
                </a>
            @endauth

              @permission('view-level-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/levels*'))) }}">
                <a href="{!!url('admin/levels')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>Level Management</span>
                </a>
            </li>
             @endauth
            @permission('view-program-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/programs*'))) }}">
                <a href="{!!url('admin/programs')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>Program Management</span>
                </a>
            </li>
            @endauth

         @permission('view-topic-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/topic*'))) }}">
                <a href="{!!url('admin/topic')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>Topic Management</span>
                </a>
         @endauth
         @permission('view-grade-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/grade*'))) }}">
                <a href="{!!url('admin/grade')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>Grade Management</span>
                </a>
            </li>
             @endauth

        @permission('view-subject-management')
            <li class="{{ active_class(if_uri_pattern(array('admin/subject*'))) }}">
                <a href="{!!url('admin/subject')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>Subject Management</span>
                </a>
            </li>
             @endauth -->


           {{--   <li class="{{ active_class(if_uri_pattern(array('admin/testimonial*'))) }}">
                <a href="{!!url('admin/testimonial')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>Testimonial Management</span>
                </a>
            </li> --}}

            <li class="{{ active_class(if_uri_pattern(array('admin/session-feedback*'))) }}">
                <a href="{!!url('admin/session-feedback')!!}">
                    <img src="{{url("/")}}/images/bell.svg" />
                    <span>Session Feedback</span>
                </a>
            </li>
            <?php
$userreportModules = ['admin/report-management/reportedbytutor*', 'admin/report-management/reportedbytutee*', 'admin/testimonial*', 'admin/contactus*'];
?>
            <li class="{{ active_class(if_uri_pattern($userreportModules)) }} treeview">
                <a href="#"><img src="{{url("/")}}/images/menu.svg" />
                    <span>Customer Care</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(if_uri_pattern($userreportModules), $activeClass = 'menu-open' )  }}" style="display: none; {{ active_class(if_uri_pattern($userreportModules), 'display: block;') }}">
                    <!-- @permission('view-used-car-specifications') -->
                    <li class="{{ active_class(if_uri_pattern(array('admin/report-management/reportedbytutor*'))) }}">
                        <a href="{!! route('admin.report.by.tutor') !!}"><span>{{ trans('labels.backend.user_report.user_reports_by_tutor') }}</span></a>
                    </li>
                 <!--    @endauth -->
                   <!-- @permission('view-used-car-specifications') -->
                    <li class="{{ active_class(if_uri_pattern(array('admin/report-management/reportedbytutee*'))) }}">
                        <a href="{!! route('admin.report.by.tutee') !!}"><span>{{ trans('labels.backend.user_report.user_reports_by_tutee') }}</span></a>
                    </li>
                    <li class="{{ active_class(if_uri_pattern(array('admin/testimonial*'))) }}">
                        <a href="{!! route('admin.testimonial.index') !!}"><span>Testimonial Management</span></a>
                    </li>
                     @permission('view-contactus-management')
                    <li class="{{ active_class(if_uri_pattern(array('admin/contactus*'))) }}">
                        <a href="{!! route('admin.contactus.index') !!}"><span>{{ trans('menus.backend.contactus.main') }}</span></a>
                    </li>
                    @endauth
                 <!--    @endauth -->
                 </ul>
            </li>

               <?php
$masterModules = ['admin/session/success*', 'admin/session/upcoming*', 'admin/session/cancelled-tutor*', 'admin/session/cancelled-tutee*'];
?>

          <!--   @if(access()->allow('edit-settings') || access()->allow('view-emailtemplate')
                || access()->allow('view-vehicle-model-type') || access()->allow('view-blog-category')
                || access()->allow('view-special-type') || access()->allow('view-new-car-specifications')
                || access()->allow('view-brand')
                || access()->allow('view-brand-model') || access()->allow('view-derivative')
                || access()->allow('view-pricetogo-percentage')
                || access()->allow('view-aggregator')
                || access()->allow('view-notice-board')
                || access()->allow('view-blog-tag')
                || access()->allow('view-used-car-specifications')
            ) -->

            <li class="{{ active_class(if_uri_pattern($masterModules)) }} treeview">
                <a href="#"><img src="{{url("/")}}/images/menu.svg" />
                    <span>Session Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(if_uri_pattern($masterModules), $activeClass = 'menu-open' )  }}" style="display: none; {{ active_class(if_uri_pattern($masterModules), 'display: block;') }}">

                    <!-- @permission('view-used-car-specifications') -->
                    <li class="{{ active_class(if_uri_pattern(array('admin/session/success*'))) }}">
                        <a href="{!! route('admin.session.index') !!}"><span>Successful</span></a>
                    </li>
                 <!--    @endauth -->

                     <!-- @permission('view-used-car-specifications') -->
                    <li class="{{ active_class(if_uri_pattern(array('admin/session/upcoming*'))) }}">
                        <a href="{!! route('admin.session.upcoming') !!}"><span>Upcoming</span></a>
                    </li>
                 <!--    @endauth -->

                    <!-- @permission('view-used-car-specifications') -->
                    <li class="{{ active_class(if_uri_pattern(array('admin/session/cancelled-tutor*'))) }}">
                        <a href="{!! route('admin.session.cancelled-tutor') !!}"><span>Cancelled Session By Tutor</span></a>
                    </li>
                 <!--    @endauth -->

                     <!-- @permission('view-used-car-specifications') -->
                    <li class="{{ active_class(if_uri_pattern(array('admin/session/cancelled-tutee*'))) }}">
                        <a href="{!! route('admin.session.cancelled-tutee') !!}"><span>Cancelled Session By Tutee</span></a>
                    </li>
                 <!--    @endauth -->

                </ul>
            </li>
           <!--  @endif -->

            <?php
$masterModules = ['admin/settings*', 'admin/punctuality-ratings*', 'admin/punctualityratings-cancelsession*',
	'admin/session-cancel-penalty*'];
?>

            @if(access()->allow('edit-settings'))

            <li class="{{ active_class(if_uri_pattern($masterModules)) }} treeview">
                <a href="#"><img src="{{url("/")}}/images/menu.svg" />
                    <span>Configuration</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(if_uri_pattern($masterModules), $activeClass = 'menu-open' )  }}" style="display: none; {{ active_class(if_uri_pattern($masterModules), 'display: block;') }}">

                    @permission('edit-settings')
                    <li class="{{ active_class(if_uri_pattern(array('admin/settings*'))) }}">
                        <a href="{!! route('admin.settings.index') !!}"><span>{{ trans('menus.backend.settings.main') }}</span></a>
                    </li>
                    @endauth

                    <!-- @permission('edit-punctuality-rating') -->
                     <li class="{{ active_class(if_uri_pattern(array('admin/punctuality-ratings*'))) }}">
                        <a href="{!! route('admin.punctuality-ratings.index') !!}"><span>{{ trans('menus.backend.punctuality_rating_success_session.management') }}</span></a>
                    </li>
                    <!-- @endauth -->

                       <!-- @permission('edit-punctuality-rating') -->
                     <li class="{{ active_class(if_uri_pattern(array('admin/punctualityratings-cancelsession*'))) }}">
                        <a href="{!! route('admin.punctualityratings-cancelsession.index') !!}"><span>{{ trans('menus.backend.punctuality_rating_cancel_session.management') }}</span></a>
                    </li>
                    <!-- @endauth -->

                     <!-- @permission('edit-punctuality-rating') -->
                     <li class="{{ active_class(if_uri_pattern(array('admin/session-cancel-penalty*'))) }}">
                        <a href="{!! route('admin.session-cancel-penalty.index') !!}"><span>{{ trans('menus.backend.session_cancel_penalty.management') }}</span></a>
                    </li>
                    <!-- @endauth -->
                </ul>
            </li>
            @endif

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
