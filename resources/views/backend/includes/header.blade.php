<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{!! route('admin.dashboard') !!}" class="logo text-center">
        <span class="logo-mini">
            <img src="{{ url('/') }}/images/logo.png" title="Tuteme" alt="Tuteme" />
        </span>
        <span class="logo-lg">
            <img src="{{ url('/') }}/images/logo.png" title="Tuteme" alt="Tuteme" class="admin_sidebar_logo"/>
            <span>Tuteme</span>
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- Notifications Menu -->
                <li class="dropdown notifications-menu">
                    <!--Menu toggle button -->
                    <a href="#" class="dropdown-toggle notifications-menu-btn" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-danger notification-counter"></span>
                    </a>
                    <ul class="notification-menu-container dropdown-menu">
                    </ul>
                </li>

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="{!! route('backend.user.index') !!}">
                        <!-- The user image in the navbar-->
                         <i class="fa fa-user-circle-o"></i>
                       <!-- <img src="{{url("/")}}/images/settings.svg" alt="User Image" class="setting-icon" />-->
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ trans('navs.general.edit') }}</span>
                    </a>
                </li>
                <li class="dropdown user log-out">
                    <!-- Menu Toggle Button -->
                    <a href="{!! route('admin.auth.logout') !!}">
                     <i class="fa fa fa-external-link"></i>
                        <!-- The user image in the navbar-->
                       <!-- <img src="{{url("/")}}/images/logout.svg" alt="User Image" class="logout-icon" />-->
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ trans('navs.general.logout') }}</span>
                    </a>
                </li>

            </ul>
        </div>
    </nav>
</header>