 <li class="header">{{ trans_choice('strings.backend.general.you_have.notifications', $unreadNotificationCount) }}</li>

<li>
    <!--Inner Menu: contains the notifications -->
    <ul class="menu">
        @foreach($notifications->toArray() as $nK => $nV)
            <li class="{{$nV['is_read']?'read':'unread'}}">
                <!--start notification -->
                <a href="#">
                    <i class="fa fa-exclamation-triangle text-{{$nV['type']}}"></i> 
                    <?php echo html_entity_decode($nV['message']) ?>
                </a>
            </li>
        @endforeach
        <!--end notification -->
    </ul>
</li>
<li class="footer"><a href="{!! route('admin.notification.index') !!}">
{{ trans('strings.backend.general.see_all.notifications') }}</a></li>

