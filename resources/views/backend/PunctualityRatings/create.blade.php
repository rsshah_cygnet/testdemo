@extends ('backend.layouts.master')

@section('title', 'Create Punctuality Rating for Successful Session')
@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Punctuality Rating Management for Successful Session<small> CREATE</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.PunctualityRatings.includes.header-buttons')
</div>
</div>
</div>
</div>

@endsection

@section('after-styles-end')
	<style type="text/css">
		.select2-selection__choice
		{
			color: #000 !important;
		}
	</style>
@endsection

@section('content')
<div class="box box-danger">
	<div class="box-body">
		{!! Form:: open(['route' => 'admin.punctuality-ratings.store','class' 	=> 'form-horizontal','role' => 'form','method' 	=> 'post','id' => 'admin-blog-model']) !!}
			{{-- Blog Form --}}
			@include('backend.PunctualityRatings.form')
	</div>

	<div class="box-footer">
		<a href="{{ route('admin.punctuality-ratings.index') }}" class="btn btn-red">Cancel</a>
		{!! Form::submit('Create', array("class"=>"btn btn-info pull-right")) !!}
	</div>

</div>
	{!! Form::close() !!}

@stop

