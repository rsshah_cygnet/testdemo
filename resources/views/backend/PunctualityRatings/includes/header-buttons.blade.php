<div class="btn-group">

    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>

    </button>

    <ul class="dropdown-menu" role="menu">

       <!-- @permission('view-punctuality-ratings-management') -->

        <li>
            <a href="{{ route('admin.punctuality-ratings.index') }}">
                {{ trans('menus.backend.blog.all') }}
            </a>
        </li>

       <!-- @endauth -->

       <!--   @permission('create-punctuality-ratings') -->

        <li>
            <a href="{{ route('admin.punctuality-ratings.create') }}">
                {{ trans('menus.backend.blog.create') }}
            </a>
        </li>

       <!--   @endauth -->
    </ul>
</div><!--btn group-->
<div class="clearfix"></div>