<?php
//print_r($curriculum);die;
?>

<div class="clear"></div>

<div class="form-horizontal">

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('From',"From",['class' => 'col-md-3 control-label required',]) !!}
        <div class="col-md-8">
            {!! Form::text('from', null, ['class' => 'form-control','placeholder' => 'From','maxlength' => 5]) !!}
        </div>
    </div>

     <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('To',"To",['class' => 'col-md-3 control-label required',]) !!}
        <div class="col-md-8">
            {!! Form::text('to', null, ['class' => 'form-control','placeholder' => 'To','maxlength' => 5]) !!}
        </div>
    </div>

     <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('rating',"Rating",['class' => 'col-md-3 control-label required',]) !!}
        <div class="col-md-8">
            {!! Form::text('rating', null, ['class' => 'form-control','placeholder' => 'Rating']) !!}
        </div>
    </div>



@if(!empty($punctualityrating))
<div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
    {!! Form::label('Status',"Active",['class' => 'col-md-3 control-label required',]) !!}
    <div class="col-lg-1">
        <div class="checkbox icheck">
          {{ Form::checkbox('status', 1, (isset($punctualityrating) && $punctualityrating->status == '1') ? true : false )}}
      </div>
  </div>
</div>
@endif
</div>
@if(!empty($punctualityrating))
{{Form::hidden('id', $punctualityrating->id)}}
@endif
<div class="clear"></div>

@section('after-scripts-end')

<script type="text/javascript">

</script>
@stop