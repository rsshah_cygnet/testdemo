<div class="clear"></div>

<!--<div class="form-group">
    {!! Form::label('title', trans('validation.attributes.backend.settings.logo'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!!  Form::file('logo') !!}
        @if($settings->logo)
        <img height="50" width="50" src="{{ asset($settings->logo) }}" alt="logo">
        @endif
    </div>
</div>
<div class="form-group">
    {!! Form::label('favicon', trans('validation.attributes.backend.settings.favicon'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!!  Form::file('favicon') !!}
        @if($settings->favicon)
        <img height="50" width="50" src="{{ asset($settings->favicon) }}" alt="favicon">
        @endif
    </div>
</div><!--form control-->
<div class="col-xs-12">


<h2>General Section:</h2>
    <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('seo_title', trans('validation.attributes.backend.settings.seo_title'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('seo_title', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.seo_title')]) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('seo_keyword', trans('validation.attributes.backend.settings.seo_keyword'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::textarea('seo_keyword', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.seo_keyword'),'rows' => 2, 'cols' => 4]) !!}
            </div>
        </div>
    </div>
    <!--form control-->
    <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('seo_description', trans('validation.attributes.backend.settings.seo_description'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::textarea('seo_description', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.seo_description'), 'rows' => 2, 'cols' => 4]) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('company_address', trans('validation.attributes.backend.settings.company_address'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::textarea('company_address', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.company_address'),'rows' => 2, 'cols' => 4]) !!}
            </div>
        </div>
       <!--  <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('company_contact', trans('validation.attributes.backend.settings.company_contact'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('company_contact', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.company_contact')]) !!}
            </div>
        </div> -->
    </div>
    <!--form control-->
    <div class="form-group">
        
        <!--form control-->
       <!--  <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('from_name', trans('validation.attributes.backend.settings.from_name'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('from_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.from_name')]) !!}
            </div>
        </div> -->
    </div>

    <!--form control-->
    <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('from_email', trans('validation.attributes.backend.settings.from_email'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('from_email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.from_email')]) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('copyright_text', trans('validation.attributes.backend.settings.copyright_text'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('copyright_text', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.copyright_text')]) !!}
            </div>

        </div>
    </div>
    <!--form control-->
    <!--    <div class="form-group">
    {!! Form::label('facebook', trans('validation.attributes.backend.settings.facebook'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('facebook', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.facebook')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('linkedin', trans('validation.attributes.backend.settings.linkedin'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('linkedin', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.linkedin')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('twitter', trans('validation.attributes.backend.settings.twitter'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('twitter', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.twitter')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('google', trans('validation.attributes.backend.settings.google'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('google', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.google')]) !!}
    </div>
</div>form control-->

    <!--form control-->
   <!--  <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('footer_text', trans('validation.attributes.backend.settings.footer_text'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('footer_text', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.footer_text')]) !!}
            </div>
        </div>
</div> -->
{{-- <h2>Exam Section:</h2>
<div class="form-group">
    <!--form control-->
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('questions_per_topic', trans('validation.attributes.backend.settings.questions_per_topic'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('number_of_questions_per_topic', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.questions_per_topic')]) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('minutes_per_questions', trans('validation.attributes.backend.settings.minutes_per_questions'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('number_of_minutes_per_questions', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.minutes_per_questions')]) !!}
            </div>
        </div>
</div>
<div class="form-group">
    <!--form control-->
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('passing_percentage', trans('validation.attributes.backend.settings.passing_percentage'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('passing_percentage', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.passing_percentage')]) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('min_questions_in_test', trans('validation.attributes.backend.settings.min_questions_in_test'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('min_questions_in_test', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.min_questions_in_test')]) !!}
            </div>
        </div>
</div>
<div class="form-group">
    <!--form control-->
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('effort_for_reappearing_test', trans('validation.attributes.backend.settings.effort_for_reappearing_test'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('reappearing_test', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.effort_for_reappearing_test')]) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('no_of_mark_per_question', 'No. of Mark Per Question', ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('no_of_mark_per_question', null, ['class' => 'form-control', 'placeholder' => 'No. of Mark Per Question']) !!}
            </div>
        </div>
    </div> --}}
<h2>Session Section:</h2>
    <!--form control-->
    <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('higher_level_grade_fee', trans('validation.attributes.backend.settings.higher_level_grade_fee'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('higher_level_grade_fee', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.higher_level_grade_fee'),'maxlength' => '5']) !!}
            </div>
        </div>


    <!--form control-->
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('lower_level_grade_fee', trans('validation.attributes.backend.settings.lower_level_grade_fee'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('lower_level_grade_fee', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.lower_level_grade_fee'),'maxlength' => '5']) !!}
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('tutor_availability_range', trans('validation.attributes.backend.settings.tutor_availability_range'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('tutor_availability_range', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.tutor_availability_range'),'maxlength' => '5']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('free_session_per_fb_count', trans('validation.attributes.backend.settings.free_session_per_fb_count'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('free_session_per_fb_count', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.free_session_per_fb_count'),'maxlength' => '5']) !!}
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('admin_commission_for_higher_level', trans('validation.attributes.backend.settings.admin_commission_for_higher_level'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('admin_commission_for_higher_level', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.admin_commission_for_higher_level'),'maxlength' => '5']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('admin_commission_for_lower_level', trans('validation.attributes.backend.settings.admin_commission_for_lower_level'), ['class' => 'col-lg-4 control-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('admin_commission_for_lower_level', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.admin_commission_for_lower_level'),'maxlength' => '5']) !!}
            </div>
        </div>

    </div>

    <!--    <div class="form-group">

    {!! Form::label('google_analytics', trans('validation.attributes.backend.settings.google_analytics'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::textarea('google_analytics', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.google_analytics')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('home_video1', trans('validation.attributes.backend.settings.home_video1'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('home_video1', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.home_video1')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('home_video2', trans('validation.attributes.backend.settings.home_video2'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('home_video2', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.home_video2')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('home_video3', trans('validation.attributes.backend.settings.home_video3'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('home_video3', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.home_video3')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('home_video4', trans('validation.attributes.backend.settings.home_video4'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('home_video4', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.home_video4')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('explanation1', trans('validation.attributes.backend.settings.explanation1'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('explanation1', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.explanation1')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('explanation2', trans('validation.attributes.backend.settings.explanation2'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('explanation2', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.explanation2')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('explanation3', trans('validation.attributes.backend.settings.explanation3'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('explanation3', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.explanation3')]) !!}
    </div>
</div>form control
<div class="form-group">
    {!! Form::label('explanation4', trans('validation.attributes.backend.settings.explanation4'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('explanation4', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.explanation4')]) !!}
    </div>
</div>form control-->


    <div class="box-footer">
        <a href="{{route('admin.dashboard')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
        <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" />
    </div>
    <!-- /.box-body -->
</div>