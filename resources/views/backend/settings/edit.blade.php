@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.settings.edit'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
<h1>{{ trans('labels.backend.settings.edit') }}<small> EDIT</small></h1>
</div>    
</div>
</div>
@endsection

@section('after-styles-end')
{!! Html::style('css/backend/plugin/jstree/themes/default/style.min.css') !!}
@stop

@section('content')
 <div class="box box-danger">
 <div class="box-body">   
{!!Form::model($settings, ['route' => ['admin.settings.update', $settings->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH',  'files' => true  , 'id' => 'edit-settings']) !!}
@include('backend.settings.form')
{!! Form::close() !!}
@stop
