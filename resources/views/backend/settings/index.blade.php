@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.settings.management') . ' | ' . trans('labels.backend.settings.edit'))

@section('page-header')
<h1>
    {{ trans('labels.backend.settings.management') }}
    <small>{{ trans('labels.backend.settings.edit') }}</small>
</h1>
@endsection

@section('after-styles-end')
{!! Html::style('css/backend/plugin/jstree/themes/default/style.min.css') !!}
@stop

@section('content')
{!! Form::model($settings, ['route' => ['admin.settings.update', $settings->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-settings']) !!}
@include('backend.settings.form')
{!! Form::close() !!}
@stop

@section('after-scripts-end')
{!! Html::script('js/backend/plugin/jstree/jstree.min.js') !!}
{!! Html::script('js/backend/access/roles/script.js') !!}
@stop