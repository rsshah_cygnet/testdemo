@extends('backend.layouts.master')

@section('title', 'Customer Management')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
<h1>Customer Management<small> LISTING</small></h1>
</div>    
</div>
</div>
@endsection

@section('content')
 <div class="box box-danger">
         <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';
    
    moduleConfig = {
        gridURL: "{!! route('admin.customer.data', ['page'=>'1']) !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader), 
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;1:Active;0:InActive",
            stringSearchOptions         = getSearchOption('string'),
            integerSearchOptions        = getSearchOption('integer'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value = 
        {
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [
                {title: false, name:'Name',index:'users.first_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Contact Number',index: 'users.phone_no',searchoptions: integerSearchOptions},
                {title: false, name:'Email Id',index: 'users.email',searchoptions:stringSearchOptions},
                {title: false, name: 'Notify Me',index: 'notifymecount',search:false},
                {title: false, name: 'Status', index: 'users.status',stype: 'select',searchoptions: selectStatusSearchOptions,
                    formatter: function (cellvalue, options, rowObject) 
                    {
                        if (cellvalue == 0) 
                        {
                            return 'InActive'
                        } 
                        else if (cellvalue == 1) 
                        {
                            return 'Active'
                        }
                    }
                },
                {title: false, name: 'act',index: 'act', sortable: false, search: false,width:70}

            ],
            
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop