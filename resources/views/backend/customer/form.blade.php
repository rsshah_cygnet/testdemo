<div class="clear"></div>
<div class="form-horizontal">
    
    <div class="form-group">
    {!! Form::label('first_name', trans('validation.attributes.backend.access.users.first_name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.first_name')]) !!}
    </div>
    </div>
    <!--form control-->

    <div class="form-group">
        {!! Form::label('last_name', trans('validation.attributes.backend.access.users.last_name'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.last_name')]) !!}
        </div>
    </div>
    <!--form control-->

    <div class="form-group">
        {!! Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label required']) !!}
        <div class="col-lg-10">
            {!! Form::text('email', null, ['class' => 'form-control','readonly', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) !!}
        </div>
    </div>
    <!--form control-->

    <div class="form-group">
        {!! Form::label('phone_no', 'Contact Number', ['class' => 'col-lg-2 control-label required']) !!}
        <div class="col-lg-10">
            {!! Form::text('phone_no', null, ['class' => 'form-control', 'readonly', 'placeholder' => 'Contact Number']) !!}
        </div>
    </div>
    <!--form control-->

    <div class="form-group">
        {!! Form::label('Address', 'Address', ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Address'], 'row => 2', 'col => 4') !!}
        </div>
    </div>
    <!--form control-->

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        
        {!! Form::label('status', "Status", ['class' => 'col-lg-2 control-label']) !!}
        
        <div class="col-lg-10">
        <div class="checkbox icheck">
            {{ Form::checkbox('status', 1, (isset($item) && $item->status == 1) ? true : false ) }}
        </div>
        </div>
    </div>

</div>

<div class="clear"></div>