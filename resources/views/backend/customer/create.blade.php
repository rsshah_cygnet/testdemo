@extends ('backend.layouts.master')

@section ('title', 'Brand Models Management')

@section('page-header')
<h1>
    Brand Management
</h1>
@endsection

@section('content')   
	{!! Form::open([
		'route' 	=> 'admin.brand.store', 
		'class' 	=> 'form-horizontal',
		'role' 		=> 'form',
		'method' 	=> 'post',
		'id' 		=> 'admin-brand-model',
		'files' 	=> true
	]) !!}

		{{-- Brand Model Form --}}
		@include('backend.brand.form')
	
		
		<div class="text-center">
			<a href="{!! URL::previous() !!}" class="btn btn-sm btn-default text-center">Cancel</a>
			{!! Form::submit('Save', array("class"=>"btn btn-sm btn-blue text-center")) !!}
		</div>
	
	{!! Form::close() !!}


@stop

@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{
		Imperial.Brand.init();
	});
</script>
@stop