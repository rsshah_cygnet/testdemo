@extends ('backend.layouts.master')

@section ('title', 'Customer Management')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
<h1>Customer Management<small> EDIT</small></h1>
</div>    
</div>
</div>
@endsection


@section('content')  
<div class="box box-danger">
<div class="box-body"> 
	{!!Form::model($item, ['route' => ['admin.customer.update', $item->id],'class' => 'form-horizontal',
		'role' 		=> 'form',
		'method' 	=> 'PATCH',
		'id' 		=> 'edit-customer',
		'files' 	=> true
	]) !!}

	{{-- customer Form --}}
	@include('backend.customer.form')

	<div class="box-footer">
		<a href="{{route('admin.customer.index')}}" class="btn btn-red">Cancel</a>
		{!! Form::submit('Save', array("class"=>"btn btn-info pull-right")) !!}
	</div>

{!! Form::close() !!}
@stop
