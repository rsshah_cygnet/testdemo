@extends('backend.layouts.master')

@section('title', 'Pdf Manager')

@section('page-header')
<div class="page-tatil-box clearfix">
  <div class="row">    
    <div class="col-xs-12 col-lg-6">
      <h1> Pdf Manager</h1>
    </div>    
    <div class="col-xs-12 col-lg-6">      
        <div class="box-tools"> 
            <div class="btn-group">
                {!! Form::open([
                    'class' 	=> 'form-horizontal',
                    'role' 	=> 'form',
                    'url'       => route('admin.dealer.pdf.download'),
                    'method' 	=> 'post',
                    'id' 	=> 'download-selected-vehicle-stock-pdf'
                    ]) !!}
                    {!! Form::hidden('selected_stock', '', array('id' => 'selected_stock')) !!}
                    @permission('email-pdf')
                        {!! Form::submit('EMAIL PDF', array("name" => "email_selected", "id" => "email_selected", "class"=>"btn btn-sm btn-grey-border create-form", "disabled" => "disabled")) !!}
                    @endauth

                    @permission('download-pdf')
                        {!! Form::submit('DOWNLOAD PDF', array("name" => "download_selected", "id" => "download_selected", "class"=>"btn btn-sm btn-grey-border create-form", "disabled" => "disabled")) !!}
                    @endauth

                    @permission('download-pdf')
                        {!! Form::submit('DOWNLOAD STOCKLIST PDF', array("name" => "download_all", "id" => "download_all", "class"=>"btn btn-sm btn-grey-border create-form")) !!}
                    @endauth
                {!! Form::close() !!}
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
@section('modal')
<div class="model-container">
    <div class="modal fade model-wrapper" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">                    
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"> Email PDF </h4>
                </div>                
                <div class="hidden  alert model-alert">    
                </div>
                <div id="model-body" class="modal-body">                
                </div>
            </div>
        </div>
    </div>        
</div>
@stop

@section('content')
    <div class="box box-danger">
        <div class="box-body">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="btn-group"> 
                          <a href="javascript:void(0)" id ="grid-view" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span></a>
                          <a href="javascript:void(0)" id ="card-view" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 hidden" id ="hidden">
                        <div class="box-tools pull-left">
                            <div class="has-feedback data-search-box">
                                <input class="form-control input-sm search" placeholder="Search..." type="text">
                                <span class="glyphicon glyphicon-search form-control-feedback text-muted"></span>
                            </div>
                        </div>
                        <div class="box-tools pull-right">
                            <select class="limit-dropdown">
                                <option value="5">Show 5</option>
                                <option value="10">Show 10</option>
                                <option value="20">Show 20</option>
                                <option value="50">Show 50</option>
                                <option value="100">Show 100</option>
                            </select>
                        </div>
                    </div>  
                </div> 
            </div>   
            <div class="grid-view">
                <table id="list"></table>
                <div id="pager"></div>    
            </div>
            <div class="card-view hidden">
                @include('backend.pdfmanager.list')
            </div>
        </div>
    </div>
@stop
@section('after-scripts-end')
<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.dealer.pdf.data', ['page'=>'1']) !!}",
        listURL:  "{!! route('admin.dealer.pdf.list') !!}",
        emailPdfFormURL: "{!! route('admin.dealer.pdf.emailform') !!}",
        emailPdfFormSubmitURL: "{!! route('admin.dealer.pdf.sendemail') !!}"
    };

    jQuery(function ()
    {
        var displayColumn               = '{!! json_encode($stockRepository->pdfColumn) !!}',
            displayColumnHeader         = '{!! json_encode($stockRepository->pdfColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader), 
            defaultOrderBy              = '{!! $stockRepository->pdfDefaultOrderBy!!}',
            displayColumnArray          = ['Select'],
            displayColumns              = eval(displayColumn),
            StatusStr                   = ":All;Active:Active;InActive:InActive",
            stringSearchOptions         = getSearchOption('string'),
            integerSearchOptions        = getSearchOption('integer'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr),
            dateSearchOptions           = getSearchOption('date');


        jQuery.each(displayColumns, function(index, element)
        {
            displayColumnArray.push(displayColumnHeaderArray[index]);
        });

        var Required_Key_Value = 
        {
            'shrinkToFit':              true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [
                {title: false,name:'Stock No',index:'vehicle_stocks.id',searchoptions:integerSearchOptions},
                {title: false, name: 'Date',  index: 'vehicle_stocks.adoption_date',stype:'date',searchoptions:dateSearchOptions},
                {title: false,name: 'Year',index:'vehicle_stocks.model_year',searchoptions:integerSearchOptions},
                {title: false,name:'Car Searies',index: 'vehicle_stocks.listing_title',
                    searchoptions: stringSearchOptions},
                {title: false, name: 'Price',  index: 'vehicle_stocks.actual_cost',searchoptions:integerSearchOptions},
                {title: false, name: 'MM Code',  index: 'vehicle_stocks.mead_mcgrouther_code',searchoptions:integerSearchOptions},
                {title: false, name: 'Mileage',  index: 'vehicle_stocks.odometer_reading',searchoptions:integerSearchOptions},
                {title: false, name: 'Mileage',  index: 'price_to_go_vehicles.pricetogo',searchoptions:integerSearchOptions},
                {title: false, name: 'act', index: 'act', sortable: false, search: false,width:90},
            ],
        };
        CustomGrid(Required_Key_Value);
    });

    function getSelectedRowData(id)
    {
      var searchIDs = $("input[name='stock[]']:checked").map(function(){
        return $(this).val();
      }).get();        
      if(searchIDs.length)
      {
        $("#selected_stock").val(searchIDs.toString()); 
        $("#download_selected").prop('disabled', false);
        $("#email_selected").prop('disabled', false);
      }
      else
      {
        $("#selected_stock").val("");
        $("#download_selected").prop('disabled', true);
        $("#email_selected").prop('disabled', true);
      }
    }
    
    //Grid View
    $( document ).on( 'click', '#grid-view', function()
    {
        $('.card-view').addClass("hidden");
        $('#hidden').addClass("hidden");
        $('.grid-view').show();
    });

    //Card View
    $( document ).on( 'click', '#card-view', function()
    {
        $('.grid-view').hide();
        $('.card-view').removeClass('hidden');
        $('#hidden').removeClass("hidden");
    });

    //slect dropdown for records limit (show 5 , show 10)
    $( document ).on( 'change', '.limit-dropdown', function()
    {
        getList(moduleConfig.listURL);
    });

    //Search Special
    $( document ).on( 'keyup', '.search', function()
    {
        getList(moduleConfig.listURL);
    });

    //get List data and append it to div
    function getList(url)
    {
        var limit = $('.limit-dropdown').val();
        var query = $('.search').val();
        
        data = {'limit' : limit, 'query':query};
        
        $.ajax({
            type:"POST",
            url : url,
            data : data,
            async: false,
            success : function(data) {
                $( ".card-view" ).empty();
                $( ".card-view" ).append(data);
            },
            error: function() {
                alert('Error occured');
            }
        });
    }
    
     //hide the model on click of close button
    $( document ).on( 'click', '.close-model', function() 
    {
        $(".model-wrapper").modal('hide');
    });

    //hide the alert on close event of modal
    $('#myModal').on('hidden.bs.modal', function() 
    {
        $("div.model-alert").addClass("hidden");
    });

    $( document ).on( 'click', '#email_selected', function(e)
    {
        e.preventDefault();
        showModalForm('email_pdf');
    });

    $( document ).on( 'submit', 'form#email-pdf-form', function(e) {
        e.preventDefault();
        $(this).append('<input type="hidden" name="selected_stock" value="'+$("#selected_stock").val()+'">');
        var data    = new FormData(this);
        submitModalForm(data);
        
    });
    
    function showModalForm(option, id)
    {
        var url = moduleConfig.emailPdfFormURL;
        jQuery.ajax(
        {
            type:   "GET",
            url:    url,
            async:  true,
            success: function(data)
            {    
                jQuery( ".modal-body" ).html(data);
                $(".model-wrapper").modal('show');
            }
        });
    }
    
    function submitModalForm(data){
        var url = moduleConfig.emailPdfFormSubmitURL;
        
        jQuery.ajax({
            type:   "POST",
            url:    url,
            data: data,
            processData: false,
            contentType: false,
            success: function(data)
            {
                if(data.status)
                {
                    $("div.page-alert" ).html(data.message).addClass("success alert-success");
                    $(".model-wrapper").modal('hide');
                    $("div.page-alert").removeClass("hidden");
                    $("#selected_stock").val("");
                    $(".stock").removeAttr('checked');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) 
            {
                if(errorThrown)
                {
                    $("div.model-alert").html("Invalid Input").addClass("danger alert-danger");
                    $("div.model-alert").removeClass("hidden");
                }
            }
        });
    }

</script>
@stop