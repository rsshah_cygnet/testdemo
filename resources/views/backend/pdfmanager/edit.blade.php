@extends ('backend.layouts.master')

@section ('title', 'Special Management')

@section('page-header')
<h1>
    Special Management
    <small>Edit</small>
</h1>
@endsection

@section('content')
	{!!Form::model($item, ['route' => ['admin.special-management.update', $item->id],'class' => 'form-horizontal',
		'role' 		=> 'form',
		'method' 	=> 'PATCH',
		'files' 	=>  true,
		'id' 		=> 'edit-special']) !!}

		{{-- Campaign Form --}}
		@include('backend.specialmanagement.form')

		<div class="text-center">
			<a href="{{route('admin.special-management.index')}}" class="btn btn-sm btn-default">Cancel</a>
			{!! Form::submit('Save', array("class"=>"btn btn-sm btn-blue")) !!}
		</div>

	{!! Form::close() !!}
@endsection