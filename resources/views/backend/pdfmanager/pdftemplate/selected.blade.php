<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
    <HEAD>
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META http-equiv="X-UA-Compatible" content="IE=8">
        <TITLE>Vehicle Stock</TITLE>
    </HEAD>
    <BODY>
        @foreach ($vehicleStock as $vehicleStockItem)
        <h2>Vehicle Stock ID: {{ $vehicleStockItem->id }}</h2>
        <div>
            <h3>Vehicle Description</h3>
            <p><b>Title: </b> {{ $vehicleStockItem->listing_title }}</p>
            <p><b>Vehicle Description:</b> {{$vehicleStockItem->description}}</p>
            <p><img src="{{ public_path('images/dealer-car-2.jpg') }}" alt="Image"></p>
            <br/>
            <h3>Vehicle Details</h3>
            <p><b>Details:</b> {{ $vehicleStockItem->listing_title }}</p>
            <p><b>Price:</b> {{ @$vehicleStockItem->vehicle->selling_price }}</p>
            <p><b>Mileage:</b> </p>
            <p><b>Year:</b> {{ $vehicleStockItem->model_year }}</p>
            <p><b>Colour:</b> {{ $vehicleStockItem->colour_description }}</p>
            <p><b>Transmission:</b> </p>
            <p><b>Fuel Type:</b> </p>
            <p><b>Stock Number:</b> {{ $vehicleStockItem->vehicle_identification_number }}</p>
            <p><b>Features:</b> <b>Interior: </b>{{ @$vehicleStockItem->vehicle->interior }}</p>
            <p><b>Features:</b> <b>Exterior: </b>{{ @$vehicleStockItem->vehicle->exterior }}</p>
            <br/>
            <h3>Dealer Details</h3>
            <p><b>Name:</b> {{ $vehicleStockItem->dealership->dealership_name }}</p>
            <p><b>Email:</b> {{ $vehicleStockItem->dealership->dealer_principal_email }}</p>
            <p><b>Contact Number:</b> {{ $vehicleStockItem->dealership->dealer_principal_contact_no }}</p>
        </div>
        <hr/>
        @endforeach
    </BODY>
</HTML>