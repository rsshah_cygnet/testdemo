@section('content')
	{!! Form::open([
		'class' 	=> 'form-horizontal',
		'role' 		=> 'form',
		'method' 	=> 'post',
		'files' 	=>  true,
		'id' 		=> 'admin-special'
	]) !!}

	{{-- Brand Model Form --}}
	@include('backend.specialmanagement.form')


	<div class="text-center">
		<a href="javascript:void(0)" class="btn btn-sm btn-default text-center close-model">Cancel</a>
		{!! Form::submit('Save', array("class"=>"btn btn-sm btn-blue text-center")) !!}
	</div>

{!! Form::close() !!}

