
<div id="products" class="row list-group">
@foreach($data as $item)
    @if(isset($transmission))
        @foreach($transmission as $specification)
            @if(isset($specification))
                @if($specification->vehicle_id == $item->vehicle_id)
                    @php
                        $property = $specification->old_specification_property;
                        $image = $specification->photo;
                    @endphp
                @endif
            @endif
        @endforeach
    @endif
    
    <div class="item col-xs-12 col-md-6 col-lg-4 grid-group-item">
        <div class="thumbnail">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="pull-left">
                        <span> {{$item->vehicle_identification_number}} </span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6">
                    <div class="pull-right">
                        <span> Add To Pdf <input type="checkbox" name="stock" id="'.$item->id.'" class="stock"  value="'.$item->id.'"></span>
                    </div>
                </div>
            </div>

            <div class="list-group-image">
                <img class="group" src="{{url('/')}}/uploads/vehicle_management/{{$item->vehicle_id}}/thumbnail/{{$image}}">
            </div>

            <div class="caption">
                <p class="group inner list-group-item-text text-center text-red">
                    RS. {{$item->actual_cost}}
                </p>

                <h4 class="group inner list-group-item-heading">
                    {{$item->vehicle->brands->brand_name}} {{$item->vehicle->brandModels->brand_model_name}}
                </h4>
                
                <p class="group inner list-group-item-text">
                    {{$item->listing_title}}
                </p>
                <p class="group inner list-group-item-text">
                    {{$property}}
                </p>
                
                <p class="group inner list-group-item-text">
                    {{$item->odometer_reading}}
                </p>
                
                <p class="group inner list-group-item-text">
                    {{$item->vehicle->vehicleTypes->vehicle_type}}
                </p>
            </div>
        </div>  
    </div>
@endforeach
</div>


<nav aria-label="Page navigation" class="pagination-bottom">
  <ul class="pagination">
    <li>
      {{ $data->links() }}        
    </li>
  </ul>
</nav>