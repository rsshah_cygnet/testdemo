{!! Form::open([
	'class' 	=> 'form-horizontal',
	'role' 		=> 'form',
	'method' 	=> 'get',
	'id' 		=> 'email-pdf-form',
	'files' 	=> true
]) !!}
	{{-- PDF Email Form --}}
        <div class="form-group">
            {!! Form::label('user_name', 'Name', ['class' => 'col-lg-3 control-label required']) !!}
            <div class="col-lg-9">
                {!! Form::text('user_name', null, ['class' => 'form-control', 'placeholder' => 'Name' , 'required']) !!}
            </div>
        </div>
	<div class="form-group">
            {!! Form::label('email_address', 'Email Address', ['class' => 'col-lg-3 control-label required']) !!}
            <div class="col-lg-9">
                {!! Form::email('email_address', null, ['class' => 'form-control', 'placeholder' => 'Email Address' , 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('body_text', 'Message', ['class' => 'col-lg-3 control-label required']) !!}
            <div class="col-lg-9">
                {!! Form::textarea('body_text', null, ['class' => 'form-control', 'placeholder' => 'Message' , 'required']) !!}
            </div>
        </div>
	<div class="text-center">
            <a href="javascript:void(0)" class="btn btn-sm btn-default text-center close-model">Cancel</a>
            {!! Form::submit('Send', array("class"=>"btn btn-sm btn-blue text-center store-form")) !!}
	</div>
{!! Form::close() !!}


@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{
		Imperial.Brand.init();
	});
</script>
@stop