<div class="clear"></div>
<div id="custom_dropdowns">
<div class="form-group">
    {!! Form::label('levels_name', trans('validation.attributes.backend.level.name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('levels_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.level.name'),'required' =>' required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('curriculum_id', trans('validation.attributes.backend.level.curriculum'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('curriculum_id', ['' => 'Select curriculum'] + $curriculums ,isset($levels->curriculum_id)?$levels->curriculum_id:"", ['class' => 'form-control select2' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required' =>' required']) !!}
    </div>
</div><!--form-group-->

</div>


@if(isset($formtype) && $formtype == "edit")
{{-- <div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
        <div class="col-lg-1">
        <div class="checkbox icheck">
            <label>{{ Form::checkbox('record_status', 1, (isset($levels->status) && $levels->status == '1') ? true : false ) }}</label>
        </div>
    </div>
</div> --}}
<input type="hidden" value="{{$levels->curriculum_id or ''}}" name="curriculum_id">
@endif
<input type="hidden" value="{{$levels->id or ''}}" name="levels_id">
<input type="hidden" value="levels" name="type" id="management_type">

<div class="box-footer">
    <a href="{{route('admin.levels.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a> @if(isset($level))
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" /> @else
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" /> @endif
</div>
<!-- /.box-body -->