@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.level.edit'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12 col-lg-6"><h1>{{ trans('labels.backend.level.management') }}<small> EDIT</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.Levels.includes.header-buttons')
</div>
</div>
</div>
</div>


@endsection
<?php
// dd($levels);
?>
@section('content')
    <div class="box box-danger">
    <div class="box-body">
    {!! Form::model($levels, ['route' => ['admin.levels.store'], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST']) !!}
            @include('backend.Levels.form')
    {!! Form::close() !!}
@endsection

@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{

			//getEducational($('#curriculum_id').val());
			getCurriculumHierarchy("curriculum_id");
			trrigerChange();

			$('body').on('change','.dynamic_dropdown',function(){
					$("#curriculum_id").prop('disabled', true);
					$("#eductional_systems_id").prop('disabled', true);
					$("#program_id").prop('disabled', true);
					$("#grade_id").prop('disabled', true);
					$("#level_id").prop('disabled', true);
					$("#subject_id").prop('disabled', true);
					trrigerChange();
			})

			var level_trriger = true;
			var grade_trriger = true;
			var education_trriger = true;
			var subject_trriger = true;
			var program_trriger = true;

			function trrigerChange(){
				setTimeout(function(){

							var curriculum = $('#curriculum_id').length;
						    var eductional_systems = $('#eductional_systems_id').length;
						    var program = $('#program_id').length;
						    var subject = $('#subject_id').length;
						    var grade = $('#grade_id').length;
						    var level = $('#level_id').length;

						var selected_education = "<?php echo isset($levels->eductional_systems_id) ? $levels->eductional_systems_id : ''; ?>";
						var selected_grade = "<?php echo isset($levels->grade_id) ? $levels->grade_id : ''; ?>";
						var selected_subject = "<?php echo isset($levels->subject_id) ? $levels->subject_id : ''; ?>";
						var selected_program = "<?php echo isset($levels->program_id) ? $levels->program_id : ''; ?>";

						//console.log(selected_grade);
						if(selected_grade != 0 && selected_grade !="" && selected_grade != "null" && grade > 0 && grade_trriger == true){
							$('#grade_id').val(selected_grade).trigger("change");
							grade_trriger = false;
						}
						if(selected_education != 0 && selected_education !="" && selected_education != "null" && eductional_systems > 0 && education_trriger == true){
							$('#eductional_systems_id').val(selected_education).trigger("change");
							education_trriger = false;
						}
						if(selected_subject != 0 && selected_subject !="" && selected_subject != "null" && subject > 0 && subject_trriger == true){
							$('#subject_id').val(selected_subject).trigger("change");
							subject_trriger = false;
						}
						if(selected_program != 0 && selected_program != "" && selected_program != "null" && program > 0 && program_trriger == true){
							$('#program_id').val(selected_program).trigger("change");
							program_trriger = false;
						}

			 		}, 300);
			}



	});

</script>
@stop
