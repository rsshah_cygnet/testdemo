<div class="btn-group">
    
    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>
        
    </button>

    <ul class="dropdown-menu" role="menu">

        @permission('view-level-management')

        <li>
            <a href="{{ route('admin.levels.index') }}">
                {{ trans('menus.backend.levels.all') }}
            </a>
        </li>
        
        @endauth

        @permission('create-level')
        
        <li>
            <a href="{{ route('admin.levels.create') }}">
                {{ trans('menus.backend.levels.create') }}
            </a>
        </li>
        
        @endauth
    </ul>
</div><!--btn group-->
<div class="clearfix"></div>