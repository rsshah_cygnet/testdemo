@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.level.create'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12 col-lg-6"><h1>{{ trans('labels.backend.level.management') }}<small> CREATE</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.Levels.includes.header-buttons')
</div>
</div>
</div>
</div>

@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => 'admin.levels.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@include('backend.Levels.form')
{!! Form::close() !!}
@endsection
