@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.create'))

@section('page-header')
<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6"><h1><strong>ADMIN USER MANAGEMENT </strong><small> CREATE</small></h1></div>
        <div class="col-xs-12 col-lg-6">
            <div class="box-tools">
                @include('backend.access.includes.partials.header-buttons')
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-styles-end')
{!! Html::style('css/backend/plugin/jstree/themes/default/style.min.css') !!}
@stop

@section('content')
<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => 'admin.access.users.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-user']) !!}

<div class="form-group">
    {!! Form::label('first_name', trans('validation.attributes.backend.access.users.first_name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.first_name')]) !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('last_name', trans('validation.attributes.backend.access.users.last_name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.last_name')]) !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('country','Country', ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::select('country_id', ['' => 'Select Country'] + $country, isset($user->country_id)?$user->country_id:'', ['class' => 'form-control','id' => 'country_id','onchange'=>'getCity()']) !!}
    </div>
</div>
<!--form control-->

<div class="form-group">
    {!! Form::label('city','City', ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::select('city_id', ['' => 'Select City'] + $city, isset($user->city_id)?$user->cityy_id:'', ['class' => 'form-control','id' => 'city_id']) !!}
    </div>
</div>
<!--form control-->

<div class="form-group">
    {!! Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.email'), 'maxlength' => 100]) !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'col-lg-2 control-label required', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) !!}
    <div class="col-lg-10">
        {!! Form::password('password', ['class' => 'form-control', 'maxlength' => 60]) !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'col-lg-2 control-label required', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation')]) !!}
    <div class="col-lg-10">
        {!! Form::password('password_confirmation', ['class' => 'form-control' , 'maxlength' => 60]) !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('phone_no', trans('validation.attributes.backend.access.users.phone_no'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('contact_no', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.phone_no')]) !!}
    </div>
</div><!--form control-->

<div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
    <div class="col-lg-1">
        <div class="checkbox icheck">
            <label><input type="checkbox" value="1" name="status" checked="checked" /></label>
        </div>
    </div>
</div>


<div class="form-group">
    <label class="col-lg-2 control-label required">{{ trans('validation.attributes.backend.access.users.associated_roles') }}</label>
    <div class="col-lg-10">
        @if (count($roles) > 0)
        @foreach($roles as $role)
        <div class="radio icheck">
        <label for="role-{{$role->id}}"><input type="radio" value="{{$role->id}}" name="assignees_roles[]" id="role-{{$role->id}}" class="get-role-for-permissions" /> <label for="role-{{$role->id}}">{!! $role->name !!}</label>
        <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
            (
            <span class="show-text">{{ trans('labels.general.show') }}</span>
            <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
            {{ trans('labels.backend.access.users.permissions') }}
            )
        </a>
        </label>
        </div>
        <br/>
        <div class="permission-list hidden" data-role="role_{{$role->id}}">
            @if ($role->all)
            {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
            @else
            @if (count($role->permissions) > 0)
            <blockquote class="small">{{--
                                        --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                @endforeach
            </blockquote>
            @else
            {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
            @endif
            @endif
        </div><!--permission list-->
        @endforeach
        @else
        {{ trans('labels.backend.access.users.no_roles') }}
        @endif
    </div>
</div><!--form control-->


<!-- grouped permissions -->
<div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.other_permissions') }}</label>
    <div class="col-lg-10">

        <div id="available-permissions">
            <div class="row">

                <div class="col-lg-12">
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i>
                        {!! getLanguageBlock('backend.lang.access.roles.associated-permissions-explanation') !!}
                    </div><!--alert-->
                </div><!--col-lg-12-->

                <div class="col-lg-6">
                    <p><strong>{{ trans('labels.backend.access.permissions.grouped_permissions') }}</strong></p>

                    @if ($groups->count())
                    <div id="permission-tree">
                        <ul>
                            @foreach ($groups as $group)
                            <li>{!! $group->name !!}
                                @if ($group->permissions->count())
                                <ul>
                                    @foreach ($group->permissions as $permission)
                                    <li id="{!! $permission->id !!}" data-dependencies="{!! json_encode($permission->dependencies->lists('dependency_id')->all()) !!}">

                                        @if ($permission->dependencies->count())
                                        <?php
//Get the dependency list for the tooltip
$dependency_list = [];
foreach ($permission->dependencies as $dependency) {
	array_push($dependency_list, $dependency->permission->display_name);
}

$dependency_list = implode(", ", $dependency_list);
?>
                                        <a data-toggle="tooltip" data-html="true" title="<strong>{{ trans('labels.backend.access.permissions.dependencies') }}:</strong> {!! $dependency_list !!}">{!! $permission->display_name !!} <small><strong>(D)</strong></small></a>
                                        @else
                                        {!! $permission->display_name !!}
                                        @endif

                                    </li>
                                    @endforeach
                                </ul>
                                @endif

                                @if ($group->children->count())
                                <ul>
                                    @foreach ($group->children as $child)
                                    <li>{!! $child->name !!}
                                        @if ($child->permissions->count())
                                        <ul> style="padding-left:40px;font-size:.8em">
                                            @foreach ($child->permissions as $permission)
                                            <li id="{!! $permission->id !!}" data-dependencies="{!! json_encode($permission->dependencies->lists('dependency_id')->all()) !!}">
                                                @if ($permission->dependencies->count())
                                                <?php
//Get the dependency list for the tooltip
$dependency_list = [];
foreach ($permission->dependencies as $dependency) {
	array_push($dependency_list, $dependency->permission->display_name);
}

$dependency_list = implode(", ", $dependency_list);
?>
                                                <a data-toggle="tooltip" data-html="true" title="<strong>{{ trans('labels.backend.access.permissions.dependencies') }}:</strong> {!! $dependency_list !!}">{!! $permission->display_name !!} <small><strong>(D)</strong></small></a>
                                                @else
                                                {!! $permission->display_name !!}
                                                @endif

                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @else
                    <p>{{ trans('labels.backend.access.permissions.no_groups') }}</p>
                    @endif
                </div><!--col-lg-6-->

                <div class="col-lg-6">
                    <p><strong>{{ trans('labels.backend.access.permissions.ungrouped_permissions') }}</strong></p>

                    @if ($permissions->count())
                    @foreach ($permissions as $perm)
                    <input type="checkbox" name="ungrouped[]" data-dependencies="{!! json_encode($perm->dependencies->lists('dependency_id')->all()) !!}" value="{!! $perm->id !!}" id="perm_{!! $perm->id !!}" /> <label for="perm_{!! $perm->id !!}">

                        @if ($perm->dependencies->count())
                        <?php
//Get the dependency list for the tooltip
$dependency_list = [];
foreach ($perm->dependencies as $dependency) {
	array_push($dependency_list, $dependency->permission->display_name);
}

$dependency_list = implode(", ", $dependency_list);
?>
                        <a style="color:black;text-decoration:none;" data-toggle="tooltip" data-html="true" title="<strong>{{ trans('labels.backend.access.permissions.dependencies') }}:</strong> {!! $dependency_list !!}">{!! $perm->display_name !!} <small><strong>(D)</strong></small></a>
                        @else
                        {!! $perm->display_name !!}
                        @endif

                    </label><br/>
                    @endforeach
                    @else
                    <p>{{ trans('labels.backend.access.permissions.no_ungrouped') }}</p>
                    @endif
                </div><!--col-lg-6-->
            </div><!--row-->
        </div><!--available permissions-->
    </div><!--col-lg-3-->
</div><!--form control-->
</div>
<div class="box-footer">
    <a href="{{route('admin.access.users.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" />
</div>
</div>

{!! Form::hidden('permissions') !!}
{!! Form::close() !!}
@stop

@section('after-scripts-end')
{!! Html::script('js/backend/plugin/jstree/jstree.min.js') !!}
{!! Html::script('js/backend/access/permissions/script.js') !!}
{!! Html::script('js/backend/access/users/script.js') !!}
@stop
