@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
	<div class="col-xs-12 col-lg-6"><h1>Admin User Management<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.access.includes.partials.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
    <div class="box box-danger">
         <div class="box-body">
		<table id="list"></table>
		<div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->


<!-- /.box-body -->

<!--box-->

@stop @section('after-scripts-end')

<script type="text/javascript">
    var moduleConfig = '';
    moduleConfig = {
        gridURL: "{!! route('admin.access.users.data', ['page'=>'1']) !!}",
        deleteSelectedItems: "{!! route('admin.access.users.deleteAll') !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    jQuery(function () {
        var StatusStr = ":All;1:Yes;0:No";
        var stringSearchOptions = getSearchOption('string');
        var selectStatusSearchOptions = getSearchOption('select', StatusStr);

        var Required_Key_Value = {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'columnFilterToolbar': true,
            'shrinkToFit': true,
            'gridLoadUrl': "{!! route('admin.access.users.data', ['page'=>'1']) !!}",
            'displayColumn': ["First Name", "Last Name","Country","City", "E-mail", "ContactNo","Status", "Roles",  'Actions'],
            'dbColumn': [
                {
                    title: false,
                    name: "first_name",
                    width: '150',
                    searchoptions: stringSearchOptions
                },
                {
                    title: false,
                    name: "last_name",
                    width: '150',
                    searchoptions: stringSearchOptions
                },
                {
                    title: false,
                    name: "country_name",
                    width: '150',
                    searchoptions: stringSearchOptions
                },
                {
                    title: false,
                    name: "city_name",
                    width: '150',
                    searchoptions: stringSearchOptions
                },
                {
                    title: false,
                    name: "email",
                    width: '150',
                    searchoptions: stringSearchOptions
                },
                {
                    title: false,
                    name: "contact_no",
                    width: '200',
                    searchoptions: stringSearchOptions
                },
                {
                    sortable: true,
                    title: false,
                    name: "users.status",
                    width: '130',
                    stype: 'select',
                    searchoptions: selectStatusSearchOptions
                },
                {
                    search: false,
                    sortable: false,
                    title: false,
                    name: "role",
                    width: '200'
                },
                {
                    title: false,
                    name: 'act',
                    index: 'act',
                    width: '200',
                    sortable: false,
                    search: false
                },
            ],
            'defaultSortColumnName': "id",
            'defaultSortOrder': "desc",

        };
        CustomGrid(Required_Key_Value);
    });

    //jQuery("#gbox_list").jqGrid('setFrozenColumns');

//    $("#gbox_list").jqGrid("destroyFrozenColumns")
//            .jqGrid("setColProp", "id", { frozen: true })
//            .jqGrid("setFrozenColumns")
//            .trigger("reloadGrid", [{ current: true}]);
</script>
@stop