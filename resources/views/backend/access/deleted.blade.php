<h3 class="box-title">{{ trans('labels.backend.access.users.deactivated') }}</h3>

<div class="box-tools pull-right">
    @include('backend.access.includes.partials.header-buttons')
</div>

<div class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-9">
            <div class="dataTables_length">
                {!! Form::label('type', trans('labels.backend.page_size')) !!} {!! Form::select('pagesize', array('10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30' ), null, ['id'=>'pagesize']) !!}
                <button id="trigerDelete" class="btn bg-purple btn-flat">{{ trans('labels.backend.delete') }}</button>
                @if ( count($users) > 0 )
                <a class="btn bg-purple btn-flat" href="{{ route('admin.access.users.export') }}">{{ trans('labels.backend.export_excel') }}</a>
                <a class="btn bg-purple btn-flat" href="{{ route('admin.access.users.csv') }}">{{ trans('labels.backend.export_csv') }}</a> @endif
            </div>
        </div>
        <div class="col-sm-3">
            <div id="grid-table_filter" class="dataTables_filter">
                <label>Search: {!! Form::text('search-top', null, ['class' => 'form-control input-sm','id' => 'search-top', 'placeholder' => trans('labels.backend.search')]) !!}</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!--hidden fields for sorting--> 
            {!! Form::hidden('orderby', 'id', ['class' => 'form-control input-sm','id' => 'orderby']) !!} {!! Form::hidden('sort', 'asc', ['class' => 'form-control input-sm','id' => 'sort']) !!}
            <table id="grid-table" class="table table-striped table-hover table-bordered dataTable" role="grid" aria-describedby="grid-table_info">
                <thead>
                    <tr>
                        <th>
                        <input type="checkbox" id="bulkDelete" /> </th>
                        <th class="sorting" data-orderby="id" data-sort="asc">{{ trans('labels.backend.access.users.table.id') }}</th>
                        <th class="sorting" data-orderby="name" data-sort="asc">{{ trans('labels.backend.access.users.table.name') }}</th>
                        <th class="sorting" data-orderby="email" data-sort="asc">{{ trans('labels.backend.access.users.table.email') }}</th>
                        <th class="sorting" data-orderby="confirmed" data-sort="asc">{{ trans('labels.backend.access.users.table.confirmed') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.roles') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.other_permissions') }}</th>
                        <th>{{ trans('labels.backend.salarytypes.table.created') }}</th>
                        <th>{{ trans('labels.backend.salarytypes.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>

                    @if ($users->count())
                    @foreach ($users as $user)
                    <tr>
                        <td>{!! $user->id !!}</td>
                        <td>{!! $user->first_name !!}</td>
                        <td>{!! link_to("mailto:".$user->email, $user->email) !!}</td>
                        <td>{!! $user->confirmed_label !!}</td>
                        <td>
                            @if ($user->roles()->count() > 0)
                            @foreach ($user->roles as $role)
                            {!! $role->name !!}<br/>
                            @endforeach
                            @else
                            {{ trans('labels.general.none')}}
                            @endif
                        </td>
                        <td>
                            @if ($user->permissions()->count() > 0)
                            @foreach ($user->permissions as $perm)
                            {!! $perm->display_name !!}<br/>
                            @endforeach
                            @else
                            {{ trans('labels.general.none') }}
                            @endif
                        </td>
                        <td class="visible-lg">{!! $user->created_at->diffForHumans() !!}</td>
                        <td class="visible-lg">{!! $user->updated_at->diffForHumans() !!}</td>
                        <td>
                            @permission('undelete-users')
                            <a href="{{route('admin.access.user.restore', $user->id)}}" class="btn btn-xs btn-success" name="restore_user"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.backend.access.users.restore_user') }}"></i></a>
                            @endauth

                            @permission('permanently-delete-users')
                            <a href="{{route('admin.access.user.delete-permanently', $user->id)}}" class="btn btn-xs btn-danger" name="delete_user_perm"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.backend.access.users.delete_permanently') }}"></i></a>
                            @endauth
                        </td>
                    </tr>
                    @endforeach
                    @else
                <td colspan="9">{{ trans('labels.backend.access.users.table.no_deleted') }}</td>
                @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>
                            <label>{!! Form::text('name', null, ['class' => 'form-control input-sm','id' => 'name', 'placeholder' => trans('labels.backend.access.users.placeHolders.name')]) !!}</label>
                        </td>
                        <td>
                            <label>{!! Form::text('email', null, ['class' => 'form-control input-sm','id' => 'email', 'placeholder' => trans('labels.backend.access.users.placeHolders.email')]) !!}</label>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="pull-left">
    {!! $users->total() !!} {{ trans_choice('labels.backend.access.users.table.total', $users->total()) }}
</div>

<div class="pull-right">
    {!! $users->render() !!}
</div>
<div class="clearfix"></div>

@section('after-scripts-end')
<script>
    $(function() {
    @permission('permanently-delete-users')
            $("a[name='delete_user_perm']").click(function(e) {
    e.preventDefault();
            swal({
            title: "{!! trans('strings.backend.general.are_you_sure') !!}",
                    text: "{!! trans('strings.backend.access.users.delete_user_confirm') !!}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{!! trans('strings.backend.general.continue') !!}",
                    closeOnConfirm: false
            }, function(isConfirmed){
            if (isConfirmed){
            window.location = $("a[name='delete_user_perm']").attr('href');
            }
            });
    });
            @endauth

            @permission('undelete-users')
            $("a[name='restore_user']").click(function(e) {
    e.preventDefault();
            swal({
            title: "{{ trans('strings.backend.general.are_you_sure') }}",
                    text: "{{ trans('strings.backend.access.users.restore_user_confirm') }}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{ trans('strings.backend.general.continue') }}",
                    closeOnConfirm: false
            }, function(isConfirmed){
            if (isConfirmed){
            window.location = $("a[name='restore_user']").attr('href');
            }
            });
    });
            @endauth
    });
</script>
@stop
