@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.deleted'))

@section('page-header')
<h1>
    {{ trans('labels.backend.access.users.management') }}
    <small>{{ trans('labels.backend.access.users.deleted') }}</small>
</h1>
@endsection

@section('content')

<div class="box-tools pull-right">
    @include('backend.access.includes.partials.header-buttons')
</div>

<div class="clear"></div>

<table id="list"></table>
<div id="pager"></div>

<!-- /.box-body -->

<!--box-->

@stop @section('after-scripts-end')

<script type="text/javascript">
    var moduleConfig = '';
    moduleConfig = {
        gridURL: "{!! route('admin.access.users.deleted', ['page'=>'1']) !!}",
        deleteSelectedItems: "{!! route('admin.access.users.deleteAll') !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    jQuery(function () {
        var StatusStr = ":All;1:Yes;0:No";
        var stringSearchOptions = getSearchOption('string');
        var selectStatusSearchOptions = getSearchOption('select', StatusStr);

        var Required_Key_Value = {
            'frozanStartColumnName': 'act',
            'frozanNumberOfColumns': '1',
            'columnFilterToolbar': true,
            'searchFooter': true,
            'gridLoadUrl': "{!! route('admin.access.users.deleted', ['page'=>'1']) !!}",
            'displayColumn': ['Actions', "Id", "Name", "E-mail", "Confirmed", "Roles", "Permissions", "Created", "Updated"],
            'dbColumn': [
                {
                    title: false,
                    name: 'act',
                    index: 'act',
                    width: '100',
                    sortable: false,
                    search: false
                },
                {
                    title: false,
                    name: "id",
                    width: '100',
                    sortable: true,
                },
                {
                    title: false,
                    name: "name",
                    width: '200',
                    searchoptions: stringSearchOptions
                },
                {
                    title: false,
                    name: "email",
                    width: '200',
                    searchoptions: stringSearchOptions
                },
                {
                    sortable: true,
                    title: false,
                    name: "confirmed",
                    width: '100',
                    stype: 'select',
                    searchoptions: selectStatusSearchOptions
                },
                {
                    search: false,
                    sortable: false,
                    title: false,
                    name: "role",
                    width: '200'
                },
                {
                    search: false,
                    sortable: false,
                    title: false,
                    name: "permission",
                    width: '200'
                },
                {
                    search: false,
                    title: false,
                    name: "created_at",
                    width: '200',
                    sortable: false
                },
                {
                    search: false,
                    title: false,
                    name: "updated_at",
                    width: '200',
                    sortable: false
                }
            ],
            'defaultSortColumnName': "name",
        };
        CustomGrid(Required_Key_Value);
    });
</script>
@stop