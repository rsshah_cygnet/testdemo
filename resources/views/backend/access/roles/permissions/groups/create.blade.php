@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.permissions.groups.create'))

@section('page-header')
<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6"><h1>Group Management<small> CREATE</small></h1></div>
        <div class="col-xs-12 col-lg-6">
            <div class="box-tools">
                @include('backend.access.includes.partials.header-buttons')
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => 'admin.access.roles.permission-group.store', 'class' => 'form-horizontal', 'role' => 'form']) !!}

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.access.permissions.groups.name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.permissions.groups.name')]) !!}
    </div>
</div><!--form control-->


<div class="box-footer">
    <a href="{!! route('admin.access.roles.permissions.index') !!}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" />
</div>

{!! Form::close() !!}
@stop