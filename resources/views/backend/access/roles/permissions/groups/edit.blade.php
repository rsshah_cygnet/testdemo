@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.permissions.groups.edit'))

@section('page-header')
<h1>
    Group Management<small> EDIT</small>
</h1>
@endsection

@section('content')
{!! Form::model($group, ['route' => ['admin.access.roles.permission-group.update', $group->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch']) !!}

<div class="box-tools pull-right">
    @include('backend.access.includes.partials.header-buttons')
</div>
<div class="clear"></div>

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.access.permissions.groups.name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.permissions.groups.name')]) !!}
    </div>
</div><!--form control-->

<div class="box-footer">
    <a href="{!! route('admin.access.roles.permissions.index') !!}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
    <input type="submit" class="btn btn-info" style="float: right;" value="{{ trans('buttons.general.crud.update') }}" />
</div>


{!! Form::close() !!}
@stop