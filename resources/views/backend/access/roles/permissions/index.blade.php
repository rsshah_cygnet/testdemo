@inject('roles', 'App\Repositories\Backend\Role\RoleRepositoryContract')

@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.permissions.management'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>{{ trans('labels.backend.access.permissions.management') }}<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.access.includes.partials.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('after-styles-end')
{!! Html::style('css/backend/plugin/nestable/jquery.nestable.css') !!}
@stop

@section('content')

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">        
        <li role="presentation" class="active">
            <a href="#permissions" aria-controls="permissions" role="tab" data-toggle="tab">
                {{ trans('labels.backend.access.permissions.tabs.permissions') }}
            </a>
        </li>
        <li role="presentation">
            <a href="#groups" aria-controls="groups" role="tab" data-toggle="tab">
                {{ trans('labels.backend.access.permissions.tabs.groups') }}
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane active" id="permissions" style="padding-top:20px">
            <!--permission grid-->
            <div class="box box-danger">
         <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
        </div><!--permissions-->        

        <div role="tabpanel" class="tab-pane" id="groups" style="padding-top:20px">
            <div class="row">
                <div class="col-lg-6">

                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> {{ trans('strings.backend.access.permissions.sort_explanation') }}
                    </div><!--alert info-->

                    <div class="dd permission-hierarchy">
                        <ol class="dd-list">
                            @foreach ($groups as $group)
                            <li class="dd-item" data-id="{!! $group->id !!}">
                                <div class="dd-handle">{!! $group->name !!} <span class="pull-right">{!! $group->permissions->count() !!} {{ trans('labels.backend.access.permissions.label') }}</span></div>

                                @if ($group->children->count())
                                <ol class="dd-list">
                                    @foreach($group->children as $child)
                                    <li class="dd-item" data-id="{!! $child->id !!}">
                                        <div class="dd-handle">{!! $child->name !!} <span class="pull-right">{!! $child->permissions->count() !!} {{ trans('labels.backend.access.permissions.label') }}</span></div>
                                    </li>
                                    @endforeach
                                </ol>
                            </li>
                            @else
                            </li>
                            @endif
                            @endforeach
                        </ol>
                    </div><!--master-list-->
                </div><!--col-lg-4-->

                <div class="col-lg-6">

                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> {{ trans('strings.backend.access.permissions.edit_explanation') }}
                    </div><!--alert info-->

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ trans('labels.backend.access.permissions.groups.table.name') }}</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($groups as $group)
                                <tr>
                                    <td>
                                        {!! $group->name !!}

                                        @if ($group->permissions->count())
                                        <div style="padding-left:40px;font-size:.8em">
                                            @foreach ($group->permissions as $permission)
                                            {!! $permission->display_name !!}<br/>
                                            @endforeach
                                        </div>
                                        @endif
                                    </td>
                                    <td>{!! $group->action_buttons !!}</td>
                                </tr>

                                @if ($group->children->count())
                                @foreach ($group->children as $child)
                                <tr>
                                    <td style="padding-left:40px">
                                        <em>{!! $child->name !!}</em>

                                        @if ($child->permissions->count())
                                        <div style="padding-left:40px;font-size:.8em">
                                            @foreach ($child->permissions as $permission)
                                            {!! $permission->display_name !!}<br/>
                                            @endforeach
                                        </div>
                                        @endif
                                    </td>
                                    <td>{!! $child->action_buttons !!}</td>
                                </tr>
                                @endforeach
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-lg-8-->
            </div><!--row-->
        </div><!--groups-->

    </div>
</div><!--permission tabs-->
@stop

@section('after-scripts-end')
<script>
    var moduleConfig = '';
    moduleConfig = {
        gridURL: "{!! route('admin.access.permissions.data', ['page'=>'1']) !!}",
        deleteSelectedItems: "{!! route('admin.access.permissions.deleteAll') !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };
    jQuery(function () {

        var StatusStr = ":All;1:Yes;0:No";
        var selectStatusSearchOptions = getSearchOption('select', StatusStr);
        var stringSearchOptions = getSearchOption('string');
        var Required_Key_Value = {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'columnFilterToolbar': true,
            'downloadDropdown': [false, false, false, false],
            'gridLoadUrl': "{!! route('admin.access.permissions.data', ['page'=>'1']) !!}",
            'displayColumn': ["{{ trans('labels.backend.access.permissions.table.permission') }}",
               "{{ trans('labels.backend.access.permissions.table.name') }}",
               "{{ trans('labels.backend.access.permissions.table.roles') }}",
               "{{ trans('labels.general.actions') }}"
            ],
            'dbColumn': [
                {title: false, name: "permissions.name", width: '250', searchoptions: stringSearchOptions},
                {title: false, name: "permissions.display_name", width: '250', searchoptions: stringSearchOptions},
                {title: false, name: "roles.name", width: '250', searchoptions: stringSearchOptions},
                {title: false, name: 'act', width: '250', index: 'act', sortable: false, search: false},
            ],
            'defaultSortColumnName': "permissions.id",
            'defaultSortOrder': "DESC",
        };
        CustomGrid(Required_Key_Value);
    });</script>
@stop

