@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.permissions.create'))

@section('page-header')
<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6"><h1>{{ trans('labels.backend.access.permissions.management') }}<small> CREATE</small></h1></div>
        <div class="col-xs-12 col-lg-6">
            <div class="box-tools">
                @include('backend.access.includes.partials.header-buttons')
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-styles-end')
{!! Html::style('css/backend/plugin/jstree/themes/default/style.min.css') !!}
@stop

@section('content')
<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => 'admin.access.roles.permissions.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission']) !!}

<div class="box-tools pull-right">

</div>
<div class="clear"></div>

<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#general" aria-controls="general" role="tab" data-toggle="tab">
                {{ trans('labels.backend.access.permissions.tabs.general') }}
            </a>
        </li>
        <li role="presentation">
            <a href="#dependencies" aria-controls="dependencies" role="tab" data-toggle="tab">
                {{ trans('labels.backend.access.permissions.tabs.dependencies') }}
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="general" style="padding-top:20px">

            <div class="form-group">
                {!! Form::label('name', trans('validation.attributes.backend.access.permissions.name'), ['class' => 'col-lg-2 control-label required']) !!}
                <div class="col-lg-10">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.permissions.name')]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('display_name', trans('validation.attributes.backend.access.permissions.display_name'), ['class' => 'col-lg-2 control-label required']) !!}
                <div class="col-lg-10">
                    {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.permissions.display_name')]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('group', trans('validation.attributes.backend.access.permissions.group'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    <select name="group" class="form-control">
                        <option value="">{{ trans('labels.general.none') }}</option>

                        @foreach ($groups as $group)
                        <option value="{!! $group->id !!}">{!! $group->name !!}</option>
                        @endforeach
                    </select>
                </div>
            </div><!--form control-->

           <!--  <div class="form-group">
                {!! Form::label('sort', trans('validation.attributes.backend.access.permissions.group_sort'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('sort', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.permissions.group_sort')]) !!}
                </div>
            </div> -->

            <div class="form-group">
                <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.permissions.associated_roles') }}</label>
                <div class="col-lg-3">
                    @if (count($roles) > 0)
                    @foreach($roles as $role)
                    <input type="checkbox" {{$role->id == 1 ? 'disabled checked' : ''}} value="{{$role->id}}" name="permission_roles[]" id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{!! $role->name !!}</label><br/>
                    @endforeach
                    @else
                    {{ trans('labels.backend.access.permissions.no_roles') }}
                    @endif
                </div>
            </div><!--form control-->
            {{ Form::hidden('system', 0) }}
            <!--            <div class="form-group">
                            <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.permissions.system') }}</label>
                            <div class="col-lg-3">
                                <input type="checkbox" name="system" />
                            </div>
                        </div>form control-->

        </div><!--general-->

        <div role="tabpanel" class="tab-pane" id="dependencies" style="padding-top:20px">

            <div class="alert alert-info">
                <i class="fa fa-info-circle"></i>
                {!! getLanguageBlock('backend.lang.access.roles.permissions.dependencies-explanation') !!}
            </div><!--alert-->

            <!-- grouped permissions -->
            <div class="form-group">
                <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.permissions.dependencies') }}</label>
                <div class="col-lg-10">

                    <div id="available-permissions">
                        <div class="row">

                            <div class="col-lg-6">
                                <p><strong>{{ trans('labels.backend.access.permissions.grouped_permissions') }}</strong></p>

                                @if ($groups->count())
                                <div id="permission-tree">
                                    <ul>
                                        @foreach ($groups as $group)
                                        <li>{!! $group->name !!}
                                            @if ($group->permissions->count())
                                            <ul>
                                                @foreach ($group->permissions as $permission)
                                                <li id="{!! $permission->id !!}" data-dependencies="{!! json_encode($permission->dependencies->lists('dependency_id')->all()) !!}">

                                                    @if ($permission->dependencies->count())
                                                    <?php
//Get the dependency list for the tooltip
$dependency_list = [];
foreach ($permission->dependencies as $dependency) {
	array_push($dependency_list, $dependency->permission->display_name);
}

$dependency_list = implode(", ", $dependency_list);
?>
                                                    <a data-toggle="tooltip" data-html="true" title="<strong>{{ trans('labels.backend.access.permissions.dependencies') }}:</strong> {!! $dependency_list !!}">{!! $permission->display_name !!} <small><strong>(D)</strong></small></a>
                                                    @else
                                                    {!! $permission->display_name !!}
                                                    @endif

                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif

                                            @if ($group->children->count())
                                            <ul>
                                                @foreach ($group->children as $child)
                                                <li>{!! $child->name !!}
                                                    @if ($child->permissions->count())
                                                    <ul> style="padding-left:40px;font-size:.8em">
                                                        @foreach ($child->permissions as $permission)
                                                        <li id="{!! $permission->id !!}" data-dependencies="{!! json_encode($permission->dependencies->lists('dependency_id')->all()) !!}">
                                                            @if ($permission->dependencies->count())
                                                            <?php
//Get the dependency list for the tooltip
$dependency_list = [];
foreach ($permission->dependencies as $dependency) {
	array_push($dependency_list, $dependency->permission->display_name);
}

$dependency_list = implode(", ", $dependency_list);
?>
                                                            <a data-toggle="tooltip" data-html="true" title="<strong>{{ trans('labels.backend.access.permissions.dependencies') }}:</strong> {!! $dependency_list !!}">{!! $permission->display_name !!} <small><strong>(D)</strong></small></a>
                                                            @else
                                                            {!! $permission->display_name !!}
                                                            @endif

                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                    @endif
                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @else
                                <p>{{ trans('labels.backend.access.permissions.no_groups') }}</p>
                                @endif
                            </div><!--col-lg-6-->

                            <div class="col-lg-6">
                                <p><strong>{{ trans('labels.backend.access.permissions.ungrouped_permissions') }}</strong></p>

                                @if ($permissions->count())
                                @foreach ($permissions as $perm)
                                <input type="checkbox" name="ungrouped[]" data-dependencies="{!! json_encode($perm->dependencies->lists('dependency_id')->all()) !!}" value="{!! $perm->id !!}" id="perm_{!! $perm->id !!}" /> <label for="perm_{!! $perm->id !!}">

                                    @if ($perm->dependencies->count())
                                    <?php
//Get the dependency list for the tooltip
$dependency_list = [];
foreach ($perm->dependencies as $dependency) {
	array_push($dependency_list, $dependency->permission->display_name);
}

$dependency_list = implode(", ", $dependency_list);
?>
                                    <a style="color:black;text-decoration:none;" data-toggle="tooltip" data-html="true" title="<strong>{{ trans('labels.backend.access.permissions.dependencies') }}:</strong> {!! $dependency_list !!}">{!! $perm->display_name !!} <small><strong>(D)</strong></small></a>
                                    @else
                                    {!! $perm->display_name !!}
                                    @endif

                                </label><br/>
                                @endforeach
                                @else
                                <p>{{ trans('labels.backend.access.permissions.no_ungrouped') }}</p>
                                @endif
                            </div><!--col-lg-6-->
                        </div><!--row-->
                    </div><!--available permissions-->
                </div><!--col-lg-3-->
            </div><!--form control-->

        </div><!--dependencies-->

    </div><!--tab content-->

</div><!--tabs-->

<div class="box-footer">
    <a href="{!! route('admin.access.roles.permissions.index') !!}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" />
</div>

{!! Form::hidden('permissions') !!}
{!! Form::close() !!}
@stop

@section('after-scripts-end')
{!! Html::script('js/backend/plugin/jstree/jstree.min.js') !!}
{!! Html::script('js/backend/access/permissions/dependencies/script.js') !!}
@stop