@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.roles.management'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>{{ trans('labels.backend.access.roles.management') }}<small> LISTING</small></h1></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.access.includes.partials.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
    <table id="list"></table>
    <div id="pager"></div>
</div><!-- /.box-body -->
</div><!--box box-success-->


@stop
@section('after-scripts-end')
<script type="text/javascript">
    var moduleConfig = '';
    moduleConfig = {
        gridURL: "{!! route('admin.access.roles.data', ['page'=>'1']) !!}",
        deleteSelectedItems: "{!! route('admin.access.roles.deleteAll') !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };



    jQuery(function () {
        var stringSearchOptions         = getSearchOption('string'),
            StatusStr                   = ":All;0:InActive;1:Active",
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
        var Required_Key_Value = {
            'refreshFooter': false,
             'searchFooter' : false,
            'columnChooser' : false,
             'downloadDropdown' : [false,false,false,false],
            'columnFilterToolbar': true,
            'shrinkToFit': true,
            'gridLoadUrl': "{!! route('admin.access.roles.data', ['page'=>'1']) !!}",
            'displayColumn': ["{{ trans('labels.backend.access.roles.table.role') }}",
                "{{ trans('labels.backend.access.roles.table.status') }}",
                "{{ trans('labels.backend.access.roles.table.number_of_users') }}",
                "{{ trans('labels.general.actions') }}"],
            'dbColumn': [
                {title: false, name: "name", width: '420', search: true, sortable: true, searchoptions: stringSearchOptions},
                {title: false, name: 'Status',  width: '420', search: true, sortable: true, stype: 'select',searchoptions: selectStatusSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                    console.log(cellvalue);
                        if (cellvalue == 0)
                        {
                            return "<label class='label label-danger'>InActive</label>"
                        }
                        else if (cellvalue == 1)
                        {
                             return "<label class='label label-success'>Active</label>"
                        }
                    }
                },
                {search: false, sortable: false, title: false, name: "noofuser", width: '320'},
               /* {search: false, sortable: false, title: false, name: "sort", width: '300'}*/
                {title: false, name: 'act', index: 'act', width: '130', sortable: false, search: false},

            ],
            'defaultSortColumnName': "name",
        };
        CustomGrid(Required_Key_Value);




    });
</script>
@stop
