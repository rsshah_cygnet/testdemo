@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.change_password'))

@section('page-header')
<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6"><h1>{{ trans('labels.backend.access.users.change_password') }}</h1></div>
        <div class="col-xs-12 col-lg-6">
            <div class="box-tools">
                 @include('backend.access.includes.partials.header-buttons')
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => ['admin.access.user.change-password', $user->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

<div class="form-group">
    {!! Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'col-lg-2 control-label required', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) !!}
    <div class="col-lg-10">
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'col-lg-2 control-label required', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation')]) !!}
    <div class="col-lg-10">
        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    </div>
</div><!--form control-->

<div class="box-footer">
    <a href="{{route('admin.access.users.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" />
</div>

{!! Form::close() !!}
@stop