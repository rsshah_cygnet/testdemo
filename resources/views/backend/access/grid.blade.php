<h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3>

<div class="box-tools pull-right">
    @include('backend.access.includes.partials.header-buttons')
</div>

<div class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-9">
            <div class="dataTables_length">
                {!! Form::label('type', trans('labels.backend.page_size')) !!} {!! Form::select('pagesize', array('10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30' ), null, ['id'=>'pagesize']) !!}
                <button id="trigerDelete" class="btn bg-purple btn-flat">{{ trans('labels.backend.delete') }}</button>
                @if ( count($records) > 0 )
                <a class="btn bg-purple btn-flat" href="{{ route('admin.access.users.export') }}">{{ trans('labels.backend.export_excel') }}</a>
                <a class="btn bg-purple btn-flat" href="{{ route('admin.access.users.csv') }}">{{ trans('labels.backend.export_csv') }}</a> @endif
            </div>
        </div>
        <div class="col-sm-3">
            <div id="grid-table_filter" class="dataTables_filter">
                <label>Search: {!! Form::text('search-top', null, ['class' => 'form-control input-sm','id' => 'search-top', 'placeholder' => trans('labels.backend.search')]) !!}</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- hidden fields for sorting -->
            {!! Form::hidden('orderby', 'id', ['class' => 'form-control input-sm','id' => 'orderby']) !!} {!! Form::hidden('sort', 'asc', ['class' => 'form-control input-sm','id' => 'sort']) !!}
            <table id="grid-table" class="table table-striped table-hover table-bordered dataTable" role="grid" aria-describedby="grid-table_info">
                <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="bulkDelete" /> </th>
                        <th class="sorting" data-orderby="name" data-sort="asc">{{ trans('labels.backend.access.users.table.name') }}</th>
                        <th class="sorting" data-orderby="email" data-sort="asc">{{ trans('labels.backend.access.users.table.email') }}</th>
                        <th class="sorting" data-orderby="confirmed" data-sort="asc">{{ trans('labels.backend.access.users.table.confirmed') }}</th>
                        <th class="sorting" data-orderby="status" data-sort="asc">{{ trans('labels.backend.access.users.table.status') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.roles') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if ( count($records) > 0 ) @foreach ($records as $record)
                    <tr>
                        <td>
                            <input type="checkbox" class="deleteRow" value="{{$record->id}}">
                        </td>
                        <td>{!! $record->name !!}</td>
                        <td>{!! $record->email !!}</td>
                        <td>{!! $record->confirmed_label !!}</td>
                        <td>{!! $record->status_label !!}</td>
                        <td>
                            @if ($record->roles()->count() > 0) @foreach ($record->roles as $role) {!! $role->name.'
                            <br/>' !!} @endforeach @else {!! trans('labels.general.none')!!} @endif
                        </td>
                        <td>{!! $record->created_at->diffForHumans() !!}</td>
                        <td>{!! $record->updated_at->diffForHumans() !!}</td>
                        <td>{!! $record->action_buttons !!}</td>
                    </tr>
                    @endforeach @else
                    <tr>
                        <td colspan="8" align="center">{{ trans('labels.backend.no_record_found') }}</td>
                    </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>
                            <label>{!! Form::text('name', null, ['class' => 'form-control input-sm','id' => 'name', 'placeholder' => trans('labels.backend.access.users.placeHolders.name')]) !!}</label>
                        </td>
                        <td>
                            <label>{!! Form::text('email', null, ['class' => 'form-control input-sm','id' => 'email', 'placeholder' => trans('labels.backend.access.users.placeHolders.email')]) !!}</label>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div class="pull-left">
    {!! $records->total() !!} {{ trans_choice('labels.backend.access.users.table.total', $records->total()) }}
</div>

<div class="pull-right">
    {!! $records->render() !!}
</div>
<div class="clearfix"></div>