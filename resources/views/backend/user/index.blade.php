@extends ('backend.layouts.master') @section ('title', trans('navs.general.edit')) 

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
  <h1>{{ trans('navs.general.edit') }}</h1> 
</div>    
</div>
</div>
@endsection 

@section('content')
<div class="box box-danger">
<div class="box-body">
<div class="form-horizontal">
  <!--   <div class="form-group clearfix">
        {!! Form::label('avatar', trans('labels.backend.user.profile.avatar'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            <img src="{{access()->user()->picture}}" class="user-profile-image" width="100px" height="100px" />
        </div>
    </div> -->
    <div class="form-group clearfix">
        @if($user->last_name != '')
            {!! Form::label('profile_name', trans('labels.backend.user.profile.first_name'), ['class' => 'col-lg-2 control-label']) !!}
        @else
            {!! Form::label('profile_name', trans('labels.backend.user.profile.name'), ['class' => 'col-lg-2 control-label']) !!}
        @endif
        <div class="col-lg-10">
            {!! $user->first_name !!}
        </div>
    </div>
    @if($user->last_name != '')
        <div class="form-group clearfix">
            {!! Form::label('profile_name', trans('labels.backend.user.profile.last_name'), ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! $user->last_name !!}
            </div>
        </div>
    @endif
    <div class="form-group clearfix">
        {!! Form::label('profile_email', trans('labels.backend.user.profile.email'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! $user->email !!}
        </div>
    </div>
    <div class="form-group clearfix">
        {!! Form::label('profile_phone', trans('labels.backend.user.profile.phone_no'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! $user->contact_no !!}
        </div>
    </div>
    <div class="form-group clearfix">
        {!! Form::label('profile_createdAt', trans('labels.backend.user.profile.created_at'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! $user->created_at !!} ({!! $user->created_at->diffForHumans() !!})
        </div>
    </div>
    <div class="form-group clearfix">
        {!! Form::label('profile_updated', trans('labels.backend.user.profile.last_updated'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! $user->updated_at !!} ({!! $user->updated_at->diffForHumans() !!})
        </div>
    </div>
    <div class="form-group clearfix">
        {!! Form::label('actions', trans('labels.general.actions'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            <a href="{!! route('backend.user.profile.edit') !!}" class="btn btn-info">{{ trans('labels.backend.user.profile.edit_information') }}</a> @if ($user->canChangePassword())
            <a href="{!! route('admin.auth.password.change') !!}" class="btn btn-info">{{ trans('navs.backend.user.change_password') }}</a> @endif
        </div>
    </div>
</div>
@endsection