@extends('backend.layouts.master') @section ('title', trans('labels.backend.user.profile.update_information')) @section('content') @section('page-header')

<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
<h1>{{ trans('labels.backend.user.profile.update_information') }}<small> EDIT</small></h1>
</div>
</div>
</div>
@endsection
@section('content')
<div class="box box-danger">
         <div class="box-body">
<div class="form-horizontal">

    {!! Form::model($user, ['route' => 'backend.user.profile.update', 'class' => 'form-horizontal', 'method' => 'PATCH', 'files' => true]) !!}
    <input type="hidden" name="email" value="{{ $user->email }}" />

   <!--  <div class="form-group clearfix">
        {!! Form::label('name', trans('validation.attributes.backend.avatar'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            <img src="{{ access()->user()->picture }}" class="user-profile-image" width="100px" height="100px" /> {!! Form::file('avatar') !!}
        </div>
    </div> -->
    <div class="form-group clearfix">
        {!! Form::label('first_name', trans('validation.attributes.backend.first_name'), ['class' => 'col-lg-2 control-label required']) !!}
        <div class="col-lg-10">
            {!! Form::input('text', 'first_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.first_name')]) !!}
        </div>
    </div>

    <div class="form-group clearfix">

        {!! Form::label('last_name', trans('validation.attributes.backend.last_name'), ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">
            {!! Form::input('text', 'last_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.last_name')]) !!}
        </div>
    </div>

    @if ($user->canChangeEmail())
    <div class="form-group clearfix">

        {!! Form::label('email', trans('validation.attributes.backend.email'), ['class' => 'col-lg-2 control-label required']) !!}

        <div class="col-lg-10">
            {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.email'), 'disabled' => 'disabled']) !!}
        </div>
    </div>
    @endif

    <div class="form-group clearfix">
        {!! Form::label('profile_phone', trans('validation.attributes.backend.phone_no'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! Form::input('contact_no', 'contact_no', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.phone_no')]) !!}
        </div>
    </div>

    <div class="form-group clearfix">
        <div class="col-lg-2  xs-hidden">

        </div>
        <div class="box-footer">
            <a href="{{route('backend.user.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a> {!! Form::submit(trans('labels.general.buttons.save'), ['class' => 'btn btn-info pull-right']) !!}
        </div>
    </div>


    {!! Form::close() !!}
    <!--panel body-->
</div>
</div><!-- col-md-10 -->
@endsection