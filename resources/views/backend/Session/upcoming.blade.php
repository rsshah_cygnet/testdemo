 @extends('backend.layouts.master')

@section('title', 'Upcoming Session')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Upcoming Session<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">

</div>
</div>
</div>
</div>
@endsection

@section('content')

    <div class="box-tools" style="margin-bottom: 10px;">
 <div class="row">
     <input type="hidden" id="session_filter_type" value="upcoming">
     <input type="hidden" id="current_date" value="{{date('Y-m-d')}}">
     <label class="col-md-12 control-label">Search Sessions by Conduct date : </label>
     <span id="search_error" style="color:red;margin-left: 15px;display: none;"></span>
 </div>
     <div class="row" style="margin-bottom: 10px;">
          <label for="dteBeginDate" class="col-md-2 control-label">Start date:</label>
          <div class="col-md-3">
            <input name="date_start" id="date_start">
          </div>
          <label for="dteEndDate" class="col-md-2 control-label">End date:</label>
          <div class="col-md-3">
            <input id="date_end" name="date_end">

          </div>
     </div>
      <div class="row" style="margin-bottom: 10px;">
          <label for="dteBeginDate" class="col-md-2 control-label">Report type:</label>
          <div class="col-md-4">
            <select id="session_report_type">
              <option value="">Select</option>
              <option value="{{config('constants.report_weekly')}}">Weekly</option>
              <option value="{{config('constants.report_fortnightly')}}">Fortnightly</option>
              <option value="{{config('constants.report_monthly')}}">Monthly</option>
              <option value="{{config('constants.report_semi_yearly')}}">Semi Yearly</option>
              <option value="{{config('constants.report_yearly')}}">Yearly</option>
            </select>
          </div>
     </div>
      <div class="row" style="margin-bottom: 10px;">
          <div class="col-md-2">
           <input type="button" style="width: 115px;" class="btn btn-info pull-right" onclick="filterSession()" value="Search" />
        </div>
          <div class="col-md-2">
          <a href="{{route('admin.session.upcoming')}}" style="width: 115px;" class="btn btn-red">Reset</a>
        </div>
      </div>
</div>

     <div class="box box-danger">
        <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.session.data-upcoming', ['page'=>'1']) !!}",
         labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;0:InActive;1:Active",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
            dateSearchOptions           = getSearchOption('date');

            /*displayColumnHeaderArray.push(displayColumnArray);*/

        var Required_Key_Value =
        {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [

                {title: false, name: 'Tutor Name',index: 'session.tutor_name',search:false},
                {title: false, name: 'Tutee Name',index: 'session.tutee_name',search:false},
                {title: false, name: 'Accepted date & time',index: 'session.accepted_date_time',searchoptions:dateSearchOptions},
                {title: false, name: 'Conducted date & time',index: 'session.scheduled_date_time',searchoptions:dateSearchOptions},
                {title: false, name: 'Topic',index: 'topic.topic_name',searchoptions:stringSearchOptions,
                formatter: function (cellvalue, options, rowObject)
                    {
                        if (!cellvalue)
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }else{
                            return cellvalue
                        }

                    }
                },
                {title: false, name: 'Subject',index: 'subject.subject_name',searchoptions:stringSearchOptions,
                formatter: function (cellvalue, options, rowObject)
                    {
                        if (!cellvalue)
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }else{
                            return cellvalue
                        }

                    }
                },
                {title: false, name: 'Session Fee',index: 'session.session_fee',searchoptions:stringSearchOptions}
               /* {title: false, name: 'act',index: 'act', sortable: false, search: false}*/

            ],
        };

        CustomGrid(Required_Key_Value);
    });

    $('#date_start, #date_end').datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0
        });
</script>
@stop