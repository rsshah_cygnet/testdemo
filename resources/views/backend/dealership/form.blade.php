<div class="clear"></div>

    <div class="form-horizontal">

        <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
            {!! Form::label('dealership_code', "Dealership Code", ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('dealership_code', null, ['class' => 'form-control', 'placeholder' => "Dealership Code", 'required' =>' required', 'disabled' => 'disabled']) !!}
            </div>
        </div>

        <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
            {!! Form::label('dealership_name', "Dealership Name", ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('dealership_name', null, ['class' => 'form-control', 'placeholder' => "Dealership Name", 'required' =>' required']) !!}
            </div>
        </div>

        <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
            {!! Form::label('province_region_id', "Dealership Region", ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::select('province_region_id', $provinceRegions, isset($item->province_region_id) ? $item->province_region_id : '', array('class' => 'form-control accAj', 'required')) !!}
            </div>
        </div>

        <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
            {!! Form::label('address', "Dealership Address", ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => "Dealership Address", 'required' =>' required']) !!}
            </div>
        </div>

        <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
            {!! Form::label('dealership_email', "Dealership email", ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('dealership_email', null, ['class' => 'form-control', 'placeholder' => "Dealership email", 'required' =>' required']) !!}
            </div>
        </div>

        <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
            {!! Form::label('dealer_principal_email', "Dealer Principal Mail", ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('dealer_principal_email', null, ['class' => 'form-control', 'placeholder' => "Dealer Principal Mail", 'required' =>' required']) !!}
            </div>
        </div>

        <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
            {!! Form::label('dealer_principal_contact_no', "Dealer Principal Phone", ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('dealer_principal_contact_no', null, ['class' => 'form-control', 'placeholder' => "Dealer Principal Phone", 'required' =>' required']) !!}
            </div>
        </div>

        <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label('status', "Status", ['class' => 'col-lg-2 control-label']) !!}
        
        <div class="col-lg-10">
          <div class="checkbox icheck">
            <label>{{ Form::checkbox('record_status', 1, (isset($item) && $item->status == 'Active') ? true : false ) }}</label>
        </div>
        </div>
    </div>

    </div>

<div class="clear"></div>
