@extends ('backend.layouts.master')

@section('title', trans('labels.backend.dealership.management') . ' | ' . trans('labels.backend.dealership.edit'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
<h1>Dealership Management<small> EDIT</small></h1>
</div>    
</div>
</div>
@endsection


@section('content')
<div class="box box-danger">
<div class="box-body">   
	{!!Form::model($item, ['route' => ['admin.dealership.update', $item->id], 'class' => 'form-horizontal',
		'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-dealership']) !!}
        <input type="hidden" name="status" value="{{ $item->status }}" />

		{{-- Dealership Model Form --}}
		@include('backend.dealership.form')

		<div class="box-footer">
			<a href="{!! URL::previous() !!}" class="btn btn-red">Cancel</a>
			{!! Form::submit('Save', array("class"=>"btn btn-info pull-right")) !!}
		</div>
</div>
</div>
	{!! Form::close() !!}

    @if($item->dealership_name != '' && $item->province_region_id != '' && $item->address != '' && $item->dealership_email != '' && $item->dealer_principal_email != '' && $item->dealer_principal_contact_no != '' && $item->status == 'Active')
        <div class="post clearfix"></div>
        <div class="panel box box-danger">
            <div class="box-header with-border">
                <h4 class="box-title">
                    <a>Dealership Floors</a>
                </h4>
            </div>
            <div class="box-body">
                <table id="list"></table>
                <div id="pager"></div>
            </div>
        </div>
    @endif

@stop

@section('after-scripts-end')
<script type="text/javascript">

    @if($item->dealership_name != '' && $item->province_region_id != '' && $item->address != '' && $item->dealership_email != '' && $item->dealer_principal_email != '' && $item->dealer_principal_contact_no != '' && $item->status == 'Active')
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.dealership.data-floor', $item->id, ['page'=>'1']) !!}"
    };

    var alertIDs = {themodal: 'alertmod', modalhead: 'alerthd', modalcontent: 'alertcnt'};

    $.jgrid.viewModal("#" + alertIDs.themodal,
        {gbox: "#gbox_" + $.jgrid.jqID('list'), jqm: true});
    $("#jqg_alrt").focus();

    /*var selectValue = $.ajax({
        url: "{ route('admin.dealership.get-floor-users', $item->id) }}",
        async: false
    }).responseText;*/

    /*$('#list').on('reloadGrid', function() {
        //alert('HI');
        selectValue();
    });*/

    var buildUserSelectFromJson = function (data) {
        var result = $.parseJSON(data);
        if (result.length == 0) {
            $.jgrid.info_dialog.call($('#list'),
                "Warning",              // dialog title
                "No sales managers are available to be assigned!"  // text inside of dialog
            );

            $("#info_dialog").css({
                margin: "auto",
                width: "300px",
                top: "80%",
                left: "50%",
            });

            $("#list").trigger("reloadGrid");
            $("html, body").animate({scrollTop: $(document).height()}, "fast");
            return true;
        }
        var html = '<select>';
        $.each(result, function (key, value) {
            html += '<option value=' + value.id + '>' + value.SalesManager + '</option>';
        });
        html += '</select>';
        return html;
    };

    var buildCodeSelectFromJson = function (data) {
        var result = $.parseJSON(data);
        if (result.length == 0) {
            $.jgrid.info_dialog.call($('#list'),
                "Warning",              // dialog title
                "No floor codes are available to be assigned!"  // text inside of dialog
            );

            $("#info_dialog").css({
                margin: "auto",
                width: "300px",
                top: "80%",
                left: "50%",
            });

            $("#list").trigger("reloadGrid");
            $("html, body").animate({scrollTop: $(document).height()}, "fast");
            return true;
        }
        var html = '<select>';
        $.each(result, function (key, value) {
            html += '<option value=' + value.FloorCode + '>' + value.FloorCode + '</option>';
        });
        html += '</select>';
        return html;
    };

    jQuery(function () {

        var displayColumn = '{!! json_encode($repository->gridColumn) !!}',
            displayColumnHeader = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray = eval(displayColumnHeader),
            defaultOrderBy = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray = ['action'],
            displayColumns = eval(displayColumn),
            StatusStr = ":All;Active:Active;InActive:InActive",
            stringSearchOptions = getSearchOption('string');

        jQuery.each(displayColumns, function (index, element) {
            displayColumnArray.push(displayColumnHeaderArray[index]);
        });

        var Required_Key_Value = {

            'loadOnce': true,
            'inlineEdit': true,
            'shrinkToFit': true,
            'gridLoadUrl': moduleConfig.gridURL,
            'displayColumn': displayColumnArray,
            'defaultSortColumnName': defaultOrderBy,
            'dbColumn': [
                {
                    title: false,
                    name: 'act',
                    index: 'act',
                    width: '50',
                    sortable: false,
                    search: false
                },
                {
                    title: true,
                    name: 'Floor Code',
                    index: 'dealership_floors.floor_code',
                    width: '475',
                    searchoptions: stringSearchOptions,
                    editable: true,
                    edittype: 'select',
                    editrules: {required: true},
                    editoptions: {
                        dataUrl: "{{ route('admin.dealership.get-floor-codes', $item->id) }}",
                        buildSelect: buildCodeSelectFromJson, dataInit: function (elem) {
                            $(elem).width(90 + '%');
                        }
                    }
                },
                {
                    title: true,
                    name: 'Floor Type',
                    index: 'dealership_floors.floor_type',
                    width: '475',
                    searchoptions: stringSearchOptions,
                    editable: true,
                    edittype: 'select',
                    editrules: {required: true},
                    editoptions: {
                        value: "N:New;U:Used;D:Demo;E:Executive",
                        style: "width: 90%"
                    },
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue == 'N') {
                            return 'New'
                        } else if (cellvalue == 'U') {
                            return 'Used'
                        } else if (cellvalue == 'D') {
                            return 'Demo'
                        } else if (cellvalue == 'E') {
                            return 'Executive'
                        }
                    }
                },
                {
                    title: false,
                    name: 'Floor Name',
                    index: 'dealership_floors.floor_name',
                    width: '475',
                    searchoptions: stringSearchOptions,
                    editable: true,
                    editrules: {required: true}
                },
                {
                    title: true,
                    name: 'Sales Manager',
                    index: 'users.first_name',
                    width: '475',
                    searchoptions: stringSearchOptions,
                    editable: true,
                    edittype: 'select',
                    editrules: {required: true},
                    editoptions: {
                        dataUrl: "{{ route('admin.dealership.get-floor-users', $item->id) }}",
                        buildSelect: buildUserSelectFromJson, dataInit: function (elem) {
                            $(elem).width(90 + '%');
                        }
                    }
                }
            ],
            'rowEditUrl': "{{ route('admin.dealership.store-floor', $item->id) }}"

        };

        CustomGrid(Required_Key_Value);
    });

    @endif

    $(document).ready(function() {
        Imperial.Dealership.init();
    });

</script>
@stop