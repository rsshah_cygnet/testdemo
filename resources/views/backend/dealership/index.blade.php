@extends('backend.layouts.master')

@section('title', trans('labels.backend.dealership.management') . ' | ' . trans('labels.backend.dealership.list'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
<h1>Dealership Management<small> LISTING</small></h1>
</div>    
</div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
    <table id="list"></table>
    <div id="pager"></div>
</div><!-- /.box-body -->
</div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';
    
    moduleConfig = {
        gridURL: "{!! route('admin.dealership.data', ['page'=>'1']) !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
    };

    jQuery(function () {

        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;Active:Active;InActive:InActive",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);

        displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value = {
            'columnFilterToolbar':      true,
            'shrinkToFit':              true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [
                {title: false, name: 'Dealership Name', index: 'dealerships.dealership_name', searchoptions:stringSearchOptions},
                {title: false, name: 'Dealership Code', index: 'dealerships.dealership_code', searchoptions:stringSearchOptions},
                {title: false, name: 'Floor',  index: 'dealership_floors.id',  searchoptions: selectStatusSearchOptions, search: false, width: '70'},
                {title: false, name: 'Dealership Email',  index: 'dealerships.dealership_email',  searchoptions:stringSearchOptions},
                {title: false, name: 'Status',  index: 'dealerships.status',  stype: 'select', searchoptions: selectStatusSearchOptions},
                {title: false, name: 'act', index: 'act', sortable: false, search: false,  width: '70'}
            ]
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop