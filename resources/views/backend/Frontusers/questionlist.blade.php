@extends('backend.layouts.master')

@section('title', 'Question Answer')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Question Answer <small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">

</div>
</div>
</div>
@endsection

@section('content')

    <div class="box box-danger">
    <?php $count = 1?>
    <?php $selected = false;

?>

<div class="box-body">
<div class="col-xs-12">
<b>Curriculum Heirarchy :
<?php
if ($data->topic_id != Null || $data->topic_id != 0) {
	echo getCurriculumHierarchy('', $data->topic_id, true);
} elseif ($data->subject_id != Null || $data->subject_id != 0) {
	echo getCurriculumHierarchy($data->subject_id, '', true);
}
?>
 </b>
</div>
</div>
    @foreach ($item as $answer)
    	<div class="box-body">
      	{!! Form::label('question', 'Question '.$count.') '.$answer->Que->question, ['class' => 'col-lg-12 control-label ']) !!}
      	@if($answer->Que->question_image != '')
      	   {{ Html::image('uploads/questions/'.$answer->Que->question_image, $answer->Que->question_image, ['width' => 150, 'height' => '150']) }}
      	@endif
      	<div class="col-xs-12">
      	<div class="col-xs-6">
      	<label>Option 1</label>
      	@if($answer->answer == '1')
      	<?php $selected = true;?>
      	{{ Form::radio("$answer->id", 'Option 1 :', $selected ,['class' => 'field']) }}
      	<label>{{$answer->Que->option1}}</label>
      	@else
      	{{ Form::radio("$answer->id", 'Option 1 :', false ,['class' => 'field']) }}
      	<label>{{$answer->Que->option1}}</label>
      	@endif
      	<br>
      	</div>
      	<div class="col-xs-6">
      	<label>Option 2</label>
      	@if($answer->answer == '2')
      	<?php $selected = true;?>
      	{{ Form::radio("$answer->id", 'Option 2 :', $selected ,['class' => 'field']) }}
      	<label>{{$answer->Que->option2}}</label>
      	@else
      	{{ Form::radio("$answer->id", 'Option 1 :', false ,['class' => 'field']) }}
      	<label>{{$answer->Que->option2}}</label>
      	@endif
      	<br>
      	</div>
      	<div class="col-xs-6">
      	<label>Option 3</label>
      	@if($answer->answer == '3')
      	<?php $selected = true;?>
      	{{ Form::radio("$answer->id", 'Option 3 :', $selected ,['class' => 'field']) }}
      	<label>{{$answer->Que->option3}}</label>
      	@else
      	{{ Form::radio("$answer->id", 'Option 3 :', false ,['class' => 'field']) }}
      	<label>{{$answer->Que->option3}}</label>
      	@endif
      	<br>
      	</div>
      	<div class="col-xs-6">
      	<label>Option 4 </label>
      	@if($answer->answer == '4')
      	<?php $selected = true;?>
      	{{ Form::radio("$answer->id", 'Option 4 :', $selected ,['class' => 'field']) }}
      	<label>{{$answer->Que->option4}}</label><br><br>
      	@else
      	{{ Form::radio("$answer->id", 'Option 4 :', false ,['class' => 'field']) }}
      	<label>{{$answer->Que->option4}}</label><br><br>
      	@endif
      	</div>
      	</div>

      	{!! Form::label('correct answer', 'Correct Answer : Option'.$answer->Que->correct_answer, ['class' => 'col-lg-3 control-label ']) !!}

		{!! Form::label('user answer', 'User Answer : Option'.$answer->answer, ['class' => 'col-lg-3 control-label ']) !!}

		{!! Form::label('iscorrect',$answer->is_correct == 1 ? 'Correct : Yes' : 'Correct : No' , ['class' => 'col-lg-3 control-label ']) !!}

        </div>
        <hr>
        <?php $count++?>
     @endforeach
    </div>

@stop