 @extends('backend.layouts.master')

@section('title', 'Users Management')

@section('page-header')
<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <h1>Users Management<small> LISTING</small></h1>
        </div>
        <div class="col-xs-12 col-lg-6">
            <div class="box-tools">
                @include('backend.Frontusers.includes.header-buttons')
            </div>
        </div>
    </div>
</div>
 <!-- <div class="ajax-loader">
  <img src="{{ url('images/loader2.svg')}}" class="img-responsive" />
</div> -->
@endsection

@section('content')
     <div class="box box-danger">
        <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->

@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.frontusers.data', ['page'=>'1']) !!}",
         labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;0:InActive;1:Active",
            StatusStrRole               = ":both;1:Tutor;2:Tutee",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
            selectStatusSearchOptionsRole = getSearchOption('select', StatusStrRole);
            dateSearchOptions           = getSearchOption('date');

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value =
        {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [

                {title: false, name: 'First Name',index: 'front_user.first_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Last Name',index: 'front_user.last_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Email',index: 'front_user.email',searchoptions:stringSearchOptions},
                 {title: false, name: 'Status',  index: 'front_user.status',  stype: 'select',searchoptions: selectStatusSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == 0)
                        {
                            return "<label class='label label-danger'>InActive</label>"
                        }
                        else if (cellvalue == 1)
                        {
                             return "<label class='label label-success'>Active</label>"
                        }
                    }
                },
                {title: false, name: 'Role',index: 'front_user.role',stype: 'select',searchoptions:selectStatusSearchOptionsRole,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == 1)
                        {
                            return "<label class='label label-success'>Tutor</label>"
                        }
                        else if (cellvalue == 2)
                        {
                             return "<label class='label label-success'>Tutee</label>"
                        }else if (cellvalue == 3) {
                             return "<label class='label label-success'>Both</label>"
                        }
                    }
                },
                {title: false, name: 'Timezone',index: 'timezone.timezone_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Country',index: 'country.country_name',searchoptions:stringSearchOptions},
                {title: false, name: 'City',index: 'city.city_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Last Login Time',index: 'front_user.last_login_date',search: false,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue)
                        {
                            return cellvalue
                        }
                        else
                        {
                             return "<label class='label label-success'>N/A</label>"
                        }
                    }
                },
             {title: false, name: 'Send Reminder On',index: 'front_user.news_mail_date',search:false,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue)
                        {
                            return cellvalue
                        }
                        else
                        {
                             return "<label class='label label-success'>N/A</label>"
                        }
                    }
                },
                 {title: false, name: 'act',index: 'act', sortable: false,width: '200',search: false}

            ],
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop