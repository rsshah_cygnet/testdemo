<div class="btn-group">

    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>

    </button>
    <ul class="dropdown-menu dropdown-menu-sent-mail-to-all" role="menu">


        @permission('send-mail-to-all-news-letter')

        <li>
            <a href="javascript:void(0);" onclick="sendNewslettermail(0)">
                Send Mail To all
            </a>
        </li>

        @endauth


    </ul>
</div><!--btn group-->
<div class="clearfix"></div>