<div class="clear"></div>
<div id="custom_dropdowns">
<div class="form-group">
    {!! Form::label('grade_name', trans('validation.attributes.backend.grade.name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('grade_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.grade.name'),'required' =>' required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('grade_type', trans('validation.attributes.backend.grade.type'), ['class' => 'col-lg-2 control-label required']) !!}
   <div class="col-lg-10">
        <div class="col-lg-6">
            {!! Form::radio('grade_type', '0', true, ['id' => 'grade_type_lower']) !!} Lower
        </div>
        <div class="col-lg-6">
            {!! Form::radio('grade_type', '1', false, ['id' => 'grade_type_higher']) !!} Higher
        </div>
    </div>
</div>
<?php

//dd(['' => 'Select curriculum'] + $curriculums);

?>

<div class="form-group">
    {!! Form::label('curriculum_id', trans('validation.attributes.backend.grade.curriculum'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('curriculum_id', ['' => 'Select curriculum'] + $curriculums ,isset($grade->curriculum_id)?$grade->curriculum_id:"", ['class' => 'form-control select2' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required' =>' required']) !!}
    </div>
</div><!--form-group-->


</div>

@if(isset($formtype) && $formtype == "edit")
{{-- <div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
        <div class="col-lg-1">
        <div class="checkbox icheck">
            <label>{{ Form::checkbox('record_status', 1, (isset($grade->status) && $grade->status == '1') ? true : false ) }}</label>
        </div>
    </div>
</div> --}}
<input type="hidden" value="{{$grade->curriculum_id or ''}}" name="curriculum_id">
@endif
<input type="hidden" value="{{$grade->id or ''}}" name="grade_id">
<input type="hidden" value="grade" name="type" id="management_type">
<div class="box-footer">
    <a href="{{route('admin.grade.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a> @if(isset($grade))
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" /> @else
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" /> @endif
</div>
<!-- /.box-body -->