<div class="btn-group">
    
    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>
        
    </button>

    <ul class="dropdown-menu" role="menu">

        @permission('view-grade-management')

        <li>
            <a href="{{ route('admin.grade.index') }}">
                {{ trans('menus.backend.grade.all') }}
            </a>
        </li>
        
        @endauth

        @permission('create-grade')
        
        <li>
            <a href="{{ route('admin.grade.create') }}">
                {{ trans('menus.backend.grade.create') }}
            </a>
        </li>
        
        @endauth
    </ul>
</div><!--btn group-->
<div class="clearfix"></div>