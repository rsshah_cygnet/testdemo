

{!! Form::close() !!}
@extends ('backend.layouts.master')

@section ('title', 'Edit Educational System')

@section('page-header')
<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <h1>
                Educational System Management <small>EDIT</small>
            </h1>
        </div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.educationalsystem.includes.partials.header-buttons')
</div>
</div>
</div>
</div>
@endsection


@section('content')
<div class="box box-danger">
 <div class="box-body">
	{!!Form::model($item, ['route' => ['admin.educational-system.update', $item->id],'class' => 'form-horizontal',
		'role' 		=> 'form',
		'method' 	=> 'PATCH',
		'id' 		=> 'edit-noticeboard',
        ]) !!}
        {{-- Educational System Form --}}
    @include('backend.educationalsystem.form')

    <div class="box-footer">
        <a href="{{ route('admin.educational-system.index') }}" class="btn btn-red">Cancel</a>
        {!! Form::submit('Update', array("class"=>"btn btn-info pull-right")) !!}
    </div>
{!! Form::close() !!}
@stop

@section('after-scripts-end')

<script type="text/javascript">
	$(document).ready(function()
	{
			$('#curriculum_id').attr('disabled', true);
			//getEducational($('#curriculum_id').val());
			
			getCurriculumHierarchy("curriculum_id");
			trrigerChange();

			$('body').on('change','.dynamic_dropdown',function(){
					$('#curriculum_id').attr('disabled', true);
					$("#eductional_systems_id").attr('disabled', true);
					$("#program_id").attr('disabled', true);
					$("#grade_id").attr('disabled', true);
					$("#level_id").attr('disabled', true);
					$("#subject_id").attr('disabled', true);
					trrigerChange();
			})

			var level_trriger = true;
			var grade_trriger = true;
			var education_trriger = true;
			var subject_trriger = true;
			var program_trriger = true;

			function trrigerChange(){
				setTimeout(function(){

							var curriculum = $('#curriculum_id').length;
						    var eductional_systems = $('#eductional_systems_id').length;
						    var program = $('#program_id').length;
						    var subject = $('#subject_id').length;
						    var grade = $('#grade_id').length;
						    var level = $('#level_id').length;

						var selected_grade = "<?php echo isset($item->grade_id) ? $item->grade_id : ''; ?>";
						var selected_subject = "<?php echo isset($item->subject_id) ? $item->subject_id : ''; ?>";
						var selected_program = "<?php echo isset($item->program_id) ? $item->program_id : ''; ?>";
						var selected_level = "<?php echo isset($item->levels_id) ? $item->levels_id : ''; ?>";

						//alert(selected_level);
						if(selected_grade != 0 && selected_grade !="" && selected_grade != "null" && grade > 0 && grade_trriger == true){
							$('#grade_id').val(selected_grade).trigger("change");
							grade_trriger = false;
						}

						if(selected_subject != 0 && selected_subject !="" && selected_subject != "null" && subject > 0 && subject_trriger == true){
							$('#subject_id').val(selected_subject).trigger("change");
							subject_trriger = false;
						}
						if(selected_program != 0 && selected_program != "" && selected_program != "null" && program > 0 && program_trriger == true){
							$('#program_id').val(selected_program).trigger("change");
							program_trriger = false;
						}
						if(selected_level != 0 && selected_level != "null" && level > 0 && selected_level != "" && level_trriger == true){
							$('#level_id').val(selected_level).trigger("change");
							level_trriger = false;

						}
			 		}, 300);
			}



	});

</script>
@stop