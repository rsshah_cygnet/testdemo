 @extends('backend.layouts.master')

@section('title', 'Educational System Management')

@section('page-header')
<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <h1>
                {{ trans('menus.backend.educationalsystem.management') }} <small>Listing</small>
            </h1>
        </div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.educationalsystem.includes.partials.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
	<div class="box box-danger">
<div class="box-body">
    <table id="list"></table>
    <div id="pager"></div>
</div><!-- /.box-body -->
</div><!--box box-success-->

@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.educational-system.data', ['page'=>'1']) !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        createURL:  "{!! route('admin.educational-system.create') !!}",
        storeURL:  "{!! route('admin.educational-system.store') !!}",
        updateURL:  "{!! route('admin.educational-system.update', ['id'=> '-1']) !!}",
        editURL:    "{!! route('admin.educational-system.edit', ['id'=> '-1'])!!}"
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;1:Active;0:InActive",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr),
            dateSearchOptions           = getSearchOption('date');

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value =
        {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [
                {title: false, name: 'Curriculum Name',  index: 'curriculum.curriculum_name', searchoptions:stringSearchOptions},
                {title: false, name: 'Name',  index: 'eductional_systems.name', searchoptions:stringSearchOptions},
                {title: false, name: 'Subject',  index: 'subject.subject_name',
                    searchoptions:stringSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == false)
                        {
                            return "<label class='label label-danger'>N/A</label>"
                        }
                        else
                        {
                            return cellvalue;
                        }
                    }
                },
                {title: false, name: 'Grade',  index: 'grade.grade_name',
                    searchoptions:stringSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == false)
                        {
                            return "<label class='label label-danger'>N/A</label>"
                        }
                        else
                        {
                            return cellvalue;
                        }
                    }
                },
                {title: false, name: 'Program',  index: 'program.program_name',
                    searchoptions:stringSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == false)
                        {
                            return "<label class='label label-danger'>N/A</label>"
                        }
                        else
                        {
                            return cellvalue;
                        }
                    }
                },
                {title: false, name: 'Level',  index: 'levels.levels_name',
                    searchoptions:stringSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == false)
                        {
                            return "<label class='label label-danger'>N/A</label>"
                        }
                        else
                        {
                            return cellvalue;
                        }
                    }
                },
                {title: false, name: 'Status', index: 'eductional_systems.status',
                    stype: 'select',
                    searchoptions: selectStatusSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == 0)
                        {
                            return "<label class='label label-danger'>InActive</label>"
                        }
                        else if (cellvalue == 1)
                        {
                            return "<label class='label label-success'>Active</label>"
                        }
                    }

                },
                {title: false, name: 'Modified By',  index: 'users.first_name',              searchoptions:stringSearchOptions},
                {title: false, name: 'Created At',  index: 'eductional_systems.created_at',stype: 'date',searchoptions:dateSearchOptions},
                {title: false, name: 'act', index: 'act', sortable: false, search: false,width:150},
            ],
        };
        CustomGrid(Required_Key_Value);
    });
</script>
@stop