
<div class="clear"></div>
<div class="form-horizontal">
    <div id="custom_dropdowns">
        <div class="form-group clearfix">

            {!! Form::label('name', "Name", ['class' => 'col-lg-2 control-label required']) !!}

            <div class="col-lg-10">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => "Name", 'required' =>' required']) !!}
            </div>
        </div>
        <div class="form-group clearfix">

            {!! Form::label('curriculum_id', "Curriculum", ['class' => 'col-lg-2 control-label required']) !!}

            <div class="col-lg-10">
             {!! Form::select('curriculum_id',$curriculum,
             isset($item->curriculum_id)?$item->curriculum_id:'', array('class' => 'form-control select2', 'required', 'style' => 'width: 100%','placeholder' => 'Select Curriculum', 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")')) !!}

         </div>
     </div>



 </div>
 @if(!empty($item))
 {{--  <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
 {!! Form::label('status', "Active", ['class' => 'col-lg-2 control-label']) !!}
 <div class="col-lg-10">
    <div class="checkbox icheck">
        <label>{{ Form::checkbox('status', 1, (isset($item) && $item->status == 1) ? true : false ) }}</label>
    </div>
</div>
</div> --}}
<input type="hidden" value="{{$item->curriculum_id or ''}}" name="curriculum_id">
@endif
</div>

<input type="hidden" value="{{$item->id or ''}}" name="education_system_id">
<input type="hidden" value="eductional_systems" name="type" id="management_type">

<div class="clear"></div>