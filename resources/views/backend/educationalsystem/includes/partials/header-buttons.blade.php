 <div class="btn-group">
            <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                {{ trans('menus.backend.educationalsystem.main') }} <span class="caret"></span>
            </button>
        
    <ul class="dropdown-menu" role="menu">
    	@permission('view-education-system-management')
        <li><a href="{{ route('admin.educational-system.index') }}">List</a></li>
        @endauth
        
        @permission('create-education-system')
        <li><a href="{{ route('admin.educational-system.create') }}">Add New</a></li>
        @endauth
    </ul>
</div><!--btn group-->