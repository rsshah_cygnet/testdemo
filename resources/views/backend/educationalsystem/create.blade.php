@extends ('backend.layouts.master')

@section ('title', 'Create Educational System')

@section('page-header')

<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <h1>
                Educational System Management <small>CREATE</small>
            </h1>
        </div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.educationalsystem.includes.partials.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
	{!! Form::open([
		'route' 	=> 'admin.educational-system.store',
		'class' 	=> 'form-horizontal',
		'role' 		=> 'form',
		'method' 	=> 'post',
		'id' 		=> 'admin-brand-model'
	]) !!}

		{{-- Educational System Form --}}
	@include('backend.educationalsystem.form')


		<div class="box-footer">
		<a href="{{ route('admin.educational-system.index') }}" class="btn btn-red">Cancel</a>
		{!! Form::submit('Create', array("class"=>"btn btn-info pull-right")) !!}
	</div>

	{!! Form::close() !!}


@stop

@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{
		Imperial.EducationalSystem.init();
	});
</script>
@stop