<div class="btn-group">
    
    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>
        
    </button>

    <ul class="dropdown-menu" role="menu">

        <!-- @permission('view-topic-management') -->

        <li>
            <a href="{{ route('admin.question.index') }}">
                {{ trans('menus.backend.question.all') }}
            </a>
        </li>
        
        <!-- @endauth -->

        <!-- @permission('create-topic') -->
        
        <li>
            <a href="{{ route('admin.question.create') }}">
                {{ trans('menus.backend.question.create') }}
            </a>
        </li>
        
       <!--  @endauth -->

       <li>
            <a href="{{ route('admin.question.import') }}">
                {{ trans('menus.backend.question.import') }}
            </a>
        </li>
    </ul>
</div><!--btn group-->
<div class="clearfix"></div>