<div class="clear"></div>

<div class="form-group">
    {!! Form::label('curriculum_id', trans('validation.attributes.backend.question.curriculum'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('curriculum_id', ['' => 'Select curriculum'] + $curriculums ,isset($selectedCurriculam)?$selectedCurriculam:"", ['class' => 'form-control select2' , 'id' => 'curriculum_id','onchange'=>'getTopics(this.value)','required' =>'required']) !!}
        <p style="display: none;color: red;" id="no_topics">There are not any topics available in this Curriculum.please add topics first.</p>
    </div>
</div><!--form-group-->

<!-- topic -->
<div class="form-group topic_div" style="display: none;">
    {!! Form::label('topic_id', trans('validation.attributes.backend.question.topic'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('topic_id', ['' => 'Select topic'],isset($question->topic_id)?$question->topic_id:"", ['class' => 'form-control select2' , 'id' => 'topic_id','required' =>' required']) !!}
    </div>
</div>
<!-- topic end-->

<div class="form-group">
    {!! Form::label('question', trans('validation.attributes.backend.question.name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('question', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.question.name'),'required' =>' required']) !!}
    </div>
</div>



<div class="form-group">
    {!! Form::label('question', trans('validation.attributes.backend.question.image'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
       <input type="file" name="question_image" value="<?php echo isset($question->question_image)?$question->question_image:old('question_image');?>">

   </div>
</div>
<?php if(isset($question->question_image) && $question->question_image != "" && $question->question_image != null){ 
    $img_path = url('uploads/question/'.$question->question_image);
    //dd($img_path);
    ?>
    <img src="{{$img_path}}" style="margin-bottom:10px;width: 8%;margin-left:17%;">
    <?php }

    ?>
    

    <!-- option1 -->
    <?php  
    $option1_value = null; 
    if(isset($question->option1) && $question->option1 != "" && $question->option1 != null){ 
        $img_path_option1 = public_path('uploads/question/'.$question->option1);
        if (file_exists($img_path_option1)) {
            $option1_value = ''; ?>
            <input type="hidden" name="old_option1_img" id="old_option1_img" value="<?php echo isset($question->option1)?$question->option1:old('option1');?>">         
            <?php   }else{
                $option1_value = null;
            }
        }
        ?>

        <div class="form-group">
            {!! Form::label('option1', trans('validation.attributes.backend.question.option1'), ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('option1',$option1_value, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.question.option1')]) !!}
            </div>
        </div>

        <center><span>OR</span></center>

        <div class="form-group">
            {!! Form::label('option1_image','Option1 Image', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
               <input type="file" name="option1_image" value="<?php echo isset($question->option1)?$question->option1:"";?>">

           </div>
       </div>
       <?php if(isset($option1_value) && $option1_value == ""){ 
        $img_path = url('uploads/question/'.$question->option1);
    //dd($img_path);
        ?>
        <img id="image_option1" src="{{$img_path}}" style="margin-bottom:10px;width: 8%;margin-left:17%;">
        <a href="javascript:void(0);" id="delete_option1_img">Delete option1 image</a>
        <?php }

        ?>

        <!-- option1 end-->

        <!-- option2 -->
        <?php  
        $option2_value = null; 
        if(isset($question->option2) && $question->option2 != "" && $question->option2 != null){ 
            $img_path_option2 = public_path('uploads/question/'.$question->option2);
            if (file_exists($img_path_option2)) {
                $option2_value = ''; ?>
                <input type="hidden" name="old_option2_img" id="old_option2_img" value="<?php echo isset($question->option2)?$question->option2:"";?>">    
                <?php   }else{
                    $option2_value = null;
                }
            }
            ?>
            <div class="form-group">
                {!! Form::label('option2', trans('validation.attributes.backend.question.option2'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('option2', $option2_value, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.question.option2')]) !!}
                </div>
            </div>

            <center><span>OR</span></center>
            <div class="form-group">
                {!! Form::label('option2_image','Option2 Image', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                   <input type="file" name="option2_image" value="<?php echo isset($question->option2)?$question->option2:old('option2');?>">

               </div>
           </div>
           <?php if(isset($option2_value) && $option2_value == ""){
            $img_path = url('uploads/question/'.$question->option2);
    //dd($img_path);
            ?>
            <img id="image_option2" src="{{$img_path}}" style="margin-bottom:10px;width: 8%;margin-left:17%;">
            <a href="javascript:void(0);" id="delete_option2_img">Delete option2 image</a>
            <?php }

            ?>
            <!-- option2 end-->
            <!-- option3 -->
            <?php  
            $option3_value = null; 
            if(isset($question->option3) && $question->option3 != "" && $question->option3 != null){ 
                $img_path_option3 = public_path('uploads/question/'.$question->option3);
                if (file_exists($img_path_option3)) {
                    $option3_value = ''; ?>
                    <input type="hidden" name="old_option3_img" id="old_option3_img" value="<?php echo isset($question->option3)?$question->option3:"";?>"> 
                    <?php   }else{
                        $option3_value = null;
                    }
                }
                ?>
                <div class="form-group">
                    {!! Form::label('option3', trans('validation.attributes.backend.question.option3'), ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('option3', $option3_value, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.question.option3')]) !!}
                    </div>
                </div>

                <center><span>OR</span></center>
                <div class="form-group">
                    {!! Form::label('option3_image','Option3 Image', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                       <input type="file" name="option3_image" value="<?php echo isset($question->option3)?$question->option3:old('option3');?>">

                   </div>
               </div>
               <?php if(isset($option3_value) && $option3_value == ""){ 
                $img_path = url('uploads/question/'.$question->option3);
    //dd($img_path);
                ?>
                <img id="image_option3" src="{{$img_path}}" style="margin-bottom:10px;width: 8%;margin-left:17%;">
                <a href="javascript:void(0);" id="delete_option3_img">Delete option3 image</a>
                <?php }

                ?>
                <!-- option3 end-->
                <!-- option4 -->
                <?php  
                $option4_value = null; 
                if(isset($question->option4) && $question->option4 != "" && $question->option4 != null){ 
                    $img_path_option4 = public_path('uploads/question/'.$question->option4);
                    if (file_exists($img_path_option4)) {
                        $option4_value = ''; ?>
                        <input type="hidden" name="old_option4_img" id="old_option4_img" value="<?php echo isset($question->option4)?$question->option4:"";?>">      
                        <?php   }else{
                            $option4_value = null;
                        }
                    }
                    ?>
                    <div class="form-group">
                        {!! Form::label('option4', trans('validation.attributes.backend.question.option4'), ['class' => 'col-lg-2 control-label']) !!}
                        <div class="col-lg-10">
                            {!! Form::text('option4', $option4_value, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.question.option4')]) !!}
                        </div>
                    </div>

                    <center><span>OR</span></center>
                    <div class="form-group">
                        {!! Form::label('option4_image','Option4 Image', ['class' => 'col-lg-2 control-label']) !!}
                        <div class="col-lg-10">
                           <input type="file" name="option4_image" value="<?php echo isset($question->option4)?$question->option4:old('option4');?>">

                       </div>
                   </div>
                   <?php if(isset($option4_value) && $option4_value == ""){
                    $img_path = url('uploads/question/'.$question->option4);
    //dd($img_path);
                    ?>
                    <img id="image_option4" src="{{$img_path}}" style="margin-bottom:10px;width: 8%;margin-left:17%;">
                    <a href="javascript:void(0);" id="delete_option4_img">Delete option4 image</a>
                    <?php }

                    ?>
                    <!-- option4 end-->
                    <?php 
                    $ans = ['' => 'Select correct answer','1' => 'option1','2' => 'option2','3' => 'option3','4' => 'option4'];
                    ?>

                    <div class="form-group">
                        {!! Form::label('correct_answer', trans('validation.attributes.backend.question.ans'),['class' => 'col-lg-2 control-label required']) !!}
                        <div class="col-lg-10">
                            {!! Form::select('correct_answer', $ans,isset($question->correct_answer)?$question->correct_answer:"", ['class' => 'form-control select2' , 'id' => 'correct_answer','required' =>' required']) !!}
                            
                        </div>
                    </div>


                    @if(isset($formtype) && $formtype == "edit")

                    <div class="form-group">
                        <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
                        <div class="col-lg-1">
                            <div class="checkbox icheck">
                                <label>{{ Form::checkbox('record_status', 1, (isset($question->status) && $question->status == '1') ? true : false ) }}</label>
                            </div>
                        </div>
                    </div>
                    @endif
                    <input type="hidden" value="{{$question->id or ''}}" name="question_id">
                    <input type="hidden" name="question_img" value="<?php echo isset($question->question_image)?$question->question_image:old('question_image');?>">




                    <div class="box-footer">
                        <a href="{{route('admin.question.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
                         @if(isset($formtype) && $formtype == "edit")
                        <input id="submit_question" type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" />
                        @else
                        <input id="submit_question" type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" />
                        @endif 
                    </div>
<!-- /.box-body -->