@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.question.edit'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
    <h1>Question Management<small> EDIT</small></h1>
</div>
</div>
</div>

@endsection
<?php
// dd($topic);
?>
@section('content')
    <div class="box box-danger">
    <div class="box-body">
    {!! Form::model($question, ['route' => ['admin.question.store'], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            @include('backend.Question.form')
    {!! Form::close() !!}
@endsection

@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{

		getTopics($('#curriculum_id').val());

		setTimeout(function(){

			var selected_topic = "<?php echo isset($question->topic_id) ? $question->topic_id : ''; ?>";
			var selected_curriculum = "<?php echo isset($question->curriculum_id) ? $question->curriculum_id : ''; ?>";

			if(selected_curriculum != 0 && selected_curriculum != "null" && selected_curriculum != ""){
				$('#curriculum_id').val(selected_curriculum).trigger("change");
			}

			if(selected_topic != 0 && selected_topic != "null"){
				$('#topic_id').val(selected_topic).trigger("change");
			}
 		}, 400);

		$('#delete_option1_img').click(function(){
			$('#option1_image').val("");
			$('#old_option1_img').val("");
			$('#image_option1').remove();
			$(this).hide();
		})

		$('#delete_option2_img').click(function(){
			$('#option2_image').val("");
			$('#old_option2_img').val("");
			$('#image_option2').remove();
			$(this).hide();
		})

		$('#delete_option3_img').click(function(){
			$('#option3_image').val("");
			$('#old_option3_img').val("");
			$('#image_option3').remove();
			$(this).hide();
		})

		$('#delete_option4_img').click(function(){
			$('#option4_image').val("");
			$('#old_option4_img').val("");
			$('#image_option4').remove();
			$(this).hide();
		})
	});

</script>
@stop
