 @extends('backend.layouts.master')

@section('title', 'Exam Settings')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Exam Settings<small> EDIT</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">

</div>
</div>
</div>
</div>
@endsection

@section('content')
 <div class="box box-danger">
    <div class="box-body">
    {!!Form::model($settings, ['route' => ['admin.exam.settings.update', $settings->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH',  'files' => true  , 'id' => 'edit-settings']) !!}
        <h2>Exam Section:</h2>
        <div class="form-group">
            <!--form control-->
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    {!! Form::label('questions_per_topic', trans('validation.attributes.backend.settings.questions_per_topic'), ['class' => 'col-lg-4 control-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('number_of_questions_per_topic', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.questions_per_topic')]) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-lg-6">
                    {!! Form::label('minutes_per_questions', trans('validation.attributes.backend.settings.minutes_per_questions'), ['class' => 'col-lg-4 control-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('number_of_minutes_per_questions', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.minutes_per_questions')]) !!}
                    </div>
                </div>
        </div>
        <div class="form-group">
            <!--form control-->
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    {!! Form::label('passing_percentage', trans('validation.attributes.backend.settings.passing_percentage'), ['class' => 'col-lg-4 control-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('passing_percentage', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.passing_percentage')]) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-lg-6">
                    {!! Form::label('min_questions_in_test', trans('validation.attributes.backend.settings.min_questions_in_test'), ['class' => 'col-lg-4 control-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('min_questions_in_test', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.min_questions_in_test')]) !!}
                    </div>
                </div>
        </div>
        <div class="form-group">
            <!--form control-->
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    {!! Form::label('effort_for_reappearing_test', trans('validation.attributes.backend.settings.effort_for_reappearing_test'), ['class' => 'col-lg-4 control-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('reappearing_test', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.settings.effort_for_reappearing_test')]) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-lg-6">
                    {!! Form::label('no_of_mark_per_question', 'No. of Mark Per Question', ['class' => 'col-lg-4 control-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('no_of_mark_per_question', null, ['class' => 'form-control', 'placeholder' => 'No. of Mark Per Question']) !!}
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{route('admin.dashboard')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
                <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" />
            </div>
            {{Form::close()}}
    </div>
</div>
@stop