@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.question.create'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
    <h1>{{ trans('labels.backend.question.management') }}<small> CREATE</small></h1>
</div>    
</div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => 'admin.question.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
@include('backend.Question.form')            
{!! Form::close() !!}
@endsection
@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{
			/*getEducational($('#curriculum_id').val());*/
			if($('#curriculum_id').val() != "" ){
				$('#curriculum_id').change();	
			}

			$('body').on('change','#topic_id',function(){
				var topic_id = $('#topic_id').val();
				$.session.set('topic_id', topic_id);
			})

			var session_topic_id = $.session.get("topic_id");
			 if (typeof session_topic_id != 'undefined' && session_topic_id != '' && session_topic_id != 0){
		 	        setTimeout(function(){
							$('#topic_id').val(session_topic_id).trigger("change");
								
					 }, 400);
		    }

			
	})	
</script>
@stop
