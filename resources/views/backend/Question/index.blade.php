 @extends('backend.layouts.master')

@section('title', 'Question Management')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Question Management<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.Question.includes.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
     <div class="box box-danger">
        <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.question.data', ['page'=>'1']) !!}",
         labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;0:InActive;1:Active",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
            dateSearchOptions           = getSearchOption('date');

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value =
        {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [

                {title: false, name: 'Curriculum',index: 'curriculum.curriculum_name',searchoptions:stringSearchOptions},

                {title: false, name: 'Topic',index: 'topic.topic_name',searchoptions:stringSearchOptions},

                {title: false, name: 'Question',index: 'question.question',searchoptions:stringSearchOptions},
                {title: false, name: 'Ans',index: 'question.correct_answer',searchoptions:stringSearchOptions,
                 formatter: function (cellvalue, options, rowObject) 
                    {
                        if (cellvalue == 1) 
                        {
                            return "<label class='label label-success'>Option1</label>"
                        } 
                        else if (cellvalue == 2) 
                        {
                             return "<label class='label label-success'>Option2</label>"
                        }else if (cellvalue == 3) 
                        {
                             return "<label class='label label-success'>Option3</label>"
                        }else if (cellvalue == 4) 
                        {
                             return "<label class='label label-success'>Option4</label>"
                        }
                    }
                },
                {title: false, name: 'Option1',index: 'question.option1',searchoptions:stringSearchOptions,
                 formatter: function (cellvalue, options, rowObject) 
                    {
                        if (cellvalue) 
                        { 
                            var url = "<?php echo url('uploads/question/');?>";
                            var final_path = url+'/'+cellvalue;
                            if(fileExists(final_path)){
                            var img = $('<img />', {src: final_path,style:"width:50%;"});
                            //  console.log(img['0'].outerHTML);
                              return img['0'].outerHTML
                            }else{
                                return cellvalue
                            } 
                            
                        }
                    }
                },
                 {title: false, name: 'Option2',index: 'question.option2',searchoptions:stringSearchOptions,
                 formatter: function (cellvalue, options, rowObject) 
                    {
                        if (cellvalue) 
                        { 
                            var url = "<?php echo url('uploads/question/');?>";
                            var final_path = url+'/'+cellvalue;
                            if(fileExists(final_path)){
                            var img = $('<img />', {src: final_path,style:"width:50%;"});
                            //  console.log(img['0'].outerHTML);
                              return img['0'].outerHTML
                            }else{
                                return cellvalue
                            } 
                            
                        }
                    }
                },
                 {title: false, name: 'Option3',index: 'question.option3',searchoptions:stringSearchOptions,
                 formatter: function (cellvalue, options, rowObject) 
                    {
                        if (cellvalue) 
                        { 
                            var url = "<?php echo url('uploads/question/');?>";
                            var final_path = url+'/'+cellvalue;
                            if(fileExists(final_path)){
                            var img = $('<img />', {src: final_path,style:"width:50%;"});
                            //  console.log(img['0'].outerHTML);
                              return img['0'].outerHTML
                            }else{
                                return cellvalue
                            } 
                            
                        }
                    }
                },
                {title: false, name: 'Option4',index: 'question.option4',searchoptions:stringSearchOptions,
                 formatter: function (cellvalue, options, rowObject) 
                    {
                        if (cellvalue) 
                        { 
                            var url = "<?php echo url('uploads/question/');?>";
                            var final_path = url+'/'+cellvalue;
                            if(fileExists(final_path)){
                            var img = $('<img />', {src: final_path,style:"width:50%;"});
                            //  console.log(img['0'].outerHTML);
                              return img['0'].outerHTML
                            }else{
                                return cellvalue
                            } 
                            
                        }
                    }
                },
               
                 {title: false, name: 'Status',  index: 'question.status',  stype: 'select',searchoptions: selectStatusSearchOptions,
                    formatter: function (cellvalue, options, rowObject) 
                    {
                        if (cellvalue == 0) 
                        {
                            return "<label class='label label-danger'>InActive</label>"
                        } 
                        else if (cellvalue == 1) 
                        {
                             return "<label class='label label-success'>Active</label>"
                        }
                    }
                },
               
                 {title: false, name: 'act',index: 'act', sortable: false, search: false}

            ],
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop