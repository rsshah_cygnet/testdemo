@extends ('backend.layouts.master')

@section ('title', 'Import Question')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
    <h1>Question Management<small> IMPORT</small></h1>
</div>
</div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => 'admin.question.save', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

<div class="clear"></div>

<div class="form-group">
    {!! Form::label('curriculum_id', trans('validation.attributes.backend.question.curriculum'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('curriculum_id', ['' => 'Select curriculum'] + $curriculums ,isset($selectedCurriculam)?$selectedCurriculam:"", ['class' => 'form-control select2' , 'id' => 'curriculum_id','onchange'=>'getTopics(this.value)','required' =>' required']) !!}
        <p style="display: none;color: red;" id="no_topics">There are not any topics available in this Curriculum.please add topics first.</p>
    </div>
</div><!--form-group-->


<!-- topic -->
<div class="form-group topic_div" style="display: none;">
    {!! Form::label('topic_id', trans('validation.attributes.backend.question.topic'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('topic_id', ['' => 'Select topic'],isset($question->topic_id)?$question->topic_id:"", ['class' => 'form-control select2' , 'id' => 'topic_id','required' =>' required']) !!}
    </div>
</div>
<!-- topic end-->

<div class="form-group">
    {!! Form::label('imported_file','File', ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
       <input type="file" name="imported_file" id="imported_file" value="" required="required">

    </div>
</div>


<div class="box-footer">
    <a href="{{route('admin.question.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
    <input id="submit_question" type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.save') }}" />
</div>
<!-- /.box-body -->


{!! Form::close() !!}
@endsection
@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{
			var curriculum_id = $('#curriculum_id').val();
			if(curriculum_id != "" && curriculum_id != 0){
				getTopics($('#curriculum_id').val());
			}

	})
</script>
@stop
