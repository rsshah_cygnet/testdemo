{!! Form::open([
	'class' 	=> 'form-horizontal',
	'role' 		=> 'form',
	'method' 	=> 'post',
	'id' 		=> 'admin-menu-management'
]) !!}

	{{-- Menu Management Form --}}
	@include('backend.menumanagement.form')

	
	<div class="box-footer">
		<a href="javascript:void(0)" class="btn btn-red close-model">Cancel</a>
		{!! Form::submit('Save', array("class"=>"btn btn-info pull-right store-form")) !!}
	</div>

{!! Form::close() !!}