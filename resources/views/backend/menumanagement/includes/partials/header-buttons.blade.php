<div class="btn-group">
        <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.menumanagement.select') }} <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ route('admin.menu-management.store') }}">{{ trans('menus.backend.menumanagement.all') }}</a></li>

            <li><a href="javascript:void(0)" class="create-form">{{ trans('menus.backend.menumanagement.create') }}</a></li>
         
        </ul>
    </div><!--btn group-->