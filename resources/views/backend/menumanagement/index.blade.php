@extends('backend.layouts.master')

@section('title', 'Menus Management')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Menus Management<small> Listing</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
      @include('backend.menumanagement.includes.partials.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('modal')
<div class="model-container">
    <div class="modal fade model-wrapper" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                    
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"> Menus Management </h4>
                </div>
                
                <div class="hidden  alert model-alert">
    
                </div>

                <div id="model-body" class="modal-body">
                
                </div>

            </div>
        </div>
    </div>        
</div>
@stop

@section('content')
    <div class="box box-danger">
         <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';
    
    moduleConfig = {
        gridURL: "{!! route('admin.menu-management.data', ['page'=>'1']) !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        createURL:  "{!! route('admin.menu-management.create') !!}",
        storeURL:  "{!! route('admin.menu-management.store') !!}",
        updateURL:  "{!! route('admin.menu-management.update', ['id'=> '-1']) !!}",
        editURL:    "{!! route('admin.menu-management.edit', ['id'=> '-1'])!!}"
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader), 
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;Active:Active;InActive:InActive",
            stringSearchOptions         = getSearchOption('string'),
            dateSearchOptions           = getSearchOption('date'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value = 
        {
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [
                {title: false, name: 'Menu Name',  index: 'menus.name',             searchoptions:stringSearchOptions},
                {title: false, name: 'Parent Menu',  index: 'parentmenu.parent', searchoptions:stringSearchOptions},
                {title: false, name: 'Website',  index: 'domains.domainid', searchoptions:stringSearchOptions},
                {title: false, name: 'Status',      index: 'menus.status',
                    stype: 'select',
                    searchoptions: selectStatusSearchOptions
                },
                {title: false, name: 'Created By',  index: 'users.first_name',              searchoptions:stringSearchOptions},
                {title: false, name: 'Created At',index: 'menus.created_at',stype:'date',searchoptions:dateSearchOptions},
                 {title: false, name: 'act', index: 'act', sortable: false, search: false, width: 80}
            ],
            
        };

        CustomGrid(Required_Key_Value);
    });

    //hide the model on click of close button
    $( document ).on( 'click', '.close-model', function() 
    {
        $(".model-wrapper").modal('hide');
    });

    //hide the alert on close event of modal
    $('#myModal').on('hidden.bs.modal', function() 
    {
        $("div.model-alert").addClass("hidden");
    });

    $( document ).on( 'click', '.create-form', function()
    {
        showModalForm('create');
    });

    $( document ).on( 'submit', 'form#admin-menu-management', function(e) {
        e.preventDefault();
        var data = $(this).serialize();
        storeModalForm(data);
        
    });

    $( document ).on( 'click', '.edit-form', function(e) {
        var id = $(this).attr('data-id');
        showModalForm('edit', id);
    });

    $( document ).on( 'submit', 'form#edit-menumanagement', function(event) 
    {
        event.preventDefault();

        var data    = $(this).serialize(),
            id      = $("#hidden").val();
        
        updateModalForm(id,data);
    });


    function showModalForm(option, id)
    {
        var url = moduleConfig.createURL;

        if(option == 'edit')
        {
            url = moduleConfig.editURL.replace('-1', id);
        }

        jQuery.ajax(
        {
            type:   "GET",
            url:    url,
            async:  true,
            success: function(data)
            {    
                handleModalBoxEvent(data);
            }
        });
    }

    function storeModalForm(data){
        var url = moduleConfig.storeURL;
        jQuery.ajax(
        {
            type:   "POST",
            url:    url,
            data:   data,
            success: function(data)
            {
                if(data.status)
                {
                    $( "div.page-alert" ).html(data.message).addClass("success alert-success");
                    $(".model-wrapper").modal('hide');
                    $("#list").trigger("reloadGrid",[{page:1,current:true}]);
                    $("div.page-alert").removeClass("hidden");
                }else{
                    $("div.model-alert").html("Menu Already Exsist").addClass("danger alert-danger");
                    $("div.model-alert").removeClass("hidden");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) 
            {
                if(errorThrown)
                {
                    $("div.model-alert").html("Invalid Input").addClass("danger alert-danger");
                    $("div.model-alert").removeClass("hidden");
                }
            }
        });
    }

    function updateModalForm(id, data){
        
        var url = moduleConfig.updateURL.replace('-1', id);
        
        jQuery.ajax({
            type: "PATCH",
            dataType : "json",
            data:data,
            url: url,
            async: true,
            success: function(data)
            {    
                if(data.status)
                {
                    $( "div.page-alert" ).html(data.message).addClass("success alert-success");
                    $(".model-wrapper").modal('hide');
                    $("#list").trigger("reloadGrid",[{page:1,current:true}]);
                    $("div.page-alert").removeClass("hidden");
                }else{
                    $("div.model-alert").html("Menu Already Exsist").addClass("danger alert-danger");
                    $("div.model-alert").removeClass("hidden");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                if(errorThrown)
                {
                    $("div.model-alert").html("Menu Already Exsist").addClass("danger alert-danger");
                    $("div.model-alert").removeClass("hidden");
                }
            }
        });
    }

    function handleModalBoxEvent(data)
    {
        jQuery( ".modal-body" ).html(data);

        $(".model-wrapper").modal('show');  
        
        $(".accAj").select2();

        $(".accAjMenu").select2();

        $(".accAjCms").select2();

        $(".accAj").change(function() {
            var value = $('select.accAj option:selected').val();
            ajax_call(value);
        });
    }

    function ajax_call(value)
    {   
        $.ajax({
            type:"POST",
            url : 'menu-management/cmspage',
            data : {'_token':"{{csrf_token()}}",value:value},
            async: false,
            success : function(data) {
                // Parse the returned json data
                var options = $.parseJSON(data);
                $('.cmspages').empty();
                $('.cmspages').append("<option value=''>Select Content Page</option>");
                
                $.each(options, function(key, element) {
                    $('.cmspages').append("<option value='" + key +"'>" + element + "</option>");
                });                
            },
            error: function() {
                alert('Error occured');
            }
        });
    }

</script>
@stop