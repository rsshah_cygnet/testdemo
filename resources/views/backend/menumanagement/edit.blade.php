{!!Form::model($item, ['route' => ['admin.menu-management.update', $item->id],'class' => 'form-horizontal',
	'role' 		=> 'form',
	'method' 	=> 'PATCH',
	'id' 		=> 'edit-menumanagement']) !!}

{{-- Menu Management Form --}}
@include('backend.menumanagement.form')

<div class="box-footer">
	<a href="javascript:void(0)" class="btn btn-red close-model">Cancel</a>
	{!! Form::submit('Save', array("class"=>"btn btn-info pull-right update-form")) !!}
</div>

{!! Form::close() !!}
