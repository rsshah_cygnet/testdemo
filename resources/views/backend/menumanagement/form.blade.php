<div class="clear"></div>
<div class="form-horizontal">
 <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        
        {!! Form::label('Menu_Item_Name', "Menu Item Name", ['class' => 'col-lg-2 control-label required']) !!}
        
        <div class="col-lg-10">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => "Menu Item Name", 'required' =>' required']) !!}
        </div>
    </div>



    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        
        {!! Form::label('domain_id', "Website", ['class' => 'col-lg-2 control-label required']) !!}
        
        <div class="col-lg-10">
        	{!! Form::select('domain_id',$website, 
        	isset($item->domain_id) ? $item->domain_id : '', array('class' => 'form-control accAj','placeholder' => "Select Website", 'required', 'style' => 'width: 100%')) !!}
            
        </div>
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        
        {!! Form::label('cmspage_id', "Content Page", ['class' => 'col-lg-2 control-label required']) !!}
        
        <div class="col-lg-10">
            {!! Form::select('cmspage_id',$contentpage, 
            isset($item->cmspage_id) ? $item->cmspage_id : '', array('class' => 'form-control cmspages accAjCms','placeholder' => "Select Content Page", 'required', 'style' => 'width: 100%')) !!}
            
        </div>
    </div>
    
    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        
        {!! Form::label('parent_menu_id', "Parent Menu", ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">

            {!! Form::select('parent_menu_id',$parentmenu, 
            isset($item->parent_menu_id) ? $item->parent_menu_id : '', array('class' => 'form-control accAjMenu','placeholder' => "Select Parent Menu", 'required', 'style' => 'width: 100%')) !!}
            
        </div>
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label('status', "Status", ['class' => 'col-lg-2 control-label']) !!}
        
        <div class="col-lg-10">
          <div class="checkbox icheck">
            <label>{{ Form::checkbox('record_status', 1, (isset($item) && $item->status == 'Active') ? true : false ) }}</label>
        </div>
        </div>
    </div>

    @if(isset($item))
         <div class="col-lg-10">
            {{ Form::hidden('id', $item->id ,['id' => 'hidden'])}}
        </div>
    @endif

</div>
<div class="clear"></div>
