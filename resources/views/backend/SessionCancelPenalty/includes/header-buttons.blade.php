<div class="btn-group">

    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>

    </button>

    <ul class="dropdown-menu" role="menu">

       <!-- @permission('view-session-cancel-penalty-management') -->

        <li>
            <a href="{{ route('admin.session-cancel-penalty.index') }}">
                {{ trans('menus.backend.session_cancel_penalty.all') }}
            </a>
        </li>

       <!-- @endauth -->

       <!--   @permission('create-punctuality-ratings') -->

        <li>
            <a href="{{ route('admin.session-cancel-penalty.create') }}">
                {{ trans('menus.backend.session_cancel_penalty.create') }}
            </a>
        </li>

       <!--   @endauth -->
    </ul>
</div><!--btn group-->
<div class="clearfix"></div>