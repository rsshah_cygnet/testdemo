@extends ('backend.layouts.master')

@section ('title', 'Edit Penalty for Cancel Session')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Penalty Management for Cancel Session<small> EDIT</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.SessionCancelPenalty.includes.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('after-styles-end')
	<style type="text/css">
		.select2-selection__choice
		{
			color: #000 !important;
		}
	</style>
@endsection

@section('content')
<div class="box box-danger">

	<div class="box-body">
		{!!Form::model($penalty, ['route' => ['admin.session-cancel-penalty.update', $penalty->id],'class' => 'form-horizontal','role' 		=> 'form','method' 	=> 'PATCH','id' => 'edit-blog']) !!}

		{{-- Brand Model Form --}}
		@include('backend.SessionCancelPenalty.form')
	</div>

	<div class="box-footer">
		<a href="{{ route('admin.session-cancel-penalty.index') }}" class="btn btn-red">
			Cancel
		</a>
		{!! Form::submit('Update', array("class"=>"btn btn-info pull-right")) !!}
	</div>

</div>

	{!! Form::close() !!}
@stop
