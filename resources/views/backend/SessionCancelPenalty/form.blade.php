<?php
//print_r($curriculum);die;
?>

<div class="clear"></div>

<div class="form-horizontal">

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('type',"Type",['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            {!! Form::select('type', $type, isset($penalty->type)?$penalty->type:'', array('class' => 'form-control select2', 'required', 'style' => 'width: 100%', 'id' => 'type')) !!}
        </div>
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('From',"From",['class' => 'col-md-3 control-label required', 'id' => 'fromlabel']) !!}
        <div class="col-md-8">
            {!! Form::text('from', isset($penalty->from)?$penalty->from:'', ['class' => 'form-control','placeholder' => 'From', 'id' => 'from','maxlength' => '5']) !!}
        </div>
    </div>

     <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('To',"To",['class' => 'col-md-3 control-label required','id' => 'tolabel']) !!}
        <div class="col-md-8">
            {!! Form::text('to', isset($penalty->to)?$penalty->to:'', ['class' => 'form-control','placeholder' => 'To', 'id' => 'to','maxlength' => '5']) !!}
        </div>
    </div>

  <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('within',"Within",['class' => 'col-md-3 control-label required','id' => 'withinlabel']) !!}
        <div class="col-md-8">
            {!! Form::text('within', isset($penalty->within)?$penalty->within:'', ['class' => 'form-control','placeholder' => 'Within', 'id' => 'within','maxlength' => '5']) !!}
        </div>
    </div>

     <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('penalty_percentage',"Penalty Percentage",['class' => 'col-md-3 control-label required','id' => 'penalty_percentagelabel']) !!}
        <div class="col-md-8">
            {!! Form::text('penalty_percentage', isset($penalty->penalty_percentage)?$penalty->penalty_percentage:'', ['class' => 'form-control','placeholder' => 'Penalty Percentage', 'id' => 'penalty_percentage','maxlength' => '5']) !!}
        </div>
    </div>

@if(!empty($penalty))
<div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
    {!! Form::label('Status',"Active",['class' => 'col-md-3 control-label required',]) !!}
    <div class="col-lg-1">
        <div class="checkbox icheck">
          {{ Form::checkbox('status', 1, (isset($penalty) && $penalty->status == '1') ? true : false )}}
      </div>
  </div>
</div>
@endif
</div>
@if(!empty($penalty))
{{Form::hidden('id', $penalty->id)}}
@endif
<div class="clear"></div>

@section('after-scripts-end')

<script>
$(document).ready(function(){

  $('#type').change(function(){
    if($(this).val() == 1)
    {
        $('#from').show();
        $('#fromlabel').show();
        $('#to').show();
        $('#tolabel').show();
        $('#penalty_percentage').show();
        $('#penalty_percentagelabel').show();
         $('#within').hide();
      $('#withinlabel').hide();

    }
    else if($(this).val() == 2)
    {
      $('#within').show();
      $('#withinlabel').show();
      $('#penalty_percentage').show();
      $('#penalty_percentagelabel').show();
      $('#from').hide();
       $('#fromlabel').hide();
      $('#to').hide();
       $('#tolabel').hide();
    }
    else{
      $('#from').hide();
      $('#fromlabel').hide();
      $('#to').hide();
      $('#tolabel').hide();
      $('#within').hide();
      $('#withinlabel').hide();
      $('#penalty_percentage').hide();
      $('#penalty_percentagelabel').hide();
    }
  });
  $('#from').hide();
      $('#fromlabel').hide();
      $('#to').hide();
      $('#tolabel').hide();
      $('#within').hide();
      $('#withinlabel').hide();
      $('#penalty_percentage').hide();
      $('#penalty_percentagelabel').hide();

      if($('#type option:selected').val() == 1)
    {
        $('#from').show();
        $('#fromlabel').show();
        $('#to').show();
        $('#tolabel').show();
        $('#penalty_percentage').show();
        $('#penalty_percentagelabel').show();
        $('#within').hide();
        $('#withinlabel').hide();

    }
    else if($('#type option:selected').val() == 2)
    {
      $('#within').show();
      $('#withinlabel').show();
      $('#penalty_percentage').show();
      $('#penalty_percentagelabel').show();
      $('#from').hide();
       $('#fromlabel').hide();
      $('#to').hide();
       $('#tolabel').hide();
    }
});
</script>
@stop