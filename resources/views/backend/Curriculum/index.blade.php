 @extends('backend.layouts.master')

@section('title', 'Curriculum Management')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Curriculum Management<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.Curriculum.includes.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
     <div class="box box-danger">
        <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.curriculum.data', ['page'=>'1']) !!}",
         labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;0:InActive;1:Active",
            StatusStrRole               = ":both;1:Tutor;2:Tutee",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
            selectStatusSearchOptionsRole = getSearchOption('select', StatusStrRole);
            dateSearchOptions           = getSearchOption('date');

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value =
        {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [

                {title: false, name: 'Name',index: 'curriculum.curriculum_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Higher Level Fee',index: 'curriculum.higher_level_fee',searchoptions:stringSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == 0 || cellvalue == 'NULL')
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }
                        else
                        {
                             return cellvalue
                        }
                    }
                },
                {title: false, name: 'Lower Level Fee',index: 'curriculum.lower_level_fee',searchoptions:stringSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == 0 || cellvalue == 'NULL')
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }
                        else
                        {
                             return cellvalue
                        }
                    }
                },
                 {title: false, name: 'Status',  index: 'curriculum.status',  stype: 'select',searchoptions: selectStatusSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == 0)
                        {
                            return "<label class='label label-danger'>InActive</label>"
                        }
                        else if (cellvalue == 1)
                        {
                             return "<label class='label label-success'>Active</label>"
                        }
                    }
                },
                 {title: false, name: 'act',index: 'act', sortable: false, search: false}

            ],
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop