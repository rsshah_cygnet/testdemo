 @extends('backend.layouts.master')

 @section('title', 'Curriculum Management')

 @section('page-header')
 <div class="page-tatil-box clearfix">
 	<div class="row">
 		<div class="col-xs-12 col-lg-6"><h1>Curriculum Management<small> LISTING</small></h1></div>
 		<div class="col-xs-12 col-lg-6">
 			<div class="box-tools">
 				@include('backend.Curriculum.includes.header-buttons')
 			</div>
 		</div>
 	</div>
 </div>
 @endsection

 @section('content')
 <div class="box box-danger">
 	<div class="box-body curriculum-links">
 		<div class="row">
 			<div class="col-xs-12 col-sm-12 col-md-6">
 				<ul class="list-unstyled">
 					<li><a href="{{url('admin/curriculum')}}">Curriculum Management</a></li>
 					<li><a href="{{url('admin/educational-system')}}">{{ trans('menus.backend.educationalsystem.management') }}</a></li>
 					<li><a href="{{url('admin/programs')}}">Program Management</a></li>
 					<li><a href="{{url('admin/levels')}}">Level Management</a></li>
 					<li><a href="{{url('admin/grade')}}">Grade Management</a></li>
 					<li><a href="{{url('admin/subject')}}">Subject Management</a></li>
 					<li><a href="{{url('admin/topic')}}">Topic Management</a></li>
 				</ul>
 			</div>
 		</div>
 	</div>
 </div>
 @stop

 @section('after-scripts-end') 
 

 @stop