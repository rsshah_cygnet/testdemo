<?php
//print_r($curriculum);die;
?>

<div class="clear"></div>

<div class="form-horizontal">

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('Curriculum Name',"Curriculum Title",['class' => 'col-md-3 control-label required',]) !!}
        <div class="col-md-8">
            {!! Form::text('curriculum_name', null, ['class' => 'form-control','placeholder' => 'Curriculum Title','maxlength' => 255, 'required']) !!}
        </div>
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('Curriculum higher Level Fee ',"Higher Level Fees",['class' => 'col-md-3 control-label',]) !!}
        <div class="col-md-8">
            {!! Form::text('higher_level_fee', null, ['class' => 'form-control','placeholder' => 'Higher Level Fees','maxlength' => 255]) !!}
        </div>
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('Curriculum lower Level Fee',"Lower Level Fees",['class' => 'col-md-3 control-label',]) !!}
        <div class="col-md-8">
            {!! Form::text('lower_level_fee', null, ['class' => 'form-control','placeholder' => 'Lower Level Fees','maxlength' => 255]) !!}
        </div>
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('display_in_program',"Display in Program",['class' => 'col-md-3 control-label ',]) !!}
        <div class="col-lg-1">
            <div class="checkbox icheck">
              {{ Form::checkbox('display_in_program', 1, (isset($curriculum) && $curriculum->display_in_program == '1') ? true : false,['id' => 'display_in_program', (isset($curriculum)) ? "disabled":"" ])}}
          </div>
      </div>
  </div>

  <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
  {!! Form::label('display_in_level',"Display in Level",['class' => 'col-md-3 control-label ',]) !!}
    <div class="col-lg-1">
        <div class="checkbox icheck">
          {{ Form::checkbox('display_in_level', 1, (isset($curriculum) && $curriculum->display_in_level == '1') ? true : false,['id' => 'display_in_level', (isset($curriculum)) ? "disabled":"" ] )}}
      </div>
  </div>
</div>

<div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
    {!! Form::label('display_in_eductional_systems',"Display in Educational System",['class' => 'col-md-3 control-label ',]) !!}
    <div class="col-lg-1">
        <div class="checkbox icheck">
          {{ Form::checkbox('display_in_eductional_systems', 1, (isset($curriculum) && $curriculum->display_in_eductional_systems == '1') ? true : false, ['id' => 'display_in_eductional_systems', (isset($curriculum)) ? "disabled":"" ])}}
      </div>
  </div>
</div>

@if(!empty($curriculum))
{{-- <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
    {!! Form::label('Status',"Active",['class' => 'col-md-3 control-label required',]) !!}
    <div class="col-lg-1">
        <div class="checkbox icheck">
          {{ Form::checkbox('status', 1, (isset($curriculum) && $curriculum->status == '1') ? true : false )}}
      </div>
  </div>
</div> --}}
@endif
</div>
@if(!empty($curriculum))
{{Form::hidden('id', $curriculum->id)}}
@endif
<div class="clear"></div>

@section('after-scripts-end')

<script type="text/javascript">

</script>
@stop