<div class="btn-group">

    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>

    </button>

    <ul class="dropdown-menu" role="menu">

        @permission('view-curriculam-management')

        <li>
            <a href="{{ route('admin.curriculum.index') }}">
                {{ trans('menus.backend.blog.all') }}
            </a>
        </li>

        @endauth

        @permission('create-curriculam')

        <li>
            <a href="{{ route('admin.curriculum.create') }}">
                {{ trans('menus.backend.blog.create') }}
            </a>
        </li>

        @endauth
    </ul>
</div><!--btn group-->
<div class="clearfix"></div>