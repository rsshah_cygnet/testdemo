<div class="clear"></div>
<div id="custom_dropdowns">
<div class="form-group">
    {!! Form::label('topic_name', trans('validation.attributes.backend.topic.name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('topic_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.topic.name'),'required' =>' required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('curriculum_id', trans('validation.attributes.backend.level.curriculum'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('curriculum_id', ['' => 'Select curriculum'] + $curriculums ,isset($topic->curriculum_id)?$topic->curriculum_id:"", ['class' => 'form-control select2 dynamic_dropdown' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required' =>' required']) !!}
    </div>
</div>
</div>

@if(isset($formtype) && $formtype == "edit")
    {{-- <div class="form-group">
        <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
            <div class="col-lg-1">
            <div class="checkbox icheck">
                <label>{{ Form::checkbox('record_status', 1, (isset($topic->status) && $topic->status == '1') ? true : false ) }}</label>
            </div>
        </div>
    </div> --}}
@endif
<input type="hidden" value="{{$topic->id or ''}}" name="topic_id">
<input type="hidden" value="topic" name="type" id="management_type">
@if(isset($formtype) && $formtype == "edit")
<input type="hidden" value="{{$topic->curriculum_id or ''}}" name="curriculum_id">
@endif
<div class="box-footer">
    <a href="{{route('admin.topic.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a> @if(isset($level))
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" /> @else
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" /> @endif
</div>
