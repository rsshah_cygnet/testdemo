 @extends('backend.layouts.master')

@section('title', 'Topic Management')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Topic Management<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.Topic.includes.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
     <div class="box box-danger">
        <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.topic.data', ['page'=>'1']) !!}",
         labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    /*Javascript for load grid and fill data in to the grid*/
    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;0:InActive;1:Active",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
            dateSearchOptions           = getSearchOption('date');

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value =
        {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [

                {title: false, name: 'Topic Name',index: 'topic.topic_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Curriculam',index: 'curriculum.curriculum_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Eductional system',index: 'eductional_systems.name',searchoptions:stringSearchOptions,
                formatter: function (cellvalue, options, rowObject) 
                    {
                        if (!cellvalue) 
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }else{
                            return cellvalue
                        } 
                        
                    }
                },
                {title: false, name: 'Level',index: 'levels.levels_name',searchoptions:stringSearchOptions,
                formatter: function (cellvalue, options, rowObject) 
                    {
                        if (!cellvalue) 
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }else{
                            return cellvalue
                        } 
                        
                    }
                },
                {title: false, name: 'Grade',index: 'grade.grade_name',searchoptions:stringSearchOptions,
                formatter: function (cellvalue, options, rowObject) 
                    {
                        if (!cellvalue) 
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }else{
                            return cellvalue
                        } 
                        
                    }
                },
                {title: false, name: 'Program',index: 'program.program_name',searchoptions:stringSearchOptions,
                formatter: function (cellvalue, options, rowObject) 
                    {
                        if (!cellvalue) 
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }else{
                            return cellvalue
                        } 
                        
                    }
                },
                {title: false, name: 'Subject',index: 'subject.subject_name',searchoptions:stringSearchOptions,
                formatter: function (cellvalue, options, rowObject) 
                    {
                        if (!cellvalue) 
                        {
                            return "<label class='label label-success'>N/A</label>"
                        }else{
                            return cellvalue
                        } 
                        
                    }
                },
                 {title: false, name: 'Status',  index: 'topic.status',  stype: 'select',searchoptions: selectStatusSearchOptions,
                    formatter: function (cellvalue, options, rowObject) 
                    {
                        if (cellvalue == 0) 
                        {
                            return "<label class='label label-danger'>InActive</label>"
                        } 
                        else if (cellvalue == 1) 
                        {
                             return "<label class='label label-success'>Active</label>"
                        }
                    }
                },
               
                 {title: false, name: 'act',index: 'act', sortable: false, search: false}

            ],
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop