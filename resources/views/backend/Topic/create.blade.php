@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.topic.create'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>{{ trans('labels.backend.topic.management') }}<small> CREATE</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.Topic.includes.header-buttons')
</div>
</div>
</div>
</div>

@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => 'admin.topic.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@include('backend.Topic.form')
{!! Form::close() !!}
@endsection

@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{
			getEducational($('#curriculum_id').val());
	})
</script>
@stop
