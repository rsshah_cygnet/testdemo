<div class="btn-group">
    
    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>
        
    </button>

    <ul class="dropdown-menu" role="menu">

        @permission('view-topic-management')

        <li>
            <a href="{{ route('admin.topic.index') }}">
                {{ trans('menus.backend.topic.all') }}
            </a>
        </li>
        
        @endauth

        @permission('create-topic')
        
        <li>
            <a href="{{ route('admin.topic.create') }}">
                {{ trans('menus.backend.topic.create') }}
            </a>
        </li>
        
        @endauth
    </ul>
</div>
<div class="clearfix"></div>