<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.cmspages.management-sub') }}</h3>

        <div class="box-tools pull-right">
            @include('backend.cmspages.includes.header-buttons')
        </div>
    </div><!-- /.box-header -->

    <div class="box-body"> 
        <div class="dataTables_wrapper form-inline dt-bootstrap">                   
            <div class="row">
                <div class="col-sm-9">
                    <div class="dataTables_length">
                        {!! Form::label('type', trans('labels.backend.page_size')) !!}
                        {!!  Form::select('pagesize', array('10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30' ), null, ['id'=>'pagesize']) !!}  
                        <button id="trigerDelete" class="btn bg-purple btn-flat">{{ trans('labels.backend.delete') }}</button>
                        @if ( count($records) > 0 )
                        <a class="btn bg-purple btn-flat" href="{{ route('admin.cmspages.export') }}">{{ trans('labels.backend.export_excel') }}</a>
                        <a class="btn bg-purple btn-flat" href="{{ route('admin.cmspages.csv') }}">{{ trans('labels.backend.export_csv') }}</a>
                        @endif
                    </div>
                </div>
                <div class="col-sm-3">
                    <div id="grid-table_filter" class="dataTables_filter"><label>Search: {!! Form::text('search-top', null, ['class' => 'form-control input-sm','id' => 'search-top', 'placeholder' => trans('labels.backend.search')]) !!}</label></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12"> 
                    <!-- hidden fields for sorting -->
                    {!! Form::hidden('orderby', 'id', ['class' => 'form-control input-sm','id' => 'orderby']) !!}
                    {!! Form::hidden('sort', 'asc', ['class' => 'form-control input-sm','id' => 'sort']) !!}
                    <table id="grid-table" class="table table-striped table-hover table-bordered dataTable" role="grid" aria-describedby="grid-table_info">
                        <thead>
                            <tr>
                                <th><input type="checkbox"  id="bulkDelete"  />  </th>
                                <th class="sorting" data-orderby="title" data-sort="asc" >{{ trans('labels.backend.cmspages.table.title') }}</th>
                                <th>{{ trans('labels.backend.cmspages.table.created') }}</th>
                                <th>{{ trans('labels.backend.cmspages.table.last_updated') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ( count($records) > 0 )
                            @foreach ($records as $record)
                            <tr>
                                <td><input type="checkbox" class="deleteRow" value="{{$record->id}}"></td>
                                <td>{!! $record->title !!}</td>
                                <td class="visible-lg">{!! $record->created_at->diffForHumans() !!}</td>  
                                <td class="visible-lg">{!! $record->updated_at->diffForHumans() !!}</td>                                
                                <td>{!! $record->action_buttons !!}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="5" align="center">{{ trans('labels.backend.no_record_found') }}</td>
                            </tr>
                            @endif
                        </tbody>   
                        <tfoot>                
                            <tr>
                                <td></td>
                                <td><label>{!! Form::text('title', null, ['class' => 'form-control input-sm','id' => 'title', 'placeholder' => trans('labels.backend.cmspages.placeHolders.title')]) !!}</label></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>                 
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="pull-left">
            {!! $records->total() !!} {{ trans_choice('labels.backend.cmspages.table.total', $records->total()) }}
        </div>

        <div class="pull-right">
            {!! $records->render() !!}
        </div>
        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->