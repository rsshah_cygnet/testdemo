<div class="clear"></div>

<div class="form-group">
    {!! Form::label('title', trans('validation.attributes.backend.cmspages.title'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.cmspages.title'),'required' =>' required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('description', trans('validation.attributes.backend.cmspages.description'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::textarea('description', null, ['class' => 'form-control tinymce', 'placeholder' => trans('validation.attributes.backend.cmspages.description')]) !!}
    </div>
</div>

<!-- 
<div class="form-group">
    {!! Form::label('cannonical_link', trans('validation.attributes.backend.cmspages.cannonical_link'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('cannonical_link', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.cmspages.cannonical_link')]) !!}
    </div>
</div> -->

<div class="form-group">
    {!! Form::label('seo_title', trans('validation.attributes.backend.cmspages.seo_title'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('seo_title', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.cmspages.seo_title')]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('seo_keyword', trans('validation.attributes.backend.cmspages.seo_keyword'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('seo_keyword', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.cmspages.seo_keyword')]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('seo_description', trans('validation.attributes.backend.cmspages.seo_description'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::textarea('seo_description', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.cmspages.seo_description')]) !!}
    </div>
</div>


<div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
        <div class="col-lg-1">
        <div class="checkbox icheck">
            <label>{{ Form::checkbox('record_status', 1, (isset($cmspages) && $cmspages->is_active == 'Active') ? true : false ) }}</label>
        </div>
    </div>
</div>


<div class="box-footer">
    <a href="{{route('admin.cmspages.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a> @if(isset($cmspages))
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" /> @else
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" /> @endif
</div>
<!-- /.box-body -->