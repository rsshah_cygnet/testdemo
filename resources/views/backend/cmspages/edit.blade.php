@extends ('backend.layouts.master')

@section ('title', 'Edit CMS Pages')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
    <h1>CMS Pages Management<small> EDIT</small></h1>
</div>
</div>
</div>

@endsection

@section('content')
    <div class="box box-danger">
    <div class="box-body">
    {!! Form::model($cmspages, ['route' => ['admin.cmspages.update', $cmspages->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) !!}
            @include('backend.cmspages.form')
    {!! Form::close() !!}
@endsection
@section('after-scripts-end')
<script type="text/javascript">
	$(document).ready(function()
	{
		Imperial.CmsPagesModel.init();
        Imperial.tinyMCE.init();
	});
</script>
@stop