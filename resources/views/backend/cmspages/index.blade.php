@extends ('backend.layouts.master')

@section ('title', 'CMS Pages Management')

@section('page-header')
<div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <h1> CMS Pages Management <small> LISTING</small></h1>
        </div>
        <div class="col-xs-12 col-lg-6">
            <div class="box-tools">
                @include('backend.cmspages.includes.header-buttons')
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="box box-danger">
        <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
</div><!--box box-success-->
@stop

@section('after-scripts-end')
<script>
    var moduleConfig = '';
    moduleConfig = {
        gridURL: "{!! route('admin.cmspages.data', ['page'=>'1']) !!}",
        deleteSelectedItems: "{!! route('admin.cmspages.deleteAll') !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };
    jQuery(function () {
        var stringSearchOptions = getSearchOption('string');
        var Required_Key_Value = {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'columnFilterToolbar': true,
            'shrinkToFit': true,
            'gridLoadUrl': "{!! route('admin.cmspages.data', ['page'=>'1']) !!}",
            'displayColumn': [
                "{{ trans('labels.backend.cmspages.table.title') }}",
                "{{ trans('labels.backend.cmspages.table.created') }}",
                "{{ trans('labels.backend.cmspages.table.last_updated') }}",
                "{{ trans('labels.general.actions') }}"
            ],
            'dbColumn': [
                {title: false, name: "title", searchoptions: stringSearchOptions},
                {sortable: false, search: false, title: false, name: "created_at"},
                {search: false, sortable: false, title: false, name: "updated_at"},
                 {title: false, name: 'act', index: 'act', sortable: false, search: false,width:40},

            ],
            'defaultSortColumnName': "title",
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop