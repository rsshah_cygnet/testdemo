<div class="btn-group">
    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">			           
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">

        @permission('view-cms-pages')
        <li><a href="{{ route('admin.cmspages.index') }}">List</a></li>
        @endauth

        @permission('create-cms-pages')
        <li><a href="{{ route('admin.cmspages.create') }}">Add New</a></li>
        @endauth
    </ul>
</div><!--btn group-->			     
<div class="clearfix"></div>