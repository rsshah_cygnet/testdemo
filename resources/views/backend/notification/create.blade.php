@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.notification.create'))

@section('page-header')
<h1>
    {{ trans('labels.backend.notification.create') }}
</h1>
@endsection

@section('content')
{!! Form::open(['route' => 'admin.notification.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-notification']) !!}
@include('backend.notification.form')
{!! Form::close() !!}
@stop
