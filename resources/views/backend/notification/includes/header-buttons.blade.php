<div class="btn-group">
    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">			           
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        @permission('view-notification')
        <li><a href="{{ route('admin.notification.index') }}">{{ trans('menus.backend.notification.all') }}</a></li>
        @endauth
    </ul>
</div><!--btn group-->			     
