<div class="box-tools pull-right">
    @include('backend.notification.includes.header-buttons')
</div>
</div><!-- /.box-header -->   


<div class="form-group">
    {!! Form::label('message', trans('validation.attributes.backend.notification.message'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('message', null, ['required' => 'required','class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.notification.message')]) !!}
    </div>
</div>  

<div class="form-group">
    {!! Form::label('created_at', trans('validation.attributes.backend.notification.created_at'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('created_at', null, ['required' => 'required','class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.notification.created_at')]) !!}
    </div>
</div><!--form-group-->

<div class="form-group">
    {!! Form::label('read', trans('validation.attributes.backend.notification.read'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        <div class="col-lg-6">
            {!! Form::radio('read', '1', true) !!} Active
        </div>
        <div class="col-lg-6">
            {!! Form::radio('read', '0', false) !!} Inactive
        </div>
    </div>
</div><!--form-group-->

<div class="text-center">
    <a href="{!! route('admin.notification.index') !!}" class="btn btn-danger btn-xs">{{ trans('buttons.general.cancel') }}</a>
    @if(isset($notification))            
    <input type="submit" class="btn btn-success btn-xs" value="{{ trans('buttons.general.crud.update') }}" />
    @else
    <input type="submit" class="btn btn-success btn-xs" value="{{ trans('buttons.general.crud.create') }}" />            
    @endif
</div><!-- /.box-body -->


@section('after-scripts-end')

@stop
