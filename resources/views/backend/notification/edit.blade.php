@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.notification.edit'))

@section('page-header')
<h1>
    {{ trans('labels.backend.notification.edit') }}
</h1>
@endsection

@section('content')
{!! Form::model($notification, ['route' => ['admin.notification.update' , $notification->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH' ]) !!}    
@include('backend.notification.form')
{!! Form::close() !!}
@stop
