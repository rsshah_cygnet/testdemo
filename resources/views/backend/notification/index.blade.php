@extends ('backend.layouts.master')
@section ('title', trans('labels.backend.notification.management'))
@section('page-header')
<h1>
    {{ trans('labels.backend.notification.management') }}
</h1>
@endsection
@section('content')

<div class="clear"></div>

<table id="list"></table>
<div id="pager"></div>


@stop
@section('after-scripts-end')
<script type="text/javascript">

    var moduleConfig = '';
    moduleConfig = {
        gridURL: "{!! route('admin.notification.data', ['page'=>'1']) !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };



    jQuery(function () {
        var stringSearchOptions = getSearchOption('string');
        var StatusStr = ":All;1:Yes;0:No";
        var selectStatusSearchOptions = getSearchOption('select', StatusStr);
        var Required_Key_Value = {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'columnFilterToolbar': true,
            'shrinkToFit': true,
            'gridLoadUrl': "{!! route('admin.notification.data', ['page'=>'1']) !!}",
            'displayColumn': [
                "{{ trans('labels.backend.notification.table.message') }}",
                 "{{ trans('labels.backend.notification.table.type') }}",
                "{{ trans('labels.backend.notification.table.created_at') }}",
                "{{ trans('labels.backend.notification.table.read') }}",
                "{{ trans('labels.general.actions') }}"
            ],
            'dbColumn': [

                {sortable: true, search: true, title: false, name: "message", width: '400', searchoptions: stringSearchOptions},
                  {sortable: true, search: false, title: false, name: "notification_type", width: '400',searchoptions: stringSearchOptions},
                {sortable: true, title: false, name: "created_at", width: '200', search: false, searchoptions: stringSearchOptions},
                {sortable: true, search: true, title: false, name: "is_read_admin", width: '400', stype: 'select', searchoptions: selectStatusSearchOptions},
                 {name: 'act', index: 'act', width: '100', sortable: false, search: false}
            ],
            'defaultSortColumnName': "id",
            'defaultSortOrder': "desc",
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop