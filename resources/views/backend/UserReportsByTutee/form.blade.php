<div class="clear"></div>

<div class="form-horizontal">

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'name',
                    "Blog Title",[
                    'class' => 'col-md-3 control-label required',
                    ]) 
        !!}

        <div class="col-md-8">
            
            {!! Form::text(
                    'name',
                     null,[
                    'class' => 'form-control',
                    'placeholder' => 'Blog Name',
                    'required' =>'',
                    'maxlength' => 100,
                    'id' => 'title'
                     ]) 
            !!}

        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'blog_category',
                    'Blog Category',[
                    'class' => 'col-md-3 control-label required'
                    ])
         !!}

        <div class="col-md-8">
            @if($formtype === "edit")
                {!! Form::select(
                        'categories',
                        $categories,
                        $selectedCategory,[
                        'placeholder' => 'Select Category',
                        'class' => 'form-control categories',
                        'style'    => 'width: 100% !important;',
                        'id' => 'cat',
                        ])
                !!}
            @else
                {!! Form::select(
                        'categories',
                        $categories,
                        null,[
                        'placeholder' => 'Select Category',
                        'class' => 'form-control categories',
                        'style'    => 'width: 100% !important;',
                        'id' => 'cat',
                        ])
                !!}
            @endif
        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'publish_datetime ',
                    'Publish Date And Time',[
                    'class' => 'col-md-3 control-label required'
                    ])
         !!}

        <div class="col-md-8">

            @if($formtype === 'edit')

                {!! Form::text(
                    'publish_datetime',
                     $item->publish_datetime,[
                     'class' => 'form-control',
                     'id' => 'datetimepicker1',
                    ]) 
                !!}

            @else

                {!! Form::text(
                        'publish_datetime',
                         null,[
                         'class' => 'form-control',
                         'required' => '',
                         'id' => 'datetimepicker1',
                        ]) 
                !!}

            @endif 

        </div>      
        
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'featured_image',
                    'Featured Image',[
                    'class' => 'col-md-3 control-label required'
                    ])
         !!}

        <div class="col-md-8">

            @if($formtype === 'edit')

                <img src="{{ url(config('constants.UPLOADFILE')).'/blog_images/'.$item->featured_image }}" height = 200px width = 400px>

                <br/>

                {!! Form::file(
                        'featured_image',
                         null,[
                        'class' => 'form-control',
                        'required' => '',
                        ]) 
                !!}

            @else

                {!! Form::file(
                         'featured_image',
                         null,[
                         'class' => 'form-control',
                         'required' => '',
                        ]) 
                !!}

            @endif 

        </div>      
        
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'content',
                    'Content',[
                    'class' => 'col-md-3 control-label required'
                    ])
        !!}

        <div class="col-md-8">

            {!! Form::textarea(
                        'content',
                        null,[
                        'class' => 'form-control',
                        'placeholder' => 'Content of your blog Post',
                        ])
            !!}

        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'tags',
                    'Tags',[
                    'class' => 'col-md-3 control-label required'
                    ])
        !!}

        <div class="col-md-8">
            @if($formtype === 'edit' && sizeof($selectedTags) > 0)

                {!! Form::select(
                            'tags[]',
                            $tags,
                            $selectedTags,[
                            'class'=>'tags form-control',
                            'multiple' => 'multiple',
                            'required' =>'',
                            'style'    => 'width: 100% !important',
                            ]) 
                !!}

            @else

                {!! Form::select(
                        'tags[]',
                        $tags,
                        null,[
                        'class'=>'tags form-control',
                        'multiple' => 'multiple',
                        'required' =>'',
                        'style'    => 'width: 100% !important;',
                        ]) 
                !!}

            @endif

        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'domain',
                    'Websites to Display',[
                    'class' => 'col-md-3 control-label required'
                    ])
        !!}

        <div class="col-md-8">

            {!! Form::select(
                        'domain_id',
                        $domains,
                        null,[
                        'placeholder'=>'Select Domain',
                        'class'=>'form-control toDisplay',
                        'style'    => 'width: 100% !important;',
                        'required' => ''
                        ])
            !!}
        </div>
    </div>


    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                   'meta_title',
                   'Meta Title',[
                   'class' => 'col-md-3 control-label'
                   ])
        !!}

        <div class="col-md-8">

            {!! Form::text(
                        'meta_title',
                        null,[
                        'class' => 'form-control',
                        'placeholder' => 'Meta Title',
                        ])
            !!}

        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'slug',
                    'Slug',[
                    'class' => 'col-md-3 control-label'
                    ])
        !!}

        <div class="col-md-8">

            {!! Form::text(
                        'slug',
                        null,[
                        'class' => 'form-control',
                        'id' => 'slug',
                        'readonly' => 'readonly'
                        ])
            !!}

        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'canonical_link',
                    'Canonical Link',[
                    'class' => 'col-md-3 control-label'
                    ])
        !!}

        <div class="col-md-8">

            {!! Form::text(
                        'cannonical_link',
                        null,[
                        'class' => 'form-control',
                        'placeholder' => 'https://abc.x.co',
                        'id' => 'cannonical_link',
                        ])
            !!}

        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">

        {!! Form::label(
                    'meta_description',
                    'Meta Description',[
                    'class' => 'col-md-3 control-label'
                    ])
        !!}

        <div class="col-md-8">

            {!! Form::textarea(
                        'meta_description',
                        null,[
                        'class' => 'form-control',
                        'placeholder' => 'Meta Description'
                        ])
            !!}

        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        
        {!! Form::label(
                    'meta_keywords',
                    'Meta Keywords',[
                    'class' => 'col-md-3 control-label'
                    ])
        !!}

        <div class="col-md-8">

            {!! Form::text(
                        'meta_keywords',
                        null,[
                        'class' => 'form-control',
                        'placeholder' => 'Meta Keywords'
                        ])
            !!}

        </div>

    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        
        {!! Form::label(
                    'status',
                    'Status',[
                    'class' => 'col-md-3 control-label required'
                    ])
        !!}

        <div class="col-md-8">
        
            @if($formtype === "edit")

                {!! Form::select(
                            'record_status',
                            $status,
                            $selectedStatus,[
                            'placeholder' => 'Select Status',
                            'class' => 'form-control status',
                            'style'    => 'width: 100% !important;',
                            ])
                !!}

            @else

                {!! Form::select(
                        'record_status',
                        $status,
                        null,[
                        'placeholder' => 'Select Status',
                        'class' => 'form-control status',
                        'style'    => 'width: 100% !important;',
                        ])
                !!}

            @endif

        </div>

    </div>

</div>

<div class="clear"></div>

@section('after-scripts-end')

<script type="text/javascript">


        

    $(document).ready(function()
    {

        Imperial.Blog.init();

        var cat,url="";

        var categories =<?php echo json_encode($categories);?>;

        // alert(JSON.stringify(categories));
        var blogUrl;

        $('#cat').on('change',function(){
            cat = $(this).val();
            category = categories[cat].toLowerCase().replace(/[^\w ]+/g,'-').replace(/ +/g,'-');
            url = $("#title").val().toLowerCase().replace(/[^\w ]+/g,'-').replace(/ +/g,'-');
            blogUrl = '/'+category+'/';
            $("#slug").val(blogUrl+url);

        });

        if(cat == undefined)
        {
            cat = $('#cat').val();
            category = categories[cat].toLowerCase().replace(/[^\w ]+/g,'-').replace(/ +/g,'-');
            blogUrl = '/'+category+'/';  
        }

        // alert(cat);
        // alert(blogUrl);
        $("#title").keyup(function()
        {
            url = $(this).val().toLowerCase().replace(/[^\w ]+/g,'-').replace(/ +/g,'-');
            $("#slug").val(blogUrl+url);

        });

    });
    
</script>
@stop