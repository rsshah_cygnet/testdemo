@extends('backend.layouts.master')

@section('page-header')
<div class="page-tatil-box clearfix">
  <div class="row">
   <div class="col-xs-12">
    <h1>
      <!--!! app_name() !!}-->
      Test demo
      <small>{{ trans('strings.backend.dashboard.title')}}</small>
    </h1></div>
  </div>
</div>


@endsection

@section('content')

<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title">{{ trans('strings.backend.dashboard.welcome') . " " . $loggedInUserName->name }}</h3>
            <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>--><!-- /.box tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                  <div class="box box-warning dashboard-section-box">
                    <div class="box-header with-border">
                      <h3 class="box-title">User Details </h3>
                    </div>
                    <div class="box-body">
                      <ul class="list-unstyled">
                        <li><b>Active Admin Users:</b> {{$ActiveAdminUsers}}</li>

                        <li><b>InActive Admin Users:</b> {{$InactiveAdminUsres}} </li>

                        <li><b>Active Tutors:</b> {{$ActiveTutor}} </li>

                        <li><b>Not Active Tutors:</b> {{$InactiveTutor}} </li>

                        <li><b>Active Tutees:</b> {{$ActiveTutees}} </li>

                        <li><b>Not Active Tutees:</b> {{$InactiveTutess}} </li>

                        <li><b>User Not Applied for test:</b> {{$tutor_not_appied_test}} </li>

                        <li><b>User Who Quit Test:</b> {{$TutorIncompaleteTest}} </li>

                        {{-- <li><b>User Got Failed In Selected Subject</b>: {{$TutorFailInTest}} </li> --}}

                        <li><b>User Applied for Multiple Tests but Got Failed In One of The Subject:</b> {{$TutorFailedInOneOfSubject}} </li>

                        <li><b>User Applied for One / Multiple Tests and Passed All Tests:</b> {{$TutorPassedAllTest}} </li>

                      </ul>
                    </div>
                  </div>

                </div>




                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                  <div class="box box-warning dashboard-section-box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Upcoming Session:</h3>
                    </div>
                    <div class="box-body">
                      <ul class="list-unstyled">
                        @foreach($upcomingSession as $session)
                        <li><b>Tutee Name:</b>  {{$session->tutee_name}}</li>
                        <li><b>Tutor Name:</b>  {{$session->tutor_name}}</li>
                        <li><b>Scheduled Date:</b>  {{$session->scheduled_date_time}}</li>
                        <li><b>Curriculum: </b> <?php
if (isset($session->topic_id) && $session->topic_id != 0 && $session->topic_id != "") {
	echo getCurriculumHierarchy('', $session->topic_id, true); //for topic
} else {
	echo getCurriculumHierarchy($session->subject_id, '', true); //for subject
}
?></li>
                        {{-- <li><b>Subject Name:</b>  {{$session->subject_name}}</li>
                        <li><b>Topic Name:</b>  {{$session->topic_name}}</li> --}}
                        <br>
                        @endforeach
                      </ul>
                    </div>
                  </div>

                </div>


                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">

                  <div class="box box-warning dashboard-section-box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Top 5 Tutors</h3>
                    </div>
                    <div class="box-body">
                      <ul class="list-unstyled">
                        @foreach($tutorWithHighestPunctualityRatings as $tutor)
                        <li><b>Tutor Name:</b> {{$tutor->tutor_name}}</li>
                        <li><b>Education Status: </b> {{$tutor->qualification_name}}</li>
                        <li><b>Rating by System:</b> {{round($tutor->system_rating,1)}}</li>
                        <li><b>Rating by Tutee: </b> {{round($tutor->tutee_rating,1)}}</li><br>
                        @endforeach
                      </ul>
                    </div>
                  </div>

                </div>
              </div>


              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                  <div class="box box-warning dashboard-section-box2">
                    <div class="box-header with-border">
                      <h3 class="box-title">Session Cancelled by Tutor:</h3>
                    </div>

                    <div class="box-body">
                      <ul class="list-unstyled">
                        @foreach($sessionCancelByTutor as $session)
                        <li><b>Tutee Name:</b>  {{$session->tutee_name}}</li>
                        <li><b>Tutor Name:</b>  {{$session->tutor_name}}</li>
                        <li><b>Scheduled Date: </b>  {{$session->scheduled_date_time}}</li>
                        <li><b>Cancelled Date: </b>  {{$session->cancelled_date_time}}</li>
                        <li><b>Curriculum: </b> <?php
if (isset($session->topic_id) && $session->topic_id != 0 && $session->topic_id != "") {
	echo getCurriculumHierarchy('', $session->topic_id, true); //for topic
} else {
	echo getCurriculumHierarchy($session->subject_id, '', true); //for subject
}
?></li>

                        {{-- <li><b>Subject Name: </b>  {{$session->subject_name}}</li>
                        <li><b>Topic Name: </b>  {{$session->topic_name}}</li> --}}
                        <br>
                        @endforeach
                      </ul>
                    </div>

                  </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                 <div class="box box-warning dashboard-section-box2">
                  <div class="box-header with-border">
                    <h3 class="box-title">Session Cancelled by Tutee:</h3>
                  </div>
                  <div class="box-body">
                    <ul class="list-unstyled">
                      @foreach($sessionCancelByTutee as $session)
                      <li><b>Tutee Name:</b>  {{$session->tutee_name}}</li>
                      <li><b>Tutor Name: </b>  {{$session->tutor_name}}</li>
                      <li><b>Scheduled Date: </b>  {{$session->scheduled_date_time}}</li>
                      <li><b>Cancelled Date: </b>  {{$session->cancelled_date_time}}</li>
                      <li><b>Curriculum: </b> <?php
if (isset($session->topic_id) && $session->topic_id != 0 && $session->topic_id != "") {
	echo getCurriculumHierarchy('', $session->topic_id, true); //for topic
} else {
	echo getCurriculumHierarchy($session->subject_id, '', true); //for subject
}
?></li>
                      {{-- <li><b>Subject Name: </b>  {{$session->subject_name}}</li>
                      <li><b>Topic Name: </b>  {{$session->topic_name}}</li> --}}
                      <br>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="box box-warning dashboard-section-box2">
                  <div class="box-header with-border">
                    <h3 class="box-title">Successful Session:</h3>
                  </div>
                  <div class="box-body">
                    <ul class="list-unstyled">
                      @foreach($successfulSession as $session)
                      <li><b>Tutee Name:</b>  {{$session->tutee_name}}</li>
                      <li><b>Tutor Name:</b>  {{$session->tutor_name}}</li>
                      <li><b>Scheduled Date:</b>  {{$session->scheduled_date_time}}</li>
                      <li><b>Curriculum: </b> <?php
if (isset($session->topic_id) && $session->topic_id != 0 && $session->topic_id != "") {
	echo getCurriculumHierarchy('', $session->topic_id, true); //for topic
} else {
	echo getCurriculumHierarchy($session->subject_id, '', true); //for subject
}
?></li>
                      {{-- <li><b>Subject Name:</b>  {{$session->subject_name}}</li>
                      <li><b>Topic Name: </b> {{$session->topic_name}}</li> --}}
                      <br>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12">
               <div class="box-header with-border">
                      <h3 class="box-title">Session Overview:</h3>
                    </div>
                <div class="box box-warning">
                 <div class="box-body">
                   <div class="row">
                     <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                       <div class="form-group">
                        <label class="control-label" for="month">Month</label>
                        <select name="month" id="month" class="form-control">
                          <option value="">Select</option>
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>
                        </select>
                        <span id="month_error" style="color: red;display: none;">Please select month</span>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="form-group">
                       <label class="control-label" for="year">Year</label>
                       <select name="year" id="year" class="form-control">
                        <option value="">Select</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                      </select>
                      <span id="year_error" style="color: red;display: none;">Please select year</span>
                    </div>
                  </div>
                </div>



                <input class="btn btn-info" style="width: 115px;" onclick="filterPiechart()" value="Search" type="button">
                <a href="{{route('admin.dashboard')}}" style="width: 115px;" class="btn btn-red">Reset</a>
              </div>
            </div>
          </div>
        </div>





      {{-- getLanguageBlock('backend.lang.welcome') --}}



      <br><br>
      <div id="pie_chart_portion">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
           <div class="box box-warning dashboard-section-box">
            <div class="box-body">
              <div id="piechart_3d" style="width: 100%; height: 300px;"></div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="box box-warning dashboard-section-box">
            <div class="box-body">
              <h3 class="box-title"><b>Successful Session Data($)</b></h3>
              <ul class="list-unstyled">
                <li><strong>Amount Paid by Tutee:</strong> {{round($sessionsData['amount_paid_by_tutee'],1)}}</li>
                <li><strong>Admin Commission:</strong> {{round($sessionsData['amount_received_by_admin'],1)}}</li>
                <li><strong>Amount Paid to Tutor:</strong> {{round($sessionsData['amount_paid_to_tutor'],1)}}</li>
              </ul>
              <br>

              <h3 class="box-title"><b>Sessions Cancelled by Tutor Data($)</b></h3>
              <ul class="list-unstyled">
                <li><strong>Amount Refund to Tutee:</strong> {{round($sessionsData['tutee_refund_amount'],1)}}</li>
              </ul>
              <br>

              <h3 class="box-title"><b>Sessions Cancelled by Tutee Data($)</b></h3>
              <ul class="list-unstyled">
                <li><strong>Penalty Amount Paid by Tutee:</strong> {{round($sessionsData['tutee_penalty_amount'],1)}}</li>
                <li><strong>Amount Paid to Tutor:</strong> {{round($sessionsData['tutor_earning_for_cancelled_by_tutee'],1)}}</li>
                <li><strong>Amount is Kept with Admin:</strong> {{round($sessionsData['amount_kept_with_admin_for_cancelled_by_tutee'],1)}}</li>
              </ul>
              <br>
            </div>
          </div>
        </div>
      </div>

        @section('after-scripts-end')
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load("current", {packages:["corechart"]});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {

           var record={!! json_encode($user) !!};
           console.log(record);
                   // Create our data table.
                   var data = new google.visualization.DataTable();
                   data.addColumn('string', 'Session Type');
                   data.addColumn('number', 'Total');
                   for(var k in record){
                    var v = record[k];

                    data.addRow([k,v]);
                         //console.log(v);
                       }
                       var options = {
                        title: 'Session overview',
                        is3D: false,
                      };
                      var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
                      chart.draw(data, options);
                    }
                  </script>
                  @stop
                </div>
              </div><!-- /.box-body -->
            </div><!--box box-success-->
            @endsection


