<?php
//print_r($curriculum);die;
?>

<div class="clear"></div>

<div class="form-horizontal">

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('type',"Type",['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            {!! Form::select('type', $type, isset($punctualityrating->type)?$punctualityrating->type:'', array('class' => 'form-control select2', 'required', 'style' => 'width: 100%', 'id' => 'type')) !!}
        </div>
    </div>

    <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('From',"From",['class' => 'col-md-3 control-label required', 'id' => 'fromlabel']) !!}
        <div class="col-md-8">
            {!! Form::text('from', isset($punctualityrating->from)?$punctualityrating->from:'', ['class' => 'form-control','placeholder' => 'From', 'id' => 'from','maxlength' => 5]) !!}
        </div>
    </div>

     <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('To',"To",['class' => 'col-md-3 control-label required','id' => 'tolabel']) !!}
        <div class="col-md-8">
            {!! Form::text('to', isset($punctualityrating->to)?$punctualityrating->to:'', ['class' => 'form-control','placeholder' => 'To', 'id' => 'to','maxlength' => 5]) !!}
        </div>
    </div>

  <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('within',"Within",['class' => 'col-md-3 control-label required','id' => 'withinlabel']) !!}
        <div class="col-md-8">
            {!! Form::text('within', isset($punctualityrating->within)?$punctualityrating->within:'', ['class' => 'form-control','placeholder' => 'Within', 'id' => 'within','maxlength' => 5]) !!}
        </div>
    </div>

     <div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
        {!! Form::label('rating',"Rating",['class' => 'col-md-3 control-label required','id' => 'ratinglabel']) !!}
        <div class="col-md-8">
            {!! Form::text('rating', isset($punctualityrating->rating)?$punctualityrating->rating:'', ['class' => 'form-control','placeholder' => 'Rating', 'id' => 'rating']) !!}
        </div>
    </div>

@if(!empty($punctualityrating))
<div class="form-group clearfix col-xs-12 col-sm-12 col-lg-12">
    {!! Form::label('Status',"Active",['class' => 'col-md-3 control-label required',]) !!}
    <div class="col-lg-1">
        <div class="checkbox icheck">
          {{ Form::checkbox('status', 1, (isset($punctualityrating) && $punctualityrating->status == '1') ? true : false )}}
      </div>
  </div>
</div>
@endif
</div>
@if(!empty($punctualityrating))
{{Form::hidden('id', $punctualityrating->id)}}
@endif
<div class="clear"></div>

@section('after-scripts-end')

<script>
$(document).ready(function(){

  $('#type').change(function(){
    if($(this).val() == 1)
    {
        $('#from').show();
        $('#fromlabel').show();
        $('#to').show();
        $('#tolabel').show();
        $('#rating').show();
        $('#ratinglabel').show();
         $('#within').hide();
      $('#withinlabel').hide();

    }
    else if($(this).val() == 2)
    {
      $('#within').show();
      $('#withinlabel').show();
      $('#rating').show();
      $('#ratinglabel').show();
      $('#from').hide();
       $('#fromlabel').hide();
      $('#to').hide();
       $('#tolabel').hide();
    }
    else{
      $('#from').hide();
      $('#fromlabel').hide();
      $('#to').hide();
      $('#tolabel').hide();
      $('#within').hide();
      $('#withinlabel').hide();
      $('#rating').hide();
      $('#ratinglabel').hide();
    }
  });
  $('#from').hide();
      $('#fromlabel').hide();
      $('#to').hide();
      $('#tolabel').hide();
      $('#within').hide();
      $('#withinlabel').hide();
      $('#rating').hide();
      $('#ratinglabel').hide();

      if($('#type option:selected').val() == 1)
    {
        $('#from').show();
        $('#fromlabel').show();
        $('#to').show();
        $('#tolabel').show();
        $('#rating').show();
        $('#ratinglabel').show();
        $('#within').hide();
        $('#withinlabel').hide();

    }
    else if($('#type option:selected').val() == 2)
    {
      $('#within').show();
      $('#withinlabel').show();
      $('#rating').show();
      $('#ratinglabel').show();
      $('#from').hide();
       $('#fromlabel').hide();
      $('#to').hide();
       $('#tolabel').hide();
    }
});
</script>
@stop