 @extends('backend.layouts.master')

@section('title', 'Punctuality Rating Management for Cancel Session')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Punctuality Rating Management for Cancel Session<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.PunctualityRatingCancelSession.includes.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
     <div class="box box-danger">
        <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.punctualityratings-cancelsession.data', ['page'=>'1']) !!}",
         labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader),
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;0:InActive;1:Active",
            StatusStrRole               = ":both;1:Tutor;2:Tutee",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
            selectStatusSearchOptionsRole = getSearchOption('select', StatusStrRole);
            dateSearchOptions           = getSearchOption('date');

            displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value =
        {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [

                {title: false, name: 'From',index: 'punctuality_ratings_for_cancelled_session.from',searchoptions:stringSearchOptions,
                formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == false)
                        {
                            return "<label class='label label-danger'>N/A</label>"
                        }
                        else{
                            return cellvalue;
                        }
                    }
                },
                {title: false, name: 'To',index: 'punctuality_ratings_for_cancelled_session.to',searchoptions:stringSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == false)
                        {
                            return "<label class='label label-danger'>N/A</label>"
                        }
                        else{
                            return cellvalue;
                        }
                    }
                },
                {title: false, name: 'Within',index: 'punctuality_ratings_for_cancelled_session.within',searchoptions:stringSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == false)
                        {
                            return "<label class='label label-danger'>N/A</label>"
                        }
                        else{
                            return cellvalue;
                        }
                    }
                },
                {title: false, name: 'Type',index: 'punctuality_ratings_for_cancelled_session.type',search:false,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == 1)
                        {
                            return "Between";
                        }
                        else if(cellvalue == 2){
                            return "Within";
                        }
                    }
                },
                {title: false, name: 'Rating',index: 'punctuality_ratings_for_cancelled_session.rating',searchoptions:stringSearchOptions},
                 {title: false, name: 'Status',  index: 'punctuality_ratings_for_cancelled_session.status',  stype: 'select',searchoptions: selectStatusSearchOptions,
                    formatter: function (cellvalue, options, rowObject)
                    {
                        if (cellvalue == 0)
                        {
                            return "<label class='label label-danger'>InActive</label>"
                        }
                        else if (cellvalue == 1)
                        {
                             return "<label class='label label-success'>Active</label>"
                        }
                    }
                },
                 {title: false, name: 'act',index: 'act', sortable: false, search: false}

            ],
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop