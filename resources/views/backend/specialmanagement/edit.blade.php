@extends ('backend.layouts.master')

@section ('title', 'Special Management')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
    <h1>Special Management<small> EDIT</small></h1>
</div>    
</div>
</div>
@endsection

@section('content')
	<div class="box box-danger">
    <div class="box-body">
	{!!Form::model($item, ['route' => ['admin.special-management.update', $item->id],'class' => 'form-horizontal',
		'role' 		=> 'form',
		'method' 	=> 'PATCH',
		'files' 	=>  true,
		'id' 		=> 'edit-special']) !!}

		{{-- Campaign Form --}}
		@include('backend.specialmanagement.form')

		<div class="box-footer">
			<a href="{{route('admin.special-management.index')}}" class="btn btn-red">Cancel</a>
			{!! Form::submit('Save', array("class"=>"btn btn-info pull-right")) !!}
		</div>

	{!! Form::close() !!}
@endsection