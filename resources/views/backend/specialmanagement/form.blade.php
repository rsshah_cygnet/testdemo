<div class="clear"></div>

<div class="form-horizontal">
    @if($role != 'dealer')
        <!-- if user is dealer then content inside if condition will not shown to user --> 
        <div class="form-group">
            
            {!! Form::label('special type', "Special Type", ['class' => 'col-lg-3 control-label required']) !!}
            
            <div class="col-lg-9">
                {!! Form::select('special_type_id',$type, 
                isset($item->special_type_id) ? $item->special_type_id : '', array('class' => 'form-control selecttype', 'required', 'placeholder'=>'Select Special Type', 'style' => 'width: 100%')) !!}
            </div>
        </div>

        <div class="form-group">
            
            {!! Form::label('vehicle type', "Vehicle Type", ['class' => 'col-lg-3 control-label required']) !!}
            
            <div class="col-lg-9">
                {!! Form::select('vehicle_type',['U' => 'Used Cars', 'N' => 'New cars'], 
                isset($item->vehicle_type) ? $item->vehicle_type : '', array('class' => 'form-control selecttype', 'required', 'placeholder'=>'Select Vehicle Type', 'style' => 'width: 100%')) !!}
            </div>
        </div>
        
        <div class="form-group">
        {!! Form::label('mandatory', "Mandatory", ['class' => 'col-lg-3 control-label mandatory']) !!}
            <div class="col-lg-1">
            <div class="checkbox icheck">
            <label>{{ Form::checkbox('is_mandatory', 1, (isset($item) && $item->is_mandatory == '1') ? true : false ), ['id'=>'remember', 'class' => 'className'] }}  </label>
            </div>
            </div>
        </div>
    @endif
    <div class="form-group">
        {!! Form::label('title', 'Title', ['class' => 'col-lg-3 control-label required']) !!}
        <div class="col-lg-9">
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title' , 'required']) !!}
        </div>
    </div><!--form-group-->
    

    <div class="form-group">
        {!! Form::label('header', 'Header', ['class' => 'col-lg-3 control-label required']) !!}
        <div class="col-lg-9">
            {!! Form::text('header', null, ['class' => 'form-control', 'placeholder' => 'Header', 'required']) !!}
        </div>
    </div><!--form-group-->

    <div class="form-group">
        {!! Form::label('description', 'Description', ['class' => 'col-lg-3 control-label']) !!}
        <div class="col-lg-9">            
            {!! Form::textarea('description', null , ['class' => 'form-control tinymce', 'id' => 'txtBody' , 'placeholder' => 'Description']) !!}
        </div>
    </div> <!--form-group-->

    <div class="form-group">
        <div class="col-lg-6">
            {!! Form::label('start_end', 'Start Date', ['class' => 'col-lg-3 control-label required']) !!}
            <div class="col-lg-9">
                {!! Form::text('start_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'Start Date', 'required']) !!}
            </div>
        </div>
        <div class="col-lg-6">
            {!! Form::label('end_date', 'End Date', ['class' => 'col-lg-3 control-label required']) !!}
            <div class="col-lg-9">
            {!! Form::text('end_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'End Date', 'required']) !!}
        </div>
        </div>
    </div><!--form-group-->

    <div class="form-group">
        
        {!! Form::label('model_year', "Model Year", ['class' => 'col-lg-3 control-label required']) !!}
        
        <div class="col-lg-9">
            {!! Form::select('model_year',$year, 
            isset($item->model_year) ? $item->model_year : '', array('class' => 'form-control selectyear selectdropdown', 'id'=>'model-year', 'placeholder'=>'select Model Year', 'style' => 'width: 100%', 'required')) !!}
        </div>
    </div>

    <div class="form-group">
        
        {!! Form::label('brand_id', "Brand", ['class' => 'col-lg-3 control-label required']) !!}
        
        <div class="col-lg-9">
            {!! Form::select('brand_id',$brands, 
            isset($item->brand_id) ? $item->brand_id : '', array('class' => 'form-control selectbrand selectdropdown','id'=>'brand', 'placeholder'=>'select brands', 'style' => 'width: 100%', 'required')) !!}
        </div>
    </div>

    <div class="form-group">
        
        {!! Form::label('brand_model_name', "Brand Model", ['class' => 'col-lg-3 control-label required']) !!}
        
        <div class="col-lg-9">
            {!! Form::select('brand_model_id', $model, 
            isset($item->brand_model_id) ? $item->brand_model_id : '', array('class' => 'form-control selectmodel selectdropdown', 'id'=>'brand-model','placeholder'=>'Select Brand Model', 'style' => 'width: 100%', 'required')) !!}
        </div>
    </div>

    <div class="form-group">
        
        {!! Form::label('derivative', "Derivative", ['class' => 'col-lg-3 control-label required']) !!}
        
        <div class="col-lg-9">
            {!! Form::select('derivative_id', $derivative, 
            isset($item->derivative_id) ? $item->derivative_id : '', array('class' => 'form-control selectderivative selectdropdown', 'id'=>'derivative','placeholder'=>'Select Derivative', 'style' => 'width: 100%', 'required')) !!}
        </div>
    </div>

    @if($role != 'dealer')
        <!-- if user is dealer then content inside if condition will not shown to user --> 
        <div class="form-group jqgrid hidden">
            <div class="col-lg-3">
                
            </div>
            
            <div class="col-lg-9">
                <table id="list"></table>
                <div id="pager"></div>
            </div>
        </div>

        <div class="form-group">
            
            {!! Form::label('brand_logo_status', "Banner", ['class' => 'col-lg-3 control-label']) !!}
            
            <div class="col-lg-9">
                {{ Form::file('banner_image', ['class' => 'form-control']) }}
                
                @if(isset($item) && isset($item->banner_image))
                    {{ Html::image('special_management/'.$item->banner_image, $item->title, ['width' => 150, 'height' => '150']) }}
                @endif
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('banner_description', "Banner Description", ['class' => 'col-lg-3 control-label']) !!}
            <div class="col-lg-9">
                {!! Form::textarea('banner_description', null, ['class' => 'form-control', 'placeholder' => "Banner Description"]) !!}
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label('price', 'Price', ['class' => 'col-lg-3 control-label required']) !!}
            <div class="col-lg-9">
                {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price', 'required']) !!}
            </div>
        </div><!--form-group-->

        <div class="form-group">
                    
            {!! Form::label('domain_id', "Website", ['class' => 'col-lg-3 control-label required']) !!}
            
            <div class="col-lg-9">
                {!! Form::select('domain_id', $domain, 
                isset($selecteddomain) ? $selecteddomain : '', array('class' => 'form-control selectdomain','placeholder'=>'Select Website', 'style' => 'width: 100%', 'required')) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('meta_title', "Meta Title", ['class' => 'col-lg-3 control-label required']) !!}
            <div class="col-lg-9">
                {!! Form::text('meta_title', null, ['class' => 'form-control', 'placeholder' => "Meta Title" , 'required']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('meta_keyword', "Meta Keyword", ['class' => 'col-lg-3 control-label required']) !!}
            <div class="col-lg-9">
                {!! Form::text('meta_keywords', null, ['class' => 'form-control', 'placeholder' => "Meta Keyword", 'required']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('meta_description', "Meta Description", ['class' => 'col-lg-3 control-label']) !!}
            <div class="col-lg-9">
                {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'placeholder' => "Meta Description"]) !!}
            </div>
        </div>        


        <div class="form-group">
        <label class="col-lg-3 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
        <div class="col-lg-1">
        <div class="checkbox icheck">
            <label>{{ Form::checkbox('record_status', 1, (isset($item) && $item->status == 'Active') ? true : false ) }}</label>
        </div>
        </div>
        </div>

        @if(isset($item))
             <div class="col-lg-10">
                {{ Form::hidden('id', $item->id ,['id' => 'hidden'])}}
            </div>
        @endif
    @endif
</div>
<div class="clear"></div>


@section('after-scripts-end')
<script type="text/javascript">
    
    $(function () {
        Imperial.tinyMCE.init();
        Imperial.SpecialManagement.init();  
    });
    
    //check if cheackbox is checked or not
    $(window).on('load', function() {
        var checkbox = 0;
        
        if($('input[name="is_mandatory"]').prop('checked')){
            checkbox = 1;
        }
        
        handleCheckbox(checkbox);
    });
    
    //when checkbox value gets changed
    $('input[name="is_mandatory"]').on('ifChanged', function(){
        var checkbox = 0;
        
        if(this.checked) {
            checkbox = 1;
        }
        
        handleCheckbox(checkbox);
    });

    //if checkbox is checked then show grid otherwise hide it
    function handleCheckbox(checkbox){
        
        if(checkbox == 1)
        {
            $('.jqgrid').removeClass("hidden");
            generateGridData();
            filterGridRecord();
        }
        else
        {
            $('.jqgrid').addClass("hidden");
        }
    }

    //change event of brand dropdown
    $(".selectbrand").change(function() {
        var value = $('select.selectbrand option:selected').val();
        getModel(value);
        filterGridRecord();
    });

    //change event of brand model dropdown
    $(".selectmodel").change(function() {
        var brand = $('select.selectbrand option:selected').val();
        var model = $('select.selectmodel option:selected').val();
        getDerivative(brand, model);
        filterGridRecord()   
    });

    //change event of model year dropdown
    $(".selectyear").change(function() {
        filterGridRecord();
    });

    //change event of derivative dropdown
    $(".selectderivative").change(function() {
        filterGridRecord();
    });

    function getModel(value)
    {
        $.ajax({
            type:"POST",
            url : 'modelname',
            data : {'_token':"{{csrf_token()}}",value:value},
            async: false,
            success : function(data) {
                // Parse the returned json data
                var options = $.parseJSON(data);
                
                $('.selectmodel').empty();
                $('.selectmodel').append("<option value=''>Select Model Name</option>");
                
                $.each(options, function(key, element) {
                    $('.selectmodel').append("<option value='" + key +"'>" + element + "</option>");
                });                
            },
            error: function() {
                alert('Error occured');
            }
        });
    }

    function getDerivative(a, b)
    {
        $.ajax({
            type:"POST",
            url : 'derivative',
            data : {'_token':"{{csrf_token()}}",'brand':a,'model':b},
            async: false,
            success : function(data) {
                // Parse the returned json data
                var options = $.parseJSON(data);
                
                $('.selectderivative').empty();
                $('.selectderivative').append("<option value=''>Select Model Name</option>");
                
                $.each(options, function(key, element) {
                    $('.selectderivative').append("<option value='" + key +"'>" + element + "</option>");
                });                
            },
            error: function() {
                alert('Error occured');
            }
        });
    }


    function generateGridData()
    {
        var moduleConfig = '';
    
        moduleConfig = {
            gridURL: "{!! route('admin.vehical-stock.data', ['page'=>'1']) !!}",
        };

        var displayColumn               = '{!! json_encode($stockRepository->gridColumn) !!}',
            displayColumnHeader         = '{!! json_encode($stockRepository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader), 
            defaultOrderBy              = '{!! $stockRepository->gridDefaultOrderBy!!}',
            displayColumnArray          = ['Select'],
            displayColumns              = eval(displayColumn),
            StatusStr                   = ":All;Active:Active;InActive:InActive",
            stringSearchOptions         = getSearchOption('string'),
            integerSearchOptions         = getSearchOption('integer');
        
        jQuery.each(displayColumns, function(index, element)
        {
            displayColumnArray.push(displayColumnHeaderArray[index]);
        });

        var _Key_Value = 
        {
            'shrinkToFit':              true,
            'editable':                 true,
            'multiselect':              true,
            'datatype':                 "json",
            'loadonce':                 true,
            'viewrecords':              true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [
                {title:false,name:'Stock Number',index: 'vehicle_stocks.id',searchoptions:stringSearchOptions},
                {title:false,name:'Dealer Code',index:'dealerships.dealership_code',searchoptions:stringSearchOptions},
                {title:false,name:'Year',index:'vehicle_stocks.model_year',searchoptions:integerSearchOptions},
                {title:false,name:'Brand',index:'brands.brand_name',searchoptions:stringSearchOptions},
                {title: false, name: 'Model',index: 'brand_models.brand_model_name',searchoptions:stringSearchOptions},
                {title:false,name:'act', index: 'act', sortable: false, search: false, width:55,edittype: 'checkbox', editable: true},
            ]
        };

        CustomGrid(_Key_Value);
        
        filterGridRecord();

        var id = $("input[name='id']").val();
        //get selected data cheacked
        if(id != undefined){
            getCheckboxValue(id);
        }        
    }

    function filterGridRecord()
    {
        var year = $('#model-year option:selected').val(),
            brand = $('#brand option:selected').val(),
            model = $('#brand-model option:selected').val(),
            derivative = $('#derivative option:selected').val();
        
        //filter object
        //filter with AND operation
        f = {groupOp:"AND",rules:[]};
        
        //if year is selected then filetr with year
        if(year){
            f.rules.push({field:"vehicle_stocks.model_year",op:"eq",data:year});
        }
        
        //if brand is selected then filetr with brand
        if(brand){
            f.rules.push({field:"brands.id",op:"eq",data:brand});
        }
        
        //if model is selected then filetr with model
        if(model){
            f.rules.push({field:"brand_models.id",op:"eq",data:model});
        }
        
        //if derivative is selected then filetr with derivative
        if(derivative){
            f.rules.push({field:"derivatives.id",op:"eq",data:derivative});
        }
        
        //set search is true in parameters
        $("#list")[0].p.search = true;
        
        //convert data into string
        $.extend($("#list")[0].p.postData,{filters:JSON.stringify(f)});
        
        //reload grid
        $("#list").trigger("reloadGrid",[{page:1,current:true}]);


        var id = $("input[name='id']").val();
        //get selected data cheacked
        if(id != undefined){
            getCheckboxValue(id);
        }
    
    }

    function getSelectedRowData(id)
    {
        var searchIDs = $("input[name='stock[]']:checked").map(function(){
          return $(this).val();
        }).get();        
    }

    function getCheckboxValue(id)
    {
        $.ajax({
            type:"POST",
            url : 'checkbox',
            data : {'_token':"{{csrf_token()}}",'id':id},
            async: false,
            success : function(data) {
                var options = $.parseJSON(data);

                $.each(options, function(key, element) {
                    $("input[id='"+ element +"']").prop('checked', true);
                });   
            },
            error: function() {
                alert('Error occured');
            }
        });
    }
    
</script>
@stop
