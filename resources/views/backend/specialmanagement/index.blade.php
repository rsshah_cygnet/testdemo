@extends('backend.layouts.master')

@section('title', 'Special Managment')

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Special Managment<small> Listing</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.specialmanagement.includes.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
<table id="list"></table>
<div id="pager"></div>
</div><!-- /.box-body -->
</div><!--box box-success-->    
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';
    
    moduleConfig = {
        gridURL: "{!! route('admin.special-management.data', ['page'=>'1']) !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
            displayColumnHeaderArray    = eval(displayColumnHeader), 
            defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
            displayColumnArray          = 'Action',
            StatusStr                   = ":All;Active:Active;InActive:InActive",
            stringSearchOptions         = getSearchOption('string'),
            selectStatusSearchOptions   = getSearchOption('select', StatusStr);
            dateSearchOptions           = getSearchOption('date');

            displayColumnHeaderArray.push(displayColumnArray);

        

        var Required_Key_Value = 
        {   'columnFilterToolbar':      true,
            'shrinkToFit':              true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [
                {title: false, name: 'Title',  index: 'specials.title',             searchoptions:stringSearchOptions},
                {title: false, name: 'Start Date',  index: 'specials.start_date', searchoptions:dateSearchOptions},
                {title: false, name: 'Finish Date',index: 'specials.end_date',                searchoptions: dateSearchOptions
                },
                {title: false, name: 'Type',  index: 'special_types.special_type',searchoptions:stringSearchOptions},
                {title: false, name: 'Status',index: 'specials.status',stype:'select',searchoptions:selectStatusSearchOptions},
                {title: false, name: 'act', index: 'act', sortable: false, search: false, width:70}
            ],
        };
        CustomGrid(Required_Key_Value);
    });

    $(".selectbrand").change(function() {
        var value = $('select.selectbrand option:selected').val();
        getModelName(value);
    });
    
    function getModelName(value)
    {
        console.log(value);
        $.ajax({
            type:"POST",
            url : 'derivative/modelname',
            data : {'_token':"{{csrf_token()}}",value:value},
            async: false,
            success : function(data) {
                // Parse the returned json data
                var options = $.parseJSON(data);
                $('.selectmodel').empty();
                $('.selectmodel').append("<option value=''>Select Model Name</option>");
                
                $.each(options, function(key, element) {
                    $('.selectmodel').append("<option value='" + key +"'>" + element + "</option>");
                });                
            },
            error: function() {
                alert('Error occured');
            }
        });
    }
</script>
@stop