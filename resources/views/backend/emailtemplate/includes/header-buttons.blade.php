<div class="btn-group">
    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">			           
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        @permission('view-emailtemplate')
        <li><a href="{{ route('admin.emailtemplate.index') }}">{{ trans('menus.backend.emailtemplate.all') }}</a></li>
        @endauth

        @permission('create-emailtemplate')
        <li><a href="{{ route('admin.emailtemplate.create') }}">{{ trans('menus.backend.emailtemplate.create') }}</a></li>
        @endauth

    </ul>
</div><!--btn group-->			     
