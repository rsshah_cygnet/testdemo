<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.emailtemplate.create') }}</h3>

        <div class="box-tools pull-right">
            @include('backend.emailtemplate.includes.header-buttons')
        </div>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="dataTables_wrapper form-inline dt-bootstrap">                   
            <div class="row">
                <div class="col-sm-9">
                    <div class="dataTables_length">
                        {!! Form::label('type', trans('labels.backend.page_size')) !!}
                        {!!  Form::select('pagesize', array('10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30' ), null, ['id'=>'pagesize']) !!}  
                        <button id="trigerDelete" class="btn bg-purple btn-flat">{{ trans('labels.backend.delete') }}</button>
                        @if ( count($templates) > 0 ) 
                        <a class="btn bg-purple btn-flat" href="{{ route('admin.emailtemplate.export') }}">{{ trans('labels.backend.export_excel') }}</a>
                        <a class="btn bg-purple btn-flat" href="{{ route('admin.emailtemplate.csv') }}">{{ trans('labels.backend.export_csv') }}</a>
                        @endif
                    </div>
                </div>
                <div class="col-sm-3">
                    <div id="grid-table_filter" class="dataTables_filter"><label>Search: {!! Form::text('search-top', null, ['class' => 'form-control input-sm','id' => 'search-top', 'placeholder' => trans('labels.backend.search')]) !!}</label></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <!-- hidden fields for sorting -->
                    {!! Form::hidden('orderby', 'id', ['class' => 'form-control input-sm','id' => 'orderby']) !!}
                    {!! Form::hidden('sort', 'asc', ['class' => 'form-control input-sm','id' => 'sort']) !!}
                    <!-- <table class="table table-striped table-bordered table-hover"> -->
                    <table id="grid-table" class="table table-striped table-hover table-bordered dataTable" role="grid" aria-describedby="grid-table_info">
                        <thead>
                            <tr>
                                <th><input type="checkbox"  id="bulkDelete"  />  </th>
                                <th class="sorting" data-orderby="title" data-sort="asc" >{{ trans('labels.backend.emailtemplate.table.title') }}</th>
                                <!-- <th class="sorting sorting_asc sorting_desc" aria-sort="sorting">{{ trans('labels.backend.emailtemplate.table.title') }}</th> -->
                                <th class="sorting" data-orderby="subject" data-sort="asc">{{ trans('labels.backend.emailtemplate.table.subject') }}</th>
                                <th class="visible-lg">{{ trans('labels.backend.emailtemplate.table.last_updated') }}</th>                        
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ( count($templates) > 0 )
                            @foreach ($templates as $template)
                            <tr>
                                <td><input type="checkbox" class="deleteRow" value="{{$template->id}}"></td>
                                <td>{!! $template->title !!}</td>
                                <td>{!! $template->subject !!}</td>                                        
                                <td class="visible-lg">{!! $template->created_at->diffForHumans() !!}</td>                                
                                <td>{!! $template->action_buttons !!}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4" align="center">{{ trans('labels.backend.no_record_found') }}</td>
                            </tr>
                            @endif
                        </tbody> 
                        <tfoot>
                            @if ( count($templates) > 0 )
                            <tr>
                                <td></td>
                                <td><label>{!! Form::text('title', null, ['class' => 'form-control input-sm','id' => 'title', 'placeholder' => trans('labels.backend.emailtemplate.placeHolders.title')]) !!}</label></td>
                                <td><label>{!! Form::text('subject', null, ['class' => 'form-control input-sm','id' => 'subject', 'placeholder' => trans('labels.backend.emailtemplate.placeHolders.subject')]) !!}</label></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endif
                        </tfoot>                   
                    </table>
                </div>
            </div>
        </div>

        <div class="pull-left">
            {!! $templates->total() !!} {{ trans_choice('labels.backend.emailtemplate.table.total', $templates->total()) }}
        </div>

        <div class="pull-right">
            {!! $templates->render() !!}
        </div>

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->