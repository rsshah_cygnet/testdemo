@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.emailtemplate.edit'))

@section('page-header')
<h1>
    Email Template Management <small>EDIT</small>
</h1>
@endsection

@section('content')
{!! Form::model($templates, ['route' => ['admin.emailtemplate.update' , $templates->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH' ]) !!}
@include('backend.emailtemplate.form')
{!! Form::close() !!}
@stop
