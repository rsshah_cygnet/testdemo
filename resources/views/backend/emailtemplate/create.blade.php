@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.emailtemplate.create'))

@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
<h1>Email Template Management<small> CREATE</small></h1>
</div>
</div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
{!! Form::open(['route' => 'admin.emailtemplate.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-emailtemplate']) !!}
@include('backend.emailtemplate.form')
{!! Form::close() !!}
@stop
