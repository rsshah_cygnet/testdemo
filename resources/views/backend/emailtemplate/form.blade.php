@section('modal')
<div class="model-container">
    <div class="modal fade model-wrapper" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                    
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"> Email Template </h4>
                </div>
                
                <div id="model-body" class="modal-body">
                
                </div>

            </div>
        </div>
    </div>        
</div>
@stop

<div class="clear"></div>

<div class="form-group">
    {!! Form::label('title', trans('validation.attributes.backend.emailtemplate.title'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.emailtemplate.title'),'required' =>' required']) !!}
    </div>
</div><!--form-group-->

<div class="form-group">
    {!! Form::label('template_name','Template Name', ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('template_name', null, ['class' => 'form-control', 'placeholder' => 'Template name','required' =>' required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('type', trans('validation.attributes.backend.emailtemplate.type'),['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::select('type', array_merge(['0' => 'Select Type'], $types) ,isset($templates->type_id)?$templates->type_id:"", ['class' => 'form-control select2' , 'id' => 'templatetype']); !!}
    </div>
</div><!--form-group-->

<div class="form-group">
    {!! Form::label('placeholder', trans('validation.attributes.backend.emailtemplate.placeholder'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-8">            
        {!! Form::select('placeholders', array_merge(['' => 'Select PlaceHolder'], $placeHolders) , 'all', ['class' => 'form-control select2' , 'id' => 'placeHolder']); !!}

    </div>
    <div class="col-lg-2">               
        <input type="button" id="addPlaceHolder" class="btn btn-info" value="{{ trans('buttons.general.crud.add') }}" />
    </div>

</div> <!--form-group-->

<div class="form-group">
    {!! Form::label('subject', trans('validation.attributes.backend.emailtemplate.subject'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('subject', null, ['class' => 'form-control', 'id' => 'txtSubject' , 'placeholder' => trans('validation.attributes.backend.emailtemplate.subject'),'required' =>' required']) !!}
    </div>
</div><!--form-group-->

<div class="form-group">
    {!! Form::label('body', trans('validation.attributes.backend.emailtemplate.body'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">            
        {!! Form::textarea('body', null , ['class' => 'form-control tinymce', 'id' => 'txtBody' , 'placeholder' => trans('validation.attributes.backend.emailtemplate.body')]) !!}
    </div>
</div> <!--form-group-->



<div class="box-footer">
    <a href="{!! route('admin.emailtemplate.index') !!}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a>
    <input type="button" id="showPreview" data-toggle="modal" data-target="#templatePreview" class="btn btn-info" value="{{ trans('buttons.general.preview') }}" />
    @if(isset($templates))            
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" />
    @else
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" />            
    @endif
</div>
@section('after-scripts-end')

<script type="text/javascript">

    $(function () {
        Imperial.emailTemplate.init();
    });

</script>
@stop
