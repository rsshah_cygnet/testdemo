@extends ('backend.layouts.master')
@section ('title', 'Email Template Management')
@section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
    <div class="col-xs-12 col-lg-6"><h1>Email Template Management<small> LISTING</small></h1></div>
    <div class="col-xs-12 col-lg-6">
<div class="box-tools">
    @include('backend.emailtemplate.includes.header-buttons')
</div>
</div>
</div>
</div>
@endsection

@section('content')
<div class="box box-danger">
<div class="box-body">
    <table id="list"></table>
    <div id="pager"></div>
</div><!-- /.box-body -->
</div><!--box box-success-->
@stop

@section('after-scripts-end')
<script type="text/javascript">

    var moduleConfig = '';
    moduleConfig = {
        gridURL: "{!! route('admin.emailtemplate.data', ['page'=>'1']) !!}",
        deleteSelectedItems: "{!! route('admin.emailtemplate.deleteAll') !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };



    jQuery(function () {
        var stringSearchOptions = getSearchOption('string');
        var Required_Key_Value = {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'columnFilterToolbar': true,
            'shrinkToFit': true,
            'gridLoadUrl': "{!! route('admin.emailtemplate.data', ['page'=>'1']) !!}",
            'displayColumn': [
                "{{ trans('labels.backend.emailtemplate.table.title') }}",
                "{{ trans('labels.backend.emailtemplate.table.subject') }}",
                "{{ trans('labels.backend.emailtemplate.table.last_updated') }}",
                "{{ trans('labels.general.actions') }}",
            ],
            'dbColumn': [
                {sortable: true, search: true, title: false, name: "title", searchoptions: stringSearchOptions},
                {sortable: true, search: true, title: false, name: "subject", searchoptions: stringSearchOptions},
                {sortable: false, title: false, name: "noofuser", search: false},
                {title: false, name: 'act', index: 'act', sortable: false, search: false},
            ],
            'defaultSortColumnName': "id",
            'defaultSortOrder': "desc",
        };

        CustomGrid(Required_Key_Value);
    });

</script>
@stop