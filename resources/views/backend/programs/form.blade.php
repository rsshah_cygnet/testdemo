<div class="clear"></div>
<div id="custom_dropdowns">
<div class="form-group">
    {!! Form::label('program_name', trans('validation.attributes.backend.programs.name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('program_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.programs.name'),'required' =>' required']) !!}
    </div>
</div>

<!-- curriculum -->
<div class="form-group">
    {!! Form::label('curriculum_id', trans('validation.attributes.backend.programs.curriculum'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('curriculum_id', ['' => 'Select curriculum'] + $curriculums ,isset($programs->curriculum_id)?$programs->curriculum_id:"", ['class' => 'form-control select2' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required' =>' required']) !!}
    </div>
</div><!--form-group--><!-- curriculum -->
</div>

<!-- level end-->
@if(!empty($programs))
{{-- <div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
        <div class="col-lg-1">
        <div class="checkbox icheck">
            <label>{{ Form::checkbox('record_status', 1, (isset($programs->status) && $programs->status == '1') ? true : false ) }}</label>
        </div>
    </div>
</div> --}}
<input type="hidden" value="{{$programs->curriculum_id or ''}}" name="curriculum_id">
@endif
<input type="hidden" value="{{$programs->id or ''}}" name="programs_id">
<input type="hidden" value="programs" name="type" id="management_type">

<div class="box-footer">
    <a href="{{route('admin.programs.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a> @if(isset($programs))
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" /> @else
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" /> @endif
</div>
<!-- /.box-body -->