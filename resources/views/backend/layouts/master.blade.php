<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    <title>Test demo</title>
    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', 'Default Description')">


    <link rel="shortcut icon" type="image/x-icon" href="{{url("/")}}/images/favicon.ico" />
    <link rel="icon" type="image/x-icon" href="{{url("/")}}/images/favicon.ico"/>
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        @yield('before-styles-end')
        {!! Html::style('css/backend/backend.css') !!}
        <!-- {!! Html::style('css/developer.css') !!} -->
        @yield('after-styles-end')
    </head>

    <body class="skin-purple sidebar-mini">
        <div class="wrapper">
            @include('backend.includes.header')
            @include('backend.includes.sidebar')

            @yield('modal')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
               <div class="ajax-loader">
                  <img src="{{ url('images/loader2.svg')}}" class="img-responsive" />
              </div>
              <!-- Content Header (Page header) -->
              <section class="content-header">
               {{-- Change to Breadcrumbs::render() if you want it to error to remind you to create the breadcrumbs for the given route --}}
               {!! Breadcrumbs::renderIfExists() !!}
           </section>
           <!-- Main content -->
           <section class="content">
            @include('includes.partials.messages')
            @yield('page-header')
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('backend.includes.footer')
    <div class="control-sidebar-bg">

    </div>
</div>

<!-- ./wrapper -->
@yield('before-scripts-end')
{!! HTML::script('js/backend.js') !!}
{!! Html::script(('js/backend/jquerysession.js')) !!}
<!-- REQUIRED JS SCRIPTS -->
{{-- {!! Html::script('js/plugin/tinymce/tinymce.min.js') !!} --}}
{!! Html::script('js/backend/admin.js') !!}
{!! Html::script('js/backend/notification.js') !!} {{-- Need to check if it is useful or not --}}
{!! Html::script('js/backend/global.js') !!} {{-- Need to check if it is useful or not --}}




<script type="text/javascript">
    var baseUrl = {!! json_encode(url('/')) !!};
</script>
@yield('after-scripts-end')
</body>

</html>