<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}" /> @if(!empty($settings))
        {{-- <title>{!! $settings->seo_title !!}</title> --}}
<title> Testdemo | Testdemo </title>
        <!-- Meta -->
        <meta name="keywords" content="{!! $settings->seo_keyword !!}">
        <meta name="description" content="{!! $settings->seo_description !!}"> @endif
        <meta name="author" content="@yield('meta_author', 'Anthony Rappa')"> @yield('meta')

        <link rel="shortcut icon" type="image/x-icon" href="{{url("/")}}/images/favicon.ico" />
        <!-- Styles -->
        @yield('before-styles-end')
         {!! Html::style('css/backend.css') !!}
        @yield('after-styles-end')

        <!-- Font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500" rel="stylesheet">
    </head>

    <body id="app-layout" class="frontend-page">
        <div class="login-logo">
            <a href="{{ url('/admin/login') }}">
                <img src="{{url("/")}}/images/logo.png" class="login_header_logo">
            </a>
        </div>
        <div class="login-box">
            @include('includes.partials.messages')
            @yield('content')
        </div>
        <!-- container -->

        <!-- JavaScripts -->

        @yield('before-scripts-end')
        {!! Html::script('js/backend.js') !!}
        @yield('after-scripts-end')
        @include('includes.partials.ga')

    </body>
</html>