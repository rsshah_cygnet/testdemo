@extends('backend.layouts.auth-master') @section('content')

<div class="login-box-body">
    {!! Form::open(['url' => 'admin/login']) !!}
    <div class="form-group has-feedback">
   	 	<label for="exampleInputEmail1">E-mail</label>
        @if(isset($AdminloginData[0]))
         {!! Form::input('email', 'email', $AdminloginData[0], ['class' => 'form-control', 'autocomplete' => 'off', 'maxlength' => 100]) !!}
        @else
        {!! Form::input('email', 'email', null, ['class' => 'form-control', 'autocomplete' => 'off', 'maxlength' => 100]) !!}
        @endif
    </div>
    <div class="form-group has-feedback">
    	<label for="exampleInputEmail1">Password</label>
        @if(isset($AdminloginData[1]))
        {!! Form::input('password', 'password', $AdminloginData[1], ['class' => 'form-control', 'maxlength' => 100]) !!}
        @else
        {!! Form::input('password', 'password', null, ['class' => 'form-control', 'maxlength' => 100]) !!}
        @endif
    </div>
	<div class="form-group has-feedback">
        <div class="row">
        <div class="col-xs-12 col-sm-6">
        		<div class="checkbox icheck">
                    <label>
                        {{-- <input type="checkbox" name="remember"> --}}
                        {!! Form::checkbox('remember', (1 or true), null) !!}
                        Remember me
                    </label>
                </div>
           </div>
    	<div class="col-xs-12 col-sm-6"><div class="link-password">{!! link_to('admin/password/reset', trans('labels.frontend.passwords.forgot_password')) !!}</div>
    </div>
    </div>
    </div>
    <div class="form-group form-group-last clearfix">
        <div class="text-center">
            {!! Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-block login-btn']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection