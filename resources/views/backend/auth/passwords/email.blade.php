@extends('backend.layouts.auth-master') @section('content')
<div class="login-box-body">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif {!! Form::open(['url' => 'admin/password/email']) !!}

    <div class="form-group has-feedback">
    	<label for="exampleInputEmail1">E-mail</label>
        {!! Form::input('email', 'email', null, ['class' => 'form-control']) !!}
    </div>
    <!--form-group-->

    <div class="form-group form-group-last text-center">
        {!! Form::submit(trans('labels.frontend.passwords.send_password_reset_link_button'), ['class' => 'btn btn-block login-btn']) !!}	

    </div>
    <!--form-group-->

    {!! Form::close() !!}


</div>





@endsection