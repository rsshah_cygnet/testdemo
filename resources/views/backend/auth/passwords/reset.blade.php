@extends('backend.layouts.auth-master') @section('content')


<div class="login-box-body">

    {!! Form::open(['url' => 'admin/password/reset', 'class' => '']) !!}

    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group has-feedback">
		<label for="exampleInputEmail1">E-mail</label>
        {!! Form::input('email', 'email', null, ['class' => 'form-control']) !!}      
        <!--col-md-6-->
    </div>
    <!--form-group-->

    <div class="form-group has-feedback">
		<label for="exampleInputEmail1">Password</label>
        {!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
        <!--col-md-6-->
    </div>
    <!--form-group-->

    <div class="form-group has-feedback ">
		<label for="exampleInputEmail1">Password Confirmation</label>
        {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}
        <!--col-md-6-->
    </div>
    <!--form-group-->

    <div class="form-group form-group-last text-center">

        {!! Form::submit(trans('labels.frontend.passwords.reset_password_button'), ['class' => 'btn btn-block login-btn']) !!}

        <!--col-md-6-->
    </div>
    <!--form-group-->

    {!! Form::close() !!}


    <!-- panel body -->




</div>


<!-- row -->
@endsection