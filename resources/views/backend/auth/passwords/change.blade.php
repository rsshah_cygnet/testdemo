@extends ('backend.layouts.master') @section ('title', trans('labels.frontend.user.passwords.change')) @section('page-header')
<div class="page-tatil-box clearfix">
<div class="row">
<div class="col-xs-12">
<h1>{{ trans('labels.frontend.user.passwords.change') }}<small> EDIT</small></h1> 
</div>    
</div>
</div>
@endsection 

@section('content')
<div class="box box-danger">
<div class="box-body">
<div class="form-horizontal">
    {!! Form::open(['route' => ['admin.auth.password.update']]) !!}
    <div class="form-group clearfix">
        {!! Form::label('old_password', trans('validation.attributes.frontend.old_password'), ['class' => 'col-lg-2 control-label required']) !!}
        <div class="col-lg-10">
            {!! Form::input('password', 'old_password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.old_password')]) !!}
        </div>
    </div>

    <div class="form-group clearfix">
        {!! Form::label('password', trans('validation.attributes.frontend.new_password'), ['class' => 'col-lg-2 control-label required']) !!}
        <div class="col-lg-10">
            {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.new_password')]) !!}
        </div>
    </div>

    <div class="form-group clearfix">
        {!! Form::label('password_confirmation', trans('validation.attributes.frontend.new_password_confirmation'), ['class' => 'col-lg-2 control-label required']) !!}
        <div class="col-lg-10">
            {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.new_password_confirmation')]) !!}

        </div>
    </div>

    <div class="form-group clearfix">
        <div class="col-lg-2 control-label"></div>
        <div class="box-footer">
            <a href="{{route('backend.user.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a> {!! Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn btn-info pull-right']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    <!--panel body-->
</div>

<!-- row -->
@endsection