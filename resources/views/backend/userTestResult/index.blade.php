 @extends('backend.layouts.master')

 @section('title', "User's test")

 @section('page-header')
 <div class="page-tatil-box clearfix">
    <div class="row">
        <div class="col-xs-12 col-lg-6"><h1>User&#39;s Test <small> LISTING</small></h1></div>
        <div class="col-xs-12 col-lg-6">

        </div>
    </div>
</div>
@endsection

@section('content')
<div class="box box-danger">
    <div class="box-body">
        <table id="list"></table>
        <div id="pager"></div>
    </div><!-- /.box-body -->
</div><!--box box-success-->
@stop

@section('after-scripts-end')

<script>
    var moduleConfig = '';

    moduleConfig = {
        gridURL: "{!! route('admin.frontusers.testResultData', ['page'=>'1','id'=>$id]) !!}",
        labelRecordsDeleted: "{!! trans('labels.backend.records_deleted') !!}",
        labelRecordsActivated: "{!! trans('labels.backend.records_activated') !!}",
        labelAuthUnknown: "{!! trans('auth.unknown') !!}",
    };

    jQuery(function ()
    {
        var displayColumnHeader         = '{!! json_encode($repository->gridColumnHeader) !!}',
        displayColumnHeaderArray    = eval(displayColumnHeader),
        defaultOrderBy              = '{!! $repository->gridDefaultOrderBy!!}',
        displayColumnArray          = 'Action',
        StatusStr                   = ":All;0:InActive;1:Active",
        ResultStr                   = ":All;0:Fail;1:Pass",
        StatusStrRole               = ":both;1:Tutor;2:Tutee",
        stringSearchOptions         = getSearchOption('string'),
        selectStatusSearchOptions   = getSearchOption('select', StatusStr);
        selectResultSearchOptions   = getSearchOption('select', ResultStr);
        selectStatusSearchOptionsRole = getSearchOption('select', StatusStrRole);
        dateSearchOptions           = getSearchOption('date');

        displayColumnHeaderArray.push(displayColumnArray);

        var Required_Key_Value =
        {
            'refreshFooter': false,
            'searchFooter' : false,
            'columnChooser' : false,
            'downloadDropdown' : [false,false,false,false],
            'shrinkToFit':              true,
            'columnFilterToolbar':      true,
            'gridLoadUrl':              moduleConfig.gridURL,
            'displayColumn':            displayColumnHeaderArray,
            'defaultSortColumnName':    defaultOrderBy,
            'dbColumn': [


            {title: false, name: 'Test Date-Time',index: 'tutor_details.test_date_time',search:false,width:300},
            {title: false, name: 'First Name',index: 'front_user.first_name',searchoptions:stringSearchOptions},
            {title: false, name: 'Last Name',index: 'front_user.last_name',searchoptions:stringSearchOptions},
            {title: false, name: 'Level',index: 'grade.grade_type',searchoptions:stringSearchOptions,
            formatter: function (cellvalue, options, rowObject)
            {
                       // var arr = Object.values(rowObject);

                       if (cellvalue == 0)
                       {
                        return "Lower";
                    }
                    else
                    {
                      return "Higher";
                  }
              }
          },

            {title: false, name: 'Subject',index: 'subject.subject_name',searchoptions:stringSearchOptions},
            {title: false, name: 'Topic',index: 'topic.topic_name',searchoptions:stringSearchOptions,
             formatter: function (cellvalue, options, rowObject)
            {
                    var arr = Object.values(rowObject);
                    var grade_type = "";
                    if(typeof  arr['3'] != 'undefined'){
                         grade_type = arr['3'];
                    }

                    if (cellvalue == false)
                    {
                      return "<label class='label label-success'>N/A</label>";
                    }
                    else
                    {
                      if(grade_type == 1){
                        return cellvalue;
                      }else{
                         return "<label class='label label-success'>N/A</label>";
                      }

                  }
              }
          },
            {title: false, name: 'No. of total que',index: 'tutor_details.no_of_total_questions',searchoptions:stringSearchOptions},
            {title: false, name: 'No. of correct answer',index: 'tutor_details.no_of_correct_answers',searchoptions:stringSearchOptions},

            {title: false, name: 'No. of minutes',index: 'tutor_details.no_of_minutes',searchoptions:stringSearchOptions},
            {title: false, name: 'result Status',index: 'tutor_details.passed_status',stype: 'select',searchoptions:selectResultSearchOptions,
            formatter: function (cellvalue, options, rowObject)
            {
                       // var arr = Object.values(rowObject);

                       if (cellvalue == 0)
                       {
                        return "<label class='label label-danger'>Fail</label>";
                    }
                    else
                    {
                      return "<label class='label label-success'>Pass</label>";
                  }
              }
          },

          {title: false, name: 'No. of percentage',index: 'tutor_details.no_of_percentage',searchoptions:stringSearchOptions,
          formatter: function (cellvalue, options, rowObject)
          {
             return cellvalue+"%";
         }


     },

     {title: false, name: 'act',index: 'act', sortable: false, search: false}

     ],
 };

 CustomGrid(Required_Key_Value);
});

</script>
@stop