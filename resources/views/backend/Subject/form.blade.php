<div class="clear"></div>
<div id="custom_dropdowns">
<div class="form-group">
    {!! Form::label('subject_name', trans('validation.attributes.backend.subject.name'), ['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::text('subject_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.subject.name'),'required' =>' required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('curriculum_id', trans('validation.attributes.backend.subject.curriculum'),['class' => 'col-lg-2 control-label required']) !!}
    <div class="col-lg-10">
        {!! Form::select('curriculum_id', ['' => 'Select curriculum'] + $curriculums ,isset($subject->curriculum_id)?$subject->curriculum_id:"", ['class' => 'form-control select2' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required' =>' required']) !!}
    </div>
</div><!--form-group-->

</div>
<div class="form-group number_of_questions_per_topic">
    {!! Form::label('number_of_questions_per_topic','Number of questions per topic(Lower Grade)', ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('number_of_questions_per_topic',(isset($subject->number_of_questions_per_topic) && $subject->number_of_questions_per_topic != 0 ) ? $subject->number_of_questions_per_topic : "", ['class' => 'form-control', 'placeholder' => 'Number of questions per topic','maxlength' => '3', 'id' => 'number_of_questions_per_topic']) !!}
    </div>
</div>

<div class="form-group number_of_max_questions_per_topic" style="display: none">
    {!! Form::label('number_of_max_questions_per_topic','Number of Maximum questions per topic(Higher Grade)', ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('number_of_max_questions_per_topic',(isset($subject->number_of_max_questions_per_topic) && $subject->number_of_max_questions_per_topic != 0 ) ? $subject->number_of_max_questions_per_topic : "", ['class' => 'form-control', 'placeholder' => 'Number of questions per topic','maxlength' => '3', 'id' => 'number_of_max_questions_per_topic']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('number_of_minutes_per_questions','Number of minutes per questions', ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('number_of_minutes_per_questions', (isset($subject->number_of_minutes_per_questions) && $subject->number_of_minutes_per_questions != 0 ) ? $subject->number_of_minutes_per_questions : "", ['class' => 'form-control', 'placeholder' => 'Number of minutes per questions','maxlength' => '3', 'id' => 'number_of_minutes_per_questions']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('passing_percentage','Passing percentage', ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::text('passing_percentage', (isset($subject->passing_percentage) && $subject->passing_percentage != 0 ) ? $subject->passing_percentage : "", ['class' => 'form-control', 'placeholder' => 'Passing percentage','maxlength' => '5', 'id' => 'passing_percentage']) !!}
    </div>
</div>

@if(isset($formtype) && $formtype == "edit")
{{-- <div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
        <div class="col-lg-1">
        <div class="checkbox icheck">
            <label>{{ Form::checkbox('record_status', 1, (isset($subject->status) && $subject->status == '1') ? true : false ) }}</label>
        </div>
    </div>
</div> --}}
@endif
<input type="hidden" value="{{$subject->id or ''}}" name="subject_id">
<input type="hidden" value="subject" name="type" id="management_type">
@if(isset($formtype) && $formtype == "edit")
<input type="hidden" value="{{$subject->curriculum_id or ''}}" name="curriculum_id">
@endif
<div class="box-footer">
    <a href="{{route('admin.subject.index')}}" class="btn btn-red">{{ trans('buttons.general.cancel') }}</a> @if(isset($subject))
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.update') }}" /> @else
    <input type="submit" class="btn btn-info pull-right" value="{{ trans('buttons.general.crud.create') }}" /> @endif
</div>
<!-- /.box-body -->