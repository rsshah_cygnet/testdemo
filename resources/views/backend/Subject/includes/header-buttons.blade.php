<div class="btn-group">
    
    <button type="button" class="btn btn-grey-border btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {{-- {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>  --}}
        Actions <span class="caret"></span>
        
    </button>

    <ul class="dropdown-menu" role="menu">

        @permission('view-subject-management')

        <li>
            <a href="{{ route('admin.subject.index') }}">
                {{ trans('menus.backend.subject.all') }}
            </a>
        </li>
        
        @endauth

        @permission('create-subject')
        
        <li>
            <a href="{{ route('admin.subject.create') }}">
                {{ trans('menus.backend.subject.create') }}
            </a>
        </li>
        
        @endauth
    </ul>
</div><!--btn group-->
<div class="clearfix"></div>