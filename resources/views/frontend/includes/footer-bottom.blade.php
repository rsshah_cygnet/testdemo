<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="footer-menu">
                    <li><a href="javascript:void(0)">About Imperial Select</a></li>
                    <li><a href="javascript:void(0)">Financial Services</a></li>
                    <li><a href="javascript:void(0)">Terms and Conditions</a></li>
                    <li><a href="javascript:void(0)">Privacy Policy</a></li>
                    <li><a href="javascript:void(0)">Sitemap</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <p>Copyright &copy; 2016 Imperial Select South Africa : an IMPERIAL Group Company. All rights reserved.</p>
            </div>
        </div>
    </div>
</div>