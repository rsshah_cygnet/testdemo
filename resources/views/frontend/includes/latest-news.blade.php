<div class="container">
    <div class="section-title-primary">
        <h2>news &amp; <span>reviews</span></h2>
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="blog-date">MONDAY&nbsp;&nbsp;|&nbsp;&nbsp;DECEMBER 12, 2016</div>
            <h2 class="blog-title">Mercedes X-Class double-cab bakkie heading for South Africa</h2>
            <div class="read-more-link"><a href="javascript:void(0)">review</a></div>
            <ul class="blog-social">
                <li><a href="javascript:void(0)"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-send" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="mob-fullwidth"><img src="{{ url('/') }}/images/latest-news.png" alt="Latest News"></div>
        </div>
    </div>
</div>