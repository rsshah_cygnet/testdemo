<div class="left-aside-dashboard">
        <ul>
    <?php
$active_class_dash = $em_tag_dash = $active_class_my_sessions = $em_tag_my_sessions =
$em_tag_my_session_request = $active_class_session_request = $active_class_tutor_payment =
$em_tag_tutor_payment = $em_tag_share_experience = $active_class_share_experience =
$active_class_password_change = $em_tag_password_change = $active_class_free_session =
$em_tag_free_session = $active_class_profile = $em_tag_profile = $em_tag_tutor_mytest = $active_class_tutor_available = $em_tag_tutor_available = $active_class_share_experience = $em_tag_share_experience = $active_class_tutor_mytest = '';

if (Route::getFacadeRoot()->current()->uri() == 'tutor-dashboard') {
	$active_class_dash = 'menu-active';
	$em_tag_dash = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == 'tutor-sessions') {
	$active_class_my_sessions = 'menu-active';
	$em_tag_my_sessions = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == 'tutor-session-request') {
	$active_class_session_request = 'menu-active';
	$em_tag_my_session_request = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == 'tutor/payment' || Route::getFacadeRoot()->current()->uri() == 'tutor/payment/{tabReload}') {
	$active_class_tutor_payment = 'menu-active';
	$em_tag_tutor_payment = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == '') {
	$active_class_share_experience = 'menu-active';
	$em_tag_share_experience = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == 'change-password') {
	$active_class_password_change = 'menu-active';
	$em_tag_password_change = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == '') {
	$active_class_free_session = 'menu-active';
	$em_tag_free_session = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == 'profile' || Route::getFacadeRoot()->current()->uri()== 'profile/edit') {
	$active_class_profile = 'menu-active';
	$em_tag_profile = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == 'tutor/mytest' || Route::getFacadeRoot()->current()->uri() == 'tutor/mytest/{tabReload}') {
	$active_class_tutor_mytest = 'menu-active';
	$em_tag_tutor_mytest = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == 'tutor-availablity') {
	$active_class_tutor_available = 'menu-active';
	$em_tag_tutor_available = '<em class="new-notification"></em>';
}
if (Route::getFacadeRoot()->current()->uri() == 'share-experience') {
  $active_class_share_experience = 'menu-active';
  $em_tag_share_experience = '<em class="new-notification"></em>';
}
?>
          <li class="{{$active_class_dash}}">
          <a href="javascript:void(0);" id="tutor_dashboard_link">
            <!-- <a href="{{route('frontend.tutorDashboard')}}"> -->
              <i class="icon icon-dashboard"></i>
              <span>Dashboard</span>
              <?php echo $em_tag_dash; ?>
            </a>
          </li>
         <!--  <li id="find_tutor_li">
            <a href="{{route('frontend.findTutor')}}">
              <i class="icon icon-search"></i>
              <span>Find Tutor</span>
            </a>
          </li> -->
          <li class="{{$active_class_profile}}">
           <a href="javascript:void(0);" id="tutee_profile">
    <!-- <a href="{{route('frontend.profile')}}"> -->
              <i class="icon icon-my_profile" aria-hidden="true"></i>
              <span>My Profile</span>
               <?php echo $em_tag_profile; ?>
            </a>
          </li>
          <li class="{{$active_class_tutor_available}}">
           <a href="javascript:void(0);" id="tutor_my_availability">
           <!--  <a href="{{route('frontend.tutorAvalilablity')}}"> -->
              <i class="icon icon-my_preferences" aria-hidden="true"></i>
              <span>My Availability</span>
              <?php echo $em_tag_tutor_available; ?>
            </a>
          </li>
          <!-- <li>
            <a href="javascript:void(0);">
              <i class="icon icon-my_free_session" aria-hidden="true"></i>
              <span>My Free Sessions</span>
            </a>
          </li> -->
          <li class="{{$active_class_my_sessions}}">
          <a href="javascript:void(0);" id="tutor_my_sessions">
            <!-- <a href="{{route('frontend.tutorSessions')}}"> -->
              <i class="icon icon-my_session"></i>
              <span>My Sessions</span>
              <?php echo $em_tag_my_sessions; ?>
            </a>
          </li>
          <li class="{{$active_class_session_request}}">
          <a href="javascript:void(0);" id="tutor_session_request">
            <!-- <a href="{{route('tutor.pending.session')}}"> -->
              <i class="icon icon-session_req" aria-hidden="true"></i>
              <span>Session Request</span>
              <?php echo $em_tag_my_session_request; ?>
            </a>
          </li>
          <li class="{{$active_class_tutor_payment}}">
          <a href="javascript:void(0);" id="tutor_my_payment">
            <!-- <a href="{{route('tutor.payment.request')}}"> -->
              <i class="icon icon-payment"></i>
              <span>My Payment</span>
              <?php echo $em_tag_tutor_payment; ?>
            </a>
          </li>
          <li class="{{$active_class_tutor_mytest}}">
          <a href="javascript:void(0);" id="tutor_my_test">
            <!-- <a href="{{route('tutor.mytest')}}"> -->
              <i class="fa fa-file-text"></i>
              <span>My Test</span>
              <?php echo $em_tag_tutor_mytest; ?>
            </a>
          </li>
          <li class="{{$active_class_share_experience}}">
             <a href="javascript:void(0);" id="share_exp_tutee">
            <!-- <a href="{{route('frontuser.share.experience')}}"> -->
              <i class="icon icon-share_exp"></i>
              <span>Share Experience</span>
               <?php echo $em_tag_share_experience; ?>
            </a>
          </li>
          <li class="{{$active_class_password_change}}">
          <a href="javascript:void(0);" id="settings">
            <!-- <a href="{{route('auth.password.change')}}"> -->
              <i class="icon icon-setting" aria-hidden="true"></i>
              <span>Settings</span>
              <?php echo $em_tag_password_change; ?>
            </a>
          </li>
          <li>
            <a href="{{route('auth.logout')}}">
              <i class="icon icon-logout"></i>
              <span>Logout</span>
            </a>
          </li>
        </ul>
        <div class="dashboard-copyright">
            Copyright©  <?php echo date("Y"); ?> Test demo.
        </div>
      </div>