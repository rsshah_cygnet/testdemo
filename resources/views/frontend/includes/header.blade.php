<!-- Global Menu Start -->
<header>
    <div class="inner-container clearfix">
        <div class="row">
            <div class="col-lg-6 col-md-5 col-sm-6 hidden-sm-xs">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <a href="{{ route('frontend.index') }}"><img src="{{ url('/') }}/images/logo01.png" alt="Imperial Select"></a>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <div class="header-search">
                            <a class="icon" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                            {{--<input type="text" placeholder="search cars">--}}
                            {!! Form::text('search_text', null, ['placeholder' => 'search cars', 'id'=>'search_text']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-10 visible-sm-xs mobile-header">
                <a href="{{ route('frontend.index') }}" class="d-i-b"><img src="{{ url('/') }}/images/back-arrow.png" alt="Back"></a>
                @if(Request::route()->getName() == 'auth.register')
                    <h2 class="d-i-b">Register <span>Favourites</span></h2>
                @elseif(Request::route()->getName() == 'cars.index')
                    <h2 class="d-i-b">All <span>Cars</span></h2><a href="javascript:void(0);" class="btn btn-white btn-sm" data-toggle="modal" data-target="#refine-mobile">Refine cars</a>
                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'new')
                    <h2 class="d-i-b">New <span>Cars</span></h2><a href="javascript:void(0);" class="btn btn-white btn-sm" data-toggle="modal" data-target="#refine-mobile">Refine cars</a>
                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'used')
                    <h2 class="d-i-b">Used <span>Cars</span></h2><a href="javascript:void(0);" class="btn btn-white btn-sm" data-toggle="modal" data-target="#refine-mobile">Refine cars</a>
                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'demo')
                    <h2 class="d-i-b">Demo <span>Cars</span></h2><a href="javascript:void(0);" class="btn btn-white btn-sm" data-toggle="modal" data-target="#refine-mobile">Refine cars</a>
                @elseif(Request::route()->getName() == 'auth.login')
                    <h2 class="d-i-b">Login <span>Me</span></h2>
                @elseif(Request::route()->getName() == 'auth.password.reset')
                    <h2 class="d-i-b">Reset <span>Password</span></h2>
                @elseif(Request::route()->getName() == 'frontend.findADealer')  
                    <h2 class="d-i-b"><span>Find A</span> Dealer</h2>
                @endif
            </div>
            <div class="col-lg-6 col-md-7 header-right col-sm-6 col-xs-2">
                <ul class="header-links d-i-b hidden-sm-xs">
                    @if(Auth::check())
                    <li class="favourites">
                        <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-favourites.png" alt="Icon Favourites">favourites<span>8</span></a>
                    </li>
                    <li class="notify">
                        <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-notify.png" alt="Icon Favourites">notify<span>3</span></a>
                    </li>
                    @endif
                    <li class="compare">
                        <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-compare.png" alt="Icon Favourites">compare<span>2</span></a>
                    </li>
                    @if(Auth::guest())
                    <li><a href="{{ route('auth.register') }}" class="btn btn-white btn-sm">register</a><a href="{{ route('auth.login') }}" class="btn btn-white btn-sm">login</a></li>
                    @endif
                </ul>
                <a href="javascript:void(0);" class="toggle d-i-b" id="menu-toggle1">
                    <img src="{{ url('/') }}/images/menu-button-white.png" alt="Menu Icon White">
                </a>
                <ul class="menu header-links" data-menu data-menu-toggle="#menu-toggle1, #menu-toggle2">
                    @if(Auth::check())
                    <li style="display: none;"></li>
                    <li class="favourites">
                        <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-favourites.png" alt="Icon Favourites">favourites<span>8</span></a>
                    </li>
                    <li class="notify">
                        <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-notify.png" alt="Icon Favourites">notify<span>3</span></a>
                    </li>
                    @endif
                    <li class="compare">
                        <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-compare.png" alt="Icon Favourites">compare<span>2</span></a>
                    </li>
                    @if(Auth::guest())
                    <li style="display: none;"></li>
                    <li style="display: none;"></li>
                    <li><a href="{{ route('auth.register') }}" class="btn btn-white btn-sm">register</a><a href="{{ route('auth.login') }}" class="btn btn-white btn-sm">login</a></li>
                    @endif
                    <li><a href="javascript:void(0);">About us</a></li>
                    <li><a href="javascript:void(0);">Contact us</a></li>
                    @if(Auth::check())
                        <li><a href="javascript:void(0);">Hello, {{ auth()->user()->first_name }} {{ auth()->user()->last_name }}</a></li>
                            <li><a href="{{ route('auth.logout') }}">Logout</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- Global Menu End -->