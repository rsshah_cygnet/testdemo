 @if(\Session::get('current_role') == 1)
              <a href="javascript:void(0);" id="tutor_notification" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning badge notification_count">{{$notificationCount or 0}}</span>
              </a>
              <ul class="dropdown-menu header-notification-list notification_list_li">
               <li class="header-notification-list clearfix">
                <div class="list-notification">Notification List</div>
                <div class="view-all-notification">
                  <a href="javascript:void(0);" onclick="loadNotificationPage()" title="View All">View all</a>
                </div>
              </li>
              <?php  if(!empty($notifications)){ ?>
              @foreach($notifications as $key => $value)
              <li>
               @if(\Session::get('current_role') == 1)
               <div class="notification-message notification_tutor">
                @else
                <div class="notification-message">
                  @endif
                  <div class="notification-message">
                    <?php echo html_entity_decode($value['message']) ?>
                  </div>
                  <div class="notification-detail">
                    <?php
                    echo ConverTimeZoneForUser($value['created_at'], 'd-m-Y') . " | " . ConverTimeZoneForUser($value['created_at'], 'h:i a');
                    ?>
                  </div>
                </li>
                @endforeach
                <?php }else{?>
                <p>You don't have unread notifications</p>
                <?php } ?>
                </ul>
               @else
                
              <a href="javascript:void(0);" id="tutee_notification" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning badge notification_count">{{$notificationCount}}</span>
              </a>
              <ul class="dropdown-menu header-notification-list">
               <li class="header-notification-list clearfix">
                <div class="list-notification">Notification List</div>
                <div class="view-all-notification">
                  <a href="javascript:void(0);" onclick="loadNotificationPage()" title="View All">View all</a>
                </div>
              </li>
              <?php if (!empty($notifications)) { ?>
              @foreach($notifications as $key => $value)
              <li>
                <div class="notification-message"><?php echo html_entity_decode($value['message']) ?></div>
                <div class="notification-detail">
                 <?php
                 echo ConverTimeZoneForUser($value['created_at'], 'd-m-Y') . " | " . ConverTimeZoneForUser($value['created_at'], 'h:i a');
                 ?>
               </div>
             </li>
             @endforeach
             <?php } else {?>
             <p>You don't have unread notifications</p>
             <?php }?>
           </ul>
         
                  @endif
