      <div class="col-sm-12 col-md-7 col-lg-8">
        <div class="filter-result-wrapper">
          <div class="total-result"> <strong>@if(count($result) != 0) {{$result->total()}} @else No @endif</strong> @if(count($result) > 1) results @else result @endif found</div>
          <div class="filter-search-result">
            <ul class="filter-search-result-box">
              @foreach ($result as $results)
              <li>
                <div class="tutor-pearsonal-detail clearfix">
                  <div class="tutor-image"> 
                    @php  
                    $img_path = url('uploads/user/' . $results->photo);
                    if(($results->photo) != "")
                    {
                    $img_path = $img_path;
                  }
                  else
                  {
                  $img_path = url('images/default-user.png');
                }
                @endphp
                <img src="{{$img_path}}" alt="{{$results->first_name}} {{$results->last_name}}" class="round-120"></div>
                <div class="search-tutor-detail">
                  <h3>{{$results->first_name}} {{$results->last_name}}</h3>
                  <div class="degree">{{$results->qualification_name}}</div>
                  <div class="clearfix">
                    <div class="search-tutor-basic-detail-wrapper">
                      <div class="search-tutor-basic-detail">
                        <dl>
                          <dt>Nationality</dt>
                          <dd>{{$results->nationality}}</dd>
                        </dl>
                        <dl>
                          <dt>Country</dt>
                          <dd>{{$results->country_name}}</dd>
                        </dl>
                        <dl>
                          <dt>Languages</dt>
                          <dd>{{getLanguageBasedOnLanguageIdString($results->lang_id)}}</dd>
                        </dl>
                        <dl>
                          <dt>Subjects</dt>
                          <dd>{{$results->subjects}}
                          </dd>
                        </dl>
                      </div>
                    </div>
                    <div class="rating-wrapper">
                      <div class="rating"> 
                        <span>Rating</span> 
                        <input type="number" name="your_awesome_parameter" id="rating-empty-clearable" class="rating" value="{{$results->rating}}" /> 
                        <a href="javascript:void(0);" onclick="loadTutorDetails('<?php echo $results->userId; ?>')" class="star-rating-count" title="{{getReviewCountByUserId($results->userId)}} Reviews">{{getReviewCountByUserId($results->userId)}} Reviews</a> </div>
                      </div>
                      @if(!empty($results->punctuality))
                      <div class="punctual-rating-wrapper">
                        <div class="punctual-rating"> <span class="block">Punctuality Ratings</span>
                          <div class="punctual-rating-block"> <strong class="out-of">{{$results->punctuality}}</strong>
                            <storng class="out">/ 10</storng>
                          </div>
                        </div>
                      </div>
                      @endif
                      <div class="register-button-wrapper"> 
                      <!-- <a href="{{url('tutorDetail/'.$results->userId)}}" class="tutee-dashboard-btn" title="RESERVE">RESERVE</a>  -->
                      <a href="javascript:void(0);" onclick="loadTutorDetails('<?php echo $results->userId; ?>')" class="tutee-dashboard-btn" title="RESERVE">RESERVE</a> 
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              @endforeach
            </ul>
            <nav>
              <input type="hidden" name="searchhidden" value="{{$json_string}}">
              <ul class="pagination pagination-md">
                {{$result->render()}}  
              </ul>
            </nav>
          </div>
        </div>