<div class="left-aside-dashboard">
  <?php
  $active_class_dash = $em_tag_dash = $active_class_my_sessions = $em_tag_my_sessions = $em_tag_my_session_request = $active_class_session_request = $active_class_tutee_payment = $em_tag_tutee_payment = $em_tag_share_experience = $active_class_share_experience = $active_class_password_change = $em_tag_password_change = $active_class_free_session = $em_tag_free_session = $active_class_tutee_profile = $em_tag_tutee_profile = $active_class_tutee_preference = $em_tag_tutee_preference = $active_class_find_a_tutor = $em_tag_find_a_tutor = '';
  if (Route::getFacadeRoot()->current()->uri() == 'tutee-dashboard') {
   $active_class_dash = 'menu-active';
   $em_tag_dash = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'tutee-sessions') {
   $active_class_my_sessions = 'menu-active';
   $em_tag_my_sessions = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'tutee-session-request') {
   $active_class_session_request = 'menu-active';
   $em_tag_my_session_request = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'tutee/payment' || Route::getFacadeRoot()->current()->uri() == 'tutee/payment/{tabReload}') {
   $active_class_tutee_payment = 'menu-active';
   $em_tag_tutee_payment = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'share-experience') {
   $active_class_share_experience = 'menu-active';
   $em_tag_share_experience = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'change-password') {
   $active_class_password_change = 'menu-active';
   $em_tag_password_change = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'free-session') {
   $active_class_free_session = 'menu-active';
   $em_tag_free_session = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'profile' || Route::getFacadeRoot()->current()->uri() == 'profile/edit') {
   $active_class_tutee_profile = 'menu-active';
   $em_tag_tutee_profile = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'tutee-preference') {
   $active_class_tutee_preference = 'menu-active';
   $em_tag_tutee_preference = '<em class="new-notification"></em>';
 }
 if (Route::getFacadeRoot()->current()->uri() == 'find-a-tutor') {
  $active_class_find_a_tutor = 'menu-active';
  $em_tag_find_a_tutor = '<em class="new-notification"></em>';
}
?>
<ul id="tutee_left_sidebar">
  <li id="tutee_dashboard" class="{{$active_class_dash}}">
   <a href="javascript:void(0);" id="tutee_dashboard_link">
   <!--  <a href="{{route('frontend.tuteeDashboard')}}"> -->
      <i class="icon icon-dashboard"></i>
      <span>Dashboard</span>
      <?php echo $em_tag_dash; ?>
    </a>
  </li>
  <li class="{{$active_class_find_a_tutor}}">
   <a href="javascript:void(0);" id="tutee_find_tutor">
    <!-- <a href="{{route('frontend.findTutor')}}"> -->
      <i class="icon icon-search"></i>
      <span>Find Tutor</span>
      <?php echo $em_tag_find_a_tutor; ?>
    </a>
  </li>
  <li class="{{$active_class_tutee_profile}}">
  <a href="javascript:void(0);" id="tutee_profile">
    <!-- <a href="{{route('frontend.profile')}}"> -->
      <i class="icon icon-my_profile" aria-hidden="true"></i>
      <span>My Profile</span>
      <?php echo $em_tag_tutee_profile; ?>
    </a>
  </li>
  <li class="{{$active_class_tutee_preference}}">
   <a href="javascript:void(0);" id="tutee_my_preference">
   <!--  <a href="{{route('frontend.tuteePreference')}}"> -->
      <i class="icon icon-preference" aria-hidden="true"></i>
      <span>My Preferences</span>
      <?php echo $em_tag_tutee_preference; ?>
    </a>
  </li>
  <li class="{{$active_class_free_session}}">
  <a href="javascript:void(0);" id="tutee_free_session">
   <!--  <a href="{{route('tutee.free.session')}}"> -->
      <i class="icon icon-free-session" aria-hidden="true"></i>
      <span>My Free Sessions</span>
      <?php echo $em_tag_free_session; ?>
    </a>
  </li>
  <li class="{{$active_class_my_sessions}}">
   <a href="javascript:void(0);" id="tutee_my_sessions">
   <!--  <a href="{{route('frontend.tuteeSessions')}}"> -->
      <i class="icon icon-my_session"></i>
      <span>My Sessions</span>
      <?php echo $em_tag_my_sessions; ?>
    </a>
  </li>
  <li class="{{$active_class_session_request}}">
  <a href="javascript:void(0);" id="tutee_session_request">
    <!-- <a href="{{route('tutee.pending.session')}}"> -->
      <i class="fa fa-download" aria-hidden="true"></i>
      <span>Session Request</span>
      <?php echo $em_tag_my_session_request; ?>
    </a>
  </li>
  <li class="{{$active_class_tutee_payment}}">
  <a href="javascript:void(0);" id="tutee_my_payment">
    <!-- <a href="{{route('tutee.payment.request')}}"> -->
      <i class="icon icon-payment"></i>
      <span>My Payment</span>
      <?php echo $em_tag_tutee_payment; ?>
    </a>
  </li>
  <li class="{{$active_class_share_experience}}">
   <!--  <a href="{{route('frontuser.share.experience')}}"> -->
   <a href="javascript:void(0);" id="share_exp_tutee">
      <i class="icon icon-share_exp"></i>
      <span>Share Experience</span>
      <?php echo $em_tag_share_experience; ?>
    </a>
  </li>
  <li class="{{$active_class_password_change}}">
  <a href="javascript:void(0);" id="settings">
    <!-- <a href="{{route('auth.password.change')}}"> -->
      <i class="fa fa-cog" aria-hidden="true"></i>
      <span>Settings</span>
      <?php echo $em_tag_password_change; ?>
    </a>
  </li>
  <li>
    <a href="{{route('auth.logout')}}">
      <i class="icon icon-logout"></i>
      <span>Logout</span>
    </a>
  </li>
</ul>
<div class="dashboard-copyright">
  Copyright©  2017 Test demo.
</div>
</div>