<footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="footer-logo-wrapper">
                            <a href="javascript:void(0);" title="TUTE ME" class="footer-logo"> <img src="images/footer-logo.png" alt="TUTE ME" title="TUTE ME"> </a>
                        </div>
                        <div class="copyright"> <span> Copyright  ©  2010-2017 <a href="#" title="TUTE ME">TUTE ME</a>. All screenshots © their respective owners. </span> </div>
                        <ul class="list-inline footer-menu">
                            <li> {!! link_to_route('frontend.cmspages.show', 'Terms of Use', ['page_slug'=>'terms-of-use']) !!} </li>
                            <li>{!! link_to_route('frontend.cmspages.show','Support', ['page_slug' => 'support']) !!}</li>
                            <li>{!! link_to_route('frontend.cmspages.show','Privacy Policy', ['page_slug' => 'privacy-policy']) !!}</li>
                        </ul>
                        <div class="footer-social-media-wrapper">
                            <ul class="footer-social-media">
                                <li>
                                    <a href="javascript:void(0)" title="Twitter" target="_blank"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Dribble" target="_blank"> <i class="fa fa-dribbble" aria-hidden="true"></i> </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Google Plus" target="_blank"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Facebook" target="_blank"> <i class="fa fa-facebook-official" aria-hidden="true"></i> </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="Feed" target="_blank"> <i class="fa fa-rss" aria-hidden="true"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>