<div class="content-wrapper">
				<div class="content">
					<div class="row">
						<div class="col-xs-12 col-lg-12">
							<div class="box notification-list-page">
								<div class="box-header clearfix with-border">
									<div class="row">
									    <div class="col-xs-12 col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <h3>Notification List ({{$total}})</h3>
                                                </div>
                                           
                                                <div class="col-sm-4">
                                                    <span class="unread">Unread <em>{{$isNotRead}}</em></span>
                                                </div>
                                            </div>
									    </div>
									    <div class="col-xs-12 col-sm-6 notification-status">
									         
									    <a href="javascript:void(0);" id="qwe" class="older read prev" onclick="loadMoreNotifications('p');" style="display: none">Older</a>
									        @if($total != $to)
									        <a href="javascript:void(0);" id="" class="Newer next" onclick="loadMoreNotifications('n');">Newer</a>@endif
									    </div>
									</div>
								</div>
								
								@foreach ($notifications as $notification)
								@if(\Session::get('current_role') == 1)
								<div class="box-body notification_tutor">
								@else
								<div class="box-body">
								@endif
                                    <ul class="notification-list">
									    <li>
									       <div class="row">

									  <?php 
									  	$class = "";
									  	if($notification->notification_type_id == 1){
									  		$class = "important_notification";
									  	}
									  ?>     
									           <div class="col-xs-12 col-sm-8 {{$class}} notification-message-wrapper">

									              @if($notification->is_read == 0) 
									              	<span class="unread"></span>	
									              @else 
									              	 <span class="read"></span>
									              @endif
									              <?php echo html_entity_decode($notification->message) ?>
									              <strong></strong>
									           </div>
									           <div class="col-xs-12 col-sm-4">
									              <div class="notification-detail">
									              {{ConverTimeZoneForUser($notification->created_at, 'F d, Y')}} |
                                                       {{ConverTimeZoneForUser($notification->created_at, 'h:i a')}}
                                                  </div>
									           </div>
									       </div>
									    </li>  
                                    </ul>
                                @endforeach
									<div class="total-notification">
									    <div class="row">
									        <div class="col-xs-12 col-sm-8 text-center">
									        @if($total != 0 )
									           Showing {{$from}} to {{$to}}
									        @endif
									        </div>
									        <div class="col-xs-12 col-sm-4 notification-status">
									        <a href="javascript:void(0);" id="" class="older read prev" onclick="loadMoreNotifications('p');" style="display: none">Older</a>
									         @if($total != $to)
									        <a href="javascript:void(0);" id="" class="Newer next" onclick="loadMoreNotifications('n');">Newer</a>@endif
									    </div>
									        
									    </div>
									</div>
									
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		