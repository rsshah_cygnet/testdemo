<div class="container">
    <div class="section-title-primary">
        <h2 class="d-i-b">latest <span>specials</span></h2>
        <ul class="nav nav-tabs d-i-b">
            <li class="active"><a data-toggle="tab" href="#new-special">New car specials</a></li>
            <li><a data-toggle="tab" href="#used-special">Used car specials</a></li>
        </ul>
    </div>
    <div class="tab-content mob-fullwidth">
        <div id="new-special" class="tab-pane fade active in">
            <div id="latest-newcar" class="owl-carousel">
                <div class="item"><img src="{{ url('/') }}/images/latest-offer.jpg" alt="Latest Offer"></div>
                <div class="item"><img src="{{ url('/') }}/images/latest-offer.jpg" alt="Latest Offer"></div>
            </div>
        </div>
        <div id="used-special" class="tab-pane fade">
            <div id="latest-usedcar" class="owl-carousel">
                <div class="item"><img src="{{ url('/') }}/images/latest-offer.jpg" alt="Latest Offer"></div>
                <div class="item"><img src="{{ url('/') }}/images/latest-offer.jpg" alt="Latest Offer"></div>
            </div>
        </div>
    </div>
</div>