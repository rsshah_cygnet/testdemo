<div class="footer-top">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-xs-12">
                <h3>cars <span>for sale</span></h3>
                <div class="row mob-mb-15">
                    <div class="col-xs-4 p-r-n"><a href="{{ url('cars/new') }}" class="btn btn-default d-b">New cars</a></div>
                    <div class="col-xs-4 p-r-n"><a href="{{ url('cars/used') }}" class="btn btn-default d-b">Used cars</a></div>
                    <div class="col-xs-4"><a href="{{ url('cars/demo') }}" class="btn btn-default d-b">Demo cars</a></div>
                </div>
            </div>
            <div class="col-md-7 col-xs-12">
                <h3>our <span>brands</span></h3>
                <ul class="footer-brands">
                    @foreach($brands as $brand)
                        <li>
                            <a href="{{ url('cars/new/brand/'.$brand->id) }}    "><img src="{{ url('/') }}/brand_logo/{{ $brand->brand_logo }}" alt="{{ $brand->brand_name }} Logo" title="{{ $brand->brand_name }}"></a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-xs-12">
                <div class="footer-divider">
                </div>
            </div>
            <div class="col-md-7 find-us col-xs-12">
                <div class="row mob-mb-15">
                    <div class="col-md-5 col-xs-4 p-r-n"><a href="{{ route('frontend.findADealer') }}" class="btn btn-default d-b">find a <span>dealer</span></a></div>
                    <div class="col-xs-4 p-r-n"><a href="{{ route('frontend.blog.index') }}" class="btn btn-default d-b">our <span>blog</span></a></div>
                    @if(Auth::guest())
                    <div class="col-md-3 col-xs-4"><a href="{{ route('auth.register') }}" class="btn btn-default d-b"><span>register</span></a></div>
                    @endif
                </div>
            </div>
            <div class="col-md-5 talk-to-us col-xs-12">
                <div class="row">
                    <div class="col-md-4 p-r-n col-xs-12">
                        <h3><span>talk</span> to us</h3>
                    </div>
                    <div class="col-md-4 p-r-n col-xs-6"><a href="{{ route('frontend.feedback', ['type' => 'Compliment']) }}" class="btn btn-default d-b">Compliment</a></div>
                    <div class="col-md-4  col-xs-6"><a href="{{ route('frontend.feedback', ['type' => 'Complaint']) }}" class="btn btn-default d-b">Complaint</a></div>
                </div>

            </div>
        </div>
    </div>
</div>