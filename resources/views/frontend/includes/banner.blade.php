<div class="banner-top text-center">
    <a href="{{ url('/') }}"><img src="{{ url('/') }}/images/logo01.png" alt="Imperial Select"></a>
</div>
<ul class="banner-bottom clearfix">
    <li>
        <div class="link-hover">
            <h2>new <span>cars</span></h2>
            <div class="brand-logos">
                <div class="row">
                    @foreach($brands as $brand)
                        <div class="col-xs-3">
                            <a href="{{ url('cars/new/brand/'.$brand->id) }}"><img src="{{ url('/') }}/brand_logo/{{ $brand->brand_logo }}" alt="{{ $brand->brand_name }} Logo" title="{{ $brand->brand_name }}"></a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="text-center view-all">
                <a href="{{ url('cars/new') }}" class="btn btn-default btn-lg">view all new cars <img src="{{ url('/') }}/images/view-all-arrow.png" alt="View all arrow"></a>
                <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['N'])){{ $vehicleTypesCnt['N'] }}@else 0 @endif cars</p>
            </div>
            <div class="specials text-center">
                <a href="javascript:void(0)" class="btn btn-red btn-lg">specials</a>
            </div>
        </div>
    </li>
    <li>
        <div class="link-hover">
            <h2>used <span>cars</span></h2>
            <div class="used-car-types">
                <div class="row">
                    @foreach($vehicleTypes as $vehicleType)
                    <div class="col-xs-4">
                        <a href="{{ url('cars/used/type/'.$vehicleType->id) }}"><img src="{{ url('/') }}/images/icon-sedan.png" alt="Sedan Icon"><span>{{ $vehicleType->vehicle_type }}</span></a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="text-center view-all">
                <a href="{{ url('cars/used') }}" class="btn btn-default btn-lg">view all used cars <img src="{{ url('/') }}/images/view-all-arrow.png" alt="View all arrow"></a>
                <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['U'])){{ $vehicleTypesCnt['U'] }}@else 0 @endif cars</p>
            </div>
            <div class="specials text-center">
                <a href="javascript:void(0)" class="btn btn-red btn-lg">specials</a>
            </div>
        </div>
    </li>
    <li>
        <div class="link-hover">
            <h2>demo <span>cars</span></h2>
            <div class="demo-cars text-center">
                <p class="text-white">The way to go if you are looking for<br> a car without that pre-owned feel; something<br> practically brand new without the price tag.

                </p>
            </div>
            <div class="text-center view-all">
                <a href="{{ url('cars/demo') }}" class="btn btn-default btn-lg">view all demo cars <img src="{{ url('/') }}/images/view-all-arrow.png" alt="View all arrow"></a>
                <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['D'])){{ $vehicleTypesCnt['D'] }}@else 0 @endif cars</p>
            </div>
            <div class="specials text-center">
                <a href="javascript:void(0)" class="btn btn-red btn-lg">specials</a>
            </div>
        </div>
    </li>
</ul>
<div class="mobile-menu">
    <ul class="mobile-link clearfix d-b">
        <li><a href="#newcar-tab" data-toggle="tab" class="btn btn-default">new <span>cars</span></a>
            <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['N'])){{ $vehicleTypesCnt['N'] }}@else 0 @endif cars</p>
        </li>
        <li><a href="#usedcar-tab" data-toggle="tab" class="btn btn-default">used <span>cars</span></a>
            <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['U'])){{ $vehicleTypesCnt['U'] }}@else 0 @endif cars</p>
        </li>
        <li><a href="#democar-tab" data-toggle="tab" class="btn btn-default">demo <span>cars</span></a>
            <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['D'])){{ $vehicleTypesCnt['D'] }}@else 0 @endif cars</p>
        </li>
    </ul>
    <div class="mobile-tab">
        <div class="tab-content">
            <div class="tab-pane fade" id="newcar-tab">
                <div class="mobile-hover">
                    <div class="brand-logos">
                        <div class="row">
                            @foreach($brands as $brand)
                                <div class="col-xs-3">
                                    <a href="{{ url('cars/new/brand/'.$brand->id) }}"><img src="{{ url('/') }}/brand_logo/{{ $brand->brand_logo }}" alt="{{ $brand->brand_name }} Logo" title="{{ $brand->brand_name }}"></a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="text-center view-all">
                        <a href="{{ url('cars/new') }}" class="btn btn-default btn-lg">view all new cars <img src="{{ url('/') }}/images/view-all-arrow.png" alt="View all arrow"></a>
                        <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['N'])){{ $vehicleTypesCnt['N'] }}@else 0 @endif cars</p>
                    </div>
                    <div class="specials text-center">
                        <a href="javascript:void(0)" class="btn btn-red btn-lg">specials</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="usedcar-tab">
                <div class="mobile-hover">
                    <div class="used-car-types">
                        <div class="row">
                            @foreach($vehicleTypes as $vehicleType)
                                <div class="col-xs-4">
                                    <a href="{{ url('cars/used/type/'.$vehicleType->id) }}"><img src="{{ url('/') }}/images/icon-sedan.png" alt="Sedan Icon"><span>{{ $vehicleType->vehicle_type }}</span></a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="text-center view-all">
                        <a href="{{ url('cars/used') }}" class="btn btn-default btn-lg">view all used cars <img src="{{ url('/') }}/images/view-all-arrow.png" alt="View all arrow"></a>
                        <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['U'])){{ $vehicleTypesCnt['U'] }}@else 0 @endif cars</p>
                    </div>
                    <div class="specials text-center">
                        <a href="javascript:void(0)" class="btn btn-red btn-lg">specials</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade active in" id="democar-tab">
                <div class="demo-cars text-center">
                    <p class="text-white">The way to go if you are looking for<br> a car without that pre-owned feel; something<br> practically brand new without the price tag.

                    </p>
                </div>
                <div class="text-center view-all">
                    <a href="{{ url('cars/demo') }}" class="btn btn-default btn-lg">view all demo cars <img src="{{ url('/') }}/images/view-all-arrow.png" alt="View all arrow"></a>
                    <p>@if((count($vehicleTypesCnt) > 0) && isset($vehicleTypesCnt['D'])){{ $vehicleTypesCnt['D'] }}@else 0 @endif cars</p>
                </div>
                <div class="specials text-center">
                    <a href="javascript:void(0)" class="btn btn-red btn-lg">specials</a>
                </div>
            </div>
        </div>
    </div>
</div>