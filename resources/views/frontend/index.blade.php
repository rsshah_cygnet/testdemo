@extends('frontend.layouts.master')

@section('page-wrapper')

    <!-- Page Wrapper Start -->
    <div class="page-wrapper">

        <!-- Banner Start -->
        <section class="banner" id="banner">
            @include('frontend.includes.banner')
        </section>
        <!-- Banner End -->

        <!-- Latest Special Start -->
        <section id="latest-specials" class="section-padding latest-specials">
            @include('frontend.includes.latest-specials')
        </section>
        <!-- Latest Special End -->

        <!-- Latest News Srart -->
        <section id="latest-news" class="section-padding latest-news bg-gray">
            @include('frontend.includes.latest-news')
        </section>
        <!-- Latest News End -->

        <!-- Footer Start -->
        <footer class="footer">
            @include('frontend.includes.footer-top')
            @include('frontend.includes.footer-bottom')
        </footer>
        <!-- Footer End -->

    </div>
    <!-- Page Wrapper End -->

@endsection