<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="/images/favicon.ico">
	<meta property="og:url" content="http://localhost:8000/" />
	<meta property="og:title" content="{{$meta_title}}" />
	<meta property="og:description" content="{{$meta_description}}" />
	<meta property="og:image" content="http://localhost:8000/images/header-fix-logo.png" />
	<title>::Welcome to TEST DEMO::</title>
	<!--css starts-->

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700,900">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/css/fontello.css">
	<link rel="stylesheet" type="text/css" href="/css/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.css">
	<link rel="stylesheet" type="text/css" href="/css/base.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="/css/aos.css">
	<link rel="stylesheet" type="text/css" href="/css/developer.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
	<input type="hidden" id="csrf_token_header" value="<?php echo e(csrf_token()); ?>" />

	<!--css ends-->
	<!--Script starts-->
	<script src="js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="/js/modernizr-3.0.0.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/developer.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

	<!---->
	<!--Script ends-->
</head>

<body class="landing-page-main">
	<section class="page-wrapper landing-page">
		<header class="header-landing-page">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-3 col-lg-3">
						<div class="logo">
							<a title="TEST DEMO">
								<img src="images/landing-page-logo.png" alt="TEST DEMO" class="unfix-logo hidden-xs hidden-sm" title="TEST DEMO">
								<img src="/images/header-fix-logo.png" alt="TEST DEMO" class="fix-logo" title="TEST DEMO">
							</a>
						</div>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="col-xs-12 col-md-9 col-lg-9  pull-right">
						<nav id="mainnav" class="navbar collapse navbar-collapse">
							<div class="menu-navigation-container">
								<ul id="menu-navigation" class="nav navbar-nav">
									<li class="about">
										<a href="#aboutUs" title="About Us">About Us</a>
									</li>
									<li class="howitwork">
										<a href="#howItWork" title="How It Works">How It Works</a>
									</li>
									<li>
										<a href="javascript:void(0);" data-toggle="modal" data-target="#findTutor" title="Find Tutor">Find Tutor</a>
									</li>
									<li class="contact">
										<a href="#contactUS" title="Contact Us">Contact Us</a>
									</li>
									<li>
										<a href="{{route('frontend.login')}}" title="Login"> Login</a>
									</li>
									<li>
										<a href="{{route('auth.register')}}" title="Sign Up" class="sign-up-button"> Sign Up</a>
									</li>

								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<section  class="banner-section">
			<div class="banner-caption">
				<div class="banner-content">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">

								@if(Session::has('activationLinkSendForTutee'))
								<div class="alert alert-success">
									{{Session::get('activationLinkSendForTutee')}}
								</div>
								{{\Session::forget('activationLinkSendForTutee')}}
								@endif
								@if(Session::has('activationLinkSendForTutor'))
								<div class="alert alert-success">
									{{Session::get('activationLinkSendForTutor')}}
								</div>
								{{\Session::forget('activationLinkSendForTutor')}}
								@endif
								@if(Session::has('activeAccount'))
								<div class="alert alert-success">
									{{Session::get('activeAccount')}}
								</div>
								{{\Session::forget('activeAccount')}}
								@endif

								@if(Session::has('examLinkSendToEmail'))
								<div class="alert alert-success">
									{{Session::get('examLinkSendToEmail')}}
								</div>
								{{\Session::forget('examLinkSendToEmail')}}
								@endif
								<h1>New Era, New Technology, New Learning System</h1>
								<h5>Transforming education through innovative LMS technology Anywhere, Anytime</h5>
								<a href="{{route('auth.register')}}" title="Sign Up" class="sign-up-button">Sign Up</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="character-wrapper hidden-xs">
				<div class="visible-lg character first-character" data-aos-delay="300" data-aos="fade-right" data-aos-anchor-placement="top-bottom" data-aos-duration="1000">
					<img src="images/banner/11.png" alt="" title="" />
				</div>
				<div class="character second-character" data-aos="zoom-in" data-aos-delay="600" data-aos-anchor-placement="center-bottom" data-aos-delay="200">
					<img src="images/banner/22.png" alt="" title="" />
				</div>
				<div class="character third-character" data-aos="zoom-in-up" data-aos-delay="900" data-aos-anchor-placement="center-bottom" data-aos-delay="300">
					<img src="images/banner/33.png" alt="" title="" />
				</div>
				<div class="character fourth-character" data-aos="zoom-out-right" data-aos-delay="1300" data-aos-anchor-placement="center-bottom" data-aos-delay="300">
					<img src="images/banner/44.png" alt="" title="" />
				</div>
				<div class="visible-lg character fifth-character" data-aos="fade-left" data-aos-delay="1700" data-aos-anchor-placement="center-bottom" data-aos-delay="300">
					<img src="images/banner/55.png" alt="" title="" />
				</div>
			</div>
		</section>
		<!-- about section start form here -->
		<section id="aboutUs" class="about-us-section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-5 col-lg-5 about-us-content-wrapper">
						<h2>About <em>TEST<span>demo</span></em> </h2>
						<div class="about-us-content">
							<p>Develop a platform for Tutors to share his knowledge among people who needs an assistance in their routine studies.</p>
							<p>Same way this portal provides a platform for Tutees who can search for Tutor based upon their preferences &amp; get guided in the topics they face difficulties</p>
							<p>So even tutor &amp; tutees are not at the same place they can have a live session via whiteboard facility available</p>
						</div>
						<div class="hidden-md hidden-lg"><img src="/images/about-us.png" class="img-responsive" alt="about us"></div>


						<div class="button-wrapper clearfix">
							<a href="#contactUS" class="btn btn-round contact-btn-wrapper without-color">Contact Us</a>
							<a href="#aboutUsVideo" class="btn btn-round with-color" data-toggle="modal"><i class="icon icon-play" aria-hidden="true"></i>&nbsp;Play</a>
						</div>

					</div>
				</div>
			</div>
			<div class="left-round-baloons" data-aos="fade-right">
				<img src="/images/round-dot-connect.png">
			</div>
			<div class="tute-me-dashboard-image hidden-sm hidden-xs" data-aos="fade-left">
				<img src="/images/about-us.png" alt="about us">
			</div>
		</section>
		<!-- about section start form here -->
		<!-- how it work section start form here -->
		<section id="howItWork" class="how-it-work-section">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<h2>How it works</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,..</p>
					</div>
				</div>
			</div>

			<div class="container-fluid circle-wrapper">
				<div class="row">
					<div class="col-xs-12 circle-place-wrapper" style="">

						<div class="work-circle circle-1">
							<div class="table-display">
								<div class="table-cell">
									Tutee sets preferences & get suggestions for Tutor
								</div>
							</div>

						</div>
						<div class="work-circle circle-2">
							<div class="table-display">
								<div class="table-cell">
									Tutee can even find Tutors by level, subject, topic, date & time
								</div>
							</div>

						</div>
						<div class="work-circle circle-3">
							<div class="table-display">
								<div class="table-cell">
									Send request to Tutors regarding session
								</div>
							</div>

						</div>
						<div class="work-circle circle-4">
							<div class="table-display">
								<div class="table-cell">
									Attend scheduled session to get assistance from Tutor
								</div>
							</div>

						</div>
						<div class="work-circle circle-5">
							<div class="table-display">
								<div class="table-cell">
									Ratings helps Tutee to go with best Tutor
								</div>
							</div>

						</div>
						<div class="line-wrapper">
							<div class="line-1">
								<img src="/images/line/1.png" alt="line-1" title="">
							</div>
							<div class="line-2">
								<img src="/images/line/2.png" alt="line-2" title="">
							</div>
							<div class="line-3">
								<img src="/images/line/3.png" alt="line-3" title="">
							</div>
							<div class="line-4">
								<img src="/images/line/4.png" alt="line-4" title="">
							</div>
							<div class="line-5">
								<img src="/images/line/5.png" alt="line-5" title="">
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="container hidden-xs">
				<div class="row">
					<div class="col-xs-12 text-center work-type">
						<img src="/images/how-it-work.png" alt="how it work" title="how it work">
					</div>
				</div>
			</div>

		</section>
		<!-- how it works section end form here-->
		<!-- Benefits section start form here -->
		<section class="benefits-section">
			<div class="benefits-bg-image"></div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h2>Benefits</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
					</div>
					<div class="col-xs-12 col-sm-3 benefits-circle-item">
						<div class="benefits-circle">
							<i class="icon icon-tutors"></i>
						</div>
						<p>Connect qualified <br>Tutors with Tutees</p>
					</div>
					<div class="col-xs-12 col-sm-3 benefits-circle-item">
						<div class="benefits-circle">
							<i class="icon icon-globe"></i>
						</div>
						<p>Connect from <br>Anywhere at Anytime</p>
					</div>
					<div class="col-xs-12 col-sm-3 benefits-circle-item">
						<div class="benefits-circle">
							<i class="icon  icon-emoji"></i>
						</div>
						<p>Guaranteed <br>Satisfaction</p>
					</div>
					<div class="col-xs-12 col-sm-3 benefits-circle-item">
						<div class="benefits-circle">
							<i class="icon  icon-my_session"></i>
						</div>
						<p>Smarter <br>e-Learning</p>
					</div>
				</div>
			</div>
			<div class="benefits-left-hero" data-aos="fade-right">
				<img src="images/benefits-left-hero.png" alt="benefits-left-hero" />
			</div>
			<div class="benefits-right-hero" data-aos="fade-left">

				<img src="images/benefits-right-hero.png" alt="benefits-right-hero" />
			</div>
		</section>
		<!-- Benefits section Ends here -->
		<!-- contact us section start from here  -->
		<section id="contactUS" class="contact-us-wrapper">
			<!--background image of contact us form -->
			<div class="contact-us-bg-image"></div>
			<!--background image of contact us form -->
			<!-- form start form here -->
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div id="contactUsMsg" class="alert alert-success"></div>
						<div id="errorMsg" class="alert alert-danger"></div>
						<h2>Over 2 million learner already using TESTDEMO. Contact Us Today!</h2>
						<div class="row">
							<form name="contactUsForm" id="contactUs-form" method="POST" class="clearfix landing-input">
								{{csrf_field()}}
								<div class="col-xs-12 col-sm-6 col-md-3 form-group">
									<input type="text" class="form-control" name="name" placeholder="Your Name*" required="required">
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 form-group">
									<input type="email" class="form-control" name="email" placeholder="Email Address*" required="required">
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4 form-group">
									<input type="text" class="form-control landing-input" name="message" placeholder="Message*" required="required">
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2 form-group">
									<button type="submit" id="Submit-contactUs" class="btn login-btn landing-submit btn-block">Submit</button>
								</div>
							</form>
						</div>

						<div class="row">

							<div class="col-xs-12">
								<hr class="contact-section-border">
							</div>

						</div>

						<div class="row contact-help">
							<div class="col-xs-12 col-sm-4">
								<p><span><a href="tel:212-465-0500"><i class="icon  icon-phone" aria-hidden="true"></i> +1 212-465-0500</a></span></p>
							</div>
							<div class="col-xs-12 col-sm-4">
								<p><span><a href="mailto:info@testdemo.com"><i class="icon icon-line-mail" aria-hidden="true"></i> info@testdemo.com</a></span></p>
							</div>

							<div class="col-xs-12 col-sm-4">
								<p><span><a href="https://www.google.co.in/maps/place/Westfield+World+Trade+Center/@40.7116116,-74.0136559,17z/data=!3m2!4b1!5s0x89c25a19f1335beb:0xad0170a731cdc4b0!4m5!3m4!1s0x89c25a1a317ff7e1:0xe1ac02412ef2bd1e!8m2!3d40.7116116!4d-74.0114672" target="_blank"><i class="icon icon-location" aria-hidden="true"></i> Westfield Trade Center New York, NY, USA</a></span></p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- form end form here -->
		</section>
		<!-- contact us section end from here  -->
		<!-- footer start from here -->
		<footer>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<div class="footer-logo-wrapper">
							<a title="TEST DEMO" class="footer-logo"> <img src="images/footer-logo.png" alt="TEST DEMO" title="TEST DEMO"> </a>
						</div>
						<div class="copyright"> <span> Copyright  ©  2010-2017 <a href="#" title="TEST DEMO">TEST DEMO</a>. All screenshots © their respective owners. </span> </div>
						<ul class="list-inline footer-menu">
							<li> <a href="/cmspages/terms-of-use" title="Terms of Use">Terms of Use</a> </li>
							<li><a href="/cmspages/support" title="Support">Support</a></li>
							<li><a href="/cmspages/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
						</ul>
						<div class="footer-social-media-wrapper">
							<ul class="footer-social-media">
								<li>
									<a href="javascript:void(0)" title="Twitter" target="_blank"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
								</li>
								<li>
									<a href="javascript:void(0)" title="Dribble" target="_blank"> <i class="fa fa-dribbble" aria-hidden="true"></i> </a>
								</li>
								<li>
									<a href="javascript:void(0)" title="Google Plus" target="_blank"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a>
								</li>
								<li>
									<a href="javascript:void(0)" title="Facebook" target="_blank"> <i class="fa fa-facebook-official" aria-hidden="true"></i> </a>
								</li>
								<li>
									<a href="javascript:void(0)" title="Feed" target="_blank"> <i class="fa fa-rss" aria-hidden="true"></i> </a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- footer end from here -->
	</section>

	<!-- Modal HTML -->
	<div id="aboutUsVideo" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">About Us Video</h4>
				</div>
				<div class="modal-body">
					<iframe id="VideoPlay" width="560" height="315" src="https://www.youtube.com/embed/YE7VzlLtp-4" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div id="findTutor" class="modal fade find-tutor-modal transparent-modal" role="dialog">
		<div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">

				<div class="modal-body clearfix">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="row">
						<form method="POST" id="searchTutor" name="searchTutor" action="{{route('frontend.findTutor')}}"> {{csrf_field()}}
							<!--search wrapper start form here-->
							<input type="hidden" name="page" value="1">
							<div class="col-xs-12">
								<div class="custom-search form-group">
									<input type="text" placeholder="Find Tutor by name" class="custom-search form-control" name="search[tutor_name]">
									<input type="submit" class="search-submit">
								</div>
							</div>
							<!--search wrapper end from here-->
							<!--curriculum and other section start form here-->
							<div class="col-xs-12 col-md-6">
								<div class="select-accordion-wrapper" id="custom_dropdowns">
									<div class="form-group">
										<label>Select Curriculum <span class="astrick">*</span></label>
										{!! Form::select("search[curriculum_id]",['' => 'Select'] + $curriculumn, isset($curriculumn)?"":"",['class' => 'form-control select-curriculum flat-select' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")']) !!}
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-md-6">
								<div class="form-group landing-modal-dropdown">
									<label for="languageSpoken">Language Spoken</label>
									{!! Form::select('search[language][]', $language ,isset($language)?"":"", ['class' => 'form-control selectpicker language-drop' ,'multiple'=>'', 'id' => 'language_id']) !!}
								</div>
								<div class="form-group">
									<label for="tuteeRating">Tutees Rating</label>
									<select name="search[rating]" class="form-control select-curriculum" id="tuteeRating">
										<option value="">Select</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</div>
								<div class="form-group">
									<label for="punctualRating">Punctuality Rating</label>
									<select name="search[punctual-rating]" class="form-control select-curriculum" id="punctualRating">
										<option value="">Select</option>
										<option value="1">1/10</option>
										<option value="2">2/10</option>
										<option value="3">3/10</option>
										<option value="4">4/10</option>
										<option value="5">5/10</option>
										<option value="6">6/10</option>
										<option value="7">7/10</option>
										<option value="8">8/10</option>
										<option value="9">9/10</option>
										<option value="10">10/10</option>
									</select>
								</div>
								<div class="form-group reservation-box">
									<label for="reservation">Select Date</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="icon icon-calendar"></i>
										</div>
										<input type="text" id="reservation" class="form-control" name="search[reservation]">

									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-xs-12 col-md-6 time-picker-wrapper form-group">
											<div class="input-group">
												<div class="input-group-addon"> <i class="icon icon-line-clock"></i> </div>
												<input type="text" name="search[from]" class="form-control time-picker pull-right" placeholder="From">
											</div>
										</div>
										<div class="col-xs-12 col-md-6 form-group time-picker-wrapper">
											<div class="input-group">
												<div class="input-group-addon"> <i class="icon icon-line-clock"></i> </div>
												<input type="text" name="search[to]" class="form-control time-picker pull-right" placeholder="From">
											</div> 
										</div>

										@if(\Session::get('front_user_id') == "" && \Session::get('front_user_id') == 0)
										<div class="col-xs-12 col-md-12">
											<div class="form-group">
												<label for="tuteeRating">Select Timezone</label>
												<select class="form-control select-curriculum" id="timezone" name="search[timezone]">
													<option value ="">Select</option>
													@foreach($timezone as $key => $value)
													<option value="{{ $key }}">{{ $value }}</option>
													@endforeach
												</select>
											</div>
										</div>
										@endif
									</div>
								</div>
							</div>
							<!--curriculum and other section start end here-->
							<div class="col-xs-12 text-center">
								<button type="submit" id="submit" class="btn login-btn tuteme-md-btn"> SEARCH </button>
							</div>
						</form>
					</div>
				</div>

			</div>

		</div>
	</div>

	<!--Script for plugin starts-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script type="text/javascript" src="/js/daterangepicker.js"></script>
	<script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="/js/aos.js"></script>
	<script type="text/javascript">
		AOS.init({
			easing: 'ease-out-back',
			duration: 1000
		});
		$('#reservation').datetimepicker({
			"drops": "down",
			"opens": "center"
		})
		var url = $("#VideoPlay").attr('src');
		$("#aboutUsVideo").on('hide.bs.modal', function(){
			$("#VideoPlay").attr('src', '');
		});
		$("#aboutUsVideo").on('show.bs.modal', function(){
			$("#VideoPlay").attr('src', url);
		});

	</script>

	<script type="text/javascript" src="/js/global.js"></script>
	<!--Script for plugin ends-->
</body>

</html>

<script type="text/javascript">

	$(document).ready(function()
	{
		$("#reservation").val("");
		$('#contactUsMsg').hide();
		$('#errorMsg').hide();
		$('#contactUs-form').on('submit', function(e){
			e.preventDefault();
			var token = '<?php echo csrf_token() ?>';
			var url = "{{ route('frontend.contactUs') }}";

			jQuery.ajax(
			{
				type:   "POST",
				url:    url,
				dataType : 'html',
				data: $("#contactUs-form").serialize()+"&_token="+token,
				success: function(data)
				{
					$('#contactUs-form')[0].reset();
					var json_obj = $.parseJSON(data);
					var msg = json_obj.status;
					$('#contactUsMsg').show();
					$('#contactUsMsg').empty();
					$('#contactUsMsg').append(json_obj.status).fadeOut(5000);
				},
				error: function (xhr, ajaxOptions, thrownError) {
				var error = xhr.responseText;
				var json_obj = $.parseJSON(error);
				if(typeof json_obj.name !== 'undefined'){
					msg = 'Name field is required.';
				}else if(typeof json_obj.email !== 'undefined'){
					if(json_obj.email == 'Email already taken'){
						msg = 'Email already taken.';
					}else{
						msg = 'Email must be a valid email address.';
					}
				}
				else if(typeof json_obj.message !== 'undefined'){
					msg = 'Message field is required.';
				}else{
					msg = 'You have already submitted your response. Thank you for contact us.';
				}
					$('#errorMsg').show();
					$('#errorMsg').empty();
					$('#errorMsg').html(msg).fadeOut(2000);
				}
			});
		});
		$(function(){
			$(".alert").delay(2000).slideUp();
		});
	});


</script>

<script type="text/javascript">
	var baseUrl = {!! json_encode(url('/')) !!};

//Starts-  Below script is used to block back arrow of browser, because after quit the test user redirect to login page
	/*	$(document).ready(function() {
			var previous_url = document.referrer;
			alert(previous_url.substr('test'))
			if(previous_url.substr('test') != -1)
			{
			function disableBack() { window.history.forward() }
			window.onload = disableBack();
			window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
		}
		});
//Ends
*/
</script>
