@extends('frontend.layouts.master')

@section('page-wrapper')

    <!-- Page Wrapper Start -->
    <div class="page-wrapper find-dealer-page">
    	
    	<!-- Global Menu Start -->
    	@include('frontend.includes.header')
    	<!-- Global Menu End -->

    	<div class="middle-container">
            <div class="inner-container">
                <div class="find-dealer-wrapper" style="height:600px">
                    <div class="find-dealer-container">
                        <div class="row error">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="alert alert-danger">
                                    You have to select one Dealership!
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2 col-md-8 col-sm-offset-2 col-lg-8 col-lg-offset-2">
                                <h1 class="hidden-xs">Find a <span>dealer</span></h1>
                                <div class="find-dealer-content">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 province-dropdown">
                                            <label>Provinces</label>
                                            <select class="province" style="width:100%">
                                                @foreach($provinces as $province)
                                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 branch-block">
                                            <div class="dealer-branch">
                                                 <label>Branch</label>
                                                 <select class="dealership" style="width:100%">
                                                 <option selected disabled hidden>All Dealerships</option>
                                                @if($dealerships)
                                                    @foreach($dealerships as $dealership)
                                                        <option>{{ $dealership->dealership_name }}</option>     
                                                    @endforeach
                                                @endif
                                                </select>
                                            </div>
                                            <a class="lets-go btn btn-red hidden-xs" href="{{ url('cars') }}">LET’S GO!</a>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-xs-12 visible-xs">
                                         <a class="lets-go btn btn-red" href="{{ url('cars') }}">LET’S GO!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    </div><!-- End pagewrapper -->

@endsection

@section('after-scripts-end')
	
    <script type="text/javascript">

        //hide error div
        $(".error").hide();
        //if nothing is selected  on the dealership dropdown
        jQuery(document).on("click",".lets-go",function(){
            if($(".dealership").val() == null)
            {
                $(".error").show(300);
                return false;
            }
            else
            {
                $(".error").hide(300);
                return true;
            }
        });

        //to find dealerships of the particular province
        $(document).on("change",".province",function(){
            var province_id = $(this).val();

            $.ajax({
                url:'/find-a-dealer',
                data: { 'province_id':province_id},
                success: function(data)
                {
                    $(".dealership").html("");
                    $(".dealership").html(data);

                }
            });
        });

    </script>
	
@endsection