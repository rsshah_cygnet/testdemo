 @extends('frontend.layouts.tutor',['session' => $sessionData])
 @section('middle-content')
    
    <!-- Modal -->
       @include('frontend.test.mid_section_MyTest')

<div class="modal new-test-modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content modal-content-box">
            <div id="passTestMsg" class="alert alert-danger">
            </div>
            {{ Form::open(['route' => 'tutor.appear.test', 'id' => 'appear-test-form'])}}
            <div class="modal-body modal-body-section new-test-modal">
                <h2>Appear for New Test</h2>
                <div class="select-accordion-wrapper form-group">
                    <div class="form-group" id="custom_dropdowns">
                            {{ Form::select('curriculum_id',['' => 'Select Curriculum'] + $curriculumn  ,isset($result->curriculum_id)?$result->curriculum_id:"", ['class' => 'form-control select-curriculum' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required'=>'required']) }}
                        <div class="col-xs-12 select-accordion form-group">
                            <div class="row">
                                <div class="clearfix form-group">
                                   {{--  <label class="col-xs-12 col-sm-6 label-title">Education System</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>


                                </div> --}}
                                {{-- <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Level</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>


                                </div>
                                <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Program</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>


                                </div>
                                <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Grade</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Subject</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Topic</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix text-center">
                    <button type="submit" class="btn btn-orange btn-small-pad" id="give-test">GIVE TEST</button>
                    <button type="submit" class="btn btn-grey btn-small-pad" data-dismiss="modal">CANCEL</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
<!-- Modal end-->
@endsection
