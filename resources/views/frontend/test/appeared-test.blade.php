@foreach($getPaymentsByFilter as $record)
<div class="payment-box">
                                                        <div class="payment-top">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                    <div class="row send-card-requests pad-0">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                                            <div class="subject-section my-test-subject-section clearfix">
                                                                                <p class="d-i-b pull-left">Curriculum</p>
                                                                                <span class="table-cell text-left">
  <b><?php
if (isset($record->topic_id) && $record->topic_id != 0 && $record->topic_id != "") {
    echo getCurriculumHierarchy('', $record->topic_id, true); //for topic
} else {
    echo getCurriculumHierarchy($record->subject_id, '', true); //for subject
}
?>
                          </b>
                                                                                </span>
</div>
<div class="subject-section my-test-subject-section clearfix">
    <p class="d-i-b">Subject</p>
    <span><b>{{$record->subject_name}}</b></span>
</div>
<div class="subject-section my-test-subject-section clearfix">
    <p class="d-i-b">Topic</p>
    <span><b><?php echo isset($record->topic_name) && $record->topic_name != '' ? $record->topic_name : 'N/A' ?></b></span>
</div>

</div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    @if($record->passed_status == 1 && $record->is_completed == 1)
                    <div class="amount-block">
                        <div class="result-status">
                            <label>Result</label>
                            <span>
                                <i class="fa fa-check"></i>
                                    Pass
                            </span>
                        </div>
                        <div class="result-percentage">
                            <span>{{round($record->no_of_percentage, 2)}}%</span>
                        </div>
                    </div>
                    @elseif($record->passed_status == 0 && $record->is_completed == 1)
                    <div class="amount-block result-fail">
                        <div class="result-status">
                            <label>Result</label>
                            <span>
                                <i class="fa fa-close"></i>
                                    Fail
                            </span>
                        </div>
                        <div class="result-percentage">
                            <span>{{round($record->no_of_percentage, 2)}}%</span>
                        </div>
                    </div>
                     @elseif($record->is_completed == 0)
                    <div class="amount-block incomplete-status">
                        <div class="result-status">
                            <label>Result</label>
                            <span>
                                <i class="fa fa-circle-o"></i>Incomplete
                            </span>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="payment-bottom">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="schedule-list list-unstyled list-inline">
                        <li>
                            <label class="pull-left">Test appeared on :</label>
                            <span class="table-cell text-left">
                                <div><i class="icon icon-calendar-fill"></i>
                                {{getOnlyDate(ConverTimeZoneForUser($record->test_date_time,'d-m-Y'))}}
                                </div>
                                <div><i class="icon icon-clock"></i>
                                {{getOnlyTime(ConverTimeZoneForUser(($record->test_date_time),'H:i'))}}
                                </div>
                            </span>
                        </li>
                    </ul>
                    @if($record->passed_status == 0 && $record->is_completed == 1)
                    <div class="amount-refunded-right">
                        <ul class="schedule-list list-unstyled list-inline">
                            <li>
                            <div>
                                
                                @if($remainingAttemptes>0)
                                <?php
                                    $loggedInUserId     =   \Session::get('front_user_id');
                                    $userDetail = \DB::table('tutor_details')->where('id', $record->id)->first();

                                    $userId = $userDetail->front_user_id;
                                    $curriculum_id          = $userDetail->curriculum_id;
                                    $education_system_id    = $userDetail->education_system_id;
                                    $level_id               = $userDetail->level_id;
                                    $program_id             = $userDetail->program_id;
                                    $grade_id               = $userDetail->grade_id;
                                    $subject_id             = $userDetail->subject_id;
                                    $topic_id               = $userDetail->topic_id;
                                    $checkPassedTest        = \DB::table('tutor_details')->where('front_user_id', $loggedInUserId);
                                    if(!empty($curriculum_id)){
                                        $checkPassedTest = $checkPassedTest->where('curriculum_id', $curriculum_id);
                                    }
                                    if(!empty($education_system_id)){
                                        $checkPassedTest = $checkPassedTest->where('education_system_id', $education_system_id);
                                    }
                                    if(!empty($level_id)){
                                        $checkPassedTest = $checkPassedTest->where('level_id', $level_id);
                                    }
                                    if(!empty($program_id)){
                                        $checkPassedTest = $checkPassedTest->where('program_id', $program_id);
                                    }
                                    if(!empty($grade_id)){
                                        $checkPassedTest = $checkPassedTest->where('grade_id', $grade_id);
                                    }
                                    if(!empty($subject_id)){
                                        $checkPassedTest = $checkPassedTest->where('subject_id', $subject_id);
                                    }
                                    if(!empty($topic_id)){
                                        $checkPassedTest = $checkPassedTest->where('topic_id', $topic_id);
                                    }

                                    $check = $checkPassedTest->where('front_user_id', $loggedInUserId)
                                                             ->where('appeared', 1)
                                                             ->where('is_completed', 1)
                                                             ->where('passed_status', 1)
                                                             ->first();

                                                         //    dd($check);
                                ?>
                                @if(empty($check))
                                    {{$remainingAttemptes}} more attempts remaining
                                     <a href="{{route('tutor.giveTestAgain', ['id'=>\Crypt::encrypt($record->id)])}}" id="reattemp-test">Give Test</a>
                                 @endif
                                 @else
                                 You have no more attempt remaining to give test.
                                @endif
                                 </div>
                            </li>
                        </ul>
                    </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
@endforeach
@if($getPaymentsByFilter->total() > 10)
        {{$getPaymentsByFilter->links()}}
@endif
