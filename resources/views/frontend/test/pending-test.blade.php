@foreach($getPendingTest as $record)
 <?php
    $checkTestResult = checkPassedTest($record->id);
?>
 @if(empty($checkTestResult))
 <!-- Test box start here-->
 <div class="payment-box">
    <div class="payment-top">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-7 col-lg-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pending-test-vline">
                        <div class="subject-section my-test-subject-section clearfix">
                        
                            <p class="d-i-b pull-left">Curriculum</p>
                            <span class="table-cell text-left">
                                <b><?php
if (isset($record->topic_id) && $record->topic_id != 0 && $record->topic_id != "") {
    echo getCurriculumHierarchy('', $record->topic_id, true); //for topic
} else {
    echo getCurriculumHierarchy($record->subject_id, '', true); //for subject
}
?>
</b>
                            </span>
                        </div>
                        <div class="subject-section my-test-subject-section clearfix">
                            <p class="d-i-b">Subject</p>
                            <span><b>{{$record->subject_name}}</b></span>
                        </div>
                        <div class="subject-section my-test-subject-section clearfix">
                            <p class="d-i-b">Topic</p>
                            <span><b><?php echo isset($record->topic_name) && $record->topic_name != '' ? $record->topic_name : 'N/A' ?></b></span>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                <div class="amount-block">
                    <a href="{{route('tutor.test', ['id'=>\Crypt::encrypt($record->id)])}}" class="btn btn-give-test">GIVE TEST</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!-- Test box end here-->
@endforeach
    @if($getPendingTest->total() > 10)
        {{$getPendingTest->links()}}
    @endif
