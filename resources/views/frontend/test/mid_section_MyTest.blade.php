    <div class="container-fluid">
      <!-- Left aside start from here -->
       @include('frontend.includes.tutor-leftsidebar')
      <!-- Left aside end from here -->
      <!-- Right side start form here-->
      <div class="content-wrapper">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12">
                            <div class="box">
<input type="hidden" value="tutor_my_test" name="is_tutor_test" id="is_tutor_test">
                                    <div class="box-header clearfix with-border payment-header">
                                    <div class="pull-left box-title session-title">
                                        <h3 class="my-test-heading">My Test</h3>
                                    </div>
                                    <div class="payment-date payment-date-padd col-xs-12 col-sm-10 col-md-10 col-lg-9">
                                        {{-- {{Form::open(['route' => 'tutor.mytest', 'id' => 'payment-form', 'method' => 'get'])}} --}}
                                        <div class="date-block">
                                            <label class="label-top-spacing col-sm-3 col-md-4 col-lg-5 text-right">Select Date</label>
                                            <div class="datetime-range my-test-daterange">
                                                <div class="input-group">
                                                    <div class="input-group-addon"> <i class="icon icon-calendar"></i> </div>
                                                      {{--   <input type="text" class="form-control pull-right" id="payment-date"> --}}
                                                         {{Form::text('date','value', ['class' => "form-control pull-right", 'id' => "payment-date", 'name' => 'datefilter'])}}
                                                    </div>
                                            </div>
                                        </div>
                                         {{-- {{Form::hidden('removeFilter',false, ['id' => 'removeFilter'])}} --}}
                                        {{-- {{Form::close()}} --}}
                                            <div class="btn-appear-block">
                                            @if($remainingAttemptes>0)
                                                <a href="#myModal" data-toggle="modal" class="btn btn-orange"><i class="fa fa-plus"></i>Appear for New Test</a>
                                            @endif
                                           </div>
                                    </div>
                                </div>
                                            @if(Session::has('reAttemptMessage'))
                                                <div class="alert alert-danger">
                                                    {{Session::get('reAttemptMessage')}}
                                                </div>
                                            @endif
                                            @if(Session::has('ExamInconvienceMessage'))
                                              <div class="alert alert-danger">
                                                {{Session::get('ExamInconvienceMessage')}}
                                              </div>
                                              {{\Session::forget('ExamInconvienceMessage')}}
                                            @endif
                                            @include('includes.partials.messages')
                                    <div class="box-body">
                                        <div class="payment-tabs my-test-tabs">
                                            <ul class="nav nav-tabs" id="PaymentTab">
                                                <li class="active"  id="tab-home"><a data-press="tab" href="#home">Test Appeared for</a></li>
                                                <li class="pending-test"  id="tab-menu1"><a data-press="tab" href="#menu1">Pending Test</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <!-- Frist Tab start here-->
                                                <div id="home" class="tab-pane fade in active success-tab">
                                                    @foreach($appearedTest as $record)
                                                    <!-- Result Box 1 start here-->
                                                    <div class="payment-box">
                                                        <div class="payment-top">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                    <div class="row send-card-requests pad-0">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                                            <div class="subject-section my-test-subject-section clearfix">
                                                                                <p class="d-i-b pull-left">Curriculum</p>
                                                                                <span class="table-cell text-left">
  <b><?php
if (isset($record->topic_id) && $record->topic_id != 0 && $record->topic_id != "") {
	echo getCurriculumHierarchy('', $record->topic_id, true); //for topic
} else {
	echo getCurriculumHierarchy($record->subject_id, '', true); //for subject
}
?>
                          </b>
                                                                                </span>
</div>
<div class="subject-section my-test-subject-section clearfix">
    <p class="d-i-b">Subject</p>
    <span><b>{{$record->subject_name}}</b></span>
</div>
<div class="subject-section my-test-subject-section clearfix">
    <p class="d-i-b">Topic</p>
    <span><b><?php echo isset($record->topic_name) && $record->topic_name != '' ? $record->topic_name : 'N/A' ?></b></span>
</div>

</div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    @if($record->passed_status == 1 && $record->is_completed == 1)
                    <div class="amount-block">
                        <div class="result-status">
                            <label>Result</label>
                            <span>
                                <i class="fa fa-check"></i>
                                    Pass
                            </span>
                        </div>
                        <div class="result-percentage">
                            <span>{{round($record->no_of_percentage, 2)}}%</span>
                        </div>
                    </div>
                    @elseif($record->passed_status == 0 && $record->is_completed == 1)
                    <div class="amount-block result-fail">
                        <div class="result-status">
                            <label>Result</label>
                            <span>
                                <i class="fa fa-close"></i>
                                    Fail
                            </span>
                        </div>
                        <div class="result-percentage">
                            <span>{{round($record->no_of_percentage, 2)}}%</span>
                        </div>
                    </div>
                     @elseif($record->is_completed == 0)
                    <div class="amount-block incomplete-status">
                        <div class="result-status">
                            <label>Result</label>
                            <span>
                                <i class="fa fa-circle-o"></i>Incomplete
                            </span>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="payment-bottom">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="schedule-list list-unstyled list-inline">
                        <li>
                            <label class="pull-left">Test appeared on :</label>
                            <span class="table-cell text-left">
                                <div><i class="icon icon-calendar-fill"></i>
                                {{getOnlyDate(ConverTimeZoneForUser($record->test_date_time,'d-m-Y'))}}
                                </div>
                                <div><i class="icon icon-clock"></i>
                                {{getOnlyTime(ConverTimeZoneForUser(($record->test_date_time),'H:i'))}}
                                </div>
                            </span>
                        </li>
                    </ul>
                    @if($record->passed_status == 0 && $record->is_completed == 1)
                    <div class="amount-refunded-right">
                        <ul class="schedule-list list-unstyled list-inline">
                            <li>
                            <div>
                                
                                @if($remainingAttemptes>0)
                                <?php
                                    $loggedInUserId     =   \Session::get('front_user_id');
                                    $userDetail = \DB::table('tutor_details')->where('id', $record->id)->first();

                                    $userId = $userDetail->front_user_id;
                                    $curriculum_id          = $userDetail->curriculum_id;
                                    $education_system_id    = $userDetail->education_system_id;
                                    $level_id               = $userDetail->level_id;
                                    $program_id             = $userDetail->program_id;
                                    $grade_id               = $userDetail->grade_id;
                                    $subject_id             = $userDetail->subject_id;
                                    $topic_id               = $userDetail->topic_id;
                                    $checkPassedTest        = \DB::table('tutor_details')->where('front_user_id', $loggedInUserId);
                                    if(!empty($curriculum_id)){
                                        $checkPassedTest = $checkPassedTest->where('curriculum_id', $curriculum_id);
                                    }
                                    if(!empty($education_system_id)){
                                        $checkPassedTest = $checkPassedTest->where('education_system_id', $education_system_id);
                                    }
                                    if(!empty($level_id)){
                                        $checkPassedTest = $checkPassedTest->where('level_id', $level_id);
                                    }
                                    if(!empty($program_id)){
                                        $checkPassedTest = $checkPassedTest->where('program_id', $program_id);
                                    }
                                    if(!empty($grade_id)){
                                        $checkPassedTest = $checkPassedTest->where('grade_id', $grade_id);
                                    }
                                    if(!empty($subject_id)){
                                        $checkPassedTest = $checkPassedTest->where('subject_id', $subject_id);
                                    }
                                    if(!empty($topic_id)){
                                        $checkPassedTest = $checkPassedTest->where('topic_id', $topic_id);
                                    }

                                    $check = $checkPassedTest->where('front_user_id', $loggedInUserId)
                                                             ->where('appeared', 1)
                                                             ->where('is_completed', 1)
                                                             ->where('passed_status', 1)
                                                             ->first();

                                                         //    dd($check);
                                ?>
                                @if(empty($check))
                                    {{$remainingAttemptes}} more attempts remaining
                                     <a href="{{route('tutor.giveTestAgain', ['id'=>\Crypt::encrypt($record->id)])}}" id="reattemp-test">Give Test</a>
                                 @endif
                                 @else
                                 You have no more attempt remaining to give test.
                                @endif
                                 </div>
                            </li>
                        </ul>
                    </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
    <!-- Result Box 1 End here-->
    @endforeach
    @if($appearedTest->total() > 10)
        {{$appearedTest->links()}}
    @endif
</div>
<!-- Frist Tab End here-->

<!-- Second Tab Start here-->
<div id="menu1" class="tab-pane fade pending-test-tab">
@foreach($pendingTest as $record)
 <?php
    $checkTestResult = checkPassedTest($record->id);
?>
 @if(empty($checkTestResult))
 <!-- Test box start here-->
 <div class="payment-box">
    <div class="payment-top">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-7 col-lg-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pending-test-vline">
                        <div class="subject-section my-test-subject-section clearfix">
                        
                            <p class="d-i-b pull-left">Curriculum</p>
                            <span class="table-cell text-left">
                                <b><?php
if (isset($record->topic_id) && $record->topic_id != 0 && $record->topic_id != "") {
	echo getCurriculumHierarchy('', $record->topic_id, true); //for topic
} else {
	echo getCurriculumHierarchy($record->subject_id, '', true); //for subject
}
?>
</b>
                            </span>
                        </div>
                        <div class="subject-section my-test-subject-section clearfix">
                            <p class="d-i-b">Subject</p>
                            <span><b>{{$record->subject_name}}</b></span>
                        </div>
                        <div class="subject-section my-test-subject-section clearfix">
                            <p class="d-i-b">Topic</p>
                            <span><b><?php echo isset($record->topic_name) && $record->topic_name != '' ? $record->topic_name : 'N/A' ?></b></span>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                <div class="amount-block">
                    <a href="{{route('tutor.test', ['id'=>\Crypt::encrypt($record->id)])}}" class="btn btn-give-test">GIVE TEST</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!-- Test box end here-->
@endforeach
    @if($pendingTest->total() > 10)
        {{$pendingTest->links()}}
    @endif
</div>
<!-- Second Tab End here-->

</div>
</div>
</div>
</div>
                        </div>
                    </div>
                </div>
            </div>
      <!-- Right side end form here-->
      </div>
    </section>
    <!-- Modal -->


<div class="modal new-test-modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content modal-content-box">
            <div id="passTestMsg" class="alert alert-danger">
            </div>
            {{ Form::open(['route' => 'tutor.appear.test', 'id' => 'appear-test-form'])}}
            <div class="modal-body modal-body-section new-test-modal">
                <h2>Appear for New Test</h2>
                <div class="select-accordion-wrapper form-group">
                    <div class="form-group" id="custom_dropdowns">
                            {{ Form::select('curriculum_id',['' => 'Select Curriculum'] + $curriculumn  ,isset($result->curriculum_id)?$result->curriculum_id:"", ['class' => 'form-control select-curriculum' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required'=>'required']) }}
                        <div class="col-xs-12 select-accordion form-group">
                            <div class="row">
                                <div class="clearfix form-group">
                                   {{--  <label class="col-xs-12 col-sm-6 label-title">Education System</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>


                                </div> --}}
                                {{-- <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Level</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>


                                </div>
                                <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Program</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>


                                </div>
                                <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Grade</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Subject</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix form-group">
                                    <label class="col-xs-12 col-sm-6 label-title">Topic</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <select class="form-control custom-select">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix text-center">
                    <button type="submit" class="btn btn-orange btn-small-pad" id="give-test">GIVE TEST</button>
                    <button type="submit" class="btn btn-grey btn-small-pad" data-dismiss="modal">CANCEL</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
