<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="/images/favicon.ico">
	<title>::Welcome to TEST DEMO::</title>
	<!--css starts-->


	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700,900">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/css/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.css">
	<link rel="stylesheet" type="text/css" href="/css/base.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="/css/developer.css">
	<!--css ends-->
	<!--Script starts-->
	<script src="/js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="/js/modernizr-3.0.0.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<!-- date-range-picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="/js/daterangepicker.js"></script>
	<script type="text/javascript" src="/js/bootstrap-rating-input.min.js"></script>
	<!---->
	<!--Script ends-->
	<!-- Countdown start-->
	<script type="text/javascript" src="/js/frontend/jquery.countdownTimer.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/frontend/jquery.countdownTimer.css" />
	<!--  Countdown end-->
	<!--For  Test start here -->
	<link rel="stylesheet" type="text/css" href="/css/introjs.css">
	<!-- End here-->
	<script type="text/javascript" src="/js/intro.js"></script>
</head>

<body class="find-tutor">
	<section class="page-wrapper landing-page new-test-profile">
		<nav role="navigation" class="navbar landing-page-header navbar-fixed-top">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header d-i-b">
					<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle hidden-xs"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
					<a href="#" class="navbar-brand logo"> <img src="/images/logo.png" alt="TEST DEMO" title="TEST DEMO"> </a>
				</div>
				<!-- Collection of nav links, forms, and other content for toggling -->
				<div id="navbarCollapse" class="collapse navbar-collapse navbar-right ">
					<?php
if (!empty($getUser->photo)) {
	$img_path = 'uploads/user/' . $getUser->photo;
	if (file_exists($img_path)) {
		$img_path = $img_path;
	} else {
		$img_path = url('images/default-user.png');
	}
} else {
	$img_path = url('images/default-user.png');

}
?>
					<ul class="nav navbar-nav">
						<li>
							<div class="profile-pic-container">
								<div class="profile-pic">
									<div class="profile-pic-content">
										<span>
											{{ Html::image($img_path, $getUser->tutor, ['class'=>"user-image round-60"]) }}
										</span>
									</div>
									<div class="profile-pic-name">
										Welcome, <strong>{{$getUser->tutor}}</strong>
									</div>
								</div>
							</div>
						</li>
						<li class="signout"><a href="#" id="quitTest" title="Quit Test"><i class="fa fa-sign-in" aria-hidden="true"></i></a></li>

					</ul>
				</div>
			</div>
		</nav>
		<section class="mid-section">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-9">
						<ul class="mcq" data-step="1" data-intro="The Whole Curriculum would be displayed here..." data-position='bottom'>
							@if(!empty($getTestDetails->curriculum_name))
								<li>Curriculum: <strong> {{$getTestDetails->curriculum_name}}</strong></li>
							@endif
							@if(!empty($getTestDetails->educationSystem_name))
								<li>System: <strong> {{$getTestDetails->educationSystem_name}}</strong></li>
							@endif
							@if(!empty($getTestDetails->program_name))
								<li>Program: <strong> {{$getTestDetails->program_name}}</strong></li>
							@endif
							@if(!empty($getTestDetails->grade_name))
								<li>Grade: <strong> {{$getTestDetails->grade_name}}
								@if(!empty($getTestDetails->grade_type))
									@if($getTestDetails->grade_type == 0)
										(Lower)
									@elseif($getTestDetails->grade_type == 1)
										(Higher)
								@endif
							@endif
							</strong></li>
							@endif
							@if(!empty($getTestDetails->subject_name))
								<li data-step="2" data-intro="This is the Topic for which you are going to give the test" data-position='bottom'>Subject: <strong> {{$getTestDetails->subject_name}}</strong></li>
							@endif
						</ul>
					</div>
					<div class="col-md-3 timer-container">
						<div class="timer" id="countdowntimer" data-step="3" data-intro="The timer would start as soon as you are on this page. You would not be able to stop the timer in between" data-position='bottom'><span class="timer-title">Timer :</span> <span class="timer-min" id="future_date">00:00:00</span></div>
					</div>
					<div class="clearfix">
						<div class="col-xs-12 col-sm-12">
							<div class="tutor-detail-wrapper box clearfix">
								<div class="row">
									<div class="col-xs-12 timer-header-section">
										<p class="red-title"><b>DIRECTIONS:</b>  &nbsp;Solve each problem, choose the correct answer, and then fill in the corresponding oval on your answer document.</p>
										<p class="small-font">Do not linger over problems that take too much time. &nbsp;Solve as many as you can.</p>
									</div>
								</div>
								{{ Form::open(array('route' => 'tutor.test.answer', 'files'=>true, 'id'=>'Testform')) }}
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<ul class="main-panel">
											<?php $i = 1;?>
											@if($questions != null)

											@foreach($questions as $question)

											<li class="box-list">
												@if($i == 1)
												<p data-step="4" data-intro="Select a Question" data-position='bottom'> {{$question->question}}</p>
												@else
												<p> {{$question->question}}</p>
												@endif
												{{ Form::hidden($question->id)}}

												@if(!empty($question->question_image))
												<?php
$img_path_option1 = public_path('uploads/question/' . $question->question_image)
?>
												@if(file_exists($img_path_option1))
												<p>
													{{ Html::image('uploads/question/'.$question->question_image, $question->question_image, ['width' => 150, 'height' => '150']) }}
												</p>
												@endif
												@endif
												<ol class="list-radio-btn list-inline">
													<li>
														<div class="custom-radio box-radio-user">
															<input type="radio" name="{{$question->id}}" id="{{$i}}-a" class="custom-radio" value="1">
															@if($i == 1)
															<label for="{{$i}}-a" data-step="5" data-intro="Select an appropriate answer" data-position='bottom'>
																@else
																<label for="{{$i}}-a">
																	@endif
																	@if(!empty($question->option1))
																	<?php
$img_path_option1 = public_path('uploads/question/' . $question->option1)
?>
																	@if(file_exists($img_path_option1))
																	{{ Html::image('uploads/question/'.$question->option1, $question->option1, ['width' => 150, 'height' => '150']) }}
																	@else
																	{{$question->option1}}
																	@endif
																	@endif
																</label>
															</div>
														</li>
														<li>
															<div class="custom-radio box-radio-user">
																<input type="radio" name="{{$question->id}}" id="{{$i}}-b" class="custom-radio" value="2">
																<label for="{{$i}}-b">
																	@if(!empty($question->option2))
																	<?php
$img_path_option2 = public_path('uploads/question/' . $question->option2)
?>
																	@if(file_exists($img_path_option2))
																	{{ Html::image('uploads/question/'.$question->option2, $question->option2, ['width' => 150, 'height' => '150']) }}
																	@else
																	{{$question->option2}}
																	@endif
																	@endif
																</label>
															</div>
														</li>
														<li>
															<div class="custom-radio box-radio-user">
																<input type="radio" name="{{$question->id}}" id="{{$i}}-c" class="custom-radio" value="3">
																<label for="{{$i}}-c">
																	@if(!empty($question->option3))
																	<?php
$img_path_option3 = public_path('uploads/question/' . $question->option3)
?>
																	@if(file_exists($img_path_option3))
																	{{ Html::image('uploads/question/'.$question->option3, $question->option3, ['width' => 150, 'height' => '150']) }}
																	@else
																	{{$question->option3}}
																	@endif
																	@endif
																</label>
															</div>
														</li>
														<li>
															<div class="custom-radio box-radio-user">
																<input type="radio" name="{{$question->id}}" id="{{$i}}-d" class="custom-radio" value="4">
																<label for="{{$i}}-d">
																	@if(!empty($question->option4))
																	<?php
$img_path_option4 = public_path('uploads/question/' . $question->option4)
?>
																	@if(file_exists($img_path_option4))
																	{{ Html::image('uploads/question/'.$question->option4, $question->option4, ['width' => 150, 'height' => '150']) }}
																	@else
																	{{$question->option4}}
																	@endif
																	@endif
																</label>
															</div>
														</li>
													</ol>
												</li>
												<?php $i++;?>
												@endforeach
											</ul>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 text-center">
											<button type="submit" id="submitTest" class="btn btn-default login-btn" data-step="6" data-intro="Submit your Answer" data-position='top'>Submit your all selected answers</button>
											<button type="submit" id="submitTestAfterTimeup" class="btn btn-default login-btn hidden">Submit your all selected answers</button>
											
										</div>
									</div>
									{{ Form::hidden('tutorId', $tutorID)}}
									{{ Form::hidden('num_of_que', $num_of_que)}}
									{{ Form::hidden('minutes',$minutes) }}
									{{ Form::hidden('passing_percentage', $passing_percentage)}}
									{{ Form::hidden('submit','',['id'=>'submit']) }}
									{{Form::close()}}
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<footer>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<div class="footer-logo-wrapper">
							<a href="javascript:void(0);" title="TEST DEMO" class="footer-logo"> <img src="/images/footer-logo.png" alt="TEST DEMO" title="TEST DEMO"> </a>
						</div>
						<div class="copyright"> <span> Copyright  ©  2010-2017 <a href="#" title="TEST DEMO">TEST DEMO</a>. All screenshots © their respective owners. </span> </div>
						<ul class="list-inline footer-menu">
							<li> <a href="#" title="Terms of Use">Terms of Use</a> </li>
							<li><a href="#" title="Support">Support</a></li>
							<li><a href="#" title="Privacy Policy">Privacy Policy</a></li>
						</ul>
						<div class="footer-social-media-wrapper">
							<ul class="footer-social-media">
								<li>
									<a href="javascript:void(0)" title="Twitter" target="_new"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
								</li>
								<li>
									<a href="javascript:void(0)" title="Dribble" target="_new"> <i class="fa fa-dribbble" aria-hidden="true"></i> </a>
								</li>
								<li>
									<a href="javascript:void(0)" title="Google Plus" target="_new"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a>
								</li>
								<li>
									<a href="javascript:void(0)" title="Facebook" target="_new"> <i class="fa fa-facebook-official" aria-hidden="true"></i> </a>
								</li>
								<li>
									<a href="javascript:void(0)" title="Feed" target="_new"> <i class="fa fa-rss" aria-hidden="true"></i> </a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</section>

	<!--Script for plugin starts-->
	<script type="text/javascript" src="/js/global.js"></script>
	<!--Script for plugin ends-->
	<!-- Countdown script starts-->
	<script>
		$(function(){
			$('#submitTest').click(function() {
				var status = confirm("Click OK to submit test?");
				if(status == false){
					return false;
				}
				else{
					$('#submit').val('submit');
					return true;
				}
			});
		
			$('#quitTest').click(function() {
				var status = confirm('Do you want to quit test ?');
				if(status == true){
					$('#submit').val('submit');
					$('#submitTestAfterTimeup').click();
				}
			});
		});
	</script>
	<!-- Countdown script ends-->
	<script>
		$(document).ready(function() {
			function disableBack() { window.history.forward() }

			window.onload = disableBack();
			window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
		});
	</script>
	<script>
		$(window).load(function(){
			introJs().setOptions({
				exitOnOverlayClick: false,
			}).oncomplete(function() {
	          //startTimer();
	        }).onexit(function() {
	           startTimer();
	        }).start();
		});
		window.onbeforeunload = confirmExit;
		function confirmExit()
		{
			var submit = $('#submit').val();
			if(submit != '')
				console.log('');
			else
				return "";
		}

		function startTimer(){
			$("#future_date").countdowntimer({
				hours : "<?php echo $hours ?>",
				minutes : "<?php echo $minutes ?>",
				seconds : "<?php echo $seconds ?>",
				size : "sm",
				borderColor : "#f79237",
				backgroundColor : '#f79237',
				timeUp : timeisUp
			});


			function timeisUp() {
				$('#submit').val('submit');
				$('#submitTestAfterTimeup').click();

			}
		}
	</script>
</body>

</html>