<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <link rel="shortcut icon" href="images/favicon.ico">
  <title>::Welcome to TEST DEMO::</title>
  <!--css starts-->

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700,900" >
{!! Html::style('css/bootstrap.min.css') !!}
{!! Html::style('css/daterangepicker.css') !!}
{!! Html::style('css/bootstrap-datetimepicker.css') !!}
{!! Html::style('css/base.css') !!}
{!! Html::style('css/style.css') !!}
{!! Html::style('css/responsive.css') !!}
{!! Html::style('css/developer.css') !!}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<input type="hidden" id="csrf_token_header" value="<?php echo e(csrf_token()); ?>" />
<!--css ends-->
<!--Script starts-->
  {!! Html::script(('js/jquery-1.12.0.min.js')) !!}
  {!! Html::script(('js/modernizr-3.0.0.js')) !!}
  {!! Html::script(('js/bootstrap.min.js')) !!}
  {!! Html::script(('js/developer.js')) !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
  {!! Html::script(('js/bootstrap-datetimepicker.js')) !!}
  {!! Html::script(('js/daterangepicker.js')) !!}
  {!! Html::script(('js/bootstrap-rating-input.min.js')) !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<!--Script ends-->

</head>

<body class="find-tutor">
  <section class="page-wrapper landing-page tuteme-nav-toggle tuteme-nav-bar">
    <nav role="navigation" class="navbar landing-page-header navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a href="{{url('/')}}" class="navbar-brand logo"> <img src="{{url('images/logo.png')}}" alt="TEST DEMO" title="TEST DEMO"> </a> </div>
          <!-- Collection of nav links, forms, and other content for toggling -->
          <div id="navbarCollapse" class="collapse navbar-collapse navbar-right ">
            <ul class="nav list-xs-inline-b navbar-nav">
              <li> <a href="{{url('/')}}" title="Home" class="home"> <i class="fa fa-home"></i> </a> </li>
              <li> <a href="{{route('frontend.login')}}" title="Login">Login</a> </li>
              <li> <a href="{{route('auth.register')}}" title="Sign Up" class="sign-up-button">Sign Up</a> </li>
            </ul>
          </div>
        </div>
      </nav>
<!-- Middle Section start -->
<section class="mid-section findtutor-mid-section">
  @yield('middle-content') 
</section>
<!-- Middle Section end -->

<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12">
       <div class="footer-logo-wrapper">
        <a href="{{url('/')}}" title="TEST DEMO" class="footer-logo">
          <img src="{{url('images/footer-logo.png')}}" alt="TEST DEMO" title="TEST DEMO">
        </a>
      </div>
      <div class="copyright">
       <span>
         Copyright  ©  2010-2017 <a href="{{url('/')}}" title="TEST DEMO">TEST DEMO</a>. All screenshots © their respective owners.
       </span>
     </div>
     <ul class="list-inline footer-menu">
       <li>
         <a href="#" title="Terms of Use">Terms of Use</a>
       </li>
       <li><a href="#" title="Support">Support</a></li>
       <li><a href="#" title="Privacy Policy">Privacy Policy</a></li>
     </ul>
     <div class="footer-social-media-wrapper">
       <ul class="footer-social-media">
         <li>
           <a href="javascript:void(0)" title="Twitter" target="_new">
             <i class="fa fa-twitter" aria-hidden="true"></i>
           </a>
         </li>
         <li>
           <a href="javascript:void(0)" title="Dribble" target="_new">
             <i class="fa fa-dribbble" aria-hidden="true"></i>
           </a>
         </li>
         <li>
           <a href="javascript:void(0)" title="Google Plus" target="_new">
             <i class="fa fa-google-plus" aria-hidden="true"></i>
           </a>
         </li>
         <li>
           <a href="javascript:void(0)" title="Facebook" target="_new">
             <i class="fa fa-facebook-official" aria-hidden="true"></i>
           </a>
         </li>
         <li>
           <a href="javascript:void(0)" title="Feed" target="_new">
             <i class="fa fa-rss" aria-hidden="true"></i>
           </a>
         </li>
       </ul>
     </div>
   </div>
 </div>
</div>
</footer>
</section>
@yield('before-scripts-end')

@yield('after-scripts-end')
<!--Script for plugin starts-->
{!! Html::script(('js/find_tutor.js')) !!}
{!! Html::script(('js/global.js')) !!}
<script type="text/javascript">
  var baseUrl = {!! json_encode(url('/')) !!};
  
</script>
<!--Script for plugin ends-->
</body>
</html>