<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <link rel="shortcut icon" href="/images/favicon.ico">
  <meta name="_token" content="{{ csrf_token() }}" />
  <title>::Welcome to TEST DEMO::</title>
  <!--css starts-->
  <input type="hidden" id="csrf_token_header" value="<?php echo e(csrf_token()); ?>" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700,900" >
  <link rel="stylesheet" type="text/css" href="/css/fontello.css">
  <!-- ajax pagination starts -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <!-- ajax pagination ends -->
  @yield('before-page-start')
  {!! Html::style('css/bootstrap.min.css') !!}
  {!! Html::style('css/fullcalendar.min.css') !!}
  {!! Html::style('css/perfect-scrollbar.min.css') !!}
  {!! Html::style('css/base.css') !!}
  {!! Html::style('css/style.css') !!}
  {!! Html::style('css/responsive.css') !!}
  {!! Html::style('css/theme.css') !!}
  {!! Html::style('css/developer.css') !!}
  {!! Html::style('css/fontello.css') !!}
  {!! Html::style('fonts/fontello.eot') !!}
  {!! Html::style('css/bootstrap-select.min.css') !!}
  {!! Html::style('css/bootstrap-datetimepicker.css') !!}
  {!! Html::style('css/daterangepicker.css') !!}
  <!--css ends-->
  <!--Script starts-->
  {!! Html::script(('js/jquery-1.12.0.min.js')) !!}
  {!! Html::script(('js/modernizr-3.0.0.js')) !!}
  {!! Html::script(('js/bootstrap.min.js')) !!}
  {!! Html::script(('js/bootstrap-select.min.js')) !!}
{!! Html::script(('js/developer.js')) !!}

  <!-- Jquery session script start-->
  {!! Html::script(('js/frontend/jquerysession.js')) !!}
  
  {!! Html::script(('js/tutee.js')) !!}
    
  <!-- Jquery session script end -->

  <!-- date-range-picker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-tabcollapse.js"></script>
  <script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="/js/daterangepicker.js"></script>
  <script type="text/javascript" src="/js/bootstrap-rating-input.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.16/js/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <!--Script ends-->
  <!--Starts Hide session messages -->
  <script>
    $(function(){
      $(".alert").delay(5000).slideUp();
    });
  </script>
  <!--End Hide session messages -->
  <!--Script ends-->
</head>

<body class="tutee-dashboard dashboard-page tutee-calender">
  <section class="page-wrapper">
    <nav role="navigation" class="navbar dashboard-header navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header navbar-left">
          <a href="{{route('frontend.tuteeDashboard')}}" class="navbar-brand logo">
           <!--  <img src="/images/dashboard-white-logo.png" alt="TUTE ME" title="TUTE ME"> -->
          </a>

          <button type="button" class="navbar-toggle left-sidebar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav navbar-right with-become-tutor">
            <!-- login as start from here-->
            <li class="select-user">
              <?php
              $hidden_class = "";
              if ($session['front_user_role'] == "2") {
                ?>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#become-turot-dashboard" class="become-tutor">Become Tutor</a>
                <?php
                $hidden_class = "hidden-xs hidden-sm";
              }?>
             
              <?php
              $tutor_selected_class = "";
              $tutee_selected_class = "";
              $both_selected_class = "";
             // echo $session['current_role'];exit;
              if ($session['current_role'] == 1) {
                $tutor_selected_class = "selected=selected";
              } else if ($session['current_role'] == 2) {
                $tutee_selected_class = "selected=selected";
              }

              ?>
            <?php if ($session['front_user_role'] == "3") {?>
             <label class="">Logged as</label>
              <select class="" id="tutor_header_role_dropdown">
                <option selected='selected' value="2">Tutee</option>
                <option value="3">Tutor</option>
              </select>
              <?php } ?>
            </li>


            <!-- login as end from here-->
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu notification_main_li">
              <a href="javascript:void(0);" id="tutee_notification" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning badge notification_count">{{$notificationCount}}</span>
              </a>
              <ul class="dropdown-menu header-notification-list">
               <li class="header-notification-list clearfix">
                <div class="list-notification">Notification List</div>
                <div class="view-all-notification">
                  <a href="javascript:void(0);" onclick="loadNotificationPage()" title="View All">View all</a>
                </div>
              </li>
              <?php if (!empty($notifications)) { ?>
              @foreach($notifications as $key => $value)
              <li>
                <div class="notification-message"><?php echo html_entity_decode($value['message']) ?></div>
                <div class="notification-detail">
                 <?php
                 echo ConverTimeZoneForUser($value['created_at'], 'd-m-Y') . " | " . ConverTimeZoneForUser($value['created_at'], 'h:i a');
                 ?>
               </div>
             </li>
             @endforeach
             <?php } else {?>
             <p>You don't have unread notifications</p>
             <?php }?>
           </ul>
         </li>
         <!-- User Account: style can be found in dropdown.less -->
         <?php
         $session['front_user_photo'] = \DB::table('front_user')->select('photo', 'first_name', 'last_name')->where('id', \Session::get('front_user_id'))->first();
         $session['front_user_first_name'] = $session['front_user_photo']->first_name;
         $session['front_user_last_name'] = $session['front_user_photo']->last_name;
         $session['front_user_photo'] = $session['front_user_photo']->photo;
         ?>
         <li class="user">
          <a href="javascript:void(0);" onclick="loadProfile()" class="user-detail">
            <?php
            $img_path = url('uploads/user/' . $session['front_user_photo']);
            if ($session['front_user_photo'] != "") {
              $img_path = $img_path;
            } else {
              $img_path = url('images/default-user.png');
            }
            ?>

            <img src="{{$img_path}}" class="user-image round-60" alt="{{ $session['front_user_first_name'].' '.$session['front_user_last_name'] }}" title="{{ $session['front_user_first_name'].' '.$session['front_user_last_name'] }}">


            <span class="hidden-xs"> Welcome, <strong>{{ $session['front_user_first_name']." ".$session['front_user_last_name'] }}</strong></span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- Middle Section start -->
<section class="mid-section">

  @yield('middle-content') 
  {!! Html::script(('js/fullcalendar.min.js')) !!}
  @yield('middle-scripts') 
</section>

<!-- Middle Section end -->


<footer></footer>
</section>

@yield('before-scripts-end')


{!! Html::script(('/js/global.js')) !!}
{!! Html::script(('js/find_tutor.js')) !!}
<script type="text/javascript">
  var baseUrl = {!! json_encode(url('/')) !!};
  var tutee_url = baseUrl + '/tutee-dashboard';
  var tutor_url = baseUrl + '/tutor-dashboard';
  $('#tutor_header_role_dropdown').change(function(){
    var role_id = $('#tutor_header_role_dropdown').val();
    if(role_id == '3'){
      window.location.href = tutor_url;
    }else {
      window.location.href = tutee_url;
    }
  })

  $('body').on('click','#tutee_notification',function(){
        refreshNotifications();
        readNotifications(3);
         setTimeout(function() {
            updateTuteeNotificationCount();
          }, 3000);
    })

   function updateTuteeNotificationCount(){
       var final_count = 0;
       var notifications_count = $('.notification_count').text();
        if(parseInt(notifications_count) >= parseInt(3)){
             var final_count = parseInt(notifications_count) - parseInt(3);
        }else if(parseInt(notifications_count) == parseInt(2)){
            var final_count = parseInt(notifications_count) - parseInt(2);
        }else if(parseInt(notifications_count) == parseInt(1)){
            var final_count = parseInt(notifications_count) - parseInt(1);
        }
        $('.notification_count').text(final_count);
    }
</script>
@yield('after-scripts-end')
<div id="become-turot-dashboard" class="modal fade become-tutor-dashboard transparent-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title text-center">Please select subject/topic you want to teach</h2>
      </div>
      {{ Form::open(['route' => 'becometutor.appear.test', 'id' => 'submit'])}}
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 select-accordion-wrapper">
            <div class="form-group">
              <label>Curriculum <span class="astrick">*</span></label>
              <?php
              $Curriculum = getCurroculumList();
              ?>
              {{Form::hidden('becomeTutor', 1)}}
              {{ Form::select('curriculum_id',['' => 'Select Curriculum'] + $Curriculum  ,"", ['class' => 'form-control select-curriculum' , 'id' => 'curriculum_id_become','onchange'=>'getCurriculumHierarchyForBecomeTutor("curriculum_id_become")','required'=>'required']) }}
              {{-- <select class="form-control select-curriculum">
              <option>Select</option>
              <option value="lorem">Lorem</option>
            </select> --}}
            <div class="col-xs-12 select-accordion show">
              <div class="row" id="custom_dropdowns_become">
                {{-- <div class="clearfix form-group">
                <label class="col-xs-12 col-sm-6 label-title">Education System</label>
                <div class="col-xs-12 col-sm-6">
                  <select class="form-control custom-select">
                    <option value="">Select</option>
                  </select>
                </div>
              </div> --}}
            </div>
            <span class="error" id="topic_error_msg"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="modal-footer">
     <div class="row">
       <div class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
         <div class="button-wrapper-group form-group">
           <button type="submit" class="btn login-btn" id="test-submit">SUBMIT</button>
           <button type="button" class="btn btn-grey" data-dismiss="modal">CANCEL</button>
         </div>
         <p>We will send examination link for this subject/topic to your registered 
         Email id on click of submit</p>
       </div>
     </div>

   </div>
 </div>
 {{Form::close()}}
</div>
</div>
</body>

</html>