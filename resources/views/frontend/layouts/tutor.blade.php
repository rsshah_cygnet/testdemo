<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta http-equiv="X-UA-Compatible" content="IE=11">
  <link rel="shortcut icon" href="/images/favicon.ico">
  <meta name="_token" content="{{ csrf_token() }}" />
  <title>::Welcome to TEST DEMO::</title>
  <!--css starts-->
  <input type="hidden" id="csrf_token_header" value="<?php echo e(csrf_token()); ?>" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700,900" >


  <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.css">
  @yield('before-page-start')
  {!! Html::style('css/fontello.css') !!}
  {!! Html::style('css/bootstrap.min.css') !!}
  {!! Html::style('css/fullcalendar.min.css') !!}
  {!! Html::style('css/perfect-scrollbar.min.css') !!}
  {!! Html::style('css/base.css') !!}
  {!! Html::style('css/style.css') !!}
  {!! Html::style('css/responsive.css') !!}
  {!! Html::style('css/theme.css') !!}
  {!! Html::style('css/developer.css') !!}
  {!! Html::style('fonts/fontello.eot') !!}
  {!! Html::style('css/fontello.css') !!}
  {!! Html::style('css/bootstrap-select.min.css') !!}


  <!--css ends-->
  <!--Script starts-->
  <script src="/js/jquery-1.12.0.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.16/js/perfect-scrollbar.jquery.min.js"></script>
  {!! Html::script(('js/modernizr-3.0.0.js')) !!}
  {!! Html::script(('js/bootstrap.min.js')) !!}
  {!! Html::script(('js/bootstrap-select.min.js')) !!}

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
  <script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="/js/daterangepicker.js"></script>
  <script type="text/javascript" src="/js/bootstrap-rating-input.min.js"></script>
  <script type="text/javascript" src="/js/bootstrap-tabcollapse.js"></script>
{!! Html::script(('js/frontend/jquerysession.js')) !!}
  {!! Html::script(('js/tutor.js')) !!}
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <!--Script ends-->
  <!--Starts Hide session messages -->
  <script>
    $(function(){
      $(".alert").delay(5000).slideUp();
    });
  </script>
  <!--End Hide session messages -->
</head>

<body class="tutor-dashboard dashboard-page tutor-calender">

  <section class="page-wrapper">
    <nav role="navigation" class="navbar dashboard-header navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header navbar-left">
          <a href="{{route('frontend.tutorDashboard')}}" class="navbar-brand logo">
           <!--  <img src="/images/dashboard-white-logo.png" alt="TUTE ME" title="TUTE ME"> -->
          </a>

          <button type="button" class="navbar-toggle left-sidebar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav navbar-right">
            <!-- login as start from here-->
            <li class="select-user">
              <input type="hidden" id="logged_in_as" value="tutor" />
              <?php 
              $hidden_class = "";
              if ($session['front_user_role'] == "1") {?>
              <a href="{{route('become.tutee', ['id' => 2])}}" class="become-tutor {{$hidden_class}}">Become Tutee</a>
              <?php }
              
              ?>
              
              <?php
              $tutor_selected_class = "";
              $tutee_selected_class = "";
              $both_selected_class = "";

              if ($session['front_user_role'] != "1") {
                $tutor_selected_class = "selected='selected'";
              } else if ($session['front_user_role'] == "2") {
                $tutee_selected_class = "selected='selected'";
              }

              ?>
              <?php if ($session['front_user_role'] == "3") {?>
              <label class="">Logged as</label>
              <select class="" id="tutor_header_role_dropdown">
                <option {{$tutee_selected_class}} value="2">Tutee</option>
                <option {{$tutor_selected_class}} value="3">Tutor</option>
              </select>
              <?php } 
              //else {
                ?>
              <!-- <select class="">
                <option {{$tutor_selected_class}} value="1">Tutor</option>
              </select> -->
              <?php //}?>
            </li>
            <!-- login as end from here-->
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu notification_main_li">
              <a href="javascript:void(0);" id="tutor_notification" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning badge notification_count">{{$notificationCount}}</span>
              </a>
              <ul class="dropdown-menu header-notification-list notification_list_li">
               <li class="header-notification-list clearfix">
                <div class="list-notification">Notification List</div>
                <div class="view-all-notification">
                  <a href="javascript:void(0);" onclick="loadNotificationPage()" title="View All">View all</a>
                </div>
              </li>
              <?php  if(!empty($notifications)){ ?>
              @foreach($notifications as $key => $value)
              <li>
               @if(\Session::get('current_role') == 1)
               <div class="notification-message notification_tutor">
                @else
                <div class="notification-message">
                  @endif
                  <div class="notification-message">
                    <?php echo html_entity_decode($value['message']) ?>
                  </div>
                  <div class="notification-detail">
                    <?php
                    echo ConverTimeZoneForUser($value['created_at'], 'd-m-Y') . " | " . ConverTimeZoneForUser($value['created_at'], 'h:i a');
                    ?>
                  </div>
                </li>
                @endforeach
                <?php }else{?>
                <p>You don't have unread notifications</p>
                <?php } ?>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <?php
              $session['front_user_photo'] = \DB::table('front_user')->select('photo', 'first_name', 'last_name')->where('id', \Session::get('front_user_id'))->first();
              $session['front_user_first_name'] = $session['front_user_photo']->first_name;
              $session['front_user_last_name'] = $session['front_user_photo']->last_name;
              $session['front_user_photo'] = $session['front_user_photo']->photo;
              ?>
              <li class="user">
                <a href="javascript:void(0);" onclick="loadProfile()" class="user-detail">
                  <?php
                  $img_path = url('uploads/user/' . $session['front_user_photo']);
                  if ($session['front_user_photo'] != "") {
                    $img_path = $img_path;
                  } else {
                    $img_path = url('images/default-user.png');
                  }
                  ?>

                  <img src="{{$img_path}}" class="user-image round-60" alt="{{ $session['front_user_first_name'].' '.$session['front_user_last_name'] }}" title="{{ $session['front_user_first_name'].' '.$session['front_user_last_name'] }}">


                  <span class="hidden-xs"> Welcome, <strong>{{ $session['front_user_first_name']." ".$session['front_user_last_name'] }}</strong></span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
     
      <section class="mid-section">
      @yield('middle-content') 
        {!! Html::script(('js/fullcalendar.min.js')) !!}
        @yield('middle-scripts') 
      </section>

      <footer></footer>
    </section>
    
    
    {!! Html::script(('/js/global.js')) !!}
    {!! Html::script(('/js/developer.js')) !!}
    @yield('before-scripts-end')
    
    <script type="text/javascript">
     var logged_in_as = $('#logged_in_as').val();
     if(logged_in_as == 'tutor'){
      $('#find_tutor_li').hide();
    } 
    
    var baseUrl = {!! json_encode(url('/')) !!};
    var tutee_url = baseUrl + '/tutee-dashboard';
    var tutor_url = baseUrl + '/tutor-dashboard';
    $('#tutor_header_role_dropdown').change(function(){
      var role_id = $('#tutor_header_role_dropdown').val();
      if(role_id == '3'){
        window.location.href = tutor_url;
      }else {
        window.location.href = tutee_url;
      }
    })
    
    $('body').on('click','#tutor_notification',function(){
        refreshNotifications();
        readNotifications(3);
         setTimeout(function() {
            updateNotificationCount();
          }, 3000);
       
    })

    function updateNotificationCount(){
       var final_count = 0;
       var notifications_count = $('.notification_count').text();
        if(parseInt(notifications_count) >= parseInt(3)){
             var final_count = parseInt(notifications_count) - parseInt(3);
        }else if(parseInt(notifications_count) == parseInt(2)){
            var final_count = parseInt(notifications_count) - parseInt(2);
        }else if(parseInt(notifications_count) == parseInt(1)){
            var final_count = parseInt(notifications_count) - parseInt(1);
        }
        $('.notification_count').text(final_count);
    }
  </script>
  @yield('after-scripts-end')

</body>

</html>