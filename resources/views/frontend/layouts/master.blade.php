<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/images/favicon.ico">
    <meta name="_token" content="{{ csrf_token() }}" />
    <input type="hidden" id="csrf_token_header" value="{{ csrf_token() }}" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>::Welcome to TESTDEMO::</title>

    <!--css starts-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700,900" >
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/base.css">
<!--     <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/responsive.css"> -->
    <link rel="stylesheet" href="/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="/css/theme.css">
    <link rel="stylesheet" type="text/css" href="/css/developer.css">
    <!-- for forgot password-->
    <script src="/js/jquery-1.12.0.min.js"></script>
    <!-- -->
    <!--css ends-->
    <!--Script starts-->
     <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
<!-- <script src="/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="/js/modernizr-3.0.0.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script> -->
<script type="text/javascript" src="/js/modernizr-3.0.0.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/developer.js"></script>

<!-- -->

@yield('before-styles-end')
{!! Html::style('/css/style.css') !!}
{!! Html::style('/css/responsive.css') !!}
@yield('after-styles-end')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn not work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

@if (Request::route()->getName() == 'frontend.findADealer')
<body class="find-dealer-body">
    @else
    <body>
        @endif

        <!-- Page Wrapper Start -->
         <div class="ajax-loader">
              <img src="{{ url('images/loader2.svg')}}" class="img-responsive" />
            </div>
        @yield('page-wrapper')
        <!-- Page Wrapper End -->

        @yield('before-scripts-end')
        {!! Html::script(('/js/global.js')) !!}
        <script type="text/javascript">
            var baseUrl = {!! json_encode(url('/')) !!};
        </script>
        @yield('after-scripts-end')

    </body>

    </html>
