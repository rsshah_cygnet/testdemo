@extends('frontend.layouts.tutee',['session' => $sessionData])
@section('middle-content')
		<section class="mid-section">
		<div class="container-fluid">
			<!-- Left aside start from here -->
			  @include('frontend.includes.tutee-leftsidebar')
			<!-- Left aside end from here -->
			<!-- Right side start form here-->
		<form name="loadNotification" id="loadMoreNotification" method="POST"> 
        <input type="hidden" name="hidden_limit" id="hidden_limit" value="0">
        <input type="hidden" name="totalNext" id="totalNext" value="{{$total-10}}">
        <input type="hidden" name="totalPrev" id="totalPrev" value="0">
		<div id="notificationslistDiv">
				
		</div>
		</form>
			<!-- Right side end form here-->
			</div>
		</section>

@stop

@section('after-scripts-end')
<script type="text/javascript">


	$(document).ready(function()
	{

	 	var token = '<?php echo csrf_token() ?>';
	 	
		$.ajax({ 
    	url: '{{ route("frontend.getNotificationlist") }}',
    	type: 'POST',
    	dataType : 'html',
    	data: $("#loadMoreNotification").serialize()+"&_token="+token,
    	success: function(data)
    	{ 	
    		$('#notificationslistDiv').html(data);
    	}

    	});
		
		var Total = {{$total}}
	    if(Total <= 10)
	    { 
	     $(".next").removeClass('hidden');
	    }
   	});

	function loadMore(button) 
	{	

		var token = '<?php echo csrf_token() ?>';

		var hidden_limit = $('#hidden_limit').val();
		var totalNext = $('#totalNext').val();
	    var totalPrev = $('#totalPrev').val();

		if(button == 'p')
        {  
         	
          $('#hidden_limit').val(parseInt(hidden_limit) - 10);
          $('#totalPrev').val(parseInt(totalPrev) - 10);
          $('#totalNext').val(parseInt(totalNext) + 10);
        }
        else
        { 
          $('#hidden_limit').val(parseInt(hidden_limit) + 10); 
          $('#totalNext').val(parseInt(totalNext) - 10);
          $('#totalPrev').val(parseInt(totalPrev) + 10);
        }

		$.ajax({ 
    	url: '{{ route("frontend.getNotificationlist") }}',
    	type: 'POST',
    	dataType : 'html',
    	data: $("#loadMoreNotification").serialize()+"&_token="+token,
    	success: function(data)
    	{  
    	   $('#notificationslistDiv').html(data);
    	   var totalNext = $('#totalNext').val();
    	   var totalPrev = $('#totalPrev').val();

		if(totalPrev >= 0)
		$(".prev").show();

    	if(button == 'p')
      	{  
	        if(totalPrev <= 0)
	        { 
	          $(".prev").addClass('hidden');
	          $(".next").removeClass('hidden');
	        }

	        if(data == "" && button != "p")
	        { 
	          $(".prev").addClass('hidden');
	          $(".next").removeClass('hidden');
	        }
      	}
        
        else
        { 	
	        if(totalNext <= 0)
	        { 
	          $(".next").addClass('hidden');
	          $(".prev").removeClass('hidden');
	        }

	        if(data == "" && button != "n")
	        { 
	          $(".next").addClass('hidden');
	          $(".prev").removeClass('hidden');
	        }
      	}
    }

    	});

	}


</script>
  @stop