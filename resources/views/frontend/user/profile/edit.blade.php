@extends('frontend.layouts.master') @section ('title', trans('labels.frontend.user.profile.update_information')) @section('content') @section('page-header')
<h1>
    {{ trans('labels.frontend.user.profile.update_information') }}
</h1> @endsection @section('content')
<div class="form-horizontal">

    {!! Form::model($user, ['route' => 'frontend.user.profile.update', 'class' => 'form-horizontal', 'method' => 'PATCH', 'files' => true]) !!}

    <div class="form-group clearfix">
        {!! Form::label('name', trans('validation.attributes.frontend.avatar'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            <img src="{{ access()->user()->picture }}" class="user-profile-image" width="100px" height="100px" /> {!! Form::file('avatar') !!}
        </div>
    </div>
    <div class="form-group clearfix">

        {!! Form::label('name', trans('validation.attributes.frontend.name'), ['class' => 'col-lg-2 control-label required']) !!}

        <div class="col-lg-10">
            {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.name')]) !!}
        </div>
    </div>

    @if ($user->canChangeEmail())
    <div class="form-group clearfix">

        {!! Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-lg-2 control-label required']) !!}

        <div class="col-lg-10">
            {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) !!}
        </div>
    </div>
    @endif


    <div class="form-group clearfix">
        <div class="col-lg-2  xs-hidden">

        </div>
        <div class="col-lg-10">
            <a href="{{route('frontend.user.dashboard')}}" class="btn btn-blue">{{ trans('buttons.general.cancel') }}</a> {!! Form::submit(trans('labels.general.buttons.save'), ['class' => 'btn btn-blue']) !!}
        </div>
    </div>


    {!! Form::close() !!}
    <!--panel body-->
</div>
</div><!-- col-md-10 -->

@endsection