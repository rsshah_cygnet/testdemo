

	<?php
//dd($sessionData);
	?>
	<div class="container-fluid">
		<!-- Left aside start from here -->
		<?php if((isset($sessionData['current_role'])) && $sessionData['current_role'] != "" && $sessionData['current_role'] == '2'){ ?>
		@include('frontend.includes.tutee-leftsidebar')
		<?php }else{?>
		@include('frontend.includes.tutor-leftsidebar')
		<?php } ?>
		<!-- Left aside end from here -->
		<!-- Left aside end from here -->
		<!-- Right side start form here-->
		<div class="content-wrapper">
			<div class="content">
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="box">
							<div class="box-header clearfix with-border">
								<div class="pull-left box-title">
									<h3 class="">Share Your Experience</h3>
									<p class="custom_success_msg"></p>
								</div>
							</div>
							{{Form::open(['route' => 'frontuser.share.experience.store', 'method' => 'post','onsubmit' => 'return false'])}}

							@include('includes.partials.messages')
							@if(Session::has('status'))
							<div class="alert alert-success">
								{{Session::get('status')}}
							</div>
							@endif

							<div class="box-body">
								<p>Please share your experience with us so that we can make our system better</p>
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<form class="clearfix login-form">
											<div class="form-group">
												{{Form::textarea('experience', null, ['class'=>"form-control text_exp", 'rows'=>"6", 'placeholder'=>"Enter your experience description here",'id' => "share_exp_textarea"])}}
												<p class="custom_error_msg"></p>
											</div>
											
											<div class="form-group">
												<?php if((isset($sessionData['current_role'])) && $sessionData['current_role'] != "" && $sessionData['current_role'] == '2'){ ?>		
												{{Form::button('SUBMIT', ['class'=>"btn login-btn setting-btn-pad exp_submit"])}}
												<?php }else{ ?>
												{{Form::button('button', ['class'=>"btn login-btn setting-btn-pad exp_submit",'style' => "background: #f79237 none repeat scroll 0 0"])}}
												<?php } ?>			
												
												<?php if((isset($sessionData['current_role'])) && $sessionData['current_role'] != "" && $sessionData['current_role'] == '2'){ ?>
												<a href="javascript:void(0);" onclick="loadTuteeDashboard()">
													<button type="button" class="btn setting-btn-pad btn-grey">CANCEL</button></a>
													<?php }else{ ?>	
													<a href="javascript:void(0);" id="cancel_shareexp">
														<button type="button" class="btn setting-btn-pad btn-grey">CANCEL</button></a>	
														<?php } ?>	
													</div>
													
												</form>
											</div>
										</div>

									</div>
									{{Form::close()}}
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Right side end form here-->
			</div>
