<div class="container-fluid">
		<!-- Left aside start from here -->
		@include(isset($sessionData['current_role']) && $sessionData['current_role'] != "" && $sessionData['current_role'] == '2' ? 'frontend.includes.tutee-leftsidebar' : 'frontend.includes.tutor-leftsidebar')
		<!-- Left aside end from here -->
		<!-- Right side start form here-->
		{!! Form::open(['route' => 'profile.update', 'enctype' => 'multipart/form-data' ,  'method'=>'post','id' => 'edit_profile_form']) !!}
		<input type="hidden" id="is_registration" name="is_registration" value="register">
		<div class="content-wrapper">
			<div class="content">
				<div class="row">

					<div class="col-xs-12 col-lg-12">
						<div class="box free-sessions-section">
							<div class="box-header clearfix with-border">
								<div class="pull-left box-title">
									<h3 class="">Edit Profile</h3>
								</div>
							</div>
							@include('includes.partials.messages')
							<div class="box-body">
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<div class="my-profile-section form-group">
											<p class="my-profile-heading">Personal details</p>
											<div class="row">
												<div class="col-xs-12 col-md-3 col-lg-2 text-center form-group">
													<div class="personal-profile-photo round-160">

														@php
														$img_path = url('uploads/user/' . $result->photo);
														if(!empty($result->photo) != "")
														{
														$img_path = $img_path;

													}
													else
													{
													$img_path = url('images/default-user.png');
												}
												@endphp

												<img src="{{$img_path}}" class="img-circle round-160" alt="profile" id="user_img">
												{{-- {!! Form::open(['url' => 'register_user_photo', 'class' => '','id'=>'user_photo_form','enctype' => 'multipart/form-data' ,'files'=>true,'method'=>'post']) !!} --}}
												<!-- name="tutee_image" -->
												<div class="upload-profile-image">
													<input type="file" name="tutee_image" id="user_photo" onchange="upload_photos()">
													<i class="fa fa-camera" aria-hidden="true"></i>
												</div>
												<span class="text-right display-block has-error-text file_error file_error_value" style="display: none">It should be valid image.</span>
												{{-- {!! Form::close() !!} --}}
											</div>
										</div>
										{{-- {!! Form::open(['route' => 'tuteeprofile.update', 'enctype' => 'multipart/form-data' ,  'method'=>'post' ,'onsubmit' => 'return false','id' => 'edit_profile_form']) !!} --}}
										<div class="col-xs-12 col-md-9 col-lg-10 personal-details-left">

											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<label><i class="fa fa-user" aria-hidden="true"></i>First Name <span class="astrick">*</span></label>
														<input type="text" class="form-control" value="{{$result->first_name}}" name="first_name" required="required">
														 <span class="text-right display-block has-error-text first_name_error first_name_error_value" style="display: none"></span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<label><i class="fa fa-user" aria-hidden="true"></i>Last Name <span class="astrick">*</span></label>
														<input type="text" class="form-control" value="{{$result->last_name}}" name="last_name" required="required">
														 <span class="text-right display-block has-error-text last_name_error last_name_error_value" style="display: none"></span>
													</div>
												</div>
											</div>

										</div>
										<!-- form end -->

										<div class="col-xs-12 col-md-9 col-lg-10 personal-details-left">

											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<label><i class="fa fa-user" aria-hidden="true"></i>Nationality <span class="astrick">*</span></label>
														{{ Form::input('text', 'nationality', $result->nationality, ['class' => 'form-control', 'placeholder' => 'Your nationality','id' => 'nationality','required'=>'required']) }}
														 <span class="text-right display-block has-error-text nationality_error nationality_error_value" style="display: none"></span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<label><i class="fa fa-calendar" aria-hidden="true"></i>Date of Birth <span class="astrick">*</span></label>
														<div class="input-group date">
															<input type="text" class="form-control datepicker" value="{{date('m/d/Y', strtotime($result->dob))}}" name="dob" required="required">
															 <span class="text-right display-block has-error-text dob_error dob_error_value" style="display: none"></span>
															<div class="input-group-addon" >
																<i class="fa fa-calendar"></i>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>

										<div class="col-xs-12 col-md-9 col-lg-10 personal-details-left pull-right">
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<label for="userTimeZone"><i class="fa fa-language"></i>Languages Spoken <span class="astrick">*</span></label>
														
														{!! Form::select('language_id[]', $language  ,isset($result->lang_id)?$result->lang_id:"", ['class' => 'form-control edit_language selectpicker custome_lang_dropdown language-drop' ,'multiple'=>'', 'id' => 'language_id','style'=>'width:100%','required'=>'required']) !!}
														 <span class="text-right display-block has-error-text language_id_error language_id_error_value" style="display: none"></span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<label><i class="fa fa-clock-o" aria-hidden="true"></i>Time Zone <span class="astrick">*</span></label>
														{!! Form::select('timezone_id', $timezone  ,isset($result->timezone_id)?$result->timezone_id:"", ['class' => 'form-control' , 'id' => 'timezone_id','required'=>'required']) !!}
														 <span class="text-right display-block has-error-text timezone_id_error timezone_id_error_value" style="display: none"></span>
													</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-md-9 col-lg-10 personal-details-left pull-right">
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<div>
															<label for="gender"><i class="fa fa-transgender" aria-hidden="true"></i> Gender <span class="astrick">*</span></label>
														</div>

														<div class="col-xs-12 col-sm-6 form-group">
															<input checked="checked" name="gender" type="radio" value="0" <?php echo ($result->gender == 0) ? 'checked' : '' ?> > Male
														</div>
														<div class="col-xs-12 col-sm-6 form-group">
															<input name="gender" type="radio" value="1" <?php echo ($result->gender == 1) ? 'checked' : '' ?> > Female
														</div>


														<span class="glyphicon glyphicon-warning-sign form-control-feedback username_error" aria-hidden="true" style="display: none"></span>
														<span class="text-right display-block has-error-text username_error username_error_value" style="display: none">Username already exist</span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">

												</div>
											</div>
										</div>



									</div>
								</div>
							</div>
						</div>


						<div class="row">
							<!-- Address Start Section -->
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Address</p>
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
											<div class="form-group">
												<label><i class="fa fa-globe" aria-hidden="true"></i>Country <span class="astrick">*</span></label>
												{!! Form::select('country_id', ['' => 'Country'] + $country ,isset($result->country_id)?$result->country_id:"", ['class' => 'form-control' , 'id' => 'country_id','onchange'=>'getState(this.value)','required'=>'required']) !!}
												 <span class="text-right display-block has-error-text country_id_error country_id_error_value" style="display: none"></span>

											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
											<div class="form-group">
												<label><i class="fa fa-flag" aria-hidden="true"></i>State</label>
												{!! Form::select('state_id', ['' => 'State'] + $state ,isset($result->state_id)?$result->state_id:"", ['class' => 'form-control' , 'id' => 'state_id','onchange'=>'getCity(this.value)']) !!}
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
											<div class="form-group">
												<label><i class="fa fa-map-marker" aria-hidden="true"></i>City</label>
												{!! Form::select('city_id', $city + ['' => 'City'] ,isset($result->city_id)?$result->city_id:"", ['class' => 'form-control' , 'id' => 'city_id']) !!}
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Address End Section -->

							<!-- Contact Details Start Section -->
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Contact Details</p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
											<div class="form-group">
												<label><i class="fa fa-envelope" aria-hidden="true"></i>Email Address <span class="astrick">*</span></label>
												<input type="text" class="form-control" value="{{$result->email}}" name="email" required="required">
												<span class="text-right display-block has-error-text email_error email_error_value" style="display: none"></span>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
											<div class="form-group">
												<label><i class="fa fa-phone-square" aria-hidden="true"></i>Contact Number</label>
												<input type="text" class="form-control" value="{{$result->contact_no}}" name="contact_no">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Contact Details End Section-->
						</div>

						<!-- Educational Details Start Section-->
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Educational Details</p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6">
											<div class="form-group">
												<label><i class="fa fa-graduation-cap" aria-hidden="true"></i>Qualification Status <span class="astrick">*</span></label>
												{!! Form::select('qualification_id', ['' => 'Select'] + $qualification  ,isset($result->qualification_id)?$result->qualification_id:"", ['class' => 'form-control' , 'id' => 'qualification_id','required'=>'required','onchange'=>'changeCurriculumn()']) !!}
												<span class="text-right display-block has-error-text qualification_id_error qualification_id_error_value" style="display: none"></span>
											</div>
											<div class="form-group">
												<label><i class="fa fa-university" aria-hidden="true"></i>School / University Name</label>
												<input type="text"  value="{{$result->educational_details}}" class="form-control" name="educational_details">
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 select-accordion-wrapper" id="custom_dropdowns">
											<div class="form-group user_curriculum_div">
												<label><i class="fa fa-book" aria-hidden="true"></i>Select Curriculum</label>
												{!! Form::select('curriculum_id',['' => 'Select Curriculum'] + $curriculumn  ,isset($result->curriculum_id)?$result->curriculum_id:"", ['class' => 'form-control select-curriculum user_educational_curriculum' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required'=>'required']) !!}
												
											</div>
											<span class="text-right display-block has-error-text curriculum_id_error curriculum_id_error_value" style="display: none"></span>
											<div class="form-group user_subject_name_div" style="display: none">
												<label for="userSubjectName">Subject Name<span class="astrick">*</span></label>
												{{ Form::input('text', 'subject_name', isset($result->subject_name)?$result->subject_name:"", ['class' => 'form-control', 'placeholder' => 'Enter your Subject Name','id' => 'subject_name', 'maxlength' => 255]) }}
												<!-- <span class="glyphicon glyphicon-warning-sign form-control-feedback subject_name_error" aria-hidden="true" style="display: none"></span> -->
												<span class="text-right display-block has-error-text subject_name_error subject_name_error_value" style="display: none">The Subject name is required</span>
											</div>

										</div>
									</div>
									<!-- Educational Details End Section-->

									

								</div>
							</div>

						</div>
						<div class="row">
							<!-- Occupational Details Start Section -->
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Occupational Details</p>
									<div class="form-group">
										<label><i class="fa fa-user" aria-hidden="true"></i>Occupation</label>
										<input type="text"  value="{{$result->occupation_details}}" class="form-control" name="Occupation">
									</div>
								</div>
							</div>
							<!-- Occupational Details End Section -->

							<!-- Paypal Details Start Section -->
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Paypal Details <span class="astrick">*</span></p>
									<div class="form-group">
										<label><i class="fa fa-paypal" aria-hidden="true"></i>Where system can transfer (credit or debit) payment</label>
										<input type="text"  value="{{$result->paypal_registered_email}}" class="form-control" name="paypal_email" required="required">
										<span class="text-right display-block has-error-text paypal_registered_email_error paypal_registered_email_error_value" style="display: none"></span>
									</div>
								</div>
							</div>
							<!-- Paypal Details End Section-->
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<input type="hidden" name="photo" value="{{$result->photo}}" id="photo_hidden">
								<input type="submit" name="submit" class="btn login-btn setting-btn-pad" value="Update" id="edit_profile_submit_btn">
							</div>
						</div>

					</div>
	<input type="hidden" name="is_edit" id="is_edit" value="edit_profile">					
	<input type="hidden" value="<?php echo isset($result->education_system_id) ? $result->education_system_id : ''; ?>" name="edit_education_id" id="edit_education_id">
	<input type="hidden" value="<?php echo isset($result->level_id) ? $result->level_id : ''; ?>" name="edit_level_id" id="edit_level_id">
	<input type="hidden" value="<?php echo isset($result->program_id) ? $result->program_id : ''; ?>" name="edit_program_id" id="edit_program_id">
	<input type="hidden" value="<?php echo isset($result->grade_id) ? $result->grade_id : ''; ?>" name="edit_grade_id" id="edit_grade_id">
	<input type="hidden" value="<?php echo isset($result->subject_id) ? $result->subject_id : ''; ?>" name="edit_subject_id" id="edit_subject_id">
	<input type="hidden" value="<?php echo isset($result->topic_id) ? $result->topic_id : ''; ?>" name="edit_topic_id" id="edit_topic_id">		

				</div>
			</div>
		</div>
		<!-- Right side end form here-->
	</div>
</div>
{{Form::hidden('id',$result->userId)}}
{!! Form::close() !!}
</div>