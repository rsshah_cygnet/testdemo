	<div class="container-fluid">
		<!-- Left aside start from here -->
		@if((isset($sessionData['current_role'])) && $sessionData['current_role'] != "" && $sessionData['current_role'] == '1')
		@include('frontend.includes.tutor-leftsidebar')
		@else
		@include('frontend.includes.tutee-leftsidebar')
		@endif
		<!-- Left aside end from here -->
		<!-- Right side start form here-->
		<div class="content-wrapper">
			<div class="content">
				<div class="row">

					<div class="col-xs-12 col-lg-12">
						<div class="box free-sessions-section">
							<div class="box-header clearfix with-border">
								<div class="pull-left box-title">
									<h3 class="">My Profile</h3>
								</div>
								<div id="Msg"></div>
								<div class="pull-right">
									@if($sessionData['current_role'] == '2')
									<a href="javascript:void(0);" onclick="loadEditProfile();">Edit</a>
									@else
									<a class="color-title" href="javascript:void(0);" onclick="loadEditProfile();">Edit</a>	
									@endif
								</div>
							</div>
							<div class="box-body">

								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<div class="my-profile-section form-group">
											<p class="my-profile-heading">Personal details</p>
											<div class="row">
												<div class="col-xs-12 col-md-3 col-lg-2 text-center form-group">
													<div class="personal-profile-photo round-160">
														@php
														$img_path = url('uploads/user/' . $result->photo);
														if(!empty($result->photo) != "")
														{
														$img_path = $img_path;
													}
													else
													{
													$img_path = url('images/default-user.png');
												}
												@endphp
												<img src="{{$img_path}}" class="img-circle round-160 profile_image" alt="profile">
											</div>
										</div>
										<div class="col-xs-12 col-md-9 col-lg-10 personal-details-left">

											<div class="row">
												<div class="col-xs-12 col-sm-12">
													<div class="form-group my-profile-name-heading">
														<h2 class="color-title">{{$result->first_name}} {{$result->last_name}}</h2>
													</div>
												</div>
											</div>

										</div>

										<div class="col-xs-12 col-md-9 col-lg-10 personal-details-left">

											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<div class="basic-details detail-wtih-icon">
															<div class="media">
																<div class="media-left"><i class="fa fa-user" aria-hidden="true"></i></div>
																<div class="media-body">
																	<label class="basic-title full-width">Nationality</label>
																	<div class="basic-answer profile_nationality">{{$result->nationality}}</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">

													<div class="form-group">
														<div class="basic-details detail-wtih-icon">
															<div class="media">
																<div class="media-left"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																<div class="media-body">
																	<label class="basic-title full-width">Date of Birth</label>
																	<div class="basic-answer profile_dob">{{date('d/m/Y', strtotime($result->dob))}}</div>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>

										</div>

										<div class="col-xs-12 col-md-9 col-lg-10 personal-details-left pull-right">

											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<div class="basic-details detail-wtih-icon">
															<div class="media">
																<div class="media-left"><i class="fa fa-language" aria-hidden="true"></i> </div>
																<div class="media-body">
																	<label class="basic-title full-width">Languages Spoken</label>
																	<div class="basic-answer profile_language">{{$result->language}} </div>
																</div>
															</div>
														</div>
													</div>

												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">

													<div class="form-group">
														<div class="basic-details detail-wtih-icon">
															<div class="media">
																<div class="media-left"><i class="fa fa-clock-o" aria-hidden="true"></i> </div>
																<div class="media-body">
																	<label class="basic-title full-width">Time Zone</label>
																	<div class="basic-answer profile_timezone_name">{{$result->timezone_name}}</div>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>

										</div>

										<div class="col-xs-12 col-md-9 col-lg-10 personal-details-left pull-right">

											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group">
														<div class="basic-details detail-wtih-icon">
															<div class="media">
																<div class="media-left"><i class="fa fa-transgender" aria-hidden="true"></i> </div>
																<div class="media-body">
																	<label class="basic-title full-width"> Gender</label>
																	<div class="basic-answer profile_gender">{{$result->gender == 0 ? "Male": "Female"}}</div>
																</div>
															</div>
														</div>
													</div>

												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">


												</div>
											</div>

										</div>




									</div>
								</div>

							</div>
						</div>

						<div class="row">
							<!-- Address Start Section -->
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Address</p>
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

											<div class="form-group">
												<div class="basic-details detail-wtih-icon">
													<div class="media">
														<div class="media-left"><i class="fa fa-globe" aria-hidden="true"></i> </div>
														<div class="media-body">
															<label class="basic-title full-width">Country</label>
															<div class="basic-answer profile_country_name">{{$result->country_name}}</div>
														</div>
													</div>
												</div>
											</div>

										</div>

										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

											<div class="form-group">
												<div class="basic-details detail-wtih-icon">
													<div class="media">
														<div class="media-left"><i class="fa fa-flag" aria-hidden="true"></i> </div>
														<div class="media-body">
															<label class="basic-title full-width">State</label>
															<div class="basic-answer profile_state_name">
																@if(!empty($result->state_name))
																{{$result->state_name}}
																@else
																N/A
																@endif
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">

											<div class="form-group">
												<div class="basic-details detail-wtih-icon">
													<div class="media">
														<div class="media-left"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
														<div class="media-body">
															<label class="basic-title full-width">City</label>
															<div class="basic-answer profile_city_name">
																@if(!empty($result->city_name))
																{{$result->city_name}}
																@else
																N/A
																@endif
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
							<!-- Address End Section -->

							<!-- Contact Details Start Section -->
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Contact Details</p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">

											<div class="form-group">
												<div class="basic-details detail-wtih-icon">
													<div class="media">
														<div class="media-left"><i class="fa fa-envelope" aria-hidden="true"></i></div>
														<div class="media-body">
															<label class="basic-title full-width">Email Address</label>
															<div class="basic-answer profile_email">{{$result->email}}</div>
														</div>
													</div>
												</div>
											</div>

										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">

											<div class="form-group">
												<div class="basic-details detail-wtih-icon">
													<div class="media">
														<div class="media-left"><i class="fa fa-phone-square" aria-hidden="true"></i></div>
														<div class="media-body">
															<label class="basic-title full-width">Contact Number</label>
															<div class="basic-answer profile_contact_no">
																@if(!empty($result->contact_no))
																{{$result->contact_no}}
																@else
																N/A
																@endif
															</div>
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
								<!-- Contact Details End Section-->
							</div>

							<!-- Educational Details Start Section-->
							
							<div class="col-xs-12 col-sm-12">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Educational Details</p>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<div class="form-group">
												<div class="basic-details detail-wtih-icon">
													<div class="media">
														<div class="media-left"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
														<div class="media-body">
															<label class="basic-title full-width">Qualification Status</label>
															<div class="basic-answer profile_qualification_name">
																{{$result->qualification_name}}
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											<div class="form-group">
												<div class="basic-details detail-wtih-icon">
													<div class="media">
														<div class="media-left"><i class="fa fa-university" aria-hidden="true"></i></div>
														<div class="media-body">
															<label class="basic-title full-width">School / University Name</label>
															<div class="basic-answer profile_educational_details">
																@if(!empty($result->educational_details))
																{{$result->educational_details}}
																@else
																N/A
																@endif
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<div class="form-group">
												<div class="basic-details detail-wtih-icon">
													<div class="media">
														<div class="media-left"><i class="fa fa-book" aria-hidden="true"></i></div>
														@if(!empty($result->subject_name))
														<div class="media-body">
															<label class="basic-title full-width">Subject</label>
															<div class="basic-answer profile_subject_name">
																{{$result->subject_name}}
															</div>
														</div>
														@else
		<div class="media-body">
			<label class="basic-title full-width">Curriculum</label>
			<div class="basic-answer profile_curriculum">

				<?php
				if ($result->topic_id != Null || $result->topic_id != 0) {
					echo getCurriculumHierarchy('', $result->topic_id, true);
				} elseif ($result->subject_id != Null || $result->subject_id != 0) {
					$final_heirachy = getCurriculumHierarchy($result->subject_id, '', false);
					
						$counter = 1;
						$string = '';
						foreach ($final_heirachy as $key => $value) {
							$total_number = count($final_heirachy);

							if ($counter == ($total_number-1)) {
								$delimeter = "";
							} else {
								$delimeter = " | ";
							}

							if ($value['reference'] == 'grade') {
								$string .= $value['name']  . $delimeter;
							} else {
								if($value['reference'] != 'subject')
								$string .= $value['name'] . $delimeter;
							}

							$counter++;
						}
						echo $string ;

				}
				?>
			</div>
		</div>
														@endif
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Educational Details End Section-->

							
							<!-- Occupational Details Start Section -->
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Occupational Details</p>
									<div class="form-group">
										<div class="basic-details detail-wtih-icon">
											<div class="media">
												<div class="media-left"><i class="fa fa-user" aria-hidden="true"></i></div>
												<div class="media-body">
													<label class="basic-title full-width">Occupation</label>
													<div class="basic-answer profile_occupation_details">
														@if(!empty($result->occupation_details))
														{{$result->occupation_details}}
														@else
														N/A
														@endif
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Occupational Details End Section -->

							<!-- Paypal Details Start Section -->
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="my-profile-section form-group">
									<p class="my-profile-heading">Paypal Details</p>

									<div class="form-group">
										<div class="basic-details detail-wtih-icon">
											<div class="media">
												<div class="media-left"><i class="fa fa-paypal" aria-hidden="true"></i></div>
												<div class="media-body">
													<label class="basic-title full-width">Where system can transfer (credit or debit) payment</label>
													<div class="basic-answer profile_paypal_registered_email">{{$result->paypal_registered_email}}
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
							<!-- Paypal Details End Section-->
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Right side end form here-->
</div>
