<!DOCTYPE html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>joinIfRunning</title>
    {!! Html::style('css/joinSession.css') !!}
    <script type="text/javascript" charset="utf-8">
         if(window.top==window) {
            // you're not in a frame so you reload the site
             window.setTimeout('location.reload()', 10000); //reloads after 10 seconds
         } else {
            //you're inside a frame, so you stop reloading
         }
    </script>
</head>

<body id="joinifrunning" onload="">
<div id="main">
    <h1>Attempting to Join...</h1>
        <?php
        $img_path = url('images/ajax-loader.gif');
        ?>
              @if(isset($data['err']) && $data['err'] != '')
            <div id="status">
                <p>Your meeting has not yet started. Waiting for a tutor to start the meeting...</p>
                <p>You need to request tutor to start recording once your meeting has started. So you can watch session video later.</p>
                  <img src="{{$img_path}}" alt="...contacting server..." title="...contacting server..." />
                <p>You will be connected as soon as the meeting starts.</p>
            </div>
            @endif


       
</div>
</body>