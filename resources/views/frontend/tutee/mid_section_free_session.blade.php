    <div class="container-fluid">
      <!-- Left aside start from here -->
       @include('frontend.includes.tutee-leftsidebar')
      <!-- Left aside end from here -->
      <!-- Right side start form here-->
      <div class="content-wrapper">
                    <div class="content">
                        <div class="row">

                            <div class="col-xs-12 col-lg-12">
                                <div class="box free-sessions-section">
                                    <div class="box-header clearfix with-border">
                                        <div class="pull-left box-title">
                                            <h3 class="">My Free Sessions</h3>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <h2 class="color-title">Thank you!</h2>
                                        <?php 

                                        $users_session = "0";
                                        if($getFreeSession['users_free_session'] > 0){ 
                                             $users_session = $getFreeSession['users_free_session'];
                                            ?>
                                        <p>You got {{$getFreeSession['users_free_session'] or 0}} free sessions for sharing {{$getFreeSession['free_session_per_fb_count']}} session on facebook. KEEP SHARING!!</p>
                                        <?php }else{ ?>
                                        <p>Currently you don't have a free session. Please share successful sessions on Facebook to get free session.</p>
                                        <?php } ?>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <ul class="list-inline">
                                                    <li><div class="well">{{$users_session}}</div></li>
                                                    <li><p>You have {{$users_session}} free sessions!!</p></li>
                                                    <li>
                                                        <a href="javascript:void(0);" onclick="loadFindTutor()"><button type="button" class="tutee-dashboard-btn">
                                                        Reserve Session
                                                    </button></li></a>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      <!-- Right side end form here-->
      </div>