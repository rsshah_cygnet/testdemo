<div id="success_session_modal" class="modal fade session-popup-modal" role="dialog">
      <div class="modal-body modal-content modal-dialog">
         <div class="calendar-modal modal-light-brown center-block">
         <div class="row">
     <div class="col-xs-12 col-sm-12">
      <button type="button" class="close modal-close-btn" data-dismiss="modal">&times;</button>
    </div>
  </div>
  <input type="hidden" value="{{$session['0']['id']}}" name="session_id" id="session_id">
            <!-- <span class="calendar-arrow"></span> -->
            <div class="row">
                 <div class="col-sm-7">
                     <h2>{{$session['0']['first_name']." ".$session['0']['last_name']}}</h2>
                      <p>{{$session['0']['qualification_name']}}</p>
                </div>
                 <div class="col-sm-5 calendar-schedule-right">
                        <ul class="schedule-list list-unstyled list-inline">
                            <li>
                                <div class="report_tutee"><i class="fa fa-exclamation-circle"></i>Report Tutor</div>
                            </li>
                   <?php if($session['0']['share_by_tutee'] == 0){ ?>         
                            <li id="fb_share_tutee_btn">
                               <a href="javascript:void(0);" style="color: #4267b2;" title="Share on facebook"onclick="FBShareOp();"><div id="fb_share_btm"><i class="fa fa-facebook-official"></i>Share</div></a>
                            </li>
                   <?php }
                    if($session['0']['rated_by_tutee'] == 0){ 
                     ?>
                            <li>
                                <div class="rating_tutor"><i class="fa fa-exclamation-circle"></i>Rating Tutor</div>
                            </li>  
                      <?php } ?>  
                        <?php
                        if($session['0']['feedback_by_tutee'] == 0){ 
                     ?>
                            <li>
                                <div class="session_feedback"><i class="fa fa-exclamation-circle"></i>Session Feedback</div>
                            </li>  
                      <?php } ?>              
                        </ul>
                    
                </div>
            </div>
            <?php //dd($session['0']['punctuality_ratings']); ?>
            <div class="ratings-block">
                <label>Punctuality Ratings</label><span>{{$session['0']['punctuality_ratings']}}<em>/10</em></span></div>

            <?php if(!empty($session['0']['ratings_given_by_tutee'])){ ?>    
            <div id="start_success">
             <div class="star-rating-block">
                  <label>Rating</label>     
                  <div class="ratings-star-inner"><input type="number" value="{{$session['0']['ratings_given_by_tutee']}}" name="your_awesome_parameter" id="rating-empty-clearable" class="rating" readonly/></div>
                 <span>({{$session['0']['ratings_given_by_tutee']}}/5)</span>
             </div>        
          </div>
          <?php } ?>
          <div class="ratings-block">
         <?php if(isset($session['0']['session_feedback_tutor']) && $session['0']['session_feedback_tutor'] != ""){ ?> 
          <label>Review : <strong>{{$session['0']['session_feedback_tutor']}}</strong></label>
          <?php } ?>
        <!--       <label>Review <strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
  sed do eiusmod ... <a href="javascript:void(0);">Read More</a></strong></label> -->
          </div>
      <?php 
        if(isset($session['0']['topic_id']) && $session['0']['topic_id'] != 0 && $session['0']['topic_id'] != ""){ 
          $curriculumHierarchy = getCurriculumHierarchy('',$session['0']['topic_id'],true); 
        }else{ 
             $curriculumHierarchy = getCurriculumHierarchy($session['0']['subject_id'],'',true);          
        }
      ?>
                  <p>Session : <b>{{$curriculumHierarchy}}</b></p>
          <!-- <ul class="list-unstyled curriculam-list list-inline">
                  <li>Session</li>
                  <li>UK</li>
                  <li>IB</li>
                  <li>MYP</li>
                  <li>10th</li>
                  <li>English</li>
                  <li>Grammar</li>
              </ul> -->
          <ul class="schedule-list list-unstyled list-inline">
                 <!--  <li>
                      <label>Accepted on :</label>
                      <div><i class="fa fa-calendar"></i>{{ConverTimeZoneForUser($session['0']['accepted_date_time'],'d-m-Y')}}</div>
                      <div><i class="fa fa-clock-o"></i>{{ConverTimeZoneForUser($session['0']['accepted_date_time'],'h:i a')}}</div>
                  </li> -->
                  <li>
                      <label>Conducted on :</label>
                      <div><i class="fa fa-calendar"></i>{{ConverTimeZoneForUser($session['0']['conducted_date_time'],'d-m-Y')}}</div>
                      <div><i class="fa fa-clock-o"></i>{{ConverTimeZoneForUser($session['0']['conducted_date_time'],'h:i a')}}</div>
                  </li>
                   @if(isset($session['0']['record_url']) && $session['0']['record_url'] != '')
                   <li>
                      Click <a href = "{{ $session['0']['record_url'] }}" target="_blank">here</a> to watch session video
                  </li>
                  @endif
              </ul>
      </div>
     </div>
       <?php 
          $tutor_name = $session['0']['first_name'].' '.$session['0']['last_name']; 
          $tutee_name = $session['0']['tutee_first_name'].' '.$session['0']['tutee_last_name']; 
          $date = ConverTimeZoneForUser($session['0']['conducted_date_time'],'h:i a');
          $description = "Tutor name : $tutor_name, 
            Tutee Name : $tutee_name, 
            Topic or Subject : $curriculumHierarchy, 
            Session Date : $date";
      ?>
      <input type="hidden" value="{{$description}}" id="share_description" name="share_description">
  </div>


 <!-- Modal -->
<div class="modal fade" id="report_tutee_modal" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content modal-content-box">

      <div class="modal-body modal-body-section">
        <form id="ReportTuteeForm" method="POST">
          <label class="error"></label>
          <div class="form-group">
        
            <label for="">Report Tutor</label>
            <textarea class="form-control sm-textarea" placeholder="Enter your detail to give your feedback" id="reporttext" name="reporttext"></textarea>

          </div>
          <div class="clearfix text-center">
            <button type="submit" class="btn login-btn btn-small-pad">SUBMIT</button>
            <button type="button" class="btn btn-grey btn-small-pad" data-dismiss="modal">CANCEL</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
<!-- Modal end-->


<!-- Modal -->
<div class="modal fade" id="rating_tutor_modal" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content modal-content-box">

      <div class="modal-body modal-body-section">
        <form id="RatingTutorForm" method="POST">
         <label class="error"></label>
          <div class="form-group">
            <label for="">Rating</label>
            <div class="dashboard-rating-border">
              <input type="number" name="rating" id="rating-empty-clearable-tutee" class="rating" value="" /> <span id="rating-outof" class="rating-outof">0/5</span>
            </div>
          </div>
          <div class="form-group">
            <label for="">Review Tutor</label>
            <textarea class="form-control sm-textarea" placeholder="Enter your detail to give your feedback" name="review" id="review"></textarea>

          </div>
          <div class="clearfix text-center">
            <button type="submit" class="btn login-btn btn-small-pad">SUBMIT</button>
            <button type="button" class="btn btn-grey btn-small-pad" data-dismiss="modal">CANCEL</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
<!-- Modal end-->

<!-- Session Feedback Modal Start-->
<div class="modal fade" id="session_feedback_modal" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content modal-content-box">

      <div class="modal-body modal-body-section">
        <form id="SessionFeedbackForm" method="POST">
          <label class="error"></label>
          <div class="form-group">
        
            <label for="">Session Feedback</label>
            <textarea class="form-control sm-textarea" placeholder="Enter your detail to give your feedback" id="feedbackText" name="feedbackText"></textarea>

          </div>
          <div class="clearfix text-center">
            <button type="submit" class="btn login-btn btn-small-pad">SUBMIT</button>
            <button type="button" class="btn btn-grey btn-small-pad" data-dismiss="modal">CANCEL</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
