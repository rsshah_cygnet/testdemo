    <div class="container-fluid">
      <!-- Left aside start from here -->
       @include('frontend.includes.tutee-leftsidebar')
      <!-- Left aside end from here -->
      <!-- Right side start form here-->
      <div class="content-wrapper">
        <div class="content">
          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="box">
                <div class="box-header clearfix with-border">
                  <div class="pull-left box-title session-title">
                    <h3>My Session</h3>
                    @include('includes.partials.messages')
                     </div>
                </div>
                <div class="box-body">
                <input type="hidden" name="logged_in_user_id" id="logged_in_user_id" value="<?php echo \Session::get('front_user_id') ?>">
                  <div id="calendar"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Right side end form here-->
  <div id="session_modal_div"></div>



  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content modal-content-box">
          <div id="model-body" class="modal-body text-center modal-body-section">
             <h2>Are you sure,</h2>
          <p>Do you want to cancel this session ?</p>
          <div class="form-group clearfix">
            <button type="submit" class="btn btn-small-pad login-btn" id="declineSession">Yes</button>
            <button type="submit" class="btn btn-grey btn-small-pad" data-dismiss="modal">No</button>
          </div>
          </div>
      </div>
    </div>
  </div>
  </div>

      </div>
   @section('middle-scripts')
 
     
   <script type="text/javascript">
    var html='';
     html+='<div class="color-legend"><ul class="list-inline box-legend-calender"><li><span class="small-box upcomming-session"></span>Upcoming Session</li><li><span class="small-box successful-session"></span>Successful Session</li><li><span class="small-box cancel-session-tutee"></span>Session cancelled by Tutee</li><li><span class="small-box cancel-session-tutor"></span>Session cancelled by Tutor</li></ul></div>';



    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();

$('#calendar').fullCalendar({
    eventAfterAllRender: function (event, element) {
        if($('.fc-header-toolbar').parent().find('.color-legend').length <= 0){
            $('.fc-header-toolbar').after(html)
        }
    },
        header: {
            left: '',
            center: 'prev,title,next,today',
            right: ''
        },
        defaultDate: '<?php echo $currentDate; ?>',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        eventClick: function(calEvent, jsEvent, view) {
          var status = calEvent.status;
          var id = calEvent.id;
          showSessionPopup(id,status);
          //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

        },
        events : <?php echo $finalSessionData; ?>

});
    /*$('body').on('click','#fb_share_btm',function(){
              FBShareOp();
          });*/
</script>

@stop
