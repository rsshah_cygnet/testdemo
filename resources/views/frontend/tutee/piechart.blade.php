   <div id="piechart_3d" style="width: 100%; height: 320px;"></div>
   <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

       var record={!! json_encode($user) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
       data.addColumn('string', 'Session Type');
       data.addColumn('number', 'Total');
       for(var k in record){
            var v = record[k];

             data.addRow([k,v]);
             //console.log(v);
          }
        var options = {
            title: '',
            is3D: false,
            width: '90%',
            height: '90%',
            chartArea: {
                left: "3%",
                top: "3%",
                height: "80%",
                width: "80%"
            },
            /*legend:{position: 'top'},
*/
          };
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
