 <?php
 $selected_languages = array();

 if(isset($tutee_basic_preference_data['lang_id']) && $tutee_basic_preference_data['lang_id'] != '')
  $selected_languages = explode(',',$tutee_basic_preference_data['lang_id']);

?>
  <div class="container-fluid">
    <!-- Left aside start from here -->
    @include('frontend.includes.tutee-leftsidebar')
    <!-- Left aside end from here -->
    <!-- Right side start form here-->
    <div class="content-wrapper">
      <div class="content">
        <div class="row">

          <div class="col-xs-12 col-lg-12">
            <div class="box free-sessions-section">
              <div class="box-header clearfix with-border">
                <div class="pull-left box-title">
                  <h3 class="">My Preferences</h3>
                </div>
              </div>
              <div class="box-body">

                <h3 class="color-title">Tutees preferences for taking Session</h3>
                <div id="Msg"></div>
                {!! Form::open(['url' => 'register', 'class' => '','onsubmit'=>'return false','id'=>'tutee_preference']) !!}
                <div class="row preferences-section-form">
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label>Language Spoken</label>
                     
                     {!! Form::select('lang_id[]', $language  ,$selected_languages, ['multiple'=>'','class' => 'form-control selectpicker custome_lang_dropdown language-drop' , 'id' => 'lang_id','style'=>'width:100%']) !!}
                     <span class="text-right display-block has-error-text lang_id_error lang_id_error_value" style="display: none">Username already exist</span>
                   </div>




                   <div class="form-group">
                    <label for="from_punctual_rating">Punctuality Rating</label>
                    <div class="row">
                      <div class="col-xs-12 col-sm-6 form-group">
                        {{ Form::input('number', 'from_punctual_rating', $tutee_basic_preference_data['from_punctual_rating'], ['class' => 'form-control', 'placeholder' => 'From Punctuality Rating','id' => 'from_punctual_rating']) }}
                        <span class="text-right display-block has-error-text from_punctual_rating_error from_punctual_rating_error_value" style="display: none">Username already exist</span>
                      </div>
                      <div class="col-xs-12 col-sm-6 form-group">
                        {{ Form::input('number', 'to_punctual_rating', $tutee_basic_preference_data['to_punctual_rating'], ['class' => 'form-control', 'placeholder' => 'To Punctuality Rating','id' => 'to_punctual_rating']) }}
                        <span class="text-right display-block has-error-text to_punctual_rating_error to_punctual_rating_error_value" style="display: none">Username already exist</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>Tutees Rating</label>
                    <div class="row">
                      <div class="col-xs-12 col-sm-6 form-group">
                       {{ Form::input('number', 'from_rating', $tutee_basic_preference_data['from_rating'], ['class' => 'form-control', 'placeholder' => 'From Tutees Rating','id' => 'from_rating']) }}
                       <span class="text-right display-block has-error-text from_rating_error from_rating_error_value" style="display: none">Username already exist</span>
                     </div>
                     <div class="col-xs-12 col-sm-6 form-group">
                      {{ Form::input('number', 'to_rating', $tutee_basic_preference_data['to_rating'], ['class' => 'form-control', 'placeholder' => 'To Tutees Rating','id' => 'to_rating']) }}
                      <span class="text-right display-block has-error-text to_rating_error to_rating_error_value" style="display: none">Username already exist</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input type="hidden" name="hidden_id" value="{{$tutee_basic_preference_data['id']}}" id="hidden_id" />
            {!! Form::submit('SAVE', ['class' => 'tuteme_preference_save_hidden hidden']) !!}
            {!! Form::close() !!}
            <div class="with-border"></div>
            <h3 class="color-title">Select the curriculum for which you would like to take sessions for </h3>

            {!! Form::open(['url' => '', 'class' => '','onsubmit'=>'return false','id' => 'tutee_session_preference']) !!}
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <div class="row preferences-section-form">
              <div class="col-xs-12 col-sm-12 col-md-6 select-accordion-wrapper add-curriculum-wrapper mrg-0">
                <div class="form-group">
                  <label>Select Curriculum</label>
                          <!-- <select class="form-control select-curriculum">
                            <option>Select</option>
                            <option value="lorem">Lorem</option>
                          </select> -->
                          {!! Form::select('curriculum_id_payment', ['' => 'Select'] + $curriculum  ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control select-curriculum' ,'required'=>'required' ,'id' => 'curriculum_id_payment','onchange'=>'getCurriculumHierarchyForPayment("curriculum_id")']) !!}
                          <div class="col-xs-12 select-accordion select-accordion-space show">
                            <div class="row" id="custom_dropdowns_payment">
                            </div>
                            <div class="add-curriculum">
                              <!-- <a href="javascript:void(0);" class="text-center" title="Add">Add</a> -->
                              {!! Form::submit('Add', ['class' => 'text-center tutee_curriculumn_preference']) !!}
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-12 col-md-6 ">

                        <div class="form-group">
                          <label>Added Curriculum</label>
                          <span class="total-curriculum">Total <?php echo count($tutee_session_preference_data) ?></span>
                          <div class="curriculum-list-wrapper perfect-scroll added-curriculum-record">

                            @foreach($tutee_session_preference_data as $key=>$value)

                            <div class="curriculum-list_<?php echo $value['id']; ?>" clearfix">
                              <div class="curriculum-record-wrapper">
                                <div class="curriculum-record">
                                  <dl class="dl-horizontal">
                                    <dt>Curriculum:</dt>
                                    <b>
                                      @if(isset($value['topic_id']) && $value['topic_id'] != '' && $value['topic_id'] != 0)
                                      {{getCurriculumHierarchy('',$value['topic_id'],true)}}

                                      @elseif(isset($value['subject_id']) && $value['subject_id'] != '' && $value['subject_id'] != 0)
                                      {{getCurriculumHierarchy($value['subject_id'],'',true)}} 

                                      @endif
                                    </b>
                                  </dl>
                                </div>
                                <div class="curriculum-record">
                                  <dl class="dl-horizontal dl-inline-block">
                                    <dt>Subject:</dt>
                                    <dd><b><?php echo isset($value['subject_name']) && $value['subject_name'] != '' ? $value['subject_name'] : 'N/A' ?></b></dd>
                                  </dl>
                                  <dl class="dl-horizontal dl-inline-block">
                                    <dt>Topic:</dt>
                                    <dd><b><?php echo isset($value['topic_name']) && $value['topic_name'] != '' ? $value['topic_name'] : 'N/A'; ?> </b></dd>
                                  </dl>
                                </div>
                              </div>
                              <div class="curriculum-record-delete">
                                <a href="javascript:void(0);" onclick="deleteTuteePreferenceCurriculum({{$value['id']}})">
                                  <img src="images/delete-button.png" alt="">
                                </a>
                              </div>
                            </div>

                            @endforeach

                          </div>
                        </div>
                        <span class="text-right display-block has-error-text curriculumn_added_payment_error curriculumn_added_payment_error_value" style="display: none">Pleas add atleat one curriculum</span>
                      </div>
                      
                    </div>

                    <input type="hidden" name="added_curriculum_number" id="added_curriculum_number" value="<?php echo count($tutee_session_preference_data)?>">
                    {!! Form::close() !!}

                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group clearfix">
                          {!! Form::submit('SAVE', ['class' => 'btn login-btn tuteme-md-btn','onclick'=>'submit_form()']) !!}
                          <a href="javascript:void(0);" onclick="loadTuteeDashboard()" title="CANCEL">
                          <button type="submit" class="btn btn-grey tuteme-md-btn"> CANCEL </button>
                          </a>
                        </div>
                      </div>
                    </div>



                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <!-- Right side end form here-->
      </div>
    