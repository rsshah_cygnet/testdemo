<div id="upcomming_session_modal" class="modal fade session-popup-modal" role="dialog">
      <div class="modal-body modal-content modal-dialog">
         <div class="calendar-modal modal-green center-block">
         <div class="row">
           <div class="col-xs-12 col-sm-12">
            <button type="button" class="close modal-close-btn" data-dismiss="modal">&times;</button>
          </div>
        </div>
            <!-- <span class="calendar-arrow"></span> -->
              <div class="row">
                   <div class="col-sm-7">
                      <h2>{{$session['0']['first_name']." ".$session['0']['last_name']}}</h2>
                      <p>{{$session['0']['qualification_name']}}</p>
                  </div>
                   <div class="col-sm-5 calendar-schedule-right">
                          <ul class="schedule-list list-unstyled list-inline">
                              <li>
                                  <div><i class="fa fa-calendar"></i>{{ConverTimeZoneForUser($session['0']['scheduled_date_time'],'d-m-Y')}}</div>
                              </li>
                              <li>
                                  <div><i class="fa fa-clock-o"></i>{{ConverTimeZoneForUser($session['0']['scheduled_date_time'],'h:i a')}}</div>
                              </li>
                          </ul>
                  </div>
              </div>
              <div>
      <?php 
        if(isset($session['0']['topic_id']) && $session['0']['topic_id'] != 0 && $session['0']['topic_id'] != ""){ 
          $curriculumHierarchy = getCurriculumHierarchy('',$session['0']['topic_id'],true); 
        }else{ 
             $curriculumHierarchy = getCurriculumHierarchy($session['0']['subject_id'],'',true);          
        }
      ?>
                  <p>Session : <b>{{$curriculumHierarchy}}</b></p>
              
              </div>
             <div class="ratings-block">
                  <label>Punctuality Ratings</label><span>{{$session['0']['tutor_punctuality']}}<em>/10</em></span></div>
              <div>
              <?php if(!empty($session['0']['tutor_ratings'])){ ?>    
            <div id="start_success">
             <div class="star-rating-block">
                  <label>Rating</label>     
                  <div class="ratings-star-inner"><input type="number" value="{{$session['0']['tutor_ratings']}}" name="your_awesome_parameter" id="rating-empty-clearable" class="rating" readonly/></div>
                 <span>({{$session['total_tutor_ratings']}})</span>
             </div>        
          </div>
          <?php } ?>
              <div class="btn-block text-center">
                   <a href="javascript:void(0);" class="btn btn-session tutee-join-session tutee-join-session-<?php echo $session['0']['id'] ?>" onclick="tuteeJoinSession('<?php echo encrypt($session['0']['id'])?>')">Join Session</a>    
                   <a href="javascript:void(0);" class="btn btn-cancel" id="cancelSession"><i class="fa fa-close"></i> Cancel Session</a>
                      <input type="hidden" id="session-id" name="sessionId" value="{{$session['0']['id']}}">        

              </div>
              </div>
        </div>
  </div>