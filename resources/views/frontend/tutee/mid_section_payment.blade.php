    <div class="container-fluid">
      <!-- Left aside start from here -->
       @include('frontend.includes.tutee-leftsidebar')
      <!-- Left aside end from here -->
      <!-- Right side start form here-->
      <div class="content-wrapper">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12">
                                <div class="box">
                                    <div class="box-header clearfix with-border payment-header">
                                    <input type="hidden" name="is_tutee_payment" id="is_tutee_payment" value="tutee_payment">
                                        <div class="pull-left box-title session-title">
                                            <h3>My Payment</h3>
                                        </div>
                                        <div class="payment-date col-xs-12 col-sm-8 col-md-6 col-lg-5 ">
                                            <label class="label-top-spacing col-sm-4 col-md-4 col-lg-4 text-right">Select Date</label>
                                            <div class="datetime-range">
                                              <div class="input-group">
                                                <div class="input-group-addon"> <i class="icon icon-calendar"></i> </div>
                                                    {{Form::text('date','value', ['class' => "form-control pull-right", 'id' => "payment-date", 'name' => 'datefilter'])}}
                                                   {{-- <input type="text" class="form-control pull-right" id="payment-date">% --}}
                                                  </div>
                                               </div>
                                        </div>

                                    </div>
                                    <div class="box-body">
                                        <div class="payment-tabs">
                                            <ul class="nav nav-tabs" id="PaymentTab">
                                                <li class="active" id="tab1"><a data-press="tab" href="#home">Successful Session</a></li>
                                                <li class="cancel-tutee" id="tab2"><a data-press="tab" href="#menu1">Session cancelled by Me</a></li>
                                                <li class="cancel-tutor" id="tab3"><a data-press="tab" href="#menu2">Session cancelled by Tutor</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="home" class="tab-pane fade in active">
                                                @foreach($getPayments as $record)
                                                    <div class="payment-box">
                                                    <div class="payment-top">
                                                        <div class="row send-card-requests pad-0">
                                                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                                                                        <div class="">
                                                                                <h2>{{$record->tutor}}</h2>
                                                                                <p>{{$record->qualification_name}}</p>
                                                                            </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 my-payment-pera payment-user-info">
                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b pull-left">Curriculum</p>
                                                                            <span class="table-cell text-left"><strong>
                          <?php
if (isset($record['topic_id']) && $record['topic_id'] != 0 && $record['topic_id'] != "") {
	echo getCurriculumHierarchy('', $record['topic_id'], true); //for topic
} else {
	echo getCurriculumHierarchy($record['subject_id'], '', true); //for subject
}
?>

                                                                            </strong></span>
                                                                        </div>
                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b">Subject</p>
                                                                                <span><strong>{{$record->subject_name}}</strong></span>
                                                                            </div>
                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b">Topic</p>
                                                                                <span><strong>{{$record->topic_name}}</strong></span>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-4 col-md-offset-0 col-lg-5">
                                                                <div class="amount-block">
                                                                    <div class="session-fee-rate pad-0">
                                                                            <span>Session Fee<strong>${{$record->session_fee}}</strong></span>
                                                                         </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="payment-bottom">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="schedule-list list-unstyled list-inline">
                                                                        <li>
                                                                            <label>Scheduled on :</label>
    <div><i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->scheduled_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->scheduled_date_time)}} --}}
    </div>
    <div><i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->scheduled_date_time, 'H:i'))}}
    {{-- {{getOnlyTime($record->scheduled_date_time)}} --}}
    </div>
                                                                        </li>
                                                                        <li>
                                                                            <label>Accepted on :</label>
    <div><i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->accepted_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->accepted_date_time)}} --}}
    </div>
    <div><i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->accepted_date_time, 'H:i'))}}
    {{-- {{getOnlyTime($record->accepted_date_time)}} --}}
    </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
	                                                <div id="tab1-pagination">
	                                                @if($getPayments->total() > 10)
			                                            {{$getPayments->links()}}
			                                        @endif
                                                    {{$getPayments->total()}}
			                                        </div>
                                                </div>

                                                <div id="menu1" class="tab-pane fade cancel-tutee-tab">
                                                  @foreach($cancelSessionsByTutee as $record)
                                                    <div class="payment-box">
                                                    <div class="payment-top">
                                                        <div class="row send-card-requests pad-0">
                                                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                                                                        <div class="">
                                                                                <h2>{{$record->tutor}}</h2>
                                                                                <p>{{$record->qualification_name}}</p>
                                                                            </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 my-payment-pera payment-user-info">
                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b pull-left">Curriculum</p>
                                                                            <span class="table-cell text-left"><b>
                          <?php
if (isset($record['topic_id']) && $record['topic_id'] != 0 && $record['topic_id'] != "") {
	echo getCurriculumHierarchy('', $record['topic_id'], true); //for topic
} else {
	echo getCurriculumHierarchy($record['subject_id'], '', true); //for subject
}
?>
                                                                              </b></span>
                                                                        </div>
                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b">Subject</p>
                                                                            <span><b>{{$record->subject_name}}</b></span>
                                                                                </div>
                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b">Topic</p>
                                                                            <span><b>{{$record->topic_name}}</b></span>
                                                                                    </div>
                                                                        <div>
                                                                            <p class="pull-left"> Session cancelled on : </p>
                                                                            <div class="session-date-time session-f-size">
    <span> <i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->cancelled_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->cancelled_date_time)}} --}}
     </span>
    <span> <i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->cancelled_date_time, 'H:i'))}}
        {{-- {{getOnlyTime($record->cancelled_date_time)}} --}}
    </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-5">
                                                                <div class="amount-block">
                                                                    <div class="session-fee-rate penalty-amount amount-credit">
                                                                            <span>Penalty Amount<strong>${{$record->penalty_amount}}</strong></span>
                                                                        </div>
                                                                    <div class="session-fee-rate amount-refund">
                                                                            <span>Amount Refunded<strong>${{$record->refund_amount}}</strong></span>
                                                                       </div>
                                                                    <div class="session-fee-rate">
                                                                            <span>Session Fee<strong>${{$record->session_fee}}</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="payment-bottom">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="schedule-list list-unstyled list-inline">
                                                                        <li>
                                                                            <label>Scheduled on :</label>
    <div><i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->scheduled_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->scheduled_date_time)}} --}}
    </div>
    <div><i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->scheduled_date_time, 'H:i'))}}
    {{-- {{getOnlyTime($record->scheduled_date_time)}} --}}
    </div>
                                                                        </li>
                                                                        <li>
                                                                            <label>Accepted on :</label>
    <div><i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->accepted_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->accepted_date_time)}} --}}
    </div>
    <div><i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->accepted_date_time, 'H:i'))}}
    {{-- {{getOnlyTime($record->accepted_date_time)}} --}}
    </div>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="amount-refunded-right">
                                                                         <ul class="schedule-list list-unstyled list-inline">
                                                                        <li>
                                                                            <label>Amount refunded on :</label>
    <div><i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->tutee_refund_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->tutee_refund_date_time)}} --}}
    </div>
    <div><i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->tutee_refund_date_time, 'H:i'))}}
    {{-- {{getOnlyTime($record->tutee_refund_date_time)}} --}}
    </div>
                                                                        </li>
                                                                    </ul>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                {{-- {{ $cancelSessionsByTutee->links() }} --}}
                                                <div id="tab2-pagination">
	                                                @if($cancelSessionsByTutee->total() > 10)
			                                            {{$cancelSessionsByTutee->links()}}
			                                        @endif
                                                    {{$cancelSessionsByTutee->total()}}
			                                        </div>
                                                </div>
                                                <div id="menu2" class="tab-pane fade cancel-tutor-tab">
                                                     @foreach($cancelSessionByTutor as $record)
                                                     <div class="payment-box">
                                                    <div class="payment-top">
                                                        <div class="row send-card-requests pad-0">
                                                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                                                                        <div class="">
                                                                                <h2>{{$record->tutor}}</h2>
                                                                                <p>{{$record->qualification_name}}</p>
                                                                            </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 my-payment-pera payment-user-info">
                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b pull-left">Curriculum</p>
                                                                            <span class="table-cell text-left"><b>
                          <?php
if (isset($record['topic_id']) && $record['topic_id'] != 0 && $record['topic_id'] != "") {
	echo getCurriculumHierarchy('', $record['topic_id'], true); //for topic
} else {
	echo getCurriculumHierarchy($record['subject_id'], '', true); //for subject
}
?></b></span>
                                                                        </div>

                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b">Subject</p>
                                                                             <span><b>{{$record->subject_name}}</b></span>
                                                                        </div>
                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b">Topic</p>
                                                                            <span><b>{{$record->topic_name}}</b>
                                                                        </div>
                                                                        <div>
                                                                            <p class="pull-left"> Session cancelled on : </p>
                                                                            <div class="session-date-time session-f-size">
                        <span> <i class="icon icon-calendar-fill"></i>
                        {{ConverTimeZoneForUser($record->cancelled_date_time, 'd-m-Y')}}
                        {{-- {{getOnlyDate($record->cancelled_date_time)}} --}}
                         </span>
                        <span> <i class="icon icon-clock"></i>
                        {{getOnlyTime(ConverTimeZoneForUser($record->cancelled_date_time, 'H:i'))}}
                        {{-- {{getOnlyTime($record->cancelled_date_time)}} --}}
                        </span>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-4 col-md-offset-0 col-lg-5">
                                                                    <div class="amount-block">
                                                                        <div class="session-fee-rate pad-0">
                                                                            <span>Session Fee<strong>${{$record->session_fee}}</strong></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="payment-bottom">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="schedule-list list-unstyled list-inline">
                                                                        <li>
                                                                            <label>Scheduled on :</label>
                    <div>
                    <i class="icon icon-calendar-fill">
                    </i>
                    {{ConverTimeZoneForUser($record->scheduled_date_time, 'd-m-Y')}}
                    {{-- {{getOnlyDate($record->scheduled_date_time)}} --}}
                    </div>
                    <div><i class="icon icon-clock"></i>
                    {{getOnlyTime(ConverTimeZoneForUser($record->scheduled_date_time, 'H:i'))}}
                    {{-- {{getOnlyTime($record->scheduled_date_time)}} --}}
                    </div>
                                                                        </li>
                                                                        <li>
                                                                            <label>Accepted on :</label>
    <div><i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->accepted_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->accepted_date_time)}} --}}
    </div>
    <div>
    <i class="icon icon-clock"></i>
        {{getOnlyTime(ConverTimeZoneForUser($record->accepted_date_time, 'H:i'))}}
    </div>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="amount-refunded-right">
                                                                         <ul class="schedule-list list-unstyled list-inline">
                                                                        <li>
                                                                            <label>Fees refunded on:</label>
<div><i class="icon icon-calendar-fill"></i>
{{ConverTimeZoneForUser($record->tutee_refund_date_time, 'd-m-Y')}}
{{-- {{getOnlyDate($record->tutee_refund_date_time)}} --}}
</div>
<div><i class="icon icon-clock"></i>
{{getOnlyTime(ConverTimeZoneForUser($record->tutee_refund_date_time, 'H:i'))}}
{{-- {{getOnlyTime($record->tutee_refund_date_time)}} --}}
</div>
                                                                        </li>
                                                                    </ul>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                    {{-- {{ $cancelSessionByTutor->links() }} --}}
													   <div id="tab3-pagination">
                                               		 	@if($cancelSessionByTutor->total() > 10)
		                                            		{{$cancelSessionByTutor->links()}}
		                                        		@endif
		                                        		</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      <!-- Right side end form here-->
      </div>
