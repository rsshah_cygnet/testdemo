    <div class="container-fluid">
      <!-- Left aside start from here -->
        @include('frontend.includes.tutee-leftsidebar')
      <!-- Left aside end from here -->
      <!-- Right side start form here-->
      <div class="content-wrapper">
        <div class="content">
          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="box">
                <div class="box-header clearfix with-border">
                  <div class="box-left-title box-title session-title">

                
                  @if(Session::has('ExamInconvienceMessage'))
                  <div class="alert alert-danger">
                    {{Session::get('ExamInconvienceMessage')}}
                  </div>
                  {{\Session::forget('ExamInconvienceMessage')}}
                  @endif

                  @if(\Session::has('sessionExpired'))
                  <div class="alert alert-danger">
                    {{\Session::get('sessionExpired')}}
                  </div>
                  {{\Session::forget('sessionExpired')}}
                  @endif
                  @if(\Session::has('becomeTutorExamLinkMessage'))
                  <div class="alert alert-success">
                    {{\Session::get('becomeTutorExamLinkMessage')}}
                  </div>
                  {{\Session::forget('becomeTutorExamLinkMessage')}}
                  @endif
                    <h3>Upcoming Sessions</h3>
                  </div>
                  <div class="box-right-title box-title-link view-all-session"> <a href="javascript:void(0);" onclick="loadTuteeMysessions()" class="color-title" title="View all">View All Sessions</a> </div>
                </div>
                <div class="box-body">
                  <ul class="upcomming-sessions">


                <?php if (!empty($upcommingSessions)) {
    foreach ($upcommingSessions as $key => $value) {
        ?>

                    <li class="clearfix">
                      <div class="session-detail">
                        <h3 class="color-title">{{$value['first_name']." ".$value['last_name']}}</h3>

                        <div class="session-date-time"> <span><i class="fa fa-calendar"></i> {{ConverTimeZoneForUser($value['scheduled_date_time'],'d-m-Y')}} </span> <span><i class="fa fa-clock-o"></i> {{ConverTimeZoneForUser($value['scheduled_date_time'],'h:i a')}} </span> </div>
                        <div class="session-subject">
                             <?php
if (isset($value['topic_id']) && $value['topic_id'] != 0 && $value['topic_id'] != "") {
            echo getCurriculumHierarchy('', $value['topic_id'], true); //for topic
        } else {
            echo getCurriculumHierarchy($value['subject_id'], '', true); //for subject
        }
        ?>
                        </div>
                      </div>
                      <div class="join-session"> <a href="javascript:void(0);" onclick="tuteeJoinSession('<?php echo encrypt($value['id']) ?>')" class="tutee-dashboard-btn tutee-join-session tutee-join-session-<?php echo $value['id'] ?>">JOIN SESSION</a> </div>
                    </li>

                    <?php }
}else{
?>
  <p>There are No Upcoming Sessions for you.</p>
<?php } ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="box">
                <div class="box-header clearfix with-border">
                  <div class="box-left-title box-title">
                    <h3 class="">Session Details</h3>
                  </div>
                  <div class="box-right-title session-detail-month">
                    <!-- <label> Session Details</label> -->
                      <select name="month" id="month_piechart">
                        <option value="">Select Month</option>
                        <option value="01">January</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                  </div>
                   <div class="box-right-title session-detail-month">

                     <select name="year" id="year_piechart">
                        <option value="">Select Year</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                    </select>
                  </div>
                </div>
                 <p id="piechart_error" style="display: none;color: red;"></p>
                <div class="box-body" id="pie_chart_portion"> <div id="piechart_3d" style="height: 320px;width: 100%;"></div> </div>
              </div>
            </div>
            <div class="col-xs-12 col-lg-12">
              <div class="box suggested-tutor-box" style="overflow:hidden;">
                <div class="box-header clearfix ">
                  <div class="box-left-title box-title session-title">
                    <h3>Suggested Tutors</h3>
                  </div>
                  <div class="box-right-title box-title-link view-all-session"> <a href="javascript:void(0);" onclick="loadFindTutor()" class="color-title" title="View all">View all</a> </div>
                </div>
                <div class="box-body clearfix">
                  <div class="suggested-tutor-list-wrapper">
                    <div class="suggested-tutor-scroll">
                  <?php
if (!empty($tutors)) {
    foreach ($tutors as $key => $value) {
        $img_path = url('uploads/user/' . $value['photo']);
        if ($value['photo'] != "") {
            $img_path = $img_path;
        } else {
            $img_path = url('images/default-user.png');
        }
        $fullName = $value['first_name'] . " " . $value['last_name'];
        //  $tutor_avg_ratings = getTutorAvgRatings($value['userId']);
        $total_tutor_ratings = getTotalTutorRatings($value['userId']);

        $tutor_url = url('tutorDetail/' . $value['userId']);
        $tutor_id = $value['userId'];
        ?>

                            <div class="suggested-tutor-list">
                        <div class="suggested-tutor-image"> <img src="{{$img_path}}" alt="{{$fullName}}" title="{{$fullName}}" class="round-120" />
                          <div class="pancutal-rating-wrapper new-panctual-rating" title="Punctuality Rating">
                            <span class="pancutal-rating-outof">{{$value['punctuality'] or 0 }}/10</span>
                             </div>
                             <div class="panctual-timings">
                            <i class="fa fa-clock-o"></i>
                           </div>
                        </div>
                        <div class="text-center suggested-tutor-info">
                          <h3><a href="javascript:void(0);" onclick="loadTutorDetails('<?php echo $tutor_id; ?>')">{{$fullName}}</a></h3>
                          <div class="degree">{{$value['qualification_name']}}</div>
                          <div class="suggested-tutor-rating">
                            <input type="number" name="your_awesome_parameter" id="rating-empty-clearable" value="{{$value['rating']}}" class="rating"/>

                            <span class="tutor-total-rating">({{$total_tutor_ratings}})</span> </div>
                          <dl>
                            <dt>Languages</dt>
                            <!-- <dd>{{$value['language']}}</dd> -->
                            <dd>{{getLanguageBasedOnLanguageIdString($value['lang_id'])}}</dd>
                          </dl>
                          <dl>
                            <dt>Subjects</dt>
                            <dd>
                            <?php
if (!empty($value['subjects'])) {
            $subject = explode(',', $value['subjects']);
            if (!empty($subject)) {
                foreach ($subject as $key => $value) {
                    $subject_name        = explode(':', $value);
                    $subject_curriculumn = getCurriculumHierarchy($subject_name['0'], '', true);
                    if ($key > 0) {
                        $subject_name_final = "," . $subject_name['1'];
                    } else {
                        $subject_name_final = $subject_name['1'];
                    }
                    ?>
                                          <span data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$subject_curriculumn}}">{{$subject_name_final}}</span>
                          <?php }
            }

        }

        ?>

                          </dl>
                          <div class="suggested-tutor-review"> <a href="javascript:void(0);" onclick="loadTutorDetails('<?php echo $tutor_id; ?>')" class="tutee-dashboard-btn" title="View All Reviews">View All Reviews</a> </div>
                        </div>
                      </div>
                  <?php }
}

?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
      <!-- Right side end form here-->
    </div>
  @section('middle-scripts')
      
      <script type="text/javascript">
       
       google.charts.load('current', {packages: ['corechart', 'bar']});
       google.charts.setOnLoadCallback(drawChart);
       var record={!! json_encode($user) !!}
      function drawChart() {

        //var record={!! json_encode($user) !!}; 
  
         var data = new google.visualization.DataTable();
         data.addColumn('string', 'Session Type');
         data.addColumn('number', 'Total');
         for(var k in record){
              var v = record[k];

               data.addRow([k,v]);
               //console.log(v);
            }
         var options = {
            title: '',
            is3D: false,
            width: '90%',
            height: '90%',
            chartArea: {
                left: "3%",
                top: "3%",
                height: "80%",
                width: "80%"
            },
            /*legend:{position: 'top'},
*/
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
          chart.draw(data, options);
        }



        $(document).ready(function(){
          $('.rating-input').unbind('mouseenter mouseleave click');
          checkTuteeJoinSession(<?php echo \Session::get('front_user_id') ?>);

         setInterval("checkTuteeJoinSession(<?php echo \Session::get('front_user_id') ?>)",60000);
        })
      </script>
  @stop


