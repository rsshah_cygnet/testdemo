
  <div class="container-fluid">
    <!-- Left aside start from here -->
    @include('frontend.includes.tutee-leftsidebar')
    <!-- Left aside end from here -->
    <!-- Right side start form here-->
    <div class="content-wrapper">
      <div class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-header clearfix with-border">
                @include('includes.partials.messages')
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message') }}</p>
                @endif
                @if(Session::has('successmessage'))
                <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('successmessage') }}</p>
                @endif
                <div id="Msg"></div>
                <div class="pull-left box-title">
                  <h3 class="">Session Request</h3>
                </div>
              </div>
              <div class="box-body">
                <h2 class="color-title heading-f-s-20">Sent Requests</h2>

                @foreach($sessionRequest as $request)

                <div class="row send-card-requests">

                  <div class="col-xs-12 col-sm-2 col-md-1">
                    <div class="img-round">

                      <?php
                      if (!empty($request->photo)) {
                       $img_path = 'uploads/user/' . $request->photo;
                       if (file_exists($img_path)) {
                        $img_path = $img_path;
                      } else {
                        $img_path = url('images/default-user.png');
                      }
                    } else {
                     $img_path = url('images/default-user.png');

                   }
                   ?>
                   {{ Html::image($img_path, $request->tutor, ['class'=>"user-image round-60"]) }}


                 </div>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-3 p-l-section">
                <h3 class="heading-f-s-20">{{$request->tutor}}</h3>

                <div>
                  <?php

                  if (isset($request->tutor_id) && $request->tutor_id != "" && $request->tutor_id != 0) {
                   $tutor_id = $request->tutor_id;} else {

                     $tutor_id = $sessionData['front_user_id'];}
                     $total_tutor_ratings = \App\Models\TutorRatings\TutorRatings::where('tutor_id', $tutor_id)->count();
                     ?>
                     <p class="pull-left">Ratings </p>
                     <div class="suggested-tutor-rating mrg-0 subject-section d-i-b">
                      <input type="number" name="your_awesome_parameter" id="rating-empty-clearable" class="rating pull-left" value="{{$request->rating}}"/>
                      <p class="tutor-total-rating">({{$total_tutor_ratings}})</p>
                    </div>
                  </div>
                  <div>
                    <p class="punctual-reating">Punctuality Ratings</p>
                    <div class="punctual-rating-block small-rating pull-left"> <strong class="out-of"><span>{{$request->punctuality}}</span></strong>
                      <storng class="out-small">/ 10</storng>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-5">
                  <div class="subject-section">
                    <p class="d-i-b">Curriculum</p>
                    <span><b>
                      <?php
                      if (isset($request['topic_id']) && $request['topic_id'] != 0 && $request['topic_id'] != "") {
	echo getCurriculumHierarchy('', $request['topic_id'], true); //for topic
} else {
	echo getCurriculumHierarchy($request['subject_id'], '', true); //for subject
}
?>
</b></span>
</div>
<div class="subject-section">
  <p class="d-i-b">Subject</p>
  <span><b>{{$request->subject_name}}</b></span>
</div>
@if(!empty($request->topic_name))
<div class="subject-section">
  <p class="d-i-b">Topic</p>
  <span><b>{{$request->topic_name}}</b></span>
</div>
@endif
<div class="objective-heading">
  <p class="d-i-b mrg-0 pull-left">Objective</p>
  <span class="table-cell text-left"><b>{{$request->objective}}</b></span>
</div>
<div>
  <p class="pull-left">Request sent on :</p>
  <div class="session-date-time session-f-size">
    <span><i class="icon icon-calendar-fill"></i>
      {{ConverTimeZoneForUser($request->reserved_date_time, 'd-m-Y')}}
      {{-- {{getOnlyDate($request->created_at)}}  --}}
    </span>
    <span><i class="icon icon-clock"></i>
      {{getOnlyTime(ConverTimeZoneForUser($request->reserved_date_time, 'H:i'))}}
      {{-- {{getOnlyTime($request->created_at)}}  --}}
    </span>
  </div>
</div>
<div>
  <p class="pull-left">Session Date and Time :</p>
  <div class="session-date-time session-f-size">
    <span><i class="icon icon-calendar-fill"></i>
      {{ConverTimeZoneForUser($request->scheduled_date_time, 'd-m-Y')}}
      {{-- {{getOnlyDate($request->created_at)}}  --}}
    </span>
    <span><i class="icon icon-clock"></i>
      {{getOnlyTime(ConverTimeZoneForUser($request->scheduled_date_time, 'H:i'))}}
      {{-- {{getOnlyTime($request->created_at)}}  --}}
    </span>
  </div>
</div>
</div>
<div class="col-xs-12 col-sm-6 pull-right col-md-3">
  <a href="#myModal"   id="decline-request" id2="{{$request->id}}" class="tutee-dashboard-btn btn-small-pad decline-btn-tutor">CANCEL REQUEST</a>
</div>
</div>

<div class="with-border"></div>
@endforeach
</div>
{{ $sessionRequest->render() }}
</div>
</div>
</div>
</div>
</div>
<!-- Right side end form here-->
</div>
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content modal-content-box">
      <div id="model-body" class="modal-body text-center modal-body-section">

      </div>
    </div>
  </div>
</div>

