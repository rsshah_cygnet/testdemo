
      {{Form::open(['route'=>array('decline.tutor.session.request', $id),'onclick' => 'return false' ])}}
          <h2>Are you sure,</h2>
          <p>You want to cancel this request ?</p>
          <input type="hidden" id="cancelled_session_id" value="{{$id}}" name="cancelled_session_id">
          <div class="form-group clearfix">
            <button type="submit" id="cancel_request_tutee" class="btn login-btn btn-small-pad">Yes</button>
            <button type="submit" class="btn btn-grey btn-small-pad hide_modal" data-dismiss="modal">No</button>
          </div>
        {{Form::close()}}
