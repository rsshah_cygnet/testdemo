<div class="detail-column col-lg-5 col-md-5 col-sm-12 col-xs-12 hidden-sm-xs" data-mcs-theme="minimal-dark">
    <div class="car-detail">
        <div class="top-slider m-b-15">
            <div class="owl-carousel CarSlider" id="CarSlider">
                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car" />
                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car" />
                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car" />
            </div>
            <div class="slide-num"></div>
            <a href="javascript:void(0);" class="car-detail-close"><img src="{{ url('/') }}/images/close-detail-white.png" alt="Car Detail Close"></a>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 get-slug">
                    <h2 class="{!! $vehicle->vehicle_slug !!}">{{ $vehicle->brands->brand_name }} {{ $vehicle->brandModels->brand_model_name }}<br>{{ $vehicle->derivatives->derivative_name }}</h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <ul class="car-detail-links text-right">
                        <li>
                            <a href="register-for-favourites.html"><img src="{{ url('/') }}/images/icon-favourite-detail.png" alt="Add to Favourites"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-compare-detail.png" alt="Compare"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-share-detail.png" alt="Share"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 gray-right-border car-type-list-row">
                    <ul class="car-type-list">
                        <li><img src="{{ url('/') }}/images/icon-hatchback-detail.png" alt="Icon Hatchback">
                            <p>Hatchback</p>
                        </li><li><img src="{{ url('/') }}/images/icon-manual-detail.png" alt="Icon Manual">
                            <p>Manual</p>
                        </li><li><img src="{{ url('/') }}/images/icon-petrol-detail.png" alt="Icon Petrol">
                            <p>Petrol</p>
                        </li><li><img src="{{ url('/') }}/images/icon-average-detail.png" alt="Icon Average">
                            <p>6,3 L/100 km</p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h2 class="car-price"><span class="car-installment d-b">R 2,326 <span class="installment-type">monthly<sup>*</sup></span></span>R 129,995</h2>
                    <a class="btn btn-repayment btn-sm d-b" href="javascript:void(0);">Calculate repayment</a>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="gray-divider"></div>
                    <p>The all-new Picanto blends modern and distinctive exterior styling with a fresh, trendy interior. Combining the very highest standards of engineering with European aesthetics in every aspect of its exterior, interior...<a href="#">more</a></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center m-t-15">
                    <a href="javascript:void(0);" class="btn btn-green" data-toggle="modal" data-target="#EnquireForm">enquire</a>
                </div>
                <div class="col-xs-12 car-option">
                    <div class="owl-thumbs owl-carousel CarThumbSlider">
                        <button class="owl-thumb-item"><span class="car-color-image"><img src="{{ url('/') }}/images/white-car-thumb.jpg" alt="White Car thumb"></span><span>Signle white</span></button>
                        <button class="owl-thumb-item"><span class="car-color-image"><img src="{{ url('/') }}/images/black-car-thumb.jpg" alt="Black Car thumb"></span><span>Gray Silver</span></button>
                        <button class="owl-thumb-item"><span class="car-color-image"><img src="{{ url('/') }}/images/red-car-thumb.jpg" alt="Red Car thumb"></span><span>Signle red</span></button>
                        <button class="owl-thumb-item"><span class="car-color-image"><img src="{{ url('/') }}/images/yellow-car-thumb.jpg" alt="Yellow Car thumb"></span><span>Signle Yellow</span></button>
                        <button class="owl-thumb-item"><span class="car-color-image"><img src="{{ url('/') }}/images/lemon-yellow-car-thumb.jpg" alt="Lemon Yellow Car thumb"></span><span>Lemon Yellow</span></button>
                    </div>
                    <div class="owl-carousel CarColorSlider">
                        <div class="item"><img src="{{ url('/') }}/images/car-white.jpg" alt="White Car"></div>
                        <div class="item"><img src="{{ url('/') }}/images/car-gray-silver.jpg" alt="Gary Silver Car"></div>
                        <div class="item"><img src="{{ url('/') }}/images/car-red.jpg" alt="Red Car"></div>
                        <div class="item"><img src="{{ url('/') }}/images/car-yellow.jpg" alt="Yellow Car"></div>
                        <div class="item"><img src="{{ url('/') }}/images/car-lemon-yellow.jpg" alt="New Car"></div>
                    </div>
                </div>
                <div class="col-xs-12 car-interior">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a data-toggle="tab" href="#interior">interior</a></li>
                        <li><a data-toggle="tab" href="#exterior">exterior</a></li>
                        <li><a data-toggle="tab" href="#safety">safety</a></li>
                        <li><a data-toggle="tab" href="#specs">specs</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="interior" class="tab-pane active">
                            <h2><span>SOPHISTICATED</span> INTERIOR</h2>
                            <p>Picanto's new found maturity is also reflected in the interior design, which sets a new benchmark in its class. The sophisticated interior will offer buyers numerous 'big car' comfort and convenience features - many of them fitted for the first time on a car in this segment. It will set new class standards for material quality that introduces a 'premium' feel offering to this segment.</p>
                            <div class="owl-carousel CarSlider">
                                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car" />
                                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car" />
                                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car" />
                            </div>
                            <h5 class="tab-text">With its distinctive styling and impressive list of features at an affordable price to get you going, the Picanto is truly awesome for everyone. </h5>
                        </div>
                        <div id="exterior" class="tab-pane fade">
                            <h2><span>MATURE &amp; HANDSOME </span> EXTERIOR</h2>
                            <p>Kia has taken a courageous new step with the latest version of its smallest model. The character of the car has been transformed from 'cute' and 'friendly' into mature and handsome, designed to turn heads on the high street, thanks to its good looking proportions and dynamic design language. Picanto in its newest form exudes a bold self-confidence and maturity which is unique in the A-segment. Up front, the face of the Picanto expresses the same assertive vigour and refinement as its bigger brother, the Sportage, and shares with them the same signature 'tiger nose' grille.</p>
                            <div class="owl-carousel CarSlider">
                                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car" />
                                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car" />
                                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car" />
                            </div>
                        </div>
                        <div id="safety" class="tab-pane fade">
                            <h2><span>ACTIVE &amp; PASSIVE</span> safety</h2>
                            <p>Both active and passive safety in the new Picanto has been raised to the next level. The all-new bodyshell created uses a greater percentage of high-strength steel and incorporates ring-shaped reinforcing loops within the B- and C-pillars, and across the floor pan and roof for improved rigidity. Available safety equipment include a driver airbag (1.0 litre) and an additional passenger airbag (1.0 LX and 1.2 EX) with ABS anti-lock braking for the 1.2 EX only. For additional safety, a new ESS (Emergency Stop Signal) system is available on the 1.2 EX. Sensors detect when the driver is braking suddenly and hard, and then flash the brake lights three times to alert following drivers that the car is slowing rapidly. </p>
                            <div class="owl-carousel CarSlider">
                                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car" />
                                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car" />
                                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car" />
                            </div>
                        </div>
                        <div id="specs" class="tab-pane fade">
                            <h2><span>VEHICLE</span> SPECIFICATIONS</h2>
                            <div class="panel-group accordion" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Engine">Engine</a>
                                    </div>
                                    <div id="Engine" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <th scope="row">Bore and Stroke (mm)</th>
                                                    <td>71 x 84.0</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Compression Ratio</th>
                                                    <td>10.5 : 1</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Displacement (cc)</th>
                                                    <td>998</td>

                                                </tr>
                                                <tr>
                                                    <th scope="row">Engine Type</th>
                                                    <td>In-line 3 cylinders 12 Valve DOHC</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Max Power (kW @ rpm)</th>
                                                    <td>51 @ 6 200</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Max Torque (Nm @ rpm)</th>
                                                    <td>94 @ 3 500</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Transmission">Transmission</a>
                                    </div>
                                    <div id="Transmission" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Suspension">
                                            Suspension</a>
                                    </div>
                                    <div id="Suspension" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Fuel">
                                            Fuel</a>
                                    </div>
                                    <div id="Fuel" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Steering">
                                            Steering</a>
                                    </div>
                                    <div id="Steering" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">

                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Brakes">
                                            Brakes</a>

                                    </div>
                                    <div id="Brakes" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">

                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Wheels-Tyres">
                                            Wheels &amp; Tyres</a>

                                    </div>
                                    <div id="Wheels-Tyres" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">

                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Dimensions">
                                            Dimensions</a>

                                    </div>
                                    <div id="Dimensions" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">

                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Features">
                                            Features</a>

                                    </div>
                                    <div id="Features" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">

                                        <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion" href="#Service-Warranty">
                                            Service &amp; Warranty</a>

                                    </div>
                                    <div id="Service-Warranty" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>