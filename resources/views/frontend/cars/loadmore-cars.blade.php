@foreach($vehicles as $vehicle)
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 car-list" id="CarSelect" data-id="{{ $vehicle->id }}" data-vehicle="{{ $vehicle->vehicle_type }}">
        <div class="car-box">
            <div class="car-image">
                <img class="img-responsive" src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
            </div>
            <div class="car-information">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
                        <h2><a href="javascript:void(0);">{{ $vehicle->brands->brand_name }} {{ $vehicle->brandModels->brand_model_name }}<br>{{ $vehicle->derivatives->derivative_name }}</a></h2>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 text-right"><img src="{{ url('/') }}/brand_logo/{{ $vehicle->brands->brand_logo }}" alt="{{ $vehicle->brandModels->brand_model_name }} Logo" title="{{ $vehicle->brandModels->brand_model_name }}"></div>
                </div>
                @if((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'new')
                    <h2 class="car-price"><a href="javascript:void(0);">R {{ number_format($vehicle->selling_price) }}</a><span class="car-installment">R 2,326</span> <span class="installment-type">monthly<sup>*</sup></span></h2>
                    <ul class="car-features">
                        <li>{{ $vehicle->vehicleTypes->vehicle_type }}</li>
                        <li>Manual</li>
                        <li>Petrol</li>
                    </ul>
                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'used')
                    <div class="clearfix">
                        <h2 class="car-price"><a href="javascript:void(0);">R {{ number_format($vehicle->selling_price) }}</a><span class="pull-right"><span class="car-installment">R 3,450</span> <span class="installment-type">p/m<sup>*</sup></span></span></h2>
                    </div>
                    <div class="clearfix car-info-bottom">
                        <div class="car-info-bottom-left">
                            <ul class="car-features">
                                <?php
                                $vehicle->vehicleSpecification->each(function ($spec) {
                                if ($spec->old_specification_id == 1) { ?>
                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                <?php } else if ($spec->old_specification_id == 2) { ?>
                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                <?php } else if ($spec->old_specification_id == 3) { ?>
                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                <?php }
                                });
                                ?>
                            </ul>
                        </div>
                        <button class="btn btn-gp">GP</button>
                    </div>
                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'demo')
                    <div class="clearfix">
                        <h2 class="car-price"><a href="javascript:void(0);">R {{ number_format($vehicle->selling_price) }}</a><span class="pull-right"><span class="car-installment">R 3,450</span> <span class="installment-type">p/m<sup>*</sup></span></span></h2>
                    </div>
                    <div class="clearfix car-info-bottom">
                        <div class="car-info-bottom-left">
                            <ul class="car-features">
                                <?php
                                $vehicle->vehicleSpecification->each(function ($spec) {
                                if ($spec->old_specification_id == 1) { ?>
                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                <?php } else if ($spec->old_specification_id == 2) { ?>
                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                <?php } else if ($spec->old_specification_id == 3) { ?>
                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                <?php }
                                });
                                ?>
                            </ul>
                        </div>
                        <button class="btn btn-gp">GP</button>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endforeach