<!-- Modal content-->

<div class="modal-content">
 <div class="hidden alert model-alert enquiry-model-alert"></div>

    <div class="modal-header">
        <div class="row">
            <div class="col-lg-10 col-md-9 col-sm-11 col-xs-10">
                <h2>vehicle <span>enquiry</span></h2>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-1 col-xs-2 text-right">
                <a href="javascript:void(0);" id="RefineClose" data-dismiss="modal"><img src="{{ url('/') }}/images/close-refine.png" alt="Refine Close"></a>
            </div>
        </div>
    </div>
    <div class="modal-body">
        {!! Form::open(array('route' => 'enquiry-lead.store','id'=>'enquiry-form')) !!}
        <div class="enquire-car">
            <div class="row">
                <div class="col-sm-7"><h3>{{ $vehicle->brands->brand_name }} {{ $vehicle->brandModels->brand_model_name }} {{ $vehicle->derivatives->derivative_name }}
                </h3><div class="modal-car-price">R {{ number_format($vehicle->selling_price) }} <span class="car-installment">R 1,352 p/m*</span></div><ul class="car-features">
                        <li>{{ $vehicle->vehicleTypes->vehicle_type }}</li>
                        <li>Manual</li>
                        <li>Petrol</li>
                    </ul></div>
                <div class="col-sm-5"><img src="{{ url('/') }}/images/car-red.jpg"></div>
            </div>
        </div>
        <div class="enquire-form">
            <h3 class="sub-title">vehicle <span>enquiry</span></h3>
            <form class="row clearfix">
                <div class="form-group col-sm-6">
                    <label>Province</label>
                    <input class="form-control" placeholder="{{@$vehicle->vehicleStock->dealership->province_region->region_name}}" readonly type="text">
                </div>
                <div class="form-group col-sm-6">
                    <label>Dealer</label>
                    <input readonly class="form-control" placeholder="{{@$vehicle->vehicleStock->dealership->dealership_name}}" type="text">
                </div>
            </form>
            <h3 class="sub-title"><span>your</span> details</h3>
            <div class="row clearfix">
                <div class="form-group col-sm-6">
                    {!! Form::label('first_name', "Name", ['class' => 'required']) !!}
                    <div class="error-message" id="first_name_error"></div>
                    {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => "John", 'required' =>' required']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('last_name', "Surname", ['class' => 'required']) !!}
                    <div class="error-message" id="last_name_error"></div>
                    {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => "Smith", 'required' =>' required']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('email', "Email", ['class' => 'required']) !!}
                    <div class="error-message" id="email_error"></div>
                    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => "jonn@smith.com"]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('phone_no', "Cell Number", ['class' => 'required']) !!}
                    <div class="error-message" id="phone_no_error"></div>
                    {!! Form::text('phone_no', null, ['class' => 'form-control', 'placeholder' => "+27721234567"]) !!}
                    {!! Form::hidden('vehicle_id',$vehicle->id) !!}
                </div>
            </div>
            <div class="row clearfix">
                <div class="form-group col-xs-4 custom-checkbox">
                    {!! Form::checkbox('test_drive','1',null,['id'=>'test_drive']) !!}
                    {!! Form::label('test_drive', "Test Drive", ['class' => 'btn btn-default d-b required']) !!}
                </div>
                <div class="form-group col-xs-4 custom-checkbox">
                    {!! Form::checkbox('finance','1',null,['id'=>'finance']) !!}
                    {!! Form::label('finance', "Need Finance", ['class' => 'btn btn-default d-b required']) !!}
                </div>
                <div class="form-group col-xs-4 custom-checkbox">
                     {!! Form::checkbox('trade_in','1',null,['id'=>'trade_in']) !!}
                     {!! Form::label('trade_in', "Have trade in", ['class' => 'btn btn-default d-b required']) !!}
                </div>
                <div class="form-group col-xs-12">
                     {!! Form::submit('Send Enquiry', ['class' => 'btn btn-enquiry', 'id' => 'send-inquiry-btn']) !!} 
                    <!-- <a href="javascript:void(0);" class="btn btn-enquiry">send enquiry</a> -->
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>



<script type="text/javascript">
    
   $( document ).on( 'submit', 'form#enquiry-form', function(event) 
    {
        event.preventDefault();

        var data  = new FormData(this);
        saveModalForm(data);

        return false;
    });

    function saveModalForm(data){
        jQuery.ajax({
            type: "POST",
            url: '{{route("enquiry-lead.store")}}',
            data:data,
            processData: false,
            beforeSend: function(xhr){
                $('div.error-message').html(''); 
                $('#send-inquiry-btn').attr('disabled','disabled');
            },
            contentType: false,
            success: function(data)
            {    
                if(data.status)
                {
                    $( "div.enquiry-model-alert" ).html(data.message).removeClass("danger alert-danger").addClass("success alert-success");
                    $("div.enquiry-model-alert").removeClass("hidden");
                    setTimeout(function(){$("#EnquireForm").modal('hide')},2000);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                $('#send-inquiry-btn').removeAttr('disabled');
                if(errorThrown)
                {
                    errorResponse = JSON.parse(XMLHttpRequest.responseText);
                    $.each(errorResponse,function(key,val){
                        $("#"+key+"_error").html(val);
                    });

                    $("div.enquiry-model-alert").html("Please enter valid input").addClass("danger alert-danger");
                    $("div.enquiry-model-alert").removeClass("hidden");
                }
            }
        });
    }

</script>