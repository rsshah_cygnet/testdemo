@extends('frontend.layouts.master')

@section('page-wrapper')

    <!-- Page Wrapper Start -->
    <div class="page-wrapper">

        <!-- Global Menu Start -->
        @include('frontend.includes.header')
        <!-- Global Menu End -->

        <!-- Middle Content Start -->
        <div class="middle-container">
            <div class="new-car-container">
                <div class="car-detail">
                    <div class="top-slider m-b-15">
                        <div class="owl-carousel DealerCarSlider" id="DealerCarSlider">
                            <div class="item">
                                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                            </div>
                            <div class="item">
                                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car">
                            </div>
                            <div class="item">
                                <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                            </div>
                            <div class="item">
                                <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                <h2 class="{!! $vehicle->vehicle_slug !!}">{!! $vehicle->vehicleStock->model_year !!} {!! $vehicle->vehicleStock->listing_title !!}</h2>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <ul class="car-detail-links text-right">
                                    @if(\Auth::check())
                                    <li>
                                        <a href="javascript:void(0);" class="add-as-favourites" data-favourites="{{ $vehicle->id }}"><img src="{{ url('/') }}/images/icon-favourite-detail.png" alt="Add to Favourites"></a>
                                    </li>
                                    @endif
                                    <li>
                                        <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-compare-detail.png" alt="Compare"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-share-detail.png" alt="Share"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8 col-xs-12 gray-right-border car-type-list-row">
                                <ul class="car-type-list">
                                    <li><img src="{{ url('/') }}/images/icon-hatchback-detail.png" alt="Icon Hatchback">
                                        <p>{{ $carTypeList['vehicleType'] }}</p>
                                    </li><li><img src="{{ url('/') }}/images/icon-manual-detail.png" alt="Icon Manual">
                                        <p>{{ $carTypeList['transmissionType'] }}</p>
                                    </li><li><img src="{{ url('/') }}/images/icon-petrol-detail.png" alt="Icon Petrol">
                                        <p>{{ $carTypeList['fuelType'] }}</p>
                                    </li><li><img src="{{ url('/') }}/images/icon-average-detail.png" alt="Icon Average">
                                        <p>6,3 L/100 km</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-6"><h2 class="car-price"><span class="car-installment d-b">R 3 015.00 <span class="installment-type">monthly<sup>*</sup></span></span>R {{ number_format($vehicle->selling_price) }}</h2></div>
                                    <div class="col-sm-12 col-xs-6"><a class="btn btn-repayment d-b" href="javascript:void(0);">Calculate repayment</a></div>
                                </div>
                            </div>
                            <!--<div class="col-sm-4 col-xs-12 text-center m-t-15 pull-right">
                                <a href="javascript:void(0);" class="btn btn-green" data-toggle="modal" data-target="#EnquireForm">enquire</a>
                            </div>-->
                            <div class="col-sm-8 col-xs-12">
                                <div class="gray-divider mobile-divider-none"></div>
                                <p><span class="description-container">{!! $vehicle->vehicleStock->description !!}</span>  <a href="javascript:void(0);" class="more-description">more</a></p>
                            </div>
                            <div class="col-sm-4 col-xs-12 dealer-detail-info">
                                <div class="dealer-popup-detail">
                                    <p>City: <strong>{{ $vehicle->vehicleStock->dealership->province_region->region_name }}</strong></p>
                                    <p>Dealership: <strong>{{ $vehicle->vehicleStock->dealership->dealership_name }}</strong></p>
                                    <button class="btn btn-blue">Dealer Showroom</button>
                                    <p>Stock Number: <strong>{{ $vehicle->vehicleStock->vehicle_number }}</strong></p>
                                    <p>M&amp;M Code: <strong>{{ $vehicle->vehicleStock->mead_mcgrouther_code }}</strong></p>
                                </div>
                            </div>

                            <div class="col-xs-12 car-interior car-dealer-features">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active"><a data-toggle="tab" href="#features">FEATURES</a></li>
                                </ul>
                                <div class="tab-content clearfix">
                                    <div id="features" class="tab-pane active">
                                        <table class="table table-striped">
                                            <tbody>
                                            <tr>
                                                <?php $i = 1; ?>
                                                @foreach($carTypeList['features'] as $feature)
                                                    <td>{!! $feature !!}</td>
                                                    @if($i % 2 === 0)
                                            </tr><tr>
                                                @endif
                                                <?php $i++; ?>
                                                @endforeach
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="col-xs-12">
                                            <a href="javascript:void(0);" class="btn btn-green m-t-15" data-toggle="modal" data-target="#EnquireForm" onclick="getVehicleDetail({{ $vehicle->id }});">ENQUIRE WITH DEALER</a>
                                            <a href="javascript:void(0);" class="btn btn-default m-t-15 back-top">Back to top</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Car Column End -->
            </div>
        </div>
        <!-- Middle Content End -->

    </div>

    <!-- Enquire modal Start -->
    <div id="EnquireForm" class="modal fade enquiry-form" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-lg-10 col-md-9 col-sm-11 col-xs-10">
                            <h2>vehicle <span>enquiry</span></h2>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-1 col-xs-2 text-right">
                            <a href="javascript:void(0);" id="RefineClose" data-dismiss="modal"><img src="{{ url('/') }}/images/close-refine.png" alt="Refine Close"></a>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="enquire-car">
                        <div class="row">
                            <div class="col-xs-7"><h3>RENAULT SANDERO 1.6 UNITED</h3><div class="modal-car-price">R 74,990 <span class="car-installment">R 1,352 p/m*</span></div><ul class="car-features">
                                    <li>Hatchback</li><li>Manual</li><li>Petrol</li>
                                </ul></div>
                            <div class="col-xs-5"><img src="{{ url('/') }}/images/car-red.jpg"></div>
                        </div>
                    </div>
                    <div class="enquire-form">
                        <h3 class="sub-title">vehicle <span>enquiry</span></h3>
                        <form class="row clearfix">
                            <div class="form-group col-sm-6">
                                <label>province</label>
                                <select><option>All Provinces</option><option>Dealer 1</option><option>Dealer 2</option><option>Dealer 3</option><option>Dealer 4</option></select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label>dealer</label>
                                <select><option>All branches</option><option>Branch 1</option><option>Branch 2</option><option>Branch 3</option><option>Branch 4</option></select>
                            </div>
                        </form>
                        <h3 class="sub-title"><span>your</span> details</h3>
                        <form class="row clearfix">
                            <div class="form-group col-sm-6">
                                <label>Name</label>
                                <input class="form-control" placeholder="John" type="email">
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Surname</label>
                                <input class="form-control" placeholder="Smith" type="text">
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Email</label>
                                <input class="form-control" placeholder="name@email.com" type="text">
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Cell Number</label>
                                <input class="form-control" type="text" placeholder="+27 72 123 4567">
                            </div>
                            <div class="col-xs-12 modal-buttons-row">
                                <div class="row">
                                    <div class="form-group col-xs-4"><a href="javascript:void(0);" class="btn btn-default d-b">Test drive</a></div>
                                    <div class="form-group col-xs-4"><a href="javascript:void(0);" class="btn btn-default d-b">Need Finance</a></div>
                                    <div class="form-group col-xs-4"><a href="javascript:void(0);" class="btn btn-default d-b">Have trade in</a></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 m-t-10"><a href="javascript:void(0);" class="btn btn-enquiry">send enquiry</a></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Enquire modal Start -->

@endsection
@section('after-scripts-end')
    <script type="text/javascript">
        /**
         * This function is used to add as favourites AND (WILL BE COMMON SOON)
         */
        $('.add-as-favourites').on('click', function () {
            var vId = $(this).data('favourites');
            var url = '{{ route("frontend.cars.add-favourites") }}';

            $.ajax({
                type: "POST",
                url: url,
                data: {"_token": "{{ csrf_token() }}", vId: vId},
                dataType: "JSON"
            }).success(function (data) {
                if (data == true) {
                    alert('Removed as Favourites.');
                } else {
                    alert('Added as Favourites.');
                }
            }).error(function (data) {
                console.log('Error:', data);
            });
        });
    </script>
@stop
