@extends('frontend.layouts.master')

@section('page-wrapper')

    <!-- Page Wrapper Start -->
    <div class="page-wrapper">

        <!-- Global Menu Start -->
    @include('frontend.includes.header')
    <!-- Global Menu End -->

        <!-- Middle Content Start -->
        <div class="middle-container">
            <div class="inner-container">
                <div class="row new-car-container">
                    <!-- Refine Column Start -->
                    <div class="col-lg-1 col-md-1 hidden-sm-xs" id="RefineColumn">
                        <div class="refine-car">
                            <p class="text-center" id="RefineArrowRow">
                                <a href="javascript:void(0);" id="RefineArrow" class="d-i-b"><img src="{{ url('/') }}/images/arrow-refine.png" alt="Refine Arrow"></a>
                            </p>
                            <div id="RefineTitle" class="refine-title">
                                <div class="row section-title-secondary">
                                    <div class="col-lg-10 col-md-9">
                                        <h2>Refine your car</h2>
                                    </div>
                                    <div class="col-lg-2 col-md-3 text-center">
                                        <a href="javascript:void(0);" id="RefineClose"><img src="{{ url('/') }}/images/close-refine.png" alt="Refine Close"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row left-refine" id="RefineOptRow">
                                <div class="col-lg-10 col-md-10">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 car-model">
                                            <a href="{{ url('/cars/new') }}" class="btn btn-refine d-b" @if(isset($vType) && $vType == 'new') style="border-color: rgb(237, 28, 36); background-color: rgb(237, 28, 36); color: white;" @endif>New cars</a>
                                        </div>
                                        <div class="col-lg-3 col-md-3 car-model">
                                            <a href="{{ url('/cars/used') }}" class="btn btn-refine d-b" @if(isset($vType) && $vType == 'used') style="border-color: rgb(237, 28, 36); background-color: rgb(237, 28, 36); color: white;" @endif>Used cars</a>
                                        </div>
                                        <div class="col-lg-3 col-md-3 car-model">
                                            <a href="{{ url('/cars/demo') }}" class="btn btn-refine d-b" @if(isset($vType) && $vType == 'demo') style="border-color: rgb(237, 28, 36); background-color: rgb(237, 28, 36); color: white;" @endif>Demo cars</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10 m-t-15" id="RefineSpecials" style="display:none;">
                                    @if((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'new')
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <a class="btn btn-refine d-b">New car specials</a>
                                            </div>
                                        </div>
                                    @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'used')
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <a class="btn btn-refine d-b">Priced to go</a>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <a class="btn btn-refine d-b">Specials</a>
                                            </div>
                                        </div>
                                    @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'demo')
                                        <!-- Will be filter buttons here if there is anything for demo -->
                                    @endif
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="gray-divider"></div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="row">
                                        <div class="col-lg-11 col-md-11">
                                            <div class="row refine-by-brand" id="RefineByBrand">
                                                @foreach($brands as $brand)
                                                    <div class="col-lg-3 col-md-3 m-b-15">
                                                        <a href="javascript:void(0);" class="btn-filter @if(isset($vFilter) && $vFilter == 'brand' && $brand->id == $vId) active @endif" id="brand-{{ $brand->id }}"><img src="{{ url('/') }}/brand_logo/{{ $brand->brand_logo }}" alt="{{ $brand->brand_logo }} Logo" title="{{ $brand->brand_logo }}"></a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1"><a href="javascript:void(0);" class="arrow-down active"></a></div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="gray-divider"></div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="row">
                                        <div class="col-lg-11 col-md-11 col-xs-11">
                                            <div class="row type-refine">
                                                <div class="col-lg-3 col-md-3">
                                                    <h3 class="text-red">type<span class="d-b">1 type</span></h3>
                                                </div>
                                                <div class="col-lg-9 col-md-9">
                                                    @foreach($vehicleTypes as $vehicleType)
                                                        <a class="btn btn-sort btn-filter @if(isset($vFilter) && $vFilter == 'type' && $vehicleType->id == $vId) active @endif" id="type-{{ $vehicleType->id }}">{{ $vehicleType->vehicle_type }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-xs-1"><a href="javascript:void(0);" class="arrow-down active"></a></div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="gray-divider"></div>
                                </div>

                                @if((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'new')
                                <div class="col-lg-12 col-md-12">
                                    <div class="row">
                                        <div class="col-lg-11 col-md-11 col-xs-11">
                                            <div class="row model-refine">
                                                <div class="col-lg-3 col-md-3">
                                                    <h3 class="text-red">model<span class="d-b">1 model</span></h3>
                                                </div>
                                                <div class="col-lg-9 col-md-9">
                                                    @foreach($vehicles as $vehicle)
                                                        <a class="btn btn-sort btn-filter" id="model-{{ $vehicle->derivatives->id }}">{{ $vehicle->derivatives->derivative_name }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-xs-1"><a href="javascript:void(0);" class="arrow-down active"></a></div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="gray-divider"></div>
                                </div>
                                @endif

                                <div class="col-lg-12 col-md-12">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3">
                                                    <h3 class="m-t-5">price</h3>
                                                </div>
                                                <div class="col-lg-9 col-md-9">
                                                    <?php $priceRanges = config('constants.PRICE_RANGE'); ?>
                                                    <select class="d-b" id="price-filter">
                                                        <option>Min-Max Price</option>
                                                        @foreach($priceRanges as $value => $option)
                                                            <option value="{{ $value }}">{{ $option }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2"></div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="gray-divider"></div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3">
                                                    <h3 class="m-t-5">monthly</h3>
                                                </div>
                                                <div class="col-lg-9 col-md-9">
                                                    <?php $monthlyRanges = config('constants.MONTHLY_RANGE'); ?>
                                                    <select class="d-b">
                                                        <option>Min-Max monthly</option>
                                                        @foreach($monthlyRanges as $value => $option)
                                                            <option value="{{ $value }}">{{ $option }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2"></div>
                                    </div>
                                </div>

                                @if((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'used')
                                    <div class="col-lg-12 col-md-12">
                                        <div class="gray-divider"></div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-10 col-sm-11 col-xs-10">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <h3 class="m-t-5">kms</h3>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                        <label>from</label>
                                                        <?php
                                                            $currently_selected = 5000;
                                                            $startKM = 5000;
                                                            $endKM = 200000;
                                                            echo '<select class="d-b">';
                                                            for($i=$startKM;$i<=$endKM;$i+=5000) {
                                                                echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                                            }
                                                            echo '</select>';
                                                        ?>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-xs-offset-3">
                                                        <label>to</label>
                                                        <?php
                                                            $currently_selected = 200000;
                                                            $startKM = 5000;
                                                            $endKM = 200000;
                                                            echo '<select class="d-b">';
                                                            for($i=$startKM;$i<=$endKM;$i+=5000) {
                                                                echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                                            }
                                                            echo '</select>';
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="gray-divider"></div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-10 col-sm-11 col-xs-10">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <h3 class="m-t-5">year</h3>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                        <label>from</label>
                                                        <?php
                                                            $currently_selected = 1950;
                                                            $earliest_year = 1950;
                                                            $latest_year = date('Y');
                                                            echo '<select class="d-b">';
                                                            //echo '<option value="">from</option>';
                                                            foreach ( range( $latest_year, $earliest_year ) as $i ) {
                                                                echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                                            }
                                                            echo '</select>';
                                                        ?>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-xs-offset-3">
                                                        <label>to</label>
                                                        <?php
                                                            $currently_selected = date('Y');
                                                            $earliest_year = 1950;
                                                            $latest_year = date('Y');
                                                            echo '<select class="d-b">';
                                                            //echo '<option value="">to</option>';
                                                            foreach ( range( $latest_year, $earliest_year ) as $i ) {
                                                                echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                                            }
                                                            echo '</select>';
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                @endif

                                <div class="col-lg-12 col-md-12 text-center">
                                    <button class="btn-text reset-filters-tag">reset filters</button>
                                </div>
                                <div class="col-lg-12 col-md-12 text-center">
                                    <button class="btn btn-red btn-apply apply-filters-tag">Apply Filters<!--<span>(238 results)</span>--></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Refine Column End -->
                    <!-- Car Column Start -->
                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 sorting-column" data-mcs-theme="minimal-dark" id="SortingColumn">
                        <!-- Sorting Row Start -->
                        <ul class="sorting">
                            <li class="sort-result hidden-sm-xs">
                                <h3>{{ $vehicles->total() }} <span>results</span></h3>
                            </li>
                            <li>
                                <h3 class="d-i-b">price:</h3>
                                <a href="javascript:void(0);" class="btn btn-sort m-r-10 btn-filter" id="order-asc">Lowest</a>
                                <a href="javascript:void(0);" class="btn btn-sort btn-filter" id="order-desc">Highest</a>
                            </li>
                            <li>
                                <h3 class="d-i-b hidden-sm-xs">dealers:</h3>
                                {!! Form::select('province_region_id', array(0 => 'All Provinces') + $provinceRegions, isset($item->province_region_id) ? $item->province_region_id : '', array('class' => 'form-control accAj', 'required')) !!}
                            </li>
                        </ul>
                        <!-- Sorting Row End -->
                        <!-- Car Listing Start -->
                        <div class="row">
                            <div id="results">
                                @foreach($vehicles as $vehicle)
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 car-list" id="CarSelect" data-id="{{ $vehicle->id }}" data-vehicle="{{ $vehicle->vehicle_type }}">
                                        <div class="car-box">
                                            <div class="car-image">
                                                <img class="img-responsive" src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                                            </div>
                                            <div class="car-information">
                                                <div class="row">
                                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
                                                        <h2><a href="javascript:void(0);">{{ $vehicle->brands->brand_name }} {{ $vehicle->brandModels->brand_model_name }}<br>{{ $vehicle->derivatives->derivative_name }}</a></h2>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 text-right"><img src="{{ url('/') }}/brand_logo/{{ $vehicle->brands->brand_logo }}" alt="{{ $vehicle->brandModels->brand_model_name }} Logo" title="{{ $vehicle->brandModels->brand_model_name }}"></div>
                                                </div>
                                                @if((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'new')
                                                    <h2 class="car-price"><a href="javascript:void(0);">R {{ number_format($vehicle->selling_price) }}</a><span class="car-installment">R 2,326</span> <span class="installment-type">monthly<sup>*</sup></span></h2>
                                                    <ul class="car-features">
                                                            <li>{{ $vehicle->vehicleTypes->vehicle_type }}</li>
                                                            <li>Manual</li>
                                                            <li>Petrol</li>
                                                    </ul>
                                                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'used')
                                                    <div class="clearfix">
                                                        <h2 class="car-price"><a href="javascript:void(0);">R {{ number_format($vehicle->selling_price) }}</a><span class="pull-right"><span class="car-installment">R 3,450</span> <span class="installment-type">p/m<sup>*</sup></span></span></h2>
                                                    </div>
                                                    <div class="clearfix car-info-bottom">
                                                        <div class="car-info-bottom-left">
                                                            <ul class="car-features">
                                                                <?php
                                                                $vehicle->vehicleSpecification->each(function ($spec) {
                                                                    if ($spec->old_specification_id == 1) { ?>
                                                                        <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                                                <?php } else if ($spec->old_specification_id == 2) { ?>
                                                                        <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                                                <?php } else if ($spec->old_specification_id == 3) { ?>
                                                                        <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                                                <?php }
                                                                });
                                                                ?>
                                                            </ul>
                                                        </div>
                                                        <button class="btn btn-gp">GP</button>
                                                    </div>
                                                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'demo')
                                                    <div class="clearfix">
                                                        <h2 class="car-price"><a href="javascript:void(0);">R {{ number_format($vehicle->selling_price) }}</a><span class="pull-right"><span class="car-installment">R 3,450</span> <span class="installment-type">p/m<sup>*</sup></span></span></h2>
                                                    </div>
                                                    <div class="clearfix car-info-bottom">
                                                        <div class="car-info-bottom-left">
                                                            <ul class="car-features">
                                                                <?php
                                                                $vehicle->vehicleSpecification->each(function ($spec) {
                                                                if ($spec->old_specification_id == 1) { ?>
                                                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                                                <?php } else if ($spec->old_specification_id == 2) { ?>
                                                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                                                <?php } else if ($spec->old_specification_id == 3) { ?>
                                                                <li>{{ $spec->oldProperty->old_specification_property }}</li>
                                                                <?php }
                                                                });
                                                                ?>
                                                            </ul>
                                                        </div>
                                                        <button class="btn btn-gp">GP</button>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- Below hidden div for filter cars column size -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 car-list hidden" style="display: none;"></div>
                            <!-- Above hidden div for filter cars column size -->
                            <div class="ajax-loading"><img src="{{ asset('images/loading.gif') }}" /></div>
                        </div>
                        <!-- Car Listing End -->
                    </div>
                    <!-- Car Column End -->

                    <!-- Car Detail Column Start -->
                    <div id="details-car"></div>
                    <!-- Car Detail Column End -->
                </div>


            </div>
        </div>
        <!-- Middle Content End -->

    </div>
    <!-- Page Wrapper End -->

    <!-- Mobile Refine modal Start -->
    <div id="refine-mobile" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="refine-car">
                        <div class="refine-title">
                            <div class="row section-title-secondary">
                                <div class="col-lg-10 col-md-9 col-sm-11 col-xs-10">
                                    <h2>Refine your car</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-1 col-xs-2 text-right">
                                    <a href="javascript:void(0);" id="RefineClose" data-dismiss="modal"><img src="{{ url('/') }}/images/close-refine.png" alt="Refine Close"></a>
                                </div>
                            </div>
                        </div>
                        <div class="row left-refine" id="RefineOptRow">
                            <div class="col-lg-10 col-md-10 col-sm-11 col-xs-10">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 car-model">
                                        <a href="{{ url('/cars/new') }}" class="btn btn-refine d-b" @if(isset($vType) && $vType == 'new') style="border-color: rgb(237, 28, 36); background-color: rgb(237, 28, 36); color: white;" @endif>New cars</a>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 car-model">
                                        <a href="{{ url('/cars/used') }}" class="btn btn-refine d-b" @if(isset($vType) && $vType == 'used') style="border-color: rgb(237, 28, 36); background-color: rgb(237, 28, 36); color: white;" @endif>Used cars</a>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 car-model">
                                        <a href="{{ url('/cars/demo') }}" class="btn btn-refine d-b" @if(isset($vType) && $vType == 'demo') style="border-color: rgb(237, 28, 36); background-color: rgb(237, 28, 36); color: white;" @endif>Demo cars</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-11 col-xs-10" id="RefineSpecials">
                                @if((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'new')
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <a class="btn btn-refine d-b">New car specials</a>
                                        </div>
                                    </div>
                                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'used')
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <a class="btn btn-refine d-b">Priced to go</a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <a class="btn btn-refine d-b">Specials</a>
                                        </div>
                                    </div>
                                @elseif((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'demo')
                                <!-- Will be filter buttons here if there is anything for demo -->
                                @endif
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="gray-divider"></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                        <div class="row refine-by-brand" id="RefineByBrand">
                                            @foreach($brands as $brand)
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 m-b-15">
                                                    <a href="javascript:void(0);" class="btn-filter @if(isset($vFilter) && $vFilter == 'brand' && $brand->id == $vId) active @endif" id="brand-{{ $brand->id }}"><img src="{{ url('/') }}/brand_logo/{{ $brand->brand_logo }}" alt="{{ $brand->brand_logo }} Logo" title="{{ $brand->brand_logo }}"></a>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="javascript:void(0);" class="arrow-down active"></a></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="gray-divider"></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                        <div class="row type-refine">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h3 class="text-red">type<span class="d-b">1 type</span></h3>
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                @foreach($vehicleTypes as $vehicleType)
                                                    <a class="btn btn-sort btn-filter @if(isset($vFilter) && $vFilter == 'type' && $vehicleType->id == $vId) active @endif" id="type-{{ $vehicleType->id }}">{{ $vehicleType->vehicle_type }}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="javascript:void(0);" class="arrow-down active"></a></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="gray-divider"></div>
                            </div>

                            @if((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'new')
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row model-refine">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h3 class="text-red">model<span class="d-b">1 model</span></h3>
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                @foreach($vehicles as $vehicle)
                                                    <a class="btn btn-sort btn-filter" id="model-{{ $vehicle->derivatives->id }}">{{ $vehicle->derivatives->derivative_name }}</a>
                                                @endforeach
                                                <a href="javascript:void(0);" class="arrow-down active"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-2"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="gray-divider"></div>
                            </div>
                            @endif

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-10">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h3 class="m-t-5">price</h3>
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                <?php $priceRanges = config('constants.PRICE_RANGE'); ?>
                                                <select class="d-b" id="price-filter">
                                                    <option>Min-Max Price</option>
                                                    @foreach($priceRanges as $value => $option)
                                                        <option value="{{ $value }}">{{ $option }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-2"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="gray-divider"></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-10">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h3 class="m-t-5">monthly</h3>
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                <?php $monthlyRanges = config('constants.MONTHLY_RANGE'); ?>
                                                <select class="d-b">
                                                    <option>Min-Max monthly</option>
                                                    @foreach($monthlyRanges as $value => $option)
                                                        <option value="{{ $value }}">{{ $option }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-2"></div>
                                </div>
                            </div>

                            @if((Request::route()->getName() == 'cars.show' || Request::route()->getName() == 'cars.show-filter') && Request::segment(2) == 'used')
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="gray-divider"></div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10 col-sm-11 col-xs-10">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <h3 class="m-t-5">kms</h3>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                    <label>from</label>
                                                    <?php
                                                    $currently_selected = 5000;
                                                    $startKM = 5000;
                                                    $endKM = 200000;
                                                    echo '<select class="d-b">';
                                                    for($i=$startKM;$i<=$endKM;$i+=5000) {
                                                        echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                                    }
                                                    echo '</select>';
                                                    ?>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-xs-offset-3">
                                                    <label>to</label>
                                                    <?php
                                                    $currently_selected = 200000;
                                                    $startKM = 5000;
                                                    $endKM = 200000;
                                                    echo '<select class="d-b">';
                                                    for($i=$startKM;$i<=$endKM;$i+=5000) {
                                                        echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                                    }
                                                    echo '</select>';
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="gray-divider"></div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10 col-sm-11 col-xs-10">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <h3 class="m-t-5">year</h3>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                    <label>from</label>
                                                    <?php
                                                    $currently_selected = 1950;
                                                    $earliest_year = 1950;
                                                    $latest_year = date('Y');
                                                    echo '<select class="d-b">';
                                                    //echo '<option value="">from</option>';
                                                    foreach ( range( $latest_year, $earliest_year ) as $i ) {
                                                        echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                                    }
                                                    echo '</select>';
                                                    ?>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-xs-offset-3">
                                                    <label>to</label>
                                                    <?php
                                                    $currently_selected = date('Y');
                                                    $earliest_year = 1950;
                                                    $latest_year = date('Y');
                                                    echo '<select class="d-b">';
                                                    //echo '<option value="">to</option>';
                                                    foreach ( range( $latest_year, $earliest_year ) as $i ) {
                                                        echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                                    }
                                                    echo '</select>';
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endif

                            <div class="col-xs-12">
                                <p class="reset-filters text-center"><a href="javascript:void(0);" class="reset-filters-tag d-b">reset filters</a></p>
                                <a href="javascript:void(0);" class="btn btn-red d-b apply-filters-tag">apply filters</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Refine modal End -->

    <!-- Enquire modal Start -->
    <div id="EnquireForm" class="modal fade enquiry-form" role="dialog">
        <div class="modal-dialog" id="enquiry-modal-content"></div>
    </div>
    <!-- Enquire modal End -->

    <!-- Hidden input Start -->
    <input type="hidden" id="apply_filter_data" />
    <input type="hidden" id="page_cnt" value="1" />
    <!-- Hidden input End -->

@endsection

@section('after-scripts-end')
    <link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet">
    <script src="http://demo.expertphp.in/js/jquery-ui.min.js"></script>
    <script type="text/javascript">

        var arr = [];
        var baseUrl = "<?php echo \Request::url(); ?>";
        $('.ajax-loading').hide();

        //load_more(page); //initial content load
        $(window).scroll(function() { //detect page scroll
            var page = $('#page_cnt').val(); //track user scroll as page number, right now page number is 1
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) { //if user scrolled from top to bottom of the page
                page++; //page number increment
                load_more(page); //load content
            }
        });

        var page = $('#page_cnt').val(); //track user scroll as page number, right now page number is 1
        $('#SortingColumn').mCustomScrollbar({
            callbacks: {
                onTotalScroll: function () {
                    page++; //page number increment
                    load_more(page); //load content
                }
            }
        });

        function addCarsAsFavourites() {
            $('.add-as-favourites').on('click', function () {
                var vId = $(this).data('favourites');
                var url = '{{ route("frontend.cars.add-favourites") }}';

                $.ajax({
                    type: "POST",
                    url: url,
                    data: { "_token": "{{ csrf_token() }}", vId: vId },
                    dataType: "JSON"
                }).success(function (data) {
                    if(data == true) {
                        alert('Removed as Favourites.');
                    } else {
                        alert('Added as Favourites.');
                    }
                }).error(function (data) {
                    console.log('Error:', data);
                });
            });
        }

        function carDetailsLoadingEvent() {
            $('.car-list').on('click', function () {

                $('div#CarSelect div.car-box').removeClass('car-seleted');
                $(this).children(":first").addClass('car-seleted');

                var vehicleType = $(this).data('vehicle');
                var url = '{{ route("frontend.cars.getDetails", ":vId") }}';
                url = url.replace(':vId', $(this).data('id'));

                var vId = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: "JSON"
                }).success(function (data) {

                    $('#details-car').html(data.view);

                    var $div = $('.description-container');
                    var words = $div.text().split(' ');
                    words.splice(60, words.length - 1);
                    var excerpt = words.join(' ') + '&hellip;';

                    $div.data('html', $div.html()).html(excerpt);

                    $('.more-description').click(function () {
                        var isHidden = $(this).text() == 'more';
                        $div.html(isHidden ? $div.data('html') : excerpt);
                        $(this).text(isHidden ? 'less' : 'more');
                    });

                    var owl = $('.DealerCarSlider');
                    owl.owlCarousel({
                        loop: true,
                        items: 1,
                        thumbs: true,
                        thumbImage: true,
                        thumbContainerClass: 'owl-thumbs',
                        nav: false,
                        thumbItemClass: 'owl-thumb-item'
                    });

                    $(".back-top").click(function () {
                        $("html, body").animate({scrollTop: 0}, 1000);
                    });

                    if (vehicleType == 'N') {
                        window.history.replaceState(null, null, "{{ url('cars/new') }}/" + $('.get-slug h2').attr('class'));
                    } else if (vehicleType == 'U') {
                        window.history.replaceState(null, null, "{{ url('cars/used') }}/" + $('.get-slug h2').attr('class'));
                    } else if (vehicleType == 'D') {
                        window.history.replaceState(null, null, "{{ url('cars/demo') }}/" + $('.get-slug h2').attr('class'));
                    }

                    car_detail_close();
                    addCarsAsFavourites();

                }).error(function (data) {
                    console.log('Error:', data);
                });
            });
        }

        function loadClassDynamic(strClasses) {
            if (strClasses.indexOf('col-lg-3') != -1) { //alert(4);
                $('.car-list').addClass('col-lg-3');
                $('.car-list').removeClass('col-lg-6');
                $('.car-list').removeClass('col-lg-4');
            } else if (strClasses.indexOf('col-lg-4') != -1) { //alert(3);
                $('.car-list').addClass('col-lg-4');
                $('.car-list').removeClass('col-lg-6');
                $('.car-list').removeClass('col-lg-3');
            } else if (strClasses.indexOf('col-lg-6') != -1) { //alert(2);
                $('.car-list').addClass('col-lg-6');
                $('.car-list').removeClass('col-lg-3');
                $('.car-list').removeClass('col-lg-4');
            }
        }

        function load_more(page) {
            $('#page_cnt').val(page);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            baseUrl = window.location.href;
            $.ajax({
                type: "GET",
                url: baseUrl + '?page=' + page + '&filters=' + encodeURIComponent($('#apply_filter_data').val()),
                dataType: "JSON",
                beforeSend: function () {
                    $('.ajax-loading').show();
                }
            }).success(function (data) {
                if (data.view.length == 0) {
                    //notify user if nothing to load
                    $('.ajax-loading').html("No more records!");
                    return;
                }
                $('.ajax-loading').hide(); //hide loading animation once data is received
                var strClasses = $('.car-list').attr('class');
                $('#results').append(data.view); //append data into #results element
                loadClassDynamic(strClasses);
                carDetailsLoadingEvent();
                new_car();

            }).error(function (data) {
                console.log('Error:', data);
            });
        }

        function filterPriceForCars(id) {
            baseUrl = window.location.href;
            $.ajax({
                type: "GET",
                url: baseUrl + '?price=' + id,
                dataType: "JSON",
                beforeSend: function () {
                    $('.ajax-loading').show();
                }
            }).success(function (data) {
                if (data.view.length == 0) {
                    //notify user if nothing to load
                    $('.ajax-loading').html("No more records!");
                    return;
                }
                $('.ajax-loading').hide(); //hide loading animation once data is received
                var strClasses = $('.car-list').attr('class');
                $("#results").html(data.view); //append data into #results element
                loadClassDynamic(strClasses);
                carDetailsLoadingEvent();
                new_car();

            }).error(function (data) {
                console.log('Error:', data);
            });
        }

        function getVehicleDetail(vehicleId){
            url = "{{ route('frontend.vehicle.enquiryPopup','-1') }}";
            $.ajax({
                type: "GET",
                url: url.replace('-1', vehicleId),
                beforeSend: function () {
                    $('.ajax-loading').show();
                }
            }).success(function (data) {
                if (data) {
                    $("#enquiry-modal-content").html(data);
                }
                $('.ajax-loading').hide(); //hide loading animation once data is received

            }).error(function (data) {
                console.log('Error:', data);
            });
        }
     
        function applyFilters(url) {
            if (!url) {
                url = baseUrl;
            }

            $.ajax({
                type: "GET",
                url: url + '?filters=' + encodeURIComponent($('#apply_filter_data').val()),
                dataType: "JSON",
                beforeSend: function () {
                    $('.ajax-loading').show();
                }
            }).success(function (data) {
                if (data.view.length == 0) {
                    //notify user if nothing to load
                    $("#results").html('');
                    $('.ajax-loading').html("No more records!");
                    return;
                }
                $('.ajax-loading').hide(); //hide loading animation once data is received
                var strClasses = $('.car-list').attr('class');
                $("#results").html(data.view); //append data into #results element
                loadClassDynamic(strClasses);
                carDetailsLoadingEvent();
                new_car();

            }).error(function (data) {
                console.log('Error:', data);
            });
        }

        $(document).ready(function() {

            $('.middle-container a.btn-filter').on('click', function () {
                $('#page_cnt').val(1);

                var id = $(this).attr('id');
                if (id == 'order-asc' || id == 'order-desc') {
                    filterPriceForCars(id);
                }

                var data = id.split('-');
                var added = false;

                $.map(arr, function (elementOfArray, indexInArray) {
                    if (elementOfArray.key == data[0] && elementOfArray.value == data[1]) {
                        elementOfArray.value = data[1];
                        added = true;
                    }
                });

                if (!added) {
                    arr.push({key: data[0], value: data[1]});
                }

                $('.middle-container a.btn-filter').each(function (index) {
                    if ($(this).hasClass('active') == false) {
                        //console.log(index + ": " + $(this).attr('id'));
                        var id = $(this).attr('id');
                        var splitID = id.split('-');
                        if (arr.length != 0) {
                            for (var i = 0; i < arr.length; i++) {
                                if (arr[i].key == splitID[0] && arr[i].value == splitID[1]) {
                                    arr.splice(i, 1);
                                }
                            }
                        }
                    }
                });

                //$.parseJSON($('#project').val());
                $('#apply_filter_data').val(JSON.stringify(arr));
            });

            $('#refine-mobile a.btn-filter').on('click', function () {
                $('#page_cnt').val(1);

                var id = $(this).attr('id');
                if (id == 'order-asc' || id == 'order-desc') {
                    filterPriceForCars(id);
                }

                var data = id.split('-');
                var added = false;

                $.map(arr, function (elementOfArray, indexInArray) {
                    if (elementOfArray.key == data[0] && elementOfArray.value == data[1]) {
                        elementOfArray.value = data[1];
                        added = true;
                    }
                });

                if (!added) {
                    arr.push({key: data[0], value: data[1]});
                }

                $('#refine-mobile a.btn-filter').each(function (index) {
                    if ($(this).hasClass('active') == false) {
                        //console.log(index + ": " + $(this).attr('id'));
                        var id = $(this).attr('id');
                        var splitID = id.split('-');
                        if (arr.length != 0) {
                            for (var i = 0; i < arr.length; i++) {
                                if (arr[i].key == splitID[0] && arr[i].value == splitID[1]) {
                                    arr.splice(i, 1);
                                }
                            }
                        }
                    }
                });

                //$.parseJSON($('#project').val());
                $('#apply_filter_data').val(JSON.stringify(arr));
            });

            /**
             * Reload Page - Reset Filter
             */
            $('.apply-filters-tag').click(function () {
                var str = window.location.href;

                if ((str.match(/new/gi) && str.match(/brand/gi)) || (str.match(/new/gi))) {
                    window.history.replaceState(null, null, "{{ url('cars/new') }}");

                    if (str.match(/brand/gi)) {
                        var value = str.match(/\d+/);
                        arr.push({key: 'brand', value: parseInt(value)});
                        $('#apply_filter_data').val(JSON.stringify(arr));
                    }

                    str = window.location.href;
                } else if ((str.match(/used/gi) && str.match(/type/gi)) || (str.match(/used/gi))) {
                    window.history.replaceState(null, null, "{{ url('cars/used') }}");

                    if (str.match(/type/gi)) {
                        var value = str.match(/\d+/);
                        arr.push({key: 'type', value: parseInt(value)});
                        $('#apply_filter_data').val(JSON.stringify(arr));
                    }

                    str = window.location.href;
                }

                applyFilters(str);
            });

            $('#price-filter').on('change', function () {
                if (arr.length != 0) {
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i].key == 'price') {
                            arr.splice(i, 1);
                        }
                    }
                }

                arr.push({key: 'price', value: $(this).val()});
                $('#apply_filter_data').val(JSON.stringify(arr));
            });

            /**
             * Reload Page - Reset Filter
             */
            $('.reset-filters-tag').click(function () {
                location.reload();
            });

            /**
             * AutoComplete - Search
             */
            //alert(baseUrl + '{ \Route::getFacadeRoot()->current()->uri() }});
            $("#search_text").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: '{{ route('frontend.search.autoComplete') }}',
                        dataType: "json",
                        data: {
                            term : request.term
                        },
                        success: function(data) {
                            response(data);

                        }
                    });
                },
                min_length: 3,

            });

            /**
             * This function is used to get car details by vehicle ID
             */
            carDetailsLoadingEvent();

            /**
             * This function is used to add as favourites
             */
            addCarsAsFavourites();
        });
 
    </script>
@stop
