<div class="detail-column col-lg-5 col-md-5 col-sm-12 col-xs-12 hidden-sm-xs" data-mcs-theme="minimal-dark">
    <a id="target-element"></a>
    <div class="car-detail">
        <div class="top-slider m-b-15">
            <div class="owl-carousel DealerCarSlider" id="DealerCarSlider">
                <div class="item">
                    <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                </div>
                <div class="item">
                    <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car">
                </div>
                <div class="item">
                    <img src="{{ url('/') }}/images/car-1.jpg" alt="New Car">
                </div>
                <div class="item">
                    <img src="{{ url('/') }}/images/car-2.jpg" alt="New Car">
                </div>
            </div>
            <a href="javascript:void(0);" class="car-detail-close"><img src="{{ url('/') }}/images/close-detail-white.png" alt="Car Detail Close"></a>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 get-slug">
                    <!-- <h2>2013 AUDI A1 1.4T FSI<br>AMBITION 3 DR</h2> -->
                    <h2 class="{!! $vehicle->vehicle_slug !!}">{!! $vehicle->vehicleStock->model_year !!} {!! $vehicle->vehicleStock->listing_title !!}</h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <ul class="car-detail-links text-right">
                        @if(\Auth::check())
                        <li>
                            <a href="javascript:void(0);" class="add-as-favourites" data-favourites="{{ $vehicle->id }}"><img src="{{ url('/') }}/images/icon-favourite-detail.png" alt="Add to Favourites"></a>
                        </li>
                        @endif
                        <li>
                            <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-compare-detail.png" alt="Compare"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0);"><img src="{{ url('/') }}/images/icon-share-detail.png" alt="Share"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 gray-right-border car-type-list-row">
                    <ul class="car-type-list">

                        <li><img src="{{ url('/') }}/images/icon-hatchback-detail.png" alt="Icon Hatchback">
                            <p>{{ $carTypeList['vehicleType'] }}</p>
                        </li>
                        <li><img src="{{ url('/') }}/images/icon-manual-detail.png" alt="Icon Manual">
                            <p>{{ $carTypeList['transmissionType'] }}</p>
                        </li>
                        <li><img src="{{ url('/') }}/images/icon-petrol-detail.png" alt="Icon Petrol">
                            <p>{{ $carTypeList['fuelType'] }}</p>
                        </li>
                        <li><img src="{{ url('/') }}/images/icon-average-detail.png" alt="Icon Average">
                            <p>6,3 L/100 km</p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h2 class="car-price"><span class="car-installment d-b">R 3 015.00 <span class="installment-type">monthly<sup>*</sup></span></span>R {{ number_format($vehicle->selling_price) }}</h2>
                    <a class="btn btn-repayment btn-sm d-b" href="javascript:void(0);">Calculate repayment</a>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="gray-divider"></div>
                    <p><span class="description-container">{!! $vehicle->vehicleStock->description !!}</span>  <a href="javascript:void(0);" class="more-description">more</a></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-left m-t-15">
                    <div class="dealer-popup-detail">
                        <p>City: <strong>{{ $vehicle->vehicleStock->dealership->province_region->region_name }}</strong></p>
                        <p>Dealership: <strong>{{ $vehicle->vehicleStock->dealership->dealership_name }}</strong></p>
                        <button class="btn btn-blue">Dealer Showroom</button>
                        <p>Stock Number: <strong>{{ $vehicle->vehicleStock->vehicle_number }}</strong></p>
                        <p>M&amp;M Code: <strong>{{ $vehicle->vehicleStock->mead_mcgrouther_code }}</strong></p>
                    </div>
                </div>

                <div class="col-xs-12 car-interior car-dealer-features">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a data-toggle="tab" href="#features">FEATURES</a></li>
                    </ul>
                    <div class="tab-content clearfix">
                        <div id="features" class="tab-pane active">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <?php $i = 1; ?>
                                    @foreach($carTypeList['features'] as $feature)
                                        <td>{!! $feature !!}</td>
                                        @if($i % 2 === 0)
                                            </tr><tr>
                                        @endif
                                    <?php $i++; ?>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-xs-12">
                                <a href="javascript:void(0);" class="btn btn-green m-t-15" data-toggle="modal" data-target="#EnquireForm" onclick="getVehicleDetail({{ $vehicle->id }});">ENQUIRE WITH DEALER</a>
                                <a href="javascript:void(0);" class="btn btn-default m-t-15 back-top">Back to top</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>