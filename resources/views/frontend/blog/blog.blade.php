@extends('frontend.layouts.master')

@section('before-styles-end')

    @if($default_post != null)
    <meta name="description" content="{{ $default_post->meta_description }}"/>
    <meta name="keywords" content="{{ $default_post->meta_keywords }}"/>
    <link rel="canonical" href="{{ $default_post->cannonical_link }}" />
    @else
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <link rel="canonical" href="" />
    @endif
@endsection

@section('after-styles-end')
<style type="text/css" rel="stylesheet">
		.highlight-post{
			background-color: #ed1c24;
			color: #fff;
			padding: 10px;
		}
		.highlight-post div.read-small-link a{
			background-color: #fff;
			color: #ed1c24;
		}
		.icon i{
			padding-top: 12.5px;
		}
		li.compare{
			padding-top: 12.5px;
		}
        .parent
        {
            height: 100%;
            width: 100%;
            position: : relative;
        }
        .child
        {
            width: 100%;
            z-index: 10000;
            position: absolute;
            top: 60%;
            left: 50%;
            transform: translate(-50%,-50%);
        }
	</style>
@endsection

@section('page-wrapper')

	<!-- Page Wrapper Start -->
	<div class="page-wrapper">

		<!-- Global Menu Start -->
	@include('frontend.includes.header')
	<!-- Global Menu End -->
		<!-- Middle Content Start -->
		<div class="middle-container">
			<div class="blog-top-section page-padding">
				<div class="inner-container">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 news-title">
							<div class="section-title-primary d-i-b">
								<h2>news &amp; <span>reviews</span></h2>
							</div>
							<ul class="sorting">
								<li>
									<h3 class="d-i-b hidden-sm-xs">sort by</h3>
									<select tabindex="-1" class="select2-hidden-accessible sort_ing" aria-hidden="true">
										<option class="latest" value="latest">Latest</option>
										<option class="oldest" value="oldest">Oldest</option>
									</select>
								</li>
							</ul>
						</div>
						<div class="col-lg-9 col-md-8 col-sm-7 col-xs-12">
							<ul class="blog-tabs">
								@foreach($categories as $category)
									<li><a href="javascript:void(0);" class="category" data-cat-id="{{$category->id }}">{{ $category->name }}</a></li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
            <div class="row child hidden">
                <div class="col-md-6 col-md-offset-3">
                    <div class="error">
                        <h1 class="alert alert-danger text-center">No Posts Found</h1>                
                    </div>
                </div>    
            </div>
			<div class="blog-tab-content clearfix">             
                <ul class="resp-tabs-list blog-detail-tabs blog-left-section">
                    <div class="load-more-blogs">
                        @include('frontend.blog.posts')
                    </div>
                </ul>

				<div id="default-review-post">
					@include('frontend.blog.defaultPost')
				</div>
			</div>
		</div>
		<!-- Middle Content End -->

	</div>
	<!-- Page Wrapper End -->
@endsection
<script src="/HTML/js/jquery.mCustomScrollbar.concat.min.js"></script>
@section('after-scripts-end')
	<script type="text/javascript">

        $(document).ready(function(){

            var posts = <?php echo $posts ?>;

            var baseUrl = "<?php echo \Request::url() ?>";

            var page = 1;

            var meta_title = document.head.querySelector("title");
            
            var canonical = document.head.querySelector("[rel=canonical]");
            
            var meta_description = document.head.querySelector("[name=description]");
            
            var meta_keywords = document.head.querySelector("[name=keywords]");

            var title = "Imperial Select | ";

            var sortingOrder;

            var id;


            $(".load-more-blogs li:first-child").addClass('highlight-post');
            social_plugins();
            if(posts == "")
            {
                $(".child").removeClass('hidden');
            }
            $(document).on('change','.sort_ing',function(){

                sortingOrder = $(this).val();
                if(sortingOrder == "oldest")
                {
                    $.ajax({
                        url: '/blog-sorting',
                        type: 'GET',
                        data: {'posts' :  posts, 'sortingOrder' : sortingOrder },
                        success: function(data)
                        {
                            $('.load-more-blogs').html('');
                            $('.load-more-blogs').html(data['html']);
                            $('#default-review-post').html('');
                            $('#default-review-post').html(data['default_post']);
                            meta_data_loader(data['post']['name'],data['post']['meta_description'],data['post']['meta_keywords'],data['post']['cannonical_link']);
                            posts = data['posts'];
                            $(".load-more-blogs li:first-child").addClass('highlight-post');
                            social_plugins();


                        }
                    });
                }
                else
                {
                    $.ajax({
                        url: '/blog-sorting',
                        type: 'GET',
                        data: {'posts' :  posts, 'sortingOrder' : sortingOrder },
                        success: function(data)
                        {
                            $('.load-more-blogs').html('');
                            $('.load-more-blogs').html(data['html']);
                            $('#default-review-post').html('');
                            $('#default-review-post').html(data['default_post']);
                            meta_data_loader(data['post']['name'],data['post']['meta_description'],data['post']['meta_keywords'],data['post']['cannonical_link']);
                            posts = data['posts'];
                            $(".load-more-blogs li:first-child").addClass('highlight-post');
                            social_plugins();


                        }
                    });
                }

            });


            $('.category').click(function(){
                id = $(this).attr('data-cat-id');
                $.ajax({

                    url:'/blog-categories',
                    data: {'catId':id},
                    type: 'GET',
                    success: function(data){
                        $('.load-more-blogs').html('');
                        $('.load-more-blogs').html(data['html']);
                        $('#default-review-post').html('');
                        {{-- alert(data['post']); --}}
                        if(data['post'] != null)
                        {
                            $(".child").addClass("hidden");
                            meta_data_loader(data['post']['name'],data['post']['meta_description'],data['post']['meta_keywords'],data['post']['cannonical_link']);
                            posts = data['posts'];
                            $('.sort_ing').val('latest').change();
                        }
                        else
                        {
                            $(".child").removeClass("hidden");
                        }
                        $('#default-review-post').html(data['default_post']);
                        social_plugins();
                        
                    }

                });
            });



            $(document).on('click', '.review-button', function(e)
            {
                e.preventDefault();

                $('.review-button').removeClass('highlight-post');
                var reviewId = $(this).attr('data-review-id');
                var pageurl = $(this).attr('data-slug');
                $.ajax({
                    url: '/blog',
                    data: {'reviewId':reviewId},
                    type: 'GET',
                    success: function(data) {
                        $('#default-review-post').html('');
                        $('#default-review-post').html(data['html']);
                        meta_data_loader(data['post']['name'],data['post']['meta_description'],data['post']['meta_keywords'],data['post']['cannonical_link']);
                        $(".load-more-blogs li").removeClass("highlight-post");
                        $('#'+reviewId).addClass('highlight-post');
                        social_plugins();



                    }
                });

                //to change the browser URL to the given link location

                if(pageurl!=window.location){

                    window.history.replaceState({path:pageurl},'','/blog/'+pageurl);
                }
                //stop refreshing to the page given in
                return false;

            });


            function load_more_posts(page) {
                $.ajax({
                    type: "GET",
                    url: baseUrl + "?page=" + page,
                    dataType: "JSON",
                    data: {'paginate': 'yes'},
                    beforeSend: function () {
                        $('.ajax-loading').show();
                    }
                }).success(function (data) {
                    $('.load-more-blogs').append(data.html);
                }).error(function (data) {
                    console.log('Error:', data);
                });
            }

            $('.blog-detail-tabs').mCustomScrollbar({
                callbacks:{
                    onTotalScroll:function(){
                        page++; //page number increment
                        load_more_posts(page); //load content
                    }
                }
            });

            function social_plugins()
            {
                //fb share
                $("#fb").attr('href',"https://www.facebook.com/sharer/sharer.php?u="+escape(canonical.href));

                //twitter share
                $("#twitter").attr('data-text',escape(canonical.href));

                //Google+ share
                $("#gplus").attr('href',"https://plus.google.com/share?url="+escape(canonical.href));

                //Direct Mail
                $("#send").attr('href',"mailto:abc@xyz.com?&body="+escape(canonical.href));

            }

            function meta_data_loader(name,description,keywords,link)
            {
                //meta title
                meta_title.text = title + name;

                //meta description
                meta_description.content = description.replace(/<\/?[^>]+(>|$)/g, "");

                //meta keywords
                meta_keywords = keywords;

                //canonical link
                canonical.href = link;
            }
        });

	</script>

@stop
