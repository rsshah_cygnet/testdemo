@if($default_post !== null)
<div class="resp-tabs-container blog-tab-container"> {{--class blog-detail-tabs--}}
	<div><br/>
		<div class="blog-date">Uploaded {{ $default_post->publish_datetime }}</div>
		<h2 class="blog-title">{{ $default_post->name }}</h2>
		<div class="read-more-link d-i-b"><a href="blog.html">review</a></div>
		<ul class="blog-social d-i-b pull-right">
			<li><a id="fb" href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a id="twitter" href="https://twitter.com/intent/tweet" class="twitter-share-button" data-size="large" data-text="">
			<i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li><a id="gplus" href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
			<li><a id="send" href=""><i class="fa fa-send" aria-hidden="true"></i></a></li>
		</ul>
		<ul class="blog-detail-list clearfix">
			<li class="drop-caps-image">
				<p><span>{{ $default_post->name }}</span></p>
			</li>
			<li>
				<div class="col-md-12 col-sm-12 hidden-xs"><img src="{{ url(config('constants.UPLOADFILE')).'/blog_images/'.$default_post->featured_image }}" alt="Blog Image"></div>
			</li>
			<li>
				{!! $default_post->content !!}
			</li>
		</ul>
	</div>
</div>
@else
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1 class="text-center alert alert-danger"> No Posts Found </h1>
	</div>
</div>
@endif