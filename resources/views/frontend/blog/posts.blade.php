@if(sizeof($posts)>0)
	@foreach($posts as $post)
		<li>
			<div class="row review-button" data-slug="{{ $post->slug }}" id="{{ $post->id }}" data-review-id="{{ $post->id }}">
				<div class="col-md-5 col-sm-4 hidden-xs"><img src="{{ url(config('constants.UPLOADFILE')).'/blog_images/'.$post->featured_image }}" alt="Blog Image"></div>
				<div class="col-md-7 col-sm-8 col-xs-12">
					<div class="blog-date">Uploaded {{ $post->publish_datetime }}
					</div>
					<h2 class="blog-tab-title">{{ $post->name }}</h2>
					<div class="read-small-link"><a href="javascript:void(0)">review</a></div>
				</div>
			</div>
		</li>
	@endforeach
@endif