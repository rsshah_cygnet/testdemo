@extends('frontend.layouts.master')
@section('page-wrapper')


<!-- Page Wrapper Start -->
<section class="page-wrapper register-page-wrapper">


    <!--left side fix section start form here-->
    <div class="full-size-image visible-md visible-lg">


    </div>
    <!--left side fix section end from here-->
    <div class="fluid-content-section">
     <div class="logo-wrapper">
        <a href="/" class="logo">
           <img src="images/logo.png" alt="TEST DEMO" title="TEST DEMO">
       </a>
   </div>

   <div class="title-wrapper">
     <div class="title-left-content">
        <h3>Sign Up to Test demo</h3>
        <small><a href="{{route('frontend.indexpage')}}">Go To Home</a></small>
        <!-- <small><a id="fb_signup" href="javascript:void(0);" >Sign Up with Facebook</a></small>
        
     
        <fb:login-button id="fb_signup_frame" scope="public_profile,email,user_hometown,user_birthday" onlogin="checkLoginState();"></fb:login-button>
            <div id="status"></div> -->
           
    </div>
    <div class="title-right-content">
     <span>Already Registered?&nbsp;&nbsp;<a href="{{route('frontend.login')}}" title="">LOG IN</a>
     </span>
 </div>
</div>
<!-- user type start form here -->
<!-- <div class="fluid-content-section"> -->
<div class="select-user-type">
    <h6>Select type of User</h6>
    <div class="clearfix">
        <div class="row">
            <div class="col-xs-12 col-sm-4 ">
                <div class="box-radio-user custom-radio">
                 <input type="radio" name="tutee" id="tutor-user" checked="checked" onchange="setUserRole(1)">
                 <label for="tutor-user">
                    <img src="images/user-type.png" alt="" title=""/>
                    Tutor
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 ">
         <div class="box-radio-user custom-radio">
             <input type="radio" name="tutee" id="tutee-user" onchange="setUserRole(2)">
             <label for="tutee-user">
                <img src="images/user-type.png" alt="" title=""/>
                Tutee
            </label>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 ">
        <div class="box-radio-user custom-radio">
         <input type="radio" name="tutee" id="both-user" onchange="setUserRole(3)">
         <label for="both-user">
            <img src="images/user-type.png" alt="" title=""/>
            Both
        </label>
    </div>
</div>
</div>
</div>
<ul id="userDetail" class="nav nav-tabs user-detail-section">
    <li  class="active a"><a href="#basicDetail" data-press="tab">1. Basic details </a></li>
    <li><a href="#educationDetail"  id="educationtab" data-press="tab">2. Education/Payment details </a></li>
    <li><a href="#sessionPayment"  id="sessiontab" data-press="tab">3. Session details </a></li>
</ul>
<div id="myTabContent" class="tab-content user-detail-tab">
<!-- 58 17 -->
    <input type="hidden" name="hidden_user_id" id="hidden_user_id" value="">
    <input type="hidden" name="hidden_user_education_id" id="hidden_user_education_id" value="">
    <input type="hidden" name="hidden_user_role" id="hidden_user_role" value="1">

 <!-- First tab start from here -->
 @include('frontend.auth.register_basic')
<!-- Second tab start from here -->
@include('frontend.auth.register_education')
<!-- third tab start from here -->
@include('frontend.auth.register_payment')

            </div>

</div>
</div>
<!-- user type end from here-->


</section>
@endsection
<!-- Page Wrapper End -->

@section('before-styles-end')
<link rel="stylesheet" href="css/perfect-scrollbar.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
@endsection


@section('before-scripts-end')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.16/js/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap-tabcollapse.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script type="text/javascript" src="js/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endsection
@section('after-scripts-end')

<script type="text/javascript">



    $(window).load(function() {
        

 //$('#userDetail a[href="#educationDetail"]').tab('show');
 //$('#userDetail a[href="#sessionPayment"]').tab('show');

//1st registerbasic form
$('#basicForm').on('submit', function(ev){
    ev.preventDefault();
lang_id = $( "#lang_id" ).val();
if (lang_id == null)
lang_id = '';
      hidden_user_role = $('#hidden_user_role').val();
    hidden_user_id = $('#hidden_user_id').val();
    $.ajax({
    url: '{{ route("auth.register_basic") }}',
    type: 'POST',
    data: $( "#basicForm" ).serialize()+"&hidden_user_id="+hidden_user_id+"&hidden_user_role="+hidden_user_role+"&lang_id="+lang_id,
    success: function(data){
        // Success...
        // Loop through all values in outputfromserver
             var data = $.parseJSON(data);

             $('.has-error-text').hide();
             $('.glyphicon-warning-sign').hide();
             $('.form-group').removeClass(" has-error has-feedback ");
             $('form').removeClass("has-feedback");

            if(typeof data.username_error != 'undefined')
            {
                        $( '#username').closest('.form-group').addClass(" has-error has-feedback ");
                        $('.username_error').show();
                        $('.username_error_value').html(data.username_error);
            }
            if(typeof data.email_error != 'undefined')
            {
                        $( '#email').closest('.form-group').addClass(" has-error has-feedback ");
                        $('.email_error').show();
                        $('.email_error_value').html(data.email_error);
            }
            if(typeof data.id != 'undefined')
            {
                        $('#hidden_user_id').val(data.id);
                        $('#userDetail a[href="#educationDetail"]').tab('show');
                        if ($(window).width() <= 768){
                        $('#basicDetail-collapse').removeClass('in');
                        $('#educationDetail-collapse').addClass('in');
                        $('html, body').animate({
                          scrollTop: $("a[href='#educationDetail-collapse'").offset().top
                        }, 10);
                        }


            }

    },
    error: function(data){
         $('.has-error-text').hide();
        $('.glyphicon-warning-sign').hide();
        $('.form-group').removeClass(" has-error has-feedback ");
        $('form').removeClass("has-feedback");

        // Error...
        var errors = data.responseJSON;


        $.each(errors, function(index, value) {
            $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
            $('.'+index+"_error").show();
            $('.'+index+"_error_value").html(value);
           //$(index+"_error").html(value);
        });
    }
});
    });

//session add curriculumn
    $('#SessionAddCurriculum').on('submit', function(ev){
    ev.preventDefault();
    var lastValue = $('#custom_dropdowns_payment div select:last-child').attr('id');
    var type = '';
    //var current_dropdown_id = $this.attr('id');
    var dropdown_array = [];
    dropdownIDsArray = $.map($("div[id*=custom_dropdowns_payment]").find("select"), function (item, index) {
            var next = $(item).attr("id");
            dropdown_array.push(next);
            //alert(next);
        });

 var string ='';
    if(typeof dropdown_array != 'undefined' && !jQuery.isEmptyObject(dropdown_array)){
            $.each(dropdown_array, function(key, value) {
                    if(value != "subject_id_payment" &&  value != "topic_id_payment"){
                        string += $('#'+value+' option:selected').attr("data-name")+" | ";
                    }
            });
     }

    var grade_type = $('#grade_id_payment option:selected').attr('data-type');
    var subject_id_payment = $('#subject_id_payment option:selected').attr('data-name');
    var topic_id_payment = $('#topic_id_payment option:selected').attr('data-name');
    var added_curriculum_number = $('#added_curriculum_number').val();

    if(grade_type == 0)
    type = "lower";
    else
    type = "higher";

    if(!topic_id_payment)
    topic_id_payment = 'N/A';

    hidden_user_id = $('#hidden_user_id').val();
    $.ajax({
    url: '{{ route("auth.register_tutor_curriculum") }}',
    type: 'POST',
    data: $( "#SessionAddCurriculum" ).serialize()+"&hidden_user_id="+hidden_user_id+"&grade_type="+grade_type,
    success: function(data){
        // Success...
        if(typeof data.topic_error != 'undefined')
            {
                        $('.topic_id_payment_error').show();
                        return false;
            }
            else
            {
                $('.topic_id_payment_error').hide();
            }
            if(typeof data.curriculumn_error != 'undefined')
            {
                        $('.curriculumn_added_payment_error').html(data.curriculumn_error);
                        $('.curriculumn_added_payment_error').show();
                        return false;
            }
            else
            {
                $('.curriculumn_added_payment_error').hide();
            }
        $('#added_curriculum_number').val(parseInt(added_curriculum_number) + 1);
        $('.total-curriculum').html('Total '+ (parseInt(added_curriculum_number) + 1));
        $('.added_curriculum_number_error').hide();



var append_html = '<div class="curriculum-list_'+data.id+' clearfix"><div class="curriculum-record-wrapper"><div class="curriculum-record"><dl class="dl-horizontal"><dt>Curriculum</dt>'+string+'</div><div class="curriculum-record"><dl class="dl-horizontal dl-inline-block"><dt>Subject</dt><dd><b>'+subject_id_payment+'</b></dd></dl><dl class="dl-horizontal dl-inline-block"><dt>Topic</dt><dd><b>'+topic_id_payment+'</b></dd></dl></div></div><div class="curriculum-record-delete"><a href="javascript:void(0);"><img src="images/delete-button.png" onclick = "deleteTutorCurriculum('+"'"+data.id+"'" +')" alt=""></a></div></div>';


$('.curriculum-list-wrapper').append(append_html);

//update added number of curriculum

    },
    error: function(data){
         $('.has-error-text').hide();
        $('.glyphicon-warning-sign').hide();
        $('.form-group').removeClass(" has-error has-feedback ");
        $('form').removeClass("has-feedback");

        // Error...
        var errors = data.responseJSON;

        $.each(errors, function(index, value) {
            //$( '#'+index+"_payment").closest('.form-group').addClass(" has-error has-feedback ");
            $('.'+index+"_error").show();
            $('.'+index+"_error_value").html(value);
           //$(index+"_error").html(value);
        });
    }
});
        });
    //end session add curriculumn

    //education 2nd form

        $('#educationForm').on('submit', function(ev){
        ev.preventDefault();
        $('.ajax-loader').show();

        hidden_user_id = $('#hidden_user_id').val();
        hidden_user_education_id = $('#hidden_user_education_id').val();
        hidden_user_role = $('#hidden_user_role').val();
        var grade_type = $('#grade_id option:selected').attr('data-type');

        $.ajax({
        url: '{{ route("auth.register_education") }}',
        type: 'POST',
        data: $( "#educationForm" ).serialize()+"&hidden_user_id="+hidden_user_id+"&hidden_user_education_id="+hidden_user_education_id+"&hidden_user_role="+hidden_user_role+"&grade_type="+grade_type,
        success: function(data){
            $('.ajax-loader').hide();


        var data = $.parseJSON(data);

           $('.has-error-text').hide();
            $('.glyphicon-warning-sign').hide();
            $('.has-error').removeClass('has-error');
            $('.has-feedback').removeClass('has-feedback');

        if(typeof data.error != 'undefined')
        {


          $.each(data.validation_error, function(index,value){
            //show error
            $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
            $('.'+index+"_error").show();
            $('.'+index+"_error_value").html(value);

        });
          return false;
        }
        else
        {

            $('#hidden_user_education_id').val(data.id);
            if(hidden_user_role == 2)
                window.location.href = baseUrl;
            else
            {
                 $('#userDetail a[href="#sessionPayment"]').tab('show');
                if ($(window).width() <= 768){
                        $('#basicDetail-collapse').removeClass('in');
                        $('#educationDetail-collapse').removeClass('in');
                        $('#sessionPayment-collapse').addClass('in');
                        $('html, body').animate({
                           scrollTop: $("a[href='#sessionPayment-collapse'").offset().top
                        }, 10);
                        }
            }

        }

        // Success...

    },
    error: function(data){
        $('.ajax-loader').hide();
         $('.has-error-text').hide();
        $('.glyphicon-warning-sign').hide();
        $('.has-error').removeClass('has-error');
            $('.has-feedback').removeClass('has-feedback');

        // Error...
        var errors = data.responseJSON;

        $.each(errors, function(index, value) {
            $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
            $('.'+index+"_error").show();
            $('.'+index+"_error_value").html(value);
           //$(index+"_error").html(value);
        });
    }
});
});
//end education 2nd form

//session 3rd form
$('#SessionForm').on('submit', function(ev){
$('.ajax-loader').show();
        ev.preventDefault();

        added_curriculum_number = $('#added_curriculum_number').val();

if(added_curriculum_number == 0)
{
            $(".added_curriculum_number_error").show();
            $('.ajax-loader').hide();
             return false;
}

hidden_user_id = $('#hidden_user_id').val();
hidden_user_education_id = $('#hidden_user_education_id').val();

    $.ajax({
    url: '{{ route("auth.register_tutor_education") }}',
    type: 'POST',
    data: $( "form" ).serialize()+"&hidden_user_id="+hidden_user_id+"&hidden_user_education_id="+hidden_user_education_id,
    success: function(data){
        $('.ajax-loader').hide();
        // Success...
         $('.has-error-text').hide();
             $('.glyphicon-warning-sign').hide();
           $('.has-error').removeClass('has-error');
            $('.has-feedback').removeClass('has-feedback');

         var data = $.parseJSON(data);
        if(typeof data.error != 'undefined')
        {

          $.each(data.validation_error, function(index,value){
            //show error
            $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
            $('.'+index+"_error").show();
            $('.'+index+"_error_value").html(value);

        });
          return false;
        }
        else
        {
           // $('#hidden_user_education_id').val(id);


             window.location.href = baseUrl;

        }


    },
    error: function(data){
        $('.ajax-loader').hide();
         $('.has-error-text').hide();
        $('.glyphicon-warning-sign').hide();
        $('.has-error').removeClass('has-error');
            $('.has-feedback').removeClass('has-feedback');

        // Error...
        var errors = data.responseJSON;


        $.each(errors, function(index, value) {
           // $( '#'+index ).closest('.form-group').addClass(" has-error has-feedback ");
            $('.'+index+"_error").show();
            $('.'+index+"_error_value").html(value);
           //$(index+"_error").html(value);
        });
    }
});

 });
//End session 3rd form


        $('.captcha_payment_div').html('<img src="<?php echo captcha_src(); ?>" alt="captcha" class="captcha-img-payment" data-refresh-config="default">');

 //$('#userDetail a[href="#sessionPayment"]').tab('show');

        $('body').on('click','img.captcha-img',function () {
            var captcha = $(this);
            var config = captcha.data('refresh-config');
            $.ajax({
                method: 'GET',
                url: '/get_captcha/' + config,
            }).done(function (response) {
                captcha.prop('src', response);
            });

         });

            $('body').on('click','img.captcha-img-payment',function () {
                    var captcha = $(this);
                    var config = captcha.data('refresh-config');
                    $.ajax({
                        method: 'GET',
                        url: '/get_captcha/' + config,
                    }).done(function (response) {
                        captcha.prop('src', response);
                 });
            });

        $('.education_back').on('click',function()
        {
            if ($(window).width() <= 768){
                $('#basicDetail-collapse').addClass('in');
                $('#educationDetail-collapse').removeClass('in');
                $('#sessionPayment-collapse').removeClass('in');
            }
             $('#userDetail a[href="#basicDetail"]').tab('show');
        });

        $('.payment_back').on('click',function()
        {
            if ($(window).width() <= 768){
                $('#basicDetail-collapse').removeClass('in');
                $('#educationDetail-collapse').addClass('in');
                $('#sessionPayment-collapse').removeClass('in');
            }
            $('#userDetail a[href="#educationDetail"]').tab('show');
        });

        $('#password-eye').click(function(){
           var password = $('#password').attr('type');
           if(password == 'password')
           {
                $('#password').attr('type', 'text');
           }
           else if(password == 'text')
           {
                $('#password').attr('type', 'password');
           }
        });

        $('#cpassword-eye').click(function(){
            var cpassword = $('#cpassword').attr('type');
            if(cpassword == 'password'){
                $('#cpassword').attr('type', 'text');
            }
            else if(cpassword == 'text'){
                $('#cpassword').attr('type', 'password');
            }
        });


        $('#dob').daterangepicker({
            "singleDatePicker": true,
            showDropdowns: true,
             maxDate: new Date()
        });
        $('#dob').val('');
    });

</script>


<script>

function deleteTutorCurriculum(id)
{

    $.ajax({
    url: '{{ route("auth.delete_tutor_curriculum") }}',
    type: 'POST',
    data: "id="+id+"&_token="+$('#csrf_token_header').val(),
    success: function(data){
        // Success...
         $('.curriculum-list_'+id).remove();
        var added_curriculum_number = $('#added_curriculum_number').val();
        $('#added_curriculum_number').val(parseInt(added_curriculum_number) - 1);
        $('.total-curriculum').html('Total '+ (parseInt(added_curriculum_number) - 1));
         var string = '';

    },
    error: function(data){

    }
});
}

function setUserRole(value)
{
    $('#userDetail a[href="#basicDetail"]').tab('show');
    if(value == 2)
    {
        $('#sessiontab').hide();
        $('.captcha_tutee').show();
        $('.captcha-img-payment').remove();
        $('.captcha_div').html('<img src="<?php echo captcha_src(); ?>" alt="captcha" class="captcha-img" data-refresh-config="default">');

    }
    else
    {
        $('#sessiontab').show();
        $('.captcha_tutee').hide();
        $('.captcha-img').remove();
        $('.captcha_payment_div').html('<img src="<?php echo captcha_src(); ?>" alt="captcha" class="captcha-img-payment" data-refresh-config="default">');

    }
    $('#hidden_user_role').val(value);
}

function browse_photo()
{
    $('#user_photo').click();
}

function upload_photo()
{

  var file_data = $('#user_photo').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajaxSetup({
            headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
        });
        $('.file_error').hide();
        $.ajax({
            url: "{{route("auth.register_user_photo") }}", // point to server-side PHP script
            data: form_data,
            type: 'POST',
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData: false,
            success: function (data) {
                if (data.fail) {
                  // $('#user_img').attr('src','uploads/user/default.jpg');
                    alert(data.errors['file']);
                }
                else {
                    filename = data;
                    $('#user_img').attr('src','uploads/user/'+filename);
                    $('#photo_hidden').val(filename);

                }
            },
           error: function (xhr, status, error) {
                var json = JSON.parse(xhr.responseText);
                var error = json.file; // or json["Data"]
                $('.file_error').show();
                 $('.file_error_value').html(error);
            }
        });
}

function changeCurriculumn()
{
   
    var grade_type = $('#qualification_id option:selected').val();
    if(grade_type == 1 || grade_type == 2 || grade_type == 3)
    {
        $('.user_curriculum_div').show();
        $('.user_educational_curriculum').attr('required','required');
           $('#subject_name').removeAttr('required');
        $('.user_subject_name_div').hide();

    }
    else
    {
        $('.user_curriculum_div').hide();
        $('.user_educational_curriculum').removeAttr('required');
        $('#subject_name').attr('required','required');
        $('#curriculum_id').val('').trigger('change');;
        $('.user_subject_name_div').show();
    }
}



</script>

<!-- Facebook login script starts-->
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
   
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '105326336717430',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use graph api version 2.8
  });

  
  FB.getLoginStatus(function(response) {
    //statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.login(function(response) {
        console.log('login');
        console.log(response);
        FB.api('/me',{fields: "id,about,age_range,picture,birthday,email,first_name,gender,last_name,name"}, function(response) {
        signupFb(response);
    });
     // handle the response
    }, {scope: 'public_profile,email'});
    
  }

  function signupFb(response){
    if(response.gender == 'male'){
            var gender = 0;
        }else if(response.gender == 'female'){
            var gender = 1;
        }
        $.ajax({
            type: 'POST',
            url: '{{ route("fb.register") }}',
            data: {
                first_name: response.first_name,
                last_name: response.last_name,
                dob: response.birthday,
                email: response.email,
                gender: gender,
                _token: $('#csrf_token_header').val()
            },
            success: function(data) {
                data = $.parseJSON(data);
                if(data.status == 'success'){
                     if(data.role == 2){
                        window.location = '/tutee-dashboard';
                     }else{
                        window.location = '/tutor-dashboard';
                     }
                    
                }
                
            }
        });
  }

  $('body').on('click','#fb_signup',function(){
      testAPI();
  })
</script>
<!-- Facebook login script ends -->
@endsection


