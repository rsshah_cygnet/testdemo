<?php
    use Illuminate\Support\Facades\Route;
    $currentPath = Route::getFacadeRoot()->current()->uri();
    if($currentPath == "password/email") {
        $route = 'password/reset/';
    } else {
        $route = 'admin/password/reset/';
    }
?>

{{ trans('strings.emails.auth.reset_password') }}: <a href="{{ url($route.$token) }}">Your Password Reset Link</a>