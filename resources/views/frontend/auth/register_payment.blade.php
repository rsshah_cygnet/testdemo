<div class="tab-pane fade" id="sessionPayment">

  
  <div class="row">
    <div class="col-xs-12 col-sm-6">
{!! Form::open(['url' => 'register', 'class' => '','onsubmit'=>'','id' => 'SessionAddCurriculum']) !!}
    <div class="row">
      <div class="col-xs-12 select-accordion-wrapper add-curriculum-wrapper PaymentPart">
        <div class="form-group">
            <label>Select Curriculum <span class="astrick">*</span></label>
            {!! Form::select('curriculum_id_payment', ['' => 'Select'] + $curriculum  ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control select-curriculum' , 'id' => 'curriculum_id_payment','onchange'=>'getCurriculumHierarchyForPayment("curriculum_id")']) !!}
          <div class="col-xs-12 select-accordion ">
             <div class="row" id="custom_dropdowns_payment">
             </div>
             <div class="add-curriculum">
                {!! Form::submit('Add', ['class' => 'text-center']) !!}
             </div>
          </div>
        </div>
      </div>
    </div>
</div>

{!! Form::close() !!}

{!! Form::open(['url' => 'register', 'class' => '','onsubmit'=>'','id' => 'SessionForm']) !!}
<div class="col-xs-12 col-sm-6">
 <div class="row">
   <div class="col-xs-12">
    <div class="form-group">
      <label>Added Curriculum</label>
      <span class="total-curriculum">Total 0</span>
      <div class="curriculum-list-wrapper perfect-scroll">
</div>

<input type="hidden" name="added_curriculum_number" id="added_curriculum_number" value="0">
      <span class="text-right display-block has-error-text added_curriculum_number_error added_curriculum_number_error_value" style="display: none">Pleas add atleast one curriculum</span>
      <span class="text-right display-block has-error-text curriculumn_added_payment_error curriculumn_added_payment_error_value" style="display: none">This heirarchy is already added before</span>
</div>

</div>

<div class="col-xs-12">
 <div class="form-group">
  <label for="userPaypalDetail">Enter Captcha <span class="astrick">*</span></label>
  <div class="input-group captcha-image">
     <div class="input-group-addon captcha_payment_div">
       <!-- <img src="{{ captcha_src() }}" alt="captcha" class="captcha-img-payment" data-refresh-config="default"> -->
     </div>
      <input type="text" class="form-control" id="userPaypalDetail" name="captcha_payment" placeholder="Type the text" required="required">

</div>
<span class="text-right display-block has-error-text captcha_payment_error captcha_payment_error_value" style="display: none">The paypal registered email must be a valid email address.</span>
 </div>
</div>
</div>

</div>

<div class="row">
 <div class="col-xs-12 form-group action-button">
   <div class="row">
     <div class="col-xs-12 col-sm-6">
      <a href="javascript:void(0);" class="arrow-secondry-btn payment_back"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>

    </div>
    <div class="col-xs-12 col-sm-6">

     <!-- <a href="javascript:void(0);" class="arrow-primary-btn"  onclick = "submit_register_payment();" >Next <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>  -->

     {!! Form::submit(trans('labels.frontend.auth.register_basic'), ['class' => 'arrow-primary-btn']) !!}
   </div>
 </div>
</div>
</div>

</div>

{!! Form::close() !!}

</div>