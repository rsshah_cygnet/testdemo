@extends('frontend.layouts.master')

@section('page-wrapper')
<div class="page-wrapper">
    <div class="table-display">
    <!-- Left side image part start form here -->
    @include('frontend.includes.leftside-sidebar')
    <!-- Left side image part end form here -->

    <!-- right side image part start form here -->
    <div class="login-user">
      <div class="login-page-container"> <a href="#" class="logo"> <img src="{{url('/')}}/images/logo.png" alt="TUTE ME" title="TUTE ME"> </a>
        <div class="login-wrapper clearfix">
          <div class="title-wrapper">
          @if (session('status'))
          <h3>Forgot your password?</h3>
            <small>Enter your email address to reset your password</small> </div>
            <!-- success username -->
          <div class="send-username-success">

            <div class="success-icon">
                <img src="{{url('/')}}/images/success-message.png" style="" alt="" title="">
            </div>
            <div class="success-content">
                <p>
               Password is successfully sent to your email account</br>
                @if(Session::has('email'))
                <strong>{{Session::get('email')}}</strong>
                @endif
                </p>
            </div>
          </div>
          <!-- success username -->
          <div class="clearfix"></div>
          <a href="{{route('frontend.login')}}" class="btn login-btn col-xs-12"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back to LOGIN</a>

        </div>
      </div>
          @else
            <h3>Forgot your password?</h3>
            <small>Enter your email address to reset your password</small> </div>
         <!-- <form class="clearfix login-form" > -->
         @if (session('message'))
            <div class="alert alert-danger">
                {{Session::get('message')}}
            </div>
         @endif
        @include('includes.partials.messages')

          {{ Form::open(array('url' => 'password/email', 'class' => 'clearfix login-form')) }}
           {{--  <div class="form-group">
              <label for="username">Username <span class="astrick">*</span></label>
              <a href="{{route('username.reset')}}" title="Forgot username?" class="pull-right hidden-xs">Forgot username?</a>
              {{ Form::text('username', null, ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'Enter your username'])}}
            </div> --}}
            <div class="form-group">
              <label for="usermailid">Email Address <span class="astrick">*</span></label>
           <!--   <input type="text" class="form-control" id="usermailid" placeholder="Enter your email address"> -->
                {{ Form::email('email',null, ['class' => 'form-control', 'id' => 'usermailid', 'placeholder' => 'Enter your email address'])}}
            </div>
            <div class="form-group clearfix forgot-password-button">
                <button type="submit" class="btn login-btn col-xs-12 col-sm-5">SUBMIT</button>
                <a href="{{route('frontend.login')}}" class="btn col-xs-12 col-sm-5 pull-right btn-grey forgot-btn-grey"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Back</a>
            </div>
            {{ Form::close() }}
          <!-- </form> -->
          <div class="text-center col-xs-12 sign-up visible-xs form-group"> <span>Can't access account?&nbsp;  <br/><a href=""> Forgot UserName </a></span> </div>
          <div class="text-center col-xs-12 sign-up"> <span>Don't have an account?&nbsp;<a href="{{url('/register')}}"> SIGN UP </a></span> </div>
        </div>
      </div>
      @endif
      <div class="copyright-login"> Copyright &copy; 2010-{{date('Y')}}  TUTE ME. </div>
    </div>
    <!-- right side image part start form here -->
  </div>
</div>
@endsection

@section('before-styles-end')
<link rel="stylesheet" href="/css/perfect-scrollbar.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="/css/daterangepicker.css">
@endsection

@section('before-scripts-end')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.16/js/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-tabcollapse.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script type="text/javascript" src="/js/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

@endsection