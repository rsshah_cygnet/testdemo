@extends('frontend.layouts.master')

@section('page-wrapper')
<div class="page-wrapper">
  <div class="table-display">
    <!-- Left side image part start form here -->
    @include('frontend.includes.leftside-sidebar')
    <!-- Left side image part end form here -->

    <!-- right side image part start form here -->
    <div class="login-user">
      <div class="login-page-container"> <a href="#" class="logo"> <img src="{{url('/')}}/images/logo.png" alt="TUTE ME" title="TUTE ME"> </a>
        <div class="login-wrapper clearfix">
          <div class="title-wrapper">
            <h3>Reset Password</h3>
            <small>Enter your new password</small> 
          </div>
          @include('includes.partials.messages')
          @if(Session::has('status'))
          <div class="alert alert-success">
            {{Session::get('status')}}
          </div>
          @endif
          {!! Form::open(['url' => 'password/reset', 'class' => 'm-t-15 clearfix login-form']) !!}
          <input type="hidden" name="token" value="{{ $token }}">
          <div class="form-group">
            <label for="usermailid">Email Address <span class="astrick">*</span></label>
            <!--   <input type="text" class="form-control" id="usermailid" placeholder="Enter your email address"> -->
            <div class="form-group">
              {{ Form::email('email',null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'autocomplete' => 'off', 'id' => 'useremail'])}}
            <!-- <div class="input-group-addon">
                <i class="fa fa-eye" aria-hidden="true" id="email-checkbox" data-status="unchecked"></i>
              </div> -->
            </div>
          </div>
          <div class="form-group">
            <label for="password">Password<span class="astrick">*</span></label>
            <!--   <input type="text" class="form-control" id="usermailid" placeholder="Enter your email address"> -->
            <div class="input-group">
              {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password'), 'id' => 'password' ])}}
              <div class="input-group-addon"><i class="fa fa-eye" id="psd-checkbox" data-status="unchecked" aria-hidden="true"></i></div>
            </div>
          </div>
          <div class="form-group">
            <label for="Password Confirmation">Password Confirmation<span class="astrick">*</span></label>
            <!--   <input type="text" class="form-control" id="usermailid" placeholder="Enter your email address"> -->
            <div class="input-group">
              {{ Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'cpassword', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')])}}
              <div class="input-group-addon" id="eye"><i class="fa fa-eye" id="cpsd-checkbox" data-status="unchecked" aria-hidden="true"></i></div>
            </div>
          </div>
          {{--  <div class="form-group">
          <label for="username">Username <span class="astrick">*</span></label>
          <a href="#" title="Forgot username?" class="pull-right hidden-xs">Forgot username?</a>
          {{ Form::text('username', null, ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'Enter your username'])}}
        </div> --}}
        <div class="form-group clearfix forgot-password-button">
          <button type="submit" class="btn login-btn col-xs-12 col-sm-5">SUBMIT</button>
          <a href="{{route('frontend.login')}}" class="btn col-xs-12 col-sm-5 pull-right btn-grey">CANCEL</a>
        </div>
        {{ Form::close() }}
        <!-- </form> -->
        <div class="text-center col-xs-12 sign-up visible-xs form-group"> <span>Can't access account?&nbsp;  <br/><a href=""> Forgot UserName </a></span> </div>
        <div class="text-center col-xs-12 sign-up"> <span>Don't have an account?&nbsp;<a href="{{url('/register')}}"> SIGN UP </a></span> </div>
      </div>
    </div>

    <div class="copyright-login"> Copyright &copy; 2010-{{date('Y')}}  TUTE ME. </div>
  </div>
  <!-- right side image part start form here -->
</div>
</div>
@endsection

@section('before-scripts-end')
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.16/js/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-tabcollapse.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script type="text/javascript" src="/js/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
  $(document).ready(function(){
    $('#psd-checkbox').on('click', function()
    {
       // alert($(this).attr('data-status'));
       if($(this).attr('data-status') == "unchecked")
       {
        $(this).attr('data-status', 'checked');
        $('#password').attr('type',"text");
      }
      else
      {
        $(this).attr('data-status', 'unchecked');
        $('#password').attr('type',"password");
      }
    });
    $('#cpsd-checkbox').on('click', function()
    {
       // alert($(this).attr('data-status'));
       if($(this).attr('data-status') == "unchecked")
       {
        $(this).attr('data-status', 'checked');
        $('#cpassword').attr('type',"text");
      }
      else
      {
        $(this).attr('data-status', 'unchecked');
        $('#cpassword').attr('type',"password");
      }
    });
    $('#email-checkbox').on('click', function()
    {
       // alert($(this).attr('data-status'));
       if($(this).attr('data-status') == "unchecked")
       {
        $(this).attr('data-status', 'checked');
        $('#useremail').attr('type',"text");
      }
      else
      {
        $(this).attr('data-status', 'unchecked');
        $('#useremail').attr('type',"password");
      }
    });
  });
</script>
@endsection

@section('after-scripts-end')

@endsection