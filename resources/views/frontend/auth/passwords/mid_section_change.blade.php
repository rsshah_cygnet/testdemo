
    <div class="container-fluid">
      <!-- Left aside start from here -->
      <?php if((isset($sessionData['current_role'])) && $sessionData['current_role'] != "" && $sessionData['current_role'] == '2'){ ?>
    @include('frontend.includes.tutee-leftsidebar')
    <?php }else{?>
    @include('frontend.includes.tutor-leftsidebar')
    <?php } ?>
      <!-- Left aside end from here -->
      <!-- Right side start form here-->
      <div class="content-wrapper">
        <div class="content">
          <div class="row">

            <div class="col-xs-12 col-lg-12">
                <div class="box">
                    <div class="box-header clearfix with-border">
                        <div class="pull-left box-title">
                            <h3 class="">Settings</h3>
                        </div>

                    </div>
                    <div class="box-body">
                        <h2 class="color-title heading-f-s-20">Change Password</h2>
                         <div id="Msg" style="display: none;"></div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-8">
                            @include('includes.partials.messages')
                                  {!! Form::open(['route' => 'auth.password.update', 'class'=>"clearfix login-form", 'onclick' => 'return false','id' => 'change_password_form']) !!}
                                  <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                    <div class="form-group">
                                        <div class="form-group">
                                                        <label for="oldpassword">Old Password <span class="astrick">*</span></label>
                                                        <div class="input-group">
                                                         {!! Form::input('password', 'old_password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.old_password'), 'id' => 'oldpsd']) !!}
                                                            <div class="input-group-addon"><i class="fa fa-eye" aria-hidden="true" id="oldpsd-checkbox" data-status="unchecked"></i></div>
                                                     <!--         <span class="glyphicon glyphicon-warning-sign form-control-feedback old_password_error" aria-hidden="true" style="display: none"></span> -->
                                                              
                                                        </div>
<span class="text-right display-block has-error-text old_password_error
old_password_error_value" style="display: none"></span>
</div>

                                                    <div class="form-group">
                                                        <label for="newpassword">New Password <span class="astrick">*</span></label>
                                                        <div class="input-group">
                                                             {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.new_password'), 'id' => 'newpsd']) !!}
                                                            <div class="input-group-addon"><i class="fa fa-eye" aria-hidden="true" id="newpsd-checkbox" data-status="unchecked"></i></div>
                                                            <!--  <span class="glyphicon glyphicon-warning-sign form-control-feedback password_error" aria-hidden="true" style="display: none"></span> -->
                                                            
                                                        </div>
                                                         <span class="text-right display-block has-error-text password_error password_error_value" style="display: none"></span>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="conformpassword">Confirm Password <span class="astrick">*</span></label>
                                                        <div class="input-group">
                                                            {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.new_password_confirmation'), 'id' => 'confirmpsd']) !!}
                                                            <div class="input-group-addon"><i class="fa fa-eye" aria-hidden="true" id="confirmpsd-checkbox" data-status="unchecked"></i></div>
                                                          <!--   <span class="glyphicon glyphicon-warning-sign form-control-feedback password_confirmation_error" aria-hidden="true" style="display: none"></span> -->
                                                             
                                                        </div>
                                                         <span class="text-right display-block has-error-text password_confirmation_error password_confirmation_error_value" style="display: none"></span>
                                                    </div>

                                                    <div class="form-group" style=" margin-top: 30px;">
  <?php if((isset($sessionData['current_role'])) && $sessionData['current_role'] != "" && $sessionData['current_role'] == '2'){ ?>    
               <button type="submit" id="change_pass_submit" class="btn login-btn setting-btn-pad">SUBMIT</button>
          <?php }else{ ?>
                 <button type="submit" id="change_pass_submit" style="background: #f79237 none repeat scroll 0 0;" class="btn login-btn setting-btn-pad">SUBMIT</button>
        <?php } ?>    
     <a href="javascript:void(0);" id="cancel_changepassbtn"><button type="button" class="btn setting-btn-pad btn-grey">CANCEL</button><a/>


                                                       
                                                    </div>
                                    {!! Form::close() !!}
        </div>
    <!--panel body-->

</div>
      </div>
      <!-- Right side end form here-->
      </div>
    </div>
</div>
</div>
