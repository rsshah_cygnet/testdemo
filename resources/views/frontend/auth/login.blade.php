<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="images/favicon.ico">
<title>::Welcome to TEST DEMO::</title>
<!--css starts-->
  <input type="hidden" id="csrf_token_header" value="<?php echo e(csrf_token()); ?>" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700,900" >
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/base.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<!--css ends-->
<!--Script starts-->
<script src="js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="js/modernizr-3.0.0.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!---->
 <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!--Script ends-->
<script src="http://connect.facebook.net/en_US/all.js"></script>
</head>
<body class="login-page">
<section class="page-wrapper">
  	<div class="table-display">
  	<!-- Left side image part start form here -->

    @include('frontend.includes.leftside-sidebar')

    <!-- Left side image part end form here -->

    <!-- right side image part start form here -->
    <div class="login-user">
      <div class="login-page-container"> <a href="/" class="logo"> <img src="images/logo.png" alt="TEST DEMO" title="TEST DEMO"> </a>
        <div class="login-wrapper clearfix">
          <div class="title-wrapper">
            <h3>Sign in to Testdemo.</h3>
            <small>Enter your details below. </small> </div>
           <!--  <fb:login-button scope="public_profile,email,user_hometown,user_birthday" onlogin="checkLoginState();"></fb:login-button> -->
           <!--  <a id="fb_login" href="javascript:void(0);" >Login with Facebook</a> -->
            <div id="status"></div>
            @include('includes.partials.messages')
            @if(Session::has('status'))
                 <div class="alert alert-success">
                    {{Session::get('status')}}
                 </div>
                 {{\Session::forget('status')}}
            @endif
            @if(Session::has('InvalidActivation'))
                 <div class="alert alert-danger">
                    {{Session::get('InvalidActivation')}}
                 </div>
                 {{\Session::forget('InvalidActivation')}}
            @endif
            @if(Session::has('activationLinkSend'))
                  <div class="alert alert-success">
                    {{Session::get('activationLinkSend')}}
                 </div>
                 {{\Session::forget('activationLinkSend')}}
            @endif
          {{ Form::open(['route' => 'auth.login', 'class' => 'clearfix login-form']) }}
            <div class="form-group">
              <label for="username">E-mail <span class="astrick">*</span></label>

              
              @if(isset($cookieData[0]))
              {{ Form::input('text', 'email', $cookieData[0], ['class' => 'form-control', 'placeholder' => 'Enter your Email', 'autocomplete' => 'off']) }}
              @else

             

              {{ Form::input('text', 'email', null, ['class' => 'form-control', 'placeholder' => 'Enter your Email', 'autocomplete' => 'off']) }}
              @endif
            </div>
            <div class="form-group">
              <label for="userpassword">Password <span class="astrick">*</span></label>
              <a href="{{route('password.reset.form')}}" title="Forgot password?" class="pull-right hidden-xs">Forgot password?</a>
              <div class="input-group">
               @if(isset($cookieData[1]))
               {{ Form::input('password', 'password', $cookieData[1], ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Enter your Password' ]) }}
               @else
               {{ Form::input('password', 'password', null, ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Enter your Password' ]) }}
                @endif
                <div class="input-group-addon"><i class="fa fa-eye abc" aria-hidden="true" id="psd-checkbox" data-status="unchecked"></i></div>
              </div>
            </div>
            <div class="form-group">
                <div class="checkbox checkbox-danger">
                  <input type="checkbox" id="remember" value="1" name="remember">
                  <label for="remember">Remember me </label>
                </div>
            </div>
            <button type="submit" class="btn btn-default login-btn col-xs-12" style="width: 100%;">login</button>
            {{ Form::close() }}
          <div class="text-center col-xs-12 sign-up visible-xs form-group"> <span>Can't access account?&nbsp;  <br/><a href="{{route('password.reset.form')}}"> Forgot Password </a> </span> </div>
          <div class="text-center col-xs-12 sign-up"> <span>Don't have an account?&nbsp;<a href="{{route('auth.register')}}"> SIGN UP </a></span><br>
          <span><a href="/" title="Go to home">Go to home</a></span>
           </div>
        </div>
      </div>
      <div class="copyright-login">
      	Copyright &copy; 2010-2016  TEST DEMO.
      </div>
    </div>
    <!-- right side image part start form here -->
    </div>
</section>

<script>
$(document).ready(function(){
    // alert("imdad");
    $('#psd-checkbox').on('click', function()
    {
       // alert($(this).attr('data-status'));
       if($(this).attr('data-status') == "unchecked")
       {
          $(this).attr('data-status', 'checked');
          $('#password').attr('type',"text");
       }
        else
        {
          $(this).attr('data-status', 'unchecked');
          $('#password').attr('type',"password");
        }
    });

    });
</script>
<!-- Facebook login script starts-->
<script>
  function statusChangeCallback(response) {
  
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      loginFbAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '105326336717430',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use graph api version 2.8
  });

  
  FB.getLoginStatus(function(response) {
    //statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function loginFbAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.login(function(response) {
        console.log('login');
        console.log(response);
        FB.api('/me',{fields: "id,about,age_range,picture,birthday,email,first_name,gender,last_name,name"}, function(response) {
        loginFb(response);
    });
     // handle the response
    }, {scope: 'public_profile,email'});
    
  }

  function loginFb(response){
    if(response.gender == 'male'){
            var gender = 0;
        }else if(response.gender == 'female'){
            var gender = 1;
        }
        $.ajax({
            type: 'POST',
            url: '{{ route("fb.register") }}',
            data: {
                first_name: response.first_name,
                last_name: response.last_name,
                dob: response.birthday,
                email: response.email,
                gender: gender,
                _token: $('#csrf_token_header').val()
            },
            success: function(data) {
                data = $.parseJSON(data);
                if(data.status == 'success'){
                     if(data.role == 2){
                        window.location = '/tutee-dashboard';
                     }else{
                        window.location = '/tutor-dashboard';
                     }
                    
                }
                
            }
        });
  }

  $('body').on('click','#fb_login',function(){
      loginFbAPI();
  })
</script>
<!-- Facebook login script ends -->
