<div class="tab-pane fade" id="educationDetail">
  {!! Form::open(['url' => 'register', 'class' => '','onsubmit'=>'','id' => 'educationForm']) !!}
  <input type="hidden" id="is_registration" name="is_registration" value="register">
  <div class="row">
    <div class="col-xs-12 col-sm-6">
     <div class="form-group">
      <label>Select Qualification <span class="astrick">*</span></label>
      {!! Form::select('qualification_id', ['' => 'Select'] + $qualification  ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control' , 'id' => 'qualification_id','required'=>'required','onchange'=>'changeCurriculumn()']) !!}
      <span class="glyphicon glyphicon-warning-sign form-control-feedback last_selected_curriculum_error" aria-hidden="true" style="display: none"></span>
      <span class="text-right display-block has-error-text qualification_id_error qualification_id_error_value" style="display: none">Username already exist</span>
    </div>
    <div class="form-group ">
      <label for="userEducationDetail">School/College/University Details</label>
      <textarea id="userEducationDetail" name="educational_details" class="form-control" placeholder="Enter your detail"></textarea>
    </div>
    <div class="form-group">
      <label for="userOccupationDetail">Occupational Details (Optional)</label>
      <textarea id="userOccupationDetail" name="occupation_details" class="form-control" placeholder="Enter your detail"></textarea>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 select-accordion-wrapper educationDetailPart" >
    <div class="form-group user_curriculum_div">
      <label>Select Curriculum <span class="astrick">*</span></label>
      {!! Form::select('curriculum_id', ['' => 'Select'] + $curriculum  ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control select-curriculum user_educational_curriculum' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")','required'=>'required']) !!} 
      <span class="glyphicon glyphicon-warning-sign form-control-feedback curriculum_id_error" aria-hidden="true" style="display: none"></span>
      <span class="text-right display-block has-error-text curriculum_id_error curriculum_id_error_value" style="display: none">Username already exist</span>

      <div class="col-xs-12 select-accordion ">
       <div class="row" id="custom_dropdowns">
         <div class="clearfix form-group education_div">
           <label class="col-xs-12 col-sm-6 label-title">Education System</label>
           <div class="col-xs-12 col-sm-6">
             {!! Form::select('education_id', ['' => 'Select'] ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control custom-select' , 'id' => 'eductional_systems_id']) !!}
           </div>
           
           
         </div>
         <div class="clearfix form-group level_div">
           <label class="col-xs-12 col-sm-6 label-title">Level</label>
           <div class="col-xs-12 col-sm-6">
             {!! Form::select('level_id', ['' => 'Select'] ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control custom-select' , 'id' => 'level_id']) !!}
             
           </div>
           
           
         </div>
         <div class="clearfix form-group program_div">
           <label class="col-xs-12 col-sm-6 label-title">Program</label>
           <div class="col-xs-12 col-sm-6">
            {!! Form::select('program_id', ['' => 'Select'] ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control custom-select' , 'id' => 'program_id']) !!}      
          </div>
          
          
        </div>
        <div class="clearfix form-group grade_div">
         <label class="col-xs-12 col-sm-6 label-title">Grade</label>
         <div class="col-xs-12 col-sm-6">
           {!! Form::select('grade_id', ['' => 'Select'] ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control custom-select' , 'id' => 'grade_id']) !!}      
         </div>
       </div>

       <div class="clearfix form-group subject_div">
         <label class="col-xs-12 col-sm-6 label-title">Subject</label>
         <div class="col-xs-12 col-sm-6">
           {!! Form::select('subject_id', ['' => 'Select'] ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control custom-select' , 'id' => 'subject_id']) !!}      
         </div>
       </div>
     </div>
   </div>
 </div>

 <div class="form-group user_subject_name_div" style="display: none">
  <label for="userSubjectName">Subject Name<span class="astrick">*</span></label>
  {{ Form::input('subject_name', 'subject_name', null, ['class' => 'form-control', 'placeholder' => 'Enter your Subject Name','id' => 'subject_name']) }}
  <span class="glyphicon glyphicon-warning-sign form-control-feedback subject_name_error" aria-hidden="true" style="display: none"></span>
  <span class="text-right display-block has-error-text subject_name_error subject_name_error_value" style="display: none">The Subject name is required</span>
</div>

<div class="form-group">
  <label for="userPaypalDetail">PayPal details  <span class="astrick">*</span></label>
  <div class="input-group">
    <input type="text" class="form-control" id="userPaypalDetail"  name="paypal_registered_email_payment" placeholder="Enter PayPal registered Email ID" required="required">

    <div class="input-group-addon"><i class="fa fa-paypal fa-2x" aria-hidden="true"></i></div>
  </div>
  <span class="help-text-bottom"> <a href="javascript:void(0);">Where system can transfer (credit or debit) payment</a> </span>
  <span class="text-right display-block has-error-text paypal_registered_email_payment_error paypal_registered_email_payment_error_value" style="display: none">The paypal registered email must be a valid email address.</span>
</div>

<div class="form-group captcha_tutee" style="display: none">
 <div class="form-group">
  <label for="userPaypalDetail">Enter Captcha <span class="astrick">*</span></label>
  <div class="input-group captcha-image">
   <div class="input-group-addon captcha_div">
     
     <!-- <img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default"> -->
   </div>
   <input type="text" class="form-control" id="userPaypalDetail" name="captcha" placeholder="Type the text">
 </div>
 <span class="text-right display-block has-error-text captcha_error captcha_error_value" style="display: none">Captcha validation fail.</span>
</div>
</div>

</div>
</div>
<div class="row">
 <div class="col-xs-12 form-group action-button">
   <div class="row">
     <div class="col-xs-12 col-sm-6">
       
      <a href="javascript:void(0);" class="arrow-secondry-btn education_back"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a> 
      
    </div>
    <div class="col-xs-12 col-sm-6">
      
     <!-- {!! Form::submit(trans('labels.frontend.auth.register_basic'), ['class' => 'arrow-primary-btn','onclick' => 'submit_register_educational()']) !!} -->

     {!! Form::submit(trans('labels.frontend.auth.register_basic'), ['class' => 'arrow-primary-btn']) !!}
     
   </div>
 </div>
 {!! Form::close() !!}
</div>
</div>
</div>