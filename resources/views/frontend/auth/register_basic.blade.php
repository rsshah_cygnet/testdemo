
<div class="tab-pane fade in active user-basic-tab" id="basicDetail">

 <div class="row">


   <div class="user-image-wrapper col-xs-12 col-md-3 ">
     <div class="basic-image-upload-wrapper">
       <img src="images/upload-image-sample.png" alt="" title="" id="user_img" />
     </div>
     <a href="javascript:void(0);" onclick="browse_photo()" class="upload-button">Select Photo</a>
     {!! Form::open(['url' => 'register_user_photo', 'class' => '','id'=>'user_photo_form','enctype' => 'multipart/form-data' ,'files'=>true,'method'=>'post']) !!}
     <input type="file" name="user_photo" id="user_photo" value="Select Photo" class="upload-button" onchange="upload_photo()" style="visibility: hidden;"></input>
     <span class="text-right display-block has-error-text file_error file_error_value" style="display: none">It should be valid image.</span>

     {!! Form::close() !!}
   </div>


   <div class="col-xs-12 col-sm-6 col-md-5">
     {!! Form::open(['url' => 'register', 'class' => '','onsubmit'=>'return false','id'=>'basicForm']) !!}
     <div class="row">
      <div class="col-xs-12">
        <div class="form-group">
          <label for="userFirstName">First Name <span class="astrick">*</span></label>
          {{ Form::input('text', 'first_name', null, ['class' => 'form-control', 'placeholder' => 'Enter First Name','id' => 'first_name','required'=>'required']) }}
          <span class="glyphicon glyphicon-warning-sign form-control-feedback first_name_error" aria-hidden="true" style="display: none"></span>
          <span class="text-right display-block has-error-text first_name_error first_name_error_value" style="display: none">Username already exist</span>
        </div>
      </div>
      <div class="col-xs-12">
        <div class="form-group">
          <label for="userLastName">Last Name <span class="astrick">*</span></label>
          {{ Form::input('text', 'last_name', null, ['class' => 'form-control', 'placeholder' => 'Enter Last Name','id' => 'last_name','required'=>'required']) }}
          <span class="glyphicon glyphicon-warning-sign form-control-feedback last_name_error" aria-hidden="true" style="display: none"></span>
          <span class="text-right display-block has-error-text last_name_error last_name_error_value" style="display: none">Username already exist</span>

        </div>
      </div>
      <div class="col-xs-12 register-gender">
        <div class="form-group">
         <div>
          <label for="gender">Gender <span class="astrick">*</span></label>
        </div>
        <div class="col-xs-12 col-sm-6 form-group">
          {!! Form::radio('gender', '0', true, ['id' => 'male']) !!} Male
        </div>
        <div class="col-xs-12 col-sm-6 form-group">
          {!! Form::radio('gender', '1', false, ['id' => 'female']) !!} Female
        </div>

        <span class="glyphicon glyphicon-warning-sign form-control-feedback username_error" aria-hidden="true" style="display: none"></span>
        <span class="text-right display-block has-error-text username_error username_error_value" style="display: none">Username already exist</span>


      </div>
    </div>
    <div class="col-xs-12">
      <div class="form-group">
        <label for="userPassword">Password <span class="astrick">*</span></label>
        <div class="input-group">
          {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => 'Enter your password','id' => 'password','required'=>'required']) }}
          <div class="input-group-addon"><i class="fa fa-eye" aria-hidden="true" id="password-eye"></i></div></div>
          <span class="glyphicon glyphicon-warning-sign form-control-feedback password_error" aria-hidden="true" style="display: none"></span>
          <span class="text-right display-block has-error-text password_error password_error_value" style="display: none">Username already exist</span>
        
      </div>
    </div>
    <div class="col-xs-12">
      <div class="form-group">
        <label for="userConfirmPassword">Confirm Password <span class="astrick">*</span></label>
        <div class="input-group">
         {{ Form::input('password', 'cpassword', null, ['class' => 'form-control', 'placeholder' => 'Enter Confirm password','id' => 'cpassword','required'=>'required']) }}
         <div class="input-group-addon"><i class="fa fa-eye" aria-hidden="true" id="cpassword-eye"></i></div>
         </div>
         <span class="glyphicon glyphicon-warning-sign form-control-feedback cpassword_error" aria-hidden="true" style="display: none"></span>
         <span class="text-right display-block has-error-text cpassword_error cpassword_error_value" style="display: none">Username already exist</span>
       
     </div>
   </div>
   <div class="col-xs-12">
    <div class="form-group">
      <label for="userEmail">Email Address <span class="astrick">*</span></label>
      {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email','id' => 'email','required'=>'required']) }}
      <span class="glyphicon glyphicon-warning-sign form-control-feedback email_error" aria-hidden="true" style="display: none"></span>
      <span class="text-right display-block has-error-text email_error email_error_value" style="display: none">Username already exist</span>
    </div>
  </div>
   <div class="col-xs-12">
    <div class="form-group">
      <label for="contact_no">Contact Number <span class="astrick">*</span></label>
      {{ Form::input('contact_no', 'contact_no', null, ['class' => 'form-control', 'placeholder' => 'Enter Contact No','id' => 'contact_no','required'=>'required','maxlength' => '12']) }}
      <span class="glyphicon glyphicon-warning-sign form-control-feedback contact_no_error" aria-hidden="true" style="display: none"></span>
       <span class="text-right display-block has-error-text contact_no_error contact_no_error_value" style="display: none"></span>
      
    </div>
  </div>
</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
 <div class="row">
   <div class="col-xs-12 ">
    <div class="form-group">
      <label for="userNationality">Nationality <span class="astrick">*</span></label>
      {{ Form::input('text', 'nationality', null, ['class' => 'form-control', 'placeholder' => 'Your nationality','id' => 'nationality','required'=>'required']) }}
      <span class="glyphicon glyphicon-warning-sign form-control-feedback nationality_error" aria-hidden="true" style="display: none"></span>
      <span class="text-right display-block has-error-text nationality_error nationality_error_value" style="display: none">Username already exist</span>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <label>Date of Birth <span class="astrick">*</span></label>
      <div class="row">
        <div class="col-xs-12">
          <div class="input-group date">
           {{ Form::input('text', 'dob', null, ['class' => 'form-control','required'=>'required', 'id' => 'dob']) }}
           <span class="glyphicon glyphicon-warning-sign form-control-feedback dob_error" aria-hidden="true" style="display: none"></span>
           <span class="text-right display-block has-error-text dob_error dob_error_value" style="display: none">Username already exist</span>
           <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-xs-12">

 <div class="row">
  <div class="col-xs-12">


   <div class="form-group">
     <label>Select Address <span class="astrick">*</span></label>
     {!! Form::select('country_id', ['' => 'Country'] + $country ,isset($grade->curriculum_id)?$grade->curriculum_id:"", ['class' => 'form-control' , 'id' => 'country_id','onchange'=>'getState(this.value)','required'=>'required']) !!}
     <span class="glyphicon glyphicon-warning-sign form-control-feedback country_id_error" aria-hidden="true" style="display: none"></span>
     <span class="text-right display-block has-error-text country_id_error country_id_error_value" style="display: none">Username already exist</span>
   </div>
 </div>
 <div class="col-xs-12 col-sm-6">

  <div class="form-group">
   <label class="hidden-xs blank-label"></label>
   {!! Form::select('state_id', ['' => 'State'] ,isset($grade->curriculum_id)?$grade->curriculum_id:"", ['class' => 'form-control' , 'id' => 'state_id','onchange'=>'getCity(this.value)']) !!}

 </div>
</div>
<div class="col-xs-12 col-sm-6 ">

  <div class="form-group">
    <label class="hidden-xs blank-label"></label>


    {!! Form::select('city_id', ['' => 'City'] ,isset($grade->curriculum_id)?$grade->city_id:"", ['class' => 'form-control' , 'id' => 'city_id']) !!}

  </div>
</div>
</div>

</div>
<div class="col-xs-12">
  <div class="form-group">
    <label for="userTimeZone">Select Time Zone <span class="astrick">*</span></label>

    {!! Form::select('timezone_id', ['' => 'Time Zone'] + $timezone  ,null, ['class' => 'form-control' , 'id' => 'timezone_id','required'=>'required']) !!}
    <span class="glyphicon glyphicon-warning-sign form-control-feedback timezone_id_error" aria-hidden="true" style="display: none"></span>
    <span class="text-right display-block has-error-text timezone_id_error timezone_id_error_value" style="display: none">Username already exist</span>

  </div>
</div>
<div class="col-xs-12">
  <div class="form-group">
    <label for="userTimeZone">Languages Spoken <span class="astrick">*</span></label>

    {!! Form::select('lang_id', $language  ,isset($grade->curriculum_id)?$grade->timezone_id:"", ['class' => 'form-control selectpicker language-drop' ,'multiple'=>'', 'id' => 'lang_id','style'=>'width:100%','required'=>'required']) !!}
    <span class="glyphicon glyphicon-warning-sign form-control-feedback lang_id_error" aria-hidden="true" style="display: none"></span>
    <span class="text-right display-block has-error-text lang_id_error lang_id_error_value" style="display: none">Username already exist</span>
  </div>
</div>

</div>
</div>
<div class="row">
 <div class="col-xs-12 form-group action-button">
   <div class="clearfix">
     <div class="col-xs-12 col-sm-6">

      <a href="javascript:void(0);" class="arrow-secondry-btn disabled"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>

    </div>
    <div class="col-xs-12 col-sm-6">

      <input type="hidden" name="photo" value="" id="photo_hidden">
      <input type="hidden" name="signup_with" value="0" id="signup_with">
      {!! Form::submit(trans('labels.frontend.auth.register_basic'), ['class' => 'arrow-primary-btn']) !!}

    </div>
    {!! Form::close() !!}

  </div>
</div>
<div class="clearfix"></div>
</div>


</div>
</div>






