<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="/images/favicon.ico">
	<meta property="og:url" content="http://localhost:8000/" />
	<meta property="og:title" content="Tuteme" />
	<meta property="og:description" content="This is a sample description of tuteme website" />
	<meta property="og:image" content="http://localhost:8000/images/header-fix-logo.png" />
	<title>::Welcome to TUTE ME::</title>
	<!--css starts-->

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700,900">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/css/fontello.css">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.css">
	<link rel="stylesheet" type="text/css" href="/css/base.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="/css/aos.css">
	<link rel="stylesheet" type="text/css" href="/css/developer.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
	<input type="hidden" id="csrf_token_header" value="<?php echo e(csrf_token()); ?>" />

	<!--css ends-->
	<!--Script starts-->
	<script src="js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="/js/modernizr-3.0.0.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

	<!---->
	<!--Script ends-->
</head>

<body class="landing-page-main">
	<section class="page-wrapper landing-page">
		<header class="header-landing-page fixed_header">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-3 col-lg-3">
						<div class="logo">
							<a href="/" title="Tute Me">
								<img src="images/landing-page-logo.png" alt="Tute Me" class="unfix-logo hidden-xs hidden-sm" title="Tute Me">
								<img src="/images/header-fix-logo.png" alt="Tute Me" class="fix-logo" title="Tute Me">

							</a>
						</div>
						
					</div>
					<div class="col-xs-12 col-md-9 col-lg-9  pull-right">
						<nav id="mainnav" class="navbar collapse navbar-collapse">
							<div class="menu-navigation-container">
								<ul id="menu-navigation" class="nav navbar-nav">
									<li>
										<a href="{{route('frontend.login')}}" title="Login"> Login</a>
									</li>
									<li>
										<a href="{{route('auth.register')}}" title="Sign Up" class="sign-up-button"> Sign Up</a>
									</li>

								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<!-- <section  class="banner-section">
			<div class="character-wrapper hidden-xs">
				<div class="visible-lg character first-character" data-aos-delay="300" data-aos="fade-right" data-aos-anchor-placement="top-bottom" data-aos-duration="1000">
					<img src="images/banner/11.png" alt="" title="" />
				</div>
				<div class="character second-character" data-aos="zoom-in" data-aos-delay="600" data-aos-anchor-placement="center-bottom" data-aos-delay="200">
					<img src="images/banner/22.png" alt="" title="" />
				</div>
				<div class="character third-character" data-aos="zoom-in-up" data-aos-delay="900" data-aos-anchor-placement="center-bottom" data-aos-delay="300">
					<img src="images/banner/33.png" alt="" title="" />
				</div>
				<div class="character fourth-character" data-aos="zoom-out-right" data-aos-delay="1300" data-aos-anchor-placement="center-bottom" data-aos-delay="300">
					<img src="images/banner/44.png" alt="" title="" />
				</div>
				<div class="visible-lg character fifth-character" data-aos="fade-left" data-aos-delay="1700" data-aos-anchor-placement="center-bottom" data-aos-delay="300">
					<img src="images/banner/55.png" alt="" title="" />
				</div>
			</div>
		</section> -->
		<!-- about section start form here -->
		<section id="aboutUs" class="about-us-section" style="padding-top: 115px;padding-bottom:0px;">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-5 col-lg-12 about-us-content-wrapper">
						<center><h2>Your account activated successfully. Please click <a href="{{route('frontend.login')}}" title="Login"> here</a> to login.</h2></center>
					</div>
				</div>
			</div>
			
		</section>
		<!-- about section start form here -->
		
	</section>

	
</body>

</html>
