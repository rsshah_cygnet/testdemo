@extends('frontend.layouts.master')

@section('page-wrapper')
    <!-- Page Wrapper Start -->
    <div class="page-wrapper register-pages">

        <!-- Global Menu Start -->
        @include('frontend.includes.header')
        <!-- Global Menu End -->

        <!-- Middle Content Start -->
        <div class="middle-container">
            <div class="inner-container">
                <div class="register-block page-padding">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 register-info col-lg-offset-1 col-md-offset-1 col-sm-12">
                            <div class="register-left text-center">
                                <img src="images/star.png" alt="Star">
                                <h4 class="block-title">Why should I register?</h4>
                                <p>Lorem ipsum dolor amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis ullamco laboris ut aliquip.</p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-lg-offset-4  col-md-offset-5 col-sm-12">
                            <div class="register-content">
                                <div class="section-title-secondary">
                                    <h2>Customer Feedback</h2>
                                </div>
                                <h3 class="sub-title">your <span>details</span></h3>
                                @include('includes.partials.messages')
                                <p class="mandatory-text">all fields are mandatory</p>
                                <div class="row">
                                    {{ Form::open(['route' => 'frontend.feedback', 'class' => 'm-t-15 clearfix']) }}
                                        <div class="form-group col-sm-6">
                                            <label>Name</label >
                                            {{ Form::input('name', 'first_name', null, ['class' => 'form-control', 'placeholder' => 'John']) }}
                                            @if(isset($type))
                                                {{ Form::input('hidden', 'type', $type) }}
                                            @endif
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Surname</label>
                                            {{ Form::input('text', 'surname', null, ['class' => 'form-control', 'placeholder' => 'Smith']) }}
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Email address</label >
                                            {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => 'name@email.com', 'autocomplete' => 'off']) }}
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Cell number</label >
                                            {{ Form::input('text', 'phone_no', null, ['class' => 'form-control', 'placeholder' => '+27721234567', 'autocomplete' => 'off']) }}
                                        </div>
                                        
                                        <div class="form-group col-sm-12">
                                         <label>Subject</label >
                                           {{ Form::input('text', 'subject', null, ['class' => 'field form-control','placeholder' => 'Subject', ]) }}
                                        </div>

                                        <div class="form-group col-sm-12">
                                         <label>Your Comments</label >
                                           {{ Form::textarea('comment', null, ['class' => 'field form-control']) }}
                                        </div>

                                        <div class="col-sm-12">
                                            {{ Form::submit('Submit Feedback', ['class' => 'btn-green']) }}
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Wrapper End -->

@endsection

