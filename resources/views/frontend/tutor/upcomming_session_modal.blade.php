<div id="upcomming_session_modal" class="modal fade session-popup-modal" role="dialog">
  <div class="modal-body modal-content modal-dialog">
   <div class="calendar-modal modal-green center-block">
    <!-- <span class="calendar-arrow"></span> -->
    <div class="row">
     <div class="col-xs-12 col-sm-12">
      <button type="button" class="close modal-close-btn" data-dismiss="modal">&times;</button>
    </div>
  </div>
  <div class="row">
   <div class="col-sm-7">
    <h2><b>{{$session['0']['first_name']." ".$session['0']['last_name']}}</b></h2>
    <p>{{$session['0']['qualification_name']}}</p>
  </div>
  <div class="col-sm-5 calendar-schedule-right">
    <ul class="schedule-list list-unstyled list-inline">
    <li>
        <div><i class="fa fa-calendar"></i>{{ConverTimeZoneForUser($session['0']['scheduled_date_time'],'d-m-Y')}}</div>
      </li>
      <li>
        <div><i class="fa fa-clock-o"></i> {{ConverTimeZoneForUser($session['0']['scheduled_date_time'],'h:i a')}}</div>
      </li>
    </ul>
  </div>
</div>
<div>
  <?php 
  if(isset($session['0']['topic_id']) && $session['0']['topic_id'] != 0 && $session['0']['topic_id'] != ""){ 
    $curriculumHierarchy = getCurriculumHierarchy('',$session['0']['topic_id'],true); 
  }else{ 
   $curriculumHierarchy = getCurriculumHierarchy($session['0']['subject_id'],'',true);          
 }
 ?>
 <p>Session : <b>{{$curriculumHierarchy}}</b></p>
              
              </div>
            
              <div>
              <label>Objective : {{$session['0']['objective']}}</label>
              <div class="btn-block text-center">
                   <a href="javascript:void(0);" class="btn btn-session tutor-join-session tutor-join-session-<?php echo $session['0']['id']?> hidden" onclick="tutorJoinSession('<?php echo $session['0']['id']?>')">Join Session</a>    
                   <a href="javascript:void(0);" class="btn btn-cancel" id="cancelSession"><i class="fa fa-close"></i> Cancel Session</a>
                   <input type="hidden" id="session-id" name="sessionId" value="{{$session['0']['id']}}">    
              </div>
              </div>
        </div>
  </div>

