  <div class="container-fluid">
      <!-- Left aside start from here -->
       @include('frontend.includes.tutor-leftsidebar') 
      <!-- Left aside end from here -->
      <!-- Right side start form here-->
      <div class="content-wrapper">
        <div class="content">
          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="box">
                <div class="box-header clearfix with-border">
                  <div class="box-left-title box-title session-title">
                   <input type="hidden" name="logged_in_user_id" id="logged_in_user_id" value="<?php echo \Session::get('front_user_id')?>"> 


                  @if(\Session::has('sessionExpired'))
                  <div class="alert alert-danger">
                    {{\Session::get('sessionExpired')}}
                  </div>
                  {{\Session::forget('sessionExpired')}}
                  @endif


                    <h3>Upcoming Sessions</h3>
                  </div>
                  <div class="box-right-title box-title-link view-all-session">
                   <a href="javascript:void(0);" onclick="loadTutorMysessions()" class="color-title" title="View all">View All Sessions</a>
                  </div>
                </div>
                <div class="box-body">
                  <ul class="upcomming-sessions">
                    <?php 
                      if(!empty($upcommingSessions)){
                        foreach ($upcommingSessions as $key => $value) { ?>
                          <li class="clearfix">
                            <div class="session-detail">
                              <h3 class="color-title">{{$value['first_name']." ".$value['last_name']}}</h3>
                              <div class="session-date-time">
                                <span><i class="fa fa-calendar"></i> {{ConverTimeZoneForUser($value['scheduled_date_time'],'d-m-Y')}} </span>
                                <span><i class="fa fa-clock-o"></i> {{ConverTimeZoneForUser($value['scheduled_date_time'],'h:i a')}} </span>
                              </div>
                              <div class="session-subject">
                               <?php 
                                  if(isset($value['topic_id']) && $value['topic_id'] != 0 && $value['topic_id'] != ""){
                                      echo getCurriculumHierarchy('',$value['topic_id'],true); //for topic
                                  }else{
                                      echo getCurriculumHierarchy($value['subject_id'],'',true); //for subject
                                  }
                               ?> 

                              </div>
                            </div>
                            <div class="join-session">
                              <a href="javascript:void(0);" onclick="tutorJoinSession(<?php echo $value['id'] ?>)" class="tutor-dashboard-btn tutor-join-session tutor-join-session-<?php echo $value['id']?> hidden ">JOIN SESSION</a>
                            </div>
                          </li>
                    <?php    }
                      }else{ ?>
                      <p>There are No Upcoming Sessions for you.</p>
                  <?php    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
                <div class="box tutor-tutee-review">
                    <div class="basic-details detail-wtih-icon form-group">
                    <div class="media">
                     <div class="media-left media-middle"> 
                                    <h3> Tutee Reviews </h3>
                                  </div>
                      <div class="media-body media-middle">
                       
                        <div class="basic-answer teach clearfix"> 
                          <!-- Rating start form here -->
                          <div class="rating-total">
                            <h3>
                            @if(!empty(round($tutor_overall_ratings['avg_tutor_ratings'])))      
                                {{round($tutor_overall_ratings['avg_tutor_ratings'],2)}}
                            @else
                                0
                            @endif
                            </h3>
                            <div class="rating-input"><input type="number" value="<?php echo round($tutor_overall_ratings['avg_tutor_ratings']); ?>" name="your_awesome_parameter" id="rating-empty-clearable" class="rating" readonly/></div>
                            <span>( {{$tutor_overall_ratings['total_tutor_ratings'] or 0}} Total )</span> </div>
                          <!-- Rating start end here --> 
                          <!-- rating breakdown -->
                          <div class="rating-breakdown-wrapper">
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 5 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$tutor_overall_ratings['tutor_ratings_five_percentage']}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_overall_ratings['tutor_ratings_five_percentage']}}%"> {{$tutor_overall_ratings['tutor_ratings_five']}} </div>
                                </div>
                              </div>
                            </div>
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 4 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{$tutor_overall_ratings['tutor_ratings_four_percentage']}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_overall_ratings['tutor_ratings_four_percentage']}}%"> {{$tutor_overall_ratings['tutor_ratings_four']}} </div>
                                </div>
                              </div>
                            </div>
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 3 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$tutor_overall_ratings['tutor_ratings_three_percentage']}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_overall_ratings['tutor_ratings_three_percentage']}}%"> {{$tutor_overall_ratings['tutor_ratings_three']}} </div>
                                </div>
                              </div>
                            </div>
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 2 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{{$tutor_overall_ratings['tutor_ratings_two_percentage']}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_overall_ratings['tutor_ratings_two_percentage']}}%"> {{$tutor_overall_ratings['tutor_ratings_two']}} </div>
                                </div>
                              </div>
                            </div>
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 1 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{$tutor_overall_ratings['tutor_ratings_one_percentage']}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_overall_ratings['tutor_ratings_one_percentage']}}%"> {{$tutor_overall_ratings['tutor_ratings_one']}} </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!--rating breakdown --> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6">
                  <div class="box tutor-panctual-rating">
                    <div class="basic-details detail-wtih-icon form-group">
                                <div class="media">
                                 <div class="media-left media-middle"> 
                                    <h3> Punctuality Ratings </h3> 
                                  </div>
                                  <div class="media-body media-middle">
                                   <?php 
                                    if(isset($tutor_overall_ratings['punctuality_rating']) && $tutor_overall_ratings['punctuality_rating'] != 0 && $tutor_overall_ratings['punctuality_rating'] != ""){
                                   ?> 
                                    <div class="basic-answer">
                                      <div class="lg-rating-outof-wrapper"> <span class="lg-rating-outof"> <span class="brackets">[</span> <span class="lg-text">{{$tutor_overall_ratings['punctuality_rating'] or 0}}</span> <span class="total-of-raing">/10</span> <span class="brackets close-brackets">]</span> </span> </div>
                                      <div class="rating-outof-text"> Punctuality Ratings are been given by the system </div>
                                    </div>
                                    <?php }else{?>
                                     <div class="basic-answer">
                                      <div class="rating-outof-text">Punctuality Ratings is not available for you because you have not attended any session.</div>
                                    </div>
                                    <?php } ?>
                                  </div>
                                </div>
                  </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            <div class="col-xs-12 col-lg-6">
              <div class="box">
                <div class="box-header clearfix with-border">
                  <div class="box-left-title box-title">
                    <h3 class="">Session Details</h3>
                  </div>
                    <div class="box-right-title session-detail-month">
                     <!-- <label> Filter</label> -->
                    <select name="month" id="month_piechart">
                        <option value="">Select Month</option>
                        <option value="01">January</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                  </div>
                  <div class="box-right-title session-detail-month">
                   
                     <select name="year" id="year_piechart">
                        <option value="">Select Year</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                    </select>
                  </div>
                </div>
                <p id="piechart_error" style="display: none;color: red;"></p>
                <div class="box-body" id="pie_chart_portion">
                 <!--  <img src="images/chart-placeholder.jpg" class="img-responsive" alt="" title=""/> -->
                  <div id="piechart_3d" style="height: 320px;width: 100%;"></div>
                </div>
              </div>
            </div>
            
            <div class="col-xs-12 col-lg-6">
              <div class="box">
                <div class="box-header clearfix with-border">
                  <div class="box-left-title box-title">
                    <h3 class="">Ratings</h3>
                  </div>
                  <div class="box-right-title session-detail-month">
                   <!--  <label> Ratings</label> -->
                    <select id="quarter">
                      <option value="">Select Quarter</option>
                      <option value="1">Quarter 1</option>
                      <option value="2">Quarter 2</option>
                      <option value="3">Quarter 3</option>
                    </select>
                  </div>
                     <div class="box-right-title session-detail-month">
                   
                     <select name="year" id="year_columnchart">
                        <option value="">Select Year</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                    </select>
                  </div>
                </div>
                <p id="column_chart_error" style="display: none;color: red;"></p>
                <div class="box-body" id="column_chart_portion">
                  <div id="columnchart_material" style="width: 100%; height: 320px;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Right side end form here-->
      </div>
    