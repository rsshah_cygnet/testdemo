  <div class="container-fluid">
   <?php 
   $class_wrappper = "";
   if(isset($sessionData['front_user_id']) && $sessionData['front_user_id'] != 0 && $sessionData['front_user_id'] != ""){ 
        $class_wrappper = "content-wrapper";
    ?>
        @include('frontend.includes.tutee-leftsidebar')     
   <?php } ?>
   
   <!-- Left aside end from here -->
   <div class="{{$class_wrappper}}">
    <div class="content">
     <div class="row">
      <div class="left-aside col-md-5 col-lg-4">
        
       <!-- <div class="left-afix affix" data-spy="affix" data-offset-top="0" data-offset-bottom="300"> -->
       <div class="left-afix">

         <h2 class="sm-font-title hidden-xs hidden-sm">Find Tutor</h2>
         <div class="filter-wrapper clearfix">
          <div class="col-xs-12">
            <div class="visible-sm visible-xs close-drawer">
              <i class="fa fa-times"></i>
            </div>

            <h2 class="sm-font-title visible-xs visible-sm">Find Tutor</h2>
            <h2 class="red-title">Filter</h2>
            <form class="filter-form" style="position:relative;" method="POST" id="searchTutor" name="searchTutor" onsubmit="return false;">
              @if(!empty($searchData))
              <div  id="searchhidden">
                <input type="hidden" name="searchhidden" value="{{json_encode($searchData)}}">
              </div>
              @endif

              <div class="form-group">
                <div class="custom-search">
                  <input type="text" placeholder="Find Tutor by name" class="custom-search form-control" name="search[tutor_name]" value="@if(!empty($searchData['search']['tutor_name'])){{$searchData['search']['tutor_name']}}@endif">
                  <input type="submit" class="search-submit" />
                </div>
              </div>
              <div class="form-group">
                <div class="flat-select-wrapper flat-padd-lan clearfix ">
                  <label class="col-sm-6 label-top-spacing">Language Spoken</label>
                  <div class="col-sm-6">
                    {!! Form::select('search[language][]', $language ,isset($searchData['search']['language'])?$searchData['search']['language']:"", ['class' => 'form-control selectpicker language-drop custome_lang_dropdown flat-select find-tutor-dropdown-lan' ,'multiple'=>'', 'id' => 'language_id']) !!}

                  </div>
                </div>
              </div>
              <div class="select-accordion-wrapper form-group" id="custom_dropdowns">
                <div class="clearfix">
                  <label class="col-xs-12 col-sm-6">Select Curriculum</label>
                  <div class="col-xs-12  col-sm-6">
                    {!! Form::select("search[curriculum_id]",['' => 'Select'] + $curriculumn, isset($searchData['search']['curriculum_id'])?$searchData['search']['curriculum_id']:"",['class' => 'form-control select-curriculum flat-select' , 'id' => 'curriculum_id','onchange'=>'getCurriculumHierarchy("curriculum_id")']) !!}
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flat-select-wrapper clearfix">
                  <label class="col-sm-6 label-top-spacing">Tutees Rating</label>
                  <div class="col-sm-6">
                    <select name="search[rating]" id="" class="form-control flat-select">
                      <option value="">Select</option>
                      <option value="1"  @if(!empty($searchData['search']['rating']) && $searchData['search']['rating'] == 1) selected='selected' @endif>1</option>
                      <option value="2" @if(!empty($searchData['search']['rating']) && $searchData['search']['rating'] == 2) selected='selected' @endif>2</option>
                      <option value="3" @if(!empty($searchData['search']['rating']) && $searchData['search']['rating'] == 3) selected='selected' @endif>3</option>
                      <option value="4" @if(!empty($searchData['search']['rating']) && $searchData['search']['rating'] == 4) selected='selected' @endif>4</option>
                      <option value="5" @if(!empty($searchData['search']['rating']) && $searchData['search']['rating'] == 5) selected='selected' @endif>5</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flat-select-wrapper clearfix">
                  <label class="col-sm-6 label-top-spacing">Punctuality Rating</label>
                  <div class="col-sm-6">
                    <select name="search[punctual-rating]" id="" class="form-control flat-select">
                      <option value="">Select</option>
                      <option value="1" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 1) selected='selected' @endif>1/10</option>
                      <option value="2" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 2) selected='selected' @endif>2/10</option>
                      <option value="3" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 3) selected='selected' @endif>3/10</option>
                      <option value="4" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 4) selected='selected' @endif>4/10</option>
                      <option value="5" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 5) selected='selected' @endif>5/10</option>
                      <option value="6" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 6) selected='selected' @endif>6/10</option>
                      <option value="7" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 7) selected='selected' @endif>7/10</option>
                      <option value="8" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 8) selected='selected' @endif>8/10</option>
                      <option value="9" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 9) selected='selected' @endif>9/10</option>
                      <option value="10" @if(!empty($searchData['search']['punctual-rating']) && $searchData['search']['punctual-rating'] == 10) selected='selected' @endif>10/10</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flat-select-wrapper clearfix">
                  <label class="label-top-spacing col-xs-12">Select Date</label>
                  <div class="col-xs-12 datetime-range">
                    <div class="input-group">
                      <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                      <input type="text-center" class="form-control pull-right" id="reservation" name="search[reservation]" value="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flat-select-wrapper clearfix">
                  <label class="label-top-spacing col-xs-12">Select Time</label>
                  <div class="form-group col-xs-12 col-md-6">
                    <div class="input-group">
                      <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
                      <input type="text" class="form-control time-picker pull-right" Placeholder="From" name="search[from]" id="from" value="@if(!empty($searchData['search']['from'])){{$searchData['search']['from']}}@endif">
                    </div>
                  </div>
                  <div class="form-group col-xs-12 col-md-6">
                    <div class="input-group">
                      <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
                      <input type="text" class="form-control time-picker pull-right" Placeholder="To" id="to" name="search[to]" value="@if(!empty($searchData['search']['to'])){{$searchData['search']['to']}}@endif">
                    </div>
                  </div>
                </div>
              </div>
              @if(\Session::get('front_user_id') == "" && \Session::get('front_user_id') == 0)
              <div class="form-group">
                <div class="flat-select-wrapper clearfix">
                  <label class="col-sm-6 label-top-spacing">Select Timezone</label>
                  <div class="col-sm-6">
                    <select name="search[timezone]" id="timezone" class="form-control flat-select">
                     <option value ="">Select</option>
                     @foreach($timezone as $key => $value)
                     <option value="{{ $key }}" @if(!empty($searchData['search']['timezone']) && $searchData['search']['timezone'] == $key) selected='selected' @endif>{{ $value }}</option>
                     @endforeach
                   </select>
                 </div>
               </div>
             </div>
             @endif
             <div class="form-group text-center">
              <input type="submit" class="btn btn-default tutee-primary-btn" value="APPLY" title="Apply" id="submit">
            </div>
            <div class="form-group text-center">
              <input type="reset" class="btn-link filter-reset" value="Reset" title="Reset" id="reset">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" value="find_tutor" name="is_find_tutor" id="is_find_tutor">
<input type="hidden" value="<?php echo csrf_token() ?>" name="tutor_find_token" id="tutor_find_token">
<input type="hidden" value="<?php echo isset($searchData['search']['reservation'])?$searchData['search']['reservation']:""; ?>" name="search_reservation" id="search_reservation">
<input type="hidden" value="<?php echo isset($searchData['education_id']) ? $searchData['education_id'] : ''; ?>" name="search_education_id" id="search_education_id">
<input type="hidden" value="<?php echo isset($searchData['level_id']) ? $searchData['level_id'] : ''; ?>" name="search_level_id" id="search_level_id">
<input type="hidden" value="<?php echo isset($searchData['program_id']) ? $searchData['program_id'] : ''; ?>" name="search_program_id" id="search_program_id">
<input type="hidden" value="<?php echo isset($searchData['grade_id']) ? $searchData['grade_id'] : ''; ?>" name="search_grade_id" id="search_grade_id">
<input type="hidden" value="<?php echo isset($searchData['subject_id']) ? $searchData['subject_id'] : ''; ?>" name="search_subject_id" id="search_subject_id">
<input type="hidden" value="<?php echo isset($searchData['topic_id']) ? $searchData['topic_id'] : ''; ?>" name="search_topic_id" id="search_topic_id">
  <div id="tutor-result">
   <!-- Start View Page -->

   <!-- End View Page -->
 </div>
</div>
</div>
</div>

<div class="filter-mobile"> <i class="fa fa-filter"></i> </div>
</div>
</div>