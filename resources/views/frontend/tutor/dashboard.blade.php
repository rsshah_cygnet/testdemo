 @extends('frontend.layouts.tutor',['session' => $sessionData])
 @section('middle-content')
   @include('frontend.tutor.mid_section_dashboard') 
@endsection
@section('after-scripts-end')

      <script type="text/javascript">
        //google.charts.load("current", {packages:["corechart"]});
        //google.charts.setOnLoadCallback(drawChart);
       google.charts.load('current', {packages: ['corechart']});
       google.charts.setOnLoadCallback(drawChart);
       google.charts.setOnLoadCallback(drawChartRatings);

        function drawChart() {
      
         var record={!! json_encode($user) !!};
         console.log(record);
         // Create our data table.
         var data = new google.visualization.DataTable();
         data.addColumn('string', 'Session Type');
         data.addColumn('number', 'Total');
         for(var k in record){
              var v = record[k];
             
               data.addRow([k,v]);
               //console.log(v);
            }
          var options = {
            title: '',
            is3D: false,
            width: '90%',
            height: '90%',
            chartArea: {
                left: "3%",
                top: "3%",
                height: "80%",
                width: "80%"
            },
            /*legend:{position: 'top'},
*/
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
          chart.draw(data, options);
        }

      

      function drawChartRatings() {
        var data = google.visualization.arrayToDataTable(<?php echo $columnChartArray; ?>);
    
         var options = {
          chart: {
            title: 'Ratings',

            //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
          bars: 'vertical',
          //vAxis: {format: 'decimal'},
         // height: 400,
          legend: {position: 'top'},
          colors: ['#f79237', '#228B22'],
            width: '100%',
            height: '100%',
          
            chartArea: {
              /*left: "3%",
              top: "3%",*/
              height: "85%",
              width: "85%",
              
          },
        };

       // var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
       var chart =  new google.visualization.ColumnChart(document.getElementById('columnchart_material'));

        chart.draw(data, options);
      }

      </script>
  @stop