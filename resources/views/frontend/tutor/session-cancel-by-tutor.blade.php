                                                  @foreach($cancelSessionByTutorByFilter as $record)
                                                  <!-- payment box start here -->
                                                     <div class="payment-box">
                                                    <div class="payment-top">
                                                        <div class="row send-card-requests pad-0">
                                                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                                                                        <div class="">
                                                                            <h2>{{$record->tutee}}</h2>
                                                                            <p>{{$record->qualification_name}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 my-payment-pera payment-user-info">

                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b pull-left">Curriculum</p>
                                                                            <span class="table-cell text-left">
<b>
                          <?php
if (isset($record['topic_id']) && $record['topic_id'] != 0 && $record['topic_id'] != "") {
	echo getCurriculumHierarchy('', $record['topic_id'], true); //for topic
} else {
	echo getCurriculumHierarchy($record['subject_id'], '', true); //for subject
}
?>
                          </b>
                                                                            </span>
                                                                        </div>

                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b">Subject</p>
                                                                            <span><b>{{$record->subject_name}}</b></span>
                                                                        </div>

                                                                        <div class="subject-section clearfix">
                                                                            <p class="d-i-b">Topic</p>
                                                                            <span><b>{{$record->topic_name}}</b></span>
                                                                        </div>

                                                                        <div>
                                                                            <p class="pull-left"> Fee credited on : </p>
                                                                            <div class="session-date-time session-f-size">
    <span> <i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->fee_credited_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->fee_credited_date_time)}} --}}
    </span>
    <span> <i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->fee_credited_date_time, 'H:i'))}}
    {{-- {{getOnlyTime($record->fee_credited_date_time)}} --}}
    </span>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-4 col-md-offset-0 col-lg-5">
                                                                <div class="amount-block">
                                                                    <div class="session-fee-rate amount-credit">
                                                                        <span>Amount Credited<strong>${{$record->tutor_earning}}</strong></span>
                                                                    </div>
                                                                    <div class="session-fee-rate">
                                                                        <span>Session Fee<strong>${{$record->session_fee}}</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="payment-bottom">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <ul class="schedule-list list-unstyled list-inline">
                                                                    <li>
                                                                        <label class="pull-left">Scheduled on :</label>
                                                                        <span class="table-cell text-left">
    <div><i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->scheduled_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->scheduled_date_time)}} --}}
    </div>
    <div><i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->scheduled_date_time, 'H:i'))}}
    {{-- {{getOnlyTime($record->scheduled_date_time)}} --}}
    </div>
                                                                        </span>
                                                                    </li>
                                                                    <li>
                                                                        <label class="pull-left">Accepted on :</label>
                                                                        <span class="table-cell text-left">
    <div><i class="icon icon-calendar-fill"></i>
    {{ConverTimeZoneForUser($record->accepted_date_time, 'd-m-Y')}}
    {{-- {{getOnlyDate($record->accepted_date_time)}}</div> --}}
    <div><i class="icon icon-clock"></i>
    {{getOnlyTime(ConverTimeZoneForUser($record->accepted_date_time, 'H:i'))}}
    {{-- {{getOnlyTime($record->accepted_date_time)}}</div> --}}
                                                                        </span>
                                                                    </li>
                                                                </ul>
                                                                <div class="amount-refunded-right">
                                                                    <ul class="schedule-list list-unstyled list-inline">
                                                                        <li>
                                                                            <label class="pull-left">Fee credited on:</label>
                                                                            <span class="table-cell text-left">
<div><i class="icon icon-calendar-fill"></i>
{{ConverTimeZoneForUser($record->fee_credited_date_time, 'd-m-Y')}}
{{-- {{getOnlyDate($record->fee_credited_date_time)}} --}}
</div>
<div><i class="icon icon-clock"></i>
{{getOnlyTime(ConverTimeZoneForUser($record->fee_credited_date_time, 'H:i'))}}
{{-- {{getOnlyTime($record->fee_credited_date_time)}} --}}
</div>
                                                                            </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- payment box end here -->
                                                @endforeach
                                                <div id="tab2-pagination">
                                                    @if($cancelSessionByTutorByFilter->total() > 10)
                                                        {{$cancelSessionByTutorByFilter->links()}}
                                                    @endif
                                                </div>