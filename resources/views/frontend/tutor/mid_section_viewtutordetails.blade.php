    <div class="container-fluid">
      <div class="row">
        <div class="clearfix title-detail">
          <div class="col-xs-12 col-sm-5 col-md-4"> <a href="javascript:void(0);" onclick=" loadFindTutor()" title="Back to search results" class="back-link-btn"> <i class="fa fa-chevron-left" aria-hidden="true"></i> Back </a> </div>
          <div class="col-xs-12 col-sm-3 col-md-3">
            <h2>Tutor Details </h2>
          </div>
        </div>

        <div class="clearfix">
          <div class="col-xs-12 col-sm-8">
            <div class="tutor-detail-wrapper clearfix">
              <!--tutor image-->
              <div class="tutor-image">
               @php
                  $img_path = url('uploads/user/' . $result->photo);
                    if(($result->photo) != "")
                    {
                      $img_path = $img_path;
                    }
                    else
                    {
                      $img_path = url('images/default-user.png');
                    }
                @endphp
                <img src="{{$img_path}}" alt="">
              </div>
              <!--tutor image-->
              <!--tutee revidew start form here-->
              <div class="tutor-detail clearfix">
                <div class="tutor-basic-detail-wrapper clearfix">
                  <div class="tutee-title">
                    <h2 class="red-title">{{$result->first_name}} {{$result->last_name}}</h2>
                    <div class="education-detail detail-wtih-icon">
                      <div class="media">
                        <div class="media-left"> <i class="fa fa-graduation-cap" aria-hidden="true"></i> </div>
                        <div class="media-body">
                          <div class="degree"> {{$result->qualification_name}} </div>
                          <div class="university-name">
                          {{empty($result->educational_details)? '' : $result->educational_details }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                   {!! Form::open(['route' => 'frontend.checkPreference', 'class' => '','id'=>'sessionReserve','method'=>'post','onsubmit' => 'return false']) !!}
                  <div class="reserve-place">  <?php if (isset($front_user_id) && $front_user_id != "" && $front_user_id != 0) {?>
                  <a  href="javascript:void(0);" title="RESERVE" class="tutee-dashboard-btn dropdown-toggle" data-toggle="dropdown" id="reserve_btn" aria-haspopup="true" aria-expanded="false">RESERVE</a>
                 <?php } else {?>
                    <a href="javascript:void(0);" id="reserve_without_login" class="tutee-dashboard-btn" title="RESERVE">RESERVE</a>
                 <?php }?>
                    <div class="dropdown-menu">
                      <button type="button" class="close">&times;</button>
            <div class="reserve-tutee-wrapper">
              <!-- date control start form here --><p class="msg" style="color: red"></p>
              <div class="form-group">
              <label class="select-date-label">Select Date <span class="astrick">*</span></label>
              <div class="" id="calender-inline"></div>
              </div>
              <div class="form-group">
              <label class="label-top-spacing">Time <span class="astrick">*</span></label>
              <div class="input-group">
                <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
                <input type="text" name="from_time" id="from_time" class="form-control time-picker pull-right" placeholder="From" required="required">
              </div>
              </div>
              <!-- date control end form here -->

              <!-- Curriculum start form here -->
              <div class="select-accordion-wrapper form-group" id="custom_dropdowns">
                <div class="clearfix">
                <label class="col-xs-12 col-sm-6 label-title">Select Curriculum <span class="astrick">*</span></label>
                <div class="col-xs-12  col-sm-6">

               {!! Form::select('curriculum_id',['' => 'Select Curriculum'] + $curriculumn, isset($result)?$result:"",['class' => 'form-control select-curriculum flat-select' , 'id' => 'curriculum_id','onchange'=>'getCurriculumsystemForSessionReserve("curriculum_id")','required'=>'required']) !!}
                </div>
                </div>
             </div>
              <!-- Curriculum end form here-->

              <!-- objective start from here -->
               <div class="form-group ">
              <label>Objective  <span class="astrick">*</span></label>
              <input type="text" name="objective" id="objective" class="form-control" placeholder="Your objective"
              required="required">
              </div>
              <!-- objective end from here-->
              <!-- fees for this session  -->

              <!--fees for this session -->
              <!-- reverse button -->
              <div class="form-group">
                 <a href="javascript:void(0);" title="RESERVE" onclick="reserveSession()" id="reserve" class="tutee-primary-btn" >RESERVE</a>
              </div>
              <!-- reverse button-->
                    </div>
              {!! Form::close() !!}

               {!! Form::open(['route' => 'frontend.conformPreference', 'class' => '','id'=>'sessionConform','method'=>'post','onsubmit' => 'return false']) !!}
          <!-- conform tutee start from here -->
          <input type="hidden" name="resonseData" id="resonseData" value="{{$result->userId}}">
          <div class="conform-wrapper" style="display:none;">
            <!-- back button start form here -->
            <div class="form-group">
              <label class="back-button">
              <a href="javascript:void(0);" class="back_btn_reserve" title="Back">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back
              </a>
              </label>
            </div>
            <!-- back button end from here-->

            <!-- session date and time start form here -->
            <div class="form-group border-bottom-space session-date-time">
              <label>Session Date & Time :</label>
              <div class="date-time-reserve">
                <span><i class="fa fa-calendar fromdate" aria-hidden="true"></i></span>
                <span><i class="fa fa-clock-o fromtime" aria-hidden="true"></i></span>
              </div>
            </div>
            <!-- session date and time end from here-->
              <!-- session start form here -->
            <div class="form-group border-bottom-space">
              <label>Session:</label>
              <span class="session-take">
                <strong class="hierarchy"></strong>
              </span>
            </div>
            <!-- session end from here-->
             <!-- objective start from here -->
               <div class="form-group  border-bottom-space">
              <label>Objective</label>
              <p class="obj"></p>
              </div>
              <!-- objective end from here-->
            <!-- fees for this session  -->
               <div class="form-group fees-section clearfix">
               <label class=""> <i class="fa fa-usd" aria-hidden="true"></i> <strong> Fees for this session</strong> </label>
               <span><strong class="fees">$</strong></span>
               </div>
              <!--fees for this session -->
              <!-- reverse button -->
              <div class="form-group">
              <a href="javascript:void(0);" onclick="confirmSession()" title="CONFIRM" class="tutee-primary-btn" id="conform">CONFIRM</a>
              </div>
              <!-- reverse button-->
          </div>
            {!! Form::close() !!}
          <!-- conform tutee end form here-->
          </div>
                  <div class="dropdown-menu signup-dropdown hide">
                           <button type="button" class="close">&times;</button>
                           <!-- signup drop down start form here -->
                            <div class="signup-dropdown-wrapper">
                                <div class="login-title border-bottom-space form-group">
                                    You need to login to reserve the session
                                </div>
                                <div class="login-content text-center">
                                   <div class="form-group">
                                        <label>Already Registered?</label>
                                    </div>
                                    <div class="login-button form-group">
                                        <a href="javascript:void(0);" title="LOGIN" class="tutee-primary-btn">LOGIN</a>
                                    </div>
                                    <div class="form-group">
                                        <div class="xs-line"></div>
                                    </div>

                                        <div class="text-center sign-up">
                                         <span>Don't have an account?&nbsp;<a href="javascript:void(0);" title="SIGN UP"> SIGN UP </a></span>
                                         </div>

                                </div>
                            </div>
                           <!-- sign up dropdpwn end form here-->
                      </div>
                  </div>
                </div>
                <div class="tutor-basic-detail-wrapper">
                  <div class="basic-details detail-wtih-icon form-group">
                    <div class="media">
                      <div class="media-left"> <i class="fa fa-user" aria-hidden="true"></i> </div>
                      <div class="media-body">
                        <label class="basic-title  full-width"> Nationality </label>
                        <div class="basic-answer"> {{$result->nationality}}  </div>
                      </div>
                    </div>
                  </div>
                  <div class="basic-details detail-wtih-icon form-group">
                    <div class="media">
                      <div class="media-left"> <i class="fa fa-globe" aria-hidden="true"></i> </div>
                      <div class="media-body">
                        <label class="basic-title  full-width"> Country </label>
                        <div class="basic-answer"> {{$result->country_name}} </div>
                      </div>
                    </div>
                  </div>
                  <div class="basic-details detail-wtih-icon form-group">
                    <div class="media">
                      <div class="media-left"> <i class="fa fa-briefcase" aria-hidden="true"></i> </div>
                      <div class="media-body">
                        <label class="basic-title  full-width"> Occupation </label>
                        <div class="basic-answer">{{empty($result->occupation_details)? 'Occupation Details Not Available' : $result->occupation_details }}</div>
                      </div>
                    </div>
                  </div>
                  <div class="basic-details detail-wtih-icon form-group">
                    <div class="media">
                      <div class="media-left"> <i class="fa fa-language" aria-hidden="true"></i> </div>
                      <div class="media-body">
                        <label class="basic-title  full-width"> Languages Spoken </label>
                        <div class="basic-answer">{{getLanguageBasedOnLanguageIdString($result->lang_id)}}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tutor-basic-detail-wrapper">
                  <div class="basic-details detail-wtih-icon form-group">
                    <div class="media">
                      <div class="media-left"> <i class="fa fa-book" aria-hidden="true"></i> </div>
                      <div class="media-body">
                        <label class="basic-title full-width"> I can teach </label>
                        <div class="basic-answer teach">
                          @foreach($hierarchy as $hK => $hV)
                            @if($hV['topic_id'] != Null || $hV['topic_id'] != 0)
                              <div class="basic-answer teach"> <strong class="tutor-list-style">              {{getCurriculumHierarchy('' ,$hV['topic_id'],true)}}
                              </strong></div>
                            @elseif($hV['subject_id'] != Null || $hV['subject_id'] != 0)
                              <div class="basic-answer teach"> <strong class="tutor-list-style">
                              {{getCurriculumHierarchy($hV['subject_id'] ,'',true)}}
                              </strong></div>
                            @endif
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tutor-basic-detail-wrapper">
                  <div class="basic-details detail-wtih-icon form-group">
                    <div class="media">
                      <div class="media-body">
                        <label class="basic-title full-width"> Tutee Reviews </label>
                        <div class="basic-answer teach clearfix">
                          <!-- Rating start form here -->
                          <div class="rating-total">
                            <h3>{{$result->rating}}</h3>
                            <input type="number" name="your_awesome_parameter" id="rating-empty-clearable" class="rating" value="{{$result->rating}}"/>
                            <span>( {{getReviewCountByUserId($result->userId,0)}} Total )</span> </div>
                          <!-- Rating start end here -->
                          <!-- rating breakdown -->
                          <div class="rating-breakdown-wrapper">
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 5 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                  @php
                                    $tutor_ratings_five_percentage = '';
                                    if(getReviewCountByUserId($result->userId,0) != "" && getReviewCountByUserId($result->userId,0) != 0)
                                    {
                                      $tutor_ratings_five_percentage = (100 * getReviewCountByUserId($result->userId,5)) /  getReviewCountByUserId($result->userId,0);
                                    }
                                  @endphp
                                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$tutor_ratings_five_percentage}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_ratings_five_percentage}}%"> {{getReviewCountByUserId($result->userId,5)}}
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 4 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                  @php
                                    $tutor_ratings_four_percentage = '';
                                    if(getReviewCountByUserId($result->userId,0) != "" && getReviewCountByUserId($result->userId,0) != 0)
                                    {
                                      $tutor_ratings_four_percentage = (100 * getReviewCountByUserId($result->userId,4)) /  getReviewCountByUserId($result->userId,0);
                                    }
                                  @endphp
                                  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{$tutor_ratings_four_percentage}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_ratings_four_percentage}}%"> {{getReviewCountByUserId($result->userId,4)}}
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 3 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                  @php
                                    $tutor_ratings_three_percentage = '';
                                    if(getReviewCountByUserId($result->userId,0) != "" && getReviewCountByUserId($result->userId,0) != 0)
                                    {
                                      $tutor_ratings_three_percentage = (100 * getReviewCountByUserId($result->userId,3)) /  getReviewCountByUserId($result->userId,0);
                                    }
                                  @endphp

                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$tutor_ratings_three_percentage}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_ratings_three_percentage}}%"> {{getReviewCountByUserId($result->userId,3)}}
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 2 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                 @php
                                    $tutor_ratings_two_percentage = '';
                                    if(getReviewCountByUserId($result->userId,0) != "" && getReviewCountByUserId($result->userId,0) != 0)
                                    {
                                      $tutor_ratings_two_percentage = (100 * getReviewCountByUserId($result->userId,2)) /  getReviewCountByUserId($result->userId,0);
                                    }
                                  @endphp

                                  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{{$tutor_ratings_two_percentage}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_ratings_two_percentage}}%"> {{getReviewCountByUserId($result->userId,2)}}
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="rating-breakdown clearfix">
                              <div class="pull-left rate-list"> <i class="glyphicon glyphicon-star"></i> 1 </div>
                              <div class="pull-left rating-progress">
                                <div class="progress">
                                 @php
                                    $tutor_ratings_one_percentage = '';
                                    if(getReviewCountByUserId($result->userId,0) != "" && getReviewCountByUserId($result->userId,0) != 0)
                                    {
                                      $tutor_ratings_one_percentage = (100 * getReviewCountByUserId($result->userId,1)) /  getReviewCountByUserId($result->userId,0);
                                    }
                                  @endphp
                                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{$tutor_ratings_one_percentage}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$tutor_ratings_one_percentage}}%"> {{getReviewCountByUserId($result->userId,1)}}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!--rating breakdown -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tutor-basic-detail-wrapper">
                  <div class="basic-details detail-wtih-icon form-group">
                    <div class="media">
                      <div class="media-body">
                      @if(isset($sessionData['front_user_id']) && $sessionData['front_user_id'] != "" && $sessionData['front_user_id'] != 0 && !empty($result->punctuality))
                        <label class="basic-title full-width"> Punctuality Ratings </label>
                          @endif
                        @if(!empty($result->punctuality))
                        <div class="basic-answer">
                          <div class="lg-rating-outof-wrapper"> <span class="lg-rating-outof"> <span class="brackets">[</span> <span class="lg-text">{{$result->punctuality}}</span> <span class="total-of-raing">/10</span> <span class="brackets close-brackets">]</span> </span> </div>
                          <div class="rating-outof-text">
                           Punctuality Ratings are been given by the system 
                           </div>
                        </div>@elseif(isset($sessionData['front_user_id']) && $sessionData['front_user_id'] != "" && $sessionData['front_user_id'] != 0)
                        <p>Punctuality Rating is not available for this tutor.</p>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!--tutee revidew end form here-->
            </div>
          </div>
          <form name="loadForm" id="loadMoreForm" method="POST">
           <input type="hidden" name="page_type" id="page_type" value="tutor_detail">
           <input type="hidden" name="total_reviews" id="total_reviews" value="{{$total}}">
          <input type="hidden" name="hidden_limit" id="hidden_limit" value="0">
          <input type="hidden" name="totalNext" id="totalNext" value="{{$total-10}}">
          <input type="hidden" name="totalPrev" id="totalPrev" value="0">
          <input type="hidden" name="tutorid" id="tutorid" value="{{$result->userId}}">
           <input type="hidden" name="enabledDates" id="enabledDates" value='<?php echo '"'.implode('","', $dates).'"' ?>'>
          </form>

        <div class="col-xs-12 col-sm-4">
            <div class="tutee-review-wrapper">
              <div class="tutee-review-title">
                <h3>Tutee Reviews ({{getReviewCountByUserId($result->userId,0)}})</h3>
              </div>
              <ul id ="tutor_rating_div">
          <!--right side part start form here-->
          @include('frontend.includes.viewreview')
          </ul>


          </div>
        </div>

        <!--right side part end form here-->
      </div>
    </div>