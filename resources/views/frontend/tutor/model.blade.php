
      {{Form::open(['route'=>array('decline.tutee.session.request', $id),'onclick' => 'return false'])}}
          <h2>Are you sure,</h2>
          <p>You want to decline this request ?</p>
          {{ Form::hidden('id', $id ,['id' => 'hidden'])}}
          <input type="hidden" id="cancelled_session_id" value="{{$id}}" name="cancelled_session_id">
          <div class="form-group clearfix">
            <button type="submit" class="btn tutor-btn btn-small-pad" id="cancel_request_tutor">Yes</button>
            <button type="submit" class="btn btn-grey btn-small-pad" data-dismiss="modal">No</button>
          </div>
        {{Form::close()}}
