 @extends('frontend.layouts.tutor',['session' => $sessionData])
 @section('middle-content')
 <?php

$tutor_start_date = ConverTimeZoneForUser($tutor_availability_range['start_date'],'Y-m-d H:i:s');
$admin_end_range_date = date('d-m-Y', strtotime("+" . $tutor_availability_range['month']. " months", strtotime($tutor_start_date)));

  $admin_start_range_date = date('d-m-Y',strtotime($tutor_availability_range['start_date']));

  $admin_end_range_date_for_jquery = date('m/d/Y', strtotime($admin_end_range_date));
  ?>  
 @include('frontend.tutor.mid_section_my_availablity')
@endsection


