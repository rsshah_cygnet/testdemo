
  <div class="container-fluid">
    <!-- Left aside start from here -->
    @include('frontend.includes.tutor-leftsidebar')
    <!-- Left aside end from here -->
    <!-- Right side start form here-->
    <div class="content-wrapper">
      <div class="content">
        <div class="row">
          <?php

        $tutor_start_date = ConverTimeZoneForUser($tutor_availability_range['start_date'],'Y-m-d H:i:s');
        $admin_end_range_date = date('d-m-Y', strtotime("+" . $tutor_availability_range['month']. " months", strtotime($tutor_start_date)));

          $admin_start_range_date = date('d-m-Y',strtotime($tutor_availability_range['start_date']));

          $admin_end_range_date_for_jquery = date('m/d/Y', strtotime($admin_end_range_date));

          ?>  
          <div class="col-xs-12 col-lg-12">
            <div class="box">

              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                <input type="hidden" name="admin_end_range_date_for_jquery" id="admin_end_range_date_for_jquery" value="{{$admin_end_range_date_for_jquery}}">
                  <div class="box-header clearfix with-border">
                    <div class="pull-left box-title">
                      <h3 class="">My Availability</h3>
                    </div>

                  </div>
                  <div class="box-body">
                  <?php 
                  if(isset($pass_exam_count) && $pass_exam_count == 0){ ?>
                   <p style="color: green;">You can't set your availability because you have not passed any exam yet</p>
                   <?php } ?>
                    <h2 class="color-title heading-f-s-20 available-everyday"> Set sessions time for everyday</h2>
                    <h2 class="color-title heading-f-s-20 available-specific-day" style="display: none;">Set sessions date &amp; time</h2>

                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 form-group">
                        <div class="box-radio-user custom-radio tutor-box-radio-box m">
                          <input type="radio" name="available" id="tutor-user" class="user-tab" checked="" value="2">
                          <label for="tutor-user"><i class="fa fa-calendar"></i><span>Available Everyday *</span></label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 form-group">
                        <div class="box-radio-user custom-radio tutor-box-radio-box">
                          <input type="radio" name="available" id="tutee-user" class="user-tab" value="3">
                          <label for="tutee-user"><i class="fa fa-calendar"></i><span>Available at Specific Days</span></label>
                        </div>
                      </div>
                    </div>

                    <!-- Available Everyday section start form here-->

                    {!! Form::open(['url' => 'register', 'class' => '','onsubmit'=>'return false','id'=>'available_everyday']) !!}

                    <div class="available-everyday">

                      <div class="row">
                        <div class="col-xs-12 col-sm-10">
                          <p class="tutor-availability-b">* You would not be able to select the Date if you have selected available all day option </p>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
                          <div class="form-group">
                            <label>Available Date</label>
                            <div class="available-date-everyday">
                              <ul class="list-inline">
                                <li>{{$admin_start_range_date}}</li>
                                <li><span>To</span></li>
                                <li>{{ $admin_end_range_date }}</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group mrg-0"><label class="label-top-spacing">Select Time</label></div>
                      <div class="row select-time-spacing">

                        <div class="col-xs-8 col-sm-10 col-md-10 col-lg-9">

                          <div class="row">
                            <div class="col-sm-5 col-md-5">
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon"> <i class="fa fa-clock-o color-title"></i> </div>
                                  <input type="text" name = "from_time" id="from_time" class="form-control time-picker pull-right" Placeholder="From" >

                                </div>
                              </div>
                              <span class="text-right display-block has-error-text from_time_error from_time_error_value" style="display: none">Username already exist</span>
                            </div>
                            <div class="col-sm-2 col-md-1 text-center form-group">
                              <p class="select-time-to">To</p>
                            </div>

                            <div class="col-sm-5 col-md-5">
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon"> <i class="fa fa-clock-o color-title"></i> </div>
                                  <input type="text" name = "to_time" id="to_time" class="form-control time-picker pull-right" Placeholder="To" >

                                </div>
                              </div>
                              <span class="text-right display-block has-error-text to_time_error to_time_error_value" style="display: none">Username already exist</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 text-left form-group">
                <?php if(isset($pass_exam_count) && $pass_exam_count == 0){ ?>
                        <a href="javascript:void(0);" class="btn tutor-btn plus-btn"><i class="fa fa-plus" aria-hidden="true"></i></a>
                   <?php }else{ ?>  
                        <a href="#" class="btn tutor-btn plus-btn" onclick="submit_form()"><i class="fa fa-plus" aria-hidden="true"></i></a>
                  <?php } ?>      
                        </div>
                      </div>
                    </div>

                    {!! Form::submit('SAVE', ['class' => 'btn login-btn tuteme-md-btn','id' =>'available_everyday_btn','style'=>'display:none']) !!}
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="from_date" value="{{convertDateInSqlFormat($admin_start_range_date)}}">
                    <input type="hidden" name="to_date" value="{{convertDateInSqlFormat($admin_end_range_date)}}">
                    {!! Form::close() !!}

                    <!-- Available Everyday section end form here-->



                    <!-- Available at Specific Days section start form here-->

                    {!! Form::open(['url' => 'register', 'class' => '','onsubmit'=>'return false','id'=>'available_specific_day']) !!}

                    <div class="available-specific-day" style="display: none;">

                      <div class="row">
                        <div class="col-xs-12 col-sm-10">
                          <p class="tutor-availability-b">Click on Plus to save your changes and click on Add if you want to add the timeslot for other date</p>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">

                          <div class="form-group">
                            <label>Select Date</label>

                            <div class="input-group date">
                              <input type="text" class="form-control datepicker" name="from_date"
                              id="from_date_specific">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>

                          </div>

                        </div>
                      </div>

                      <div class="form-group mrg-0"><label class="label-top-spacing">Select Time</label></div>
                      <div class="row select-time-spacing">

                        <div class="col-xs-8 col-sm-10 col-md-10 col-lg-9">

                          <div class="row">
                            <div class="col-sm-5 col-md-5">
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon"> <i class="fa fa-clock-o color-title"></i> </div>
                                  <input type="text" name = "from_time" id="from_time_specific" class="form-control time-picker pull-right" Placeholder="From" >

                                </div>
                              </div>
                              <span class="text-right display-block has-error-text from_time_specific_error from_time_specific_error_value" style="display: none">Username already exist</span>
                            </div>
                            <div class="col-sm-2 col-md-1 text-center form-group">
                              <p class="select-time-to">To</p>
                            </div>

                            <div class="col-sm-5 col-md-5">
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon"> <i class="fa fa-clock-o color-title"></i> </div>
                                  <input type="text" name = "to_time" id="to_time_specific" class="form-control time-picker pull-right" Placeholder="To" >

                                </div>
                              </div>
                              <span class="text-right display-block has-error-text to_time_specific_error to_time_specific_error_value" style="display: none">Username already exist</span>
                            </div>

                          </div>
                        </div>

                        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 text-left form-group">
                            <?php if(isset($pass_exam_count) && $pass_exam_count == 0){ ?>
                       <a href="javascript:void(0);" class="btn tutor-btn plus-btn"><i class="fa fa-plus" aria-hidden="true"></i></a>
                   <?php }else{ ?>  
                        <a href="#" class="btn tutor-btn plus-btn" onclick="submit_form()"><i class="fa fa-plus" aria-hidden="true"></i></a>
                  <?php } ?>  


                        </div>
                      </div>
                    </div>

                    {!! Form::submit('SAVE', ['class' => 'btn login-btn tuteme-md-btn','id' =>'available_specific_day_btn','style'=>'display:none']) !!}
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    {!! Form::close() !!}

                    <!-- Second hidden section end form here-->

                  </div>
                </div>

                <!--  Selected time right side section start here -->

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 right-side-time-section available-everyday">
                  <div class="right-under-section">
                    <div class="col-xs-12">
                      <h2><i class="fa fa-clock-o"></i> Selected Time</h2>
                      <div class="selected-time-section-scroll">
                        <ol type="1" id="available_everyday_div">

                         @foreach($avalablity_every_day as $key=>$value)
                         <li class='<?php echo "availablity_" . $value['id'] ?>'>
                          <dl class="dl-horizontal selected-time-list d-i-b">
                            <dt>From Time:</dt>
                            <dd>{{ getonlytime(ConverTimeZoneForUser($value['start_time'],'H:i')) }}</dd>
                            <dt>To Time:</dt>
                            <dd>{{ getonlytime(ConverTimeZoneForUser($value['end_time'],'H:i')) }}</dd>
                          </dl>
                          <a href="#" class="btn tutor-dashboard-trash-btn select-time-trash" onclick="deleteTutorAvaliablity({{ $value['id'] }})">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </a>
                        </li>
                        @endforeach




                      </ol>
                    </div>
                  </div>
                </div>
              </div>

              <!--  Selected time right side section start here -->


              <!--  Selected time right side section start here -->

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 right-side-time-section available-specific-day" style="display: none;">
                <div class="right-under-section">
                  <div class="col-xs-12">
                    <h2><i class="fa fa-clock-o"></i> Selected Date &amp; Time</h2>
                    <div class="selected-time-section-scroll">
                      <ol id="available_specific_div">


                       @foreach($avalablity_specific_day as $key => $value)


                       <li class='<?php echo "availablity_" . $value['id'] ?>'>
                         <div class="horizontal-custom d-i-b">
                          <dl class="dl-horizontal selected-time-list ">
                            <dt>Date:</dt>
                            <dd>{{ getOnlyDate(ConverTimeZoneForUser($value['start_date'],'d-m-Y')) }}</dd>
                          </dl>
                          <dl class="dl-horizontal selected-time-list d-i-b from-to-time">
                            <dt>From Time:</dt>
                            <dd>{{ getonlytime(ConverTimeZoneForUser($value['start_time'],'H:i')) }}</dd>
                            <dt>To Time:</dt>
                            <dd>{{ getOnlyTime(ConverTimeZoneForUser($value['end_time'],'H:i')) }}</dd>
                          </dl>
                        </div>
                        <a href="#" class="btn tutor-dashboard-trash-btn select-time-trash" onclick="deleteTutorAvaliablity({{ $value['id'] }})">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                      </li>
                      @endforeach

                    </ol>
                  </div>
                </div>
              </div>
            </div>

            <!--  Selected time right side section start here -->


            <!--  save and cancel button start here -->

            <div class="col-xs-12 col-sm-6"> 
              <div class="form-group clearfix save-cancel-block">
                <!-- <button type="submit" class="btn tutor-primary-btn primary-btn-custom tuteme-md-btn"> SAVE </button> -->
                <a title="CANCEL" href="javascript:void(0);" onclick="loadTutorDashboard()">
                  <button type="submit" class="btn btn-grey tuteme-md-btn"> CANCEL </button>
                </a>
              </div>
            </div>   

            <!--  save and cancel button end here -->




          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Right side end form here-->
</div>
</div>
<!-- Right side end form here-->
</div>