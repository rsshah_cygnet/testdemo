 <div id="columnchart_material" style="width: 100%; height: 320px;"></div>
   <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
    <script type="text/javascript">
      google.charts.load("current", {packages:["bar"]});
        google.charts.setOnLoadCallback(drawChart2);
  function drawChart2() {
        var data = google.visualization.arrayToDataTable(<?php echo $columnChartArray; ?>);

         var options = {
            chart: {
              title: 'Ratings',

              //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
            },
            bars: 'vertical',
            //vAxis: {format: 'decimal'},
           // height: 400,
            legend: {position: 'top'},
            colors: ['#f79237', '#228B22'],
              width: '100%',
              height: '100%',
            
              chartArea: {
                /*left: "3%",
                top: "3%",*/
                height: "85%",
                width: "85%",
                
            },
          };

         // var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
         var chart =  new google.visualization.ColumnChart(document.getElementById('columnchart_material'));

          chart.draw(data, options);
      }
    </script>
