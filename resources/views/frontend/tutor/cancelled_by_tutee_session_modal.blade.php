<div id="cancelled_by_tutee_session_modal" class="modal fade session-popup-modal" role="dialog">
      <div class="modal-body modal-content modal-dialog">
         <div class="calendar-modal modal-red center-block">
          <div class="row">
           <div class="col-xs-12 col-sm-12">
            <button type="button" class="close modal-close-btn" data-dismiss="modal">&times;</button>
          </div>
        </div>
            <!-- <span class="calendar-arrow"></span> -->
              <h2><b>{{$session['0']['first_name']." ".$session['0']['last_name']}}</b></h2>
              <p>{{$session['0']['qualification_name']}}</p>
             <!--  <div class="ratings-block">
                  <label>Punctual Ratings</label><span>{{$session['0']['punctuality']}}<em>/10</em></span>
              </div> -->
              <div style="margin-bottom: 10px;">
       <?php 
        if(isset($session['0']['topic_id']) && $session['0']['topic_id'] != 0 && $session['0']['topic_id'] != ""){ 
          $curriculumHierarchy = getCurriculumHierarchy('',$session['0']['topic_id'],true); 
        }else{ 
             $curriculumHierarchy = getCurriculumHierarchy($session['0']['subject_id'],'',true);          
        }
      ?>
                  <p>Session : <b>{{$curriculumHierarchy}}</b></p>
                  <!-- <ul class="list-unstyled curriculam-list list-inline">
                      <li>Session</li>
                      <li>UK</li>
                      <li>IB</li>
                      <li>MYP</li>
                      <li>10th</li>
                      <li>English</li>
                      <li>Grammar</li>
                  </ul> -->
              </div>
              <div>
                  <ul class="schedule-list list-unstyled list-inline">
                      <li>
                          <label>Scheduled on :</label>
                          <div><i class="fa fa-calendar"></i>{{ConverTimeZoneForUser($session['0']['scheduled_date_time'],'d-m-Y')}}</div>
                          <div><i class="fa fa-clock-o"></i>{{ConverTimeZoneForUser($session['0']['scheduled_date_time'],'h:i a')}}</div>
                      </li>
                      <li>
                          <label>Accepted on :</label>
                          <div><i class="fa fa-calendar"></i>{{ConverTimeZoneForUser($session['0']['accepted_date_time'],'d-m-Y')}}</div>
                          <div><i class="fa fa-clock-o"></i>{{ConverTimeZoneForUser($session['0']['accepted_date_time'],'h:i a')}}</div>
                      </li>
                       <li>
                          <label>Cancelled on :</label>
                          <div><i class="fa fa-calendar"></i>{{ConverTimeZoneForUser($session['0']['cancelled_date_time'],'d-m-Y')}}</div>
                          <div><i class="fa fa-clock-o"></i>{{ConverTimeZoneForUser($session['0']['cancelled_date_time'],'h:i a')}}</div>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
     </div>