<?php

return [
	/*
		      |--------------------------------------------------------------------------
		      | Validation Language Lines
		      |--------------------------------------------------------------------------
		      |
		      | The following language lines contain the default error messages used by
		      | the validator class. Some of these rules have multiple versions such
		      | as the size rules. Feel free to tweak each of these messages here.
		      |
	*/

	'accepted' => 'The :attribute must be accepted.',
	'active_url' => 'The :attribute is not a valid URL.',
	'after' => 'The :attribute must be a date after :date.',
	'alpha' => 'The :attribute may only contain letters.',
	'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
	'alpha_num' => 'The :attribute may only contain letters and numbers.',
	'array' => 'The :attribute must be an array.',
	'before' => 'The :attribute must be a date before :date.',
	'between' => [
		'numeric' => 'The :attribute must be between :min and :max.',
		'file' => 'The :attribute must be between :min and :max kilobytes.',
		'string' => 'The :attribute must be between :min and :max characters.',
		'array' => 'The :attribute must have between :min and :max items.',
	],
	'boolean' => 'The :attribute field must be true or false.',
	'confirmed' => 'The :attribute confirmation does not match.',
	'date' => 'The :attribute is not a valid date.',
	'date_format' => 'The :attribute does not match the format :format.',
	'different' => 'The :attribute and :other must be different.',
	'digits' => 'The :attribute must be :digits digits.',
	'digits_between' => 'The :attribute must be between :min and :max digits.',
	'email' => 'The :attribute must be a valid email address.',
	'exists' => 'The selected :attribute is invalid.',
	'filled' => 'The :attribute field is required.',
	'image' => 'The :attribute must be an image.',
	'in' => 'The selected :attribute is invalid.',
	'integer' => 'The :attribute must be an integer.',
	'ip' => 'The :attribute must be a valid IP address.',
	'json' => 'The :attribute must be a valid JSON string.',
	'max' => [
		'numeric' => 'The :attribute may not be greater than :max.',
		'file' => 'The :attribute may not be greater than :max kilobytes.',
		'string' => 'The :attribute may not be greater than :max characters.',
		'array' => 'The :attribute may not have more than :max items.',
	],
	'mimes' => 'The :attribute must be a file of type: :values.',
	'min' => [
		'numeric' => 'The :attribute must be at least :min.',
		'file' => 'The :attribute must be at least :min kilobytes.',
		'string' => 'The :attribute must be at least :min characters.',
		'array' => 'The :attribute must have at least :min items.',
	],
	'not_in' => 'The selected :attribute is invalid.',
	'numeric' => 'The :attribute must be a number.',
	'regex' => 'The :attribute format is invalid.',
	'required' => 'The :attribute field is required.',
	'required_if' => 'The :attribute field is required when :other is :value.',
	'required_unless' => 'The :attribute field is required unless :other is in :values.',
	'required_with' => 'The :attribute field is required when :values is present.',
	'required_with_all' => 'The :attribute field is required when :values is present.',
	'required_without' => 'The :attribute field is required when :values is not present.',
	'required_without_all' => 'The :attribute field is required when none of :values are present.',
	'same' => 'The :attribute and :other must match.',
	'size' => [
		'numeric' => 'The :attribute must be :size.',
		'file' => 'The :attribute must be :size kilobytes.',
		'string' => 'The :attribute must be :size characters.',
		'array' => 'The :attribute must contain :size items.',
	],
	'string' => 'The :attribute must be a string.',
	'timezone' => 'The :attribute must be a valid zone.',
	'unique' => 'The :attribute has already been taken.',
	'url' => 'The :attribute format is invalid.',
	/*
		      |--------------------------------------------------------------------------
		      | Custom Validation Language Lines
		      |--------------------------------------------------------------------------
		      |
		      | Here you may specify custom validation messages for attributes using the
		      | convention "attribute.rule" to name the lines. This makes it quick to
		      | specify a specific custom language line for a given attribute rule.
		      |
	*/
	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
	],
	/*
		      |--------------------------------------------------------------------------
		      | Custom Validation Attributes
		      |--------------------------------------------------------------------------
		      |
		      | The following language lines are used to swap attribute place-holders
		      | with something more reader friendly such as E-Mail Address instead
		      | of "email". This simply helps us make messages a little cleaner.
		      |
	*/
	'attributes' => [
		'backend' => [
			'access' => [
				'permissions' => [
					'associated_roles' => 'Associated Roles',
					'dependencies' => 'Dependencies',
					'display_name' => 'Display Name',
					'group' => 'Group',
					'group_sort' => 'Group Sort',
					'groups' => [
						'name' => 'Group Name',
					],
					'name' => 'Name',
					'system' => 'System?',
				],
				'roles' => [
					'associated_permissions' => 'Associated Permissions',
					'name' => 'Name',
					'sort' => 'Sort',
					'status' => 'Status',
				],
				'users' => [
					'active' => 'Active',
					'associated_roles' => 'Associated Roles',
					'confirmed' => 'Confirmed',
					'email' => 'Email',
					'name' => 'Name',
					'first_name' => 'First Name',
					'last_name' => 'Last Name',
					'phone_no' => 'Contact Number',
					'other_permissions' => 'Other Permissions',
					'password' => 'Password',
					'password_confirmation' => 'Password Confirmation',
					'send_confirmation_email' => 'Send Confirmation E-mail',
					'select_dealership' => 'Select Dealership',
					'select_province_region' => 'Select Province Region',
					'input_type' => 'Input Type',
					'country' => 'Country Name',
					'city' => 'City Name',
				],
			],
			'media' => [
				'alt' => 'Alt Attribute',
				'description' => 'Description',
				'category' => 'Category',
			],
			'emailtemplate' => [
				'title' => 'Title',
				'subject' => 'Subject',
				'body' => 'Body',
				'type' => 'Type',
				'placeholder' => 'Add PlaceHolder',
			],
			'cmspages' => [
				'title' => 'Title',
				'description' => 'Description',
				'domain' => 'Domain',
				'select_domain' => 'Select Domain',
				'cannonical_link' => 'Cannonical Link',
				'seo_title' => 'SEO Title',
				'seo_keyword' => 'SEO Keyword',
				'seo_description' => 'SEO Description',
			],
			'level' => [
				'name' => 'Level name',
				'curriculum' => 'Curriculum',
				'educational' => 'Education System',
				'subject' => 'Subject',
				'program' => 'Program',
				'grade' => 'Grade',
			],

			'programs' => [
				'name' => 'Program name',
				'curriculum' => 'Curriculum',
				'educational' => 'Education System',
				'subject' => 'Subject',
				'level' => 'Level',
				'grade' => 'Grade',
			],

			'topic' => [
				'name' => 'Topic name',
				'curriculum' => 'Curriculum',
				'educational' => 'Education System',
				'subject' => 'Subject',
				'program' => 'Program',
				'grade' => 'Grade',
				'level' => 'Level',
			],
			'question' => [
				'name' => 'Question',
				'curriculum' => 'Curriculum',
				'topic' => 'Topic',
				'option1' => 'Option1',
				'option2' => 'Option2',
				'option3' => 'option3',
				'option4' => 'Option4',
				'image' => 'Image',
				'ans' => 'Correct answer',
			],

			'grade' => [
				'name' => 'Grade name',
				'type' => 'Grade Type',
				'curriculum' => 'Curriculum',
				'educational' => 'Education System',
				'subject' => 'Subject',
				'program' => 'Program',
				'grade' => 'Grade',
				'level' => 'Level',
			],
			'subject' => [
				'name' => 'Subject name',
				'curriculum' => 'Curriculum',
				'educational' => 'Education System',
				'grade' => 'Grade',
				'program' => 'Program',
				'level' => 'Level',

			],
			'article' => [
				'content' => 'Content',
				'specialization_id' => 'Specialization',
				'image' => 'Images',
				'comment' => 'Comment',
			],
			'notification' => [
				'message' => 'Message',
				'created_at' => 'Date/Time',
				'read' => 'Read',
			],
			'settings' => [
				'logo' => 'Site Logo',
				'favicon' => 'Fav icon',
				'seo_title' => 'Meta Title',
				'seo_keyword' => 'Meta Keywords',
				'seo_description' => 'Meta Description',
				'company_contact' => 'Company Contact',
				'company_address' => 'Company Address',
				'from_name' => 'From Name',
				'from_email' => 'From E-mail Address',
				'facebook' => 'Facebook Account',
				'linkedin' => 'Linkedin Account',
				'twitter' => 'Twitter Account',
				'google' => 'Google Account',
				'copyright_text' => 'Copyright Text',
				'footer_text' => 'Footer Text',
				'google_analytics' => 'Google Analytics',
				'home_video1' => 'Home Page Video 1',
				'home_video2' => 'Home Page Video 2',
				'home_video3' => 'Home Page Video 3',
				'home_video4' => 'Home Page Video 4',
				'explanation1' => 'Explanation Text 1',
				'explanation2' => 'Explanation Text 2',
				'explanation3' => 'Explanation Text 3',
				'explanation4' => 'Explanation Text 4',

				//Start: This is used in settings, topic and subject module
				'questions_per_topic' => 'No. of questions to be asked per Topic (For Lower Grade Only)',
				'minutes_per_questions' => 'No. of Minutes Per Question ',
				'passing_percentage' => 'Passing Percentage',
				'min_questions_in_test' => 'No. of maximum questions to be asked per Topic (For Higher Grade Only)',
				'effort_for_reappearing_test' => 'No. of Efforts Available For Reappearing Test',
				'higher_level_grade_fee' => 'Higher Level Grade Fees ($)',
				'lower_level_grade_fee' => 'Lower Level Grade Fees ($)',
				'tutor_availability_range' => 'Tutor Availability',
				'free_session_per_fb_count' => 'Free Session Per Facebook Count',
				'admin_commission_for_higher_level' => 'Admin Commission For Higher Level Grade(%)',
				'admin_commission_for_lower_level' => 'Admin Commission For Lower Level Grade(%)',
				//End:
			],
			'email' => 'E-mail Address',
			'name' => 'Name',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'avatar' => 'Avatar',
			'password' => 'Password',
			'password_confirmation' => 'Password Confirmation',
			'old_password' => 'Old Password',
			'new_password' => 'New Password',
			'new_password_confirmation' => 'New Password Confirmation',
			'phone_no' => 'Contact Number',
		],
		'frontend' => [
			'email' => 'E-mail Address',
			'name' => 'Name',
			'avatar' => 'Avatar',
			'password' => 'Password',
			'password_confirmation' => 'Password Confirmation',
			'old_password' => 'Old Password',
			'new_password' => 'New Password',
			'new_password_confirmation' => 'New Password Confirmation',
		],
	],
];
