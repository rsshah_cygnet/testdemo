<?php

return [
	/*
		      |--------------------------------------------------------------------------
		      | Labels Language Lines
		      |--------------------------------------------------------------------------
		      |
		      | The following language lines are used in labels throughout the system.
		      | Regardless where it is placed, a label can be listed here so it is easily
		      | found in a intuitive way.
		      |
	*/
	'general' => [
		'all' => 'All',
		'yes' => 'Yes',
		'no' => 'No',
		'custom' => 'Custom',
		'actions' => 'Actions',
		'buttons' => [
			'save' => 'Save',
			'update' => 'Update',
		],
		'hide' => 'Hide',
		'none' => 'None',
		'show' => 'Show',
		'toggle_navigation' => 'Toggle Navigation',
	],
	'backend' => [
		'access' => [
			'permissions' => [
				'create' => 'Create Permission',
				'dependencies' => 'Dependencies',
				'edit' => 'Edit Permission',
				'groups' => [
					'create' => 'Create Group',
					'edit' => 'Edit Group',
					'table' => [
						'name' => 'Name',
					],
				],
				'grouped_permissions' => 'Grouped Permissions',
				'label' => 'permissions',
				'management' => 'Permission Management',
				'no_groups' => 'There are no permission groups.',
				'no_permissions' => 'No permission to choose from.',
				'no_roles' => 'No Roles to set',
				'no_ungrouped' => 'There are no ungrouped permissions.',
				'table' => [
					'dependencies' => 'Dependencies',
					'group' => 'Group',
					'group-sort' => 'Group Sort',
					'name' => 'Display Name',
					'permission' => 'Permission',
					'roles' => 'Roles',
					'status' => 'Status',
					'system' => 'System',
					'total' => 'permission total|permissions total',
					'users' => 'Users',
				],
				'tabs' => [
					'general' => 'General',
					'groups' => 'All Groups',
					'dependencies' => 'Dependencies',
					'permissions' => 'All Permissions',
				],
				'ungrouped_permissions' => 'Ungrouped Permissions',
			],
			'roles' => [
				'create' => 'Create Role',
				'edit' => 'Edit Role',
				'management' => 'Role Management',
				'table' => [
					'number_of_users' => 'Number of Users',
					'permissions' => 'Permissions',
					'role' => 'Role',
					'sort' => 'Sort',
					'total' => 'role total|roles total',
					'status' => 'Status',
				],
				'placeHolders' => [
					'name' => 'Search Name',
				],
			],
			'users' => [
				'active' => 'Active Users',
				'all_permissions' => 'All Permissions',
				'change_password' => 'Change Password',
				'change_password_for' => 'Change Password for :user',
				'create' => 'Create Admin User',
				'deactivated' => 'Deactivated Users',
				'deleted' => 'Deleted Users',
				'dependencies' => 'Dependencies',
				'edit' => 'Edit User',
				'management' => 'Admin User Management',
				'no_other_permissions' => 'No Other Permissions',
				'no_permissions' => 'No Permissions',
				'no_roles' => 'No Roles to set.',
				'permissions' => 'Permissions',
				'permission_check' => 'Checking a permission will also check its dependencies, if any.',
				'table' => [
					'confirmed' => 'Confirmed',
					'created' => 'Created',
					'email' => 'E-mail',
					'id' => 'ID',
					'last_updated' => 'Updated',
					'name' => 'Name',
					'no_deactivated' => 'No Deactivated Users',
					'no_deleted' => 'No Deleted Users',
					'other_permissions' => 'Permissions',
					'roles' => 'Roles',
					'total' => 'user total|users total',
				],
				'placeHolders' => [
					'name' => 'Search Name',
					'email' => 'Search Email',
				],
			],
		],
		'cmspages' => [
			'active' => 'Active CMS Pages',
			'create' => 'Create CMS Page',
			'edit' => 'Edit CMS Page',
			'management' => 'CMS Pages',
			'management-sub' => 'CMS Pages',
			'table' => [
				'title' => 'Title',
				'created' => 'Created At',
				'last_updated' => 'Updated At',
				'total' => 'CMS Page total|CMS Pages total',
			],
			'placeHolders' => [
				'title' => 'Search Title',
			],
		],
		'level' => [
			'active' => 'Active Level',
			'create' => 'Create Level',
			'edit' => 'Edit Level',
			'management' => 'Level Management',
			'management-sub' => 'Levels',
			'table' => [
				'title' => 'Title',
				'created' => 'Created At',
				'last_updated' => 'Updated At',
				'total' => 'level Page total|level Pages total',
			],
			'placeHolders' => [
				'title' => 'Search name',
			],
		],
		'topic' => [
			'active' => 'Active Topic',
			'create' => 'Create Topic',
			'edit' => 'Edit Topic',
			'management' => 'Topic Management',
			'management-sub' => 'Topic',
			'table' => [
				'title' => 'Title',
				'created' => 'Created At',
				'last_updated' => 'Updated At',
				'total' => 'Topic Page total|Topic Pages total',
			],
			'placeHolders' => [
				'title' => 'Search name',
			],
		],
		'question' => [
			'active' => 'Active Question',
			'create' => 'Create Question',
			'edit' => 'Edit Question',
			'management' => 'Question Management',
			'management-sub' => 'Question',
			'table' => [
				'title' => 'Title',
				'created' => 'Created At',
				'last_updated' => 'Updated At',
				'total' => 'Question Page total|Question Pages total',
			],
			'placeHolders' => [
				'title' => 'Search Question',
			],
		],
		'grade' => [
			'active' => 'Active Grade',
			'create' => 'Create Grade',
			'edit' => 'Edit Grade',
			'management' => 'Grade Management',
			'management-sub' => 'Grade',
			'table' => [
				'title' => 'Title',
				'created' => 'Created At',
				'last_updated' => 'Updated At',
				'total' => 'CMS Page total|CMS Pages total',
			],
			'placeHolders' => [
				'title' => 'Search name',
			],
		],
		'subject' => [
			'active' => 'Active Subject',
			'create' => 'Create Subject',
			'edit' => 'Edit Subject',
			'management' => 'Subject Management',
			'management-sub' => 'Subject',
			'table' => [
				'title' => 'Title',
				'created' => 'Created At',
				'last_updated' => 'Updated At',
				'total' => 'CMS Page total|CMS Pages total',
			],
			'placeHolders' => [
				'title' => 'Search name',
			],
		],
		'programs' => [
			'active' => 'Active Program',
			'create' => 'Create Program',
			'edit' => 'Edit Program',
			'management' => 'Program Management',
			'management-sub' => 'Programs',
		],
		'settings' => [
			'edit' => 'Settings',
			'management' => 'Settings Management',
			'management-sub' => 'Settings',
		],
		'dealership' => [
			'list' => 'List Dealerships',
			'create' => 'Create Dealerships',
			'edit' => 'Edit Dealership',
			'management' => 'Dealership Management',
			'management-sub' => 'Dealership',
		],
		'blogcategory' => [
			'management' => 'Blog Category Management',
			'edit' => 'Edit Settings',
		],
		'derivative' => [
			'edit' => 'Edit derivative',
			'create' => 'Create derivative',
			'management' => 'Derivative Management',
			'management-sub' => 'derivative',
		],
		'blogtag' => [
			'edit' => 'Edit Settings',
			'management' => 'Blog Tag Management',
			'management-sub' => 'Settings',
		],
		'notification' => [
			'create' => 'Create Notification',
			'list' => 'Notifications',
			'edit' => 'Edit Notification',
			'management' => 'Notification Management',
			'management-sub' => 'Notifications',
			'table' => [
				'message' => 'Message',
				'type' => 'Notification Type',
				'created_at' => 'Date/Time',
				'read' => 'Read',
				'status' => 'Status',
			],
			'placeHolders' => [
				'name' => 'Search Name',
			],
		],
		'media' => [
			'create' => 'Upload Media',
			'edit' => 'Edit Media Details',
			'management' => 'Media Management',
			'management-sub' => 'Media',
			'table' => [
				'filename' => 'FileName',
				'thumbnail' => 'Thumbnail',
				'created' => 'Created At',
				'total' => 'Media total',
			],
			'placeHolders' => [
				'name' => 'Search Name',
			],
		],
		'emailtemplate' => [
			'create' => 'Create Email Template',
			'edit' => 'Edit Email Template',
			'management' => 'Email Template',
			'management-sub' => 'Email Templates',
			'table' => [
				'title' => 'Title',
				'subject' => 'Subject',
				'body' => 'Body',
				'last_updated' => 'Last Updated',
				'total' => 'Email Template total|Email Templates total',
			],
			'placeHolders' => [
				'title' => 'Search Title',
				'subject' => 'Search Subject',
			],
		],
		'article' => [
			'create' => 'Create Article',
			'list' => 'List Article',
			'edit' => 'Edit Article',
			'view' => 'View Article',
			'comment' => 'Comments',
			'add-comment' => 'Add Comment',
			'management' => 'Article Management',
			'management-sub' => 'Articles',
		],
		'user' => [
			'passwords' => [
				'change' => 'Change Password',
			],
			'profile' => [
				'avatar' => 'Avatar',
				'created_at' => 'Created At',
				'edit_information' => 'Edit Information',
				'email' => 'E-mail',
				'last_updated' => 'Last Updated',
				'name' => 'Name',
				'first_name' => 'First Name',
				'last_name' => 'Last Name',
				'update_information' => 'Update Information',
				'phone_no' => 'Contact Number',
			],
		],
		'user_report' => [
			'user_reports_by_tutor' => 'User Reports By Tutor',
			'user_reports_by_tutee' => 'User Reports By Tutee',
		],
		// general  @to do add it in to general
		'page_size' => 'Show: ',
		'search' => 'Search Records',
		'delete' => 'Delete All',
		'export_excel' => 'Export As Excel',
		'export_csv' => 'Export As CSV',
		'records_deleted' => 'Record(s) has been deleated sucessfully',
		'records_activated' => 'Record status has been changed sucessfully',
		'no_record_found' => 'No matching records found',
	],
	'frontend' => [
		'auth' => [
			'login_box_title' => 'Login',
			'login_button' => 'Login',
			'login_with' => 'Login with :social_media',
			'register_box_title' => 'Register',
			'register_button' => 'Register',
			'register_basic' => 'Next',
			'remember_me' => 'Remember Me',
		],
		'passwords' => [
			'forgot_password' => 'Forgot password?',
			'reset_password_box_title' => 'Reset Password',
			'reset_password_button' => 'Reset Password',
			'send_password_reset_link_button' => 'Send Password Reset Link',
		],
		'macros' => [
			'country' => [
				'alpha' => 'Country Alpha Codes',
				'alpha2' => 'Country Alpha 2 Codes',
				'alpha3' => 'Country Alpha 3 Codes',
				'numeric' => 'Country Numeric Codes',
			],
			'macro_examples' => 'Macro Examples',
			'state' => [
				'mexico' => 'Mexico State List',
				'us' => [
					'us' => 'US States',
					'outlying' => 'US Outlying Territories',
					'armed' => 'US Armed Forces',
				],
			],
			'territories' => [
				'canada' => 'Canada Province & Territories List',
			],
			'timezone' => 'Timezone',
		],
		'user' => [
			'passwords' => [
				'change' => 'Change Password',
			],
			'profile' => [
				'avatar' => 'Avatar',
				'created_at' => 'Created At',
				'edit_information' => 'Edit Information',
				'email' => 'E-mail',
				'last_updated' => 'Last Updated',
				'name' => 'Name',
				'update_information' => 'Update Information',
			],
		],
	],

];
