<?php

return [
	/*
		      |--------------------------------------------------------------------------
		      | Menus Language Lines
		      |--------------------------------------------------------------------------
		      |
		      | The following language lines are used in menu items throughout the system.
		      | Regardless where it is placed, a menu item can be listed here so it is easily
		      | found in a intuitive way.
		      |
	*/

	'backend' => [
		'educationalsystem' => [
			'main' => 'Action',
			'sidebar' => 'Educational System',
			'all' => 'All',
			'create' => 'Create',
			'management' => 'Educational System Management',
		],
		'aggregator' => [
			'all' => 'All',
			'create' => 'Create',
		],
		'special' => [
			'main' => 'Special Management',
			'create' => 'Create',
		],
		'noticeboard' => [
			'all' => 'All',
			'create' => 'Create Notice Board',
			'notice_board' => 'Notice Board',
		],
		'contactus' => [
			'main' => 'ContactUs Management',
		],
		'topic' => [
			'main' => 'Action',
			'sidebar' => 'Topic',
			'all' => 'All',
			'create' => 'Create',
		],
		'question' => [
			'main' => 'Action',
			'sidebar' => 'Question',
			'all' => 'All',
			'create' => 'Create',
			'import' => 'Import',
			'questionm' => 'Question Management',
		],
		'access' => [
			'title' => 'Admin User Management',
			'user' => 'Users Management',
			'dealership_management' => 'Dealership Mangement',
			'blogs' => 'Blogs',
			'deal_website_management' => 'Deal Website Management',
			'permissions' => [
				'all' => 'All Permissions',
				'create' => 'Create Permission',
				'edit' => 'Edit Permission',
				'groups' => [
					'all' => 'All Groups',
					'create' => 'Create Group',
					'edit' => 'Edit Group',
					'main' => 'Groups',
				],
				'main' => 'Permissions',
				'management' => 'Permission Management',
			],
			'roles' => [
				'all' => 'All Roles',
				'create' => 'Create Role',
				'edit' => 'Edit Role',
				'management' => 'Role Management',
				'main' => 'Roles',
			],
			'users' => [
				'all' => 'All Admin Users',
				'change-password' => 'Change Password',
				'create' => 'Create Admin User',
				'deactivated' => 'Deactivated Users',
				'deleted' => 'Deleted Users',
				'edit' => 'Edit Admin User',
				'main' => 'Admin Users',
			],
		],
		'settings' => [
			'management' => 'Job Type Management',
			'main' => 'Settings',
		],
		/*
			          'media' => [
			          'all' => 'All Media',
			          'create' => '',
			          'edit' => '',
			          'management' => 'Media Management',
			          'main' => 'Media',
		*/
		'emailtemplate' => [
			'all' => 'List',
			'create' => 'Add New',
			'edit' => 'Edit EmailTemplate',
			'management' => 'Email Template Management',
			'main' => 'Email Templates',
		],
		'deal_website' => [
			'all' => 'All Deal Website',
			'create' => 'Create Deal Website',
			'edit' => 'Edit Deal Website',
		],
		'notification' => [
			'all' => 'All Notification',
			'create' => 'Create Notification',
			'edit' => 'Edit Notification',
			'management' => 'Notification Management',
			'main' => 'Notifications',
		],
		'deal-website' => [
			'main' => 'Deal Website',
		],
		'article' => [
			'all' => 'All Article',
			'create' => 'Create Article',
			'edit' => 'Edit Article',
			'management' => 'Article Management',
			//'main' => 'Articles',
			'main' => 'Blogs',
		],
		'cmspages' => [
			'all' => 'All Pages',
			'create' => 'Create Pages',
			'edit' => 'Edit Pages',
			'main' => 'CMS Pages',
			'management' => 'CMS Pages Management',
		],
		'log-viewer' => [
			'main' => 'Log Viewer',
			'dashboard' => 'Dashboard',
			'logs' => 'Logs',
		],
		'sidebar' => [
			'dashboard' => 'Dashboard',
			'general' => 'General',
			'masters' => 'Masters',
		],
		'vehiclemodeltype' => [
			'select' => 'Actions',
			'all' => 'All Vehicle Model Type',
			'create' => 'Create Vehicle Model Type',
		],
		'menumanagement' => [
			'select' => 'Actions',
			'all' => 'List',
			'create' => 'Add New',
		],
		'blog_category' => [
			'all' => 'All Blog Category',
			'create' => 'Create Blog Category',
		],
		'blog' => [
			'all' => 'List',
			'create' => 'Add New',
		],
		'levels' => [
			'all' => 'List',
			'create' => 'Add New',
		],
		'grade' => [
			'all' => 'List',
			'create' => 'Add New',
		],
		'subject' => [
			'all' => 'List',
			'create' => 'Add New',
		],
		'blogTag' => [
			'all' => 'All Blog Tags',
			'create' => 'Create Blog Tag',
		],
		'derivative' => [
			'all' => 'All Derivative',
			'create' => 'Create Derivative',
			'edit' => 'Edit Derivative',
			'main' => 'Derivative',
			'management' => 'Derivative Management',
		],
		'special-type' => [
			'all' => 'All Special Types',
			'change-password' => 'Change Password',
			'create' => 'Create Special Type',
			'deactivated' => 'Deactivated Special Type',
			'deleted' => 'Deleted Special Type',
			'edit' => 'Edit Special Type',
			'main' => 'Special Type',
		],
		'support-ticket' => [
			'all' => 'All Support Tickets',
			'create' => 'Raise Support Ticket',
			'deactivated' => 'Deactivated Support Tickets',
			'deleted' => 'Deleted Support Tickets',
			'edit' => 'Edit Support Ticket',
			'main' => 'Support Tickets',
		],
		'programs' => [
			'all' => 'List',
			'create' => 'Add New',
		],
		'punctuality_rating_success_session' => [
			'management' => 'Punctuality Rating Management For Successful Session',
		],
		'punctuality_rating_cancel_session' => [
			'all' => 'List',
			'create' => 'Add New',
			'management' => 'Punctuality Rating Management For Cancel Session ',
		],
		'session_cancel_penalty' => [
			'all' => 'List',
			'create' => 'Add New',
			'management' => 'Penalty Management for Cancel Session',
		],

	],
	'language-picker' => [
		'language' => 'Language',
		/**
		 * Add the new language to this array.
		 * The key should have the same language code as the folder name.
		 * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
		 * Be sure to add the new language in alphabetical order.
		 */
		'langs' => [
			'de' => 'German',
			'en' => 'English',
			'es' => 'Spanish',
			'fr' => 'French',
			'it' => 'Italian',
			'pt-BR' => 'Brazilian Portuguese',
			'sv' => 'Swedish',
		],
	],
	'language-picker' => [
		'language' => 'Language',
		/**
		 * Add the new language to this array.
		 * The key should have the same language code as the folder name.
		 * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
		 * Be sure to add the new language in alphabetical order.
		 */
		'langs' => [
			'de' => 'German',
			'en' => 'English',
			'es' => 'Spanish',
			'fr' => 'French',
			'it' => 'Italian',
			'pt-BR' => 'Brazilian Portuguese',
			'sv' => 'Swedish',
		],
	],
];
