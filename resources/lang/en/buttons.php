<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Buttons Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in buttons throughout the system.
      | Regardless where it is placed, a button can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
        'access' => [
            'users' => [
                'activate' => 'Activate',
                'change_password' => 'Change Password',
                'deactivate' => 'Deactivate',
                'delete_permanently' => 'Delete Permanently',
                'resend_email' => 'Resend Confirmation E-mail',
                'restore_user' => 'Restore User',
            ],
        ],
        'activate' => 'Activate',
        'deactivate' => 'Deactivate',
        'read' => 'Read',
        'unread' => 'Unread',
    ],
    'general' => [
        'cancel' => 'Cancel',
        'preview' => 'Preview',
        'crud' => [
            'create' => 'Create',
            'delete' => 'Delete',
            'edit' => 'Edit',
            'add' => 'Add',
            'download' => 'Download',
            'update' => 'Update',
            'change_password' => 'Change Password',
        ],
        'save' => 'Save',
        'view' => 'View',
    ],
    'userTestResult' => [
    'view' => 'View Test Result',
    'pass' => 'Pass',
    'fail' => 'Fail',
    ]
];
