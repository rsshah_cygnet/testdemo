<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Author: Disha Goyal
 * Date: 11/23/2016
 */

return [
    'DATEFORMAT' => 'd/m/Y',
    'ARTICLELIMIT' => '5',
    'COMMENTLIMIT' => '5',
    'FORUMLIMIT' => '5',
    'FORUMCOMMENTLIMIT' => '2',

    /**
     * Price Range is used for Cars Refine Section
     */
    'PRICE_RANGE' => [
        '0-500000' => 'R 0 - R 500 000',
        '0-500000+' => 'R 0 - R 500 000 +',
        '0-600000' => 'R 0 - R 600 000',
        '0-500000+' => 'R 0 - R 600 000 +',
        '0-700000' => 'R 0 - R 700 000',
        '0-700000+' => 'R 0 - R 700 000 +',
        '0-800000' => 'R 0 - R 800 000',
        '0-800000+' => 'R 0 - R 800 000 +'
    ],

    /**
     * Monthly Range is used for Cars Refine Section
     */
    'MONTHLY_RANGE' => [
        '0-1000' => 'R 0 - R 1,000',
        '0-1000+' => 'R 0 - R 1,000 +',
        '0-2000' => 'R 0 - R 2,000',
        '0-2000+' => 'R 0 - R 2,000 +',
        '0-3000' => 'R 0 - R 3,000',
        '0-3000+' => 'R 0 - R 3,000 +',
        '0-4000' => 'R 0 - R 4,000',
        '0-4000+' => 'R 0 - R 4,000 +',
        '0-5000' => 'R 0 - R 5,000',
        '0-5000+' => 'R 0 - R 5,000 +'
    ],
    'UPLOADFILE' => 'uploads',
    'MIN_IMAGE_UPLOAD' => 5,
    'BLOG_IMAGE_HEIGHT' => 306,
    'BLOG_IMAGE_WIDTH' => 600,

    'report_weekly' => 7,
    'report_fortnightly' => 15,
    'report_monthly' => 30,
    'report_semi_yearly' => 180,
    'report_yearly' => 365,

];
