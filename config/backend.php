<?php

return [
    'users_table' => 'users',
    'assigned_roles_table' => 'assigned_roles',
    'permission_user_table' => 'permission_user',
    'roles_table' => 'roles',
    'permissions_table' => 'permissions',
    'derivatives_table' => 'derivatives',
    
     /*
     * Notifications table to store notifications ofuser into the database.
     * Creator : Sagar
     */
    'notification_table' => 'notifications',
    
    /*
     * Cmspages table used by Access to save cmspage to the database.
     */
    'cmspage_table' => 'cmspages',
    /*
     * Settings table used by Access to save cmspage to the database.
     */
    'setting_table' => 'settings',
    
    /*
     * Media table is used to store media files and related information
     */
    'media_table' => 'media_files',

    /*
     * Tables related to email templates
     */
    'email_template_table' => 'email_template',
    'email_template_placeholder_table' => 'email_template_placeholder',
    
    /*
     * Tables related to article/newsfeed
     */
    'article_table' => 'articles',
    'article_specialization_table' => 'article_specializations',
    'article_comment_table' => 'article_comments',
    'article_image_table' => 'article_images',
    'article_like_table' => 'article_likes',
    'specialization_table' => 'specializations',
    'city_table' => 'city',
    
    /**
     * Skin for Admin LTE backend theme
     *
     * Available options:
     *
     * black
     * black-light
     * blue
     * blue-light
     * green
     * green-light
     * purple
     * purple-light
     * red
     * red-light
     * yellow
     * yellow-light
     */
    'theme' => 'purple',

    'general' => [
        /*
         * Administration tables
         */
        'default_per_page' => 10,
    ],

    'days' => [
        '10' => 10,
        '20' => 20,
        '30' => 30,
        '40' => 40,
        '50' => 50,
        '60' => 60,
        '70' => 70,
        '80' => 80,
        '90' => 90,
        '100' => 100,
        '110' => 110,
        '120' => 120,
        '130' => 130,
        '140' => 140,
        '150' => 150,
    ],
];
