<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

            Validator::extend('greater_than', function ($attribute, $value, $params, $validator) {

                $other = Input::get($params[0]);
                return intval($value) >= intval($other);
            });

            Validator::replacer('greater_than', function ($message, $attribute, $rule, $params) {
                return str_replace('_', ' ', 'The ' . $attribute . ' must be greater than the ' . $params[0]);
            });


             Validator::extend('less_than', function ($attribute, $value, $params, $validator) {

                $other = Input::get($params[0]);
                return intval($value) <= intval($other);
            });

            Validator::replacer('less_than', function ($message, $attribute, $rule, $params) {
                return str_replace('_', ' ', 'The ' . $attribute . ' must be less than the ' . $params[0]);
            });


        /**
         * Application locale defaults for various components
         *
         * These will be overridden by LocaleMiddleware if the session local is set
         */

        /**
         * setLocale for php. Enables ->formatLocalized() with localized values for dates
         */
        setLocale(LC_TIME, config('app.locale_php'));

        /**
         * setLocale to use Carbon source locales. Enables diffForHumans() localized
         */
        Carbon::setLocale(config('app.locale'));

        /**
         * setSettings to use Settings locales.
         */

        $schema = \Schema::hasTable('settings');
        if ($schema === true) {
            $settings = Setting::latest()->first();
            if (!empty($settings)) {
                view()->share('settings', $settings);
            }
        } else {
            //Else Condition
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
