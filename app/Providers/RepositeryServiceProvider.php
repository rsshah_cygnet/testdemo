<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositeryServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {

		/**
		 * FrontUsers
		 */
		$this->app->bind(
			\App\Repositories\Backend\Frontusers\FrontusersRepositoryContract::class, \App\Repositories\Backend\Frontusers\EloquentFrontusersRepository::class
		);

		/**
		 * UserTestREsult
		 */
		$this->app->bind(
			\App\Repositories\Backend\UserTestResult\UserTestResultContract::class, \App\Repositories\Backend\UserTestResult\EloquentUserTestResultRepository::class
		);

		/**
		 * Cmspages
		 */
		$this->app->bind(
			\App\Repositories\Backend\Cmspages\CmspagesRepositoryContract::class, \App\Repositories\Backend\Cmspages\EloquentCmspagesRepository::class
		);

		/**
		 * Settings
		 */
		$this->app->bind(
			\App\Repositories\Backend\Settings\SettingsRepositoryContract::class, \App\Repositories\Backend\Settings\EloquentSettingsRepository::class
		);

		/**
		 * Notification
		 */
		$this->app->bind(
			\App\Repositories\Backend\Notification\NotificationRepositoryContract::class, \App\Repositories\Backend\Notification\EloquentNotificationRepository::class
		);

		/**
		 * EmailTemplate
		 */
		$this->app->bind(
			\App\Repositories\Backend\EmailTemplate\EmailTemplateRepositoryContract::class, \App\Repositories\Backend\EmailTemplate\EloquentEmailTemplateRepository::class
		);

		/**
		 * Blog
		 */
		$this->app->bind(
			\App\Repositories\Backend\Blog\BlogRepositoryContract::class,
			\App\Repositories\Backend\Blog\EloquentBlogRepository::class

		);

		/**
		 * Notice Board
		 */
		$this->app->bind(
			\App\Repositories\Backend\NoticeBoard\NoticeBoardRepositoryContract::class, \App\Repositories\Backend\NoticeBoard\EloquentNoticeBoardRepository::class
		);

		/**
		 * Menu Management
		 */
		$this->app->bind(
			\App\Repositories\Backend\MenuManagement\MenuManagementRepositoryContract::class, \App\Repositories\Backend\MenuManagement\EloquentMenuManagementRepository::class
		);

		/**
		 * Contact Us
		 */
		$this->app->bind(
			\App\Repositories\Backend\Contactus\ContactusRepositoryContract::class, \App\Repositories\Backend\Contactus\EloquentContactusRepository::class
		);

		/**
		 * Curriculum Management
		 */
		$this->app->bind(
			\App\Repositories\Backend\Curriculum\CurriculumRepositoryContract::class, \App\Repositories\Backend\Curriculum\EloquentCurriculumRepository::class
		);

		/**
		 * Levels Management
		 */

		$this->app->bind(
			\App\Repositories\Backend\Levels\LevelsRepositoryContract::class, \App\Repositories\Backend\Levels\EloquentLevelsRepository::class
		);

		/**
		 * Education Systems
		 */
		$this->app->bind(
			\App\Repositories\Backend\EducationalSystem\EducationalSystemContract::class,
			\App\Repositories\Backend\EducationalSystem\EloquentEducationalSystemRepository::class
		);

		/**
		 * Programs Management
		 */

		$this->app->bind(
			\App\Repositories\Backend\Programs\ProgramsRepositoryContract::class,
			\App\Repositories\Backend\Programs\EloquentProgramsRepository::class
		);

		/**
		 * Topic
		 */
		$this->app->bind(
			\App\Repositories\Backend\Topic\TopicRepositoryContract::class, \App\Repositories\Backend\Topic\EloquentTopicRepository::class
		);
		/*
			 * Grade Management
		*/

		$this->app->bind(
			\App\Repositories\Backend\Grade\GradeRepositoryContract::class, \App\Repositories\Backend\Grade\EloquentGradeRepository::class
		);

		/**
		 * Subject Management
		 */

		$this->app->bind(
			\App\Repositories\Backend\Subject\SubjectRepositoryContract::class, \App\Repositories\Backend\Subject\EloquentSubjectRepository::class

		);

		/**
		 * Question Management
		 */

		$this->app->bind(
			\App\Repositories\Backend\Question\QuestionRepositoryContract::class, \App\Repositories\Backend\Question\EloquentQuestionRepository::class

		);

		/**
		 * Session success
		 */

		$this->app->bind(
			\App\Repositories\Backend\SessionSuccess\SessionSuccessRepositoryContract::class, \App\Repositories\Backend\SessionSuccess\EloquentSessionSuccessRepository::class

		);

		/**
		 * Tutor List
		 */

		$this->app->bind(
			\App\Repositories\Frontend\Tutor\TutorRepositoryContract::class,
			\App\Repositories\Frontend\Tutor\EloquentTutorRepository::class

		);

		/**
		 * Session upcoming
		 */

		$this->app->bind(
			\App\Repositories\Backend\SessionUpcoming\SessionUpcomingRepositoryContract::class, \App\Repositories\Backend\SessionUpcoming\EloquentSessionUpcomingRepository::class

		);

		/**
		 * SessionCancelledByTutor
		 */

		$this->app->bind(
			\App\Repositories\Backend\SessionCancelledByTutor\SessionCancelledByTutorRepositoryContract::class, \App\Repositories\Backend\SessionCancelledByTutor\EloquentSessionCancelledByTutorRepository::class

		);

		/**
		 * SessionCancelledByTutee
		 */

		$this->app->bind(
			\App\Repositories\Backend\SessionCancelledByTutee\SessionCancelledByTuteeRepositoryContract::class, \App\Repositories\Backend\SessionCancelledByTutee\EloquentSessionCancelledByTuteeRepository::class

		);

		/**
		 * Testimonial
		 */
		$this->app->bind(
			\App\Repositories\Backend\Testimonial\TestimonialRepositoryContract::class, \App\Repositories\Backend\Testimonial\EloquentTestimonialRepository::class
		);

		/**
		 * Session Feedback
		 */
		$this->app->bind(
			\App\Repositories\Backend\SessionFeedback\SessionFeedbackRepositoryContract::class,
			\App\Repositories\Backend\SessionFeedback\EloquentSessionFeedbackRepository::class
		);

		/**
		 * Punctuality Ratings for successful session
		 */
		$this->app->bind(
			\App\Repositories\Backend\PunctualityRatings\PunctualityRatingsRepositoryContract::class,
			\App\Repositories\Backend\PunctualityRatings\EloquentPunctualityRatingsRepository::class
		);

		/**
		 * Punctuality Ratings for cancel session
		 */
		$this->app->bind(
			\App\Repositories\Backend\PunctualityRatingCancelSession\PunctualityRatingCancelSessionRepositoryContract::class,
			\App\Repositories\Backend\PunctualityRatingCancelSession\EloquentPunctualityRatingCancelSessionRepository::class
		);

		/**
		 * Session cancel penalty
		 */
		$this->app->bind(
			\App\Repositories\Backend\SessionCancelPenalty\SessionCancelPenaltyRepositoryContract::class,
			\App\Repositories\Backend\SessionCancelPenalty\EloquentSessionCancelPenaltyRepository::class
		);

		/**
		 * Front User Questions
		 */
		$this->app->bind(
			\App\Repositories\Frontend\Test\TestContract::class,
			\App\Repositories\Frontend\Test\EloquentTestRepository::class
		);

		/**
		 * Front User Share Experience
		 */
		$this->app->bind(
			\App\Repositories\Frontend\ShareExperience\ShareExperienceRepositoryContract::class,
			\App\Repositories\Frontend\ShareExperience\EloquentShareExperienceRepository::class
		);

		/**
		 * Front User - Tutee
		 */
		$this->app->bind(
			\App\Repositories\Frontend\Tutee\TuteeRepositoryContract::class,
			\App\Repositories\Frontend\Tutee\EloquentTuteeRepository::class
		);

		/**
		 * Tutor Session Request
		 */
		$this->app->bind(
			\App\Repositories\Frontend\TutorSessionRequest\TutorSessionRequestRepositoryContract::class,
			\App\Repositories\Frontend\TutorSessionRequest\EloquentTutorSessionRequestRepository::class
		);

		/**
		 * Tutee Session Request
		 */
		$this->app->bind(
			\App\Repositories\Frontend\TuteeSessionRequest\TuteeSessionRequestRepositoryContract::class,
			\App\Repositories\Frontend\TuteeSessionRequest\EloquentTuteeSessionRequestRepository::class
		);

		/**
		 * Tutee Profile
		 */
		$this->app->bind(
			\App\Repositories\Frontend\TuteeProfile\TuteeProfileRepositoryContract::class,
			\App\Repositories\Frontend\TuteeProfile\EloquentTuteeProfileRepository::class
		);
		/*
			 * Tutee Payment Request
		*/

		$this->app->bind(
			\App\Repositories\Frontend\TuteePayment\TuteePaymentRepositoryContract::class,
			\App\Repositories\Frontend\TuteePayment\EloquentTuteePaymentRepository::class
		);

		/**
		 * Tutor Payment Request
		 */
		$this->app->bind(
			\App\Repositories\Frontend\TutorPayment\TutorPaymentRepositoryContract::class,
			\App\Repositories\Frontend\TutorPayment\EloquentTutorPaymentRepository::class
		);

		/**
		 * Tutee Free Session
		 */
		$this->app->bind(
			\App\Repositories\Frontend\TuteeFreeSesssion\TuteeFreeSesssionRepositoryContract::class,
			\App\Repositories\Frontend\TuteeFreeSesssion\EloquentTuteeFreeSesssionRepository::class
		);

		/**
		 * User reports by tutor
		 */
		$this->app->bind(
			\App\Repositories\Backend\UserReports\UserReportsRepositoryContract::class,
			\App\Repositories\Backend\UserReports\EloquentUserReportsRepository::class
		);

		/**
		 * User reports by tutee
		 */
		$this->app->bind(
			\App\Repositories\Backend\UserReportsByTutee\UserReportsByTuteeRepositoryContract::class,
			\App\Repositories\Backend\UserReportsByTutee\EloquentUserReportsByTuteeRepository::class
		);

	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}

}
