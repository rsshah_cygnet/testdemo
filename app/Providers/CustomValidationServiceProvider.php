<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Image;
/**
 * ValidationServiceProvider
 *
 * @author Amit Pandey avpandey@cygnet-infotech.com
 */
class CustomValidationServiceProvider extends ServiceProvider {
    
    /**
     * Bootstrap application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set custom validation rule for colours        
        Validator::extend('arrayUnique', function($attribute, $value, $parameters, $validator) {
            $value = array_map('trim',$value);
            $value = array_map('StrToLower',$value);
            return count($value) === count(array_unique($value));
        });
        
        // Set Message for defined rule for colours
        Validator::replacer('arrayUnique', function($message, $attribute, $rule, $parameters) {
            return str_replace($message, $attribute." must be unique", $message);
        });
        
        // Set custom validation rule for number of images required during insert        
        Validator::extend('minNoOfImages', function($attribute, $value, $parameters, $validator) {
            $images = json_decode($value);
            $filePath = "vehicle_images";
            if(count($images) >= config('constants.MIN_IMAGE_UPLOAD')) {
                $imgCount = 0;
                foreach ($images as $img) {
                    if(file_exists(config('constants.UPLOADFILE').DIRECTORY_SEPARATOR.$filePath.DIRECTORY_SEPARATOR.Session::getId().DIRECTORY_SEPARATOR.$img)) {
                        $imgCount++;
                    }
                }
                return ($imgCount >= config('constants.MIN_IMAGE_UPLOAD'));
            }
            return false;
        });
        
        // Set Message for defined rule for number of images required during insert
        Validator::replacer('minNoOfImages', function($message, $attribute, $rule, $parameters) {
            return str_replace($message, "Minimum ".config('constants.MIN_IMAGE_UPLOAD')." ".$attribute. " required.", $message);
        });
        
        Validator::extend('resolution',function($attribute, $value,$parameters,$validator)
        {
            $resX = explode("x", $parameters[0])[0];
            $resY = explode("x", $parameters[0])[1];
            $img = Image::make($value);
            return $img->height() == $resY  && $img->width() == $resX;
        });
        Validator::replacer('resolution', function($message, $attribute, $rule, $parameters) {
            return str_replace($message, "Image should be ".config('constants.BLOG_IMAGE_WIDTH')."x".config('constants.BLOG_IMAGE_HEIGHT'),$message);
        });
//        // Set custom validation rule for number of images required during update
//        Validator::extend('minNoOfImagesUpdate', function($attribute, $value, $parameters, $validator) {
//            $images = json_decode($value);
//            $filePath = "vehicle_images";
//            if(count($images) >= config('constants.MIN_IMAGE_UPLOAD')) {
//                $imgCount = 0;
//                foreach ($images as $img) {
//                    if(file_exists(config('constants.UPLOADFILE').DIRECTORY_SEPARATOR.$filePath.DIRECTORY_SEPARATOR.Request::segment(3).DIRECTORY_SEPARATOR.$img)) {
//                        $imgCount++;
//                    }
//                }                
//                return ($imgCount >= config('constants.MIN_IMAGE_UPLOAD'));
//            }
//            return false;
//        });
//        
//        // Set Message for defined rule for number of images required during update
//        Validator::replacer('minNoOfImagesUpdate', function($message, $attribute, $rule, $parameters) {
//            return str_replace($message, "Minimum 30 ".$attribute. " required.", $message);
//        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
