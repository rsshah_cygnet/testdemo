<?php

use App\Models\Session\Session;
use App\Models\Notification\Notification;
use Carbon\Carbon;
use App\Models\Frontusers\Frontusers;

/**
 * Global helpers file with misc functions
 *
 */

if (!function_exists('app_name')) {
	/**
	 * Helper to grab the application name
	 *
	 * @return mixed
	 */
	function app_name() {
		return config('app.name');
	}
}

if (!function_exists('access')) {
	/**
	 * Access (lol) the Access:: facade as a simple function
	 */
	function access() {
		return app('access');
	}
}

if (!function_exists('javascript')) {
	/**
	 * Access the javascript helper
	 */
	function javascript() {
		return app('JavaScript');
	}
}

if (!function_exists('gravatar')) {
	/**
	 * Access the gravatar helper
	 */
	function gravatar() {
		return app('gravatar');
	}
}

if (!function_exists('getFallbackLocale')) {
	/**
	 * Get the fallback locale
	 *
	 * @return \Illuminate\Foundation\Application|mixed
	 */
	function getFallbackLocale() {
		return config('app.fallback_locale');
	}
}

if (!function_exists('getLanguageBlock')) {

	/**
	 * Get the language block with a fallback
	 *
	 * @param $view
	 * @param array $data
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	function getLanguageBlock($view, $data = []) {
		$components = explode("lang", $view);
		$current = $components[0] . "lang." . app()->getLocale() . "." . $components[1];
		$fallback = $components[0] . "lang." . getFallbackLocale() . "." . $components[1];

		if (view()->exists($current)) {
			return view($current, $data);
		} else {
			return view($fallback, $data);
		}
	}
}

if (!function_exists('getLoggedInUser')) {
	/**
	 * Get Logged In User Id
	 *
	 */
	function getLoggedInUser() {
		return (access()->user()) ? access()->user()->id : null;
	}
}

if (!function_exists('getApplicationDateFormat')) {
	/**
	 * Get Application Date Format
	 *
	 */
	function getApplicationDateFormat() {
		return 'd/m/Y';
	}
}

if (!function_exists('getAuthUserRole')) {

	/**
	 * Get the role of authenticated user
	 *
	 * @return mixed
	 */
	function getAuthUserRole() {
		$result = \Illuminate\Support\Facades\DB::table('users')
			->select('assigned_roles.role_id')
			->join('assigned_roles', 'users.id', '=', 'assigned_roles.user_id')
			->where('users.id', auth()->user()->id)
			->first();

		return $result;
	}
}

if (!function_exists('getAuthDealership')) {

	/**
	 * This function is used to get dealership IDs as array by Authenticated user if exists
	 *
	 * @return array
	 */
	function getAuthDealership() {
		$results = \Illuminate\Support\Facades\DB::table('users')
			->select('user_dealerships.dealership_id')
			->join('user_dealerships', 'users.id', '=', 'user_dealerships.user_id')
			->where('users.id', auth()->user()->id)
			->get();

		$arrDealerships = array();
		if ($results) {
			foreach ($results as $result) {
				$arrDealerships[] = $result->dealership_id;
			}
		}

		return $arrDealerships;
	}
}

if (!function_exists('commonUserListCondition')) {

	/**
	 * This function is used to get user list with role wise filter
	 *
	 * @param $query
	 * @return mixed
	 */
	function commonUserListCondition($query) {

		$query = $query->select('users.*', 'assigned_roles.role_id');
		$query = $query->leftjoin('assigned_roles', 'users.id', '=', 'assigned_roles.user_id');

		$query = $query->where('assigned_roles.role_id', '>', getAuthUserRole()->role_id);
		$query = $query->groupBy('users.id');

		return $query;

	}
}

if (!function_exists('commonModelListCondition')) {

	/**
	 * This function will be updated - Demo
	 * This function is used to get model(any) list with role wise filter
	 *
	 * @param $model
	 * @param $query
	 * @return mixed
	 */
	function commonModelListCondition($model, $query) {
		$query = $query->join('assigned_roles', 'users.id', '=', 'assigned_roles.user_id');

		$query = $query->where('assigned_roles.role_id', '>', getAuthUserRole()->role_id);

		return $query;

	}
}

if (!function_exists('getRandomString')) {
	/**
	 * Helper to generate alphanumeric string
	 *
	 * @
	 * @return mixed
	 */
	function getRandomString($length) {
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$string = '';
		$max = strlen($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$string .= $characters[mt_rand(0, $max)];
		}

		return $string;

	}
}

if (!function_exists('getChildRolesFromAuthUser')) {

	/**
	 * This function is used to get child roles details with permissions from authenticated user
	 *
	 * @return mixed
	 */
	function getChildRolesFromAuthUser() {
		$result = \App\Models\Access\Role\Role::with('permissions')
			->where('roles.id', '>', getAuthUserRole()->role_id)
			->where('roles.deleted_at', null)
			->where('roles.status', 1)
			->orderBy('sort', 'asc')
			->get();

		return $result;
	}
}

if (!function_exists('checkDependencyInjection')) {

	/**
	 * This function is used to check dependency from main table
	 *
	 * @param $table -> Here we have to define main table name where we need to find record
	 * @param $id -> Here we have to define main table primary key
	 * @param $value -> Here we have to define main table primary key's value
	 * @return mixed
	 */
	function checkDependencyInjection($table, $id, $value) {
		$result = \Illuminate\Support\Facades\DB::table($table)
			->select('id')
			->where($id, $value)
		//->whereNull('deleted_at') // Don't remove: Maybe need for future use
			->first();

		if (is_null($result)) {
			return true;
		}

		return false;
	}
}

if (!function_exists('checkJoinDependencyInjection')) {

	/**
	 * This function is used to check dependency from join table
	 *
	 * @param $table -> Here we have to define main table name where we need to find record
	 * @param $joinTable -> Here we have to define join table name where we need to join with main($table) table
	 * @param $fkID -> Here we have to define foreign key for join table
	 * @param $id -> Here we have to define join table search key
	 * @param $value -> Here we have to define join table search key's value
	 * @return mixed
	 */
	function checkJoinDependencyInjection($table, $joinTable, $fkID, $id, $value) {
		$result = \Illuminate\Support\Facades\DB::table($table)
			->select('*')
			->join($joinTable, $table . '.id', '=', $joinTable . '.' . $fkID)
			->where($joinTable . '.' . $id, $value)
		//->whereNull($table.'.deleted_at') // Don't remove: Maybe need for future use
			->first();

		if (is_null($result)) {
			return true;
		}

		return false;
	}

}

if (!function_exists('getBrandModels')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getBrandModels($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\BrandModel\BrandModel::select('id', 'brand_model_name')->where($where);
		} else {
			$query = App\Models\BrandModel\BrandModel::select()->where($where);
		}

		return $query->get();
	}
}

if (!function_exists('getEducationalsystems')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getEducationalsystems($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\EducationalSystem\EducationalSystem::select('id', 'name')->where($where);
		} else {
			$query = App\Models\EducationalSystem\EducationalSystem::select()->where($where);
		}

		return $query->get();
	}
}

if (!function_exists('getTopics')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getTopics($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\Topic\Topic::select('id', 'topic_name')->where($where);
		} else {
			$query = App\Models\Topic\Topic::select()->where($where);
		}

		return $query->get();
	}
}

if (!function_exists('getGrades')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getGrades($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\Grade\Grade::select('id', 'grade_name', 'grade_type')->where($where);
		} else {
			$query = App\Models\Grade\Grade::select()->where($where);
		}

		return $query->get();
	}
}

if (!function_exists('getLevels')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getLevels($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\Levels\Levels::select('id', 'levels_name')->where($where);
		} else {
			$query = App\Models\Levels\Levels::select()->where($where);
		}

		return $query->get();
	}
}

if (!function_exists('getTopics')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getTopics($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\Topic\Topic::select('id', 'topic_name')->where($where);
		} else {
			$query = App\Models\Topic\Topic::select()->where($where);
		}

		return $query->get();
	}
}

if (!function_exists('getPrograms')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getPrograms($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\Program\Program::select('id', 'program_name')->where($where);
		} else {
			$query = App\Models\Program\Program::select()->where($where);
		}

		return $query->get();
	}
}

if (!function_exists('getSubjects')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getSubjects($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\Subject\Subject::select('id', 'subject_name')->where($where);
		} else {
			$query = App\Models\Subject\Subject::select()->where($where);
		}

		return $query->get();
	}
}

if (!function_exists('getState')) {
	/**
	 *
	 * @param Array $where
	 * @param Boolean $dropDown
	 * @return Object
	 */
	function getState($where = array(), $dropDown = false) {
		if ($dropDown) {
			$query = App\Models\State\State::with('Country')->where('country_id', $where['country_id'])->get()->pluck('state_name', 'id');
		} else {
			$query = App\Models\State\State::with('Country')->where('country_id', $where['country_id'])->get();
		}
		return $query;
	}
}

if (!function_exists('getCity')) {
	/*
		     * @param Array $where
		     * @param Boolean $dropDown
		     * @return Object
	*/
	function getCity($where = array(), $dropDown = false) {

		if ($dropDown) {
			$query = App\Models\City\City::with('State')->where('state_id', $where['state_id'])->get()->pluck('city_name', 'id');
		} else {
			$query = App\Models\City\City::with('State')->where('state_id', $where['state_id'])->get();
		}
		return $query;
	}
}

if (!function_exists('dirToArray')) {

	function dirToArray($dir, $recursive = true) {

		$result = array();

		$cdir = scandir($dir);
		foreach ($cdir as $key => $value) {
			if (!in_array($value, array(".", ".."))) {
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value) && $recursive) {
					$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
				} else {
					$result[] = $value;
				}
			}
		}

		return $result;
	}
}

if (!function_exists('getCurrentDomain')) {

	function getCurrentDomain() {

		$domain = $_SERVER['SERVER_NAME'];
		//temporary
		$domain = 'www.test-cherry.co.za';
		$domainObj = App\Models\Domain\Domain::where('domain', $domain)->first();

		if (!$domainObj) {
			throw new Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
		}
		return $domainObj;
	}
}

if (!function_exists('recursiveRemoveDirectory')) {
	function recursiveRemoveDirectory($directory) {
		if (is_dir($directory)) {
			foreach (glob("{$directory}/*") as $file) {
				if (is_dir($file)) {
					recursiveRemoveDirectory($file);
				} else {
					unlink($file);
				}
			}
			rmdir($directory);
		}
	}
}

//Getting the domain id of the current working domain
if (!function_exists('getDomainVerified')) {

	function getDomainVerified() {
		$domain = \DB::table('domains')->where('website_url', 'LIKE', '%' . url('/') . '%')->first();

		if ($domain) {
			return $domain->id;
		} else {
			return 0;
		}

	}
}

//Getting Days value from PriceToGo Settings table
if (!function_exists('getDaysFromPriceToGo')) {

	function getDaysFromPriceToGo() {
		$days = DB::table('price_to_go_settings')->where('status', 'Active')->first();
		if (!is_null($days)) {
			return $days->days;
		}

		return 0;
	}
}

function getCurriculumHierarchy($subject_id = '', $topic_id = '', $heirarchy_flag = false) {
	if ($topic_id != '') {
		$result = DB::select(DB::raw("(
            SELECT topic.id,topic.topic_name,curriculum.curriculum_name,levels.levels_level,grade.grade_level,grade.grade_type,program.program_level,eductional_systems.eductional_systems_level,program.program_name,grade.grade_name,levels.levels_name,eductional_systems.name,subject.subject_level,subject.subject_name
            FROM topic
            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS levels_level,id,levels_name FROM levels
            )
            `levels` ON (topic.level_id = levels.id)

            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS grade_level,id,grade_name,grade_type FROM grade
            )
            `grade` ON (topic.grade_id = grade.id)

            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS program_level,id,program_name FROM program
            )
            `program` ON (topic.program_id = program.id)


            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `levels_id` IS NULL OR  `levels_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END
            )AS eductional_systems_level,id,name FROM eductional_systems
            )
            `eductional_systems` ON (topic.eductional_systems_id = eductional_systems.id)


            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS subject_level,id,subject_name FROM subject
            )
            `subject` ON (topic.subject_id = subject.id)

            LEFT JOIN `curriculum` ON (topic.curriculum_id = curriculum.id)

            WHERE topic.id = " . $topic_id . "
            )"));

	} else if ($subject_id != '') {

		$result = DB::select(DB::raw("(

            SELECT subject.id,subject.subject_name,curriculum.curriculum_name,levels.levels_level,grade.grade_level,program.program_level,eductional_systems.eductional_systems_level,program.program_name,grade.grade_name,grade.grade_type,levels.levels_name,eductional_systems.name
            FROM subject
            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS levels_level,id,levels_name FROM levels
            )
            `levels` ON (subject.level_id = levels.id)

            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS grade_level,id,grade_name,grade_type FROM grade
            )
            `grade` ON (subject.grade_id = grade.id)

            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS program_level,id,program_name FROM program
            )
            `program` ON (subject.program_id = program.id)

            LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `levels_id` IS NULL OR  `levels_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END
            )AS eductional_systems_level,id,name FROM eductional_systems
            )
            `eductional_systems` ON (subject.eductional_systems_id = eductional_systems.id)

            LEFT JOIN `curriculum` ON (subject.curriculum_id = curriculum.id)

            WHERE subject.id = " . $subject_id . "
            )"));
	}

	if (isset($result[0]) && !empty($result[0])) {
		$data = (object_to_array($result[0]));
	} else {
		return 'N/A';
	}

	if (isset($data['grade_type']) && $data['grade_type'] == 0) {
		$grade_type_string = " ( lower ) ";
	} elseif (isset($data['grade_type']) && $data['grade_type'] == 1) {
		$grade_type_string = " ( Higher ) ";
	} else {
		$grade_type_string = "";
	}

	//echo "<pre>";print_r($data);die;

	$heirarchy[$data['levels_level']] = 'levels';
	$heirarchy[$data['program_level']] = 'program';
	$heirarchy[$data['eductional_systems_level']] = 'eductional_systems';
	$heirarchy[$data['grade_level']] = 'grade';
	if ($subject_id == '') {
		$heirarchy[$data['subject_level']] = 'subject';
	}

	ksort($heirarchy);

	$final_heirachy[0]['reference'] = 'curriculum';
	$final_heirachy[0]['name'] = $data['curriculum_name'];

	foreach ($heirarchy as $key => $value) {
		if ($value == 'eductional_systems') {
			$field_name = $data["name"];
		} else {
			$field_name = $data[$value . "_name"];
		}

		if (($value != '') && is_int($data[$value . "_level"])) {
			$count = $key + 1;

			$final_heirachy[$count]['reference'] = $value;

			if ($value == 'eductional_systems') {
				$final_heirachy[$count]['name'] = $data["name"];
			} 
			elseif ($value == 'grade') {
				$final_heirachy[$count]['name'] = $data[$value . "_name"]. $grade_type_string;
			}
			else {
				$final_heirachy[$count]['name'] = $data[$value . "_name"];
			}

		}

	}
	//echo "<pre>";print_r($final_heirachy);die;

	if ($subject_id != '') {
		$final_heirachy[$count + 1]['reference'] = 'subject';
		$final_heirachy[$count + 1]['name'] = $data['subject_name'];

	} else {

		$final_heirachy[$count + 1]['reference'] = 'topic';
		$final_heirachy[$count + 1]['name'] = $data['topic_name'];

	}
//echo "<pre>";print_r($final_heirachy);die;

	$string = '';
	$counter = 1;
//make heirarchy
	foreach ($final_heirachy as $key => $value) {
		$total_number = count($final_heirachy);

		if ($counter == ($total_number)) {
			$delimeter = "";
		} else {
			$delimeter = " | ";
		}

		if ($value['reference'] == 'grade') {
			$string .= $value['name'] . $delimeter;
		} else {
			$string .= $value['name'] . $delimeter;
		}

		$counter++;
	}

	if ($heirarchy_flag) {
		return $string;
	} else {
		return $final_heirachy;
	}
}

function object_to_array($data) {
	if (is_array($data) || is_object($data)) {
		$result = array();
		foreach ($data as $key => $value) {
			$result[$key] = object_to_array($value);
		}
		return $result;
	}
	return $data;
}

if (!function_exists('getReviewCountByUserId')) {
	/**
	 * Helper to grab the application name
	 *
	 * @return mixed
	 */
	function getReviewCountByUserId($userId, $rate = 0) {
		if (!empty($rate)) {
			return \DB::table('tutor_ratings')->where('tutor_id', $userId)->where('rating', $rate)->count();
		}
		return \DB::table('tutor_ratings')->where('tutor_id', $userId)->count();
	}
}

function randomString($length = 4) {
	$str = "";
	$characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

//count penalty for tutee for cancelled sessions
if (!function_exists('countSessionCancelledPenalty')) {

	function countSessionCancelledPenalty($hours = "") {
		$result = DB::select(DB::raw("(SELECT penalty_percentage FROM `session_cancelled_penalty` WHERE `type` = '2' AND `status` = '1' AND `within` >= " . $hours . " order by penalty_percentage asc )"));
		//dd($result);
		$penalty_percentage = "";
		$final_data = (object_to_array(end($result)));
		if (count($result) > 0) {
			if (isset($final_data['penalty_percentage']) && $final_data['penalty_percentage'] != "" && $final_data['penalty_percentage'] != 0) {
				$penalty_percentage = $final_data['penalty_percentage'];
				return $penalty_percentage;
			}
		} else {
			if (isset($result[0]) && !empty($result[0])) {
				$data = (object_to_array($result[0]));
				$penalty_percentage = $data['penalty_percentage'];
				return $penalty_percentage;
			}
		}

		if ($penalty_percentage == "" || $penalty_percentage == 0) {
			$result = DB::select(DB::raw("(SELECT penalty_percentage FROM `session_cancelled_penalty` WHERE `type` = '1' AND `status` = '1' AND `from` <= " . $hours . " AND `to` >= " . $hours . " )"));

			$penalty_percentage = "";
			$final_data = (object_to_array(end($result)));
			if (count($result) > 0) {
				if (isset($final_data['penalty_percentage']) && $final_data['penalty_percentage'] != "" && $final_data['penalty_percentage'] != 0) {
					$penalty_percentage = $final_data['penalty_percentage'];
					//dd($penalty_percentage);
					return $penalty_percentage;
				}
			} else {
				if (isset($result[0]) && !empty($result[0])) {
					$data = (object_to_array($result[0]));
					$penalty_percentage = $data['penalty_percentage'];
					return $penalty_percentage;
				}
			}

		}

	}
}

//count punctuality_ratings_for_cancelled_session
if (!function_exists('countPunctualityRatingsForCancelledSession')) {

	function countPunctualityRatingsForCancelledSession($hours = "") {
		$result = DB::select(DB::raw("(SELECT rating FROM `punctuality_ratings_for_cancelled_session` WHERE `type` = '2' AND `status` = '1' AND `within` >= " . $hours . " order by within desc )"));
		 
		$rating = "";
		$final_data = (object_to_array(end($result)));
		if (count($result) > 0) {
			if (isset($final_data['rating']) && $final_data['rating'] != "" && $final_data['rating'] != 0) {
				$rating = $final_data['rating'];
				return $rating;
			}
		} else {
			if (isset($result[0]) && !empty($result[0])) {
				$data = (object_to_array($result[0]));
				$rating = $data['rating'];
				return $rating;
			}
		}

		if ($rating == "" || $rating == 0) {
			$result = DB::select(DB::raw("(SELECT rating FROM `punctuality_ratings_for_cancelled_session` WHERE `type` = '1' AND `status` = '1' AND `from` <= " . $hours . " AND `to` >= " . $hours . " )"));

			$rating = "";
			$final_data = (object_to_array(end($result)));
			if (count($result) > 0) {
				if (isset($final_data['rating']) && $final_data['rating'] != "" && $final_data['rating'] != 0) {
					$rating = $final_data['rating'];
					return $rating;
				}
			} else {
				if (isset($result[0]) && !empty($result[0])) {
					$data = (object_to_array($result[0]));
					$rating = $data['rating'];
					return $rating;
				}
			}

		}

	}
}

//count punctuality_ratings_for_successful_session
if (!function_exists('countPunctualityRatingsForSuccessfulSession')) {

	function countPunctualityRatingsForSuccessfulSession($mins = "") {

		$result = DB::select(DB::raw("(SELECT rating FROM `punctuality_ratings` WHERE `status` = '1' AND `from` <= " . $mins . " AND `to` >= " . $mins . " )"));
		if (empty($result)) {
			$result_from = DB::select(DB::raw("(SELECT `from` FROM `punctuality_ratings` WHERE `status` = '1' AND `from` >= " . $mins . " order by  `from` asc)"));
			if (!empty($result_from)) {
				$from = (object_to_array($result_from[0]));
				if ($from['from'] > $mins) {
					return 10;
				}

			} else {
				$result_to = DB::select(DB::raw("(SELECT `to` FROM `punctuality_ratings` WHERE `status` = '1' AND `to` <= " . $mins . " order by  `from` desc)"));
				if (!empty($result_to)) {
					$to = (object_to_array($result_to[0]));
					if ($to['to'] < $mins) {
						return 0;
					}
				}

			}
		}
		$rating = "";
		$final_data = (object_to_array(end($result)));
		if (count($result) > 0) {
			if (isset($final_data['rating']) && $final_data['rating'] != "" && $final_data['rating'] != 0) {
				$rating = $final_data['rating'];
				return $rating;
			}
		} else {
			if (isset($result[0]) && !empty($result[0])) {
				$data = (object_to_array($result[0]));
				$rating = $data['rating'];
				return $rating;
			}
		}

	}
}

function getCurriculumSteps($step = "0", $table_name = "false", $curriculum_id = "", $level_id = "", $program_id = "", $eductional_systems_id = "", $subject_id = "", $grade_id = "", $topic_id = "") {

	$where = "";
	if ($step > 0) {
		$where = "WHERE";
	}
	$and = ' AND';
	$steps_all = $step + 1;
	if ($level_id != "" && $level_id != 0) {
		if ($steps_all != 0) {
			$where .= "level_id = " . $level_id . " AND";
			$steps_all--;
		} else {
			$where .= "level_id = " . $level_id . "";
		}
	}

	if ($program_id != "" && $program_id != 0) {

		if ($steps_all != 0) {
			$where .= "program_id = " . $program_id . " AND";
			$steps_all--;
		} else {
			$where .= "program_id = " . $program_id . "";
		}
	}

	if ($eductional_systems_id != "" && $eductional_systems_id != 0) {

		if ($steps_all != 0) {
			$where .= "eductional_systems_id = " . $eductional_systems_id . " AND";
			$steps_all--;
		} else {
			$where .= "eductional_systems_id = " . $eductional_systems_id . "";
		}
	}
	if ($subject_id != "" && $subject_id != 0) {

		if ($steps_all != 0) {
			$where .= "subject_id = " . $subject_id . " AND";
			$steps_all--;
		} else {
			$where .= "subject_id = " . $subject_id . "";
		}
	}
	if ($grade_id != "" && $grade_id != 0) {

		if ($steps_all != 0) {
			$where .= "grade_id = " . $grade_id . " AND";
			$steps_all--;
		} else {
			$where .= "grade_id = " . $grade_id . "";
		}
	}
	if ($topic_id != "" && $topic_id != 0) {

		if ($steps_all != 0) {
			$where .= "topic_id = " . $topic_id . " AND";
			$steps_all--;
		} else {
			$where .= "topic_id = " . $topic_id . "";
		}
	}

	$left_join_education = $left_join_levels = $left_join_grade = $left_join_program = $left_join_subject = $left_join_topic = "";

	$left_join_education = "LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `levels_id` IS NULL OR  `levels_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END
            )AS eductional_systems_level,id,name,curriculum_id FROM eductional_systems
            )
            `eductional_systems` ON (curriculum.id = eductional_systems.curriculum_id)";

	$left_join_levels = "LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS levels_level,id,levels_name,curriculum_id FROM levels
            )
            `levels` ON (curriculum.id = levels.curriculum_id)";

	$left_join_grade = "LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS grade_level,id,grade_name,curriculum_id FROM grade
            )
            `grade` ON (curriculum.id = grade.curriculum_id)";

	$left_join_program = " LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS program_level,id,program_name,curriculum_id FROM program
            )
            `program` ON (curriculum.id = program.curriculum_id)";

	$left_join_subject = "LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS subject_level,id,subject_name,curriculum_id FROM subject
            )
            `subject` ON (curriculum.id = subject.curriculum_id)";

	$left_join_topic = "LEFT JOIN
            (
            SELECT
            (
            CASE WHEN `level_id` IS NULL OR  `level_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN `grade_id` IS NULL OR  `grade_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `program_id` IS NULL OR `program_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `subject_id` IS NULL OR `subject_id` = 0 THEN 0 ELSE 1 END +
            CASE WHEN  `eductional_systems_id` IS NULL OR `eductional_systems_id` = 0 THEN 0 ELSE 1 END
            )AS topic_level,id,topic_name,curriculum_id FROM topic
            )
            `topic` ON (curriculum.id = topic.curriculum_id)";

	$select = "SELECT curriculum.id as curriculum_id";

	if ($level_id != "" && $level_id != 0) {
		$left_join_levels = '';

	}

	if ($program_id != "" && $program_id != 0) {
		$left_join_program = '';

	}

	if ($eductional_systems_id != "" && $eductional_systems_id != 0) {
		$left_join_education = '';

	}
	if ($subject_id != "" && $subject_id != 0) {
		$left_join_subject = '';

	}
	if ($grade_id != "" && $grade_id != 0) {
		$left_join_grade = '';

	}
	if ($topic_id != "" && $topic_id != 0) {
		$left_join_topic = '';

	}

	if ($left_join_education != "") {
		$select .= ",eductional_systems.eductional_systems_level";
	}

	if ($left_join_levels != "") {
		$select .= ",levels.levels_level";
	}
	if ($left_join_grade != "") {
		$select .= ",grade.grade_level";
	}
	if ($left_join_program != "") {
		$select .= ",program.program_level";
	}

	if ($left_join_subject != "") {
		$select .= ",subject.subject_level";
	}
	if ($left_join_topic != "") {
		$select .= ",topic.topic_level";
	}

	$final_join_string = "" . $left_join_education . "

            " . $left_join_levels . "

            " . $left_join_grade . "

            " . $left_join_program . "

            " . $left_join_subject . "

            " . $left_join_topic . "";

	$final = DB::select(DB::raw("(" . $select . "
            FROM curriculum

            " . $final_join_string . "

            WHERE curriculum.id = " . $curriculum_id . ")"));

	if ($table_name) {
		$final = (object_to_array($final['0'])); //dd($final);
		if (isset($final['levels_level']) && $final['levels_level'] == $step) {
			return "levels";
		} else if (isset($final['grade_level']) && $final['grade_level'] == $step) {
			return "grade";
		} else if (isset($final['program_level']) && $final['program_level'] == $step) {
			return "program";
		} else if (isset($final['eductional_systems_level']) && $final['eductional_systems_level'] == $step) {
			return "eductional_systems";
		} else if (isset($final['subject_level']) && $final['subject_level'] == $step) {
			return "subject";
		} else if (isset($final['topic_level']) && $final['topic_level'] == $step) {
			return "topic";
		}
	} else {
		return $final;
	}

}

function getRecordsByTable($table = "", $curriculum_id = "", $level_id = "", $program_id = "", $eductional_systems_id = "", $subject_id = "", $grade_id = "", $topic_id = "") {

	$where = "";
	$select = "";
	if ($table == "levels") {
		$select = "id,levels_name";
	}
	if ($table == "program") {
		$select = "id,program_name";
	}
	if ($table == "eductional_systems") {
		$select = "id,name";
	}
	if ($table == "subject") {
		$select = "id,subject_name";
	}
	if ($table == "grade") {
		$select = "id,grade_name,grade_type";
	}
	if ($table == "topic") {
		$select = "id,topic_name";
	}

	if ($level_id != "" && $level_id != 0) {
		if ($table != "levels") {
			if ($table == "eductional_systems") {
				$where .= " AND levels_id = " . $level_id . "";
			} else {
				$where .= " AND level_id = " . $level_id . "";
			}

		}

	} else {
		if ($table != "levels") {
			if ($table == "eductional_systems") {
				$where .= " AND (`levels_id` IS NULL OR `levels_id` = 0)";
			} else {
				$where .= " AND (`level_id` IS NULL OR `level_id` = 0)";
			}

		}

	}

	if ($program_id != "" && $program_id != 0) {
		if ($table != "program") {
			$where .= " AND program_id = " . $program_id . "";
		}

	} else {
		if ($table != "program") {
			$where .= " AND (`program_id` IS NULL OR `program_id` = 0)";
		}

	}

	if ($eductional_systems_id != "" && $eductional_systems_id != 0) {
		if ($table != "eductional_systems") {
			$where .= " AND eductional_systems_id = " . $eductional_systems_id . "";
		}

	} else {
		if ($table != "eductional_systems") {
			$where .= " AND (`eductional_systems_id` IS NULL OR `eductional_systems_id` = 0)";
		}

	}

	if ($subject_id != "" && $subject_id != 0) {
		if ($table != "subject") {
			$where .= " AND subject_id = " . $subject_id . "";
		}

	} else {
		if ($table != "subject") {
			$where .= " AND (`subject_id` IS NULL OR `subject_id` = 0)";
		}

	}

	if ($grade_id != "" && $grade_id != 0) {
		if ($table != "grade") {
			$where .= " AND grade_id = " . $grade_id . "";
		}

	} else {

		if ($table != "grade") {
			$where .= " AND (`grade_id` IS NULL OR `grade_id` = 0)";
		}
	}

	if ($table == 'topic') {
		if ($topic_id != "" && $topic_id != 0) {
			if ($table != "topic") {
				$where .= " AND topic_id = " . $topic_id . "";
			}

		} else {
			if ($table != "topic") {
				$where .= " AND (`topic_id` IS NULL OR `topic_id` = 0)";
			}

		}
	}
	$data = DB::select(DB::raw("(SELECT " . $select . " FROM " . $table . " WHERE deleted_at IS null AND curriculum_id = " . $curriculum_id . " " . $where . ")
    "));

	return $data;
	//dd($data);
}

function getRecordsByTableForReserveSession($tutor_id,$table = "", $curriculum_id = "", $level_id = "", $program_id = "", $eductional_systems_id = "", $subject_id = "", $grade_id = "", $topic_id = "") {

	$where = "";
	//$select = "*";
	
	$select = "";
	if ($table == "levels") {
		$select = "DISTINCT(levels.id),levels.levels_name";
	}
	if ($table == "program") {
		$select = "DISTINCT(program.id),program.program_name";
	}
	if ($table == "eductional_systems") {
		$select = "DISTINCT(eductional_systems.id),eductional_systems.name";
	}
	if ($table == "subject") {
		$select = "DISTINCT(subject.id),subject.subject_name";
	}
	if ($table == "grade") {
		$select = "DISTINCT(grade.id),grade.grade_name,grade.grade_type";
	}
	if ($table == "topic") {
		$select = "DISTINCT(topic.id),topic.topic_name";
	}

	if ($level_id != "" && $level_id != 0) {
		$where .= " AND tutor_details.level_id = " . $level_id . "";
	}

	if ($program_id != "" && $program_id != 0) {
			$where .= " AND tutor_details.program_id = " . $program_id . "";
	}
	if ($eductional_systems_id != "" && $eductional_systems_id != 0) {
	
			$where .= " AND tutor_details.education_system_id = " . $eductional_systems_id . "";
	}
	if ($subject_id != "" && $subject_id != 0) {
			$where .= " AND tutor_details.subject_id = " . $subject_id . "";
	}
	if ($grade_id != "" && $grade_id != 0) {
			$where .= " AND tutor_details.grade_id = " . $grade_id . "";
	}
	if ($topic_id != "" && $topic_id != 0) {
		$where .= " AND tutor_details.topic_id = " . $topic_id . "";
	}
	

	$data = DB::select(DB::raw("(SELECT " . $select . " FROM tutor_details 
		LEFT JOIN levels on levels.id = tutor_details.level_id
		LEFT JOIN eductional_systems on eductional_systems.id = tutor_details.education_system_id
		LEFT JOIN program on program.id = tutor_details.program_id
		LEFT JOIN grade on grade.id = tutor_details.grade_id
		LEFT JOIN subject on subject.id = tutor_details.subject_id
		LEFT JOIN topic on topic.id = tutor_details.topic_id
		WHERE tutor_details.passed_status = '1' AND tutor_details.deleted_at IS null AND tutor_details.appeared = '1' AND tutor_details.is_completed = '1' AND tutor_details.front_user_id = " . $tutor_id . " AND tutor_details.curriculum_id = " . $curriculum_id . " " . $where . ")
    "));

	return $data;
	
}

function check_front_login() {
	$user_id = \Session::get('front_user_id');
	if ($user_id != "" && $user_id != 0) {
		return true;
	} else {
		return false;
	}
}

function getOnlyDate($dateTime = "") {

	return date("d-m-Y", strtotime($dateTime));

}

function getOnlyTime($dateTime = "") {

	return date("h:i a", strtotime($dateTime));
}

function ConverTimeIn24Hour($time = "") {
	return date("H:i", strtotime($time));
}

function convertDateInSqlFormat($date = "") {
	return date("Y-m-d", strtotime($date));
}

function ConverTimeZoneForDB($time = "", $format = "") //time must be in 24 hours
{
	$defaultzone = config('app.timezone');
	$userzone = \Session::get('front_user_timezone_name');

	$cookie = isset($_COOKIE["front_user_timezone_name"])?$_COOKIE["front_user_timezone_name"]:"";
	if (!empty($userzone) && $userzone != "") {
		$userzone = $userzone;
	} else if ($cookie != "") {
		$userzone = $cookie;
	} else {
		$userzone = $defaultzone;
	}

	$date = new DateTime($time, new DateTimeZone($userzone)); //defaultzone of user
	$date->setTimezone(new DateTimeZone($defaultzone));
	$result = $date->format($format);
	return $result;
}

function ConverTimeZoneForUser($time = "", $format = "", $timezone="") //time must be in 24 hours

{
	$defaultzone = config('app.timezone');
	$userzone = \Session::get('front_user_timezone_name');
	$cookie = isset($_COOKIE["front_user_timezone_name"])?$_COOKIE["front_user_timezone_name"]:"";
	if (!empty($userzone) && $userzone != "") {
		$userzone = $userzone;
	} else if ($cookie != "") {
		$userzone = $cookie;
	} else {
		$userzone = $defaultzone;
	}
	if(!empty($timezone)){
		$userzone = $timezone;	
	}

	$date = new DateTime($time, new DateTimeZone($defaultzone)); //defaultzone of DB
	$date->setTimezone(new DateTimeZone($userzone));
	$result = $date->format($format);
	return $result;
}

function getTutorPunctualityRatings($tutor_id = "") {
	if (isset($tutor_id) && $tutor_id != "" && $tutor_id != 0) {
		$tutor_id = $tutor_id;
	} else {
		$tutor_id = $sessionData['front_user_id'];
	}

	$punctuality_rating = DB::table('session')
		->where('tutor_id', $tutor_id)
		->avg('punctuality_ratings');

	return number_format((float) $punctuality_rating, 1, '.', '');

}

function getTutorAvgRatings($tutor_id = "") {
	if (isset($tutor_id) && $tutor_id != "" && $tutor_id != 0) {
		$tutor_id = $tutor_id;
	} else {
		$tutor_id = $sessionData['front_user_id'];
	}

	$rating = DB::table('tutor_ratings')
		->where('tutor_id', $tutor_id)
		->avg('rating');

	return number_format((float) $rating, 0, '.', '');

}

function getTotalTutorRatings($tutor_id = "") {
	if (isset($tutor_id) && $tutor_id != "" && $tutor_id != 0) {
		$tutor_id = $tutor_id;
	} else {
		$tutor_id = $sessionData['front_user_id'];
	}

	$total_tutor_ratings = \App\Models\TutorRatings\TutorRatings::where('tutor_id', $tutor_id)->count();

	return $total_tutor_ratings;

}

function getCurroculumList() {
	$curriculum = \App\Models\Curriculum\Curriculum::getCurriculumList();
	return $curriculum;
}

function checkTutorTuteeJoinSession($tutor_id = '', $tutee_id = '') {

	$date = new DateTime;
	$date->modify('+70 minutes');
	$formatted_end_date = $date->format('Y-m-d H:i:s');

	$crnt_date = new DateTime;
	$current_date = $crnt_date->format('Y-m-d H:i:s');

	if ($tutor_id != '') {
		$update_data = DB::statement("UPDATE `session` SET `status` = '7' where `end_date_time` <= '" . $current_date . "' AND `status` = '2' AND `tutor_id` = " . $tutor_id); //7 mns none of join, 2 mns upcoming
	} else if ($tutee_id != '') {

		$update_data = DB::statement("UPDATE `session` SET `status` = '7' where `end_date_time` <= '" . $current_date . "' AND `status` = '2' AND `tutee_id` = " . $tutee_id); //7 mns none of join, 2 mns upcoming
	}

	$data = Session::Select('id');
	if ($tutor_id != '') {
		$data->where('tutor_id', $tutor_id);
	} else if ($tutee_id != '') {
		$data->where('tutee_id', $tutee_id);
	}
	$result_data = $data->where('end_date_time', '<=', $formatted_end_date)
		->where('end_date_time', '>=', $current_date)
		->get()->toArray();

	echo json_encode($result_data);
}

function checkTuteeJoinSession($tutee_id) {

	$date = new DateTime;
	$date->modify('+70 minutes');
	$formatted_end_date = $date->format('Y-m-d H:i:s');

	$crnt_date = new DateTime;
	$current_date = $crnt_date->format('Y-m-d H:i:s');

	$update_data = DB::statement("UPDATE `session` SET `status` = '7' where `end_date_time` <= '" . $current_date . "' AND `status` = '2' AND `tutor_id` = " . $tutor_id); //7 mns none of join, 2 mns upcoming

	$data = Session::Select('id')
		->where('tutee_id', $tutee_id)
		->where('end_date_time', '<=', $formatted_end_date)
		->where('end_date_time', '>=', $current_date)
		->get()->toArray();

	echo json_encode($data);
}

function send($template_name, $data_array = array(), $to_array = array(), $cc_array = array(), $bcc_array = array(), $subject = "", $from = array()) {

	if (empty($from)) {
		$from = Config::get('mail.from_mail');
	}

	$email_data = \App\Models\Emailtemplate\Emailtemplate::where('template_name', $template_name)->where('status', '1')->get()->toArray();

	if (!empty($email_data)) {
		if ($subject == "") {
			$subject = $email_data['0']['subject'];
		}

		$body_html = $email_data['0']['body'];
		$body = str_replace(array_keys($data_array), array_values($data_array), $body_html);

		Mail::send('frontend.emails.send', ['content' => $body], function ($message) use ($to_array, $cc_array, $bcc_array, $subject, $from) {

			$message->from($from);
			$message->to($to_array);
			$message->cc($cc_array);
			$message->bcc($bcc_array);
			$message->subject($subject);

		});
	}
}

function getCityBasedOncountry($country_id) {
	$state = App\Models\State\State::where('country_id', $country_id)->get()->pluck('id');
	$city = App\Models\City\City::whereIn('state_id', $state)->get();
	return $city;
}

function getLanguageBasedOnLanguageIdString($language_id_string){
	if($language_id_string == ""){
		return "";
	}
	$lang_ids = explode(',', $language_id_string);
	$language_name = [];
        if(!empty($lang_ids)){
            foreach ($lang_ids as $key => $value) {
               $language = App\Models\Language\Language::where('id',$value)->where('status','1')->get()->toArray();
               if(!empty($language)){
               	 $language_name[$key] = $language['0']['name'];
               }
               
            }
        }
        $language_name_string = implode(', ', $language_name);
        return $language_name_string;
}

function createDateRangeArray($strDateFrom,$strDateTo)
{
    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    
    return $aryRange;

 }

 function sendNotificationToTutorForJoinSession($id){
 	    /*Notification:Tutee joins a session */
 	    	 $id = decrypt($id);
            $session_data = Session::Select('tutor_session_url','tutor_id','tutee_id','subject_id','topic_id')
            ->where('id', $id)
            ->first()->toArray();
            $sessionData = [
                    "front_user_first_name" => \Session::get('front_user_first_name'),
                    "front_user_last_name" => \Session::get('front_user_last_name'),
                ];
            $name = $sessionData['front_user_first_name']." ".$sessionData['front_user_last_name'];
            $message = "<a href='/tutor-dashboard'><b>".$name."</b> waiting for you to join a session.please join a session as soon as possible.</a>";
            $Notification = Notification::insert([
                'from_user_id' => $session_data['tutee_id'],
                'to_user_id' => $session_data['tutor_id'],
                'notification_type_id' => 9,
                'is_read' => 0,
                'is_read_admin' => 0,
                'is_for_admin' => 0,
                'created_at' => Carbon::now()->toDateTimestring(),
                'message' => $message,
            ]);

            $topic_id = $session_data['topic_id'];
            $subject_id = $session_data['subject_id'];

            if (isset($topic_id) && $topic_id != 0 && $topic_id != "") {
                $Hierarchy  =  getCurriculumHierarchy('', $topic_id, true); //for topic
            } else {
                $Hierarchy  = getCurriculumHierarchy($subject_id, '', true); //for subject
            }

            //for admin
            $message = "<b>".$name."</b> join session for the curriculum <b>".$Hierarchy."</b>";
            $Notification = Notification::insert([
                'from_user_id' => $session_data['tutee_id'],
                'to_user_id' => $session_data['tutor_id'],
                'notification_type_id' => 9,
                'is_read' => 1,
                'is_read_admin' => 0,
                'is_for_admin' => 1,
                'created_at' => Carbon::now()->toDateTimestring(),
                'message' => $message,
            ]);
            /*Notification end*/   
 }

 function getTimezoneName($user_id) {
 	$timezone_id = App\Models\Frontusers\Frontusers::where('id',$user_id)->first();
 	$timezone = App\Models\Timezone\Timezone::where('id', $timezone_id->timezone_id)->first();
 	return $timezone->timezone_name;
}

function readNotifications($limit){
	$userId = \Session::get('front_user_id');
	DB::table('notifications')
        ->where('to_user_id', $userId)
        ->where('is_read', 0)
        ->where('is_for_admin', 0)
        ->orderBy('id', 'desc')
        ->limit($limit)
        ->update(array('is_read' => 1));
    die();
}

function getUserData($user_id){
	$data = Frontusers::where('id',$user_id)
			->first();
	return $data;		
}

/**
 * check test is passed or not with whole hierarchy
 * @param Test id $id
 */
 function checkPassedTest($id)
 {
 	$loggedInUserId     =   \Session::get('front_user_id');
   	$userDetail = \DB::table('tutor_details')->where('id', $id)->first();
 	
    $userId = $userDetail->front_user_id;
    $curriculum_id          = $userDetail->curriculum_id;
    $education_system_id    = $userDetail->education_system_id;
    $level_id               = $userDetail->level_id;
    $program_id             = $userDetail->program_id;
    $grade_id               = $userDetail->grade_id;
    $subject_id             = $userDetail->subject_id;
    $topic_id               = $userDetail->topic_id;
    $checkPassedTest        = \DB::table('tutor_details')->where('front_user_id', $loggedInUserId);
    if(!empty($curriculum_id)){
        $checkPassedTest = $checkPassedTest->where('curriculum_id', $curriculum_id);
    }
    if(!empty($education_system_id)){
        $checkPassedTest = $checkPassedTest->where('education_system_id', $education_system_id);
    }
    if(!empty($level_id)){
        $checkPassedTest = $checkPassedTest->where('level_id', $level_id);
    }
    if(!empty($program_id)){
        $checkPassedTest = $checkPassedTest->where('program_id', $program_id);
    }
    if(!empty($grade_id)){
        $checkPassedTest = $checkPassedTest->where('grade_id', $grade_id);
    }
    if(!empty($subject_id)){
        $checkPassedTest = $checkPassedTest->where('subject_id', $subject_id);
    }
    if(!empty($topic_id)){
        $checkPassedTest = $checkPassedTest->where('topic_id', $topic_id);
    }

    $check = $checkPassedTest->where('front_user_id', $loggedInUserId)
                             ->where('appeared', 1)
                             ->where('is_completed', 1)
                             ->where('passed_status', 1)
                             ->first();
    return $check;
 }


/* function sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject, $description, $location)
{
    $mailsent = Mail::raw($to_address, $subject, $message, $headers);

	$filename = 'uploads/user/calendar-event-outlook-20170605.ics';
	$mailsent =	Mail::send('frontend.emails.cal', array(), function($message) use($filename,$headers)
		{
		    $message->from('jon@example', 'sudhir');
		    $message->to('cygnet.svirpara@outlook.com')->subject('event');
		    $swiftMessage = $message->getSwiftMessage();

			$headers = $swiftMessage->getHeaders();
			$headers->addTextHeader($headers);
		  
		    $message->attach($filename, array('mime' => "text/calendar"));
		});	

    return ($mailsent)?(true):(false);
}*/