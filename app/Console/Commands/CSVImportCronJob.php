<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use DB;

class CSVImportCronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importing CSV file uploaded.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target_dir = public_path(config('constants.UPLOADFILE')). "/csv_files/".date("d-m-Y")."/";
        
        $target_file = $target_dir . "updated-file-" . date("d-m-Y") . ".csv";
        
        $tmp_vehicle_stock_table = "vehicle_stocks_csv_tmp";
    
        $cron_result_table = "cron_result_table";

        if (File::exists($target_file))
        {
            $sp_result = DB::statement('CALL `create_vehicle_stock_csv_import_logs_table`(@sp_result);');
            
            if(!$sp_result)
            {
                echo 'Error creating table';
            }

            $sp_result = DB::statement('CALL `create_vehicle_stocks_csv_tmp_table`(@sp_result);');
            
            if(!$sp_result)
            {
                echo 'Error creating table';
            }
            
            $query = "LOAD DATA LOCAL INFILE '".$target_file."'
            INTO TABLE ".$tmp_vehicle_stock_table."
            FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            IGNORE 1 ROWS
            (company,@adoption_date,vehicle_number,vehicle_identification_number,new_used_demo,manufacturer,model,colour_description,odometer_reading,mead_mcgrouther_code,listing_title,actual_cost,location_code,model_year)
            SET adoption_date = STR_TO_DATE(@adoption_date, '%d/%m/%Y');";

            DB::connection()->getpdo()->exec($query);

            $query = "CALL vehicle_stock_csv_import_sp(@sp_result);";
            
            $output = DB::connection()->getpdo()->exec($query);

        }
    }
}
