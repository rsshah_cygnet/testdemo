<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Backend\Blog\BlogRepositoryContract;
use DB;

class BlogScheduledPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change post status to Publish according to current time';

    /**
     * Repository Object
     *
     * @var object
     */
    public $repository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BlogRepositoryContract $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * __construct
     *
     * @param BlogRepositoryContract $repository
     */
    // function __construct(BlogRepositoryContract $repository)
    // {
    //     $this->repository = $repository;
    // }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $postsWithScheduledStatus = DB::table('blogs')->select('id','publish_datetime','status')->where('status','Scheduled')->get();
            
         foreach($postsWithScheduledStatus as $post)
         {
            if($post->publish_datetime <= date('Y-m-d H:i:s'))
            {
                $post->record_status = 2;

                $this->repository->update($post->id,['record_status'=>  $post->record_status]);
            }
         }
    }
}
