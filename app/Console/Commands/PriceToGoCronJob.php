<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Vehicle\Vehicle;
use App\Models\VehicleStock\VehicleStock;
use App\Models\PriceToGoVehicle\PriceToGoVehicle;
use DB;

class PriceToGoCronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:priceToGo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $priceToGoSetting = DB::table('price_to_go_settings')->where('status','Active')->first();
        $days = $priceToGoSetting->days;
        $percentage = $priceToGoSetting->percentage;

        $todays_date = date('Y-m-d');

        $vehicles = VehicleStock::select('vehicles.id','sold','adoption_date','selling_price')->leftjoin('vehicles','vehicle_stocks.vehicle_id','=','vehicles.id')->where('sold','=',0)->where('vehicle_type','U')->orWhere('vehicle_type','D')->limit(5)->get();
        // dd($vehicles);
        foreach($vehicles as $vehicle)
        {
            //adding $days after the adoption date
            $date = date('Y-m-d',strtotime("+".$days." days", strtotime($vehicle->adoption_date)));

            //checking if adoption date has passed today's date
            if($todays_date >= $date)
            {
                //calculate new value of selling price
                $newValue = ($percentage/100)*$vehicle->selling_price;

                $vehicle->newValue = $vehicle->selling_price - $newValue;

                //Updating a record or creating a new one
                PriceToGoVehicle::updateOrCreate(
                [
                'vehicle_id' => $vehicle->id
                ],
                [
                'vehicle_id' => $vehicle->id,
                'new_selling_price' => $vehicle->newValue,
                'old_selling_price' => $vehicle->selling_price,
                ]);

                Vehicle::where('id',$vehicle->id)->update(
                    ['selling_price' => $vehicle->newValue]);
            }
            

        }
    }
}
