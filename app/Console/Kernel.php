<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Commands\CSVImportCronJob;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        'App\Console\Commands\CSVImportCronJob',
        'App\Console\Commands\BlogScheduledPosts',
        'App\Console\Commands\PriceToGoCronJob'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('import:csv')->daily();
        $schedule->command('schedule:posts')->everyMinute();
//        $schedule->command('execute:priceToGo')->cron('* * */'.getDaysFromPriceToGo().' * *');

    }
}
