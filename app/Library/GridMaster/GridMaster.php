<?php

namespace App\Library\GridMaster;

/**
 * Class GridMaster
 *
 * @author Anuj Jaha ayjaha@cygnet-infotech.com
 */
use App\Http\Utilities\Jqgrid;

class GridMaster
{

    /**
     * JQ Grid
     *
     * @var object
     */
    public $jqgrid;

    /**
     * Repository
     *
     * @var object
     */
    public $repository;

    /**
     * Date Type Fields 
     *
     * @var object
     */
    public $dateTypeFields;

    /**
     * __construct
     * 
     */
    public function __construct()
    {
        $this->jqgrid = new Jqgrid;
    }

    /**
     * Get Grid Columns
     * 
     * @return $this
     */
    public function getGridColumns()
    {
        return $this->gridColumn;
    }

    /**
     * Set Grid Columns 
     * 
     * @param array
     * @return $this
     */
    public function setGridColumns($columns)
    {
        $this->gridColumn = $columns;

        return $this;
    }

    /* Get Repository
     * 
     * @return $this
     */

    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Set Grid Columns 
     * 
     * @param array
     * @return $this
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * Get Grid Data
     * @param object $request
     * @param object $repository
     * @param string $type
     * @return string json
     */
    public function getGridData($request,$repository,$type)
    {
        $data = $this->jqgrid->Preparedata($request,$repository,$type);
        $response = $this->getRows($repository,$data);

        return json_encode($response);
    }

    /**
     * Get Rows
     * 
     * @param array $data
     * @return array
     */
    public function getRows($repository,$data)
    {
        $response = $data[0];

        $i = 0;
        foreach ($data[1] as $record)
        {
            $response->rows[$i]['id'] = $record->id;
            $response->rows[$i]['cell'] = $this->getSingleRow($repository,$record);
            $i++;
        }

        return $response;
    }

    /**
     * Get Single Row
     * 
     * @param object $record
     * @return array
     */
    public function getSingleRow($repository,$record,$action = true)
    {
        $result = [];

        foreach ($this->gridColumn as $key => $column)
        {
            $gridColumn = explode('.',$this->gridColumn[$key]);
            $gridColumn = $gridColumn[1];


            $column = explode('.',$column);
            $column = $column[1];

            if (!isset($record->$column))
            {
                $value = $this->getRelationalRecord($repository,$column,$record);

                if (isset($value))
                {
                    array_push($result,$value);
                }
            }
            else if (!is_null($this->dateTypeFields) && in_array($gridColumn,$this->dateTypeFields))
            {
                $date = date(getApplicationDateFormat(),strtotime($record->$column));
                array_push($result,$date);
            }
            else if (is_object($record->$column))
            {
                array_push($result,$record->$column->format(getApplicationDateFormat()));
            }
            else
            {
                array_push($result,$record->$column);
            }
        }

        if ($action)
        {

            array_push($result,$record->action_buttons);
        }

        return $result;
    }

    /**
     * Set Date Type Fields
     *
     * @param array $dateTypeFields
     */
    public function setDateTypeFields($dateTypeFields = array())
    {
        if (is_array($dateTypeFields) && count($dateTypeFields))
        {
            $this->dateTypeFields = $dateTypeFields;
        }

        return $this;
    }

    /**
     * Get Single Row
     * 
     * @param object $repository
     * @param object $record
     * @return array
     */
    public function getRelationalRecord($repository,$column,$record)
    {
        foreach ($repository->relations as $key => $relationalColumn)
        {
            if (isset($record->$relationalColumn->$column))
            {
                return $record->$relationalColumn->$column;
            }
        }

        return false;
    }

    /**
     * Download Grid Data
     * 
     * @param object $request
     * @param object $repository
     * @param string $type
     * @return file
     */
    public function downloadGridData($request,$repository,$type)
    {
        $records = $this->jqgrid->Preparedata($request,$repository,$type);
        $dtype = $request->get('dtype');

        $data = array();

        $data[0] = $this->getDownloadFileHeaders($repository);

        //make array for json
        $i = 1;
        foreach ($records as $record)
        {

            $data[$i] = $this->getSingleRow($repository,$record,false);
            $i++;
        }

        $this->jqgrid->Download($repository->downloadfilename,$data,$dtype);
    }

    /**
     * Get Download File Headers
     * 
     * @param object $repository
     * @return array
     */
    public function getDownloadFileHeaders($repository)
    {
        $headers = [];

        foreach ($repository->gridColumn as $column)
        {
            $column = explode(".",$column);
            $column = $column[1];

            array_push($headers,ucwords(str_replace("_"," ",$column)));
        }

        return $headers;
    }

}
