<?php

namespace App\Models\FrontendUserPasswordReset;

use Illuminate\Database\Eloquent\Model;

class FrontendUserPasswordReset extends Model {
	/**
	 * Database Table
	 *
	 */
	protected $table = "front_user_password_resets";
}
