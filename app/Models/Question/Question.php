<?php namespace App\Models\Question;

/**
 * Class Question
 *
 */

use App\Models\BaseModel;
use App\Models\Question\Traits\Attribute\Attribute;
use App\Models\Question\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class Question extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "question";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
      

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}