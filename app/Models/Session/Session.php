<?php namespace App\Models\Session;

/**
 * Class Levels
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\Session\Traits\Attribute\Attribute;
use App\Models\Session\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class Session extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "session";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}