<?php

namespace App\Models\Country;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country\Traits\Attribute\Attribute;
use App\Models\Country\Traits\Relationship\Relationship;

/**
 * Class Country
 * @package App\Models
 */
class Country extends Model
{
	use Relationship, Attribute;
	
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "country";

	
    protected $fillable = [
       'country_name',
       'status',
       'sortname',
       'phonecode'
    ];
        
    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
    
    /**
     * The list of cities.
     *
     * @var array
     */
    public static function getAll()
    {
        return static::where('status','=','1')->pluck('country_name','id')->all();   
    }
}
