<?php namespace App\Models\TutorPreference;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\TutorPreference\Traits\Attribute\Attribute;
use App\Models\TutorPreference\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class TutorPreference extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "tutor_preference";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
        'id',

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}
