<?php

namespace App\Models\PunctualityRatingCancelSession;

use App\Models\PunctualityRatingCancelSession\Traits\Attribute\Attribute;
use App\Models\PunctualityRatingCancelSession\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\Model;

class PunctualityRatingCancelSession extends Model {
	use Attribute, Relationship;

	/**
	 * Database Table
	 *
	 */
	protected $table = "punctuality_ratings_for_cancelled_session";

	/**
	 * Fillable Database Fields
	 *
	 */
	protected $fillable = [
		'from',
		'to',
		'rating',
		'within',
		'type',
		'status',
	];
	/**
	 * Guarded ID Column
	 *
	 */
	protected $guarded = ["id"];
}
