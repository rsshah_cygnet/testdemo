<?php

namespace App\Models\TutorDetails;

use Illuminate\Database\Eloquent\Model;

class TutorDetails extends Model {
	protected $table = 'tutor_details';
}
