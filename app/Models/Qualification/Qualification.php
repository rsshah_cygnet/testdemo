<?php

namespace App\Models\Qualification;

use Illuminate\Database\Eloquent\Model;
use App\Models\Qualification\Traits\Attribute\Attribute;
use App\Models\Qualification\Traits\Relationship\Relationship;

/**
 * Class Qualification
 * @package App\Models
 */
class Qualification extends Model
{
	use Relationship, Attribute;
	
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "qualification";

	
    protected $fillable = [
       'qualification_name',
       'status',
    ];
        
    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
    
    /**
     * The list of cities.
     *
     * @var array
     */
    public static function getAll()
    {
        return static::where('status','=','1')->pluck('qualification_name','id')->all();   
    }
}
