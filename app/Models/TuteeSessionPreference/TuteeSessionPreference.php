<?php namespace App\Models\TuteeSessionPreference;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\TuteeSessionPreference\Traits\Attribute\Attribute;
use App\Models\TuteeSessionPreference\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class TuteeSessionPreference extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "tutee_session_preference";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
          'id',
       

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}