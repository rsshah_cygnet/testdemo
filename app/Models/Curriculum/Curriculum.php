<?php
namespace App\Models\Curriculum;

use App\Models\BaseModel;
use App\Models\Curriculum\Traits\Attribute\Attribute;
use App\Models\Curriculum\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curriculum extends BaseModel {
	use Attribute, Relationship, SoftDeletes;

	/**
	 * Database Table
	 *
	 */
	protected $table = "curriculum";

	/**
	 * Fillable Database Fields
	 *
	 */
	protected $fillable = [
		'name',
		'status',
	];
	/**
	 * Guarded ID Column
	 *
	 */
	protected $guarded = ["id"];

	public static function getCurriculumList($where='')
    {
    	if($where != '')
    		return Curriculum::where('status',1)->where($where,1)->pluck('curriculum_name','id')->all();
    	else
    		return Curriculum::where('status',1)->pluck('curriculum_name','id')->all();

    }
}
