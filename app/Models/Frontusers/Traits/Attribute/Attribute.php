<?php namespace App\Models\Frontusers\Traits\Attribute;

/**
 * Trait Attribute
 *
 * @author Sudhir Virpara srvirpara@cygnet-infotech.com
 */

trait Attribute {
	/**
	 * @return string
	 */
	public function getEditButtonAttribute() {
		if (access()->allow('edit-blog')) {
			return '<a href="' . route('admin.blog.edit', $this->id) . '" class="grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getDeleteButtonAttribute() {
		//Can't delete master admin role
		// if ($this->id != 1) {
		if (access()->allow('delete-front-users')) {
			return '<a href="' . route('admin.frontusers.delete', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
		}
		// }

		return '';
	}

	/**
	 * @return string
	 */
	public function getStatusButtonAttribute() {
		switch ($this->status) {
		case 0:
			if (access()->allow('status-front-users')) {
				return '<span class="grid-icon"><i data-url="' . route('admin.frontusers.mark', [$this->id, 1]) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.activate') . '"></i></span>';
			}

			break;

		case 1:
			if (access()->allow('status-front-users')) {
				return '<span class="grid-icon"><i data-url="' . route('admin.frontusers.mark', [$this->id, 0]) . '" class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.deactivate') . '"></i></span>';
			}

			break;

		default:
			return '';
			// No break
		}

		return '';
	}

	public function getTestResultButtonAttribute() {

		$testRecord = \DB::table('tutor_details')
			->where('front_user_id', $this->id)
			->where('appeared', 1)
			->where('is_completed', 1)
			->count();
		// dd($testRecord);
		switch ($this->role) {
		case 1:
		case 3:

			//if (access()->allow('reactivate-users')) {
			if (!empty($testRecord)) {
				return '<a target="_blank" href="' . route('admin.frontusers.test.preview', [$this->id]) . '"><i  class="fa fa-eye" data-placement="top" title="' . trans('buttons.userTestResult.view') . '"></i></a>';
				//}
			} else {
				return '<a href="#"><i  class="fa fa-eye-slash" aria-hidden="true" data-placement="top" title="' . trans('buttons.userTestResult.view') . '"></i></a>';
			}

			break;

		default:
			return '';
			// No break
		}

		return '';
	}

	public function getMailButtonAttribute() {
		switch ($this->role) {
		case 2:
			if (access()->allow('view-news-letter-management')) {
				return '<a href="javascript:void(0);" onclick="sendNewslettermail(' . $this->id . ')"><i class="fa fa-envelope" style="margin-left:10px;" data-placement="top" title="Send reminder"></i></a>';
			}

			break;
		case 3:
			if (access()->allow('view-news-letter-management')) {
				return '<a href="javascript:void(0);" onclick="sendNewslettermail(' . $this->id . ')"><i class="fa fa-envelope" style="margin-left:10px;" data-placement="top" title="Send reminder"></i></a>';
			}

			break;

		default:
			return '';
			// No break
		}

		return '';
	}

	/**
	 * @return string
	 **/
	public function getPreviewButtonAttribute() {
		$cat = explode('/', $this->slug)[0];
		$slug = explode('/', $this->slug)[1];

		if (access()->allow('view-blog')) {
			return '<a target="_blank" href="' . route('admin.blog.preview', [$cat, $slug]) . '"><i class="fa fa-eye" data-placement="top" title="' . trans('buttons.general.view') . '"></i></a>';
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getStatusLabelAttribute() {
		if ($this->isStatus()) {
			return "<label class='label label-success'>" . trans('labels.general.yes') . "</label>";
		}

		return "<label class='label label-danger'>" . trans('labels.general.no') . "</label>";
	}

	/**
	 * @return bool
	 */
	public function isStatus() {
		return $this->status == 1;
	}

	/**
	 * @return string
	 */
	public function getActionButtonsAttribute() {
		return
		//$this->getEditButtonAttribute();
		$this->getDeleteButtonAttribute() .
		$this->getStatusButtonAttribute() .
		$this->getTestResultButtonAttribute() .
		$this->getMailButtonAttribute();
	}

}