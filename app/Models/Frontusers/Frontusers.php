<?php namespace App\Models\Frontusers;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\Frontusers\Traits\Attribute\Attribute;
use App\Models\Frontusers\Traits\Relationship\Relationship;

class Frontusers extends BaseModel {
	use Attribute, Relationship;

	/**
	 * Database Table
	 *
	 */
	protected $table = "front_user";

	/**
	 * Fillable Database Fields
	 *
	 */
	protected $fillable = [
		'role',
		'first_name',
		'last_name',
		'username',
		'email',
		'password',
		'country_id',
		'state_id',
		'city_id',
		'nationality',
		'lang_id',
		'photo',
		'dob',
		'timezone_id',
		'created_at',
		'updated_at',
		'modified_by',
		'status',
		'deleted_at',

	];

	/**
	 * Guarded ID Column
	 *
	 */
	protected $guarded = ["id"];
}