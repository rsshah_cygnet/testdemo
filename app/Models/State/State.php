<?php

namespace App\Models\State;

use Illuminate\Database\Eloquent\Model;
use App\Models\State\Traits\Attribute\Attribute;
use App\Models\State\Traits\Relationship\Relationship;

/**
 * Class State
 * @package App\Models
 */
class State extends Model
{
	use Relationship, Attribute;
	
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "state";

    protected $fillable = [
       'state_name',
       'country_id',
       'status',
    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];

	/**
     * The list of cities.
     *
     * @var array
     */
    public static function getAll()
    {
        return static::where('Status','=','1')->pluck('state_name','id')->all();   
    }
}
