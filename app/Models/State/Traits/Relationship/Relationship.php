<?php
namespace App\Models\State\Traits\Relationship;

use App\Models\State\State;
use App\Models\Country\Country;

/**
 * Class CityRelationship
 * @package App\Models\City\Traits\Relationship
 */
trait Relationship
{
	/**
	 * Relationship Mapping for country
	 * 
	 * @return mixed
	 */
	public function country()
	{
		return $this->belongsTO(Country::class, 'country_id');
	}
}
