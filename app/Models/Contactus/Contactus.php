<?php namespace App\Models\Contactus;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\Contactus\Traits\Attribute\Attribute;
use App\Models\Contactus\Traits\Relationship\Relationship;
  
class Contactus extends BaseModel
{
    use Attribute, Relationship;

    /**
     * Database Table
     *
     */
    protected $table = "contact_us";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}