<?php

namespace App\Models\SessionFeedback;

use App\Models\SessionFeedback\Traits\Attribute\Attribute;
use App\Models\SessionFeedback\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\Model;

class SessionFeedback extends Model {
	use Relationship, Attribute;

	/**
	 * Database Table
	 *
	 */
	protected $table = "session_feedbacks";

	/**
	 * Fillable Database Fields
	 *
	 */
	protected $fillable = [];

	/**
	 * Guarded ID Column
	 *
	 */
	protected $guarded = ["id"];
}
