<?php

namespace App\Models\Notification\Traits\Attribute;

/**
 * Trait NotificationAttribute
 * @package App\Models\Notification\Traits\NotificationAttribute
 */
trait NotificationAttribute {

    /**
     * @return string
     */
    public function getStatusButtonAttribute() {
        switch ($this->is_read_admin) {
            case 0:
                if (access()->allow('reactivate-notification')) {
                    return '<span class="grid-icon"><a href="javascript:void(0)";><i data-url="' . route('admin.notification.mark', [$this->id, 1]) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.read') . '"></i></a></span> ';
                }

                break;

            case 1:
                if (access()->allow('deactivate-notification')) {
                    return '<span class="grid-icon"><a href="javascript:void(0)";><i data-url="' . route('admin.notification.mark', [$this->id, 0]) . '" class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.unread') . '"></i></a></span> ';
                }

                break;

            default:
                return '';
            // No break
        }

        return '';
    }

    /**
     * @return string
     */
    public function getConfirmedLabelAttribute() {
        if ($this->isActive())
            return "<label class='label label-success'>" . trans('labels.general.yes') . "</label>";
        return "<label class='label label-danger'>" . trans('labels.general.no') . "</label>";
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->is_read_admin == 1;
    }

    /**
     * @return bool
     */
    // public function isConfirmed() {
    //     return $this->confirmed == 1;
    // }

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {

        return $this->getStatusButtonAttribute();
    }

}
