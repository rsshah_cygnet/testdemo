<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;
use App\Models\Notification\Traits\Attribute\NotificationAttribute;

class Notification extends Model {

    /**
     * Attribute for action buttons 
     */
    use NotificationAttribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;
    
    public $timestamps = false;

    /**
     * [__construct description]
     */
    public function __construct() {
        $this->table = config('backend.notification_table');
    }

}
