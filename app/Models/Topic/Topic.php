<?php 

namespace App\Models\Topic;

 /**
 * Class Topic
 */

use App\Models\BaseModel;
use App\Models\Topic\Traits\Attribute\Attribute;
use App\Models\Topic\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class Topic extends BaseModel
{
    /**
     * Attribute for action buttons in grid
    */
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "topic";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [];

    /**
     * Guarded Database Fields
     *
     */
    protected $guarded = ["id"];
}