<?php namespace App\Models\Request;

/**
 * Class Request
 *
 * @author Vaishal Gandhi vhgandhi@cygnet-infotech.com
 */

use App\Models\BaseModel;
use App\Models\Request\Traits\Attribute\Attribute;
use App\Models\Request\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class Request extends BaseModel
{
    use Attribute, Relationship;

    /**
     * Database Table
     *
     */
    protected $table = "requests";

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}