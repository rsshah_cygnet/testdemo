<?php namespace App\Models\Request\Traits\Attribute;

/**
 * Trait Attribute
 *
 * @author Vaishal Gandhi vhgandhi@cygnet-infotech.com
 */

use File;

trait Attribute
{
 	/**
 	 * @return string
 	 */
 	public function getEditButtonAttribute()
 	{ 
 	    if (access()->allow('aprroved-reject-request')) {
 	        return '<a href="javascript:void(0)" data-id="'.$this->id.'" class="edit-form grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
 	    }

 	    return '';
 	}

 	/**
 	 * @return string
 	 */
 	public function getDeleteButtonAttribute()
 	{
 	    //Can't delete master admin role
 	    // if ($this->id != 1) {
 	        if (access()->allow('delete-request')) {
 	            return '<a href="' . route('admin.price-to-go.destroy', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
 	        }
 	    // }

 	    return '';
 	}

 	/**
 	 * @return string
 	 */
 	public function getActionButtonsAttribute()
 	{
 	    return $this->getEditButtonAttribute() .
 	    	   $this->getDeleteButtonAttribute();
 	}  
}