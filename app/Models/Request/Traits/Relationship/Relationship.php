<?php namespace App\Models\Request\Traits\Relationship;

/**
 * Trait Relationship
 *
 * @author Vaishal Gandhi vhgandhi@cygnet-infotech.com
 */

use App\Models\Access\User\User;

trait Relationship
{
	/**
	 * Relationship Mapping for Account
	 * 
	 * @return mixed
	 */
	public function users()
	{
		return $this->belongsTO(User::class, 'created_by');
	}

}	