<?php

namespace App\Models\PunctualityRatings;

use App\Models\PunctualityRatings\Traits\Attribute\Attribute;
use App\Models\PunctualityRatings\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\Model;

class PunctualityRatings extends Model {
	use Attribute, Relationship;

	/**
	 * Database Table
	 *
	 */
	protected $table = "punctuality_ratings";

	/**
	 * Fillable Database Fields
	 *
	 */
	protected $fillable = [
		'from',
		'to',
		'rating',
	];
	/**
	 * Guarded ID Column
	 *
	 */
	protected $guarded = ["id"];
}
