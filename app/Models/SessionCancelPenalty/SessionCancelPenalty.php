<?php

namespace App\Models\SessionCancelPenalty;

use App\Models\SessionCancelPenalty\Traits\Attribute\Attribute;
use App\Models\SessionCancelPenalty\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\Model;

class SessionCancelPenalty extends Model {
	use Attribute, Relationship;

	/**
	 * Database Table
	 *
	 */
	protected $table = "session_cancelled_penalty";

	/**
	 * Fillable Database Fields
	 *
	 */
	protected $fillable = [
		'from',
		'to',
		'penalty_percentage',
		'within',
		'type',
		'status',
	];
	/**
	 * Guarded ID Column
	 *
	 */
	protected $guarded = ["id"];
}
