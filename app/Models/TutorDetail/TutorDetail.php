<?php namespace App\Models\TutorDetail;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\TutorDetail\Traits\Attribute\Attribute;
use App\Models\TutorDetail\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Frontusers\Frontusers;
  
class TutorDetail extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "tutor_details";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
          'id',
       

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];

    public function hierachyName()
    {
        return $this->belongsTO(Frontusers::class, 'front_user_id');
    }
}