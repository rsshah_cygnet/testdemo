<?php namespace App\Models\UserEducation\Traits\Attribute;

/**
 * Trait Attribute
 *
 * @author Sudhir Virpara srvirpara@cygnet-infotech.com
 */


trait Attribute
{
 	/**
 	 * @return string
 	 */
 	public function getEditButtonAttribute()
 	{
 	    if (access()->allow('edit-blog')) {
 	        return '<a href="' . route('admin.blog.edit', $this->id) . '" class="grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
 	    }

 	    return '';
 	}

 	/**
 	 * @return string
 	 */
 	public function getDeleteButtonAttribute()
 	{
 	    //Can't delete master admin role
 	    // if ($this->id != 1) {
 	        if (access()->allow('delete-frontusers')) {
 	            return '<a href="' . route('admin.frontusers.delete', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
 	        }
 	    // }

 	    return '';
 	}

 	/**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 0:
                if (access()->allow('reactivate-users')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.frontusers.mark', [$this->id,1]) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.activate') . '"></i></span>';
                }

                break;

            case 1:
                if (access()->allow('deactivate-users')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.frontusers.mark', [$this->id,0]) . '" class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.deactivate') . '"></i></span>';
                }

                break;

            default:
                return '';
                // No break
        }

        return '';
    }

    public function getTestResultButtonAttribute()
    {
          switch ($this->passed_status) {
            case 0:
                if (access()->allow('reactivate-users')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.frontusers.testresult.mark', [$this->id,1]) . '" class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.userTestResult.pass') . '"></i></span>';
                }

                break;

            case 1:
                if (access()->allow('deactivate-users')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.frontusers.testresult.mark', [$this->id,0]) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.userTestResult.fail') . '"></i></span>';
                }

                break;

            default:
                return '';
                // No break
        }

        return '';
    }

 	/**
 	* @return string 
 	**/
 	public function getPreviewButtonAttribute()
	{     
		if (access()->allow('view-blog')) {
	 		return '<a target="_blank" href="'. route('admin.frontusers.viewQue',[$this->id]) .'"><i class="fa fa-eye" data-placement="top" title="'. trans('buttons.general.view') .'"></i></a>';
	 	}

	 	return '';
	}


	   /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isStatus())
            return "<label class='label label-success'>".trans('labels.general.yes')."</label>";
        return "<label class='label label-danger'>".trans('labels.general.no')."</label>";
    }


    /**
     * @return bool
     */
    public function isStatus() {
        return $this->status == 1;
    }
    

 	/**
 	 * @return string
 	 */
 	public function getActionButtonsAttribute()
 	{
 	    return 
 	    //$this->getEditButtonAttribute();
 	   // $this->getDeleteButtonAttribute() .
 	  //  $this->getStatusButtonAttribute().
        $this->getTestResultButtonAttribute().
 	    $this->getPreviewButtonAttribute();
 	}


 	
}