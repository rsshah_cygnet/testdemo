<?php namespace App\Models\UserEducation;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\UserEducation\Traits\Attribute\Attribute;
use App\Models\UserEducation\Traits\Relationship\Relationship;
  
class UserEducation extends BaseModel
{
    use Attribute, Relationship;

    /**
     * Database Table
     *
     */
    protected $table = "user_educational";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
          'id',
       

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}