<?php namespace App\Models\Blog\Traits\Attribute;

/**
 * Trait Attribute
 *
 * @author Viral Solani vhsolani@cygnet-infotech.com
 */

use File;
use Carbon\Carbon;

trait Attribute
{
 	/**
 	 * @return string
 	 */
 	public function getEditButtonAttribute()
 	{
 	    if (access()->allow('edit-blog')) {
 	        return '<a href="' . route('admin.blog.edit', $this->id) . '" class="grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
 	    }

 	    return '';
 	}

 	/**
 	 * @return string
 	 */
 	public function getDeleteButtonAttribute()
 	{
 	    //Can't delete master admin role
 	    // if ($this->id != 1) {
 	        if (access()->allow('delete-blog')) {
 	            return '<a href="' . route('admin.blog.destroy', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
 	        }
 	    // }

 	    return '';
 	}

 	/**
 	 * @return string
 	 */
 	public function getStatusButtonAttribute()
 	{
 	    switch ($this->is_active) {
 	        case 'Inactive':
 	            if (access()->allow('reactivate-blog')) {
 	                return '<span class="grid-icon"><i data-url="' . route('admin.blog.mark', [$this->id, 'Active']) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.activate') . '"></i></span>';
 	            }

 	            break;

 	        case 'Active':
 	            if (access()->allow('deactivate-blog')) {
 	                return '<span class="grid-icon"><i data-url="' . route('admin.cmspages.mark', [$this->id, 'Inactive']) . '"  class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.deactivate') . '"></i></span>';
 	            }

 	            break;

 	        default:
 	            return '';
 	            // No break
 	    }

 	    return '';
 	}

 	/**
 	* @return string 
 	**/
 	public function getPreviewButtonAttribute()
	{
		$cat = explode('/',$this->slug)[0];
		$slug = explode('/',$this->slug)[1];

	 	if (access()->allow('view-blog')) {
	 		return '<a target="_blank" href="'. route('admin.blog.preview',[ $cat,$slug]) .'"><i class="fa fa-eye" data-placement="top" title="'. trans('buttons.general.view') .'"></i></a>';
	 	}

	 	return '';
	}

 	/**
 	 * @return string
 	 */
 	public function getActionButtonsAttribute()
 	{
 	    return $this->getEditButtonAttribute() .
 	    $this->getDeleteButtonAttribute() .
 	    $this->getStatusButtonAttribute() .
 	    $this->getPreviewButtonAttribute();
 	}


 	public static function getStorePublishDatetimeAttribute($value)
 	{
 		return Carbon::createFromFormat('d/m/Y h:i a', $value);
 	}

 	public static function getEditPublishDatetimeAttribute($value)
 	{
 		return Carbon::parse($item->publish_datetime)->format('d/m/Y h:i a');
 	}
}