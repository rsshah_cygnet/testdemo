<?php namespace App\Models\Blog\Traits\Relationship;

/**
 * Trait Relationship
 *
 * @author Viral Solani vhsolani@cygnet-infotech.com
 */
use App\Models\BlogTag\BlogTag;
use App\Models\BlogCategoryModel\BlogCategoryModel;


trait Relationship
{

	/**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(BlogTag::class, 'blog_map_tags', 'blog_id', 'tag_id');
    }


    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(BlogCategoryModel::class, 'blog_map_categories', 'blog_id', 'category_id');
    }


}