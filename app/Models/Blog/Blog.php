<?php namespace App\Models\Blog;

/**
 * Class Blog
 *
 * @author Anuj Jaha ayjaha@cygnet-infotech.com
 */

use App\Models\BaseModel;
use App\Models\Domain\Domain;
use App\Models\Blog\Traits\Attribute\Attribute;
use App\Models\Blog\Traits\Relationship\Relationship;

class Blog extends BaseModel
{
    use Attribute, Relationship;

    /**
     * Database Table
     *
     */
    protected $table = "blogs";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
       'name',
       'publish_datetime',
       'featured_image',
       'content',
       'domain_id',
       'meta_title',
       'slug',
       'cannonical_link',
       'meta_description',
       'meta_keywords',
       'status',
       'created_by',
       'updated_by'
    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}