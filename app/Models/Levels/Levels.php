<?php namespace App\Models\Levels;

/**
 * Class Levels
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\Levels\Traits\Attribute\Attribute;
use App\Models\Levels\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class Levels extends BaseModel
{
    use Attribute, Relationship,SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "levels";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];

    /*
     * Get All Level Name
     */
    public static function getLevelsList()
    {
      return Levels::where('status',1)->pluck('levels_name','id')->all();
    }
}