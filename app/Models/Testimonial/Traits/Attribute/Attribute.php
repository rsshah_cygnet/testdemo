<?php namespace App\Models\Testimonial\Traits\Attribute;

/**
 * Trait Attribute
 *
 * @author Sudhir Virpara srvirpara@cygnet-infotech.com
 */

trait Attribute {

	/**
	 * @return string
	 */
	public function getDeleteButtonAttribute() {

		// if (access()->allow('delete-contactus')) {
		// 	return '<a href="' . route('admin.testimonial.delete', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
		// }
		return '<a href="' . route('admin.testimonial.delete', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';

		return '';
	}

	/**
	 * @return string
	 */
	public function getStatusButtonAttribute() {
		switch ($this->status) {
		case 0:
			if (access()->allow('read-contactus')) {
				return '<span class="grid-icon"><a href="javascript:void(0)";><i data-url="' . route('admin.testimonial.mark', [$this->id, 1]) . '" class="fa fa-remove active-record" data-placement="top" title="Active"></i></a></span> ';
			}

			break;

		case 1:
			if (access()->allow('read-contactus')) {
				return '<span class="grid-icon"><a href="javascript:void(0)";><i data-url="' . route('admin.testimonial.mark', [$this->id, 0]) . '" class="fa fa-check active-record" data-placement="top" title="Inactive"></i></a></span> ';
			}

			break;

		default:
			return '';
			// No break
		}

		return '';
	}


	public function getPublishButtonAttribute() {
		switch ($this->publish_on_home_page) {
		case 1:
			/*if (access()->allow('read-contactus')) {*/
				return '<span class="grid-icon"><a href="javascript:void(0)";><i data-url="' . route('admin.testimonial.publish', [$this->id, 0]) . '" class="fa fa-newspaper-o active-record" data-placement="top" title="Unpublish"></i></a></span> ';
			/*}*/

			break;

		case 0:
			/*if (access()->allow('read-contactus')) {*/
				return '<span class="grid-icon"><a href="javascript:void(0)";><i data-url="' . route('admin.testimonial.publish', [$this->id, 1]) . '" class="fa fa-newspaper-o active-record" data-placement="top" title="Publish"></i></a></span> ';
			/*}*/

			break;

		default:
			return '';
			// No break
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getStatusLabelAttribute() {
		if ($this->isStatus()) {
			return "<label class='label label-success'>" . trans('labels.general.yes') . "</label>";
		}

		return "<label class='label label-danger'>" . trans('labels.general.no') . "</label>";
	}

	/**
	 * @return bool
	 */
	public function isStatus() {
		return $this->status == 1;
	}

	/**
	 * @return string
	 */
	public function getActionButtonsAttribute() {
		return
		$this->getDeleteButtonAttribute() .
		/*$this->getStatusButtonAttribute().*/
		$this->getPublishButtonAttribute();

	}

}