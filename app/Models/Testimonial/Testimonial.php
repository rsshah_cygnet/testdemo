<?php

namespace App\Models\Testimonial;

use App\Models\Testimonial\Traits\Attribute\Attribute;
use App\Models\Testimonial\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model {
	use Relationship, Attribute;

	/**
	 * Database Table
	 *
	 */
	protected $table = "testimonials";

	/**
	 * Fillable Database Fields
	 *
	 */
	protected $fillable = [];

	/**
	 * Guarded ID Column
	 *
	 */
	protected $guarded = ["id"];

}
