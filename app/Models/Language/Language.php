<?php

namespace App\Models\Language;

use Illuminate\Database\Eloquent\Model;
use App\Models\Language\Traits\Attribute\Attribute;
use App\Models\Language\Traits\Relationship\Relationship;

/**
 * Class Country
 * @package App\Models
 */
class Language extends Model
{
	use Relationship, Attribute;
	
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "language";

	
    protected $fillable = [
       'name',
       'status'
    ];
        
    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
    
    /**
     * The list of cities.
     *
     * @var array
     */
    public static function getAll()
    {
        return static::where('status','=','1')->orderBy('name', 'ASC')->pluck('name','id')->all();   
    }
}
