<?php

namespace App\Models\Emailtemplate;

use Illuminate\Database\Eloquent\Model;

class EmailTemplatePlaceholder extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;
	
    /**
	 * Fillable fileds for cmspage
	 * @var array
	 */
	protected $fillable = [
		'id',
		'name',
		'status'
	];
	/**
	 * [__construct description]
	 */
	public function __construct()
    {
        $this->table = config('backend.email_template_placeholder_table');
    }
    /**
     * [getPlaceHoldrList description]
     * @return [type] [description]
     */
    public static function getPlaceHoldrList()
    {
    	return static::pluck('name','id')->all();   
    }
}
