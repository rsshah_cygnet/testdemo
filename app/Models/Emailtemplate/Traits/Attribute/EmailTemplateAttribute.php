<?php
namespace App\Models\Emailtemplate\Traits\Attribute;

/**
 * Trait EmailTemplateAttribute
 * @package App\Models\Emailtemplate\Traits\EmailTemplateAttribute
 */
trait EmailTemplateAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (access()->allow('edit-emailtemplate')) {
            return '<a href="' . route('admin.emailtemplate.edit', $this->id) . '" class="grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
        }
        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (access()->allow('delete-emailtemplate')) {
            return '<a href="' . route('admin.emailtemplate.destroy', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
        }
        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getEditButtonAttribute() .
        $this->getDeleteButtonAttribute();
    }
}