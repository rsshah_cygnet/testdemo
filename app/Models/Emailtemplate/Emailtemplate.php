<?php

namespace App\Models\Emailtemplate;

use Illuminate\Database\Eloquent\Model;
use App\Models\Emailtemplate\Traits\Attribute\EmailTemplateAttribute;

class Emailtemplate extends Model {

    /**
     * Attribute for action buttons 
     */
    use EmailTemplateAttribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * [__construct description]
     */
    public function __construct() {
        $this->table = config('backend.email_template_table');
    }

}
