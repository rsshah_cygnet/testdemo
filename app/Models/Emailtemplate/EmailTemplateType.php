<?php

namespace App\Models\Emailtemplate;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
	 * Fillable fileds for cmspage
	 * @var array
	 */
	protected $fillable = [
		/*'id',
		'name',
		'status'*/
	];
	/**
	 * [__construct description]
	 */
	public function __construct()
    {
        $this->table = 'email_template_type';
    }
    /**
     * [getPlaceHoldrList description]
     * @return [type] [description]
     */
    public static function getTypeList()
    {
    	return static::pluck('name','id')->all();
    }
}
