<?php
namespace App\Models\City\Traits\Relationship;

use App\Models\City\City;
use App\Models\State\State;

/**
 * Class CityRelationship
 * @package App\Models\City\Traits\Relationship
 */
trait Relationship
{
	public function state()
	{
		return $this->belongsTO(State::class, 'state_id');
	}
}
