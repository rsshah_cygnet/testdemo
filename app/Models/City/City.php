<?php

namespace App\Models\City;

use Illuminate\Database\Eloquent\Model;
use App\Models\Address\Address;
use App\Models\City\Traits\Attribute\Attribute;
use App\Models\City\Traits\Relationship\Relationship;

/**
 * Class City
 * @package App\Models
 */
class City extends Model
{
	use Relationship, Attribute;
	
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

	/**
     *
     */
    public function __construct()
    {
        $this->table = config('backend.city_table');
    }

    public function address()
    {
        return $this->belongsToMany(Address::class, 'city_id');
    }
    
    
    public function models()
    {
         return $this->has_many('Model');
    }
    
    
    /**
     * The list of cities.
     *
     * @var array
     */
    public static function getAll()
    {
        return static::where('city_active','=','1')->pluck('city','city_id')->all();   
    }
}
