<?php namespace App\Models\Notificationtype;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;

  
class NotificationType extends BaseModel
{
    
    /**
     * Database Table
     *
     */
    protected $table = "notification_type";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];


    /**
     * getNotificationtypes 
     * @return [id] [type]
     */
    public static function getNotificationtypes()
    {
      return static::pluck('notification_type','id')->all();   
    }
}