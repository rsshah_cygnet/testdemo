<?php 
namespace App\Models\EducationalSystem\Traits\Attribute;

/**
 * Trait Attribute
 *
 */

trait Attribute
{
 	/**
 	 * @return string
 	 */
 	public function getEditButtonAttribute()
 	{
 	    if (access()->allow('edit-education-system')) {
 	        return '<a href="' . route('admin.educational-system.edit', $this->id) . '" class="edit-form grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
 	    }

 	    return '';
 	}

 	/**
 	 * @return string
 	 */
 	public function getDeleteButtonAttribute()
 	{
 	    
 	        if (access()->allow('delete-education-system')) {
 	            return '<a href="' . route('admin.educational-system.destroy', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
 	        }
 	   

 	    return '';
 	}

 	/**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 0:
                if (access()->allow('status-education-system')) {
                	return '<span class="grid-icon"><i data-url="' . route('admin.educational-system.mark', [$this->id, 1]) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.activate') . '"></i></span>';
                }

                break;

            case 1:
                if (access()->allow('status-education-system')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.educational-system.mark', [$this->id, 0]) . '"  class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.deactivate') . '"></i></span>';
                }

                break;

            default:
                return '';
                // No break
        }

        return '';
    }

 	/**
 	 * @return string
 	 */
 	public function getActionButtonsAttribute()
 	{
 	    return $this->getEditButtonAttribute();
 	    // $this->getDeleteButtonAttribute() .
 	    // $this->getStatusButtonAttribute();
 	}  
}