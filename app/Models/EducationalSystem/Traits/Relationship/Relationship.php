<?php 
namespace App\Models\EducationalSystem\Traits\Relationship;

/**
 * Trait Relationship
 *
 */

use App\Models\Curriculum\Curriculum;
use App\Models\Access\User\User;

trait Relationship
{
	/**
	 * Relationship Mapping for Brand
	 * 
	 * @return mixed
	 */
	public function curriculum()
	{
		return $this->belongsTO(Curriculum::class, 'curriculum_id');
	}

	/**
	 * Relationship Mapping for Account
	 * 
	 * @return mixed
	 */
	public function users()
	{
		return $this->belongsTO(User::class, 'modified_by');
	}

}	