<?php 
namespace App\Models\EducationalSystem;

/**
 * Class EducationalSystem
 *
 */

use App\Models\BaseModel;
use App\Models\EducationalSystem\Traits\Attribute\Attribute;
use App\Models\EducationalSystem\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;


class EducationalSystem extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "eductional_systems";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
       'curriculum_id',
       'name',
       'status',
       'modified_by',
       'created_at'
    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];

    public static function getEducatinalSystem()
    {
      return static::pluck('name','id')->all();   
    }

    public static function getEducationSystemList()
    {
      return EducationalSystem::where('status',1)->pluck('name','id')->all();
    }
}