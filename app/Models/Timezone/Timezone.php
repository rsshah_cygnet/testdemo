<?php

namespace App\Models\Timezone;

use Illuminate\Database\Eloquent\Model;
use App\Models\Timezone\Traits\Attribute\Attribute;
use App\Models\Timezone\Traits\Relationship\Relationship;

/**
 * Class Timezone
 * @package App\Models
 */
class Timezone extends Model
{
	use Relationship, Attribute;
	
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "timezone";

	
    protected $fillable = [
       'timezone_name',
       'status',
    ];
        
    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
    
    /**
     * The list of cities.
     *
     * @var array
     */
    public static function getAll()
    {
        return static::where('status','=','1')->pluck('timezone_name','id')->all();   
    }
}
