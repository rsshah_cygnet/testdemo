<?php  namespace App\Models;

/**
 * Class UpdateLogger
 *
 * @author Amit Pandey avpandey@cygnet-infotech.com
 */

use App\Models\Access\User\User;
use DB, Sentry, DateTime, DateTimeZone;
use Illuminate\Database\Eloquent\Model;

class UpdateLogger extends Model
{
    /**
     * Accounts
     *
     * @var array
     */
    protected $table = "data_update_logs";

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Fillable
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "section",
        "action",
        "item",
        "action_time"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Save a new model and return the instance.
     *
     * @param  array  $attributes
     * @return static
     */
    public static function create(array $attributes = [])
    {
        $attributes['action_time'] = new DateTime;
        return parent::create($attributes);
    }

    /**
     * Get Action Time
     *
     * @param bool $model
     * @return string
     */
    public function getActionTime($model = false)
    {
        $model = ($model ? $model : $this);

        $time = new DateTime('now', new DateTimeZone('America/New_York'));

        $time->setTimestamp(strtotime($model->action_time));

        return $time->format('M j, Y, g:i a');
    }

    /**
     * Get Action Logs
     *
     * @param $model
     * @param bool $account
     * @param mixed $item
     * @param int $limit
     * @return mixed
     */
    public function getActionLogs($model, $account = false, $item = false, $limit)
    {
        if(!$account)
        {
            $account = access()->account();
        }


        $modelClass  = (new \ReflectionClass($model))->getShortName();

        if(access()->allow('view-backend'))
        {
            $actions = parent::where('account_id', '=', $account->id)
                ->where('item', '=', $model->getOriginal('id'))
                ->where('section', '=', $modelClass)
                ->orderBy('action_time', 'desc');
        }
        else
        {
            $actions = parent::where('item', '=', $model->getOriginal('id'))
                ->where('section', '=', $modelClass)
                ->orderBy('action_time', 'desc');
        }

        if($item)
        {
            $actions->where('item', '=', $item);
        }

        return $actions->take($limit)->get();
    }
}