<?php namespace App\Models\TutorRatings;

/**
 * Class TutorRatings
 *
 */

use App\Models\BaseModel;
use App\Models\Frontusers\Frontusers;

class TutorRatings extends BaseModel
{
    

    /**
     * Database Table
     *
     */
    protected $table = "tutor_ratings";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
      

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];

    public function tuteeName()
    {
        return $this->belongsTO(Frontusers::class, 'tutee_id');
    }
}