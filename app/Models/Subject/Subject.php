<?php namespace App\Models\Subject;

/**
 * Class Levels
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\Subject\Traits\Attribute\Attribute;
use App\Models\Subject\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class Subject extends BaseModel
{
     use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "subject";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}