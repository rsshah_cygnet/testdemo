<?php namespace App\Models\Subject\Traits\Attribute;

/**
 * Trait Attribute
 *
 * @author Sudhir Virpara srvirpara@cygnet-infotech.com
 */


trait Attribute
{
 	/**
 	 * @return string
 	 */
 	public function getEditButtonAttribute()
 	{
 	    if (access()->allow('edit-subject')) {
 	        return '<a href="' . route('admin.subject.edit', $this->id) . '" class="grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
 	    }

 	    return '';
 	}

 	/**
 	 * @return string
 	 */
 	public function getDeleteButtonAttribute()
 	{
 	        if (access()->allow('delete-subject')) {
 	            return '<a href="' . route('admin.subject.delete', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
 	        }
 	    
 	    return '';
 	}

 	/**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 0:
                if (access()->allow('status-subject')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.subject.mark', [$this->id,1]) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.activate') . '"></i></span>';
                }

                break;

            case 1:
                if (access()->allow('status-subject')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.subject.mark', [$this->id,0]) . '" class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.deactivate') . '"></i></span>';
                }

                break;

            default:
                return '';
                // No break
        }

        return '';
    }

    
	   /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isStatus())
            return "<label class='label label-success'>".trans('labels.general.yes')."</label>";
        return "<label class='label label-danger'>".trans('labels.general.no')."</label>";
    }


    /**
     * @return bool
     */
    public function isStatus() {
        return $this->status == 1;
    }
    

 	/**
 	 * @return string
 	 */
 	public function getActionButtonsAttribute()
 	{
 	    return 
 	    $this->getEditButtonAttribute();
 	    // $this->getDeleteButtonAttribute() .
 	    // $this->getStatusButtonAttribute();
        
 	}


 	
}