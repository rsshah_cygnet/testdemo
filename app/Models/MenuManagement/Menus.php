<?php

namespace App\Models\MenuManagement;

/**
 * Class Menus
 *
 * @author Anuj Jaha ayjaha@cygnet-infotech.com
 */
use App\Models\BaseModel;
use App\Models\MenuManagement\Traits\Attribute\Attribute;
use App\Models\MenuManagement\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menus extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "menus";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
       'name',
       'domain_id',
       'cmspage_id',
       'parent_menu_id',
       'status',
       'created_by'
    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}
