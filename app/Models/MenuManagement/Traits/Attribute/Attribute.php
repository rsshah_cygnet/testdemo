<?php 
namespace App\Models\MenuManagement\Traits\Attribute;

/**
 * Trait Attribute
 *
 * @author Justin Bevan justin@smokerschoiceusa.com
 */

use File;

trait Attribute
{
 	/**
 	 * @return string
 	 */
 	public function getEditButtonAttribute()
 	{
 	    if (access()->allow('edit-cmspage')) {
 	        return '<a href="javascript:void(0)" data-id="'.$this->id.'" class="edit-form grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
 	    }

 	    return '';
 	}

 	/**
 	 * @return string
 	 */
 	public function getDeleteButtonAttribute()
 	{
 	    //Can't delete master admin role
 	    // if ($this->id != 1) {
 	        if (access()->allow('delete-cmspage')) {
 	            return '<a href="' . route('admin.menu-management.destroy', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
 	        }
 	    // }

 	    return '';
 	}

 	/**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 'InActive':
                if (access()->allow('reactivate-cmspage')) {
                	return '<span class="grid-icon"><i data-url="' . route('admin.menu-management.mark', [$this->id, 'Active']) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.activate') . '"></i></span>';
                }

                break;

            case 'Active':
                if (access()->allow('deactivate-cmspage')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.menu-management.mark', [$this->id, 'InActive']) . '"  class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.deactivate') . '"></i></span>';
                }

                break;

            default:
                return '';
                // No break
        }

        return '';
    }

 	/**
 	 * @return string
 	 */
 	public function getActionButtonsAttribute()
 	{
 	    return $this->getEditButtonAttribute() .
 	    $this->getDeleteButtonAttribute() .
 	    $this->getStatusButtonAttribute();
 	}  
}