<?php 
namespace App\Models\MenuManagement\Traits\Relationship;

/**
 * Trait Relationship
 *
 * @author Anuj Jaha ayjaha@cygnet-infotech.com
 */

use App\Models\Domain\Domain;
use App\Models\Cmspage\Cmspage;
use App\Models\Access\User\User;

trait Relationship
{
	/**
	 * Relationship Mapping for Website
	 * 
	 * @return mixed
	 */
	public function website()
	{
		return $this->belongsTO(domains::class, 'domain_id');
	}

	/**
	 * Relationship Mapping for Content Page
	 * 
	 * @return mixed
	 */
	public function contentPage()
	{
		return $this->belongsTO(Cmspage::class, 'cmspage_id');
	}

	/**
	 * Relationship Mapping for Account
	 * 
	 * @return mixed
	 */
	public function users()
	{
		return $this->belongsTO(User::class, 'created_by');
	}

}	