<?php

namespace App\Models\Access\User\Traits\Attribute;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait UserAttribute {

	/**
	 * @return mixed
	 */
	public function canChangeEmail() {
		return config('access.users.change_email');
	}

	/**
	 * @return bool
	 */
	public function canChangePassword() {
		return !app('session')->has(config('access.socialite_session_name'));
	}

	/**
	 * @return string
	 */
	public function getStatusLabelAttribute() {
		if ($this->isStatus()) {
			return "<label class='label label-success'>" . trans('labels.general.yes') . "</label>";
		}

		return "<label class='label label-danger'>" . trans('labels.general.no') . "</label>";
	}

	/**
	 * @return bool
	 */
	public function isStatus() {
		return $this->status == 1;
	}

	/**
	 * @return string
	 */
	public function getConfirmedLabelAttribute() {
		if ($this->isConfirmed()) {
			return "<label class='label label-success'>" . trans('labels.general.yes') . "</label>";
		}

		return "<label class='label label-danger'>" . trans('labels.general.no') . "</label>";
	}

	/**
	 * @return mixed
	 */
	public function getPictureAttribute() {
		$user = access()->user();
		$avatar = !empty($user->avatar) ? 'images/profile-picture/' . $user->id . '/' . $user->avatar : 'images/profile-picture/no_profile.gif';

		//return gravatar()->get($this->email, ['size' => 50]);
		//return asset('images/profile-picture/no_profile.gif');
		return asset($avatar);
	}

	/**
	 * @param $provider
	 * @return bool
	 */
	public function hasProvider($provider) {
		foreach ($this->providers as $p) {
			if ($p->provider == $provider) {
				return true;
			}

		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function isActive() {
		return $this->status == 1;
	}

	/**
	 * @return bool
	 */
	public function isConfirmed() {
		return $this->confirmed == 1;
	}

	/**
	 * @return string
	 */
	public function getEditButtonAttribute() {
		if (access()->allow('edit-users')) {
			if (strpos($_SERVER["HTTP_REFERER"], 'admin/customer') !== false) {
				return '<a href="' . route('admin.customer.edit', $this->id) . '" class="grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
			}

			return '<a href="' . route('admin.access.users.edit', $this->id) . '" class="grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getChangePasswordButtonAttribute() {
		if (access()->allow('change-user-password')) {
			return '<a href="' . route('admin.access.user.change-password', $this->id) . '" class="grid-icon"><i class="fa fa-sign-in" data-placement="top" title="' . trans('buttons.general.crud.change_password') . '"></i></a>';
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getStatusButtonAttribute() {
		switch ($this->status) {
		case 0:
			if (access()->allow('reactivate-users')) {
				return '<span class="grid-icon"><i data-url="' . route('admin.access.user.mark', [$this->id, 1]) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.activate') . '"></i></span>';
			}

			break;

		case 1:
			if (access()->allow('deactivate-users')) {
				return '<span class="grid-icon"><i data-url="' . route('admin.access.user.mark', [$this->id, 0]) . '" class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.deactivate') . '"></i></span>';
			}

			break;

		default:
			return '';
			// No break
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getConfirmedButtonAttribute() {
		if (!$this->isConfirmed()) {
			if (access()->allow('resend-user-confirmation-email')) {
				return '<a href="' . route('admin.account.confirm.resend', $this->id) . '" class="grid-icon"><i class="fa fa-sign-in" data-placement="top" title=' . trans('buttons.backend.access.users.resend_email') . '"></i></a>';
			}
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getDeleteButtonAttribute() {
		//if (access()->allow('delete-users')) {
			return '<a href="' . route('admin.access.users.destroy', $this->id) . '" data-method="delete" title="Delete" class="grid-icon"><i class="fa fa-trash" data-placement="top" ></i></a>';
		//}
		//return '';
	}

	/**
	 * @return string
	 */
	public function getActionButtonsAttribute() {
		return $this->getEditButtonAttribute() .
		$this->getDeleteButtonAttribute().
		$this->getStatusButtonAttribute().
		$this->getChangePasswordButtonAttribute();
//        $this->getConfirmedButtonAttribute() .
		       ;
	}
}
