<?php

namespace App\Models\Cmspage\Traits\Attribute;

/**
 * Class CmspageAttribute
 * @package App\Models\Cmspage\Traits\Attribute
 */
trait Attribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (access()->allow('edit-cms-pages')) {
            return '<a href="' . route('admin.cmspages.edit', $this->id) . '" class="grid-icon"><i class="fa fa-pencil" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        //Can't delete master admin role
        // if ($this->id != 1) {
            if (access()->allow('delete-cms-pages')) {
                return '<a href="' . route('admin.cmspages.destroy', $this->id) . '" class="grid-icon" data-method="delete"><i class="fa fa-trash" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
            }
        // }

        return '';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->is_active) {
            case 'Inactive':
                if (access()->allow('reactivate-cms-pages')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.cmspages.mark', [$this->id, 'Active']) . '" class="fa fa-remove active-record" data-placement="top" title="' . trans('buttons.backend.activate') . '"></i></span>';
                }

                break;

            case 'Active':
                if (access()->allow('deactivate-cms-pages')) {
                    return '<span class="grid-icon"><i data-url="' . route('admin.cmspages.mark', [$this->id, 'Inactive']) . '"  class="fa fa-check active-record" data-placement="top" title="' . trans('buttons.backend.deactivate') . '"></i></span>';
                }

                break;

            default:
                return '';
                // No break
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getEditButtonAttribute() .
        $this->getDeleteButtonAttribute() ;
        //$this->getStatusButtonAttribute();
    }
}
