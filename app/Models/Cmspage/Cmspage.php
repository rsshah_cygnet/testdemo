<?php

namespace App\Models\Cmspage;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cmspage\Traits\Attribute\Attribute;
use App\Models\BaseModel;

/**
 * Class Cmspage
 * @package App\Models
 */

class Cmspage extends Model {

    use Attribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable fileds for cmspage
     * @var array
     */
    protected $fillable = [
        'title',
        'page_slug',
        'description',
        'domain_id',
        'cannonical_link',
        'seo_title',
        'seo_keyword',
        'seo_description',
        'page_order',
        'is_active'
    ];

    /**
     *
     */
    public function __construct() {
        $this->table = config('backend.cmspage_table');
    }

}
