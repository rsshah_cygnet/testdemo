<?php namespace App\Models\TuteeBasicPreference;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\TuteeBasicPreference\Traits\Attribute\Attribute;
use App\Models\TuteeBasicPreference\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class TuteeBasicPreference extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "tutee_basic_preference";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
          'id',
       

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}