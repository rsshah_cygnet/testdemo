<?php

namespace App\Models\Usertoken;

use Illuminate\Database\Eloquent\Model;
use App\Models\Usertoken\Traits\Relationship\UsertokenRelationship;

class Usertoken extends Model {

    /**
     * use Replationship
     */
    use UsertokenRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;    

    /**
     * [__construct description]
     */
    public function __construct() {
        $this->table = config('backend.user_token_table');
    }

}
