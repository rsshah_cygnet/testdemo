<?php

namespace App\Models\Usertoken\Traits\Relationship;

/**
 * Class UsertokenRelationship
 * @package App\Models\Labs\Traits\Relationship
 */
trait UsertokenRelationship {

    /**
     * Product - Usertoken relation
     */
    public function product() {
        return $this->belongsTo('App\Models\Access\User\User');
    }

}
