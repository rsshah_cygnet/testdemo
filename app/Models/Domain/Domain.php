<?php

namespace App\Models\Domain;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\Domain\Traits\Relationship\Relationship;

class Domain extends BaseModel
{
    use Relationship;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['domain'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.domains_table');
    }
}
