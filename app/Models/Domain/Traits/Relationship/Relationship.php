<?php namespace App\Models\Domain\Traits\Relationship;


/**
 * Trait Relationship
 *
 * @author Vaishal Gandhi vhgandhi@cygnet-infotech.com
 */

use App\Models\Cmspage\Cmspage;

trait Relationship
{
	
	/**
	 * Relationship Mapping for Cmspage
	 * 
	 * @return mixed
	 */
	public function cmspage()
	{
		return $this->hasMany(Cmspage::class);
	}


}	