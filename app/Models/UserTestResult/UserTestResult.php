<?php namespace App\Models\UserTestResult;

/**
 * Class Frontusers
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\UserTestResult\Traits\Attribute\Attribute;
use App\Models\UserTestResult\Traits\Relationship\Relationship;
  
class UserTestResult extends BaseModel
{
    use Attribute, Relationship;

    /**
     * Database Table
     *
     */
    protected $table = "tutor_details";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
          'id',
       

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}