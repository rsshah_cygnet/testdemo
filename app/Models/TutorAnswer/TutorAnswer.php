<?php namespace App\Models\TutorAnswer;

/**
 * Class TutorAnswer
 *
 */

use App\Models\BaseModel;
use App\Models\TutorAnswer\Traits\Attribute\Attribute;
use App\Models\TutorAnswer\Traits\Relationship\Relationship;
  
class TutorAnswer extends BaseModel
{
    use Attribute, Relationship;

    /**
     * Database Table
     *
     */
    protected $table = "tutor_answer";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
      

    ];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}