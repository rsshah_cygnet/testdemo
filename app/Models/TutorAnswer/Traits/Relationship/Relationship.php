<?php namespace App\Models\TutorAnswer\Traits\Relationship;

/**
 * Trait Relationship
 *
 */
use App\Models\Question\Question;

trait Relationship
{
	public function Que()
    {
        return $this->belongsTO(Question::class, 'question_id');
    }
}