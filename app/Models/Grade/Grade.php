<?php namespace App\Models\Grade;

/**
 * Class Levels
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\Grade\Traits\Attribute\Attribute;
use App\Models\Grade\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class Grade extends BaseModel
{
    
    use Attribute, Relationship, SoftDeletes;

    /**
    /**
     * Database Table
     *
     */
    protected $table = "grade";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];

    /*
     * Get All Grad Name
     */
    public static function getGradeList()
    {
      return Grade::where('status',1)->pluck('grade_name','id')->all();
    }
}