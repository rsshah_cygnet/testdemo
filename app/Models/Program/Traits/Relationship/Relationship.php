<?php namespace App\Models\Program\Traits\Relationship;

/**
 * Trait Relationship
 *
 */
use App\Models\Access\User\User;

trait Relationship
{
	/**
	 * Relationship Mapping for Account
	 * 
	 * @return mixed
	 */
	public function users()
	{
		return $this->belongsTO(User::class, 'modified_by');
	}
}