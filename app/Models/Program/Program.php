<?php namespace App\Models\Program;

/**
 * Class Levels
 *
 * @author Sudhir Virpara
 */

use App\Models\BaseModel;
use App\Models\Program\Traits\Attribute\Attribute;
use App\Models\Program\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class Program extends BaseModel
{
    use Attribute, Relationship, SoftDeletes;

    /**
     * Database Table
     *
     */
    protected $table = "program";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [];

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];

    /*
     * Get All Program Name
     */
    public static function getProgramList()
    {
      return Program::where('status',1)->pluck('program_name','id')->all();
    }
}