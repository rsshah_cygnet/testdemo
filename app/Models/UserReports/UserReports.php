<?php

namespace App\Models\UserReports;

use App\Models\UserReports\Traits\Attribute\Attribute;
use App\Models\UserReports\Traits\Relationship\Relationship;
use Illuminate\Database\Eloquent\Model;

class UserReports extends Model {
	use Attribute, Relationship;

	/**
	 * Database Table
	 *
	 */
	protected $table = "user_reports";

	/**
	 * Fillable Database Fields
	 *
	 */
	// protected $fillable = [
	// 	'id',

	// ];

	/**
	 * Guarded ID Column
	 *
	 */
	protected $guarded = ["id"];
}
