<?php

namespace App\Services\Access\Traits;

use App\Http\Requests\Frontend\Auth\FrontendUserMailRequest;
use App\Http\Requests\Frontend\Auth\FrontendUserResetLinkEmailRequest;
use App\Http\Requests\Frontend\Auth\ResetPasswordRequest;
use App\Http\Requests\Frontend\Auth\SendResetLinkEmailRequest;
use App\Models\Access\User\User;
use App\Models\FrontendUserPasswordReset\FrontendUserPasswordReset;
use App\Models\Frontusers\Frontusers;
use Illuminate\Support\Facades\Password;
use Mail;

/**
 * Class ResetsPasswords
 * @package App\Services\Access\Traits
 */
trait ResetsPasswords {
	use RedirectsUsers;
	public $email;
	public $username;
	//public $link;

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showLinkRequestForm() {
		return view('frontend.auth.passwords.email');
	}

	/**
	 * @param SendResetLinkEmailRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function sendResetLinkEmail(FrontendUserResetLinkEmailRequest $request) {
		$token = time() . str_random(20);

		$this->email = $request->email;

		// // dd($v);
		// if ($v->fails()) {
		// 	return redirect()->back();
		// }
		$User = Frontusers::where('email', $request->get('email'))->first();

		$this->link = URL('/') . '/password/reset/' . $token;
		// dd(\URL::route);
		if ($User != null) {
			$save = FrontendUserPasswordReset::insert([
				'email' => $this->email,
				'token' => $token,
			]);

			$mail = Mail::raw('Your password reset link is : ' . $this->link, function ($message) {
				$message->from('tuteme@gmail.com', 'Tuteme');
				$message->to($this->email)->subject('Your Password Reset Link');
			});
			if ($mail != 1) {
				return redirect()->back()->withErrors(['email' => trans($response)]);
			} else {
				return redirect()->back()->with('status', trans('strings.emails.auth.status'))->with('email', $request->email);
			}

			// Above condition is to check logging user is customer or not, bcz we are using single Auth functionality

			// $response = Password::sendResetLink($request->only('email'), function (Message $message) {
			// 	$message->subject(trans('strings.emails.auth.password_reset_subject'));
			// });
			// dd($response);
			// switch ($response) {
			// case Password::RESET_LINK_SENT:
			// 	return redirect()->back()->with('status', trans('strings.emails.auth.status'))->with('email', $request->email);

			// case Password::INVALID_USER:
			// 	return redirect()->back()->withErrors(['email' => trans($response)]);
			// }
		} else {
			return redirect()->back()->with('message', trans('strings.emails.auth.message'));
		}
	}

	/**
	 * @param null $token
	 * @return $this
	 */
	public function showResetForm($token = null) {
		if (is_null($token)) {
			return $this->showLinkRequestForm();
		}

		return view('frontend.auth.passwords.reset')
			->with('token', $token);
	}

	/**
	 * @param ResetPasswordRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function reset(ResetPasswordRequest $request) {
		$uri_token = $request->token;

		$credentials = $request->only(
			'email', 'password', 'password_confirmation', 'token'
		);
		// dd($credentials['email']);
		$user = Frontusers::where('email', $credentials['email'])->first();
		// dd($user);
		if ($user != null) {

			$tokenCheck = FrontendUserPasswordReset::where('token', $uri_token)->where('email', $credentials['email'])->first();

			if ($tokenCheck != null) {
				//Check expire token or not - set time to 30 mins
				$currentDate = strtotime($tokenCheck->created_at);
				$expire_stamp = $currentDate + (60 * 30); // (second * min)
				$future_date = date("Y-m-d H:i:s", $expire_stamp);
				$results = \DB::select(\DB::raw('SELECT NOW() AS now_time'));
				$serverTime = $results[0]->now_time;

				if ($serverTime > $future_date) {
					$removetoken = FrontendUserPasswordReset::where('email', $credentials['email'])->delete();
					return redirect()->back()
						->withInput($request->only('email'))
						->withErrors(['email' => trans('strings.emails.auth.token')]);
				} else {
					$user->password = md5($credentials['password']);
					$response = $user->save();
					$removetoken = FrontendUserPasswordReset::where('email', $credentials['email'])->delete();
					return redirect('/')->with('status', trans('strings.emails.auth.passwrod_success_message'));
				}
			} else {
				return redirect()->back()
					->withInput($request->only('email'))
					->withErrors(['email' => trans('strings.emails.auth.token')]);
			}
		} else {
			return redirect()->back()
				->withInput($request->only('email'))
				->withErrors(['email' => trans('strings.emails.auth.email_not_found')]);
		}

		$response = Password::reset($credentials, function ($user, $password) {
			$this->resetPassword($user, $password);
		});
		switch ($response) {
		case Password::PASSWORD_RESET:
			return redirect($this->redirectPath())->with('status', trans($response));

		default:
			return redirect()->back()
				->withInput($request->only('email'))
				->withErrors(['email' => trans($response)]);
		}
	}

	/**
	 * @param $user
	 * @param $password
	 */
	protected function resetPassword($user, $password) {
		$user->password = bcrypt($password);
		$user->save();
		return redirect('admin.auth.login')->withFlashSuccess("Password Reset Successfully");
	}

	//Username
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showUsernameRequestForm() {
		return view('frontend.auth.emails.reset');
	}

	/**
	 * @param SendResetLinkEmailRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	// public $email;
	// public $username;
	public function sendUsernameEmail(FrontendUserMailRequest $request) {
		//dd($request->all());

		// Below condition is to check logging user is customer or not, bcz we are using single Auth functionality
		$User = Frontusers::where('email', $request->get('email'))->first();
		if ($User != null) {
			$this->username = $User->username;
			$this->email = $User->email;

			// Above condition is to check logging user is customer or not, bcz we are using single Auth functionality
			Mail::raw('Your username is : ' . $this->username, function ($message) {
				$message->from('tuteme@gmail.com', 'Tuteme');
				$message->to($this->email, $this->username)->subject('Your Username Request');
			});

			return redirect()->back()->with('status', trans('strings.emails.auth.email_message'))
				->with('email', $this->email);
		} else {
			return redirect()->back()->with('message', trans('strings.emails.auth.email_not_found'));
		}
	}
}
