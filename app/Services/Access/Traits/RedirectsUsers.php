<?php
namespace App\Services\Access\Traits;

trait RedirectsUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        /**
         * Check logged in user is Admin or other
         */
       
        $this->redirectTo = "/admin/login";
        
        return property_exists($this, 'redirectTo') ? $this->redirectTo : $this->redirectTo;
    }
}