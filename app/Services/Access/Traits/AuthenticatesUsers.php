<?php

namespace App\Services\Access\Traits;

use App\Events\Frontend\Auth\UserLoggedIn;
use App\Exceptions\GeneralException;
use App\Http\Requests\Frontend\Auth\LoginRequest;
use App\Models\Access\User\User;
use App\Models\Frontusers\Frontusers;
use App\Models\Timezone\Timezone;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use App\Models\Notification\Notification;
use Carbon\Carbon;
use DB;
/**
 * Class AuthenticatesUsers
 * @package App\Services\Access\Traits
 */
trait AuthenticatesUsers {

	use RedirectsUsers;

	/**
	 * GET METHOD - It will show login form for frontend
	 * @return mixed
	 */
	public function showLoginForm(Request $request) {
		$front_user_role = \Session::get('front_user_role');
		if (isset($front_user_role) && $front_user_role != 0 && $front_user_role != "" && $front_user_role == 2) {
			return redirect()->route('frontend.tuteeDashboard');
		} else if (isset($front_user_role) && $front_user_role != 0 && $front_user_role != "") {
			return redirect()->route('frontend.tutorDashboard');
		}

		$cookieData = $request->cookie('loginData');
		//echo 'Front'; exit;
		return view('frontend.auth.login', compact('cookieData'))
			->withSocialiteLinks($this->getSocialLinks());
	}

	public function fbLogin(Request $request){
		$data = $request->all();
        if(!empty($data)){
            if(!empty($data['dob'])){
                $data['dob'] =  date("Y-m-d", strtotime($data['dob']));    
            }
            $email = isset($data['email'])?$data['email']:"";
            if(empty($email)){
            	 $array = array('status' => 'error');
                return json_encode($array);
            }
            $user_data = Frontusers::select('*')
                        ->where('email',$email)
                        ->where('sign_with',1)
                        ->whereNull('deleted_at')
                        ->get()->toArray();


        if(!empty($user_data)){
            $first_name = isset($user_data['0']['first_name'])?$user_data['0']['first_name']:"";
            $last_name = isset($user_data['0']['last_name'])?$user_data['0']['last_name']:"";
            $id = isset($user_data['0']['id'])?$user_data['0']['id']:"";
            $role = isset($user_data['0']['role'])?$user_data['0']['role']:"";
            $email = isset($user_data['0']['email'])?$user_data['0']['email']:"";
            $country_id = isset($user_data['0']['country_id'])?$user_data['0']['country_id']:"";
            $state_id = isset($user_data['0']['state_id'])?$user_data['0']['state_id']:"";
            $city_id = isset($user_data['0']['city_id'])?$user_data['0']['city_id']:"";
            $nationality = isset($user_data['0']['nationality'])?$user_data['0']['nationality']:"";
            $lang_id = isset($user_data['0']['lang_id'])?$user_data['0']['lang_id']:"";
            $photo = isset($user_data['0']['photo'])?$user_data['0']['photo']:"";
            $timezone_id = isset($user_data['0']['timezone_id'])?$user_data['0']['timezone_id']:"";
            
            $timezone_name = getTimezoneName($id);


                \Session::set('front_user_id', $id);
                \Session::set('front_user_role', $role);
                \Session::set('front_user_first_name', $first_name);
                \Session::set('front_user_last_name', $last_name);
                \Session::set('front_user_email', $email);
                \Session::set('front_user_country_id', $country_id);
                \Session::set('front_user_state_id', $state_id);
                \Session::set('front_user_city_id', $city_id);
                \Session::set('front_user_nationality', $nationality);
                \Session::set('front_user_lang_id', $lang_id);
                \Session::set('front_user_photo', $photo);
                \Session::set('front_user_timezone_id', $timezone_id);
                \Session::set('front_user_timezone_name', $timezone_name);

                 $array = array('status' => 'success','role' => $role);
                 return json_encode($array);
            }   


            $obj  = new Frontusers;
            $obj->first_name  = isset($data['first_name']) ? $data['first_name'] : '';
            $obj->last_name   = isset($data['last_name']) ? $data['last_name'] : '';
            $obj->gender      = isset($data['gender']) ? $data['gender'] : '';
            $obj->email       = isset($data['email']) ? $data['email'] : '';
            $obj->dob         = isset($data['dob']) ? ($data['dob']) : '';
            $obj->timezone_id = 572;
            $obj->role        = 1;
            $obj->status      = 1;
            $obj->sign_with   = 1;
            
            if ($obj->save()) {
                $user_id = $obj->id;
                 $timezone_name = getTimezoneName($obj->id);

                \Session::set('front_user_id', $user_id);
                \Session::set('front_user_role', $obj->role);
                \Session::set('front_user_first_name', $obj->first_name);
                \Session::set('front_user_last_name', $obj->last_name);
                \Session::set('front_user_email', $obj->email);
                \Session::set('front_user_timezone_id', $obj->timezone_id);
                \Session::set('front_user_timezone_name', $timezone_name);
              
                $array = array('status' => 'success','role' => $obj->role);
                return json_encode($array);
            }
        }

	}

	/**
	 * POST METHOD - It will login user at frontend
	 * @param LoginRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function login(LoginRequest $request) {

		$remember = $request->remember ? true : false;

		//echo 'Front'; exit;
		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		$throttles = in_array(
			ThrottlesLogins::class, class_uses_recursive(get_class($this))
		);

		if ($throttles && $this->hasTooManyLoginAttempts($request)) {
			return $this->sendLockoutResponse($request);
		}

		// if (\Hash::check($request->password, '=', '$2y$10$2BMcTU3AxoLu3ToyW4WBZ.OMBW/Ve0ZT4rpXytMXr4F7HwkxKUxXG'))
		// {   dd("check");
		//             // $user = $user->toArray();
		// }
		$User = Frontusers::where('email', $request->get('email'))->where('password', md5($request->get('password')))->first();
		
		if(isset($User->status) && $User->status == 0){
			return redirect()->back()->withErrors([
				$this->loginUsername() => 'Please activate your account and try again'
			]);
		}

		if ($User == null) {
			return redirect()->back()->withErrors([
				$this->loginUsername() => trans('auth.failed'),
			]);
		}
		$timezone_data = Timezone::where('id', $User->timezone_id)->get()->first();
		$timezone_name = $timezone_data->timezone_name;
		$details_url = isset($_COOKIE['tutor_details_url']) ? $_COOKIE['tutor_details_url'] : "";
		if ($remember == true && (!empty($User))) {
			$login = $request->email;
			$login1 = $request->password;
			// $minutes = 525600;
			$ar = [$login, $login1];
			$response = new Response();
			// $response->withCookie(cookie('loginData', $ar, $minutes));
			//return $response;
			// return redirect('/')->withCookie($response);
			// return view('frontend.auth.success');
			
			\Session::set('front_user_id', $User->id);
			\Session::set('front_user_role', $User->role);
			\Session::set('front_user_first_name', $User->first_name);
			\Session::set('front_user_last_name', $User->last_name);
			\Session::set('front_user_username', $User->username);
			\Session::set('front_user_email', $User->email);
			\Session::set('front_user_country_id', $User->country_id);
			\Session::set('front_user_state_id', $User->state_id);
			\Session::set('front_user_city_id', $User->city_id);
			\Session::set('front_user_nationality', $User->nationality);
			\Session::set('front_user_lang_id', $User->lang_id);
			\Session::set('front_user_photo', $User->photo);
			\Session::set('front_user_timezone_id', $User->timezone_id);
			\Session::set('front_user_timezone_name', $timezone_name);

			/* return response()->view('frontend.auth.success')
            ->withCookie(cookie::forever('loginData', $ar));*/
            // Redirect to test page starts
            if (isset($User->role) && $User->role != 0 && ($User->role == 1 || $User->role == 3)){
	            if(!empty(\Session::get('tutor_test_link'))){
	            	return redirect(\Session::get('tutor_test_link'));
	            }
        	}

        		$this->sendFirstTimeLoginNotification($User);


            // Redirect to test page ends
			if (isset($details_url) && $details_url != "") {
				return redirect($details_url)->withCookie(cookie::forever('loginData', $ar));
			}
			if (isset($User->role) && $User->role != 0 && $User->role == 2) {
				return redirect()->route('frontend.tuteeDashboard')->withCookie(cookie::forever('loginData', $ar));
			} else {
				return redirect()->route('frontend.tutorDashboard')->withCookie(cookie::forever('loginData', $ar));
			}
		}

		if (!empty($User)) {
			\Session::set('front_user_id', $User->id);
			\Session::set('front_user_role', $User->role);
			\Session::set('front_user_first_name', $User->first_name);
			\Session::set('front_user_last_name', $User->last_name);
			\Session::set('front_user_username', $User->username);
			\Session::set('front_user_email', $User->email);
			\Session::set('front_user_country_id', $User->country_id);
			\Session::set('front_user_state_id', $User->state_id);
			\Session::set('front_user_city_id', $User->city_id);
			\Session::set('front_user_nationality', $User->nationality);
			\Session::set('front_user_lang_id', $User->lang_id);
			\Session::set('front_user_photo', $User->photo);
			\Session::set('front_user_timezone_id', $User->timezone_id);
			\Session::set('front_user_timezone_name', $timezone_name);

			// Redirect to test page starts
			if (isset($User->role) && $User->role != 0 && ($User->role == 1 || $User->role == 3)){
	            if(!empty(\Session::get('tutor_test_link'))){
	            	return redirect(\Session::get('tutor_test_link'));
	            }
        	}


        	$this->sendFirstTimeLoginNotification($User);

            // Redirect to test page ends
			if (isset($details_url) && $details_url != "") {
				return redirect($details_url);
			}
			if (isset($User->role) && $User->role != 0 && $User->role == 2) {
				return redirect()->route('frontend.tuteeDashboard');
			} else {
				return redirect()->route('frontend.tutorDashboard');
			}

		}

		//bcrypt() for encription
		if ($throttles) {
			$this->incrementLoginAttempts($request);
		}
		return redirect()->back()
			->withInput($request->only($this->loginUsername(), $remember))
			->withErrors([
				$this->loginUsername() => trans('auth.failed'),
			]);

	}


	public function sendFirstTimeLoginNotification($User){
		$user_fullname = $User->first_name." ".$User->last_name;
        	/*First Time login notification start*/
        	if(isset($User->login_first_time) && $User->login_first_time == 0){
        		
			    if(isset($User->role) && $User->role == 2){
	        		$message      = "Hi <b>" . $user_fullname . "</b>, Please add payment method before reserving a session.";
			        $Notification = Notification::insert([
			            'from_user_id'         => 0,
			            'to_user_id'           => $User->id,
			            'notification_type_id' => 24,
			            'is_read'              => 0,
			            'is_read_admin'        => 0,
			            'is_for_admin'         => 0,
			            'created_at'           => Carbon::now()->toDateTimestring(),
			            'message'              => $message,
			        ]);
	        	}   

	        	if(isset($User->role) && $User->role == 1){
	        		$message      = "Hi <b>" . $user_fullname . "</b>, Please add payout method.";
			        $Notification = Notification::insert([
			            'from_user_id'         => 0,
			            'to_user_id'           => $User->id,
			            'notification_type_id' => 27,
			            'is_read'              => 0,
			            'is_read_admin'        => 0,
			            'is_for_admin'         => 0,
			            'created_at'           => Carbon::now()->toDateTimestring(),
			            'message'              => $message,
			        ]);

			        $message = "<a href='/tutor-availablity'>Hi <b>" . $user_fullname . "</b>, Please set your availability.</a>";
			        $Notification = Notification::insert([
			            'from_user_id'         => 0,
			            'to_user_id'           => $User->id,
			            'notification_type_id' => 26,
			            'is_read'              => 0,
			            'is_read_admin'        => 0,
			            'is_for_admin'         => 0,
			            'created_at'           => Carbon::now()->toDateTimestring(),
			            'message'              => $message,
			        ]);

			        $message = "<a href='/tutor/mytest'>Hi <b>" . $user_fullname . "</b>, Please add at least one speciality and pass it’s quality exam.</a>";
			        $Notification = Notification::insert([
			            'from_user_id'         => 0,
			            'to_user_id'           => $User->id,
			            'notification_type_id' => 25,
			            'is_read'              => 0,
			            'is_read_admin'        => 0,
			            'is_for_admin'         => 0,
			            'created_at'           => Carbon::now()->toDateTimestring(),
			            'message'              => $message,
			        ]);
	        	}   

	        	$message      = "<a href='/profile'>Hi <b>" . $user_fullname . "</b>, Please complete your profile first.</a>";
		        $Notification = Notification::insert([
		            'from_user_id'         => 0,
		            'to_user_id'           => $User->id,
		            'notification_type_id' => 23,
		            'is_read'              => 0,
		            'is_read_admin'        => 0,
		            'is_for_admin'         => 0,
		            'created_at'           => Carbon::now()->toDateTimestring(),
		            'message'              => $message,
		        ]);
			    DB::table('front_user')
			        ->where('id', $User->id)
			        ->limit(1)
			        ->update(array('login_first_time' => 1));
        	}


        	
        	/*First Time login notification end*/
	}

	/**
	 * GET METHOD - It will logour user from frontend
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function logout() {
		//echo 'Front'; exit;
		/**
		 * Remove the socialite session variable if exists
		 */
		if (app('session')->has(config('access.socialite_session_name'))) {
			//app('session')->forget(config('access.socialite_session_name'));
		}

		/*event(new UserLoggedOut(access()->user()));
        auth()->logout();*/
		\Session::set('front_user_id', '');
		\Session::set('front_user_role', '');
		\Session::set('front_user_first_name', '');
		\Session::set('front_user_last_name', '');
		\Session::set('front_user_username', '');
		\Session::set('front_user_email', '');
		\Session::set('front_user_country_id', '');
		\Session::set('front_user_state_id', '');
		\Session::set('front_user_city_id', '');
		\Session::set('front_user_nationality', '');
		\Session::set('front_user_lang_id', '');
		\Session::set('front_user_photo', '');
		\Session::set('front_user_timezone_id', '');
		\Session::set('front_user_timezone_name', '');
		\Session::set('current_role', '');

		return redirect('/login');
		/* return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');*/
	}

	/**
	 * This is here so we can use the default Laravel ThrottlesLogins trait
	 *
	 * @return string
	 */
	public function loginUsername() {
		return 'email';
	}

	/**
	 * @param Request $request
	 * @param $throttles
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws GeneralException
	 */
	protected function handleUserWasAuthenticated(Request $request, $throttles) {
		if ($throttles) {
			$this->clearLoginAttempts($request);
		}

		/**
		 * Check to see if the users account is confirmed and active
		 */
		if (!access()->user()->isConfirmed()) {
			$token = access()->user()->confirmation_code;
			auth()->logout();
			throw new GeneralException(trans('exceptions.frontend.auth.confirmation.resend', ['token' => $token]));
		} elseif (!access()->user()->isActive()) {
			auth()->logout();
			throw new GeneralException(trans('exceptions.frontend.auth.deactivated'));
		}

		event(new UserLoggedIn(access()->user()));
		return redirect()->intended($this->redirectPath());
	}

}
