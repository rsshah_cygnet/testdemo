<?php

namespace App\Services\Access\Traits;

use App\includes\BigBlueButton;
use App\Models\Session\Session;
use DB;

//require_once(base_path().'/public/includes/bbb-api.php');

/**
 * Class AuthenticatesUsers
 * @package App\Services\Access\Traits
 */
trait SessionRequest
{

    /**
     * GET METHOD - It will show login form for frontend
     * @return mixed
     */
    public function generateMeetingId()
    {

        $digits        = 8;
        $random_number = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        return $random_number;

    }

    public function getTuteeSessionUrl($meetingId, $username)
    {
        $bbb        = new BigBlueButton();
        $joinParams = array(
            'meetingId'    => $meetingId, // REQUIRED - We have to know which meeting to join.
            'username'     => $username, // REQUIRED - The user display name that will show in the BBB meeting.
            'password'     => 'ap', // REQUIRED - Must match either attendee or moderator pass for meeting.
            'createTime'   => '', // OPTIONAL - string
            'userId'       => '', // OPTIONAL - string
            'webVoiceConf' => '', // OPTIONAL - string
        );

// Get the URL to join meeting:
        $itsAllGood = true;
        try { $result = $bbb->getJoinMeetingURL($joinParams);} catch (Exception $e) {
            $itsAllGood = false;
            return '';
        }

        if ($itsAllGood == true) {
            //Output results to see what we're getting:
            return $result;
        }
    }

    public static function getTutorSessionUrl($id)
    {

        $session_data = Session::select('session.*', DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
            ->leftJoin('front_user', 'session.tutor_id', '=', 'front_user.id')
            ->where('session.id', $id)
            ->first()
            ->toArray();

        $bbb        = new BigBlueButton();
        $joinParams = array(
            'meetingId'    => $session_data['meeting_id'], // REQUIRED - We have to know which meeting to join.
            'username'     => $session_data['tutor_name'], // REQUIRED - The user display name that will show in the BBB meeting.
            'password'     => 'mp', // REQUIRED - Must match either attendee or moderator pass for meeting.
            'createTime'   => '', // OPTIONAL - string
            'userId'       => '', // OPTIONAL - string
            'webVoiceConf' => '', // OPTIONAL - string
        );

// Get the URL to join meeting:
        $itsAllGood = true;
        try { $result = $bbb->getJoinMeetingURL($joinParams);} catch (Exception $e) {
            return '';
            $itsAllGood = false;
        }

        if ($itsAllGood == true) {
            //Output results to see what we're getting:
            return $result;
        }
    }

    public static function joinSessionBytutor($id)
    {
        $encrypted_id = encrypt($id);

        $session_data = Session::select('session.*', DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
            ->leftJoin('front_user', 'session.tutor_id', '=', 'front_user.id')
            ->where('session.id', $id)
            ->first()
            ->toArray();

        // Instatiate the BBB class:
        $bbb = new BigBlueButton();

        /* ___________ CREATE MEETING w/ OPTIONS ______ */
        /*
         */
        $creationParams = array(
            'meetingId'       => $session_data['meeting_id'], // REQUIRED
            'meetingName'     => 'Session', // REQUIRED
            'attendeePw'      => 'ap', // Match this value in getJoinMeetingURL() to join as attendee.
            'moderatorPw'     => 'mp', // Match this value in getJoinMeetingURL() to join as moderator.
            'welcomeMsg'      => '', // ''= use default. Change to customize.
            'dialNumber'      => '', // The main number to call into. Optional.
            'voiceBridge'     => '', // PIN to join voice. Optional.
            'webVoice'        => '', // Alphanumeric to join voice. Optional.
            'logoutUrl'       => url('/') . "/tuteeEndSession/" . $encrypted_id, // Default in bigbluebutton.properties. Optional.
            'maxParticipants' => '1', // Optional. -1 = unlimitted. Not supported in BBB. [number]
            'record'          => 'true', // New. 'true' will tell BBB to record the meeting.
            'duration'        => 60, // Default = 0 which means no set duration in minutes. [number]
            //'meta_category' => '',                // Use to pass additional info to BBB server. See API docs.
        );

        //print_r($creationParams);die;

        // Create the meeting and get back a response:
        $itsAllGood = true;
        try {
            $result = $bbb->createMeetingWithXmlResponseArray($creationParams);
        } catch (Exception $e) {
            $data['error'] = "";
            $itsAllGood    = false;
        }

        if ($itsAllGood == true) {
            // If it's all good, then we've interfaced with our BBB php api OK:
            if ($result == null) {
                // If we get a null response, then we're not getting any XML back from BBB.
                $data['error'] = "Failed to get any response. Something went wrong";
            } else {
                // We got an XML response, so let's see what it says:

                if ($result['returncode'] == 'SUCCESS') {
                    // Then do stuff ...
                    $joinParams = array(
                        'meetingId'    => $result['meetingId'], // REQUIRED - We have to know which meeting to join.
                        'username'     => $session_data['tutor_name'], // REQUIRED - The user display name that will show in the BBB meeting.
                        'password'     => 'mp', // REQUIRED - Must match either attendee or moderator pass for meeting.
                        'createTime'   => '', // OPTIONAL - string
                        'userId'       => '', // OPTIONAL - string
                        'webVoiceConf' => '', // OPTIONAL - string
                    );

                    // Get the URL to join meeting:
                    $itsAllGood = true;
                    try {
                        $result = $bbb->getJoinMeetingURL($joinParams);
                    } catch (Exception $e) {
                        $data['error'] = '';
                        $itsAllGood    = false;
                    }
                    if ($itsAllGood == true) {
                        $data['link'] = $result;
                        return $data;
                        //Output results to see what we're getting:
                    }
                } else {
                    $data['error'] = 'Meeting creation failed';
                }
            }
        }

    }

    public static function isMeetingRunning($id)
    {
        $bbb          = new BigBlueButton();
        $session_data = Session::select('session.*', DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
            ->leftJoin('front_user', 'session.tutor_id', '=', 'front_user.id')
            ->where('session.id', $id)
            ->first()
            ->toArray();

        $meetingId = $session_data['meeting_id'];

        // Get the URL to join meeting:
        $itsAllGood = true;
        try {
            $result = $bbb->isMeetingRunningWithXmlResponseArray($meetingId);
        } catch (Exception $e) {
            $data['err'] = $e->getMessage();
            return $data;
            $itsAllGood = false;
        }

        if ($itsAllGood == true) {
            $data['link'] = (json_decode(json_encode((array) $result), true));
            return $data;
        }
    }

    public function joinIfRunningForTutee($id = '')
    {

        $session_data = Session::select('session.*', DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
            ->leftJoin('front_user', 'session.tutee_id', '=', 'front_user.id')
            ->where('session.id', $id)
            ->first()
            ->toArray();

        $bbb        = new BigBlueButton();
        $meetingId  = $session_data['meeting_id'];
        $itsAllGood = true;
        try { $result = $bbb->isMeetingRunningWithXmlResponseArray($meetingId);} catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $itsAllGood = false;
        }
        if ($itsAllGood == true) {
            //Output results to see what we're getting:
            //print_r($result);
            $status = $result['running'];
            //echo "<p style='color:red;'>".$status."</p>";

            $holdMessage = '
            <div id="status">
                <p>Your meeting has not yet started. Waiting for a moderator to start the meeting...</p>
                <img src="../assets/ajax-loader.gif" alt="...contacting server..." />
                <p>You will be connected as soon as the meeting starts.</p>
            </div>
        ';

            // The meeting is not running yet so hold your horses:
            if ($status == 'false') {
                $data['err'] = $holdMessage;
                return $data;
            } else {
                //Here we redirect the user to the joinUrl.
                // For now we output this:

                $joinParams = array(
                    'meetingId'    => $meetingId, // REQUIRED - We have to know which meeting to join.
                    'username'     => $session_data['tutor_name'], // REQUIRED - The user display name that will show in the BBB meeting.
                    'password'     => 'ap', // REQUIRED - Must match either attendee or moderator pass for meeting.
                    'createTime'   => '', // OPTIONAL - string
                    'userId'       => '', // OPTIONAL - string
                    'webVoiceConf' => '', // OPTIONAL - string
                );

                // Get the URL to join meeting:
                $allGood = true;
                try { $result = $bbb->getJoinMeetingURL($joinParams);} catch (Exception $e) {
                    $pass_data['err'] = $e->getMessage();
                    return $pass_data;
                    $allGood = false;
                }

                if ($allGood == true) {
                    //Output resulting URL. Send user there...
                    $pass_data['link'] = $result;
                    return $pass_data;
                }
            }
        }
    }

    public static function endMeeting($id)
    {

        $session_data = Session::select('session.*', DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
            ->leftJoin('front_user', 'session.tutor_id', '=', 'front_user.id')
            ->where('session.id', $id)
            ->first()
            ->toArray();

        $bbb = new BigBlueButton();

        $endParams = array(
            'meetingId' => $session_data['meeting_id'], // REQUIRED - We have to know which meeting to end.
            'password'  => 'mp', // REQUIRED - Must match moderator pass for meeting.

        );

// Get the URL to end a meeting:
        $itsAllGood = true;
        try { $result = $bbb->endMeetingWithXmlResponseArray($endParams);} catch (Exception $e) {
            $data['err'] = $e->getMessage();
            return $data;
            $itsAllGood = false;
        }

        if ($itsAllGood == true) {
            // If it's all good, then we've interfaced with our BBB php api OK:
            if ($result == null) {
                $data['err'] = "Failed to get any response. Maybe we can't contact the BBB server.";
                return $data;
            } else {
                // We got an XML response, so let's see what it says:
                if ($result['returncode'] == 'SUCCESS') {
                    $data['success'] = true;
                    return $data;
                } else {
                    $data['success'] = false;
                    return $data;
                }
            }
        }
    }

    public static function getRecording($id)
    {
        $bbb = new BigBlueButton();

        $session_data = Session::select('session.*', DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
            ->leftJoin('front_user', 'session.tutor_id', '=', 'front_user.id')
            ->where('session.id', $id)
            ->first()
            ->toArray();

        $recordingsParams = array(
            'meetingId' => $session_data['meeting_id'], // OPTIONAL - comma separate if multiples
        );
        
        $itsAllGood = true;
        try { $result = $bbb->getRecordingsWithXmlResponseArray($recordingsParams);} catch (Exception $e) {
              $data['err'] = $e->getMessage();
                return $data;
            $itsAllGood = false;
        }

        if ($itsAllGood == true) {
            // If it's all good, then we've interfaced with our BBB php api OK:
            if ($result == null) {
                $data['err'] = "Failed to get any response. Maybe we can't contact the BBB server.";
                return $data;
            } else {
                return (json_decode(json_encode((array) $result), true));die;
                if ($result['returncode'] == 'SUCCESS') {
                    // Then do stuff ...
                    echo "<p>Meeting info was found on the server.</p>";
                } else {
                    echo "<p>Failed to get meeting info.</p>";
                }
            }
        }
    }

}
