<?php

namespace App\Services\Access\Traits;

use App\Exceptions\GeneralException;
use App\Http\Requests\Frontend\User\ChangePasswordRequest;
use App\Models\Notification\Notification;
use App\Models\Session\Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
/**
 * Class ChangePasswords
 * @package App\Services\Access\Traits
 */
trait ChangePasswords {

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showChangePasswordForm(Request $request) {
		if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }
		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			"current_role" => \Session::get('current_role'),
		]; //dd($sessionData);

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		$upcommingSessions = Session::select('session.*', 'front_user.first_name', 'front_user.last_name')->leftJoin('front_user', 'session.tutee_id', '=', 'front_user.id')->where('session.tutor_id', $sessionData['front_user_id'])->where('session.status', 2)->orderBy('session.id', 'desc')->limit(3)->get()->toArray();
		//  dd($upcommingSessions);
		//$date = getOnlyDate();
		

		if($request->ajax())
        {
                $returnHTML = view('frontend.auth.passwords.mid_section_change', 
                	[
                		'sessionData' => $sessionData,
                		'upcommingSessions' => $upcommingSessions,
                		'notifications' => $notifications,
                		'notificationCount' => $notificationCount, 
                		
                	])->render();
            return response()->json(array('status' => "success",'html' => $returnHTML));
        }else{
            return view('frontend.auth.passwords.change')
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('upcommingSessions', $upcommingSessions)
			->with('notificationCount', $notificationCount);
        }	

	}

	/**
	 * @param ChangePasswordRequest $request
	 * @return mixed
	 */
	public function changePassword(ChangePasswordRequest $request) {
		if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }
		
		$id = \Session::get('front_user_id');

		$user = $this->user->findFrontUser($id);
		$old_password = md5($request->old_password);

		// if (Hash::check($input['old_password'], $user->password)) {
		// 	dd('in');
		// 	$user->password = bcrypt($input['password']);
		// 	return $user->save();
		// }
		// dd($input['password']);
		if ($old_password == $user->password) {
			$user->password = md5($request->password);
			$user->modified_by = $id;
			$user->updated_at = carbon::now()->toDateTimeString();
			$save = $user->save();
			// return $user->save();
			$array = array('status' => 'success','msg' => 'Your password has been change successfully');
        	return json_encode($array);
		}else{
			$array = array('status' => 'error','msg' => 'Old password is wrong');
        	return json_encode($array);
		}

		throw new GeneralException(trans('exceptions.frontend.auth.password.change_mismatch'));
		// $this->user->changePassword($request->all());
	}
}