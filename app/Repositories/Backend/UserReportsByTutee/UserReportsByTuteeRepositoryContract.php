<?php

namespace App\Repositories\Backend\UserReportsByTutee;

/**
 * Interface UserReportsByTuteeRepositoryContract
 *
 * @author Sudhir Virpara
 * @package App\Repositories\Backend\UserReportsByTutee
 */

interface UserReportsByTuteeRepositoryContract {
	/* Create Item
		     *
		     * @param array $input
	*/
	// public function create($input);

	/**
	 * Update Item
	 *
	 * @param int $id
	 * @param array $input
	 */
	// public function update($id, $input);

	/**
	 * Destroy Item
	 *
	 * @param  $id
	 * @return mixed
	 */
	public function destroy($id);

	public function findOrThrowException($id);

	public function mark($id, $status);

}
