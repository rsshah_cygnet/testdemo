<?php

namespace App\Repositories\Backend\Blog;

/**
 * Class EloquentBlogRepository
 *
 * @author Anuj Jaha ayjaha@cygnet-infotech.com
 */

use App\Exceptions\GeneralException;
use App\Models\Blog\Blog;
use App\Repositories\Backend\Blog\BlogRepositoryContract;
use App\Repositories\Backend\DbRepository;
use App\Http\Utilities\FileUploads;


/**
 * Class EloquentBlogRepository
 *
 * @package App\Repositories\Backend\Blog
 */
class EloquentBlogRepository extends DbRepository implements BlogRepositoryContract
{
    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumn = [
        'blogs.name',
        'blogs.publish_datetime',
        'blogs.status',
        'users.first_name',
        'blogs.created_at'
    ];

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumnHeader = [
        'Title',
        'Published DateTime',
        'Status',
        'Created By',
        'Created At',
    ];

    /**
     * Grid Default Order By
     *
     * @var string
     */
    public $gridDefaultOrderBy = 'blogs.created_at';

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $relations = [
        'users'
    ];

    /**
     * Download File Name
     *
     * @var string
     */
    public $downloadfilename = "blogs";

    /**
     * Comman search variable of this repositery
     *
     * @var string
     */
    public $searchtop;


    /**
     * Status of blog Post
     * @var Array()
     */
    public $record_status = [

        'InActive',
        'Draft',
        'Published',
        'Scheduled'

    ];
    /**
     * Record Active Status
     *
     * @var string
     */
    public $statusActive = "Published";

    /**
     * Record InActive Status
     *
     * @var string
     */
    public $statusInActive = "InActive";

    /**
     * Record Draft Status
     *
     * @var string
     */
    public $statusDraft = "Draft";

    /**
     * Record Scheduled Status
     *
     * @var string
     */
    public $statusScheduled = "Scheduled";
    
    /**
     * Upload File Path
     * 
     * @var string
     */
    public $filePath = "blog_images";

    /**
     * Upload File Aliss
     * 
     * @var string
     */
    public $fileUploadAlias = "blog";

    /**
     * Upload Manager Object
     * 
     * @var object
     */
    public $uploadManager;


    /**
     * Related model of this repositery
     *
     * @var object
     */
    public $model;

    public function __construct(Blog $model)
    {
        $this->model = $model;
        $this->uploadManager    = new FileUploads;
    }

    /**
     * Create Brand Model
     *
     * @param array $input
     * @return boolean
     * @throws GeneralException
     */
    public function create($input = array())
    {
        if(count($input))
        {
            $input = $this->addCreatedByInfo($input);
            $input = $this->getRecordStatus($input);
            $input = $this->uploadImage($input);

            return $this->model->create($input);
        }

        throw new GeneralException(trans('exceptions.backend.article.create_error'));
    }

    /**
     * Add Created By Info
     *
     * @param array $input
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

     /**
     * Get Record Status
     *
     * @param array $input
     */
    public function getRecordStatus($input = array())
    {
        if(count($input))
        {
            if($input['record_status'] == 0)
            {
                $status = $this->statusInActive;
            }
            else if($input['record_status'] == 1)
            {
                $status = $this->statusDraft;
            }
            else if($input['record_status'] == 2)
            {
                $status = $this->statusActive;
            }
            else
            {
                $status = $this->statusScheduled;
            }

            $input = array_merge($input, [ 'status' => $status]);


            return $input;
        }

        return [];
    }

    /**
     * Add Updated By Info
     *
     * @param array $input
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

    /**
     * Upload Image
     *
     * @param Array $input
     * @return Array $input
     */
    public function uploadImage($input)
    {

        $avatar = $input['featured_image'];

        if(isset($input['featured_image']) && !empty($input['featured_image']))
        {
            $fileName = $this->uploadManager->setBasePath($this->filePath)
                ->setThumbnailFlag(false)
                ->upload($input['featured_image']);

            $input = array_merge($input, [ 'featured_image' => $fileName]);

            return $input;
            
        }            
    }

    /**
     * @param int $id
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function update($id = null, $input = array())
    {
        if(isset($id) && count($input))
        {
            $model = $this->findOrThrowException($id);

            $input = $this->addUpdatedByInfo($input);
            $input = $this->getRecordStatus($input);

            // dd($input['categories']);
            if(array_key_exists('featured_image',$input))
            {
                $this->deleteOldFile($id);
                $input = $this->uploadImage($input);
            }
            return $model->update($input);
        }

        throw new GeneralException(trans('exceptions.backend.article.update_error'));
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            $this->deleteOldFile($model);
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * Destroy Old Image
     *
     * @param int $id
     */
    public function deleteOldFile($model)
    {
        $fileName   = $model->featured_image;
        $filePath   = $this->uploadManager->setBasePath($this->filePath);
        $file       = $filePath->filePath.DIRECTORY_SEPARATOR.$fileName;
        
        return $this->uploadManager->deleteFile($file);
    }

    public function getSlugFromId($id)
    {
        return Blog::find($id)->pluck('slug');
    }
    /**
     * search by fields
     * @param int $per_page
     * @param boolean $active
     * @param string $order_by
     * @param string $sort
     * @param boolean $searchtop
     * @param mixed $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '')
    {
        $query = $this->_getGridJoin();

        if($other != '' && $other != "()")
        {
            $query = $query->whereRaw($other);
        }

        if ($active)
        {
            $query = $query->where('status', $active);
        }

        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);

        return $query;
    }

    /**
     * Get Count
     *
     * @param string $where
     * @return int count
     */
    public function getCount($where)
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()")
        {
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll()
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }

    /**
     * Get Grid Join
     *
     * @param object $query
     * @return object
     */
    public function _getGridJoin($query = null)
    {
        if($query)
        {
            return $query->select('blogs.*','users.first_name')
                ->leftJoin('users', 'users.id', '=', 'blogs.created_by');;
        }

        // return $this->model->select('blogs.*');
        return $this->model->select('blogs.*', 'users.first_name')
            ->leftJoin('users', 'users.id', '=', 'blogs.created_by');

    }
}
