<?php

namespace App\Repositories\Backend\Blog;

/**
 * Interface BlogRepositoryContract
 *
 * @author Viral Solani
 * @package App\Repositories\Backend\Blog
 */

interface BlogRepositoryContract
{
    /* Create Item
     *
     * @param array $input
     */
    public function create($input);

    /**
     * Update Item
     *
     * @param int $id
     * @param array $input
     */
    public function update($id, $input);

    /**
     * Destroy Item
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);
}
