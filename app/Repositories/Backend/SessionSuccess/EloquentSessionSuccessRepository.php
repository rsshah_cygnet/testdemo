<?php

namespace App\Repositories\Backend\SessionSuccess;

/**
 * Class EloquentLevelsRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\Session\Session;
use App\Repositories\Backend\SessionSuccess\SessionSuccessRepositoryContract;
use App\Repositories\Backend\DbRepository;
use DB;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentSessionSuccessRepository extends DbRepository implements SessionSuccessRepositoryContract
{
    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumn = [
        'front_user.tutor_name',
         'front_user.tutee_name',
        'session.accepted_date_time',
        'session.scheduled_date_time',
        'topic.topic_name',
        'subject.subject_name',
        'session.ratings_given_by_tutee',
        'session.admin_commission',
        'session.tutor_earning'

    ];

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumnHeader = [
        'Tutor Name',
        'Tutee Name',
        'Accepted date & time',
        'Conducted date & time',
        'Topic',
        'Subject',
        'Ratings posted by Tutee',
        'Admin commission($)',
        'Tutor earning'
    ];

    /**
     * Grid Default Order By
     *
     * @var string
     */
    public $gridDefaultOrderBy = 'session.id';

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $relations = [
        'session'
    ];

    /**
     * Download File Name
     *
     * @var string
     */
    public $downloadfilename = "session_success";

    /**
     * Comman search variable of this repositery
     *
     * @var string
     */
    public $searchtop;


    /**
     * Status of Users
     * @var Array()
     */
    public $record_status = [

        'InActive',
        'Active'

    ];
    /**
     * Record Active Status
     *
     * @var string
     */
    public $statusActive = "Active";

    /**
     * Record InActive Status
     *
     * @var string
     */
    public $statusInActive = "InActive";

   
    /**
     * Related model of this repositery
     *
     * @var object
     */
    public $model;

    public function __construct(Session $model)
    {
        $this->model = $model;
    }


    public function findOrThrowException($id) {
        if (!is_null(Session::find($id))) {
            return Session::find($id);
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
    }


    /**
     * Add Created By Info
     *
     * @param array $input
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }


    public function getSuccessfulSessionCount($start="",$end=""){

        if($start != "" && $end != ""){
            $success_session_count = DB::table('session')
                    ->where('status', '=', 1)
                    ->where('scheduled_date_time', '>=', $start)
                    ->where('scheduled_date_time', '<=', $end)
                    ->get();
        }else{
            $success_session_count = DB::table('session')
                    ->where('status', '=', 1)
                    ->get();
        }
        
         return count($success_session_count);    
    }
     
    public function getCancelledByTuteeCount($start="",$end=""){
        
         if($start != "" && $end != ""){
            $count = DB::table('session')
                    ->where('status', '=', 4)
                    ->where('cancelled_date_time', '>=', $start)
                    ->where('cancelled_date_time', '<=', $end)
                    ->get();
         }else{
            $count = DB::table('session')
                    ->where('status', '=', 4)
                    ->get();
         }           

         return count($count);    
    }
    
    public function getCancelledByTutorCount($start="",$end=""){
        
         if($start != "" && $end != ""){
            $count = DB::table('session')
                    ->where('status', '=', 3)
                    ->where('cancelled_date_time', '>=', $start)
                    ->where('cancelled_date_time', '<=', $end)
                    ->get();
         }else{
            $count = DB::table('session')
                    ->where('status', '=', 3)
                    ->get();
         }
         return count($count);    
    } 

    public function getTotalAmountPaidByTuteeForSuccess($start="",$end=""){
        

        if($start != "" && $end != ""){
            $paid_by_tutee = $total = DB::table('session')->where('status', '=', 1)->where('scheduled_date_time', '>=', $start)->where('scheduled_date_time', '<=', $end)->sum('session_fee');
        }else{
            $paid_by_tutee = $total = DB::table('session')->where('status', '=', 1)->sum('session_fee');
        }
        return $paid_by_tutee;    
    } 

    public function getTotalAmountReceivedByAdminForSuccess($start="",$end=""){
        
        if($start != "" && $end != ""){
            $received_by_admin = $total = DB::table('session')->where('status', '=', 1)->where('scheduled_date_time', '>=', $start)->where('scheduled_date_time', '<=', $end)->sum('admin_commission');
        }else{
            $received_by_admin = $total = DB::table('session')->where('status', '=', 1)->sum('admin_commission');
        }
        return $received_by_admin;    
    } 

    public function getTotalAmountPaidToTutorForSuccess($start="",$end=""){
        
        if($start != "" && $end != ""){
            $tutor_earning = $total = DB::table('session')->where('status', '=', 1)->where('scheduled_date_time', '>=', $start)->where('scheduled_date_time', '<=', $end)->sum('tutor_earning');
        }else{
            $tutor_earning = $total = DB::table('session')->where('status', '=', 1)->sum('tutor_earning');    
        }
        return $tutor_earning;    
    } 

    public function getTotalAmountRefundToTuteeForCancelledByTutor($start="",$end=""){
       
        if($start != "" && $end != ""){
             $tutee_refund_amount = $total = DB::table('session')->where('status', '=', 3)->where('cancelled_date_time', '>=', $start)->where('cancelled_date_time', '<=', $end)->sum('refund_amount');
        }else{
             $tutee_refund_amount = $total = DB::table('session')->where('status', '=', 3)->sum('refund_amount');
        }
        return $tutee_refund_amount;    
    } 
     public function getTotalPenaltyAmountPaidByTutee($start="",$end=""){
        ;
        if($start != "" && $end != ""){
                $penalty_amount = $total = DB::table('session')->where('status', '=', 4)->where('cancelled_date_time', '>=', $start)->where('cancelled_date_time', '<=', $end)->sum('penalty_amount');
        }else{
            $penalty_amount = $total = DB::table('session')->where('status', '=', 4)->sum('penalty_amount');
        }
        return $penalty_amount;    
    } 


    public function getTotalEarningOfTutorForCancelledByTuteeSessions($start="",$end=""){
        
        if($start != "" && $end != ""){
            $tutor_earning_for_cancelled = $total = DB::table('session')->where('status', '=', 4)->where('cancelled_date_time', '>=', $start)->where('cancelled_date_time', '<=', $end)->sum('tutor_earning');
        }else{
            $tutor_earning_for_cancelled = $total = DB::table('session')->where('status', '=', 4)->sum('tutor_earning');
        }
        return $tutor_earning_for_cancelled;    
    } 

    /**
     * Add Updated By Info
     *
     * @param array $input
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

    


    /**
     * @param  $id
     * @param  $status
     * @throws GeneralException
     * @return bool
     */
    public function mark($id, $status) {
        //echo $status;exit;
        $user = $this->findOrThrowException($id);
        $user->status = $status;
        if ($user->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * search by fields
     * @param int $per_page
     * @param boolean $active
     * @param string $order_by
     * @param string $sort
     * @param boolean $searchtop
     * @param mixed $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'desc', $searchtop = '', $other = '')
    {
        $query = $this->_getGridJoin();

        if($other != '' && $other != "()")
        {
            $query = $query->whereRaw($other);
        }

        if ($active)
        {
            $query = $query->where('status',$active);
        }

        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);

        return $query;
    }

    /**
     * Get Count
     *
     * @param string $where
     * @return int count
     */
    public function getCount($where)
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()")
        {
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll()
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }

    
    /**
     * Get Grid Join
     *
     * @param object $query
     * @return object
     */
    public function _getGridJoin($query = null)
    {
         if($query)
        {
            return $query->select('session.*','subject.subject_name','topic.topic_name','front_user.first_name',DB::raw("CONCAT(tutee.first_name, ' ', tutee.last_name) as tutee_name"),DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
               ->leftJoin('subject', 'subject.id', '=', 'session.subject_id')  
                  ->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
                  ->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
                  ->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
                   ->where('session.status',1);
                
        }

        return $this->model->select('session.*','subject.subject_name','topic.topic_name','front_user.first_name',DB::raw("CONCAT(tutee.first_name, ' ', tutee.last_name) as tutee_name"),DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
               ->leftJoin('subject', 'subject.id', '=', 'session.subject_id')  
                  ->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
                  ->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
                  ->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
                  ->where('session.status',1);;
            

    }
}
