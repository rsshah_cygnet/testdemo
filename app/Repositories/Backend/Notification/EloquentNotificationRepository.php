<?php

namespace App\Repositories\Backend\Notification;

use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Backend\DbRepository;
use App\Models\Notification\Notification;

/**
 * Class EloquentNotificationRepository
 * @package App\Repositories\Backend\Notification
 */
class EloquentNotificationRepository extends DbRepository implements NotificationRepositoryContract {

    /**
     * comman search variable of this repositery
     * @var string
     */
    public $searchtop;

    /**
     * related model of this repositery
     * @var object
     */
    public $model;
    public $timestamps = false;

    public function __construct(Notification $model) {
        $this->model = $model;
    }

    /**
     * [create description]
     * @param  [type] $result [description]
     * @return [type]         [description]
     */
    public function create($message, $userId, $type = 'success', $createdBy = NULL) {
        $this->model->message = $message;
        $this->model->user_id = $userId;
        $this->model->type = $type;
        if ($createdBy) {
            $this->model->created_by = $createdBy;
        } else {
            $this->model->created_by = Auth::user()->id;
        }
        if ($this->model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param  $id
     * @param  $input
     * @throws GeneralException
     * @return bool
     */
    public function update($id, $request) {
        $notification = $this->findOrThrowException($id);
        $input = $request->all();
        $notification->name = $input['name'];
        $notification->status = $input['status'];
        $notification->updated_by = $request->user()->id;

        // dd($input);
        if ($notification->save()) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.notification.update_error'));
    }

    /**
     * @param  $id
     * @param  $is_active
     * @throws GeneralException
     * @return bool
     */
    public function mark($id, $status) {
        $notification = $this->findOrThrowException($id);
        $notification->is_read_admin = $status;
        if ($notification->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.notification.mark_error'));
    }

    /**
     * @param  $per_page
     * @param  integer    $active
     * @param  string     $order_by
     * @param  string     $sort
     * @param  string     $searchtop
     * @param  array      $other
     * @return mixed
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
        $userRoleId = Auth::user()->roles->first()->id;
        $userRoleName = Auth::user()->roles->first()->name;
        $parentId = Auth::user()->parent_id;
        $userId = Auth::user()->id;
        //$query = $this->model;

        
        $query = $this->model->select('notifications.*','notification_type.notification_type')
                ->leftJoin('notification_type', 'notification_type.id', '=', 'notifications.notification_type_id'); 

        if ($other != '' && $other != "()") {
            $query = $query->whereRaw($other);
        }

        if ($active) {
            $query = $query->where('status', $active);
        }
          $query = $query->where('notifications.is_for_admin','1');
        /*if ($userRoleName != 'Administrator') {
            if ($userRoleName == 'Manufacturer') {
                $query = $query->where('user_id', '=', $userId);
            } else {
                $query = $query->where('user_id', '=', $parentId);
            }
        }*/ 
        if ($searchtop != '') {
            $this->searchtop = $searchtop;
            $query = $query->where(function($query) {
                $query->where('notifications.message', 'like', '%' . $this->searchtop . '%');
            });
        }
        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);
        return $query;
    }

    /**
     * @param  $where
     * @return $count
     */
    public function getCount($where) {
        $query = $this->model;
        if ($where != '' && $where != "()") {
            $query = $query->whereRaw($where);
        }
        $query = $query->where('notifications.is_for_admin','1');
        $count = $query->count();
        return $count;
    }

    /**
     * @return $count
     */
    public function getcAll() {
        ini_set('memory_limit', '-1');
        $count = $this->model->get();
        return $count;
    }

    /**
     * [display description]
     * @param  [type] $result [description]
     * @return [type]         [description]
     */
    public function getNotification($where, $type = 'count', $limit = NULL) {
        $query = $this->model;
        foreach ($where as $k => $v) {
            $query = $query->where($k, "=", $v);
        }
        if ($limit) {
            $query = $query->take($limit);
        }
//        $query = $query->orderBy('is_read', 'desc');
        $query = $query->where('notifications.is_for_admin','1');
        $query = $query->orderBy('created_at', 'desc');
        $count = $query->$type();
        return $count;
    }

    /**
     * [clear description]
     * @param  [type] $result [description]
     * @return [type]         [null]
     */
    public function clearNotifications($limit = 'all') {
        $query = $this->model;
        if ($limit != 'all') {
            $query->where('is_read', '=', 0);
            $query = $query->orderBy('is_read', 'asc');
            $query = $query->orderBy('created_at', 'desc');
            $query = $query->limit($limit);
        }
        if ($query->update(['is_read' => 1])) {
            return true;
        } else {
            return false;
        }
    }

}
