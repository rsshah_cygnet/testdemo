<?php

namespace App\Repositories\Backend\Notification;

/**
 * Interface NotificationRepositoryContract
 * @package App\Repositories\Notification
 */
interface NotificationRepositoryContract
{   
    /**
     * @param  $id
     * @return mixed
     */
    public function findOrThrowException($id);

    /**
     * @param  $per_page
     * @param  string      $order_by
     * @param  string      $sort
     * @return mixed
     */
    public function getPaginated($per_page, $order_by = 'id', $sort = 'asc');

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAll($order_by = 'id', $sort = 'asc');

    /**
     * @param  $input
     * @return mixed
     */
    public function create($message,$userId,$type='success',$createdBy = NULL);

    /**
     * @param  $id
     * @param  $input
     * @return mixed
     */
     public function update($id, $input);

    /**
     * @param  $id
     * @return mixed
     */
    public function destroy($id);

    /**
    * @param $columns
    * @return mixed
    */
    public function selectAll($columns='*', $order_by = 'id', $sort = 'asc');
    
    /**
     * @param  $id
     * @param  $status
     * @return mixed
     */
    public function mark($id, $status);
    
    /*
     * @param $where is where conditions in array format
     * @param type for count or get data
     * @limit for limit data
     * @return will return count of notifications based on array
     */
    public function getNotification($where,$type = 'count',$limit = NULL);
    
    /*
     * clear and read notifications
     */
    public function clearNotifications($limit = 'all');
}
