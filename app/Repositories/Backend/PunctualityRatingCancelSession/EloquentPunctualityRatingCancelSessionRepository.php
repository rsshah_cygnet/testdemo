<?php

namespace App\Repositories\Backend\PunctualityRatingCancelSession;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\PunctualityRatingCancelSession\PunctualityRatingCancelSession;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\PunctualityRatingCancelSession\PunctualityRatingCancelSessionRepositoryContract;
use Carbon\Carbon;

/**
 * Class EloquentCurriculumRepository
 *
 * @package App\Repositories\Backend\PunctualityRatings
 */
class EloquentPunctualityRatingCancelSessionRepository extends DbRepository implements PunctualityRatingCancelSessionRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'punctuality_ratings_for_cancelled_session.from',
		'punctuality_ratings_for_cancelled_session.to',
		'punctuality_ratings_for_cancelled_session.within',
		'punctuality_ratings_for_cancelled_session.type',
		'punctuality_ratings_for_cancelled_session.rating',
		'punctuality_ratings_for_cancelled_session.status',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'From',
		'To',
		'Within',
		'Type',
		'Rating',
		'Status',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'punctuality_ratings_for_cancelled_session.created_at';

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'PunctualityRatingsCancelSession',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "Punctuality Ratings for Cancel Session";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Status of Users
	 * @var Array()
	 */
	public $record_status = [

		'InActive',
		'Active',

	];
	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(PunctualityRatingCancelSession $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(PunctualityRatingCancelSession::find($id))) {
			return PunctualityRatingCancelSession::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.role.not_found'));
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$curriculum = $this->findOrThrowException($id);
		$curriculum->status = $status;
		if ($curriculum->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('*');
		}
		// dd('dsf');

		return $this->model->select('*');

	}

	/**
	 * Create Brand Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($input = array()) {

		if (count($input)) {
			$id = \Auth::user()->id;
			$status = '1';
			$type = $input['type'];
			if ($type == 2) {
				return $this->model->insert([
					'type' => $input['type'],
					'within' => $input['within'],
					'rating' => $input['rating'],
					'from' => null,
					'to' => null,
					'modified_by' => $id,
					'status' => $status,
					'created_at' => carbon::now()->toDateTimeString(),
				]);
			} else if ($type == 1) {
				return $this->model->insert([
					'type' => $input['type'],
					'from' => $input['from'],
					'to' => $input['to'],
					'rating' => $input['rating'],
					'within' => null,
					'modified_by' => $id,
					'status' => $status,
					'created_at' => carbon::now()->toDateTimeString(),
				]);
			}
		}
		throw new GeneralException(trans('exceptions.backend.punctualityrating_cancelsession.create_error'));
	}
	public function update($id, $input) {
		$status = isset($input['status']) && $input['status'] == '1' ? '1' : '0';
		if (count($input)) {
			$LogedInid = \Auth::user()->id;
			$punctuality_ratings = PunctualityRatingCancelSession::find($id);
			$type = $input['type'];
			if ($type == 2) {
				$punctuality_ratings->type = $input['type'];
				$punctuality_ratings->within = $input['within'];
				$punctuality_ratings->rating = $input['rating'];
				$punctuality_ratings->from = null;
				$punctuality_ratings->to = null;
				$punctuality_ratings->modified_by = $LogedInid;
				$punctuality_ratings->status = $status;
				$punctuality_ratings->updated_at = carbon::now()->toDateTimeString();
				return $punctuality_ratings->save();
			} else if ($type == 1) {
				$punctuality_ratings->type = $input['type'];
				$punctuality_ratings->from = $input['from'];
				$punctuality_ratings->to = $input['to'];
				$punctuality_ratings->rating = $input['rating'];
				$punctuality_ratings->within = null;
				$punctuality_ratings->modified_by = $LogedInid;
				$punctuality_ratings->status = $status;
				$punctuality_ratings->updated_at = carbon::now()->toDateTimeString();
				return $punctuality_ratings->save();
			}
		}
		throw new GeneralException(trans('exceptions.backend.punctualityrating_cancelsession.update_error'));
	}
}
