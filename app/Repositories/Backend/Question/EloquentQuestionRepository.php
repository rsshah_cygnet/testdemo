<?php

namespace App\Repositories\Backend\Question;

/**
 * Class EloquentLevelsRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\Question\Question;
use App\Models\Setting;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\Question\QuestionRepositoryContract;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentQuestionRepository extends DbRepository implements QuestionRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'curriculum.curriculum_name',	
		'topic.topic_name',
		'question.question',
		'question.correct_answer',
		'question.option1',
		'question.option2',
		'question.option3',
		'question.option4',
		'question.status',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'Curriculum',
		'Topic Name',
		'Question',
		'Correct Option',
		'Option1',
		'Option2',
		'Option3',
		'Option4',
		'Status',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'question.id';

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'question',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "question";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Status of Users
	 * @var Array()
	 */
	public $record_status = [

		'InActive',
		'Active',

	];
	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Question $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(Question::find($id))) {
			return Question::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
	}

	public function findExamSettings($id) {
		if (!is_null(Setting::find($id))) {
			return Setting::find($id);
		}

		throw new GeneralException('Record not found');
	}

	public function findByname($id, $name) {
		$model = Question::where('question', '=', $name)->where('id', '!=', $id)->first();
		return count($model);
	}

	public function findGradeType($id) {
		$model = Grade::select('grade_type')->where('id', '=', $id)->first();
		return $model->grade_type;
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['created_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['updated_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$user = $this->findOrThrowException($id);
		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Create Brand Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($input) {
//dd($input);
		$loggedin_id = \Auth::user()->id;
		if (isset($input['question_id']) && $input['question_id'] != "" && $input['question_id'] != 0) {
			$obj = $this->findOrThrowException($input['question_id']);
			/*  $data = $this->findByname($input['question_id'],$input['question']);

				            if ($data >= 1) {
				                throw new GeneralException(trans('exceptions.backend.already_exists'));
			*/

		} else {
			$obj = new Question;
			/* if ($this->model->where('question', $input['question'])->first()) {
				                throw new GeneralException(trans('exceptions.backend.already_exists'));
			*/
		}

		$obj->modified_by = $loggedin_id;
		$obj->question = $input['question'];
		$obj->correct_answer = isset($input['correct_answer']) ? $input['correct_answer'] : 0;

		if (isset($input['option1']) && $input['option1'] != "" && $input['option1'] != 'null') {
			$obj->option1 = isset($input['option1']) ? $input['option1'] : '';
		} else {
			$obj->option1 = isset($input['option1_image_name']) ? $input['option1_image_name'] : '';
		}

		if (isset($input['option2']) && $input['option2'] != "" && $input['option2'] != 'null') {
			$obj->option2 = isset($input['option2']) ? $input['option2'] : '';
		} else {
			$obj->option2 = isset($input['option2_image_name']) ? $input['option2_image_name'] : '';
		}

		if (isset($input['option3']) && $input['option3'] != "" && $input['option3'] != 'null') {
			$obj->option3 = isset($input['option3']) ? $input['option3'] : '';
		} else {
			$obj->option3 = isset($input['option3_image_name']) ? $input['option3_image_name'] : '';
		}

		if (isset($input['option4']) && $input['option4'] != "" && $input['option4'] != 'null') {
			$obj->option4 = isset($input['option4']) ? $input['option4'] : '';
		} else {
			$obj->option4 = isset($input['option4_image_name']) ? $input['option4_image_name'] : '';
		}

		$obj->topic_id = isset($input['topic_id']) ? $input['topic_id'] : NULL;
		$obj->question_image = $input['final_image'];

		if (isset($input['question_id']) && $input['question_id'] != "" && $input['question_id'] != 0) {
			$obj->status = isset($input['record_status']) && $input['record_status'] == '1' ? 1 : 0;
		} else {
			$obj->status = '1';
		}

		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.create_error'));
	}

	/**
	 * Create Brand Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create_import($input, $data) {
//dd($input['question']);
		$obj = new Question();
		$loggedin_id = \Auth::user()->id;
		$obj->modified_by = $loggedin_id;
		$obj->question = isset($input['question']) ? $input['question'] : "";
		$obj->correct_answer = isset($input['ans']) ? $input['ans'] : "";
		$obj->topic_id = isset($data['topic_id']) ? $data['topic_id'] : NULL;

		if (isset($input['question_image']) && $input['question_image'] != 'null' && $input['question_image'] != "") {
			$obj->question_image = isset($input['question_image']) ? $input['question_image'] : "";
		} else {
			$obj->question_image = isset($input['img']) ? $input['img'] : "";
		}

		if (isset($input['img_opt1']) && $input['img_opt1'] != 'null' && $input['img_opt1'] != "") {
			$obj->option1 = isset($input['img_opt1']) ? $input['img_opt1'] : "";
		} else if (isset($input['img_option1']) && $input['img_option1'] != 'null' && $input['img_option1'] != "") {
			$obj->option1 = isset($input['img_option1']) ? $input['img_option1'] : "";
		} else {
			$obj->option1 = isset($input['option1']) ? $input['option1'] : '';
		}

		if (isset($input['img_opt2']) && $input['img_opt2'] != 'null' && $input['img_opt2'] != "") {
			$obj->option2 = isset($input['img_opt2']) ? $input['img_opt2'] : "";
		} else if (isset($input['img_option2']) && $input['img_option2'] != 'null' && $input['img_option2'] != "") {
			$obj->option2 = isset($input['img_option2']) ? $input['img_option2'] : "";
		} else {
			$obj->option2 = isset($input['option2']) ? $input['option2'] : "";
		}

		if (isset($input['img_opt3']) && $input['img_opt3'] != 'null' && $input['img_opt3'] != "") {
			$obj->option3 = isset($input['img_opt3']) ? $input['img_opt3'] : "";
		} else if (isset($input['img_option3']) && $input['img_option3'] != 'null' && $input['img_option3'] != "") {
			$obj->option3 = isset($input['img_option3']) ? $input['img_option3'] : "";
		} else {
			$obj->option3 = isset($input['option3']) ? $input['option3'] : "";
		}

		if (isset($input['img_opt4']) && $input['img_opt4'] != 'null' && $input['img_opt4'] != "") {
			$obj->option4 = isset($input['img_opt4']) ? $input['img_opt4'] : "";
		} else if (isset($input['img_option4']) && $input['img_option4'] != 'null' && $input['img_option4'] != "") {
			$obj->option4 = isset($input['img_option4']) ? $input['img_option4'] : "";
		} else {
			$obj->option4 = isset($input['option4']) ? $input['option4'] : "";
		}

		$obj->status = '1';
		$obj->save();
		/* if ($obj->save()) {
			            return true;
			        }
		*/
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('question.*', 'topic.topic_name','curriculum.curriculum_name')
				->leftJoin('topic', 'topic.id', '=', 'question.topic_id')
				->leftJoin('curriculum', 'curriculum.id', '=', 'topic.curriculum_id');

		}

		return $this->model->select('question.*', 'topic.topic_name','curriculum.curriculum_name')
			->leftJoin('topic', 'topic.id', '=', 'question.topic_id')
				->leftJoin('curriculum', 'curriculum.id', '=', 'topic.curriculum_id');

	}

	/**
	 * update exam settings
	 * @param type $id
	 * @param type $request
	 * @return boolean
	 * @throws GeneralException
	 */
	public function update($id, $request) {
		$obj = $this->findExamSettings($id);
		$input = $request->all();

		$obj->reappearing_test = $input['reappearing_test'];
		$obj->min_questions_in_test = $input['min_questions_in_test'];
		$obj->passing_percentage = $input['passing_percentage'];
		$obj->number_of_minutes_per_questions = $input['number_of_minutes_per_questions'];
		$obj->number_of_questions_per_topic = $input['number_of_questions_per_topic'];
		$obj->no_of_mark_per_question = $input['no_of_mark_per_question'];

		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.update_error'));
	}
}
