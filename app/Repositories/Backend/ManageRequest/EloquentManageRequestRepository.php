<?php

namespace App\Repositories\Backend\ManageRequest;

use App\Exceptions\GeneralException;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\ManageRequest\ManageRequestRepositoryContract;
use App\Models\Request\Request;
use App\Models\SpecialLinkPriceToGoRequest\SpecialLinkPriceToGoRequest;
use App\Models\PriceToGoRequest\PriceToGoRequest;
use App\Models\SpecialRequest\SpecialRequest;

/**
 * Class EloquentManageRequestRepository
 * 
 * @package App\Repositories\Backend\ManageRequest
 */
class EloquentManageRequestRepository extends DbRepository implements ManageRequestRepositoryContract
{
    /**
     * Grid Display Columns
     * 
     * @var array
     */
    public $gridColumn = [
        'special_link_price_to_go_requests.created_at',
        'users.first_name',
        'special_link_price_to_go_requests.description',
        'special_link_price_to_go_requests.request_type',
        'special_link_price_to_go_requests.status'
    ];

    /**
     * Grid Display Columns
     * 
     * @var array
     */
    public $gridColumnHeader = [
        'Request Date',
        'Initiated By',
        'Description',
        'Request Type',
        'Status'
    ];

    /**
     * Grid Default Order By
     * 
     * @var string
     */
    public $gridDefaultOrderBy = 'special_link_price_to_go_requests.created_at';

    /**
     * Grid Display Columns
     * 
     * @var array
     */
    public $relations = [
        'users',
    ];

    /**
     * Download File Name
     * 
     * @var string
     */
    public $downloadfilename = "Manage Request";

    /**
     * Comman search variable of this repositery
     * 
     * @var string
     */
    public $searchtop;

    /**
     * Record Approved Status
     * 
     * @var string
     */
    public $statusActive = "Approved";    

    /**
     * Record Declined Status
     * 
     * @var string
     */
    public $statusInActive = "Declined";

    /**
     * Related model of this repositery
     * 
     * @var object
     */
    public $model;

    public function __construct(SpecialLinkPriceToGoRequest $model)
    {
        $this->model = $model;
    }

    
    /**
     * Add Created By Info
     * 
     * @param array $input
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }
        
        return [];
    }

     /**
     * Get Record Status
     * 
     * @param array $input
     */
    public function getRecordStatus($input = array())
    {
        if(count($input))
        {
            if(! isset($input['record_status']) || $input['record_status'] == 0)
            {
                $status = $this->statusInActive;
            }
            else
            {
                $status = $this->statusActive;
            }
            
            $input = array_merge($input, [ 'status' => $status]);


            return $input;
        }
        
        return [];
    }

    /**
     * Upload Logo
     * 
     * @param array $input
     * @return mixed
     */
    public function uploadLogo($input = array())
    {
        if(isset($input['banner_image']) && !empty($input['banner_image']))
        {
            $fileName = $this->uploadManager->setBasePath($this->filePath)
                ->setThumbnailFlag(false)
                ->upload($input['banner_image']);

            if($fileName)
            {
                return $fileName;
            }
        }

        return false;
    }

    /**
     * Add Updated By Info
     * 
     * @param array $input
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }
        
        return [];
    }

    /**
     * @param int $id
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function update($id = null, $input = array())
    {
        if(isset($id) && count($input))
        {
            $model = $this->findOrThrowException($id);

            if($input['request_type'] == 'special')
            {
                $model->status = $input['answer'];
            }
            else
            {
                if($input['answer'] == 1)
                {
                    $model->status = 'Approved';
                }

                if($input['answer'] == 2)
                {
                    $model->status = 'Approved';
                    $model->revised_selling_price = $input['requested_selling_price'];
                }

                if($input['answer'] == 3)
                {
                    $model->status = 'Declined';
                    $model->declined_reason = $input['reject_reason'];
                }
            }
            
            return $model->save();
        }
        
        throw new GeneralException(trans('exceptions.backend.article.update_error'));
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * search by fields
     * @param int $per_page
     * @param boolean $active
     * @param string $order_by
     * @param string $sort
     * @param boolean $searchtop
     * @param mixed $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') 
    {
        $query = $this->_getGridJoin();

        if($other != '' && $other != "()") 
        {
            $query = $query->whereRaw($other);
        }

        if ($active) 
        {
            $query = $query->where('status', $active);
        }
        
        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);
        
        return $query;
    }

    /**
     * Get Count
     * 
     * @param string $where
     * @return int count
     */
    public function getCount($where) 
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()") 
        {   
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll() 
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }

    /**
     * Get Grid Join
     * 
     * @param object $query
     * @return object 
     */
    public function _getGridJoin($query = null)
    {
        if($query)
        {
            $$query =  $this->model->select(
                                        'special_link_price_to_go_requests.id',
                                        'special_link_price_to_go_requests.description',
                                        'special_link_price_to_go_requests.request_type',
                                        'special_link_price_to_go_requests.status',
                                        'users.first_name',
                                        'special_link_price_to_go_requests.created_at'
                                        )
                ->leftJoin('users', 'users.id', '=', 'special_link_price_to_go_requests.user_id');

            return $query;
        }
        
        $query =  $this->model->select(
                                        'special_link_price_to_go_requests.id',
                                        'special_link_price_to_go_requests.description',
                                        'special_link_price_to_go_requests.request_type',
                                        'special_link_price_to_go_requests.status',
                                        'users.first_name',
                                        'special_link_price_to_go_requests.created_at'
                                        )
                ->leftJoin('users', 'users.id', '=', 'special_link_price_to_go_requests.user_id');
        
            return $query;
    }
}
