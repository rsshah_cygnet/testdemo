<?php

namespace App\Repositories\Backend\ManageRequest;

/**
 * Interface ManageRequestRepositoryContract
 *
 * @author Vaishal Gandhi vhgandhi@cygnet-infotech.com
 * @package App\Repositories\Backend\Derivative
 */

interface ManageRequestRepositoryContract
{   
    /**
     * Update Menu Item
     *
     * @param int $id
     * @param array $input
     */
    public function update($id, $input);

    /**
     * Destroy Menu Item
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);
}
