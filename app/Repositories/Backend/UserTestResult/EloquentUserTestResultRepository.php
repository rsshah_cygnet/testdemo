<?php
namespace App\Repositories\Backend\UserTestResult;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Nikita Patel
 */

use App\Exceptions\GeneralException;
use App\Models\UserTestResult\UserTestResult;
use App\Models\Frontusers\Frontusers;
use App\Repositories\Backend\UserTestResult\UserTestResultContract;
use App\Repositories\Backend\DbRepository;
use Illuminate\Support\Facades\Mail;


/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentUserTestResultRepository extends DbRepository implements UserTestResultContract
{
    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumn = [
    'tutor_details.test_date_time',
        'front_user.first_name',
        'front_user.last_name',
        'grade.grade_type',
        'subject.subject_name',
        'topic.topic_name',
        'tutor_details.no_of_total_questions',
        'tutor_details.no_of_total_answers',
        'tutor_details.no_of_minutes',
        'tutor_details.passed_status',
        'tutor_details.no_of_percentage',


        
    ];

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumnHeader = [
    'Test Date-Time',
        'First Name',
        'Last Name',
        'Grade Level',
        'Subject',
        'Topic',
        'No. of que',
        'No. of ans given',
        'No. of minutes available',
        'Result status',
        'Percentage',

    ];

    /**
     * Grid Default Order By
     *
     * @var string
     */
    public $gridDefaultOrderBy = 'tutor_details.created_at';

    public $frontUserId = '';

    /**
     * Grid Display Columns
     *
     * @var array
     */
   /* public $relations = [
        'users'
    ];*/

    /**
     * Download File Name
     *
     * @var string
     */
    public $downloadfilename = "Test Result";

    /**
     * Comman search variable of this repositery
     *
     * @var string
     */
    public $searchtop;


    /**
     * Status of Users
     * @var Array()
     */
    public $record_status = [

        'InActive',
        'Active'

    ];
    /**
     * Record Active Status
     *
     * @var string
     */
    public $statusActive = "Active";

    /**
     * Record InActive Status
     *
     * @var string
     */
    public $statusInActive = "InActive";

   
    /**
     * Related model of this repositery
     *
     * @var object
     */
    public $model;

    public function __construct(UserTestResult $model)
    {
        $this->model = $model;
    }


    public function findOrThrowException($id) {
        if (!is_null(UserTestResult::find($id))) {
            return UserTestResult::find($id);
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
    }

   

    /**
     * Add Created By Info
     *
     * @param array $input
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

     

    /**
     * Add Updated By Info
     *
     * @param array $input
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

    


    /**
     * @param  $id
     * @param  $status
     * @throws GeneralException
     * @return bool
     */
    public function mark($id, $status) {
        //echo $status;exit;
        $user = $this->findOrThrowException($id);
        $user->passed_status = $status;
        if ($user->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

     /**
     * @param $user
     * @return mixed
     */
    public function sendTestResultEmail($id) {
        //$user can be user instance or id
        //if (!$user instanceof Frontusers) {
            $user = Frontusers::find($id);
        //}

        return Mail::send('frontend.auth.emails.confirm', ['token' => $user->confirmation_code], function ($message) use ($user) {
                    $message->to('ngpatel@cygnet-infotech.com', $user->first_name)->subject(app_name() . ': ' . "Your result has been changed");
                });
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * search by fields
     * @param int $per_page
     * @param boolean $active
     * @param string $order_by
     * @param string $sort
     * @param boolean $searchtop
     * @param mixed $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '')
    {
        $query = $this->_getGridJoin();
        if($other != '' && $other != "()")
        {
            $query = $query->whereRaw($other);
        }

        if ($active)
        {
            $query = $query->where('status', $active);
        }

        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);
        return $query;
    }

    /**
     * Get Count
     *
     * @param string $where
     * @return int count
     */
    public function getCount($where)
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()")
        {
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll()
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }

    /**
     * Get Grid Join
     *
     * @param object $query
     * @return object
     */
    public function _getGridJoin($query = null)
    {
         if($query)
        {
            return $query->select('tutor_details.*','front_user.first_name','front_user.last_name','grade.grade_type','subject.subject_name','topic.topic_name')
                ->leftJoin('front_user', 'front_user.id', '=', 'tutor_details.front_user_id')
                ->leftJoin('topic', 'topic.id', '=', 'tutor_details.topic_id')
                ->leftJoin('subject', 'subject.id', '=', 'tutor_details.subject_id')
                ->leftJoin('grade', 'grade.id', '=', 'tutor_details.grade_id')
                ->where('tutor_details.front_user_id','=', $this->frontUserId);
        }

        return $this->model->select('tutor_details.*','front_user.first_name','front_user.last_name','grade.grade_type','subject.subject_name','topic.topic_name')
                ->leftJoin('front_user', 'front_user.id', '=', 'tutor_details.front_user_id')
                ->leftJoin('topic', 'topic.id', '=', 'tutor_details.topic_id')
                ->leftJoin('subject', 'subject.id', '=', 'tutor_details.subject_id')
                ->leftJoin('grade', 'grade.id', '=', 'tutor_details.grade_id')
                 ->where('tutor_details.front_user_id','=', $this->frontUserId);    

    }
}
