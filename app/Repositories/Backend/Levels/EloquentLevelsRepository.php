<?php

namespace App\Repositories\Backend\Levels;

/**
 * Class EloquentLevelsRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\Levels\Levels;
use App\Repositories\Backend\Levels\LevelsRepositoryContract;
use App\Repositories\Backend\DbRepository;


/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentLevelsRepository extends DbRepository implements LevelsRepositoryContract
{
    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumn = [
        'levels.levels_name',
        'curriculum.curriculum_name',
        'eductional_systems.name',
        'grade.grade_name',
        'program.program_name',
        'subject.subject_name',
        'levels.status'
    ];

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumnHeader = [
        'Level Name',
        'Curriculum',
        'Eductional Systems',
        'Grade',
        'Program',
        'Subject',
        'Status'
    ];

    /**
     * Grid Default Order By
     *
     * @var string
     */
    public $gridDefaultOrderBy = 'levels.id';

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $relations = [
        'levels'
    ];

    /**
     * Download File Name
     *
     * @var string
     */
    public $downloadfilename = "Levels";

    /**
     * Comman search variable of this repositery
     *
     * @var string
     */
    public $searchtop;


    /**
     * Status of Users
     * @var Array()
     */
    public $record_status = [

        'InActive',
        'Active'

    ];
    /**
     * Record Active Status
     *
     * @var string
     */
    public $statusActive = "Active";

    /**
     * Record InActive Status
     *
     * @var string
     */
    public $statusInActive = "InActive";

   
    /**
     * Related model of this repositery
     *
     * @var object
     */
    public $model;

    public function __construct(Levels $model)
    {
        $this->model = $model;
    }


    public function findOrThrowException($id) {
        if (!is_null(Levels::find($id))) {
            return Levels::find($id);
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
    }



    public function findByname($id,$name,$curriculum_id) {
          $model = Levels::where('levels_name' , '=', $name)->where('curriculum_id', $curriculum_id)->where('id' , '!=', $id)->first();
          return count($model);
    }

   

    /**
     * Add Created By Info
     *
     * @param array $input
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

     

    /**
     * Add Updated By Info
     *
     * @param array $input
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

    


    /**
     * @param  $id
     * @param  $status
     * @throws GeneralException
     * @return bool
     */
    public function mark($id, $status) {
        //echo $status;exit;
        $user = $this->findOrThrowException($id);
        $user->status = $status;
        if ($user->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * search by fields
     * @param int $per_page
     * @param boolean $active
     * @param string $order_by
     * @param string $sort
     * @param boolean $searchtop
     * @param mixed $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '')
    {
        $query = $this->_getGridJoin();

        if($other != '' && $other != "()")
        {
            $query = $query->whereRaw($other);
        }

        if ($active)
        {
            $query = $query->where('status', $active);
        }

        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);

        return $query;
    }

    /**
     * Get Count
     *
     * @param string $where
     * @return int count
     */
    public function getCount($where)
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()")
        {
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll()
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }



    /**
     * Create Brand Model
     *
     * @param array $input
     * @return boolean
     * @throws GeneralException
     */
     public function create($input) {//dd($input);
        $loggedin_id = \Auth::user()->id;
        if(isset($input['levels_id']) && $input['levels_id'] != "" && $input['levels_id'] != 0){
             $obj = $this->findOrThrowException($input['levels_id']);
             $data = $this->findByname($input['levels_id'],$input['levels_name'],$input['curriculum_id']);

            if ($data >= 1) {
                throw new GeneralException(trans('exceptions.backend.already_exists'));
            }

        }else{
             $obj = new Levels;
             if ($this->model->where('levels_name', $input['levels_name'])->where('curriculum_id', $input['curriculum_id'])->first()) {
                throw new GeneralException(trans('exceptions.backend.already_exists'));
            }
        } 
        
        $obj->modified_by = $loggedin_id;
        $obj->levels_name = $input['levels_name'];

        if(isset($input['levels_id']) && $input['levels_id'] != "" && $input['levels_id'] != 0){
            //$obj->status = isset($input['record_status']) && $input['record_status'] == '1' ? 1 : 0;
        }else{
            $obj->status = '1';
            $obj->curriculum_id         = isset($input['curriculum_id'])?$input['curriculum_id']:NULL;
            $obj->subject_id            = isset($input['subject_id'])?$input['subject_id']:NULL;
            $obj->grade_id              = isset($input['grade_id'])?$input['grade_id']:NULL;
            $obj->program_id            = isset($input['program_id'])?$input['program_id']:NULL;
            $obj->eductional_systems_id = (isset($input['eductional_systems_id']) && $input['eductional_systems_id'] != "")?$input['eductional_systems_id']:NULL;
        }
        
        if ($obj->save()) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.create_error'));
    }

    /**
     * Get Grid Join
     *
     * @param object $query
     * @return object
     */
    public function _getGridJoin($query = null)
    {
         if($query)
        {
            return $query->select('levels.*','curriculum.curriculum_name','eductional_systems.name','subject.subject_name','program.program_name','grade.grade_name')
                ->leftJoin('curriculum', 'curriculum.id', '=', 'levels.curriculum_id')
                ->leftJoin('grade', 'grade.id', '=', 'levels.grade_id')
                ->leftJoin('program', 'program.id', '=', 'levels.program_id')
                ->leftJoin('subject', 'subject.id', '=', 'levels.subject_id')
                ->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'levels.eductional_systems_id');
                
        }

        return $this->model->select('levels.*','curriculum.curriculum_name','eductional_systems.name','subject.subject_name','program.program_name','grade.grade_name')
                ->leftJoin('curriculum', 'curriculum.id', '=', 'levels.curriculum_id')
                ->leftJoin('grade', 'grade.id', '=', 'levels.grade_id')
                ->leftJoin('program', 'program.id', '=', 'levels.program_id')
                ->leftJoin('subject', 'subject.id', '=', 'levels.subject_id')
                ->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'levels.eductional_systems_id');
            

    }
}
