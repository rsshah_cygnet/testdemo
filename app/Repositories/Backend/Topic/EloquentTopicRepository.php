<?php

namespace App\Repositories\Backend\Topic;

use App\Exceptions\GeneralException;
use App\Models\Grade\Grade;
use App\Models\Topic\Topic;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\Topic\TopicRepositoryContract;

/**
 * Class EloquentTopicRepository
 *
 * @package App\Repositories\Backend\Topic
 */
class EloquentTopicRepository extends DbRepository implements TopicRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'topic.topic_name',
		'curriculum.curriculum_name',
		'eductional_systems.name',
		'levels.levels_name',
		'grade.grade_name',
		'program.program_name',
		'subject.subject_name',
		'topic.status',
	];

	/**
	 * Grid Display Columns Header Name
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'Topic Name',
		'Curriculum',
		'Eductional Systems',
		'Level',
		'Grade',
		'Program',
		'Subject',
		'Status',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'topic.id';

	/**
	 * Grid relationship
	 *
	 * @var array
	 */
	public $relations = [
		'topic',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "Topic";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Status of Users
	 * @var Array()
	 */
	public $record_status = [

		'InActive',
		'Active',

	];
	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Topic $model) {
		$this->model = $model;
	}

	/**
     * Find topic based on primary key($id)
     *
     * @param array $id
     * @return array
     */
	public function findOrThrowException($id) {
		if (!is_null(Topic::find($id))) {
			return Topic::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
	}

	/**
     * Find topic based on name
     *
     * @param array $id,$name
     * @return array
     */
	public function findByname($id, $name, $curriculum_id) {
		$model = Topic::where('topic_name', '=', $name)->where('curriculum_id',$curriculum_id)->where('id', '!=', $id)->first();
		return count($model);
	}

	/**
     * Find grade type
     *
     * @param array $id
     * @return string
     */
	public function findGradeType($id) {
		$model = Grade::select('grade_type')->where('id', '=', $id)->first();
		return $model->grade_type;
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['created_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['updated_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$user = $this->findOrThrowException($id);
		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Create Topic Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($input) {

		$loggedin_id = \Auth::user()->id;
		if (isset($input['topic_id']) && $input['topic_id'] != "" && $input['topic_id'] != 0) {
			$obj = $this->findOrThrowException($input['topic_id']);
			$data = $this->findByname($input['topic_id'], $input['topic_name'],$input['curriculum_id']);

			if ($data >= 1) {
				throw new GeneralException(trans('exceptions.backend.already_exists'));
			}

		} else {
			$obj = new Topic;
			if ($this->model->where('topic_name', $input['topic_name'])->where('curriculum_id',$input['curriculum_id'])->first()) {
				throw new GeneralException(trans('exceptions.backend.already_exists'));
			}
		}

		if (isset($input['grade_id']) && $input['grade_id'] != 0 && $input['grade_id'] != "" && $input['grade_id'] != 'null') {
			$grade_type = $this->findGradeType($input['grade_id']);
			$obj->grade_type = $grade_type;
		}

		$obj->modified_by = $loggedin_id;
		$obj->topic_name = $input['topic_name'];
		if (isset($input['topic_id']) && $input['topic_id'] != "" && $input['topic_id'] != 0) {
			//$obj->status = isset($input['record_status']) && $input['record_status'] == '1' ? 1 : 0;
		} else {
			$obj->status = '1';
			$obj->curriculum_id = isset($input['curriculum_id']) ? $input['curriculum_id'] : NULL;
			$obj->subject_id = isset($input['subject_id']) ? $input['subject_id'] : NULL;
			$obj->grade_id = isset($input['grade_id']) ? $input['grade_id'] : NULL;
			$obj->program_id = isset($input['program_id']) ? $input['program_id'] : NULL;
			$obj->level_id = isset($input['level_id']) ? $input['level_id'] : NULL;
			$obj->eductional_systems_id = (isset($input['eductional_systems_id']) && $input['eductional_systems_id'] != "") ? $input['eductional_systems_id'] : NULL;
		}

		if (isset($input['number_of_questions_per_topic']) && $input['number_of_questions_per_topic'] != null && $input['number_of_questions_per_topic'] != 0) {
			$obj->number_of_questions_per_topic = $input['number_of_questions_per_topic'];
		} else {

			$data = \DB::table('settings')->select()->get();
			$obj->number_of_questions_per_topic = $data[0]->number_of_questions_per_topic;
		}

		if (isset($input['number_of_minutes_per_questions']) && $input['number_of_minutes_per_questions'] != null && $input['number_of_minutes_per_questions'] != 0) {
			$obj->number_of_minutes_per_questions = $input['number_of_minutes_per_questions'];
		} else {

			$data = \DB::table('settings')->select()->get();
			$obj->number_of_minutes_per_questions = $data[0]->number_of_minutes_per_questions;
		}

		if (isset($input['passing_percentage']) && $input['passing_percentage'] != null && $input['passing_percentage'] != 0) {
			$obj->passing_percentage = $input['passing_percentage'];
		} else {

			$data = \DB::table('settings')->select()->get();
			$obj->passing_percentage = $data[0]->passing_percentage;
		}

		if (isset($input['min_questions_in_test']) && $input['min_questions_in_test'] != null && $input['min_questions_in_test'] != 0) {
			$obj->min_questions_in_test = $input['min_questions_in_test'];
		} else {

			$data = \DB::table('settings')->select()->get();
			$obj->min_questions_in_test = $data[0]->min_questions_in_test;
		}

		if (isset($input['reappearing_test']) && $input['reappearing_test'] != null && $input['reappearing_test'] != 0) {
			$obj->reappearing_test = $input['reappearing_test'];
		} else {

			$data = \DB::table('settings')->select()->get();
			$obj->reappearing_test = $data[0]->reappearing_test;
		}

		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.create_error'));
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('topic.*', 'curriculum.curriculum_name', 'eductional_systems.name', 'levels.levels_name', 'subject.subject_name', 'grade.grade_name', 'program.program_name')
				->leftJoin('curriculum', 'curriculum.id', '=', 'topic.curriculum_id')
				->leftJoin('levels', 'levels.id', '=', 'topic.level_id')
				->leftJoin('subject', 'subject.id', '=', 'topic.subject_id')
				->leftJoin('program', 'program.id', '=', 'topic.program_id')
				->leftJoin('grade', 'grade.id', '=', 'topic.grade_id')
				->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'topic.eductional_systems_id');

		}

		return $this->model->select('topic.*', 'curriculum.curriculum_name', 'eductional_systems.name', 'levels.levels_name', 'subject.subject_name', 'grade.grade_name', 'program.program_name')
			->leftJoin('curriculum', 'curriculum.id', '=', 'topic.curriculum_id')
			->leftJoin('levels', 'levels.id', '=', 'topic.level_id')
			->leftJoin('subject', 'subject.id', '=', 'topic.subject_id')
			->leftJoin('program', 'program.id', '=', 'topic.program_id')
			->leftJoin('grade', 'grade.id', '=', 'topic.grade_id')
			->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'topic.eductional_systems_id');

	}
}
