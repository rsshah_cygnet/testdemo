<?php

namespace App\Repositories\Backend\Topic;

/**
 * Interface TopicRepositoryContract
 *
 * @package App\Repositories\Backend\Topic
 */
interface TopicRepositoryContract
{
    /**
     * Destroy topic
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * Find topic 
     *
     * @param  $id
     * @return mixed
     */
    public function findOrThrowException($id);

    /**
    * Change the topic status
    *
    * @param array $input
    */
    public function mark($id,$status);

    /**
     * Create topic
     *
     * @param array $input
     */
    public function create($input);
}
