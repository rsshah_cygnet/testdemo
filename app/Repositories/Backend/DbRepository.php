<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;

/**
 * @package App\Repositories
 */
Abstract class DbRepository
{
    /**
     * @param  $id
     * 
     * @throws GeneralException
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null|static
     */
    public function findOrThrowException($id)
    {
        if (! is_null($this->model->find($id))) {
            return $this->model->find($id);
        }
        throw new GeneralException(trans('exceptions.backend.not_found'));
    }
	/**
     * @param  $per_page
     * @param  string      $order_by
     * @param  string      $sort
     * @return mixed
     */
    public function getPaginated($per_page, $active = '', $order_by = 'id', $sort = 'asc')
    {
    	if($active)
    	{
	        return $this->model->where('status', $active)
	            ->orderBy($order_by, $sort)
	            ->paginate($per_page);
	    }
	    else
	    {
	    	return $this->model->orderBy($order_by, $sort)
	            ->paginate($per_page);
	    }
    }  

    /**
    * @param  string  $order_by
    * @param  string  $sort 
    * @return mixed
    */
    public function getAll($order_by = 'id', $sort = 'asc')
    {
        return $this->model->orderBy($order_by, $sort)->get();
    }
    
    /**
     * @param  $id
     * @throws GeneralException
     * @return bool
     */
    public function destroy($id)
    {
        // $obj = $this->findOrThrowException($id, true);
        if ($this->model->where('ID', '=', $id)->delete()) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
    * @param  array $columns 
    * @param  string  $sort
    * @param  string  $order_by
    * @return mixed
    */
    public function selectAll($columns='*', $order_by = 'id', $sort = 'asc')
    {
        //return $this->model->select($columns)->orderBy($order_by, $sort)->skip(0)->take(5)->get();
        return $this->model->select($columns)->orderBy($order_by, $sort)->get();
    }

}
