<?php

namespace App\Repositories\Backend\Subject;

/**
 * Class EloquentsubjectRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\Subject\Subject;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\Subject\SubjectRepositoryContract;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentSubjectRepository extends DbRepository implements SubjectRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'subject.subject_name',
		'curriculum.curriculum_name',
		'eductional_systems.name',
		'program.program_name',
		'grade.grade_name',
		'levels.levels_name',
		'subject.status',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'Subject Name',
		'Curriculum',
		'Eductional Systems',
		'Program',
		'Grade',
		'Level',
		'Status',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'subject.id';

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'subject',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "subject";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Status of Users
	 * @var Array()
	 */
	public $record_status = [

		'InActive',
		'Active',

	];
	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(subject $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(subject::find($id))) {
			return subject::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
	}

	public function findByname($id, $name,$curriculum_id) {
		$model = subject::where('subject_name', '=', $name)->where('curriculum_id',$curriculum_id)->where('id', '!=', $id)->first();
		return count($model);
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['created_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['updated_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$user = $this->findOrThrowException($id);
		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Create Brand Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($input) {
//dd($input);
		$loggedin_id = \Auth::user()->id;
		if(isset($input['passing_percentage']) && $input['passing_percentage'] > 100){
			throw new GeneralException('Passing percentage must be between 1 to 100');
		}
		if (isset($input['subject_id']) && $input['subject_id'] != "" && $input['subject_id'] != 0) {
			$obj = $this->findOrThrowException($input['subject_id']);
			$data = $this->findByname($input['subject_id'], $input['subject_name'],$input['curriculum_id']);

			if ($data >= 1) {
				throw new GeneralException(trans('exceptions.backend.already_exists'));
			}

		} else {
			$obj = new subject;
			if ($this->model->where('subject_name', $input['subject_name'])->where('curriculum_id',$input['curriculum_id'])->first()) {
				throw new GeneralException(trans('exceptions.backend.already_exists'));
			}
		}

		$obj->modified_by = $loggedin_id;
		$obj->subject_name = $input['subject_name'];
		$obj->number_of_questions_per_topic = $input['number_of_questions_per_topic'];
			$obj->number_of_max_questions_per_topic = $input['number_of_max_questions_per_topic'];
	 		$obj->number_of_minutes_per_questions = $input['number_of_minutes_per_questions'];
			$obj->passing_percentage = $input['passing_percentage'];

		if (isset($input['subject_id']) && $input['subject_id'] != "" && $input['subject_id'] != 0) {
			//$obj->status = isset($input['record_status']) && $input['record_status'] == '1' ? 1 : 0;
		} else {
			$obj->status = '1';
			$obj->curriculum_id = isset($input['curriculum_id']) ? $input['curriculum_id'] : NULL;
			$obj->grade_id = isset($input['grade_id']) ? $input['grade_id'] : NULL;
			$obj->level_id = isset($input['level_id']) ? $input['level_id'] : NULL;
			$obj->program_id = isset($input['program_id']) ? $input['program_id'] : NULL;
			$obj->eductional_systems_id = (isset($input['eductional_systems_id']) && $input['eductional_systems_id'] != "") ? $input['eductional_systems_id'] : NULL;
		}
		if (isset($input['min_questions_in_test']) && $input['min_questions_in_test'] != null && $input['min_questions_in_test'] != 0) {
			$obj->min_questions_in_test = $input['min_questions_in_test'];
		} else {

			$data = \DB::table('settings')->select()->get();
			$obj->min_questions_in_test = $data[0]->min_questions_in_test;
		}

		if (isset($input['reappearing_test']) && $input['reappearing_test'] != null && $input['reappearing_test'] != 0) {
			$obj->reappearing_test = $input['reappearing_test'];
		} else {

			$data = \DB::table('settings')->select()->get();
			$obj->reappearing_test = $data[0]->reappearing_test;
		}

		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.create_error'));
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $this->model->select('subject.*', 'curriculum.curriculum_name', 'eductional_systems.name', 'program.program_name', 'grade.grade_name', 'levels.levels_name')
				->leftJoin('curriculum', 'curriculum.id', '=', 'subject.curriculum_id')
				->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'subject.eductional_systems_id')
				->leftJoin('program', 'program.id', '=', 'subject.program_id')
				->leftJoin('levels', 'levels.id', '=', 'subject.level_id')
				->leftJoin('grade', 'grade.id', '=', 'subject.grade_id');

		}

		return $this->model->select('subject.*', 'curriculum.curriculum_name', 'eductional_systems.name', 'program.program_name', 'grade.grade_name', 'levels.levels_name')
			->leftJoin('curriculum', 'curriculum.id', '=', 'subject.curriculum_id')
			->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'subject.eductional_systems_id')
			->leftJoin('program', 'program.id', '=', 'subject.program_id')
			->leftJoin('levels', 'levels.id', '=', 'subject.level_id')
			->leftJoin('grade', 'grade.id', '=', 'subject.grade_id');

	}
}
