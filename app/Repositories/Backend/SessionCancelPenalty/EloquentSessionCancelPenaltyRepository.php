<?php

namespace App\Repositories\Backend\SessionCancelPenalty;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\SessionCancelPenalty\SessionCancelPenalty;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\SessionCancelPenalty\SessionCancelPenaltyRepositoryContract;
use Carbon\Carbon;

/**
 * Class EloquentCurriculumRepository
 *
 * @package App\Repositories\Backend\SessionCancelPenalty
 */
class EloquentSessionCancelPenaltyRepository extends DbRepository implements SessionCancelPenaltyRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		' session_cancelled_penalty.from',
		'session_cancelled_penalty.to',
		'session_cancelled_penalty.within',
		'session_cancelled_penalty.type',
		'session_cancelled_penalty.penalty_percentage',
		'session_cancelled_penalty.status',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'From',
		'To',
		'Within',
		'Type',
		'Penalty Percentage',
		'Status',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'session_cancelled_penalty.created_at';

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'SessionCancelPenalty',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "Session Cancelled Penalty";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Status of Users
	 * @var Array()
	 */
	public $record_status = [

		'InActive',
		'Active',

	];
	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(SessionCancelPenalty $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(SessionCancelPenalty::find($id))) {
			return SessionCancelPenalty::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.role.not_found'));
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$curriculum = $this->findOrThrowException($id);
		$curriculum->status = $status;
		if ($curriculum->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('*');
		}
		// dd('dsf');

		return $this->model->select('*');

	}

	/**
	 * Create Brand Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($input = array()) {

		if (count($input)) {
			$id = \Auth::user()->id;
			$status = '1';
			$type = $input['type'];
			if ($type == 2) {
				return $this->model->insert([
					'type' => $input['type'],
					'within' => $input['within'],
					'penalty_percentage' => $input['penalty_percentage'],
					'from' => null,
					'to' => null,
					'modified_by' => $id,
					'status' => $status,
					'created_at' => carbon::now()->toDateTimeString(),
				]);
			} else if ($type == 1) {
				return $this->model->insert([
					'type' => $input['type'],
					'from' => $input['from'],
					'to' => $input['to'],
					'penalty_percentage' => $input['penalty_percentage'],
					'within' => null,
					'modified_by' => $id,
					'status' => $status,
					'created_at' => carbon::now()->toDateTimeString(),
				]);
			}
		}
		throw new GeneralException(trans('exceptions.backend.punctualityrating_cancelsession.create_error'));
	}
	public function update($id, $input) {
		$status = isset($input['status']) && $input['status'] == '1' ? '1' : '0';
		if (count($input)) {
			$LogedInid = \Auth::user()->id;
			$penalty = SessionCancelPenalty::find($id);
			$type = $input['type'];
			if ($type == 2) {
				$penalty->type = $input['type'];
				$penalty->within = $input['within'];
				$penalty->penalty_percentage = $input['penalty_percentage'];
				$penalty->from = null;
				$penalty->to = null;
				$penalty->modified_by = $LogedInid;
				$penalty->status = $status;
				$penalty->updated_at = carbon::now()->toDateTimeString();
				return $penalty->save();
			} else if ($type == 1) {
				$penalty->type = $input['type'];
				$penalty->from = $input['from'];
				$penalty->to = $input['to'];
				$penalty->penalty_percentage = $input['penalty_percentage'];
				$penalty->within = null;
				$penalty->modified_by = $LogedInid;
				$penalty->status = $status;
				$penalty->updated_at = carbon::now()->toDateTimeString();
				return $penalty->save();
			}
		}
		throw new GeneralException(trans('exceptions.backend.punctualityrating_cancelsession.update_error'));
	}
}
