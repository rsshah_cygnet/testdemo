<?php

namespace App\Repositories\Backend\Settings;

/**
 * Interface SettingsRepositoryContract
 * @package App\Repositories\Settings
 */
interface SettingsRepositoryContract {
    /**
     * @param  $id
     * @param  bool    $withPermissions
     * @return mixed
     */
    // public function findOrThrowException($id, $withPermissions = false);

    /**
     * @param  $per_page
     * @param  string      $order_by
     * @param  string      $sort
     * @return mixed
     */
    public function getPaginated($per_page, $order_by = 'id', $sort = 'asc');

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAll($order_by = 'id', $sort = 'asc');

    /**
     * @param  $input
     * @return mixed
     */
    public function create($input);

    /**
     * @param  $id
     * @param  $input
     * @return mixed
     */
    public function update($id, $input);

    /**
     * @param  $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $columns
     * @return mixed
     */
    public function selectAll($columns = '*', $order_by = 'id', $sort = 'asc');
}
