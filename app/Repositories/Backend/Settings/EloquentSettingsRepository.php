<?php

namespace App\Repositories\Backend\Settings;

use App\Exceptions\GeneralException;
use App\Http\Utilities\FileUploads;
use App\Models\Setting;
use App\Repositories\Backend\DbRepository;

/**
 * Class EloquentSettingsRepository
 * @package App\Repositories\Settings\Settings
 */
class EloquentSettingsRepository extends DbRepository implements SettingsRepositoryContract {

	public $model;

	/**
	 * for fileuploads - App\Http\Utilities\FileUploads
	 * @var [type]
	 */
	protected $fileUploads;

	public function __construct(Setting $model, FileUploads $fileUploads) {
		$this->model = $model;
		$this->fileUploads = $fileUploads;
	}

	/**
	 * @param  $input
	 * @throws GeneralException
	 * @return bool
	 */
	public function create($request) {

	}

	/**
	 * update cms data in database
	 * @param type $id
	 * @param type $request
	 * @return boolean
	 * @throws GeneralException
	 */
	public function update($id, $request) {
		$obj = $this->findOrThrowException($id);
		$input = $request->all();

		// upload logo
		if (isset($input['logo'])) {
			$logo = $input['logo'];
			$this->fileUploads->baseDir = 'images/logo';
			$this->fileUploads->upload($logo);
			$obj->logo = $this->fileUploads->filePath();
		}

		// upload logo
		if (isset($input['favicon'])) {
			$favicon = $input['favicon'];
			$this->fileUploads->baseDir = 'images/logo';
			$this->fileUploads->upload($favicon);
			$obj->favicon = $this->fileUploads->filePath();
		}

		$obj->seo_title = $input['seo_title'];
		$obj->seo_keyword = $input['seo_keyword'];
		$obj->seo_description = $input['seo_description'];
	//	$obj->company_contact = $input['company_contact'];
		$obj->company_address = $input['company_address'];
		//$obj->from_name = $input['from_name'];
		$obj->from_email = $input['from_email'];
//        $obj->facebook = $input['facebook'];
		//        $obj->linkedin = $input['linkedin'];
		//        $obj->twitter = $input['twitter'];
		//        $obj->google = $input['google'];
		$obj->copyright_text = $input['copyright_text'];
		$obj->footer_text = isset($input['footer_text'])?$input['footer_text']:"";
		// $obj->reappearing_test = $input['reappearing_test'];
		// $obj->min_questions_in_test = $input['min_questions_in_test'];
		// $obj->passing_percentage = $input['passing_percentage'];
		// $obj->number_of_minutes_per_questions = $input['number_of_minutes_per_questions'];
		// $obj->number_of_questions_per_topic = $input['number_of_questions_per_topic'];
		$obj->lower_level_grade_fee = $input['lower_level_grade_fee'];
		$obj->higher_level_grade_fee = $input['higher_level_grade_fee'];
		$obj->tutor_availability_range = $input['tutor_availability_range'];
		$obj->free_session_per_fb_count = $input['free_session_per_fb_count'];
		// $obj->no_of_mark_per_question = $input['no_of_mark_per_question'];
		$obj->admin_commission_for_higher_level = $input['admin_commission_for_higher_level'];
		$obj->admin_commission_for_lower_level = $input['admin_commission_for_lower_level'];

//        $obj->google_analytics = $input['google_analytics'];
		//        $obj->home_video1 = $input['home_video1'];
		//        $obj->home_video1 = $input['home_video1'];
		//        $obj->home_video3 = $input['home_video3'];
		//        $obj->home_video4 = $input['home_video4'];
		//        $obj->explanation1 = $input['explanation1'];
		//        $obj->explanation2 = $input['explanation2'];
		//        $obj->explanation3 = $input['explanation3'];
		//        $obj->explanation4 = $input['explanation4'];
		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.update_error'));
	}

}
