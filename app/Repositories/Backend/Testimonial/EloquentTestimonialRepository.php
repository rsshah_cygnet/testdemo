<?php

namespace App\Repositories\Backend\Testimonial;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\Testimonial\Testimonial;
use App\Repositories\Backend\DbRepository;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentTestimonialRepository extends DbRepository implements TestimonialRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'testimonials.title',
		'front_user.name',
		'testimonials.publish_on_home_page',
		/*'testimonials.status',*/
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'Name',
		'Posted By',
		'Publish on home page',
		/*'Status',*/
	];

	public $downloadfilename = "Testimonial";

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'testimonials.created_at';

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Testimonial $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(Testimonial::find($id))) {
			return Testimonial::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['created_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['updated_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$user = $this->findOrThrowException($id);

		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	public function publish($id, $publish) {
		//echo $status;exit;
		$user = $this->findOrThrowException($id);

		$user->publish_on_home_page = $publish;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		/*if ($active)
			        {
			            $query = $query->where('status', $active);
		*/

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);
		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('testimonials.*', \DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as name"))
				->leftJoin('front_user', 'front_user.id', '=', 'testimonials.created_by');
		}

		return $this->model->select('testimonials.*', \DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as name"))
			->leftJoin('front_user', 'front_user.id', '=', 'testimonials.created_by');

	}
}
