<?php

namespace App\Repositories\Backend\EducationalSystem;

/**
 * Interface EducationalSystemContract
 *
 * @package App\Repositories\Backend\EducationalSystem
 */

interface EducationalSystemContract
{
    /* Create Menu Item
     *
     * @param array $input
     */
    public function create($input);

    /**
     * Update Menu Item
     *
     * @param int $id
     * @param array $input
     */
    public function update($id, $input);

    /**
     * Destroy Menu Item
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);
}
