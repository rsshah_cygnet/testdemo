<?php

namespace App\Repositories\Backend\EducationalSystem;

/**
 * Class EloquentEducationalSystemRepository
 *
 */

use App\Exceptions\GeneralException;
use App\Models\EducationalSystem\EducationalSystem;
use App\Models\Curriculum\Curriculum;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\EducationalSystem\EducationalSystemContract;
use Carbon\Carobn;
/**
 * Class EloquentBrandModelRepository
 *
 * @package App\Repositories\Backend\EducationalSystem
 */
class EloquentEducationalSystemRepository extends DbRepository implements EducationalSystemContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'curriculum.curriculum_name',
		'eductional_systems.name',
		'subject.subject_name',
		'grade.grade_name',
		'program.program_name',
		'levels.levels_name',
		'eductional_systems.status',
		'users.first_name',
		'eductional_systems.created_at',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'Curriculum',
		'Name',
		'Subject',
		'Grade',
		'Program',
		'Level',
		'Status',
		'modified By',
		'Created At',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'eductional_systems.name';

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'curriculum',
		'users',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "Educational System";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(EducationalSystem $model) {
		$this->model = $model;
	}

	public function findByname($id, $name, $education_system_id) {
		$model = EducationalSystem::where('name', '=', $name)->where('curriculum_id', '=', $id)->where('id', '!=', $education_system_id)->get()->all();
		return count($model);
	}

	/**
	 * Create Brand Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($input) {
		$loggedin_id = \Auth::user()->id;
		//edit
		if (isset($input['levels_id']) && $input['levels_id'] != "" && $input['levels_id'] != 0) {
			$obj = $this->findOrThrowException($input['levels_id']);
			$data = $this->findByname($input['levels_id'], $input['levels_name']);

			if ($data >= 1) {
				throw new GeneralException(trans('exceptions.backend.already_exists'));
			}

		} else {
			// $data = $this->findByname($input['curriculum_id'], $input['name']);
			// if ($data >= 1) {
			// 	throw new GeneralException("Name has already been taken for this curriculum. Please choose different name.");
			// }
			$obj = new EducationalSystem;
			if ($this->model->where('name', $input['name'])->where('curriculum_id',$input['curriculum_id'])->first()) {
				throw new GeneralException(trans('exceptions.backend.already_exists'));
			}
			// if ($this->model->where('name', $input['name'])->first()) {
			// 	throw new GeneralException(trans('exceptions.backend.already_exists'));
			// }
		}

		$obj->modified_by = $loggedin_id;
		// $obj->levels_name = $input['level_id'];
		$obj->name = $input['name'];
		$obj->curriculum_id = isset($input['curriculum_id']) ? $input['curriculum_id'] : NULL;
		$obj->levels_id = isset($input['level_id']) ? $input['level_id'] : NULL;

		$obj->subject_id = isset($input['subject_id']) ? $input['subject_id'] : NULL;
		$obj->grade_id = isset($input['grade_id']) ? $input['grade_id'] : NULL;
		$obj->program_id = isset($input['program_id']) ? $input['program_id'] : NULL;
		$obj->created_at = date('Y-m-d H:i:s');
		$obj->updated_at = null;

		// $obj->eductional_systems_id = (isset($input['eductional_systems_id']) && $input['eductional_systems_id'] != "")?$input['eductional_systems_id']:NULL;
		$obj->status = 1;
		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.create_error'));
	}
	// public function create($input = array())
	// {
	//     if(count($input))
	//     {
	//         $edsystem = EducationalSystem::where('curriculum_id', '=', $input["curriculum_id"])
	//                               ->where('name', '=', $input["name"])
	//                               ->first();
	//          if ($edsystem !== null) {
	//             return false;
	//         }

	//         $id = \Auth::user()->id;
	//         $status = '1';
	//         return $this->model->insert([
	//             'curriculum_id' => $input['curriculum_id'],
	//             'name' => $input['name'],
	//             'modified_by' => $id,
	//             'status' => $status,
	//             'created_at' => date('Y-m-d H:i:s')
	//         ]);
	//     }

	//     throw new GeneralException(trans('exceptions.backend.article.create_error'));
	// }

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Get Record Status
	 *
	 * @param array $input
	 */
	public function getRecordStatus($input = array()) {
		// if(count($input))
		// {
		//     if(! isset($input['record_status']) || $input['record_status'] == 0)
		//     {
		//         $status = $this->statusInActive;
		//     }
		//     else
		//     {
		//         $status = $this->statusActive;
		//     }

		//     $input = array_merge($input, [ 'status' => $status]);

		//     return $input;
		// }

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['updated_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param int $id
	 * @param array $input
	 * @throws GeneralException
	 * @return bool
	 */
	public function update($id = null, $input = array()) {
		$loggedin_id = \Auth::user()->id;
		//edit
		if (isset($input['education_system_id']) && $input['education_system_id'] != "" && $input['education_system_id'] != 0) {
			// dd('sdf');
			$obj = $this->findOrThrowException($input['education_system_id']);
			$data = $this->findByname($input['curriculum_id'], $input['name'], $input['education_system_id']);
			
			if ($data >= 1) {
				throw new GeneralException("Name has already been taken for this curriculum. Please choose different name.");
			}

			// $data = $this->findByname($input['education_system_id'], $input['name']);

			// if ($data >= 1) {
			// 	throw new GeneralException(trans('exceptions.backend.already_exists'));
			// }

		} else {
			
			$obj = new EducationalSystem;
			if ($this->model->where('name', $input['name'])->first()) {
				throw new GeneralException(trans('exceptions.backend.already_exists'));
			}
		}

		
		$obj->modified_by = $loggedin_id;
		// $obj->levels_name = $input['level_id'];
		$obj->name = $input['name'];
		$obj->curriculum_id = isset($input['curriculum_id']) ? $input['curriculum_id'] : NULL;
		$obj->levels_id = isset($input['level_id']) ? $input['level_id'] : NULL;

		$obj->subject_id = isset($input['subject_id']) ? $input['subject_id'] : NULL;
		$obj->grade_id = isset($input['grade_id']) ? $input['grade_id'] : NULL;
		$obj->program_id = isset($input['program_id']) ? $input['program_id'] : NULL;
		$obj->updated_at = date('Y-m-d H:i:s');

		// $obj->eductional_systems_id = (isset($input['eductional_systems_id']) && $input['eductional_systems_id'] != "")?$input['eductional_systems_id']:NULL;

		//$obj->status = isset($input['status']) ? 1 : 0;
		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.create_error'));

		throw new GeneralException(trans('exceptions.backend.article.update_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {

		if ($query) {
			return $query->select('eductional_systems.*', 'users.first_name', 'curriculum.curriculum_name', 'grade.grade_name', 'subject.subject_name', 'program.program_name', 'levels_name')
				->leftJoin('grade', 'grade.id', '=', 'eductional_systems.grade_id')
				->leftJoin('levels', 'levels.id', '=', 'eductional_systems.levels_id')
				->leftJoin('program', 'program.id', '=', 'eductional_systems.program_id')
				->leftJoin('subject', 'subject.id', '=', 'eductional_systems.subject_id')
				->leftJoin('curriculum', 'curriculum.id', '=', 'eductional_systems.curriculum_id')
				->leftJoin('users', 'users.id', '=', 'eductional_systems.modified_by');
		}

		return $this->model->select('eductional_systems.*', 'users.first_name', 'curriculum.curriculum_name', 'eductional_systems.name', 'grade.grade_name', 'subject.subject_name', 'program.program_name', 'levels_name')
			->leftJoin('grade', 'grade.id', '=', 'eductional_systems.grade_id')
			->leftJoin('levels', 'levels.id', '=', 'eductional_systems.levels_id')
			->leftJoin('program', 'program.id', '=', 'eductional_systems.program_id')
			->leftJoin('subject', 'subject.id', '=', 'eductional_systems.subject_id')
			->leftJoin('curriculum', 'curriculum.id', '=', 'eductional_systems.curriculum_id')
			->leftJoin('users', 'users.id', '=', 'eductional_systems.modified_by');
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		$user = $this->findOrThrowException($id);
		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}
}
