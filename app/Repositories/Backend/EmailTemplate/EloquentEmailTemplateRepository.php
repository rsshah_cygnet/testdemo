<?php

namespace App\Repositories\Backend\EmailTemplate;

use App\Exceptions\GeneralException;
use App\Models\Emailtemplate\Emailtemplate;
use App\Repositories\Backend\DbRepository;

/**
 * Class EloquentCmspagesRepository
 * @package App\Repositories\Backend\Media
 */
class EloquentEmailTemplateRepository extends DbRepository implements EmailTemplateRepositoryContract {

	/**
	 * comman search variable of this repositery
	 * @var string
	 */
	public $searchtop;

	/**
	 * related model of this repositery
	 * @var object
	 */
	public $model;

	public function __construct(Emailtemplate $model) {
		$this->model = $model;
	}

	/**
	 *
	 * @param type $request
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($request) {
		$input = $request->all();

		$this->model->title = $input['title'];
		$this->model->type_id = isset($input['type']) ? $input['type'] : 0;
		$this->model->template_name = isset($input['template_name']) ? $input['template_name'] : "";
		$this->model->subject = $input['subject'];
		$this->model->body = $input['body'];
		$this->model->created_by = $request->user()->id;
		$this->model->created_at = date('Y-m-d H:i:s');

		if ($this->model->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.emailtemplate.create_error'));
	}

	/**
	 *
	 * @param type $id
	 * @param type $request
	 * @return boolean
	 * @throws GeneralException
	 */
	public function update($id, $request) {
		$emailTemplate = $this->findOrThrowException($id);
		$input = $request->all();
		$emailTemplate->title = $input['title'];
		$emailTemplate->type_id = isset($input['type']) ? $input['type'] : 0;
		$emailTemplate->template_name = isset($input['template_name']) ? $input['template_name'] : "";
		$emailTemplate->subject = $input['subject'];
		$emailTemplate->body = $input['body'];
		$emailTemplate->updated_by = $request->user()->id;
		$emailTemplate->updated_at = date('Y-m-d H:i:s');

		// dd($input);
		if ($emailTemplate->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.emailtemplate.update_error'));
	}

	/**
	 *
	 * @param type $id
	 * @param type $status
	 */
	public function mark($id, $status) {
		//
	}

	/**
	 *
	 * @param type $query
	 * @return type
	 */
	public function _getGridJoin($query) {

		if (!empty($query)) {
			return $query->Join('email_template_type', 'email_template.type_id', '=', 'email_template_type.id');
		} else {
			return $query->select('email_template.id', 'title', 'name', 'subject', 'email_template.updated_at');
		}

	}

	/**
	 *
	 * @param type $per_page
	 * @param type $active
	 * @param type $order_by
	 * @param type $sort
	 * @param type $searchtop
	 * @param type $other
	 * @return type
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->model;
		$query = $query->select('email_template.id', 'email_template.title', 'email_template.subject', 'email_template.updated_at');
		/* if( count($other) > 0 && $other !='' )
			          {
			          foreach($other as $k => $v )
			          {
			          $query = $query->where($k, 'like', '%'.$v.'%');
			          }
		*/
		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}
		if ($searchtop != '') {
			$this->searchtop = $searchtop;
			$query = $query->where(function ($query) {
				$query->where('title', 'like', '%' . $this->searchtop . '%')
					->orWhere('subject', 'like', '%' . $this->searchtop . '%');
			});
		}
		// $query = $this->_getGridJoin($query);
		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);
		return $query;
	}

	/**
	 *
	 * @param type $where
	 * @return type
	 */
	public function getCount($where) {
		$query = $this->model;
		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}
		//$query = $this->_getGridJoin($query);  -> This line is removed because it's creating problem to count total records
		$count = $query->count();
		// dd($count);
		return $count;
	}

	/**
	 *
	 * @return type
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$count = $this->model->get();
		return $count;
	}

}
