<?php

namespace App\Repositories\Backend\EmailTemplate;

/**
 * Interface CmspagesRepositoryContract
 * @package App\Repositories\Media
 */
interface EmailTemplateRepositoryContract {

    /**
     * 
     * @param type $id
     */
    public function findOrThrowException($id);

    /**
     * 
     * @param type $per_page
     * @param type $order_by
     * @param type $sort
     */
    public function getPaginated($per_page, $order_by = 'id', $sort = 'asc');

    /**
     * 
     * @param type $order_by
     * @param type $sort
     */
    public function getAll($order_by = 'id', $sort = 'asc');

    /**
     * 
     * @param type $input
     */
    public function create($input);

    /**
     * 
     * @param type $id
     * @param type $input
     */
    public function update($id, $input);

    public function mark($id, $status);

    /**
     * 
     * @param type $id
     */
    public function destroy($id);
    
     /**
     * 
     * @param type $query
     * @return type
     */
    public function _getGridJoin($query);

    /**
     * 
     * @param type $columns
     * @param type $order_by
     * @param type $sort
     */
    public function selectAll($columns = '*', $order_by = 'id', $sort = 'asc');
}
