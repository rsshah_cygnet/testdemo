<?php

namespace App\Repositories\Backend\PunctualityRatings;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\PunctualityRatings\PunctualityRatings;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\PunctualityRatings\PunctualityRatingsRepositoryContract;
use Carbon\Carbon;

/**
 * Class EloquentCurriculumRepository
 *
 * @package App\Repositories\Backend\PunctualityRatings
 */
class EloquentPunctualityRatingsRepository extends DbRepository implements PunctualityRatingsRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'punctuality_ratings.from',
		'punctuality_ratings.to',
		'punctuality_ratings.rating',
		'punctuality_ratings.status',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'From',
		'To',
		'Rating',
		'Status',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'punctuality_ratings.created_at';

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'PunctualityRatings',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "Punctuality Ratings";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Status of Users
	 * @var Array()
	 */
	public $record_status = [

		'InActive',
		'Active',

	];
	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(PunctualityRatings $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(PunctualityRatings::find($id))) {
			return PunctualityRatings::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.role.not_found'));
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$curriculum = $this->findOrThrowException($id);
		$curriculum->status = $status;
		if ($curriculum->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('*');
		}
		// dd('dsf');

		return $this->model->select('*');

	}

	/**
	 * Create Brand Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($input = array()) {

		if (count($input)) {
			$id = \Auth::user()->id;
			$status = '1';
			return $this->model->insert([
				'from' => $input['from'],
				'to' => $input['to'],
				'rating' => $input['rating'],
				'modified_by' => $id,
				'status' => $status,
				'created_at' => carbon::now()->toDateTimeString(),
			]);
		}
		throw new GeneralException(trans('exceptions.backend.article.create_error'));
	}
	public function update($id, $input) {
		$status = isset($input['status']) && $input['status'] == '1' ? '1' : '0';
		if (count($input)) {
			$LogedInid = \Auth::user()->id;
			$punctuality_ratings = PunctualityRatings::find($id);
			$punctuality_ratings->from = $input['from'];
			$punctuality_ratings->to = $input['to'];
			$punctuality_ratings->rating = $input['rating'];
			$punctuality_ratings->modified_by = $LogedInid;
			$punctuality_ratings->status = $status;
			$punctuality_ratings->updated_at = carbon::now()->toDateTimeString();
			return $punctuality_ratings->save();
		}
		throw new GeneralException(trans('exceptions.backend.article.create_error'));
	}
}
