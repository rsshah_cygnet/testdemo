<?php

namespace App\Repositories\Backend\PunctualityRatings;

/**
 * Interface CurriculumRepositoryContract
 *
 * @author Umang Soni
 * @package App\Repositories\Backend\PunctualityRatings
 */
interface PunctualityRatingsRepositoryContract {
	/* Create Item
		     *
		     * @param array $input
	*/
	// public function create($input);

	/**
	 * Update Item
	 *
	 * @param int $id
	 * @param array $input
	 */
	// public function update($id, $input);

	/**
	 * Destroy Item
	 *
	 * @param  $id
	 * @return mixed$this->repository
	 */
	public function destroy($id);

	public function findOrThrowException($id);

	public function mark($id, $status);

	/* Create Menu Item
		     *
		     * @param array $input
	*/
	public function create($input);

	/**
	 * Update Menu Item
	 *
	 * @param int $id
	 * @param array $input
	 */
	public function update($id, $input);

	/**
	 * Destroy Menu Item
	 *
	 * @param  $id
	 * @return mixed
	 */
}
