<?php

namespace App\Repositories\Backend\Curriculum;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\Curriculum\Curriculum;
use App\Repositories\Backend\Curriculum\CurriculumRepositoryContract;
use App\Repositories\Backend\DbRepository;
use Carbon\Carbon;
use DB;
/**
 * Class EloquentCurriculumRepository
 *
 * @package App\Repositories\Backend\Curriculum
 */
class EloquentCurriculumRepository extends DbRepository implements CurriculumRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'curriculum.curriculum_name',
		'curriculum.higher_level_fee',
		'curriculum.lower_level_fee',
		'curriculum.status',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'Curriculum Title',
		'Higher Level Fee',
		'Lower Level Fee',
		'Status',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'curriculum.created_at';

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'curriculum',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "Curriculum";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Status of Users
	 * @var Array()
	 */
	public $record_status = [

		'InActive',
		'Active',

	];
	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Curriculum $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(Curriculum::find($id))) {
			return Curriculum::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.curriculum.record_not_found'));
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['modified_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$curriculum = $this->findOrThrowException($id);
		$curriculum->status = $status;
		if ($curriculum->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('*');
		}
		// dd('dsf');
		return $this->model->select('*');

	}

	/**
	 * Create Brand Model
	 *
	 * @param array $input
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($input = array()) {
		if (count($input)) {
			$id = \Auth::user()->id;
			$status = '1';
			$lower_level_fee = DB::table('settings')
			->select('lower_level_grade_fee')
			->first();
			
			$higher_level_grade_fee = DB::table('settings')
			->select('higher_level_grade_fee')
			->first();

			$lower_fee = (isset($input['lower_level_fee']) && $input['lower_level_fee'] !=0)?$input['lower_level_fee']:"";

			$higher_fee = (isset($input['higher_level_fee']) && $input['higher_level_fee'] !=0)?$input['higher_level_fee']:"";
			
			return $this->model->insert([
				'curriculum_name' => $input['curriculum_name'],
				'higher_level_fee' => $higher_fee,
				'lower_level_fee' => $lower_fee,
				'modified_by' => $id,
				'display_in_eductional_systems' => isset($input['display_in_eductional_systems']) && $input['display_in_eductional_systems'] == '1' ? '1' : '0',
				'display_in_level' => isset($input['display_in_level']) && $input['display_in_level'] == '1' ? '1' : '0',
				'display_in_program' => isset($input['display_in_program']) && $input['display_in_program'] == '1' ? '1' : '0',
				'status' => $status,
				'created_at' => carbon::now()->toDateTimeString(),
			]);
		}
		throw new GeneralException(trans('exceptions.backend.article.create_error'));
	}
	public function update($id, $input) {
		if(!empty($input->id)){
			
		}
		else{
			$status = isset($input['status']) && $input['status'] == '1' ? '1' : '0';
		}
		$lower_level_fee = DB::table('settings')
			->select('lower_level_grade_fee')
			->first();
			
			$higher_level_grade_fee = DB::table('settings')
			->select('higher_level_grade_fee')
			->first();

			$lower_fee = (isset($input['lower_level_fee']) && $input['lower_level_fee'] !=0)?$input['lower_level_fee']:"";

			$higher_fee = (isset($input['higher_level_fee']) && $input['higher_level_fee'] !=0)?$input['higher_level_fee']:"";
		if (count($input)) {
			$LogedInid = \Auth::user()->id;
			$curriculum = Curriculum::find($id);
			$curriculum->curriculum_name = $input['curriculum_name'];
			$curriculum->higher_level_fee = $higher_fee;
			$curriculum->lower_level_fee = $lower_fee;
			// $curriculum->display_in_eductional_systems = isset($input['display_in_eductional_systems']) && $input['display_in_eductional_systems'] = '1' ? '1' : '0';
			// $curriculum->display_in_level = isset($input['display_in_level']) && $input['display_in_level'] == '1' ? '1' : '0';
			// $curriculum->display_in_program = isset($input['display_in_program']) && $input['display_in_program'] == '1' ? '1' : '0';
			$curriculum->modified_by = $LogedInid;
			if(!empty($input->id)){

			}else{
			$curriculum->status = $status;
			$curriculum->display_in_eductional_systems = isset($input['display_in_eductional_systems']) && $input['display_in_eductional_systems'] = '1' ? '1' : '0';
			$curriculum->display_in_level = isset($input['display_in_level']) && $input['display_in_level'] == '1' ? '1' : '0';
			$curriculum->display_in_program = isset($input['display_in_program']) && $input['display_in_program'] == '1' ? '1' : '0';
			}
			$curriculum->updated_at = carbon::now()->toDateTimeString();
			return $curriculum->save();
		}
		throw new GeneralException(trans('exceptions.backend.article.create_error'));
	}
}
