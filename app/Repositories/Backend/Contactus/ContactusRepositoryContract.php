<?php

namespace App\Repositories\Backend\Contactus;

/**
 * Interface FrontusersRepositoryContract
 *
 * @author Sudhir Virpara
 * @package App\Repositories\Backend\Frontusers
 */

interface ContactusRepositoryContract
{
    /* Create Item
     *
     * @param array $input
     */
   // public function create($input);

    /**
     * Update Item
     *
     * @param int $id
     * @param array $input
     */
   // public function update($id, $input);

    /**
     * Destroy Item
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);

     public function findOrThrowException($id);


      public function mark($id, $status);

}
