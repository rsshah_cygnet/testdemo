<?php

namespace App\Repositories\Backend\Programs;

/**
 * Class EloquentLevelsRepository
 *
 */

use App\Exceptions\GeneralException;
use App\Models\Program\Program;
use App\Repositories\Backend\Programs\ProgramsRepositoryContract;
use App\Repositories\Backend\DbRepository;


/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Programs
 */
class EloquentProgramsRepository extends DbRepository implements ProgramsRepositoryContract
{
    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumn = [
        'program.program_name',
        'curriculum.curriculum_name',
        'eductional_systems.name',
        'subject.subject_name',
        'grade.grade_name',
        'levels.levels_name',
        'program.status'
    ];

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumnHeader = [
        'Program Name',
        'Curriculum',
        'Eductional Systems',
        'Subject',
        'Grade',
        'Level',
        'Status'
    ];

    /**
     * Grid Default Order By
     *
     * @var string
     */
    public $gridDefaultOrderBy = 'program.id';

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $relations = [
        'users'
    ];

    /**
     * Download File Name
     *
     * @var string
     */
    public $downloadfilename = "Programs";

    /**
     * Comman search variable of this repositery
     *
     * @var string
     */
    public $searchtop;


    /**
     * Status of Users
     * @var Array()
     */
    public $record_status = [

        'InActive',
        'Active'

    ];
    /**
     * Record Active Status
     *
     * @var string
     */
    public $statusActive = "Active";

    /**
     * Record InActive Status
     *
     * @var string
     */
    public $statusInActive = "InActive";

   
    /**
     * Related model of this repositery
     *
     * @var object
     */
    public $model;

    public function __construct(Program $model)
    {
        $this->model = $model;
    }


    public function findOrThrowException($id) {
        if (!is_null(Program::find($id))) {
            return Program::find($id);
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
    }



    public function findByname($id,$name, $curriculum_id) {
          $model = Program::where('program_name' , '=', $name)->where('curriculum_id', $curriculum_id)->where('id' , '!=', $id)->first();
          return count($model);
    }

   
    /**
     * Add Created By Info
     *
     * @param array $input
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

     

    /**
     * Add Updated By Info
     *
     * @param array $input
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

    


    /**
     * @param  $id
     * @param  $status
     * @throws GeneralException
     * @return bool
     */
    public function mark($id, $status) {
        //echo $status;exit;
        $user = $this->findOrThrowException($id);
        $user->status = $status;
        if ($user->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * search by fields
     * @param int $per_page
     * @param boolean $active
     * @param string $order_by
     * @param string $sort
     * @param boolean $searchtop
     * @param mixed $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '')
    {
        $query = $this->_getGridJoin();

        if($other != '' && $other != "()")
        {
            $query = $query->whereRaw($other);
        }

        if ($active)
        {
            $query = $query->where('status', $active);
        }

        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);

        return $query;
    }

    /**
     * Get Count
     *
     * @param string $where
     * @return int count
     */
    public function getCount($where)
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()")
        {
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll()
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }



    /**
     * Create Brand Model
     *
     * @param array $input
     * @return boolean
     * @throws GeneralException
     */
     public function create($input) {
        
        $loggedin_id = \Auth::user()->id;
        if(isset($input['programs_id']) && $input['programs_id'] != "" && $input['programs_id'] != 0){
             $obj = $this->findOrThrowException($input['programs_id']);
             $data = $this->findByname($input['programs_id'],$input['program_name'],$input['curriculum_id']);
             
            if ($data >= 1) {
                throw new GeneralException(trans('exceptions.backend.already_exists'));
            }

        }else{
             $obj = new Program;
             if ($this->model->where('program_name', $input['program_name'])->where('curriculum_id', $input['curriculum_id'])->first()) {
                throw new GeneralException(trans('exceptions.backend.already_exists'));
            }
        } 
        
        $obj->modified_by = $loggedin_id;
        $obj->program_name = $input['program_name'];        
        if(isset($input['programs_id']) && $input['programs_id'] != "" && $input['programs_id'] != 0)
        {
            //$obj->status = isset($input['record_status']) && $input['record_status'] == '1' ? 1 : 0;
        }else{
            $obj->status = 1;
            $obj->curriculum_id = isset($input['curriculum_id'])?$input['curriculum_id']:NULL;
            $obj->subject_id = isset($input['subject_id'])?$input['subject_id']:NULL;
            $obj->grade_id = isset($input['grade_id'])?$input['grade_id']:NULL;
            $obj->level_id = isset($input['level_id'])?$input['level_id']:NULL;
            $obj->eductional_systems_id = (isset($input['eductional_systems_id']) && $input['eductional_systems_id'] != "")?$input['eductional_systems_id']:NULL;
        }
        
        
        if ($obj->save()) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.create_error'));
    
    }

    /**
     * Get Grid Join
     *
     * @param object $query
     * @return object
     */
    public function _getGridJoin($query = null)
    {  
        if($query)
        {
            return $query->select('program.*','curriculum.curriculum_name','eductional_systems.name','subject.subject_name','levels.levels_name','grade.grade_name')
                ->leftJoin('curriculum', 'curriculum.id', '=', 'program.curriculum_id')
                ->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'program.eductional_systems_id')
                ->leftJoin('subject', 'subject.id', '=', 'program.subject_id')          
                ->leftJoin('grade', 'grade.id', '=', 'program.grade_id')
                ->leftJoin('levels', 'levels.id', '=', 'program.level_id');           
        }

        return $this->model->select('program.*','curriculum.curriculum_name','eductional_systems.name','subject.subject_name','levels.levels_name','grade.grade_name')
                ->leftJoin('curriculum', 'curriculum.id', '=', 'program.curriculum_id')
                ->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'program.eductional_systems_id')
                ->leftJoin('subject', 'subject.id', '=', 'program.subject_id')          
                ->leftJoin('grade', 'grade.id', '=', 'program.grade_id')
                ->leftJoin('levels', 'levels.id', '=', 'program.level_id');
    }
}
