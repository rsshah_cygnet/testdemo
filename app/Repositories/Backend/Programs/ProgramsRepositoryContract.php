<?php

namespace App\Repositories\Backend\Programs;

/**
 * Interface ProgramsRepositoryContract
 *
 * @package App\Repositories\Backend\Programs
 */
interface ProgramsRepositoryContract
{
    /**
     * Destroy Item
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);

    /* Create Item
     *
     * @param array $input
     */
    public function create($input);

    public function findOrThrowException($id);

    public function mark($id,$status);
}
