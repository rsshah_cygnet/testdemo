<?php

namespace App\Repositories\Backend\SessionFeedback;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\SessionFeedback\SessionFeedback;
use App\Repositories\Backend\DbRepository;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentSessionFeedbackRepository extends DbRepository implements SessionFeedbackRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'session_feedbacks.report_text',
		'front_user.posted_by',
		'front_user.posted_for',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'Session Feedback',
		'Posted By',
		'Posted For',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'session_feedbacks',
	];

	public $downloadfilename = "Session Feedback";

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'session_feedbacks.created_at';

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(SessionFeedback $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(SessionFeedback::find($id))) {
			return SessionFeedback::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['created_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['updated_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$user = $this->findOrThrowException($id);

		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		/*if ($active)
			        {
			            $query = $query->where('status', $active);
		*/

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('session_feedbacks.*', \DB::raw("CONCAT(posted_by.first_name, ' ', posted_by.last_name) as posted_by"), \DB::raw("CONCAT(posted_for.first_name, ' ', posted_for.last_name) as posted_for"))
				->leftJoin('front_user as posted_by', 'posted_by.id', '=', 'session_feedbacks.posted_by')
				->leftJoin('front_user as posted_for', 'posted_for.id', '=', 'session_feedbacks.posted_for');
		}

		return $this->model->select('session_feedbacks.*', \DB::raw("CONCAT(posted_by.first_name, ' ', posted_by.last_name) as posted_by"), \DB::raw("CONCAT(posted_for.first_name, ' ', posted_for.last_name) as posted_for"))
			->leftJoin('front_user as posted_by', 'posted_by.id', '=', 'session_feedbacks.posted_by')
			->leftJoin('front_user as posted_for', 'posted_for.id', '=', 'session_feedbacks.posted_for');

	}
}
