<?php

namespace App\Repositories\Backend\MenuManagement;

/**
 * Interface MenuManagementRepositoryContract
 *
 * @author Anuj Jaha ayjaha@cygnet-infotech.com
 * @package App\Repositories\Backend\MenuManagement
 */

interface MenuManagementRepositoryContract
{   
    /* Create Menu Item
     *
     * @param array $input
     */
    public function create($input);

    /**
     * Update Menu Item
     *
     * @param int $id
     * @param array $input
     */
    public function update($id, $input);

    /**
     * Destroy Menu Item
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);
}
