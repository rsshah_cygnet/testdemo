<?php

namespace App\Repositories\Backend\MenuManagement;

/**
 * Class EloquentMenuManagementRepository
 *
 * @author Anuj Jaha ayjaha@cygnet-infotech.com
 */

use App\Exceptions\GeneralException;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\MenuManagement\MenuManagementRepositoryContract;
use App\Models\MenuManagement\Menus;

/**
 * Class EloquentMenuManagementRepository
 * 
 * @package App\Repositories\Backend\MenuManagement
 */
class EloquentMenuManagementRepository extends DbRepository implements MenuManagementRepositoryContract
{
    /**
     * Grid Display Columns
     * 
     * @var array
     */
    public $gridColumn = [
        'menus.name',
        'parentmenu.parent',
        'domains.domain',
        'menus.status',
        'users.first_name',
        'menus.created_at'
    ];

    /**
     * Grid Display Columns
     * 
     * @var array
     */
    public $gridColumnHeader = [
        'Menu Name',
        'Parent Menu',
        'Website',
        'Status',
        'Created By',
        'Created At'
    ];

    /**
     * Grid Default Order By
     * 
     * @var string
     */
    public $gridDefaultOrderBy = 'menus.name';

    /**
     * Grid Display Columns
     * 
     * @var array
     */
    public $relations = [
        'menus',
        'users'
    ];

    /**
     * Download File Name
     * 
     * @var string
     */
    public $downloadfilename = "Menu Management";

    /**
     * Comman search variable of this repositery
     * 
     * @var string
     */
    public $searchtop;

    /**
     * Record Active Status
     * 
     * @var string
     */
    public $statusActive = "Active";    

    /**
     * Record InActive Status
     * 
     * @var string
     */
    public $statusInActive = "InActive";

    /**
     * Related model of this repositery
     * 
     * @var object
     */
    public $model;

    public function __construct(Menus $model)
    {
        $this->model = $model;
    }

    /**
     * Create Menu Management
     * 
     * @param array $input
     * @return boolean
     * @throws GeneralException
     */
    public function create($input = array()) 
    {   
        if(count($input))
        {
            $input = $this->addCreatedByInfo($input);
            $input = $this->getRecordStatus($input);
            return $this->model->create($input);
        }
        
        throw new GeneralException(trans('exceptions.backend.article.create_error'));
    }

    /**
     * Add Created By Info
     * 
     * @param array $input
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }
        
        return [];
    }

     /**
     * Get Record Status
     * 
     * @param array $input
     */
    public function getRecordStatus($input = array())
    {
        if(count($input))
        {
            if(! isset($input['record_status']) || $input['record_status'] == 0)
            {
                $status = $this->statusInActive;
            }
            else
            {
                $status = $this->statusActive;
            }
            
            $input = array_merge($input, [ 'status' => $status]);


            return $input;
        }
        
        return [];
    }

    /**
     * Add Updated By Info
     * 
     * @param array $input
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }
        
        return [];
    }

    /**
     * @param int $id
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function update($id = null, $input = array())
    {
        if(isset($id) && count($input))
        {
            $model = $this->findOrThrowException($id);

            $input = $this->addUpdatedByInfo($input);
            $input = $this->getRecordStatus($input);

            return $model->update($input);    
        }
        
        throw new GeneralException(trans('exceptions.backend.article.update_error'));
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * search by fields
     * @param int $per_page
     * @param boolean $active
     * @param string $order_by
     * @param string $sort
     * @param boolean $searchtop
     * @param mixed $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') 
    {
        $query = $this->_getGridJoin();

        if($other != '' && $other != "()") 
        {
            $query = $query->whereRaw($other);
        }

        if ($active) 
        {
            $query = $query->where('status', $active);
        }
        
        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);
        
        return $query;
    }

    /**
     * Get Count
     * 
     * @param string $where
     * @return int count
     */
    public function getCount($where) 
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()") 
        {   
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll() 
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }

    /**
     * Get Grid Join
     * 
     * @param object $query
     * @return object 
     */
    public function _getGridJoin($query = null)
    {
       if($query)
        {
            return $query->select('menus.*','domains.domain', 'users.first_name' , 'parentmenu.name as parent')
                            ->leftJoin('domains', 'domains.id', '=', 'menus.domain_id')
                            ->leftJoin('cmspages', 'cmspages.id', '=', 'menus.cmspage_id')
                            ->leftJoin('users', 'users.id', '=', 'menus.created_by')
                            ->leftJoin('menus as parentmenu', 'parentmenu.id', '=', 'menus.parent_menu_id')->where('menus.parent', '!=', 0);    
        }
            return $this->model->select('menus.*','domains.domain', 'users.first_name', 'parentmenu.name as parent')
                        ->leftJoin('domains', 'domains.id', '=', 'menus.domain_id')
                        ->leftJoin('cmspages', 'cmspages.id', '=', 'menus.cmspage_id')
                        ->leftJoin('users', 'users.id', '=', 'menus.created_by')
                        ->leftJoin('menus as parentmenu', 'parentmenu.id', '=', 'menus.parent_menu_id')->where('menus.parent_menu_id', '!=', 0 );    
    }

    /**
     * @param  $id
     * @param  $status
     * @throws GeneralException
     * @return bool
     */
    public function mark($id, $status) 
    {      
        $user = $this->findOrThrowException($id);
        $user->status = $status;
        if ($user->save()) 
        {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }
}

