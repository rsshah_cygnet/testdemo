<?php

namespace App\Repositories\Backend\Frontusers;

/**
 * Interface FrontusersRepositoryContract
 *
 * @author Sudhir Virpara
 * @package App\Repositories\Backend\Frontusers
 */
interface FrontusersRepositoryContract {
	/* Create Item
		     *
		     * @param array $input
	*/
	// public function create($input);

	/**
	 * Update Item
	 *
	 * @param int $id
	 * @param array $input
	 */
	// public function update($id, $input);

	/**
	 * Destroy Item
	 *
	 * @param  $id
	 * @return mixed
	 */
	public function destroy($id);

	public function findOrThrowException($id);

	public function mark($id, $status);

	/**
	 * Get Tutors
	 */
	public function getTutors();

	/**
	 * Get Tutees
	 */
	public function getTutees();

	/**
	 * Get tutors who not applied for test
	 */
	public function getTutorNotAppliedForTest();

	/**
	 * Get Tutor who not completed test
	 */
	public function getTutorWithIncompeleteTest();

	/**
	 * get Tutor who fail in fail in selected subject
	 */
	public function getTutorFailInTest($value = '');

	/**
	 * get use who failed in one of the subject
	 */
	public function getTutorFailedInOneOfSubject();

	/**
	 * Get user who applied for one / multiple tests and passed all tests
	 */
	public function getTutorPassedAllTest();
}
