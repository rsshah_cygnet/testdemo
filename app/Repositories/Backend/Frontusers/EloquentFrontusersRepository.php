<?php

namespace App\Repositories\Backend\Frontusers;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */
use App\Exceptions\GeneralException;
use App\Models\Frontusers\Frontusers;
use App\Models\TutorDetails\TutorDetails;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Backend\Frontusers\FrontusersRepositoryContract;
use Illuminate\Support\Facades\Mail;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentFrontusersRepository extends DbRepository implements FrontusersRepositoryContract {

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'front_user.first_name',
		'front_user.last_name',
		'front_user.email',
		'front_user.status',
		'front_user.role',
		'timezone.timezone_name',
		'country.country_name',
		'city.city_name',
		'front_user.last_login_date',
		'front_user.news_mail_date',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'First Name',
		'Last Name',
		'Email',
		'Status',
		'Role',
		'Timezone',
		'Country',
		'City',
		'Last Login Time',
		'Send Reminder On',
	];

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'front_user.created_at';

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'users',
	];

	/**
	 * Download File Name
	 *
	 * @var string
	 */
	public $downloadfilename = "Users";

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Status of Users
	 * @var Array()
	 */
	public $record_status = [

		'InActive',
		'Active',
	];

	/**
	 * Record Active Status
	 *
	 * @var string
	 */
	public $statusActive = "Active";

	/**
	 * Record InActive Status
	 *
	 * @var string
	 */
	public $statusInActive = "InActive";

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Frontusers $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(Frontusers::find($id))) {
			return Frontusers::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
	}

	public function findByname($id, $name) {
		$model = Frontusers::where('username', '=', $name)->where('id', '!=', $id)->first();
		return count($model);
	}
	public function findByemail($id, $name) {
		$model = Frontusers::where('email', '=', $name)->where('id', '!=', $id)->first();
		return count($model);
	}

	public function findAllUsers() {
		$users = Frontusers::select('first_name', 'email')->where(['status' => '1']);
		return $users->get();
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['created_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['updated_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$user = $this->findOrThrowException($id);
		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);

		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {
		if ($query) {
			return $query->select('front_user.*', 'country.country_name', 'city.city_name', 'timezone.timezone_name')
				->leftJoin('timezone', 'timezone.id', '=', 'front_user.timezone_id')
				->leftJoin('country', 'country.id', '=', 'front_user.country_id')
				->leftJoin('city', 'city.id', '=', 'front_user.city_id');
		}

		return $this->model->select('front_user.*', 'country.country_name', 'city.city_name', 'timezone.timezone_name')
			->leftJoin('timezone', 'timezone.id', '=', 'front_user.timezone_id')
			->leftJoin('country', 'country.id', '=', 'front_user.country_id')
			->leftJoin('city', 'city.id', '=', 'front_user.city_id');
	}

	/**
	 * @param $user
	 * @return mixed
	 */
	public function sendReminderEmail($user) {
		// return Mail::send('backend.emails.alert', ['token' => $user->confirmation_code],
		//     function ($message) use ($user) {
		//             $message->to('ngpatel@cygnet-infotech.com', $user->first_name)->subject(app_name() . ': ' . "Gentle reminder");
		//         });

		return Mail::send('backend.emails.alert', ['token' => $user->confirmation_code], function ($message) use ($user) {
			$message->to('ngpatel@cygnet-infotech.com', $user->first_name)->subject(app_name() . ': ' . "Your result has been changed");
		});
	}

	/**
	 * Get Avtive and Inactive Tutors
	 */
	public function getTutors() {
		$tutor = new Frontusers;
		$tutor->ActiveTutor = $this->model->select()->where(function ($query) {
			$query->where('role', 1)
				->orwhere('role', 3);
		})->where('status', 1)->where('deleted_at', null)->count();

		$tutor->InactiveTutor = $this->model->select()->where(function ($query) {
			$query->where('role', 1)
				->orwhere('role', 3);
		})->where('status', 0)->where('deleted_at', null)->count();

		return $tutor;
	}

	/**
	 * Get Avtive and Inactive Tutees
	 */
	public function getTutees() {
		$tutees = new Frontusers;
		$tutees->ActiveTutees = $this->model->select()->where(function ($query) {
			$query->where('role', 2)
				->orwhere('role', 3);
		})->where('status', 1)->where('deleted_at', null)->count();

		$tutees->InactiveTutees = $this->model->select()->where(function ($query) {
			$query->where('role', 2)
				->orwhere('role', 3);
		})->where('status', 0)->where('deleted_at', null)->count();

		return $tutees;
	}

	/**
	 * Get tutors who not applied for test
	 */
	public function getTutorNotAppliedForTest() {

		// $tutors = $this->model
		// 	->select('id')
		// 	->where('status', 0)
		// 	->where(function ($query) {
		// 		$query->where('role', 1)
		// 			->orwhere('role', 3);
		// 	})
		// 	->get()->toArray();
		// dd($tutors);
		// $tutorAppliedTests = TutorDetails::select('front_user_id as id')
		// 	->leftJoin('front_user', 'front_user.id', '=', 'tutor_details.front_user_id')
		// 	->where('front_user.status', 1)
		// 	->groupBy('front_user_id')->get()->toArray();
		// // dd($tutorAppliedTests);
		// $aTmp1 = [];
		// foreach ($tutors as $aV) {
		// 	$aTmp1[] = $aV['id'];
		// }

		// if ($tutorAppliedTests != null) {
		// 	foreach ($tutorAppliedTests as $aV) {
		// 		$aTmp2[] = $aV['id'];
		// 	}
		// } else {
		// 	$aTmp2 = array();
		// }
		// print_r($aTmp1);
		// print_r($aTmp2);
		// // dd($aTmp1);

		// $user = COUNT(array_diff($aTmp1, $aTmp2));
		// dd($user);
		$tutors = $this->model
			->Join('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
		//->select('id')
			->select('tutor_details.front_user_id')
			->where('front_user.status', 0)
			->where(function ($query) {
				$query->where('front_user.role', 1)
					->orwhere('front_user.role', 3);
			})
			->where('front_user.deleted_at', null)
			->groupBy('tutor_details.front_user_id')
		// ->count();
			->get()->toArray();
		$tutors = COUNT($tutors);
		return $tutors;
	}

	/**
	 *  Get Tutor who not completed test
	 */
	public function getTutorWithIncompeleteTest() {

		$user = TutorDetails::leftjoin('front_user', 'front_user.id', '=', 'tutor_details.front_user_id')
			->where('front_user.status', 1)
			->where('tutor_details.is_completed', 0)
			->where('tutor_details.appeared', 1)
			->groupBy('tutor_details.front_user_id')
			->get()->toArray();
		$getUsers = COUNT($user);
		return $getUsers;
	}

	/**
	 * get Tutor who fail in fail in selected subject
	 */
	public function getTutorFailInTest($value = '') {
		$user = TutorDetails::where('passed_status', 1)->groupBy('front_user_id')->get()->toArray();

		$getUsers = COUNT($user);
		return $getUsers;
	}

	/**
	 * get use who failed in one of the subject
	 */
	public function getTutorFailedInOneOfSubject() {
		$user = TutorDetails::leftJoin('front_user', 'front_user.id', '=', 'tutor_details.front_user_id')
			->select('tutor_details.id')
			->where('tutor_details.passed_status', 0)
			->where('tutor_details.appeared', 1)
			->where('tutor_details.is_completed', 1)
			->where('front_user.status', 1)
			->where('front_user.deleted_at', null)
			->groupBy('tutor_details.front_user_id')
			->get()->toArray();
		$getUsers = COUNT($user);
		return $getUsers;
	}

	/**
	 * Get user who applied for one / multiple tests and passed all tests
	 */
	public function getTutorPassedAllTest() {

		// $user = \DB::select("SELECT  `front_user_id` FROM  `tutor_details` WHERE  `passed_status` =1 AND  `front_user_id` NOT IN (SELECT  `front_user_id` FROM  `tutor_details` WHERE  `passed_status` =  '0' GROUP BY  `front_user_id`)GROUP BY  `front_user_id`");
		$user = \DB::select("SELECT  `front_user_id` FROM  `tutor_details` WHERE  (`passed_status` =1) AND  `front_user_id` NOT IN (SELECT  `front_user_id` FROM  `tutor_details` WHERE  `passed_status` =  '0' GROUP BY  `front_user_id`)GROUP BY  `front_user_id`");
		// dd($user);
		return $getUsers = COUNT($user);
	}

	/**
	 * @param array $data
	 * @param bool $provider
	 * @return static
	 */
	public function create_basic(array $data, $provider = false) {

		$error = array();
		if (isset($data['hidden_user_id']) && $data['hidden_user_id'] != "" && $data['hidden_user_id'] != 0) {
			//$obj = UserEducation::find($id);
			$obj = $this->findOrThrowException($data['hidden_user_id']);
			// $data_username = $this->findByname($data['hidden_user_id'],$data['username']);
			$data_email = $this->findByemail($data['hidden_user_id'], $data['email']);

			if ($data_email >= 1) {
				$error['email_error'] = 'Email already exist';
			}
		} else {
			$obj = new Frontusers;
			//$model = Frontusers::where('username', '=', $data['username'])->first();
			if (count($this->model->where('email', $data['email'])->first()) >= 1) {
				$error['email_error'] = 'Email already exist';
			}
		}

		if (!empty($error)) {
			return json_encode($error);
		} else {
			$data = array('id' => 'success');
			return json_encode($data);
			/*
				            $obj->first_name = isset($data['first_name']) ? $data['first_name'] : '';
				            $obj->role = isset($data['hidden_user_role']) ? $data['hidden_user_role'] : '';
				            $obj->last_name = isset($data['last_name']) ? $data['last_name'] : '';
				            $obj->username = isset($data['username']) ? $data['username'] : '';
				            $obj->password = isset($data['password']) ? md5($data['password']) : '';
				            $obj->email = isset($data['email']) ? $data['email'] : '';
				            $obj->nationality = isset($data['nationality']) ? $data['nationality'] : '';
				            $obj->dob = isset($data['dob']) ? date("Y-m-d", strtotime($data['dob'])) : '';
				            $obj->photo = isset($data['photo']) ? $data['photo'] : '';
				            $obj->country_id = isset($data['country_id']) ? $data['country_id'] : '';
				            $obj->state_id = isset($data['state_id']) ? $data['state_id'] : '';
				            $obj->city_id = isset($data['city_id']) ? $data['city_id'] : '';
				            $obj->timezone_id = isset($data['timezone_id']) ? $data['timezone_id'] : '';
				            $obj->lang_id = isset($data['lang_id']) ? $data['lang_id'] : '';
				            $obj->status = isset($data['status']) ? $data['status'] : '2';
				            if ($obj->save())
				            {
				                $array = array('id' => $obj->id);
				                return json_encode($array);
				            }
			*/
		}
	}

}
