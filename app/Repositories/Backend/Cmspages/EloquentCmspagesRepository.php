<?php

namespace App\Repositories\Backend\Cmspages;

use App\Exceptions\GeneralException;
use App\Models\Cmspage\Cmspage;
use App\Repositories\Backend\DbRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class EloquentCmspagesRepository
 * @package App\Repositories\Cmspages\Cmspages
 */
class EloquentCmspagesRepository extends DbRepository implements CmspagesRepositoryContract {

	public $model;

	/**
	 * comman search variable of this repositery
	 * @var string
	 */
	public $searchtop;

	/**
	 *
	 * @param Cmspage $model
	 */
	public function __construct(Cmspage $model) {
		$this->model = $model;
	}

	/**
	 * create cms page
	 * @param type $request
	 * @return boolean
	 * @throws GeneralException
	 */
	public function create($request) {
		$input = $request->all();
		// pre($input);
		if ($this->model->where('title', $input['title'])->first()) {
			throw new GeneralException(trans('exceptions.backend.already_exists'));
		}
		$obj = new Cmspage;
		$obj->title = $input['title'];
		$obj->page_slug = str_slug($input['title']);
		// $obj->page_slug = $this->latestSlug($page_slug);

		//$obj->cannonical_link = $input['cannonical_link'];
		$obj->seo_title = $input['seo_title'];
		$obj->description = $input['description'];
		$obj->seo_keyword = $input['seo_keyword'];
		$obj->seo_description = $input['seo_description'];
		$obj->is_active = isset($input['record_status']) && $input['record_status'] == '1' ? 'Active' : 'Inactive';
		$obj->created_by = $request->user()->id;
		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.create_error'));
	}

	/**
	 * update cms page
	 * @param type $id
	 * @param type $request
	 * @return boolean
	 * @throws GeneralException
	 */
	public function update($id, $request) {
		$obj = $this->findOrThrowException($id);
		$input = $request->all();
		$obj->title = $input['title'];
		//$obj->cannonical_link = $input['cannonical_link'];
		$obj->seo_title = $input['seo_title'];
		$obj->description = $input['description'];
		$obj->seo_keyword = $input['seo_keyword'];
		$obj->seo_description = $input['seo_description'];
		$obj->is_active = isset($input['record_status']) && $input['record_status'] == '1' ? 'Active' : 'Inactive';
		$obj->created_by = $obj->created_by;
		$obj->updated_by = $request->user()->id;
		// dd($input);
		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.access.roles.update_error'));
	}

	/**
	 * active inactive from grid
	 * @param type $id
	 * @param type $status
	 * @return boolean
	 * @throws GeneralException
	 */
	public function mark($id, $status) {
		$obj = $this->findOrThrowException($id);
		$obj->is_active = $status;
		$obj->updated_by = Auth::user()->id;
		if ($obj->save()) {
			return true;
		}
		throw new GeneralException(trans('exceptions.backend.mark_error'));
	}

	/**
	 * find by pageslug
	 * @param type $page_slug
	 * @return type
	 * @throws GeneralException
	 */
	public function findBySlug($page_slug) {
		if (!is_null($this->model->wherePage_slug($page_slug)->firstOrFail())) {
			return $this->model->wherePage_slug($page_slug)->firstOrFail();
		}
		throw new GeneralException(trans('exceptions.backend.not_found'));
	}

	/**
	 *
	 * @param type $page_slug
	 * @return type
	 * @throws GeneralException
	 */
	public function latestSlug($page_slug) {
		if (!is_null($this->model->whereRaw("page_slug RLIKE '^{$page_slug}(-[0-9]*)?$'")->latest('id') - pluck('page_slug'))) {
			$latestSlug = $this->model->whereRaw("page_slug RLIKE '^{$page_slug}(-[0-9]*)?$'")->latest('id') - pluck('page_slug');
			$pieces = explode('-', $latestSlug);
			$number = end($pieces);
			return $page_slug . '-' . ($number + 1);
		}
		throw new GeneralException(trans('exceptions.backend.not_found'));
	}

	/**
	 * search by fields
	 * @param type $per_page
	 * @param type $active
	 * @param type $order_by
	 * @param type $sort
	 * @param type $searchtop
	 * @param type $other
	 * @return type
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->model;
		/* if( count($other) > 0 && $other !='' )
			          {
			          foreach($other as $k => $v )
			          {
			          $query = $query->where($k, 'like', '%'.$v.'%');
			          }
		*/
		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}
		if ($searchtop != '') {
			$this->searchtop = $searchtop;
			$query = $query->where(function ($query) {
				$query->where('name', 'like', '%' . $this->searchtop . '%')
					->orWhere('email', 'like', '%' . $this->searchtop . '%');
			});
		}
		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);
		return $query;
	}

	public function getCount($where) {
		$query = $this->model;
		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}
		$count = $query->count();
		return $count;
	}

	public function getcAll() {
		ini_set('memory_limit', '-1');
		$count = $this->model->get();
		return $count;
	}

	/**
	 * get cms page data by PageSlug
	 * @param  $input
	 * @throws GeneralException
	 * @return bool
	 */
	public function cmsDataByPageSlug($input) {
		$pageSlug = $input['PageSlug'];

		return $result = $this->model
			->where(config('backend.cmspage_table') . '.page_slug', '=', $pageSlug)
			->get();
	}

	// /**
	//  * @param  $page_slug
	//  *
	//  * @throws GeneralException
	//  * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null|static
	//  */
	// public function findBySlug($page_slug)
	// {
	//     if (! is_null($this->model->wherePage_slug($page_slug)->firstOrFail())) {
	//         return $this->model->wherePage_slug($page_slug)->firstOrFail();
	//     }
	//     throw new GeneralException(trans('exceptions.backend.not_found'));
	// }

}
