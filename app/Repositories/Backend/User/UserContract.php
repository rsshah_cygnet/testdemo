<?php

namespace App\Repositories\Backend\User;

/**
 * Interface UserContract
 * @package App\Repositories\User
 */
interface UserContract {

	/**
	 * @param  $id
	 * @param  bool    $withRoles
	 * @return mixed
	 */
	public function findOrThrowException($id, $withRoles = false);

	/**
	 * @param  $per_page
	 * @param  string      $order_by
	 * @param  string      $sort
	 * @param  $status
	 * @return mixed
	 */
	public function getUsersPaginated($per_page, $status = 1, $order_by = 'id', $sort = 'asc');

	/**
	 * @param  $per_page
	 * @return \Illuminate\Pagination\Paginator
	 */
	public function getDeletedUsersPaginated($per_page);

	/**
	 * @param  string  $order_by
	 * @param  string  $sort
	 * @return mixed
	 */
	public function getAllUsers($order_by = 'id', $sort = 'asc');

	/**
	 * @param $input
	 * @param $roles
	 * @param $permissions
	 * @return mixed
	 */
	public function create($input, $roles, $permissions);

	/**
	 * @param $id
	 * @param $input
	 * @param $roles
	 * @param $permissions
	 * @return mixed
	 */
	public function update($id, $input, $roles, $permissions);

	/**
	 * @param  $id
	 * @return mixed
	 */
	public function destroy($id);

	/**
	 * @param  $id
	 * @return mixed
	 */
	public function delete($id);

	/**
	 * @param  $id
	 * @return mixed
	 */
	public function restore($id);

	/**
	 * @param  $id
	 * @param  $status
	 * @return mixed
	 */
	public function mark($id, $status);

	/**
	 * @param  array $columns
	 * @param  string  $sort
	 * @param  string  $order_by
	 * @return mixed
	 */
	public function selectAll($columns = '*', $order_by = 'id', $sort = 'asc');

	/**
	 * @param  $id
	 * @param  $input
	 * @return mixed
	 */
	public function updatePassword($id, $input);

	/**
	 * @param  $id
	 * @param  $input
	 * @return mixed
	 */
	public function validateRoleAmount($user, $roles);

	/**
	 * Check Email Already exist or not
	 * @param type $input
	 * @param type $user
	 */

	public function checkUserByEmail($input, $user);

	/**
	 * Check Mobile User Already exist or not
	 * @param type $email
	 */

	public function checkUserAlreadyExist($email);

	/**
	 * Fetch User by filed and it's value
	 * @param type $fieldname
	 * @param type $value
	 */

	public function fetchUserByField($fieldname, $value);

	/**
	 * update or create entry for usertoken
	 * @param type $userId
	 * @param type $type
	 * @param type $deviceToken
	 */

	public function updateUserToken($userId, $type, $deviceToken);

	/**
	 * check user token
	 * @param type $userId
	 * @param type $userToken
	 * @param type $deviceToken
	 */

	public function checkUserToken($userId, $userToken = NULL, $deviceToken = NULL);

	/**
	 * generate random string
	 * @param type $length
	 * @param type $type
	 */

	public function randomString($length, $type = '');

	/**
	 * delete user token
	 * @param type $userId
	 * @param type $userToken
	 */
	public function deleteUserToken($userId, $userToken);

	/**
	 * flush user roles
	 * @param type $roles
	 * @param type $user
	 */
	public function flushRoles($roles, $user);

	/**
	 * flush user permissions
	 * @param type $permissions
	 * @param type $user
	 */
	public function flushPermissions($permissions, $user);

	/**
	 * check user roles
	 * @param type $roles
	 */
	public function checkUserRolesCount($roles);

	/**
	 * save user data
	 * @param type $input
	 */
	public function createUserStub($input);

	/**
	 * get all data
	 * @param type $where
	 */
	public function getCount($where);

	/**
	 * get all data
	 */
	public function getcAll();

	/**
	 * update user password
	 * @param type $id
	 * @param type $input
	 */
	public function updateAppPassword($id, $input);

	/**
	 * save passkey Reminder
	 * @param type $id
	 * @param type $input
	 */
	public function savePasskeyReminder($id, $input);

	/**
	 * save passkey
	 * @param type $id
	 */
	public function savePasskey($id);

	/**
	 * validate passkey by userid
	 * @param type $passkey
	 * @param type $id
	 */
	public function validatePasskeyByUserid($passkey, $id);

	/**
	 * Get Admin Users
	 */
	public function getAdminUser();

	/**
	 * get upcoming session
	 */
	public function getUpcomingSession();

	/**
	 * Get tutor with highest punctuality ratings
	 */
	public function getTutorWithHighestPunctualityRatings();
}
