<?php

namespace App\Repositories\Backend\User;

use App\Exceptions\Backend\Access\User\UserNeedsRolesException;
use App\Exceptions\GeneralException;
use App\Models\Access\User\User;
use App\Models\Session\Session;
use App\Models\Usertoken\Usertoken;
use App\Repositories\Backend\Role\RoleRepositoryContract;
use App\Repositories\Frontend\User\UserContract as FrontendUserContract;

/**
 * Class EloquentUserRepository
 * @package App\Repositories\User
 */
class EloquentUserRepository implements UserContract {

	/**
	 * comman search variable of this repositery
	 * @var string
	 */
	public $searchtop;

	/**
	 * @var RoleRepositoryContract
	 */
	protected $role;

	/**
	 * @var FrontendUserContract
	 */
	protected $user;

	/**
	 * @param RoleRepositoryContract $role
	 * @param FrontendUserContract $user
	 */
	public function __construct(
		RoleRepositoryContract $role, FrontendUserContract $user, User $model, Usertoken $userToken
	) {
		$this->role = $role;
		$this->user = $user;
		$this->model = $model;
		$this->userToken = $userToken;
	}

	/**
	 * @param  $id
	 * @param  bool               $withRoles
	 * @throws GeneralException
	 * @return mixed
	 */
	public function findOrThrowException($id, $withRoles = false) {
		if ($withRoles) {
			$user = User::with('roles')->withTrashed()->find($id);
		} else {
			$user = User::withTrashed()->find($id);
		}

		if (!is_null($user)) {
			return $user;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.not_found'));
	}

	/**
	 * @param  $per_page
	 * @param  string      $order_by
	 * @param  string      $sort
	 * @param  int         $status
	 * @return mixed
	 */
	public function getUsersPaginated($per_page, $status = 1, $order_by = 'id', $sort = 'asc') {

		$query = User::where('status', $status)->where('deleted_at', null)
			->orderBy($order_by, $sort)
			->paginate($per_page);
		return json_encode($query);
	}

	/**
	 * @param  $per_page
	 * @param  integer    $active
	 * @param  string     $order_by
	 * @param  string     $sort
	 * @param  string     $searchtop
	 * @param  array      $other
	 * @return mixed
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {

		$query = $this->model;
		/* if( count($other) > 0 && $other !='' )
			          {
			          foreach($other as $k => $v )
			          {
			          $query = $query->where($k, 'like', '%'.$v.'%');
			          }
		*/
		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('users.status', $active);
		}
		// dd('in');
		if ($searchtop != '') {
			// dd('seatch');
			$this->searchtop = $searchtop;
			$query = $query->where(function ($query) {
				$query->where('users.first_name', 'like', '%' . $this->searchtop . '%')
					->orWhere('users.last_name', 'like', '%' . $this->searchtop . '%')
					->orWhere('users.contact_no', 'like', '%' . $this->searchtop . '%')
					->orWhere('users.email', 'like', '%' . $this->searchtop . '%');
			});
		}

		$query = commonUserListCondition($query); //This is helper function to get filtered users by roles

		$query = $query->leftJoin('country', 'country.id', '=', 'users.country_id')
			->leftJoin('city', 'city.id', '=', 'users.city_id')
			->select('users.*', 'country.country_name', 'city.city_name')
			->orderBy('users.' . $order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * @param  $per_page
	 * @return \Illuminate\Pagination\Paginator
	 */
	public function getDeletedUsersPaginated($per_page) {

		$query = User::onlyTrashed()
			->paginate($per_page);
//        dd(json_encode($query));
		return json_encode($query);
	}

	/**
	 * @param  string  $order_by
	 * @param  string  $sort
	 * @return mixed
	 */
	public function getAllUsers($order_by = 'id', $sort = 'asc') {
		return User::orderBy($order_by, $sort)
			->get();
	}

	/**
	 * @param  array $columns
	 * @param  string  $sort
	 * @param  string  $order_by
	 * @return mixed
	 */
	public function selectAll($columns = '*', $order_by = 'id', $sort = 'asc') {
		return User::select($columns)->orderBy($order_by, $sort)->get();
	}

	/**
	 * @param  $input
	 * @param  $roles
	 * @param  $permissions
	 * @throws GeneralException
	 * @throws UserNeedsRolesException
	 * @return bool
	 */
	public function create($input, $roles, $permissions) {
		$user = $this->createUserStub($input);
		if ($user->save()) {
			//User Created, Validate Roles
			$this->validateRoleAmount($user, $roles['assignees_roles']);

			//Attach new roles
			$user->attachRoles($roles['assignees_roles']);

			//Attach other permissions
			$current = explode(',', $permissions['permissions']);
			$userPermissions = [];

			if (count($current)) {
				foreach ($current as $perm) {
					if (is_numeric($perm)) {
						array_push($userPermissions, $perm);
					}
				}
			}
			$user->attachPermissions($userPermissions);

			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
	}

	/**
	 * @param $id
	 * @param $input
	 * @param $roles
	 * @param $permissions
	 * @return bool
	 * @throws GeneralException
	 */
	public function update($id, $input, $roles, $permissions) {
		$user = $this->findOrThrowException($id);
		$this->checkUserByEmail($input, $user);

		if ($user->update($input)) {
			//For whatever reason this just wont work in the above call, so a second is needed for now
			$user->status = isset($input['status']) ? 1 : 0;
			$user->confirmed = 1;
			$user->save();

			$this->checkUserRolesCount($roles);
			$this->flushRoles($roles, $user);
			$this->flushPermissions($permissions, $user);

			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
	}

	/**
	 * @param  $id
	 * @param  $input
	 * @throws GeneralException
	 * @return bool
	 */
	public function updatePassword($id, $input) {
		$user = $this->findOrThrowException($id);

		//Passwords are hashed on the model
		$user->password = bcrypt($input['password']);
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.update_password_error'));
	}

	/**
	 * @param  $id
	 * @throws GeneralException
	 * @return bool
	 */
	public function destroy($id) {
		if (auth()->id() == $id) {
			throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
		}

		$user = $this->findOrThrowException($id);
		if ($user->delete()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
	}

	/**
	 * @param  $id
	 * @throws GeneralException
	 * @return boolean|null
	 */
	public function delete($id) {
		$user = $this->findOrThrowException($id, true);

		//Detach all roles & permissions
		$user->detachRoles($user->roles);
		$user->detachPermissions($user->permissions);

		try
		{
			$user->forceDelete();
		} catch (\Exception $e) {
			throw new GeneralException($e->getMessage());
		}
	}

	/**
	 * @param  $id
	 * @throws GeneralException
	 * @return bool
	 */
	public function restore($id) {
		$user = $this->findOrThrowException($id);

		if ($user->restore()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		if (access()->id() == $id && $status == 0) {
			throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
		}

		$user = $this->findOrThrowException($id);
		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Check to make sure at lease one role is being applied or deactivate user
	 *
	 * @param  $user
	 * @param  $roles
	 * @throws UserNeedsRolesException
	 */
	public function validateRoleAmount($user, $roles) {
		//Validate that there's at least one role chosen, placing this here so
		//at lease the user can be updated first, if this fails the roles will be
		//kept the same as before the user was updated
		if (count($roles) == 0) {
			//Deactivate user
			$user->status = 0;
			$user->save();

			$exception = new UserNeedsRolesException();
			$exception->setValidationErrors(trans('exceptions.backend.access.users.role_needed_create'));

			//Grab the user id in the controller
			$exception->setUserID($user->id);
			throw $exception;
		}
	}

	/**
	 *
	 * @param type $email
	 * @return boolean
	 */
	public function checkUserAlreadyExist($email) {
		$result = $this->model
			->where(array(array('email', $email)))
			->count();
		if ($result > 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 *
	 * @param type $fieldname
	 * @param type $value
	 * @param type $joins
	 * @return boolean
	 */
	public function fetchUserByField($fieldname, $value, $joins = array()) {
		$result = $this->model
			->where($fieldname, '=', $value)
			->select(config('access.users_table') . '.*')
			->get();
		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/**
	 *
	 * @param type $userId
	 * @param type $type
	 * @param type $deviceToken
	 * @return type
	 */
	public function updateUserToken($userId, $type, $deviceToken) {
		$check = $this->checkUserToken($userId, NULL, $deviceToken);
		if ($check->count()) {
			$userToken = $check->get()->first();
		} else {
			$userToken = new Usertoken();
			$userToken->user_id = $userId;
			$userToken->type = $type;
			$userToken->device_token = $deviceToken;
		}
		$userToken->user_token = $this->randomString(20);
		$userToken->save();
		return $userToken->user_token;
	}

	/**
	 *
	 * @param type $userId
	 * @param type $userToken
	 * @param type $deviceToken
	 * @return type
	 */
	public function checkUserToken($userId, $userToken = NULL, $deviceToken = NULL) {
		$check = $this->userToken
			->where('user_id', '=', $userId);
		if ($userToken) {
			$check->where('user_token', '=', $userToken);
		}
		if ($deviceToken) {
			$check->where('device_token', '=', $deviceToken);
		}
		return $check;
	}

	/**
	 *
	 * @param type $userId
	 * @param type $userToken
	 * @return boolean
	 */
	public function deleteUserToken($userId, $userToken) {
		$userToken = $this->checkUserToken($userId, $userToken)->get()->first();
		$userToken->user_token = NULL;
		if ($userToken->save()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param  $input
	 * @param  $user
	 * @throws GeneralException
	 */
	public function checkUserByEmail($input, $user) {
		//Figure out if email is not the same
		if ($user->email != $input['email']) {
			//Check to see if email exists
			if (User::where('email', '=', $input['email'])->first()) {
				throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
			}
		}
	}

	/**
	 *
	 * @param type $length
	 * @param type $type
	 * @return string
	 */
	public function randomString($length, $type = '') {
		// Select which type of characters you want in your random string
		switch ($type) {
		case 'num':
			// Use only numbers
			$salt = '1234567890';
			break;
		case 'lower':
			// Use only lowercase letters
			$salt = 'abcdefghijklmnopqrstuvwxyz';
			break;
		case 'upper':
			// Use only uppercase letters
			$salt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			break;
		default:
			// Use uppercase, lowercase, numbers, and symbols
			$salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			break;
		}
		$rand = '';
		$i = 0;
		while ($i < $length) {
			// Loop until you have met the length
			$num = rand() % strlen($salt);
			$tmp = substr($salt, $num, 1);
			$rand = $rand . $tmp;
			$i++;
		}
		return $rand; // Return the random string
	}

	/**
	 * @param $roles
	 * @param $user
	 */
	public function flushRoles($roles, $user) {
		//Flush roles out, then add array of new ones
		$user->detachRoles($user->roles);
		$user->attachRoles($roles['assignees_roles']);
	}

	/**
	 * @param $permissions
	 * @param $user
	 */
	public function flushPermissions($permissions, $user) {
		//Flush permissions out, then add array of new ones if any
		$user->detachPermissions($user->permissions);

		//Attach permissions if the role does not have all access
		$current = explode(',', $permissions['permissions']);
		$userPermissions = [];

		if (count($current)) {
			foreach ($current as $perm) {
				if (is_numeric($perm)) {
					array_push($userPermissions, $perm);
				}
			}
		}
		$user->attachPermissions($userPermissions);
	}

	/**
	 * @param  $roles
	 * @throws GeneralException
	 */
	public function checkUserRolesCount($roles) {
		//User Updated, Update Roles
		//Validate that there's at least one role chosen
		if (count($roles['assignees_roles']) == 0) {
			throw new GeneralException(trans('exceptions.backend.access.users.role_needed'));
		}
	}

	/**
	 * @param  $input
	 * @return mixed
	 */
	public function createUserStub($input) {
		$user = new User;
		$user->first_name = $input['first_name'];
		$user->last_name = $input['last_name'];
		$user->country_id = $input['country_id'];
		$user->city_id = $input['city_id'];
		$user->email = $input['email'];
		$user->password = bcrypt($input['password']);
		$user->contact_no = $input['contact_no'];
		//$user->province_region_id = $input['province_region_id'];
		$user->status = isset($input['status']) ? 1 : 0;
		$user->confirmation_code = md5(uniqid(mt_rand(), true));
		$user->confirmed = 1;
		return $user;
	}

	public function getCount($where) {

		$query = $this->model;
		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}
		$count = $query->leftJoin('country', 'country.id', '=', 'users.country_id')
			->leftJoin('city', 'city.id', '=', 'users.city_id')
			->select('users.*', 'country.country_name', 'city.city_name')->where('users.deleted_at', null)->count();

		return $count;
	}

	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->model;
		$count = $query->get();
		return $count;
	}

	/**
	 * update password from API(UserController) - changePassword function
	 * @param  $id
	 * @param  $input
	 * @throws GeneralException
	 * @return bool
	 */
	public function updateAppPassword($id, $input) {
		$user = $this->findOrThrowException($id);

		//Passwords are hashed on the model
		$user->password = bcrypt($input['NewPassword']);
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.update_password_error'));
	}

	/**
	 * store passkey_reminder in users_table from UserController API - savePasskeyReminder function
	 * @param  $id
	 * @param  $input
	 * @throws GeneralException
	 * @return bool
	 */
	public function savePasskeyReminder($id, $input) {
		$user = $this->findOrThrowException($id);

		$user->passkey_reminder = $input['PasskeyReminderMinutes'];
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
	}

	/**
	 * store passkey_reminder in users_table from UserController API - savePasskeyReminder function
	 * @param  $id
	 * @param  $input
	 * @throws GeneralException
	 * @return bool
	 */
	public function savePasskey($id) {
		$user = $this->findOrThrowException($id);

		$user->passkey = rand(1000, 9999);
		if ($user->save()) {
			return $user->passkey;
		}
		throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
	}

	/**
	 * validate userid and passkey
	 * @param  $id
	 * @param  $passkey
	 * @throws GeneralException
	 * @return bool
	 */
	public function validatePasskeyByUserid($passkey, $id) {
		$user = $this->findOrThrowException($id);

		$result = $this->model
			->where(array(array('id', $id), array('passkey', $passkey['Passkey'])))
			->count();
		if ($result) {
			return true;
		} else {
			return false;
		}
		throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
	}

	/**
	 * Get Active and Inactive admin users
	 */
	public function getAdminUser() {
		$AdminUsers = new User;
		$AdminUsers->ActiveAdminUsers = $this->model->select()->where('status', 1)->where('deleted_at', null)->count();
		$AdminUsers->InactiveAdminUsres = $this->model->select()->where('status', 0)->where('deleted_at', null)->count();
		if ($AdminUsers->ActiveAdminUsers > 0) {
			$AdminUsers->ActiveAdminUsers = $AdminUsers->ActiveAdminUsers - 1;
		}
		return $AdminUsers;
	}

	/**
	 * get upcoming session
	 */
	public function getUpcomingSession() {
		$session = new Session;
		$session->upcomingSession = Session::select('session.*', \DB::raw('CONCAT(front_user.first_name, " ", front_user.last_name) as tutor_name'), \DB::raw('CONCAT(tutee.first_name, " ", tutee.last_name) as tutee_name'), 'subject.subject_name', 'topic.topic_name', 'topic.id as topic_id', 'subject.id as subject_id')
			->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
			->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
			->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
			->where('session.status', 2)
			->orderBy('session.id', 'desc')
			->take(5)->get();

		$session->successfulSession = Session::select('session.*', \DB::raw('CONCAT(front_user.first_name, " ", front_user.last_name) as tutor_name'), \DB::raw('CONCAT(tutee.first_name, " ", tutee.last_name) as tutee_name'), 'subject.subject_name', 'topic.topic_name', 'topic.id as topic_id', 'subject.id as subject_id')
			->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
			->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
			->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
			->where('session.status', 1)
			->orderBy('session.id', 'desc')
			->take(5)->get();

		$session->cancelByTutor = Session::select('session.*', \DB::raw('CONCAT(front_user.first_name, " ", front_user.last_name) as tutor_name'), \DB::raw('CONCAT(tutee.first_name, " ", tutee.last_name) as tutee_name'), 'subject.subject_name', 'topic.topic_name', 'topic.id as topic_id', 'subject.id as subject_id')
			->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
			->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
			->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
			->where('session.status', 3)
			->orderBy('session.id', 'desc')
			->take(5)->get();

		$session->cancelByTutee = Session::select('session.*', \DB::raw('CONCAT(front_user.first_name, " ", front_user.last_name) as tutor_name'), \DB::raw('CONCAT(tutee.first_name, " ", tutee.last_name) as tutee_name'), 'subject.subject_name', 'topic.topic_name', 'topic.id as topic_id', 'subject.id as subject_id')
			->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
			->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
			->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
			->where('session.status', 4)
			->orderBy('session.id', 'desc')
			->take(5)->get();

		return $session;
	}

	/**
	 * Get tutor with highest punctuality ratings
	 */
	public function getTutorWithHighestPunctualityRatings() {
		$tutor = \DB::Select("select  tutorId, tutor_name, rating_1 + rating_2 as totalrating, rating_1 as system_rating, rating_2 as tutee_rating, qualification_name
				from (select front_user.id as tutorId, CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name, AVG(session.punctuality_ratings) rating_1, AVG(tutor_ratings.rating) rating_2, qualification.qualification_name
					from session, front_user, tutor_ratings, user_educational, qualification
					where `front_user`.`id` = `session`.`tutor_id`
					and  `front_user`.`id` = `tutor_ratings`.`tutor_id`
					and  `front_user`.`id` = `user_educational`.`front_user_id`
					and   `user_educational`.`qualification_id` = `qualification`.`id`
					group by front_user.id) as q
					order by totalrating DESC limit 5");

		return $tutor;
	}
}
