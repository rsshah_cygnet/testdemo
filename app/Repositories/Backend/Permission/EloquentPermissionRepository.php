<?php

namespace App\Repositories\Backend\Permission;

use App\Exceptions\GeneralException;
use App\Models\Access\Permission\Permission;
use App\Repositories\Backend\Permission\Dependency\PermissionDependencyRepositoryContract;
use App\Repositories\Backend\Role\RoleRepositoryContract;

/**
 * Class EloquentPermissionRepository
 * @package App\Repositories\Permission
 */
class EloquentPermissionRepository implements PermissionRepositoryContract {

	/**
	 *
	 * @var type
	 */
	public $model;

	/**
	 * @var RoleRepositoryContract
	 */
	protected $roles;

	/**
	 * @var PermissionDependencyRepositoryContract
	 */
	protected $dependencies;

	/**
	 * @param RoleRepositoryContract                 $roles
	 * @param PermissionDependencyRepositoryContract $dependencies
	 */
	public function __construct(
		RoleRepositoryContract $roles, PermissionDependencyRepositoryContract $dependencies, Permission $model
	) {
		$this->roles = $roles;
		$this->dependencies = $dependencies;
		$this->model = $model;
	}

	/**
	 * @param  $id
	 * @param  bool                                                                             $withRoles
	 * @throws GeneralException
	 * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null|static
	 */
	public function findOrThrowException($id, $withRoles = false) {
		if (!is_null(Permission::find($id))) {
			if ($withRoles) {
				return Permission::with('roles')->find($id);
			}

			return Permission::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.permissions.not_found'));
	}

	/**
	 * @param  $per_page
	 * @param  string      $order_by
	 * @param  string      $sort
	 * @return mixed
	 */
	public function getPermissionsPaginated($per_page, $order_by = 'display_name', $sort = 'asc') {
		return Permission::with('roles')->orderBy($order_by, $sort)->paginate($per_page);
	}

	/**
	 * @param  string  $order_by
	 * @param  string  $sort
	 * @param  bool    $withRoles
	 * @return mixed
	 */
	public function getAllPermissions($order_by = 'display_name', $sort = 'asc', $withRoles = true) {
		if ($withRoles) {
			return Permission::with('roles', 'dependencies.permission')->orderBy($order_by, $sort)->get();
		}

		return Permission::with('dependencies.permission')->orderBy($order_by, $sort)->get();
	}

	/**
	 * @return mixed
	 */
	public function getUngroupedPermissions() {
		return Permission::whereNull('group_id')
			->orderBy('display_name', 'asc')
			->get();
	}

	/**
	 * @param  $input
	 * @param  $roles
	 * @throws GeneralException
	 * @return bool
	 */
	public function create($input, $roles) {
		$permission = new Permission;
		$permission->name = $input['name'];
		$permission->display_name = $input['display_name'];
		$permission->system = 0;
		$permission->group_id = isset($input['group']) && strlen($input['group']) > 0 ? (int) $input['group'] : null;
		$permission->sort = isset($input['sort']) ? (int) $input['sort'] : 0;

		if ($permission->save()) {
			//For each role, load role, collect perms, add perm to perms, flush perms, read perms
			if (count($roles['permission_roles']) > 0) {
				foreach ($roles['permission_roles'] as $role_id) {
					//Get the role, with permissions
					$role = $this->roles->findOrThrowException($role_id, true);

					//Get the roles permissions into an array
					$role_permissions = $role->permissions->lists('id')->all();

					if (count($role_permissions) >= 1) {
						//Role has permissions, gather them first
						//Add this new permission id to the role
						array_push($role_permissions, $permission->id);

						//For some reason the lists() casts as a string, convert all to int
						$role_permissions_temp = array();
						foreach ($role_permissions as $rp) {
							array_push($role_permissions_temp, (int) $rp);
						}
						$role_permissions = $role_permissions_temp;

						//Sync the permissions to the role
						$role->permissions()->sync($role_permissions);
					} else {
						//Role has no permissions, add the 1
						$role->permissions()->sync([$permission->id]);
					}
				}
			}

			//Add the dependencies of this permission if any
			$dependencies = explode(",", $input['permissions']);
			if (isset($dependencies) && count($dependencies) > 1) {
				foreach ($dependencies as $dependency_id) {
					if (is_numeric($dependency_id)) {
						$this->dependencies->create($permission->id, $dependency_id);
					}
				}
			}

			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.permissions.create_error'));
	}

	/**
	 * @param  $id
	 * @param  $input
	 * @param  $roles
	 * @throws GeneralException
	 * @return bool
	 */
	public function update($id, $input, $roles) {
		$permission = $this->findOrThrowException($id);
		$permission->name = $input['name'];
		$permission->display_name = $input['display_name'];
		$permission->system = isset($input['system']) ? 1 : 0;
		$permission->group_id = isset($input['group']) && strlen($input['group']) > 0 ? (int) $input['group'] : null;
		$permission->sort = isset($input['sort']) ? (int) $input['sort'] : 0;

		if ($permission->save()) {
			//Detach permission from every role, then add the permission to the selected roles
			$currentRoles = $this->roles->getAllRoles();
			foreach ($currentRoles as $role) {
				$role->detachPermission($permission);
			}

			if (count($roles['permission_roles']) > 0) {
				//For each role, load role, collect perms, add perm to perms, flush perms, read perms
				foreach ($roles['permission_roles'] as $role_id) {
					//Get the role, with permissions
					$role = $this->roles->findOrThrowException($role_id, true);

					//Get the roles permissions into an array
					$role_permissions = $role->permissions->lists('id')->all();

					if (count($role_permissions) >= 1) {
						//Role has permissions, gather them first
						//Add this new permission id to the role
						array_push($role_permissions, $permission->id);

						//For some reason the lists() casts as a string, convert all to int
						$role_permissions_temp = array();
						foreach ($role_permissions as $rp) {
							array_push($role_permissions_temp, (int) $rp);
						}
						$role_permissions = $role_permissions_temp;

						//Sync the permissions to the role
						$role->permissions()->sync($role_permissions);
					} else {
						//Role has no permissions, add the 1
						$role->permissions()->sync([$permission->id]);
					}
				}
			}

			//Add the dependencies of this permission if any
			$dependencies = explode(",", $input['permissions']);

			if (isset($dependencies) && count($dependencies)) {
				//Remove all current dependencies
				$this->dependencies->clear($permission->id);

				foreach ($dependencies as $dependency_id) {
					if (is_numeric($dependency_id)) {
						$this->dependencies->create($permission->id, $dependency_id);
					}
				}
			} else {
				//None checked, remove any if they were there prior
				$this->dependencies->clear($permission->id);
			}

			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.permissions.update_error'));
	}

	/**
	 * @param  $id
	 * @throws GeneralException
	 * @return bool
	 */
	public function destroy($id) {
		$permission = $this->findOrThrowException($id);

		if ($permission->system == 1) {
			throw new GeneralException(trans('exceptions.backend.access.permissions.system_delete_error'));
		}

		//Remove the permission from all associated roles
		$currentRoles = $permission->roles;
		foreach ($currentRoles as $role) {
			$role->detachPermission($permission);
		}

		//Remove the permission from all associated users
		$currentUsers = $permission->users;
		foreach ($currentUsers as $user) {
			$user->detachPermission($permission);
		}

		//Remove the dependencies
		$permission->dependencies()->delete();

		if ($permission->delete()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.permissions.delete_error'));
	}

	/**
	 * @param  $per_page
	 * @param  integer    $active
	 * @param  string     $order_by
	 * @param  string     $sort
	 * @param  string     $searchtop
	 * @param  array      $other
	 * @return mixed
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '', $page = "") {
		$query = $this->model;
		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		if ($active) {
			$query = $query->where('status', $active);
		}
		if ($searchtop != '') {
			$this->searchtop = $searchtop;
			$query = $query->where(function ($query) {
				$query->where('name', 'like', '%' . $this->searchtop . '%');
			});
		}
//        \DB::connection()->enableQueryLog();

		$query = $this->_getGridJoin($query);
		$query = $query->orderBy($order_by, $sort);

		$offset = ($page - 1) * $per_page;
		$query = $query->limit($per_page)->offset($offset)->get();

//        $query = \DB::getQueryLog();
		//        $lastQuery = end($query);
		//        dd($lastQuery);

		return $query;
	}

	private function _getGridJoin($query) {

		return $query->leftJoin('permission_user', 'permission_user.permission_id', '=', 'permissions.id')
			->leftJoin('users', 'users.id', '=', 'permission_user.user_id')
			->leftJoin('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
			->leftJoin('roles', 'roles.id', '=', 'permission_role.role_id')
			->leftJoin('permission_groups', 'permission_groups.id', '=', 'permissions.group_id')
			->leftJoin(\DB::raw("(select pd.permission_id as pdid, group_concat(p.display_name SEPARATOR ',\n') as dependencies from permissions p "
				. "join permission_dependencies pd where pd.dependency_id=p.id group by pd.permission_id) as dependancy"), "dependancy.pdid", "=", "permissions.id")
			->select([
				'permissions.id',
				'permissions.name AS permission',
				'permissions.display_name AS name',
				\DB::raw('GROUP_CONCAT(DISTINCT users.first_name SEPARATOR ",\n" ) as users'),
				\DB::raw('GROUP_CONCAT(DISTINCT roles.name SEPARATOR ",\n" ) as roles'),
			])
			->groupBy('permissions.id')
		;
	}

	/**
	 * getCount
	 * get list count for grid
	 *
	 * @param type $where
	 * @return type
	 */
	public function getCount($where, $status = "") {

		$query = $this->model;
		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}
		// dd($query->count());
		// $count = 23;

//        if ($status != '') {
		//            $query = $query->where('status', $status);
		//            $count = $query->count();
		//        } else {
		//            $query = $this->_getGridJoin($query);
		//            $count = count($query->get()->toArray());
		//        }
		return $query->count();
	}

}
