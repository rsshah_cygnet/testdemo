<?php

namespace App\Repositories\Backend\Dealership;

/**
 * Class EloquentDealershipRepository
 *
 * @author Brijesh Khatri bbkhatri@cygnet-infotech.com
 */

use App\Exceptions\GeneralException;
use App\Repositories\Backend\DbRepository;
use App\Models\Dealership\Dealership;
use Illuminate\Support\Facades\DB;
use App\Http\Utilities\FileUploads;

/**
 * Class EloquentDealershipRepository
 *
 * @package App\Repositories\Backend\Dealership
 */
class EloquentDealershipRepository extends DbRepository implements DealershipRepositoryContract
{
    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumn = [
        'dealerships.dealership_name',
        'dealerships.dealership_code',
        'dealership_floors.floors_count',
        'dealerships.dealership_email',
        'dealerships.status'
    ];

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumnHeader = [
        'Dealership Name',
        'Dealership Code',
        'Floor',
        'Dealership Email',
        'Status'
    ];

    /**
     * Grid Default Order By
     *
     * @var string
     */
    public $gridDefaultOrderBy = 'dealerships.dealership_name';

    public $filePath = "banner_image";

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $relations = [
        'dealerships',
        'dealership_floors'
    ];

    /**
     * Download File Name
     *
     * @var string
     */
    public $downloadfilename = "downloadfilename";

    /**
     * Comman search variable of this repository
     *
     * @var string
     */
    public $searchtop;

    /**
     * Record Active Status
     *
     * @var string
     */
    public $statusActive = "Active";

    /**
     * Record InActive Status
     *
     * @var string
     */
    public $statusInActive = "InActive";

    /**
     * Related model of this repository
     *
     * @var object
     */
    public $model;

    public function __construct(Dealership $model)
    {
        $this->model = $model;
        $this->uploadManager    = new FileUploads;
    }

    /**
     * Create Dealership Model
     *
     * @param array $input
     * @return boolean
     * @throws GeneralException
     */
    public function create($input = array())
    {
        if(count($input))
        {
            $input = $this->addCreatedByInfo($input);
            $input = $this->getRecordStatus($input);

            return $this->model->create($input);
        }

        throw new GeneralException(trans('exceptions.backend.article.create_error'));
    }

    /**
     * Add Created By Info
     *
     * @param array $input
     * @return array
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

    /**
     * Get Record Status
     *
     * @param array $input
     * @return array
     */
    public function getRecordStatus($input = array())
    {
        if (count($input)) {
            if (!isset($input['status']) || $input['status'] == 'InActive') {
                $status = $this->statusInActive;
            } else {
                $status = $this->statusActive;
            }

            $input = array_merge($input, ['status' => $status]);

            return $input;
        }

        return [];
    }

    /**
     * Add Updated By Info
     *
     * @param array $input
     * @return array
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

    /**
     * @param int $id
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function update($id = null, $input = array())
    {
        if(isset($id) && count($input))
        {
            $model = $this->findOrThrowException($id);

            $input = $this->addUpdatedByInfo($input);
            $input = $this->getRecordStatus($input);

            return $model->update($input);
        }

        throw new GeneralException(trans('exceptions.backend.article.update_error'));
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }
         /**
     * Upload Logo
     *
     * @param array $input
     * @return mixed
     */
    public function uploadLogo($input = array())
    {
        if(isset($input['banner_image']) && !empty($input['banner_image']))
        {
            $fileName = $this->uploadManager->setBasePath($this->filePath)
                ->setThumbnailFlag(false)
                ->upload($input['banner_image']);

            if($fileName)
            {
                return $fileName;
            }
        }

        return false;
    }

    /**
     * @param int $id
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function updateDealershipProfile($id = null, $input = array())
    {
        if(isset($id) && count($input))
        {
            $model = $this->findOrThrowException($id);

            $input = $this->addUpdatedByInfo($input);
            $input = $this->getRecordStatus($input);
            $banner = $this->uploadLogo($input);

            if($banner)
            {
                $input = array_merge($input, ['banner_image' => $banner]);
            }
            return $model->update($input);
        }
        throw new GeneralException(trans('exceptions.backend.article.update_error'));
    }

    /**
     * Active/InActive from grid
     *
     * @param type $id
     * @param type $status
     * @return boolean
     * @throws GeneralException
     */
    public function mark($id, $status) {
        $obj = $this->findOrThrowException($id);
        $obj->status = $status;
        $obj->updated_by = auth()->user()->id;
        if ($obj->save()) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.mark_error'));
    }

    /**
     * Search by fields
     *
     * @param $per_page
     * @param string $active
     * @param string $order_by
     * @param string $sort
     * @param string $searchtop
     * @param string $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '')
    {
        $query = $this->_getGridJoin();

        if($other != '' && $other != "()")
        {
            $query = $query->whereRaw($other);
        }

        if ($active)
        {
            $query = $query->where('status', $active);
        }

        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);

        return $query;
    }

    /**
     * Get Count
     *
     * @param string $where
     * @return int count
     */
    public function getCount($where)
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()")
        {
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll()
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }

    /**
     * Get Grid Join
     *
     * @param object $query
     * @return object
     */
    public function _getGridJoin($query = null)
    {
        if($query)
        {
            return $query->select('dealerships.*', 'users.first_name', DB::raw('count(dealership_floors.id) as floors_count'))
                ->leftJoin('dealership_floors', 'dealership_floors.dealership_id', '=', 'dealerships.id')
                ->leftJoin('users', 'users.id', '=', 'dealership_floors.user_id')
                ->groupby('dealerships.id');
        }

        return $this->model->select('dealerships.*', 'users.first_name', DB::raw('count(dealership_floors.id) as floors_count'))
            ->leftJoin('dealership_floors', 'dealerships.id', '=', 'dealership_floors.dealership_id')
            ->leftJoin('users', 'users.id', '=', 'dealership_floors.user_id')
            ->groupby('dealerships.id');

    }

    /**
     * @param null $userID
     * @return mixed
     */
    public function getDealershipByUser($userID = null)
    {
        $query = $this->model->leftJoin('user_dealerships', 'dealerships.id', '=', 'user_dealerships.dealership_id');
        if ($userID) {
            $query = $query->where('user_dealerships.user_id', $userID);
        }
        $query = $query->orWhereNull('user_dealerships.dealership_id');
        $query = $query->where('dealerships.status', 'Active');
        $query = $query->whereNull('dealerships.deleted_at');
        $query = $query->pluck('dealerships.dealership_name', 'dealerships.id');

        return $query;
    }

    /**
     * [getDealershipsFromProvince description]
     * @param  [type] $province_id [province_id from findADealerController]
     * @return [type] $query = array(dealerships)
     */
    public function getDealershipsFromProvince($province_id)
    {
        $query = \DB::table('provinces')->select('dealerships.dealership_name')->leftJoin('province_regions', 'province_regions.province_id', '=', 'provinces.id')
            ->leftJoin('dealerships', 'dealerships.province_region_id', '=', 'province_regions.id')
            ->where('provinces.id', $province_id)
            ->where('dealership_name','!=',null)
            ->get();

        return $query;
    }
}
