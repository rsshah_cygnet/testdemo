<?php

namespace App\Repositories\Backend\Dealership;

/**
 * Interface DealershipRepositoryContract
 *
 * @author Brijesh Khatri bbkhatri@cygnet-infotech.com
 * @package App\Repositories\Backend\Dealership
 */

interface DealershipRepositoryContract
{
    /**
     * Update Menu Item
     *
     * @param int $id
     * @param array $input
     */
    public function update($id, $input);

    /**
     * Destroy Menu Item
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);
}
