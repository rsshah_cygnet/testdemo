<?php

namespace App\Repositories\Backend\SessionUpcoming;

/**
 * Class EloquentLevelsRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\Session\Session;
use App\Repositories\Backend\SessionUpcoming\SessionUpcomingRepositoryContract;
use App\Repositories\Backend\DbRepository;
use DB;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentSessionUpcomingRepository extends DbRepository implements SessionUpcomingRepositoryContract
{
    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumn = [
        'front_user.tutor_name',
         'front_user.tutee_name',
        'session.accepted_date_time',
        'session.scheduled_date_time',
        'topic.topic_name',
        'subject.subject_name',
        'session.session_fee'

    ];

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $gridColumnHeader = [
        'Tutor Name',
        'Tutee Name',
        'Accepted date & time',
        'Conduct date & time',
        'Topic',
        'Subject',
        'Session Fee'
    ];

    /**
     * Grid Default Order By
     *
     * @var string
     */
    public $gridDefaultOrderBy = 'session.id';

    /**
     * Grid Display Columns
     *
     * @var array
     */
    public $relations = [
        'session'
    ];

    /**
     * Download File Name
     *
     * @var string
     */
    public $downloadfilename = "session_upcoming";

    /**
     * Comman search variable of this repositery
     *
     * @var string
     */
    public $searchtop;


    /**
     * Status of Users
     * @var Array()
     */
    public $record_status = [

        'InActive',
        'Active'

    ];
    /**
     * Record Active Status
     *
     * @var string
     */
    public $statusActive = "Active";

    /**
     * Record InActive Status
     *
     * @var string
     */
    public $statusInActive = "InActive";

   
    /**
     * Related model of this repositery
     *
     * @var object
     */
    public $model;

    public function __construct(Session $model)
    {
        $this->model = $model;
    }


    public function findOrThrowException($id) {
        if (!is_null(Session::find($id))) {
            return Session::find($id);
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
    }


    /**
     * Add Created By Info
     *
     * @param array $input
     */
    public function addCreatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'created_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

     

    /**
     * Add Updated By Info
     *
     * @param array $input
     */
    public function addUpdatedByInfo($input = array())
    {
        if(count($input))
        {
            $input = array_merge($input, [ 'updated_by' => getLoggedInUser()]);

            return $input;
        }

        return [];
    }

    


    /**
     * @param  $id
     * @param  $status
     * @throws GeneralException
     * @return bool
     */
    public function mark($id, $status) {
        //echo $status;exit;
        $user = $this->findOrThrowException($id);
        $user->status = $status;
        if ($user->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * Destroy Record
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->findOrThrowException($id);

        if($model)
        {
            return $model->delete();
        }

        throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * search by fields
     * @param int $per_page
     * @param boolean $active
     * @param string $order_by
     * @param string $sort
     * @param boolean $searchtop
     * @param mixed $other
     * @return object
     */
    public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'desc', $searchtop = '', $other = '')
    {
        $query = $this->_getGridJoin();

        if($other != '' && $other != "()")
        {
            $query = $query->whereRaw($other);
        }

        if ($active)
        {
            $query = $query->where('status',$active);
        }

        $query = $query->orderBy($order_by, $sort);
        $query = $query->paginate($per_page);

        return $query;
    }

    /**
     * Get Count
     *
     * @param string $where
     * @return int count
     */
    public function getCount($where)
    {
        $query = $this->_getGridJoin();

        if ($where != '' && $where != "()")
        {
            $query = $query->whereRaw($where);
        }

        return $query->count();
    }

    /**
     * Get Count All
     *
     * @return count
     */
    public function getcAll()
    {
        ini_set('memory_limit', '-1');
        $query = $this->_getGridJoin();

        return $query->get();
    }

    
    /**
     * Get Grid Join
     *
     * @param object $query
     * @return object
     */
    public function _getGridJoin($query = null)
    {
         if($query)
        {
            return $query->select('session.*','subject.subject_name','topic.topic_name','front_user.first_name',DB::raw("CONCAT(tutee.first_name, ' ', tutee.last_name) as tutee_name"),DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
               ->leftJoin('subject', 'subject.id', '=', 'session.subject_id')  
                  ->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
                  ->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
                  ->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
                   ->where('session.status',2);
                
        }

        return $this->model->select('session.*','subject.subject_name','topic.topic_name','front_user.first_name',DB::raw("CONCAT(tutee.first_name, ' ', tutee.last_name) as tutee_name"),DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
               ->leftJoin('subject', 'subject.id', '=', 'session.subject_id')  
                  ->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
                  ->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
                  ->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
                  ->where('session.status',2);
            

    }
}
