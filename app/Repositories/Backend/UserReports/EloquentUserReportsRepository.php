<?php

namespace App\Repositories\Backend\UserReports;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\UserReports\UserReports;
use App\Repositories\Backend\DbRepository;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentUserReportsRepository extends DbRepository implements UserReportsRepositoryContract {
	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumn = [
		'user_reports.reported_text',
		'user_reports.user_id',
		'user_reports.reported_user_id',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $gridColumnHeader = [
		'Report text',
		'Posted By',
		'Posted For',
	];

	/**
	 * Grid Display Columns
	 *
	 * @var array
	 */
	public $relations = [
		'user_reports',
	];

	public $downloadfilename = "User Reports By Tutor";

	/**
	 * Grid Default Order By
	 *
	 * @var string
	 */
	public $gridDefaultOrderBy = 'user_reports.created_at';

	/**
	 * Comman search variable of this repositery
	 *
	 * @var string
	 */
	public $searchtop;

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(UserReports $model) {
		$this->model = $model;
	}

	public function findOrThrowException($id) {
		if (!is_null(UserReports::find($id))) {
			return UserReports::find($id);
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
	}

	/**
	 * Add Created By Info
	 *
	 * @param array $input
	 */
	public function addCreatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['created_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * Add Updated By Info
	 *
	 * @param array $input
	 */
	public function addUpdatedByInfo($input = array()) {
		if (count($input)) {
			$input = array_merge($input, ['updated_by' => getLoggedInUser()]);

			return $input;
		}

		return [];
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @throws GeneralException
	 * @return bool
	 */
	public function mark($id, $status) {
		//echo $status;exit;
		$user = $this->findOrThrowException($id);

		$user->status = $status;
		if ($user->save()) {
			return true;
		}

		throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
	}

	/**
	 * Destroy Record
	 *
	 * @param int $id
	 * @return mixed
	 * @throws GeneralException
	 */
	public function destroy($id) {
		$model = $this->findOrThrowException($id);
		// dd($model);
		if ($model) {
			return $model->delete();
		}

		throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
	}

	/**
	 * search by fields
	 * @param int $per_page
	 * @param boolean $active
	 * @param string $order_by
	 * @param string $sort
	 * @param boolean $searchtop
	 * @param mixed $other
	 * @return object
	 */
	public function getPaginatedSearch($per_page, $active = '', $order_by = 'id', $sort = 'asc', $searchtop = '', $other = '') {
		$query = $this->_getGridJoin();

		if ($other != '' && $other != "()") {
			$query = $query->whereRaw($other);
		}

		/*if ($active)
			        {
			            $query = $query->where('status', $active);
		*/

		$query = $query->orderBy($order_by, $sort);
		$query = $query->paginate($per_page);

		return $query;
	}

	/**
	 * Get Count
	 *
	 * @param string $where
	 * @return int count
	 */
	public function getCount($where) {
		$query = $this->_getGridJoin();

		if ($where != '' && $where != "()") {
			$query = $query->whereRaw($where);
		}

		return $query->count();
	}

	/**
	 * Get Count All
	 *
	 * @return count
	 */
	public function getcAll() {
		ini_set('memory_limit', '-1');
		$query = $this->_getGridJoin();

		return $query->get();
	}

	/**
	 * Get Grid Join
	 *
	 * @param object $query
	 * @return object
	 */
	public function _getGridJoin($query = null) {

		if ($query) {
			return $this->model->select('user_reports.*', \DB::raw("CONCAT(user_id.first_name, ' ', user_id.last_name) as user_id"), \DB::raw("CONCAT(reported_user_id.first_name, ' ', reported_user_id.last_name) as reported_user_id"))
				->leftJoin('front_user as user_id', 'user_id.id', '=', 'user_reports.user_id')
				->leftJoin('front_user as reported_user_id ', 'reported_user_id.id', '=', 'user_reports.reported_user_id')
				->where('user_reports.reported_role', 1);
		}
		// dd('sd');
		return $this->model->select('user_reports.*', \DB::raw("CONCAT(user_id.first_name, ' ', user_id.last_name) as user_id"), \DB::raw("CONCAT(reported_user_id.first_name, ' ', reported_user_id.last_name) as reported_user_id"))
			->leftJoin('front_user as user_id', 'user_id.id', '=', 'user_reports.user_id')
			->leftJoin('front_user as reported_user_id ', 'reported_user_id.id', '=', 'user_reports.reported_user_id')
			->where('user_reports.reported_role', 1);

	}
}
