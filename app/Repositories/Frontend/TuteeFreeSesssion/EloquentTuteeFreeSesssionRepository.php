<?php

namespace App\Repositories\Frontend\TuteeFreeSesssion;

/**
 * Class EloquentTuteeFreeSessionrRepository
 *
 */

use App\Models\Frontusers\Frontusers;
use DB;

/**
 * Class EloquentTuteeFreeSesssionRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentTuteeFreeSesssionRepository implements TuteeFreeSesssionRepositoryContract {
	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Frontusers $model) {
		$this->model = $model;
	}

	/**
	 * Get tutee free session
	 */
	public function getTuteeFreeSesssion() {
		$id = \Session::get('front_user_id');
		$count = new Frontusers;
		$count->sessionShareCount = DB::table('front_user')->select('session_share_count')->where('id', $id)->first();
		$freeSessionPerFBCount = DB::table('settings')->select('free_session_per_fb_count')->first();
		// dd($count->sessionShareCount->session_share_count);
		if ($freeSessionPerFBCount->free_session_per_fb_count != 0) {
			$freeSession = $count->sessionShareCount->session_share_count / $freeSessionPerFBCount->free_session_per_fb_count;
			$count->freeSession = floor($freeSession);
			// dd($count->freeSession);
		}

		$data['free_session_per_fb_count'] = $freeSessionPerFBCount->free_session_per_fb_count;
		$data['users_free_session'] = (isset($count->freeSession) && $count->freeSession != 0 && $count->freeSession != "")?$count->freeSession:"";
		
		return $data;
	}
}
