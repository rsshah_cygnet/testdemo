<?php

namespace App\Repositories\Frontend\TuteeFreeSesssion;

/**
 * Interface TutorRepositoryContract
 *
 */
interface TuteeFreeSesssionRepositoryContract {

	/**
	 * Get tutee free session
	 */
	public function getTuteeFreeSesssion();
}
