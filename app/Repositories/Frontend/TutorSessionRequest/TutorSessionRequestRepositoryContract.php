<?php

namespace App\Repositories\Frontend\TutorSessionRequest;

/**
 * Interface FrontusersRepositoryContract
 *
 * @package App\Repositories\\Frontusers
 */

interface TutorSessionRequestRepositoryContract {
	/* Create Item
		     *
		     * @param array $input
	*/
	// public function create($input);

	/**
	 * Update Item
	 *
	 * @param int $id
	 * @param array $input
	 */
	// public function update($id, $input);

	/**
	 * get tutor pending session
	 *
	 * @return mixed
	 */
	public function getPendingSession();

	/**
	 * accept tutee session request
	 *
	 * $id as session id
	 */
	public function acceptSessionRequest($id);

	/**
	 * decline tutee session request
	 *
	 * $id as session id
	 */
	public function declineSessionRequest($id);

}
