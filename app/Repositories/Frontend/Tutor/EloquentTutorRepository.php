<?php

namespace App\Repositories\Frontend\Tutor;

/**
 * Class EloquentTutorRepository
 *
 */

use App\Models\Frontusers\Frontusers;
use DB;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */

class EloquentTutorRepository implements TutorRepositoryContract
{
    /**
     * Related model of this repositery
     *
     * @var object
     */
    public $model;

    public function __construct(Frontusers $model)
    {
        $this->model = $model;
    }

    public function getTutorList($request)
    {
        $loginId = \Session::get('front_user_id');
        $perPage = 5;
        $offset  = (($request['page'] * $perPage) - 1);

        $query = $this->model->select('front_user.id as userId', 'front_user.created_at as userCreatedAt', 'front_user.*', 'user_educational.*', 'country.*', 'subject.*', 'qualification.*', DB::raw("GROUP_CONCAT( DISTINCT (subject.subject_name)  SEPARATOR ', ') as subjects"), 'tutor_preference.*', 'tutor_details.*')
            ->leftJoin('country', 'front_user.country_id', '=', 'country.id')
            ->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'front_user.id')
            ->leftJoin('tutor_preference', 'tutor_preference.tutor_id', '=', 'front_user.id')
            ->leftJoin('qualification', 'user_educational.qualification_id', '=', 'qualification.id')
            ->leftJoin('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
            ->leftjoin('subject', 'tutor_details.subject_id', '=', 'subject.id');

        if (!empty($loginId)) {
            $query = $query->whereNotIn('front_user.id', [$loginId]);
        }

        if (!empty($request['search']['curriculum_id'])) {
            if (isset($request['search']['curriculum_id']) && $request['search']['curriculum_id'] != '') {
                $query = $query->where('tutor_details.curriculum_id', '=', $request['search']['curriculum_id']);
            }
            if (isset($request['education_id']) && $request['education_id'] != '') {
                $query = $query->where('tutor_details.education_system_id', '=', $request['education_id']);
            }
            if (isset($request['level_id']) && $request['level_id'] != '') {
                $query = $query->where('tutor_details.level_id', '=', $request['level_id']);
            }
            if (isset($request['program_id']) && $request['program_id'] != '') {
                $query = $query->where('tutor_details.program_id', '=', $request['program_id']);
            }
            if (isset($request['grade_id']) && $request['grade_id'] != '') {
                $query = $query->where('tutor_details.grade_id', '=', $request['grade_id']);
            }
            if (isset($request['subject_id']) && $request['subject_id'] != '') {
                $query = $query->where('tutor_details.subject_id', '=', $request['subject_id']);
            }
            if (isset($request['topic_id']) && $request['topic_id'] != '') {
                $query = $query->where('tutor_details.topic_id', '=', $request['topic_id']);
            }

        }

        if (!empty($request['search'])) {
            if (isset($request['search']['language']) && $request['search']['language'] != '' && !empty($request['search']['language'])) {

                $query = $query->where(function ($query) use ($request) {

                    foreach ($request['search']['language'] as $key => $value) {
                        if ($key == 0) {
                            $query->whereRaw("find_in_set('" . $value . "',front_user.lang_id)");
                        } else {
                            $query->orWhereRaw("find_in_set('" . $value . "',front_user.lang_id)");
                        }

                    }

                });
            }

            if (isset($request['search']['rating']) && $request['search']['rating'] != '') {
                $query = $query->where('user_educational.rating', '=', $request['search']['rating']);
            }

            if (isset($request['search']['punctual-rating']) && $request['search']['punctual-rating'] != '') {
                $query = $query->where('user_educational.punctuality', '=', $request['search']['punctual-rating']);
            }

             if (isset($request['search']['from']) && $request['search']['from'] != '' && isset($request['search']['to']) && $request['search']['to'] != '') {
	                $from     = $request['search']['from'];
	                $to       = $request['search']['to'];
	                $timefrom = date("H:i:s", strtotime($from));
	                $timeto   = date("H:i:s", strtotime($to));
            }

            if (isset($request['search']['reservation']) && $request['search']['reservation'] != '') {
                $dates      = explode(' - ', $request['search']['reservation']);
                $start_date = date("Y-m-d", strtotime($dates[0]));
                $end_date   = date("Y-m-d", strtotime($dates[1]));

                if(isset($timefrom) && $timefrom != "" && isset($timeto) && $timeto != ""){
                	$from_date_final = ConverTimeZoneForDB($start_date . " " . ConverTimeIn24Hour($timefrom), 'Y-m-d H:i');
	                $to_date_final   = ConverTimeZoneForDB($end_date . " " . ConverTimeIn24Hour($timeto), 'Y-m-d H:i');

	                $timefrom = ConverTimeIn24Hour($from_date_final);
                	$timeto   = ConverTimeIn24Hour($to_date_final);

                	  
                $query = $query->where('tutor_preference.start_time', '<=', $timefrom)
                    ->where('tutor_preference.end_time', '>=', $timefrom)
                    ->where('tutor_preference.start_time', '<=', $timeto)
                    ->where('tutor_preference.end_time', '>=', $timeto);

                }else{
                	$from_date_final = ConverTimeZoneForDB($start_date, 'Y-m-d');
	                $to_date_final   = ConverTimeZoneForDB($end_date, 'Y-m-d');

	            }
                

                $from_date  = convertDateInSqlFormat($from_date_final);
                $to_date    = convertDateInSqlFormat($to_date_final);

                $query = $query->where('tutor_preference.start_date', '<=', $from_date)
                    ->where('tutor_preference.end_date', '>=', $from_date)
                    ->where('tutor_preference.start_date', '<=', $to_date)
                    ->where('tutor_preference.end_date', '>=', $to_date);
            }

           /* if (isset($request['search']['from']) && $request['search']['from'] != '' && isset($request['search']['to']) && $request['search']['to'] != '') {
               
                $query = $query->where('tutor_preference.start_time', '<=', $timefrom)
                    ->where('tutor_preference.end_time', '>=', $timefrom)
                    ->where('tutor_preference.start_time', '<=', $timeto)
                    ->where('tutor_preference.end_time', '>=', $timeto);
            }
            if (isset($request['search']['timezone']) && $request['search']['timezone'] != '') {
        $query = $query->where('front_user.timezone_id', '=', $request['search']['timezone']);
        }
         */
        }

        if (!empty($request['search']['tutor_name'])) {
            $query = $query->where(DB::raw('CONCAT(front_user.first_name," ",front_user.last_name)'), 'LIKE', '%' . $request['search']['tutor_name'] . '%');
        }

        $query = $query->where('front_user.status', 1);
        $query = $query->where('tutor_details.passed_status', 1);
        $query = $query->whereIn('front_user.role', [1, 3]);
        $query = $query->groupBy('front_user.id')

            ->offset($offset)
            ->limit(1)
            ->paginate($perPage);

        if (isset($request['search']) && !empty($request['search'])) {
            $query = $query->appends(['search' => $request['search']]);
        }
        return $query;
    }
}
