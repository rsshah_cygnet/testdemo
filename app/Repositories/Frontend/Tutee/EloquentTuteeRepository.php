<?php

namespace App\Repositories\Frontend\Tutee;

/**
 * Class EloquentTutorRepository
 *
 */

use App\Models\Frontusers\Frontusers;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentTuteeRepository implements TuteeRepositoryContract {
	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Frontusers $model) {
		$this->model = $model;
	}
}
