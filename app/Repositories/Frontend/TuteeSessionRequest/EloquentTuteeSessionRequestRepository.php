<?php

namespace App\Repositories\Frontend\TuteeSessionRequest;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Models\Session\Session;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Frontend\TuteeSessionRequest\TuteeSessionRequestRepositoryContract;
use Carbon\Carbon;
use DB;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentTuteeSessionRequestRepository extends DbRepository implements TuteeSessionRequestRepositoryContract {

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Session $model) {
		$this->model = $model;
	}

	/**
	 * get tutor pending session
	 *
	 * @return mixed
	 */
	public function getPendingSession() {
		$id = \Session::get('front_user_id');
		$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
			->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
			->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
			->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
			->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
			->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.reserved_date_time')
			->where(function ($query) {
				$query->where('session.status',5);
			})
			->where('session.tutee_id', $id)
			->orderBy('id', 'desc')
			->paginate(10);
			
		return $session;
	}

	/**
	 * Decline session request by tutor
	 *
	 * @param $id as session id
	 */
	public function declineSessionRequest($id) {
		$data = Session::find($id);

		$loggedInUser = \Session::get('front_user_id');
		if (!empty($data)) {
			$request = DB::table('session')
				->where('id', $id)
				->update([
					'status' => 8,
					'modified_by' => $loggedInUser,
					'updated_at' => carbon::now()->toDateTimeString(),
					'cancelled_date_time' => carbon::now()->toDateTimeString(),
				]);
			return $request;
		} else {
			return false;
		}
	}
}
