<?php

namespace App\Repositories\Frontend\TuteeSessionRequest;

/**
 * Interface FrontusersRepositoryContract
 *
 * @package App\Repositories\\Frontusers
 */

interface TuteeSessionRequestRepositoryContract {
	/* Create Item
		     *
		     * @param array $input
	*/
	// public function create($input);

	/**
	 * Update Item
	 *
	 * @param int $id
	 * @param array $input
	 */
	// public function update($id, $input);

	/**
	 * get tutor pending session
	 *
	 * @return mixed
	 */
	public function getPendingSession();

	/**
	 * decline tutee session request
	 *
	 * $id as session id
	 */
	public function declineSessionRequest($id);

}
