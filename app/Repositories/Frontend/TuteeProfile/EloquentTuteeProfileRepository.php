<?php

namespace App\Repositories\Frontend\TuteeProfile;

/**
 * Class EloquentTutorRepository
 *
 */

use App\Exceptions\GeneralException;
use App\Models\Frontusers\Frontusers;
use DB;


/**
 * Class EloquentTuteeProfileRepository
 *
 * @package App\Repositories\Frontend\TuteeProfile
 */
class EloquentTuteeprofileRepository implements TuteeProfileRepositoryContract
{
    /**
     * Related model of this repositery
     *
     * @var object
     */
    public $model;

    public function __construct(Frontusers $model)
    {
        $this->model = $model;
    }

    public function getTuteeProfile($request)
    {  
        if(!check_front_login())
        {
               return redirect('/');
        }
        
        $loginTutee = \Session::get('front_user_id'); 

        $result = Frontusers::select('front_user.id as userId','front_user.country_id as user_country_id','front_user.*','user_educational.*','language.name as language','country.*','timezone.*','state.*','city.*','qualification.*')
            ->leftJoin('country', 'front_user.country_id', '=', 'country.id')
            ->leftJoin('state', 'front_user.state_id', '=', 'state.id')
            ->leftJoin('city', 'front_user.city_id', '=', 'city.id')
            ->leftJoin('timezone', 'front_user.timezone_id', '=', 'timezone.id')
            ->leftJoin('language', 'front_user.lang_id', '=', 'language.id')
            ->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'front_user.id')
            ->leftJoin('qualification', 'user_educational.qualification_id', '=', 'qualification.id')
            ->where('front_user.id', '=', $loginTutee)
            ->first();

            $result->country_id = $result->user_country_id ;
        return $result;
    }
}
