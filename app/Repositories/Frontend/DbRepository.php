<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;

/**
 * @package App\Repositories
 */
Abstract class DbRepository
{
   /**
    * @param  array $columns 
    * @param  string  $sort
    * @param  string  $order_by
    * @return mixed
    */
    public function selectAll($columns='*', $order_by = 'id', $sort = 'asc')
    {
        //return $this->model->select($columns)->orderBy($order_by, $sort)->skip(0)->take(5)->get();
        return $this->model->select($columns)->orderBy($order_by, $sort)->get();
    }

}
