<?php

namespace App\Repositories\Frontend\TuteePayment;

/**
 * Class EloquentTuteePaymentRepository
 *
 */

use App\Models\Session\Session;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Frontend\TuteePayment\TuteePaymentRepositoryContract;
use DB;

/**
 * Class EloquentTuteePaymentRepository
 *
 * @package App\Repositories\Frontend\Frontusers
 */
class EloquentTuteePaymentRepository extends DbRepository implements TuteePaymentRepositoryContract {

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Session $model) {
		$this->model = $model;
	}

	/**
	 * get tutor pending session
	 *
	 * @return mixed
	 */
	public function getPayments() {
		$id = \Session::get('front_user_id');
		$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
			->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
			->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
			->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
			->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.cancelled_date_time')
			->where('session.status', 1)
			->where('session.tutee_id', $id)
			->orderBy('id', 'desc')
			->paginate(10);
		// dd($session);
		return $session;
	}

	/**
	 * get tutee payments history by filter date
	 *
	 * @param startdate, enddate
	 * @return mixed
	 */
	public function getPaymentsByFilter($startDate, $endDate) {
		$id = \Session::get('front_user_id');
		// dd($endDate);
		if ($endDate == "null") {
			$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
				->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
				->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
				->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
				->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
				->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.cancelled_date_time')
				->where('session.status', 1)
				->where('session.tutee_id', $id)
				->orderBy('id', 'desc')
				->paginate(10);
		} else {
			$endDate = date('Y-m-d', strtotime($endDate . ' + ' . 1 . ' days'));
			$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
				->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
				->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
				->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
				->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
				->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.cancelled_date_time')
				->where('session.status', 1)
				->where('session.scheduled_date_time', '>=', $startDate)
				->where('session.scheduled_date_time', '<=', $endDate)
				->where('session.tutee_id', $id)
				->orderBy('id', 'desc')
				->paginate(10);
			// ->paginate(10);
		}
		// dd($session);
		return $session;
	}
	/**
	 * Get cancel session by tutee
	 *
	 */
	public function getCancelSessionByTutee() {
		$id = \Session::get('front_user_id');
		$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
			->leftJoin('front_user as tutee_id', 'tutee_id.id', '=', 'session.tutee_id')
			->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
			->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
			->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
			->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.penalty_amount', 'session.refund_amount', 'session.tutee_refund_date_time', 'session.cancelled_date_time')
			->where('session.status', 4)
			->where('session.tutee_id', $id)
			->orderBy('id', 'desc')
			->paginate(10);
		// dd($session);
		return $session;
	}

	/**
	 * get cancel Sessions By Tutee By Filter
	 *
	 * @return mixed
	 */
	public function cancelSessionsByFilter($startDate, $endDate) {
		$id = \Session::get('front_user_id');
		if ($endDate == "null") {
			$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
				->leftJoin('front_user as tutee_id', 'tutee_id.id', '=', 'session.tutee_id')
				->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
				->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
				->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
				->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
				->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.penalty_amount', 'session.refund_amount', 'session.tutee_refund_date_time', 'session.cancelled_date_time')
				->where('session.status', 4)
				->where('session.tutee_id', $id)
				->orderBy('id', 'desc')
				->paginate(10);
		} else {
			//$endDate = date('Y-m-d', strtotime($endDate . ' + ' . 1 . ' days'));
			$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
				->leftJoin('front_user as tutee_id', 'tutee_id.id', '=', 'session.tutee_id')
				->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
				->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
				->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
				->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
				->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.penalty_amount', 'session.refund_amount', 'session.tutee_refund_date_time', 'session.cancelled_date_time')
				->where('session.status', 4)
				->where('session.tutee_id', $id)
				->where('session.cancelled_date_time', '>=', $startDate)
				->where('session.cancelled_date_time', '<=', $endDate)
				->orderBy('id', 'desc')
				->paginate(10);
			// ->paginate(10);
			// dd($session);
		}
		return $session;
	}
	/**
	 * Get cancel session by tutor
	 */
	public function getCencelSessionByTutor() {
		$id = \Session::get('front_user_id');

		$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
			->leftJoin('front_user as tutee_id', 'tutee_id.id', '=', 'session.tutee_id')
			->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
			->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
			->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
			->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.penalty_amount', 'session.refund_amount', 'session.tutee_refund_date_time', 'session.cancelled_date_time')
			->where('session.status', 3)
			->where('session.tutee_id', $id)
			->orderBy('id', 'desc')
			->paginate(10);
		// dd($session);
		return $session;
	}

	/**
	 * get cancel Session By Tutor By Filter
	 *
	 * @return mixed
	 */
	public function cancelSessionByTutorByFilter($startDate, $endDate = "") {
		$id = \Session::get('front_user_id');

		if ($endDate == "null") {
			$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
				->leftJoin('front_user as tutee_id', 'tutee_id.id', '=', 'session.tutee_id')
				->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
				->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
				->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
				->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
				->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.penalty_amount', 'session.refund_amount', 'session.tutee_refund_date_time', 'session.cancelled_date_time')
				->where('session.status', 3)
				->where('session.tutee_id', $id)
				->orderBy('id', 'desc')
				->paginate(10);
		} else {
			//$endDate = date('Y-m-d', strtotime($endDate . ' + ' . 1 . ' days'));

			$session = Session::leftJoin('front_user as tutor', 'tutor.id', '=', 'session.tutor_id')
				->leftJoin('front_user as tutee_id', 'tutee_id.id', '=', 'session.tutee_id')
				->leftJoin('subject', 'subject.id', '=', 'session.subject_id')
				->leftJoin('topic', 'topic.id', '=', 'session.topic_id')
				->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'session.tutor_id')
				->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
				->select(DB::raw("CONCAT(tutor.first_name, ' ', tutor.last_name) as tutor"), 'topic.topic_name', 'subject.subject_name', 'qualification.qualification_name', 'session.created_at', 'tutor.photo', 'session.objective', 'session.topic_id', 'session.subject_id', 'session.id', 'user_educational.rating', 'user_educational.punctuality', 'tutor.id as tutor_id', 'session.scheduled_date_time', 'session.accepted_date_time', 'session.session_fee', 'session.penalty_amount', 'session.refund_amount', 'session.tutee_refund_date_time', 'session.cancelled_date_time')
				->where('session.status', 3)
				->where('session.tutee_id', $id)
				->where('session.cancelled_date_time', '>=', $startDate)
				->where('session.cancelled_date_time', '<=', $endDate)
				->orderBy('id', 'desc')
				->paginate(10);
			// ->paginate(2);
			// dd($session);
		}
		return $session;
	}
}
