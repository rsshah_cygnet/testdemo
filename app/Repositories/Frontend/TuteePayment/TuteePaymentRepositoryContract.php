<?php

namespace App\Repositories\Frontend\TuteePayment;

/**
 * Interface FrontusersRepositoryContract
 *
 * @package App\Repositories\\Frontusers
 */

interface TuteePaymentRepositoryContract {
	/* Create Item
		     *
		     * @param array $input
	*/
	// public function create($input);

	/**
	 * Update Item
	 *
	 * @param int $id
	 * @param array $input
	 */
	// public function update($id, $input);

	/**
	 * get tutee payments history
	 *
	 * @return mixed
	 */
	public function getPayments();

	/**
	 * Get cancel session by tutee
	 *
	 */
	public function getCancelSessionByTutee();

	/**
	 * Get cancel session by tutor
	 */
	public function getCencelSessionByTutor();

	/**
	 * get tutee payments history by filter date
	 *
	 * @return mixed
	 */
	public function getPaymentsByFilter($startDate, $endDate);

	/**
	 * get cancel Sessions By Tutee By Filter
	 *
	 * @return mixed
	 */
	public function cancelSessionsByFilter($startDate, $endDate);

	/**
	 * get cancel Session By Tutor By Filter
	 *
	 * @return mixed
	 */
	public function cancelSessionByTutorByFilter($startDate, $endDate);
}
