<?php

namespace App\Repositories\Frontend\Test;

use App\Models\Access\User\User;
use App\Models\Question\Question;
use App\Models\Topic\Topic;
use App\Repositories\Frontend\Test\TestContract;
use Carbon\Carbon;
use DB;
use App\Models\Frontusers\Frontusers;

/**
 * Class EloquentUserRepository
 * @package App\Repositories\Frontend\User
 */
class EloquentTestRepository implements TestContract {

	public $model;
	public $countCorrectAnswer = 0;

	/**
	 * @param TestRepositoryContract $model
	 */
	public function __construct(Question $model) {
		$this->model = $model;
	}

	/**
	 * @return mixed
	 */

	/**
	 * Get front user question's answer
	 */

	public function getQuestions($id, $limit, $grade) {
		$question_id = array();
		if ($grade == 0) {
			$topics = Topic::select('id')->where('subject_id', $id)->get()->toArray();
			$count = count($topics);
			if(empty($count)){
				return false;
			}
			foreach ($topics as $key => $val) {
				if ($key == 0) {
					$results = Question::where('topic_id', $val['id'])->take($limit)->orderBy('asked', 'asc')->inRandomOrder();
				} else {
					$query = Question::where('topic_id', $val['id'])->take($limit)->orderBy('asked', 'asc')->inRandomOrder();
					$results->union($query);
				}

			}
		} else {
			$results = Question::where('topic_id', $id)->take($limit)->orderBy('asked', 'asc')->inRandomOrder();
		}
		$que = $results->get();
		$final = $que->toArray();

		foreach ($final as $k => $v) {
			$question_id[] = $v['id'];
		}

		$update = Question::whereIn('id', $question_id)->increment('asked');
		return $que;
	}

	/**
	 * Get front user question's answer
	 */
	public function getAnswers($request) {

		$loggedInUserid = \Session::get('front_user_id');
		$tutor_details_id = $request['tutorId'];
		$totalQuestion = $request['num_of_que'];
		$minutes = $request['minutes'];
		$passing_percentage = $request['passing_percentage'];

		if (isset($request) && $request != null) {
			$countCorrectAnswer = 0;
			$totalAnswer = 0;
			foreach ($request as $key => $val) {

				$correctAnswer = DB::table('question')
					->select('correct_answer')
					->where('id', '=', $key)
					->get();

				$correct = 0;

				if (isset($correctAnswer) && $correctAnswer != null) {
					$answer = $correctAnswer[0]->correct_answer;

					if ($val != null) {
						$totalAnswer++;
					}

					if ($answer == $val) {
						$countCorrectAnswer++;
						$correct = 1;
					}
					DB::table('tutor_answer')
						->insert([
							'tutor_detail_id' => $request['tutorId'],
							'question_id' => $key,
							'answer' => $val,
							'is_correct' => $correct,
							'created_at' => carbon::now()->toDateTimeString(),
							'modified_by' => $loggedInUserid,
						]);
				}
			}

			$Percentage = DB::table('settings')->select('passing_percentage')->first();

			$passingPercentage = $Percentage->passing_percentage;
			if (isset($passing_percentage) && $passing_percentage != "" && $passing_percentage != 0) {
				$passingPercentage = $passing_percentage;
			}
			// dd($totalQuestion);
			if(!empty($totalQuestion) && $totalQuestion != 0){
				$per = (100 * $countCorrectAnswer) / $totalQuestion;
			}
			else{
				$per = 0;
			}

			$no_of_mark_per_question_data = DB::table('settings')->select('no_of_mark_per_question')->first();
			$no_of_mark_per_question = $no_of_mark_per_question_data->no_of_mark_per_question;
			$numberofmarks = $no_of_mark_per_question * $countCorrectAnswer;
			if ($per >= $passingPercentage) {
				$passed_status = 1;

				$user = DB::table('front_user')->select('email', DB::raw("CONCAT(first_name,' ', last_name) as name"))->where('id', $loggedInUserid)->first();
				$userName = $user->name;
				$userEmailId = $user->email;
				// dd($userEmailId);
				// Set curriculum starts
				
				$user = Frontusers::find(\Session::get('front_user_id'));
		        if (!empty($user)) {
		            $userData = \DB::table('tutor_details')
		                ->select('id', 'topic_id', 'subject_id')
		                ->where('id', $request['tutorId'])
		                ->get();
		                
		            $userEmailId = $user->email;

		            $link = array();
		            foreach ($userData as $id) {
		                if (isset($id->topic_id) && $id->topic_id != 0 && $id->topic_id != "") {
		                    $curriculum = getCurriculumHierarchy('', $id->topic_id, true);
		                } else if (isset($id->subject_id) && $id->subject_id != 0 && $id->subject_id != "") {
		                    $curriculum = getCurriculumHierarchy('', $id->subject_id, true);
		                }
		                $ExamlId = \Crypt::encrypt($id->id);

		                $link[] = $curriculum;
		            }

		            $makeExamLink = str_replace(array(','), '', $link);
		            $Examlink     = implode(" ", $makeExamLink);
		            $appName      = config('app.name');
		        }
				// Set curriculum ends
				\Mail::raw('Congrats ' . $userName . '. You have seccessfully passed Tech Learn test. Curriculum: ' . $Examlink . '.', function ($message) use ($Examlink, $userEmailId) {
					$message->to($userEmailId)
						->subject('Tech Learn - Exam Result');
				});
			} else {
				$passed_status = 0;

				$user = DB::table('front_user')->select('email', DB::raw("CONCAT(first_name,' ', last_name) as name"))->where('id', $loggedInUserid)->first();
				$userName = $user->name;
				// Set curriculum starts
				$user = Frontusers::find(\Session::get('front_user_id'));
		        if (!empty($user)) {
		            $userData = \DB::table('tutor_details')
		                ->select('id', 'topic_id', 'subject_id')
		                ->where('id', $request['tutorId'])
		                ->get();
		                
		            $userEmailId = $user->email;
		            $link = array();

		            foreach ($userData as $id) {
		                if (isset($id->topic_id) && $id->topic_id != 0 && $id->topic_id != "") {
		                    $curriculum = getCurriculumHierarchy('', $id->topic_id, true);
		                } else if (isset($id->subject_id) && $id->subject_id != 0 && $id->subject_id != "") {
		                    $curriculum = getCurriculumHierarchy($id->subject_id, '', true);
		                }
		                $ExamlId = \Crypt::encrypt($id->id);

		                $link[] = $curriculum;
		            }

		            $makeExamLink = str_replace(array(','), '', $link);
		            $Examlink     = implode(" ", $makeExamLink);
		            $appName      = config('app.name');

		        }
				// Set curriculum ends
				$userEmailId = $user->email;
				\Mail::raw('Dear ' . $userName . ', you have not clear Tech Learn test. Curriculum: ' . $Examlink  . '.', function ($message) use ($Examlink, $userEmailId) {
					$message->to($userEmailId)
						->subject('Tech Learn - Exam Result');
				});
			}

			$data = DB::table('tutor_details')
				->where('front_user_id', $loggedInUserid)
				->where('id', $tutor_details_id)
				->update([
					'passed_status' => $passed_status,
					'no_of_total_questions' => $totalQuestion,
					'no_of_total_answers' => $totalAnswer,
					'no_of_correct_answers' => $countCorrectAnswer,
					'no_of_minutes' => $minutes,
					'no_of_marks' => $numberofmarks,
					'no_of_percentage' => $per,
					'updated_at' => carbon::now()->toDateTimeString(),
					'is_completed' => 1,
				]);

			return $passed_status;
		}
	}

	/**
	 * Get Pending Test
	 */
	public function getPendingTest() {
		$id = \Session::get('front_user_id');
		$test = DB::table('front_user')
			->leftJoin('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
			->leftJoin('subject', 'subject.id', '=', 'tutor_details.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'tutor_details.topic_id')
			->select('tutor_details.id', 'topic.topic_name', 'subject.subject_name', 'tutor_details.topic_id', 'tutor_details.subject_id')
			->where('tutor_details.front_user_id', $id)
			->where('tutor_details.appeared', 0)
			->where('tutor_details.is_completed', 0)
			->paginate(10);

		return $test;
	}

	/**
	 * Get Test Details
	 */
	public function getTestDetails($id) {
		// dd($id);
		$loggedInUserid = \Session::get('front_user_id');
		$testDetails = DB::table('tutor_details')
			->leftJoin('curriculum', 'curriculum.id', '=', 'tutor_details.curriculum_id')
			->leftJoin('eductional_systems', 'eductional_systems.id', '=', 'tutor_details.education_system_id')
			->leftJoin('program', 'program.id', '=', 'tutor_details.program_id')
			->leftJoin('subject', 'subject.id', '=', 'tutor_details.subject_id')
			->leftJoin('grade', 'grade.id', '=', 'tutor_details.grade_id')
			->select('curriculum.curriculum_name', 'eductional_systems.name as educationSystem_name', 'subject.subject_name', 'program.program_name', 'grade.grade_name', 'grade.grade_type')
			->where('tutor_details.front_user_id', $loggedInUserid)
			->where('tutor_details.id', $id)
			->first();

		return $testDetails;
	}

	/**
	 * Get Apperared Test
	 */
	public function getAppearedTest() {
		$id = \Session::get('front_user_id');

		$test = DB::table('front_user')
			->leftJoin('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
			->leftJoin('subject', 'subject.id', '=', 'tutor_details.subject_id')
			->leftJoin('topic', 'topic.id', '=', 'tutor_details.topic_id')
			->select('tutor_details.id', 'topic.topic_name', 'subject.subject_name', 'tutor_details.topic_id', 'tutor_details.subject_id', 'tutor_details.is_completed', 'tutor_details.passed_status', 'tutor_details.no_of_percentage', 'tutor_details.test_date_time')
			->where('tutor_details.front_user_id', $id)
			->where('tutor_details.appeared', 1)
			->orderBy('tutor_details.test_date_time', 'desc')
			->paginate(10);

		return $test;
	}

	/**
	 * Get Apeeared test by filtering date
	 */
	public function getAppearedTestByFilter($startDate, $endDate) {
		// dd($startDate);
		
		$id = \Session::get('front_user_id');
		if ($endDate == "null") {
			$test = DB::table('front_user')
				->leftJoin('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
				->leftJoin('subject', 'subject.id', '=', 'tutor_details.subject_id')
				->leftJoin('topic', 'topic.id', '=', 'tutor_details.topic_id')
				->select('tutor_details.id', 'topic.topic_name', 'subject.subject_name', 'tutor_details.topic_id', 'tutor_details.subject_id', 'tutor_details.is_completed', 'tutor_details.passed_status', 'tutor_details.no_of_percentage', 'tutor_details.test_date_time')
				->where('tutor_details.front_user_id', $id)
				->where('tutor_details.appeared', 1)
				->paginate(10);
		} else {
			$endDate = date('Y-m-d', strtotime($endDate . ' + ' . 1 . ' days'));
			
			$test = DB::table('front_user')
				->leftJoin('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
				->leftJoin('subject', 'subject.id', '=', 'tutor_details.subject_id')
				->leftJoin('topic', 'topic.id', '=', 'tutor_details.topic_id')
				->select('tutor_details.id', 'topic.topic_name', 'subject.subject_name', 'tutor_details.topic_id', 'tutor_details.subject_id', 'tutor_details.is_completed', 'tutor_details.passed_status', 'tutor_details.no_of_percentage', 'tutor_details.test_date_time')
				->where('tutor_details.front_user_id', $id)
				->where('tutor_details.appeared', 1)
				->where('tutor_details.test_date_time', '>=', $startDate)
				->where('tutor_details.test_date_time', '<=', $endDate)
				->orderBy('id', 'desc')
				->paginate(10);
		}

		return $test;
	}

	/**
	 *    Apeear for test
	 */
	public function tutorTestDetails($request) {
		$id = \Session::get('front_user_id');
		if (empty($request['education_id'])) {
			$request['education_id'] = null;
		}
		if (empty($request['level_id'])) {
			$request['level_id'] = null;
		}
		if (empty($request['program_id'])) {
			$request['program_id'] = null;
		}
		if (empty($request['topic_id'])) {
			$request['topic_id'] = null;
		}

		$tutorTestDetails = DB::table('tutor_details')->insertGetId([
			'front_user_id' => $id,
			'curriculum_id' => $request['curriculum_id'],
			'education_system_id' => $request['education_id'],
			'level_id' => $request['level_id'],
			'program_id' => $request['program_id'],
			'grade_id' => $request['grade_id'],
			'subject_id' => $request['subject_id'],
			'topic_id' => $request['topic_id'],
			'test_date_time' => carbon::now()->toDateTimeString(),
			'created_at' => carbon::now()->toDateTimeString(),
		]);

		// dd($request['becomeTutor']);
		// if (!empty($request['becomeTutor']) && $request['becomeTutor'] == 1) {
		// 	// $updateRoll = DB::table('front_user')
		// 	// 	->where('id', \Session::get('front_user_id'))
		// 	// 	->update([
		// 	// 		'role' => 3,
		// 	// 	]);
		// 	// \Session::set('becomeTutor', 1);
		// }
		return $tutorTestDetails;
	}

	/**
	 *    Tutee aplly for test to become a tutor
	 */
	public function becomeTutorTest($request) {
		$id = \Session::get('front_user_id');
		if (empty($request['education_id'])) {
			$request['education_id'] = null;
		}
		if (empty($request['level_id'])) {
			$request['level_id'] = null;
		}
		if (empty($request['program_id'])) {
			$request['program_id'] = null;
		}
		if (empty($request['topic_id'])) {
			$request['topic_id'] = null;
		}

		$tuteeTestDetails = DB::table('tutor_details')->insertGetId([
			'front_user_id' => $id,
			'curriculum_id' => $request['curriculum_id'],
			'education_system_id' => $request['education_id'],
			'level_id' => $request['level_id'],
			'program_id' => $request['program_id'],
			'grade_id' => $request['grade_id'],
			'subject_id' => $request['subject_id'],
			'topic_id' => $request['topic_id'],
			'test_date_time' => carbon::now()->toDateTimeString(),
			'created_at' => carbon::now()->toDateTimeString(),
		]);

		// dd($request['becomeTutor']);
		// if (!empty($request['becomeTutor']) && $request['becomeTutor'] == 1) {
		// 	// $updateRoll = DB::table('front_user')
		// 	// 	->where('id', \Session::get('front_user_id'))
		// 	// 	->update([
		// 	// 		'role' => 3,
		// 	// 	]);
		// 	// \Session::set('becomeTutor', 1);
		// }
		return $tuteeTestDetails;
	}
}
