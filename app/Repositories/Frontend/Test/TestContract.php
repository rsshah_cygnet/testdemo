<?php

namespace App\Repositories\Frontend\Test;

/**
 * Interface UserContract
 * @package App\Repositories\Frontend\User
 */
interface TestContract {

	/**
	 * @return mixed
	 */
	public function getQuestions($id,$limit,$grade);

	/**
	 * Get front user question's answer
	 */
	public function getAnswers($request);

	/**
	 * Get Pending Test
	 */
	public function getPendingTest();

	/**
	 * Get Test Details
	 */
	public function getTestDetails($id);

	/**
	 * Get Apperared Test
	 */
	public function getAppearedTest();

	/**
	 * Get Apeeared test by filtering date
	 */
	public function getAppearedTestByFilter($startDate, $endDate);

	/**
	 *	Apeear for test
	 */
	public function tutorTestDetails($request);

	/**
	 *	Tutee aplly for test to become a tutor
	 */
	public function becomeTutorTest($request);	
}
