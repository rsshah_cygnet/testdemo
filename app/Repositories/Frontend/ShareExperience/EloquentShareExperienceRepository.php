<?php

namespace App\Repositories\Frontend\ShareExperience;

/**
 * Class EloquentFrontusersRepository
 *
 * @author Sudhir virpara
 */

use App\Exceptions\GeneralException;
use App\Models\Testimonial\Testimonial;
use App\Repositories\Backend\DbRepository;
use App\Repositories\Frontend\ShareExperience\ShareExperienceRepositoryContract;
use Carbon\Carbon;

/**
 * Class EloquentFrontusersRepository
 *
 * @package App\Repositories\Backend\Frontusers
 */
class EloquentShareExperienceRepository extends DbRepository implements ShareExperienceRepositoryContract {

	/**
	 * Related model of this repositery
	 *
	 * @var object
	 */
	public $model;

	public function __construct(Testimonial $model) {
		$this->model = $model;
	}

	public function create($input) {

		if (!empty($input)) {
			$status = '1';
			$id = \Session::get('front_user_id');
			return $this->model->insert([
				'title' => $input['experience'],
				'status' => 0,
				'created_by' => $id,
				'created_at' => carbon::now()->toDateTimeString(),
			]);
		}
		throw new GeneralException(trans('exceptions.backend.article.create_error'));
	}
}
