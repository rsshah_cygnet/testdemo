<?php

namespace App\Repositories\Frontend\ShareExperience;

/**
 * Interface FrontusersRepositoryContract
 *
 * @author Sudhir Virpara
 * @package App\Repositories\\Frontusers
 */

interface ShareExperienceRepositoryContract {
	/* Create Item
		     *
		     * @param array $input
	*/
	// public function create($input);

	/**
	 * Update Item
	 *
	 * @param int $id
	 * @param array $input
	 */
	// public function update($id, $input);

	/**
	 * Destroy Item
	 *
	 * @param  $id
	 * @return mixed
	 */
	public function destroy($id);

	public function findOrThrowException($id);

	public function create($input);

}
