<?php

namespace App\Http\Controllers\Api;

use App\Http\Acme\Transformers\CmspagesTransformer;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Cmspages\CmspagesRepositoryContract;
use Illuminate\Http\Request;

class CmspagesController extends Controller {

	public function __construct(CmspagesRepositoryContract $cmspages, CmspagesTransformer $cmsPagesTransformer) {
		$this->cmspages = $cmspages;
		$this->cmsPagesTransformer = $cmsPagesTransformer;
	}

	/**
	 * get cms content
	 * @param Request $request
	 * @return type
	 */
	public function getCmsContent(Request $request) {
		$postData = $request->all();

		if (isset($postData['PageSlug'])) {
			$record = $this->cmspages->cmsDataByPageSlug($postData);
			if (count($record) > 0) {
				$cms = $record->toArray();
				$responseData = $this->cmsPagesTransformer->transform($cms[0]);
				return $this->ApiSuccessResponse($responseData);
			} else {
				return $this->respondInternalError('CMS record not found');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function showCMSPage($page_slug, CmspagesRepositoryContract $RepositoryContract) {
		$cmspages = $RepositoryContract->findBySlug($page_slug);

		return view('frontend.cmspages.index')
			->withCmspages($cmspages);
	}

}
