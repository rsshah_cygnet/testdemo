<?php

namespace App\Http\Controllers\Api;

use App\Http\Acme\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Client\ClientRepositoryContract;
use App\Repositories\Backend\User\UserContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {

	public function __construct(UserContract $users, UserTransformer $userTransformer, ClientRepositoryContract $client) {
		$this->users = $users;
		$this->client = $client;
		$this->userTransformer = $userTransformer;
	}

	/**
	 * register user in database
	 * @param Request $request
	 * @return type
	 */
	public function register(Request $request) {
		$postData = $request->all();
		if (isset($postData['Name']) &&
			isset($postData['Email']) &&
			isset($postData['CompanyName']) &&
			isset($postData['Password'])
		) {
			//check email is already used or not
			if (!$this->users->checkUserAlreadyExist($postData['Email'])) {
				return $this->respondInternalError('Email Already Registered');
			}
			$userData = $this->client->createAppUser($postData);
			if ($userData) {
				$this->setStatusCode(200);
				$responseData['userId'] = $userData->id;
				return $this->ApiSuccessResponse($responseData);
			} else {
				return $this->respondInternalError('Cannot register client, please try again later');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

	/**
	 * for login users
	 * @param Request $request
	 * @return type
	 */
	public function login(Request $request) {
		$postData = $request->all();
		if (isset($postData['Email']) &&
			isset($postData['Password']) &&
			$request->header('DeviceToken') &&
			$request->header('DeviceType')
		) {
			//check email is already used or not
			$user = $this->users->fetchUserByField('email', $postData['Email'])->first();
			if ($user) {
				// check password
				if (Hash::check($postData['Password'], $user->password)) {
					$user = $user->toArray();
					// check user is confirmed or not
					if ($user['confirmed'] == 0) {
						return $this->respondInternalError('User not confirmed yet');
					} else {
						$this->setStatusCode(200);
						$newUserToken = $this->users->updateUserToken($user['id'], $request->header('DeviceType'), $request->header('DeviceToken'));
						$user['user_token'] = $newUserToken;
						$responseData = $this->userTransformer->transform($user);

						return $this->ApiSuccessResponse($responseData);
					}
				} else {
					return $this->respondInternalError('Invalid Login Credentials');
				}
			} else {
				return $this->respondInternalError('No Such User found');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

	/**
	 * logout user
	 * @param Request $request
	 * @return type
	 */
	public function logout(Request $request) {
		$userId = $request->header('UserId');
		$userToken = $request->header('UserToken');
		if ($request->header('UserId') && $request->header('UserToken')) {
			$response = $this->users->deleteUserToken($userId, $userToken);
			if ($response) {
				return $this->ApiSuccessResponse();
			} else {
				return $this->respondInternalError('Error in Logout');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

	/**
	 * validate passkey
	 * @param Request $request
	 * @return type
	 */
	public function validatePasskey(Request $request) {
		$postData = $request->all();
		$userId = $request->header('UserId');
		$userToken = $request->header('UserToken');

		if ($postData['Passkey'] && $userToken && $userId) {

			$user = $this->users->validatePasskeyByUserid($postData, $userId);
			if ($user) {
				return $this->ApiSuccessResponse();
			} else {
				return $this->respondInternalError('Invalid User or Passkey');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

	/**
	 * forgot password
	 * @param Request $request
	 * @return type
	 */
	public function forgotPassword(Request $request) {
		$postData = $request->all();
		if (isset($postData['Email'])) {

			$user = $this->users->fetchUserByField('email', $postData['Email'])->first();
//           check email is already used or not
			if ($user) {
				return $this->ApiSuccessResponse();
			} else {
				return $this->respondInternalError('No Such User found');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

	/**
	 * cahnge password
	 * @param Request $request
	 * @return type
	 */
	public function changePassword(Request $request) {

		$postData = $request->all();
		$userId = $request->header('UserId');

		if (isset($postData['OldPassword']) &&
			isset($postData['NewPassword']) &&
			$request->header('UserToken') &&
			$userId) {
			$user = $this->users->fetchUserByField('id', $userId)->first();

			if ($user) {

				if (Hash::check($postData['OldPassword'], $user->password)) {

					$changePassword = $this->users->updateAppPassword($userId, $postData);

					if ($changePassword) {
						return $this->ApiSuccessResponse();
					} else {
						return $this->respondInternalError('Error in change Password');
					}
				} else {
					return $this->respondInternalError('That is not your old password.');
				}
			} else {
				return $this->respondInternalError('User Not Found');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

	/**
	 * save passkey reminder in database
	 * @param Request $request
	 * @return type
	 */
	public function savePasskeyReminder(Request $request) {
		$userId = $request->header('UserId');
		$userToken = $request->header('UserToken');
		$postData = $request->all();

		if (isset($postData['PasskeyReminderMinutes']) && $postData['PasskeyReminderMinutes'] && $userToken && $userId) {
			$user = $this->users->fetchUserByField('id', $userId)->first();

			if ($user) {
				$passkeyReminder = $this->users->savePasskeyReminder($userId, $postData);

				if ($passkeyReminder) {
					return $this->ApiSuccessResponse();
				} else {
					return $this->respondInternalError('Error in Store PassKey Reminder');
				}
			} else {
				return $this->respondInternalError('User Not Found');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

	/**
	 * request passkey
	 * @param Request $request
	 * @return type
	 */
	public function requestPasskey(Request $request) {
		$userId = $request->header('UserId');
		$userToken = $request->header('UserToken');

		if ($userId && $userToken) {
			$user = $this->users->fetchUserByField('id', $userId)->first();

			if ($user) {
				$return['Passkey'] = $this->users->savePasskey($userId);
				return $this->ApiSuccessResponse($return);
			} else {
				return $this->respondInternalError('User Not Found');
			}
		} else {
			return $this->respondInternalError('Invalid Arguments');
		}
	}

}
