<?php namespace App\Http\Controllers;

/**
 * Class Controller
 *
 * @author Amit Pandey avpandey@cygnet-infotech.com
 * @package FTX\Http\Controllers
 */

use Illuminate\Support\Facades\Input;
use URL, Route, Session, Redirect, Config;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Default Pagination
     *
     * @var int
     */
    public $defaultPagination = 20;

    /**
     * Construct
     *
     */
 	public function __construct()
	{
        //$this->hasher = hasher();
	}

	/**
	 * Process Latest IDs
	 *
	 * @param $id
	 * @param $obj
	 * @return bool
	 */
    public function processLatestUpdate($id, $obj)
    {
        if(isset($obj->id))
        {
            return $obj->id;
        }

        if(isset($id) && !empty($id))
        {
            return $id;
        }

	    return false;
    }

    /**
     * Action on Successful Form Save
     *
     * @param $listRoute
     * @param $editRoute
     * @param $message
     * @param array $params
     * @param bool|false $hash
     * @return $this
     */
    public function saveSuccessAction($listRoute, $editRoute, $message, $params = array(), $hash = false, $createRoute = false)
    {
        // If ID is set, and hash is true, encode the ID
        if(isset($params['id']) && $hash && strlen($params['id']) !== 20)
        {
            //$params['id'] = crypt($params['id'], '');
            $params['id'] = $params['id'];
        }

        // If routes aren't found, redirect back to previous page
        if(!Route::has($listRoute) || !Route::has($editRoute))
        {
            return back()->withErrors('Cannot access this page');
        }

        // If input save-close is set, this sends us back to the list page
        if(Input::get('save-close'))
        {
	        return redirect()->route($listRoute)->withFlashSuccess($message);
        }

        // If input save-new is set, this sends us back to the create page
        if(Input::get('save-new'))
        {
            if($createRoute)
            {
                return redirect()->route($createRoute)->withFlashSuccess($message);
            }
            else
            {
                return redirect()->route($listRoute)->withFlashSuccess($message);
            }
            
        }

        // In normal save, send back to edit page
	    return redirect()->route($editRoute, $params)->withFlashSuccess($message);
    }

    /**
     * Toggle status of model item
     *
     * @param  int  $id
     *
     * @return void
     */
    public function toggleStatus($id)
    {
        $this->model->updateStatus($id);
        return Redirect::back();
    }

    /**
     * Mass modify model items by hashed IDs
     *
     * @return void
     */
    public function modifyAction()
    {
        $items  = Input::get('_items');
        $action = Input::get('_action');

        foreach($items as $item)
        {
            switch ($action)
            {
                case 'enable':
                    $this->model->updateStatus($item['value'], 1);
                    break;

                case 'disable':
                    $this->model->updateStatus($item['value'], 0);
                    break;

                case 'delete':
                    break;
            }
        }
    }

    /**
     * Mass modify model items by hashed IDs
     *
     * @return void
     */
    public function updateFlag()
    {
        $id     = Input::get('id');
        $value  = Input::get('flagValue');

        $this->model->updateFlag($id, $value);
    }

    /**
     * Get pagination from session or form input
     *
     * @return int
     */
    public function getPaginationLimit()
    {
        if($this->getSessionPaginateValue())
        {
            if(Input::get('perPage'))
            {
                $this->setSessionPaginateValue();
                return Input::get('perPage');
            }
            else {
                return $this->getSessionPaginateValue();
            }
        }
        if(Input::get('perPage'))
        {
            $this->setSessionPaginateValue();
            return Input::get('perPage');
        }
        else
        {
            return $this->defaultPagination;
        }
    }

    /**
     * Set session pagination value based on route
     *
     * @return boolean
     */
    private function setSessionPaginateValue()
    {
        $path = Route::getCurrentRoute()->getPath();

        if(Input::get('perPage'))
        {
            Session::put('paginate_'.$path, Input::get('perPage'));
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Get session pagination value based on route
     *
     * @return boolean
     */
    private function getSessionPaginateValue() {
        /*$path = Route::getCurrentRoute()->getPath();
        if($pagination = Session::get('paginate_'.$path))
        {
            return $pagination;
        }
        else {
            return false;
        }*/
        return false;
    }

    /**
     * Merge Account ID
     *
     * @param $request
     * @return mixed
     */
    public function mergeAccountId($request)
    {
        $account_id = access()->account()->id;
        $request->merge(['account_id' => $account_id]);
        return $request;
    }

}
