<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Settings\EditSettingsRequest;
use App\Http\Requests\Backend\Settings\UpdateSettingsRequest;
use App\Repositories\Backend\Settings\SettingsRepositoryContract;
use Illuminate\Http\Request;

/**
 * Class SettingsController
 * @package App\Http\Controllers
 */
class SettingsController extends Controller {

	/**
	 * @var SettingsRepositoryContract
	 */
	protected $repositorycontract;

	/**
	 * @param settingsRepositoryContract       $settings
	 */
	public function __construct(SettingsRepositoryContract $RepositoryContract) {
		$this->repositorycontract = $RepositoryContract;
	}

	/**
	 * show form view
	 * @param EditSettingsRequest $request
	 * @return type
	 */
	public function index(EditSettingsRequest $request) {
		$result = $this->repositorycontract->findOrThrowException(1);
		return view('backend.settings.edit')
			->withSettings($result);
	}

	/**
	 * update data in database
	 * @param type $id
	 * @param UpdateSettingsRequest $request
	 * @return type
	 */
	public function update($id, UpdateSettingsRequest $request) {
		$this->repositorycontract->update($id, $request);
		return redirect()->route('admin.settings.index')->withFlashSuccess(trans('alerts.backend.settings.updated'));
	}

}
