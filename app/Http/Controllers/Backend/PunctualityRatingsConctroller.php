<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PunctualityRatings\CreateRequest;
use App\Http\Requests\Backend\PunctualityRatings\DeleteRequest;
use App\Http\Requests\Backend\PunctualityRatings\EditRequest;
use App\Http\Requests\Backend\PunctualityRatings\MarkUserRequest;
use App\Http\Requests\Backend\PunctualityRatings\StoreRequest;
use App\Http\Requests\Backend\PunctualityRatings\UpdateRequest;
use App\Http\Requests\Backend\PunctualityRatings\ViewRequest;
use App\Library\GridMaster\GridMaster;
use App\Models\PunctualityRatings\PunctualityRatings;
use App\Repositories\Backend\PunctualityRatings\PunctualityRatingsRepositoryContract;

class PunctualityRatingsConctroller extends Controller {

	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;

	/**
	 * __construct
	 *
	 * @param PunctualityRatingsRepositoryContract $repository
	 */
	function __construct(PunctualityRatingsRepositoryContract $repository) {
		$this->repository = $repository;
	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function index(ViewRequest $request) {
		return view('backend.PunctualityRatings.index')->with(['repository' => $this->repository]);
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(ViewRequest $request) {
		$gridMaster = new GridMaster;

		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->getGridData($request, $this->repository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->downloadGridData($request, $this->repository, 'download');
		}
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkUserRequest $request) {
		$this->repository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * Delete Punctuality rating Model
	 *
	 * @param int $id
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRequest $request) {
		$item = $this->repository->findOrThrowException($id);
		if ($item->id) {
			$this->repository->destroy($id);
			return response()->json(['status' => "OK"]);
		}
	}
	/**
	 * show Punctuality rating form view
	 * @param CreateRequest $request
	 * @return type
	 */
	public function create(CreateRequest $request) {
		return view('backend.PunctualityRatings.create')->with('formtype', 'create');
	}

	/**
	 * store data in database
	 * @param StoreCmsRequest $request
	 * @return type
	 */
	public function store(StoreRequest $request) {
		$this->repository->create($request);
		// return response()->json((object) [
		// 	'status' => true,
		// 	'message' => 'Curriculum added Successfully',
		// ], 200);
		return redirect()->route('admin.punctuality-ratings.index')->withFlashSuccess('Punctuality Ratings has been created sucessfully !');
	}
	/**
	 * show edit form view with data
	 * @param type $id
	 * @param EditCurriculumRequest $request
	 * @return type
	 */
	public function edit($id, EditRequest $request) {
		$punctualityrating = $this->repository->findOrThrowException($id);
		return view('backend.PunctualityRatings.edit', compact('punctualityrating', $punctualityrating))->with('formtype', 'create');
		// return view('backend.cmspages.edit' , compact('cmsPage'));
	}

	/**
	 * update data into database
	 * @param type $id
	 * @param UpdateCmsRequest $request
	 * @return type
	 */
	public function update($id, UpdateRequest $request) {
		$punctualityrating = $this->repository->findOrThrowException($id);
		$this->repository->update($id, $request);
		return redirect()->route('admin.punctuality-ratings.index')->withFlashSuccess('Punctuality Ratings has been updated sucessfully !');
	}
}
