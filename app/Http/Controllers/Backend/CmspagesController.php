<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Cms\CreateCmsRequest;
use App\Http\Requests\Backend\Cms\DeleteCmsRequest;
use App\Http\Requests\Backend\Cms\EditCmsRequest;
use App\Http\Requests\Backend\Cms\MarkCmsRequest;
use App\Http\Requests\Backend\Cms\StoreCmsRequest;
use App\Http\Requests\Backend\Cms\UpdateCmsRequest;
use App\Http\Utilities\Jqgrid;
use App\Models\Cmspage\Cmspage;
use App\Repositories\Backend\Cmspages\CmspagesRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class CmspagesController
 * @package App\Http\Controllers
 */
class CmspagesController extends Controller {

	/**
	 * @var columnsexport
	 */
	public $columnsexport = ['id', 'title', 'created_at', 'updated_at'];

	/**
	 * @var downloadfilename
	 */
	public $downloadfilename = 'CMS Pages';

	/**
	 * @var CmspagesRepositoryContract
	 */
	protected $repositorycontract;

	/**
	 * @param CmspagesRepositoryContract       $cmspages
	 */
	function __construct(CmspagesRepositoryContract $RepositoryContract, Jqgrid $jqgrid) {
		$this->repositorycontract = $RepositoryContract;
		$this->jqgrid = $jqgrid;
	}

	/**
	 * redirect to index view
	 * @return type
	 */
	public function index() {
		return view('backend.cmspages.index');
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(Request $request) {
		if ($request->ajax()) {

			$data = $this->jqgrid->Preparedata($request, $this->repositorycontract, 'ajax');

			$responce = $data[0];
			$records = $data[1];

			$i = 0;
			//make array for json
			foreach ($records as $record) {
				$responce->rows[$i]['id'] = $record->id;
				$responce->rows[$i]['cell'] = array($record->title, $record->created_at->diffForHumans(), $record->updated_at->diffForHumans(), $record->action_buttons);
				$i++;
			}
			echo json_encode($responce);
		} else {
			// EXPORT GRID START
			$dtype = $request->get('dtype');
			$records = $this->jqgrid->Preparedata($request, $this->repositorycontract, 'download');

			$data = array();
			$data[0] = array(trans('labels.backend.cmspages.table.title'),
				trans('labels.backend.cmspages.table.created'),
				trans('labels.backend.cmspages.table.last_updated'));
			//make array for json
			$i = 1;
			foreach ($records as $record) {

				$data[$i] = array($record->title, $record->created_at->diffForHumans(), $record->updated_at->diffForHumans());
				$i++;
			}

			// download file
			$this->jqgrid->Download($this->downloadfilename, $data, $dtype);
		}
	}

	/**
	 * show cms form view
	 * @param CreateCmsRequest $request
	 * @return type
	 */
	public function create(CreateCmsRequest $request) {
		//pass domain id and domain name in dropdown
		//$domain = Domain::where('status','Active')->pluck('domain', 'id')->toArray();
		return view('backend.cmspages.create');
	}

	/**
	 * store data in database
	 * @param StoreCmsRequest $request
	 * @return type
	 */
	public function store(StoreCmsRequest $request) {

		$this->repositorycontract->create($request);
		return redirect()->route('admin.cmspages.index')->withFlashSuccess('CMS Page has been created sucessfully !');
	}

	/**
	 * show edit form view with data
	 * @param type $id
	 * @param EditCmsRequest $request
	 * @return type
	 */
	public function edit($id, EditCmsRequest $request) {
		$cmspages = $this->repositorycontract->findOrThrowException($id);
		return view('backend.cmspages.edit', compact('cmspages'));
		// return view('backend.cmspages.edit' , compact('cmsPage'));
	}

	/**
	 * update data into database
	 * @param type $id
	 * @param UpdateCmsRequest $request
	 * @return type
	 */
	public function update($id, UpdateCmsRequest $request) {//dd($request->all());
		$this->repositorycontract->update($id, $request);
		return redirect()->route('admin.cmspages.index')->withFlashSuccess('CMS Page has been updated sucessfully !');
	}

	/**
	 * change in cmspage active or inactive
	 * @param type $id
	 * @param type $status
	 * @param MarkCmsRequest $request
	 * @return type
	 */
	public function mark($id, $status, MarkCmsRequest $request) {
		$this->repositorycontract->mark($id, $status);
		return response()->json(['status' => 'OK']);
	}

	/**
	 * delete cmspage data from grid
	 * @param type $id
	 * @param DeleteCmsRequest $request
	 * @return type
	 */
	public function destroy($id, DeleteCmsRequest $request) {
		$this->repositorycontract->destroy($id);
		return response()->json(['status' => 'OK']);
	}

	/**
	 *
	 * @param DeleteCmsRequest $request
	 * @return type
	 */
	public function deleteAll(DeleteCmsRequest $request) {
		$ids = $request->get('params');
		foreach ($ids as $id) {
			$this->repositorycontract->destroy($id);
		}
		return response()->json(['status' => 'OK']);
	}

	public function exportExcel() {
		$result = $this->repositorycontract->selectAll($this->columnsexport);
		Excel::create($this->downloadfilename, function ($excel) use ($result) {
			$excel->sheet('Sheet 1', function ($sheet) use ($result) {
				$sheet->fromArray($result);
			});
		})->download('xls');
	}

	public function exportCSV() {
		$result = $this->repositorycontract->selectAll($this->columnsexport);
		Excel::create($this->downloadfilename, function ($excel) use ($result) {
			$excel->sheet('Sheet 1', function ($sheet) use ($result) {
				$sheet->fromArray($result);
			});
		})->export('csv');
	}
}
