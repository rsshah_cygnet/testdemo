<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Emailtemplate\EmailTemplatePlaceholder;
use App\Models\Emailtemplate\EmailTemplateType;
use App\Http\Requests\Backend\EmailTemplate\EditEmailTemplateRequest;
use App\Http\Requests\Backend\EmailTemplate\StoreEmailTemplateRequest;
use App\Http\Requests\Backend\EmailTemplate\CreateEmailTemplateRequest;
use App\Http\Requests\Backend\EmailTemplate\DeleteEmailTemplateRequest;
use App\Http\Requests\Backend\EmailTemplate\UpdateEmailTemplateRequest;
use App\Repositories\Backend\EmailTemplate\EmailTemplateRepositoryContract;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Utilities\Jqgrid;

class EmailTemplateController extends Controller {

    /**
     * @var columnsexport
     */
    public $columnsexport = ['id', 'title', 'subject', 'body', 'created_at', 'updated_at'];

    /**
     * @var downloadfilename
     */
    public $downloadfilename = 'emailtemplates_result';

    /**
     * @var EmailTemplateContract
     */
    protected $emailTemplate;

    /**
     * @param EmailTemplateContract       $cmspages
     */
    function __construct(EmailTemplateRepositoryContract $emailTemplate, Jqgrid $jqgrid) {
        $this->emailTemplate = $emailTemplate;
        $this->jqgrid = $jqgrid;
    }

    /**
     * @return mixed
     */
    public function index() {
        return view('backend.emailtemplate.index');
    }

    /**
     * [data description]
     * @return [type] [description]
     */
    public function data(Request $request) {
        // $results = $this->users->getAllUsers();

        if ($request->ajax()) {

            $data = $this->jqgrid->Preparedata($request, $this->emailTemplate, 'ajax');

            $responce = $data[0];
            $records = $data[1];

            $i = 0;

            //make array for json
            foreach ($records as $record) {
                $responce->rows[$i]['id'] = $record->id;
                $responce->rows[$i]['cell'] = array($record->title, $record->subject, $record->updated_at->diffForHumans(),$record->action_buttons);
                $i++;
            }
            echo json_encode($responce);
        } else { // EXPORT GRID START 
            $dtype = $request->get('dtype');
            $records = $this->jqgrid->Preparedata($request, $this->emailTemplate, 'download');


            $data = array();
            $data[0] = array(trans('labels.backend.emailtemplate.table.title'),
                trans('labels.backend.emailtemplate.table.subject'),
                trans('labels.backend.emailtemplate.table.last_updated'));
            //make array for json
            $i = 1;
            foreach ($records as $record) {
                $data[$i] = array($record->title, $record->subject, $record->updated_at->diffForHumans());
                $i++;
            }

            // download file
            $this->jqgrid->Download($this->downloadfilename, $data, $dtype);
        }
    }

    /**
     * @param  CreateEmailTemplateRequest $request
     * @return mixed
     */
    public function create(CreateEmailTemplateRequest $request) {
        $placeHolders = EmailTemplatePlaceholder::getPlaceHoldrList();
        $types = EmailTemplateType::getTypeList();
        return view('backend.emailtemplate.create')->with('placeHolders', $placeHolders)->with('types', $types);
    }

    /**
     * @param  StoreEmailTemplateRequest $request
     * @return mixed
     */
    public function store(StoreEmailTemplateRequest $request) {
        $this->emailTemplate->create($request);
        return redirect()->route('admin.emailtemplate.index')->withFlashSuccess(trans('alerts.backend.emailtemplate.created'));
    }

    /**
     * @param  $id 
     * @param  EditEmailTemplateRequest $request
     * @return mixed
     */
    public function edit($id, EditEmailTemplateRequest $request) {
        $placeHolders = EmailTemplatePlaceholder::getPlaceHoldrList();
        $types = EmailTemplateType::getTypeList();
        $result = $this->emailTemplate->findOrThrowException($id);
        return view('backend.emailtemplate.edit')->with('templates', $result)->with('placeHolders', $placeHolders)->with('types', $types);
    }

    /**
     * @param  $id
     * @param  UpdateEmailTemplateRequest $request
     * @return mixed
     */
    public function update($id, UpdateEmailTemplateRequest $request) {
        $this->emailTemplate->update($id, $request);
        return redirect()->route('admin.emailtemplate.index')->withFlashSuccess(trans('alerts.backend.emailtemplate.updated'));
    }

    /**
     * @param  $id
     * @param  DeleteEmailTemplateRequest $request
     * @return mixed
     */
    public function destroy($id, DeleteEmailTemplateRequest $request) {
        $this->emailTemplate->destroy($id);
        return response()->json(['status' => 'OK']);
    }

    /**
     * [deleteAll description]
     * @param  DeleteRequest $request [description]
     * @return [type]                        [description]
     */
    public function deleteAll(DeleteEmailTemplateRequest $request) {
        $ids = $request->get('params');
        foreach ($ids as $id) {
            $this->emailTemplate->destroy($id);
        }
        return response()->json(['status' => 'OK']);
    }

    public function exportExcel() {
        $result = $this->emailTemplate->selectAll($this->columnsexport);
        Excel::create($this->downloadfilename, function($excel) use($result) {
            $excel->sheet('Sheet 1', function($sheet) use($result) {
                $sheet->fromArray($result);
            });
        })->download('xls');
    }

    public function exportCSV() {
        $result = $this->emailTemplate->selectAll($this->columnsexport);
        Excel::create($this->downloadfilename, function($excel) use($result) {
            $excel->sheet('Sheet 1', function($sheet) use($result) {
                $sheet->fromArray($result);
            });
        })->export('csv');
    }

}
