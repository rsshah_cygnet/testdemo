<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PunctualityRatingCancelSession\CreateRequest;
use App\Http\Requests\Backend\PunctualityRatingCancelSession\DeleteRequest;
use App\Http\Requests\Backend\PunctualityRatingCancelSession\EditRequest;
use App\Http\Requests\Backend\PunctualityRatingCancelSession\MarkUserRequest;
use App\Http\Requests\Backend\PunctualityRatingCancelSession\StoreRequest;
use App\Http\Requests\Backend\PunctualityRatingCancelSession\UpdateRequest;
use App\Http\Requests\Backend\PunctualityRatingCancelSession\ViewRequest;
use App\Library\GridMaster\GridMaster;
use App\Models\PunctualityRatingCancelSession\PunctualityRatingCancelSession;
use App\Repositories\Backend\PunctualityRatingCancelSession\PunctualityRatingCancelSessionRepositoryContract;

class PunctualityRatingCancelSessionController extends Controller {
	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;

	/**
	 * __construct
	 *
	 * @param PunctualityRatingCancelSessionRepositoryContract $repository
	 */
	function __construct(PunctualityRatingCancelSessionRepositoryContract $repository) {
		$this->repository = $repository;
	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function index(ViewRequest $request) {

		return view('backend.PunctualityRatingCancelSession.index')->with(['repository' => $this->repository]);
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(ViewRequest $request) {
		$gridMaster = new GridMaster;

		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->getGridData($request, $this->repository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->downloadGridData($request, $this->repository, 'download');
		}
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkUserRequest $request) {
		$this->repository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * Delete Punctuality rating Model
	 *
	 * @param int $id
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRequest $request) {
		$item = $this->repository->findOrThrowException($id);
		if ($item->id) {
			$this->repository->destroy($id);
			return response()->json(['status' => "OK"]);
		}
	}
	/**
	 * show Punctuality rating form view
	 * @param CreateRequest $request
	 * @return type
	 */
	public function create(CreateRequest $request) {
		$type = ['' => 'Select Type', '1' => 'between', '2' => 'within'];

		return view('backend.PunctualityRatingCancelSession.create')->with(['formtype', 'create', 'type' => $type]);
	}

	/**
	 * store data in database
	 * @param StoreCmsRequest $request
	 * @return type
	 */
	public function store(StoreRequest $request) {

		$this->repository->create($request);
		// return response()->json((object) [
		// 	'status' => true,
		// 	'message' => 'Curriculum added Successfully',
		// ], 200);
		return redirect()->route('admin.punctualityratings-cancelsession.index')->withFlashSuccess('Punctuality Ratings has been created sucessfully !');
	}
	/**
	 * show edit form view with data
	 * @param type $id
	 * @param EditCurriculumRequest $request
	 * @return type
	 */
	public function edit($id, EditRequest $request) {
		$punctualityrating = $this->repository->findOrThrowException($id);
		$type = ['' => 'Select Type', '1' => 'between', '2' => 'within'];
		return view('backend.PunctualityRatingCancelSession.edit', compact('punctualityrating', $punctualityrating))->with('formtype', 'create')->with('type', $type);
		// return view('backend.cmspages.edit' , compact('cmsPage'));
	}

	/**
	 * update data into database
	 * @param type $id
	 * @param UpdateCmsRequest $request
	 * @return type
	 */
	public function update($id, UpdateRequest $request) {
		$punctualityrating = $this->repository->findOrThrowException($id);
		$this->repository->update($id, $request);
		return redirect()->route('admin.punctualityratings-cancelsession.index')->withFlashSuccess('Punctuality Ratings has been updated sucessfully !');
	}
}
