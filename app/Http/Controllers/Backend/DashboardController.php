<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Repositories\Backend\Frontusers\FrontusersRepositoryContract;
use App\Repositories\Backend\SessionSuccess\SessionSuccessRepositoryContract;
use App\Repositories\Backend\User\UserContract as BackendUser;
use App\Repositories\Frontend\User\UserContract;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends Controller {

	/**
	 * __construct
	 *
	 * @param FrontusersRepositoryContract $repository
	 */
	function __construct(FrontusersRepositoryContract $frontuser, BackendUser $adminuser, SessionSuccessRepositoryContract $session) {
		$this->frontuser = $frontuser;
		$this->adminuser = $adminuser;
		$this->session = $session;
	}
	/**
	 * @return \Illuminate\View\View
	 */
	public function index() {
		$tutors = $this->frontuser->getTutors();
		$ActiveTutor = $tutors->ActiveTutor;
		$InactiveTutor = $tutors->InactiveTutor;

		$tutees = $this->frontuser->getTutees();
		$ActiveTutees = $tutees->ActiveTutees;
		$InactiveTutess = $tutees->InactiveTutees;

		$adminusers = $this->adminuser->getAdminUser();
		$ActiveAdminUsers = $adminusers->ActiveAdminUsers;
		$InactiveAdminUsres = $adminusers->InactiveAdminUsres;

		$tutor_not_appied_test = $this->frontuser->getTutorNotAppliedForTest();

		$TutorIncompaleteTest = $this->frontuser->getTutorWithIncompeleteTest();

		$TutorFailInTest = $this->frontuser->getTutorFailInTest();

		$TutorFailedInOneOfSubject = $this->frontuser->getTutorFailedInOneOfSubject();

		$TutorPassedAllTest = $this->frontuser->getTutorPassedAllTest();

		$session = $this->adminuser->getUpcomingSession();
		$upcomingSession = $session->upcomingSession;
		$successfulSession = $session->successfulSession;
		$sessionCancelByTutor = $session->cancelByTutor;

		$sessionCancelByTutee = $session->cancelByTutee;

		$tutorWithHighestPunctualityRatings = $this->adminuser->getTutorWithHighestPunctualityRatings();

		/*Pie chart code*/

		$successful_session_count = $this->session->getSuccessfulSessionCount();
		$session_cancelled_by_tutee_count = $this->session->getCancelledByTuteeCount();
		$session_cancelled_by_tutor_count = $this->session->getCancelledByTutorCount();

		$amount_paid_by_tutee = $this->session->getTotalAmountPaidByTuteeForSuccess();
		$amount_received_by_admin = $this->session->getTotalAmountReceivedByAdminForSuccess();
		$amount_paid_to_tutor = $this->session->getTotalAmountPaidToTutorForSuccess();

		$tutee_refund_amount = $this->session->getTotalAmountRefundToTuteeForCancelledByTutor();

		$tutee_penalty_amount = $this->session->getTotalPenaltyAmountPaidByTutee();
		$tutor_earning_for_cancelled_by_tutee = $this->session->getTotalEarningOfTutorForCancelledByTuteeSessions();

		$amount_kept_with_admin_for_cancelled_by_tutee = $tutee_penalty_amount - $tutor_earning_for_cancelled_by_tutee;

		$sessionsData = [
			"amount_paid_by_tutee" => $amount_paid_by_tutee,
			"amount_received_by_admin" => $amount_received_by_admin,
			"amount_paid_to_tutor" => $amount_paid_to_tutor,
			"tutee_refund_amount" => $tutee_refund_amount,
			"tutee_penalty_amount" => $tutee_penalty_amount,
			"tutor_earning_for_cancelled_by_tutee" => $tutor_earning_for_cancelled_by_tutee,
			"amount_kept_with_admin_for_cancelled_by_tutee" => $amount_kept_with_admin_for_cancelled_by_tutee,
		];

		// $social_users=User::selectRaw('count(source) as count,source')->groupBy('source')->get();
		$user = array();
		$user = ['Session requests accepted' => $successful_session_count, "Sessions cancelled by tutor" => $session_cancelled_by_tutor_count, "Sessions cancelled by tutee" => $session_cancelled_by_tutee_count];

		$id = access()->user()->id;
		$loggedInUserName = \DB::table('users')
			->select(\DB::raw("CONCAT(first_name, ' ', last_name) as name"))
			->where('id', $id)
			->first();

		return view('backend.dashboard')
			->with('ActiveTutor', $ActiveTutor)
			->with('InactiveTutor', $InactiveTutor)
			->with('ActiveTutees', $ActiveTutees)
			->with('InactiveTutess', $InactiveTutess)
			->with('ActiveAdminUsers', $ActiveAdminUsers)
			->with('InactiveAdminUsres', $InactiveAdminUsres)
			->with('tutor_not_appied_test', $tutor_not_appied_test)
			->with('TutorIncompaleteTest', $TutorIncompaleteTest)
			->with('TutorFailInTest', $TutorFailInTest)
			->with('TutorFailedInOneOfSubject', $TutorFailedInOneOfSubject)
			->with('TutorPassedAllTest', $TutorPassedAllTest)
			->with('upcomingSession', $upcomingSession)
			->with('successfulSession', $successfulSession)
			->with('sessionCancelByTutee', $sessionCancelByTutee)
			->with('sessionCancelByTutor', $sessionCancelByTutor)
			->with('user', $user)
			->with('sessionsData', $sessionsData)
			->with('tutorWithHighestPunctualityRatings', $tutorWithHighestPunctualityRatings)
			->with('loggedInUserName', $loggedInUserName);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function view() {
		return view('backend.user.index')
			->withUser(access()->user());
	}

	/**
	 * @return mixed
	 */
	public function edit() {
		$user = access()->user();

		return view('backend.user.profile.edit', compact('user'));
	}

	/**
	 * update user profile
	 * @param UserContract $user
	 * @param UpdateProfileRequest $request
	 * @return type
	 */
	public function update(UserContract $user, UpdateProfileRequest $request) {
		$user->updateProfile(access()->id(), $request);

		return redirect()->route('backend.user.index')->withFlashSuccess(trans('strings.backend.user.profile_updated'));
	}

	public function filterPieChart(Request $request) {

		$post = $request->all();
		$month = $post['month'];
		$year = $post['year'];

		$firstday = date('01-' . $month . '-' . $year);
		$lastday = date(date('t', strtotime($firstday)) . '-' . $month . '-' . $year);

		$startDate = date("Y-m-d", strtotime($firstday));
		$endDate = date("Y-m-d", strtotime($lastday));



		$last_date = new \DateTime($endDate);

		$last_date->modify('+1 day');
		$endDate = $last_date->format('Y-m-d');


		/*Pie chart code*/
		$successful_session_count = $this->session->getSuccessfulSessionCount($startDate, $endDate);

		$session_cancelled_by_tutee_count = $this->session->getCancelledByTuteeCount($startDate, $endDate);
		$session_cancelled_by_tutor_count = $this->session->getCancelledByTutorCount($startDate, $endDate);

		$amount_paid_by_tutee = $this->session->getTotalAmountPaidByTuteeForSuccess($startDate, $endDate);
		$amount_received_by_admin = $this->session->getTotalAmountReceivedByAdminForSuccess($startDate, $endDate);
		$amount_paid_to_tutor = $this->session->getTotalAmountPaidToTutorForSuccess($startDate, $endDate);

		$tutee_refund_amount = $this->session->getTotalAmountRefundToTuteeForCancelledByTutor($startDate, $endDate);

		$tutee_penalty_amount = $this->session->getTotalPenaltyAmountPaidByTutee($startDate, $endDate);
		$tutor_earning_for_cancelled_by_tutee = $this->session->getTotalEarningOfTutorForCancelledByTuteeSessions($startDate, $endDate);

		$amount_kept_with_admin_for_cancelled_by_tutee = $tutee_penalty_amount - $tutor_earning_for_cancelled_by_tutee;

		$sessionsData = [
			"amount_paid_by_tutee" => $amount_paid_by_tutee,
			"amount_received_by_admin" => $amount_received_by_admin,
			"amount_paid_to_tutor" => $amount_paid_to_tutor,
			"tutee_refund_amount" => $tutee_refund_amount,
			"tutee_penalty_amount" => $tutee_penalty_amount,
			"tutor_earning_for_cancelled_by_tutee" => $tutor_earning_for_cancelled_by_tutee,
			"amount_kept_with_admin_for_cancelled_by_tutee" => $amount_kept_with_admin_for_cancelled_by_tutee,
		];

		$user = array();
		$user = ['Session requests accepted' => $successful_session_count, "Sessions cancelled by tutor" => $session_cancelled_by_tutor_count, "Sessions cancelled by tutee" => $session_cancelled_by_tutee_count];
		$returnHTML = view('backend.piechart', ['user' => $user, 'sessionsData' => $sessionsData])->render();
		//dd($returnHTML);
		if ($successful_session_count == 0 && $session_cancelled_by_tutor_count == 0 && $session_cancelled_by_tutee_count == 0) {
			return response()->json(array('status' => "error", 'html' => $returnHTML));
		}

		return response()->json(array('status' => "success", 'html' => $returnHTML));

	}

}
