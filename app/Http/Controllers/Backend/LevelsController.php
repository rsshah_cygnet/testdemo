<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Levels\LevelsRepositoryContract;
use App\Http\Requests\Backend\Levels\CreateRequest;
use App\Http\Requests\Backend\Levels\EditRequest;
use App\Http\Requests\Backend\Levels\StoreRequest;
use App\Http\Requests\Backend\Levels\ViewRequest;
use App\Http\Requests\Backend\Levels\UpdateRequest;
use App\Http\Requests\Backend\Levels\DeleteRequest;
use App\Http\Requests\Backend\Levels\MarkLevelsRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Library\GridMaster\GridMaster;
use App\Models\Levels\Levels;
use App\Models\Curriculum\Curriculum;
use App\Models\EducationalSystem\EducationalSystem;

/**
 * Class FrontusersController
 *
 * @author Sudhir Virpara
 */
class LevelsController extends Controller
{

    /**
     * Repository Object
     *
     * @var object
     */
    public $repository;
    

    /**
     * __construct
     *
     * @param BlogRepositoryContract $repository
     */
    function __construct(LevelsRepositoryContract $repository)
    {

        $this->repository = $repository;
        
    }

    /**
     * Listing
     *
     * @return mixed
     */
    public function index(ViewRequest $request)
    {

        return view('backend.Levels.index')->with(['repository' => $this->repository]);
    }

    /**
     * show data in grid
     * @param Request $request
     */
    public function data(ViewRequest $request)
    {
        $gridMaster = new GridMaster;

        if ($request->ajax())
        {
            return $gridMaster->setGridColumns($this->repository->gridColumn)
                            ->setRepository($this->repository)
                            ->getGridData($request, $this->repository, 'ajax');
        } else
        {
            $gridMaster->setGridColumns($this->repository->gridColumn)
                    ->setRepository($this->repository)
                    ->downloadGridData($request, $this->repository, 'download');
        }
    }

   
    /**
     * @param  $id
     * @param  $status
     * @param  MarkUserRequest $request
     * @return mixed
     */
    public function mark($id, $status, MarkLevelsRequest $request)
    {
        $this->repository->mark($id, $status);
        return response()->json(['status' => 'OK']);
        // return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    
    /**
     * Delete Blog Model
     *
     * @param int $id
     * @param DeleteRequest $request
     * @return mixed
     */
    public function destroy($id, DeleteRequest $request)
    {
        $item = $this->repository->findOrThrowException($id);
        if ($item->id)
        {
            $this->repository->destroy($id);
            return response()->json(['status' => "OK"]);
        }
    }


     /**
    * Create Blog Model
    *
    * @param CreateRequest $request
    * @return \Illuminate\View\View
    */
    public function create(CreateRequest $request)
    {

        $curriculums = Curriculum::getCurriculumList('display_in_level');
        $educationalsystems = EducationalSystem::getEducatinalSystem();
        //dd($curriculums);
        /*$tags       = BlogTag::getSelectData();
        $domains    = Domain::getSelectData('domain');*/


        //Returning to view with a form-type
        return view('backend.Levels.create')
            ->withCurriculums($curriculums)
            ->withEducationalsystems($educationalsystems)
            ->with('formtype','create');

    }


    /**
    * Store Blog Model
    *
    * @param StoreRequest $request
    * @return mixed
    */
    public function store(StoreRequest $request)
    {

       // dd($request->all());
       // $categories = array($request->categories);

        $postData = $request->all();
       
        
        if(isset($postData['levels_id']) && $postData['levels_id'] != "" && $postData['levels_id'] != 0)
        {
            $message = trans('Level has been successfully updated');
        }else{
            $message = trans('Level has been successfully created');
        }

        $model = $this->repository->create($postData);

        return redirect()->route('admin.levels.index')->withFlashSuccess($message);
       
    }


    public function edit($id, EditRequest $request)
    {

        //Finding the item
        $levels = $this->repository->findOrThrowException($id);

        $curriculums = Curriculum::getCurriculumList('display_in_level');
        $educationalsystems = EducationalSystem::getEducatinalSystem();

       
        $selectedCurriculam = $levels->curriculum_id;
        $selectedEducation = $levels->eductional_systems_id;
       
        //Returning the view with form-type edit
        return view('backend.Levels.edit')
                    ->withCurriculums($curriculums)
                    ->withEducationalsystems($educationalsystems)
                    ->withSelectedCurriculam($selectedCurriculam)
                    ->withSelectedEducation($selectedEducation)
                    ->withLevels($levels)
                    ->with('formtype','edit');

    }

}
