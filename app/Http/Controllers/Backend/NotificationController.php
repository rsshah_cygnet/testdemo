<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Notification\MarkNotificationRequest;
use App\Http\Utilities\Jqgrid;
use App\Models\Notification\Notification;
use App\Repositories\Backend\Notification\NotificationRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Facades\Excel;

class NotificationController extends Controller {

	/**
	 * @var columnsexport
	 */
	public $columnsexport = ['id', 'message', 'created_at', 'updated_at'];

	/**
	 * @var downloadfilename
	 */
	public $downloadfilename = 'notifications_result';

	/**
	 * @var EmailTemplateContract
	 */
	protected $notification;

	function __construct(NotificationRepositoryContract $notification, Jqgrid $jqgrid) {
		$this->notification = $notification;
		$this->jqgrid = $jqgrid;
	}

	/*
		     * Ajax data fetch function
	*/
	public function ajaxNotifications() {
		/*
			         * get user id
		*/
		$userId = Auth::user()->id;
		/*
			         * where conditions to get count
		*/
		$where = array('is_read_admin' => 0,'is_for_admin' => 1);
		
		/*
			         * get unread count
		*/
		$getUnreadNotificationCount = $this->notification->getNotification($where, 'count');
		/*
			         * where condition to list top notifications
		*/
		$listWhere = array('is_read_admin' => 0,'is_for_admin' => 1);
		
		/*
			         * get top 5 notifications
		*/
		$getNotifications = $this->notification->getNotification($listWhere, 'get', $limit = 5);
		/*
			         * preparing pass array which contain view and count both
		*/
		$passArray['view'] = view('backend.includes.notification')
			->with('notifications', $getNotifications)
			->with('unreadNotificationCount', $getUnreadNotificationCount)
			->render();
		$passArray['count'] = $getUnreadNotificationCount;
		/*
			         * pass jsonencode array
		*/
		echo json_encode($passArray);
		die;
	}

	/*
		     * clearCurrentNotifications
	*/
	public function clearCurrentNotifications() {
		$userId = Auth::user()->id;
		echo $this->notification->clearNotifications(5);
		die;
	}

	/**
	 * @return mixed
	 */
	public function index(Request $request) {

		/*$final_type_string = "";
			        $notification_types = NotificationType::getNotificationtypes();
			        $notification_types = array_merge(['0' => 'All'],$notification_types);

			        if(!empty($notification_types)){
			            foreach ($notification_types as $key => $value) {

			                $noti_types = "$key:$value;";

			                $final_type_string =  $final_type_string.$noti_types;
			            }

		*/
		return view('backend.notification.index');
	}

	/**
	 * [data description]
	 * @return [type] [description]
	 */
	public function data(Request $request) {
		// $results = $this->users->getAllUsers();

		if ($request->ajax()) {

			$data = $this->jqgrid->Preparedata($request, $this->notification, 'ajax');
			$responce = $data[0];
			$records = $data[1];
			$dateFormat = Config::get('constants.DATEFORMAT');
			$i = 0;

			//make array for json

			foreach ($records as $record) {

				$responce->rows[$i]['id'] = $record->id;
				$responce->rows[$i]['cell'] = array($record->message, $record->notification_type, $record->created_at, $record->confirmed_label, $record->action_buttons);

				$i++;
			}

			echo json_encode($responce);
		} else {
			// EXPORT GRID START
			$dtype = $request->get('dtype');
			$records = $this->jqgrid->Preparedata($request, $this->notification, 'download');

			$data = array();
			$data[0] = array(trans('labels.backend.notification.table.message'),
				trans('labels.backend.notification.table.created_at'),
				trans('labels.backend.notification.table.read'));
			//make array for json
			$i = 1;
			foreach ($records as $record) {
				$data[$i] = array($record->message, $record->created_at, $record->is_read_admin);
				$i++;
			}

			// download file
			$this->jqgrid->Download($this->downloadfilename, $data, $dtype);
		}
	}

	/**
	 * [export Excel description]
	 * @param   []
	 * @return [Excel File]                        [description]
	 */

	public function exportExcel() {
		$result = $this->notification->selectAll($this->columnsexport);
		Excel::create($this->downloadfilename, function ($excel) use ($result) {
			$excel->sheet('Sheet 1', function ($sheet) use ($result) {
				$sheet->fromArray($result);
			});
		})->download('xls');
	}
	/**
	 * [export CSV description]
	 * @param   []
	 * @return [Excel File]                        [description]
	 */
	public function exportCSV() {
		$result = $this->notification->selectAll($this->columnsexport);
		Excel::create($this->downloadfilename, function ($excel) use ($result) {
			$excel->sheet('Sheet 1', function ($sheet) use ($result) {
				$sheet->fromArray($result);
			});
		})->export('csv');
	}
	/**
	 *
	 * @param type $id
	 * @param type $status
	 * @param MarkNotificationRequest $request
	 * @return type
	 */
	public function mark($id, $status, MarkNotificationRequest $request) {
		$this->notification->mark($id, $status);
		return response()->json(['status' => 'OK']);
	}

}
