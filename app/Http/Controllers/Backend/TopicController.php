<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Topic\TopicRepositoryContract;
use App\Http\Requests\Backend\Topic\CreateRequest;
use App\Http\Requests\Backend\Topic\EditRequest;
use App\Http\Requests\Backend\Topic\StoreRequest;
use App\Http\Requests\Backend\Topic\ViewRequest;
use App\Http\Requests\Backend\Topic\UpdateRequest;
use App\Http\Requests\Backend\Topic\DeleteRequest;
use App\Http\Requests\Backend\Topic\MarkTopicRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Library\GridMaster\GridMaster;
use App\Models\Topic\Topic;
use App\Models\Curriculum\Curriculum;
use App\Models\EducationalSystem\EducationalSystem;

/**
 * Class TopicController
 */
class TopicController extends Controller
{

   /**
     * Repository Object
     *
     * @var object
     */
    public $repository;
    
    /**
     * TopicController constructor.
     *
     * @param TopicRepositoryContract $repository
     */
    function __construct(TopicRepositoryContract $repository)
    {

        $this->repository = $repository;
        
    }

    /**
     * Listing
     *
     * @return mixed
     */
    public function index(ViewRequest $request)
    {

        return view('backend.Topic.index')->with(['repository' => $this->repository]);
    }

    /**
     * Show data in grid
     *
     * @param ViewRequest $request
     * @return string
     */
    public function data(ViewRequest $request)
    {
        $gridMaster = new GridMaster;

        if ($request->ajax())
        {
            return $gridMaster->setGridColumns($this->repository->gridColumn)
                            ->setRepository($this->repository)
                            ->getGridData($request, $this->repository, 'ajax');
        } else
        {
            $gridMaster->setGridColumns($this->repository->gridColumn)
                    ->setRepository($this->repository)
                    ->downloadGridData($request, $this->repository, 'download');
        }
    }

    /**
     * Change in Topic Active or InActive
     *
     * @param $id
     * @param $status
     * @param MarkTopicRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mark($id, $status, MarkTopicRequest $request)
    {
        $this->repository->mark($id, $status);
        return response()->json(['status' => 'OK']);
    }

    
    /**
     * Delete Topic
     *
     * @param int $id
     * @param DeleteRequest $request
     * @return mixed
     */
    public function destroy($id, DeleteRequest $request)
    {
        $item = $this->repository->findOrThrowException($id);
        if ($item->id)
        {
            $this->repository->destroy($id);
            return response()->json(['status' => "OK"]);
        }
    }


     /**
    * Create Topic
    *
    * @param CreateRequest $request
    * @return \Illuminate\View\View
    */
    public function create(CreateRequest $request)
    {

        $curriculums = Curriculum::getCurriculumList();
        $educationalsystems = EducationalSystem::getEducatinalSystem();
        
        //Returning to view with a form-type create
        return view('backend.Topic.create')
            ->withCurriculums($curriculums)
            ->withEducationalsystems($educationalsystems)
            ->with('formtype','create');

    }


    /**
    * Save topic 
    *
    * @param StoreRequest $request
    * @return mixed
    */
    public function store(StoreRequest $request)
    {

        $postData = $request->all();
        if(isset($postData['topic_id']) && $postData['topic_id'] != "" && $postData['topic_id'] != 0)
        {
            $message = trans('alerts.backend.topic.edit');
        }else{
            $message = trans('alerts.backend.topic.add');
        }
        $model = $this->repository->create($postData);

        return redirect()->route('admin.topic.index')->withFlashSuccess($message);
       
    }


    /**
     * Edit Topic
     *
     * @param int $id
     * @param EditRequest $request
     * @return $this
     */
    public function edit($id, EditRequest $request)
    {

        $topic = $this->repository->findOrThrowException($id);

        $curriculums = Curriculum::getCurriculumList();
        $educationalsystems = EducationalSystem::getEducatinalSystem();

       
        $selectedCurriculam = $topic->curriculum_id;
        $selectedEducation = $topic->eductional_systems_id;
       
        //Returning the view with form-type edit
        return view('backend.Topic.edit')
                    ->withCurriculums($curriculums)
                    ->withEducationalsystems($educationalsystems)
                    ->withSelectedCurriculam($selectedCurriculam)
                    ->withSelectedEducation($selectedEducation)
                    ->withTopic($topic)
                    ->with('formtype','edit');

    }

}
