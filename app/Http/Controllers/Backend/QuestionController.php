<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Question\CreateRequest;
use App\Http\Requests\Backend\Question\DeleteRequest;
use App\Http\Requests\Backend\Question\EditExamSettingRequest;
use App\Http\Requests\Backend\Question\EditRequest;
use App\Http\Requests\Backend\Question\ImportRequest;
use App\Http\Requests\Backend\Question\MarkQuestionRequest;
use App\Http\Requests\Backend\Question\StoreRequest;
use App\Http\Requests\Backend\Question\UpdateExamSettingsRequest;
use App\Http\Requests\Backend\Question\ViewRequest;
use App\Library\GridMaster\GridMaster;
use App\Models\Curriculum\Curriculum;
use App\Models\Topic\Topic;
use App\Repositories\Backend\Question\QuestionRepositoryContract;
use Illuminate\Support\Facades\Redirect;
use PHPExcel_IOFactory;

/**
 * Class FrontusersController
 *
 * @author Sudhir Virpara
 */
class QuestionController extends Controller {

	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;

	/**
	 * __construct
	 *
	 * @param BlogRepositoryContract $repository
	 */
	function __construct(QuestionRepositoryContract $repository) {

		$this->repository = $repository;

	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function index(ViewRequest $request) {

		return view('backend.Question.index')->with(['repository' => $this->repository]);
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(ViewRequest $request) {
		$gridMaster = new GridMaster;

		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->getGridData($request, $this->repository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->downloadGridData($request, $this->repository, 'download');
		}
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkQuestionRequest $request) {
		$this->repository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * Delete Blog Model
	 *
	 * @param int $id
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRequest $request) {
		$item = $this->repository->findOrThrowException($id);
		if ($item->id) {
			$this->repository->destroy($id);
			return response()->json(['status' => "OK"]);
		}
	}

	/**
	 * Create Blog Model
	 *
	 * @param CreateRequest $request
	 * @return \Illuminate\View\View
	 */
	public function save_import(ImportRequest $request) {
		// dd($request);
		$file = "";
		if (isset($request->imported_file) && $request->imported_file != null && $request->imported_file != "") {

			$sheetName = time();
			$extension = $request->imported_file->getClientOriginalExtension();
			$valid_extensions = ['xls','xlsx','XLS','XLSX'];

			  if (!in_array($extension, $valid_extensions))
			  {
			  	return Redirect::back()->withErrors(['The imported file must be a file of type: xls,xlsx,XLS,XLSX.']);
			  }

			$file = $sheetName . '.' . $request->imported_file->getClientOriginalExtension();
			$path = public_path('uploads/question_import/');
			$remove_path = public_path('uploads/question_import/*');
			$remove_path_img = public_path('uploads/question_file_image/*');
			array_map('unlink', glob($remove_path));
			array_map('unlink', glob($remove_path_img));

			$request->imported_file->move($path, $file);
			/*import question start here*/
			//$path = "/var/www/html/tuteme/mcq.xlsx";
			$path = $path . $file;
			$objPHPExcel = PHPExcel_IOFactory::load($path);
			// $sheetName = $objPHPExcel->getActiveSheet()->getTitle();
			$i = 0;
			foreach ($objPHPExcel->getActiveSheet()->getDrawingCollection() as $drawing) {
				if ($drawing instanceof PHPExcel_Worksheet_MemoryDrawing) {
					ob_start();
					call_user_func(
						$drawing->getRenderingFunction(),
						$drawing->getImageResource()
					);
					$imageContents = ob_get_contents();
					ob_end_clean();
					switch ($drawing->getMimeType()) {
					case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_PNG:
						$extension = 'png';
						break;
					case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_GIF:
						$extension = 'gif';
						break;
					case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_JPEG:
						$extension = 'jpg';
						break;
					case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_JPEG:
						$extension = 'jpeg';
						break;

					}
				} else {
					$zipReader = fopen($drawing->getPath(), 'r');
					$imageContents = '';
					while (!feof($zipReader)) {
						$imageContents .= fread($zipReader, 1024);
					}
					fclose($zipReader);
					$extension = $drawing->getExtension();
				}
				$name = $drawing->getCoordinates();
				$upload_path = public_path('uploads/question_file_image/');
				$myFileName = $upload_path . $sheetName . '_' . $name . '.' . $extension;
				file_put_contents($myFileName, $imageContents);
			}

			$inputFile = $path;

			$extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
			// if($extension == 'XLSX' || $extension == 'ODS'){

			//Read spreadsheeet workbook
			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFile);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFile);
			} catch (Exception $e) {
				die($e->getMessage());
			}

			//Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$save_path = public_path('uploads/question/');
			//Loop through each row of the worksheet in turn
			for ($row = 3; $row <= $highestRow; $row++) {

				$final_data[$row]['question'] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getValue();
				$final[$row]['img'] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $row)->getValue();

				$final_data[$row]['option1'] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $row)->getValue();
				$final_data[$row]['option2'] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $row)->getValue();
				$final_data[$row]['option3'] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $row)->getValue();
				$final_data[$row]['option4'] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $row)->getValue();
				$final_data[$row]['ans'] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getValue();

				//for question image
				$url = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $row)->getHyperlink()->getUrl();
				$file_name = time() . randomString();
				if ($url != "") {
					$extention_of_img = explode('.', $url);
					$extension = end($extention_of_img);
					$final_file_name = $file_name . '.' . $extension;
					$final_data[$row]['img'] = $final_file_name;

					$this->grab_image($url, $save_path . $final_file_name);
				}

				//for option1 image

				$url_opt1 = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $row)->getHyperlink()->getUrl();
				$opt1_name = time() . randomString();
				if ($url_opt1 != "") {
					$extention_of_img = explode('.', $url_opt1);
					$extension = end($extention_of_img);
					$final_file_name = $opt1_name . '.' . $extension;
					$final_data[$row]['img_opt1'] = $final_file_name;

					$this->grab_image($url_opt1, $save_path . $final_file_name);
				}

				//for option2 image

				$url_opt2 = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $row)->getHyperlink()->getUrl();
				$opt2_name = time() . randomString();
				if ($url_opt2 != "") {
					$extention_of_img = explode('.', $url_opt2);
					$extension = end($extention_of_img);
					$final_file_name = $opt2_name . '.' . $extension;
					$final_data[$row]['img_opt2'] = $final_file_name;

					$this->grab_image($url_opt2, $save_path . $final_file_name);
				}

				//for option3 image

				$url_opt3 = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $row)->getHyperlink()->getUrl();
				$opt3_name = time() . randomString();
				if ($url_opt3 != "") {
					$extention_of_img = explode('.', $url_opt3);
					$extension = end($extention_of_img);
					$final_file_name = $opt3_name . '.' . $extension;
					$final_data[$row]['img_opt3'] = $final_file_name;

					$this->grab_image($url_opt3, $save_path . $final_file_name);
				}

				//for option4 image

				$url_opt4 = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $row)->getHyperlink()->getUrl();
				$opt4_name = time() . randomString();
				if ($url_opt4 != "") {
					$extention_of_img = explode('.', $url_opt4);
					$extension = end($extention_of_img);
					$final_file_name = $opt4_name . '.' . $extension;
					$final_data[$row]['img_opt4'] = $final_file_name;

					$this->grab_image($url_opt4, $save_path . $final_file_name);
				}

				$path = public_path('uploads/question_file_image/');
				$des_path = public_path('uploads/question/');

				$files = scandir($path);
				unset($files['0']);
				unset($files['1']);
				if (!empty($files)) {
					foreach ($files as &$value) {
						$final = [];

						$sheet_img_name = explode('_', $value);
						$name = explode('.', $sheet_img_name['1']);

						$final_img_name = str_replace("C", "", $name[0]);
						$final_img_name1 = str_replace("D", "", $name[0]);
						$final_img_name2 = str_replace("E", "", $name[0]);
						$final_img_name3 = str_replace("F", "", $name[0]);
						$final_img_name4 = str_replace("G", "", $name[0]);

						if ($row == $final_img_name) {
							$push['question_image'] = $value;
							$final_data[$row] = array_merge($final_data[$row], $push);
							copy($path . $value, $des_path . $value);
							unlink($path . $value);
							unset($push);
						}
						//     dd($row);
						if ($row == $final_img_name1) {
							$push['img_option1'] = $value;
							$final_data[$row] = array_merge($final_data[$row], $push);
							copy($path . $value, $des_path . $value);
							unlink($path . $value);
							unset($push);
						}

						if ($row == $final_img_name2) {
							$push['img_option2'] = $value;
							$final_data[$row] = array_merge($final_data[$row], $push);
							copy($path . $value, $des_path . $value);
							unlink($path . $value);
							unset($push);
						}

						if ($row == $final_img_name3) {
							$push['img_option3'] = $value;
							$final_data[$row] = array_merge($final_data[$row], $push);
							copy($path . $value, $des_path . $value);
							unlink($path . $value);
							unset($push);
						}

						if ($row == $final_img_name4) {
							$push['img_option4'] = $value;
							$final_data[$row] = array_merge($final_data[$row], $push);
							copy($path . $value, $des_path . $value);
							unlink($path . $value);
							unset($push);
						}
						//array_merge_recursive( $final_data, $final);
						//$image_name[] = $final;
					}
				}

			}
			// }
			/*else{
	                            echo "Please upload an XLSX or ODS file";
*/
			if (!empty($final_data)) {
//dd($final_data);
				foreach ($final_data as $key => $value) {
					$data['topic_id'] = $request->topic_id;
					if (isset($value['question']) && $value['question'] != "" && $value['question'] != 'null') {
						$this->repository->create_import($value, $data);
					}

				}
			}
			unlink($inputFile);
			return redirect()->route('admin.question.index')->withFlashSuccess('Questions imported successfully');
			/*import question end here*/
		} else {
			return true;
		}
	}

	public function grab_image($url, $saveto) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		$raw = curl_exec($ch);
		curl_close($ch);
		if (file_exists($saveto)) {
			unlink($saveto);
		}
		$fp = fopen($saveto, 'x');
		fwrite($fp, $raw);
		fclose($fp);
	}

	/**
	 * Create Blog Model
	 *
	 * @param CreateRequest $request
	 * @return \Illuminate\View\View
	 */
	public function create(CreateRequest $request) {
		$curriculums = Curriculum::getCurriculumList();
		return view('backend.Question.create')
			->withCurriculums($curriculums)
			->with('formtype', 'create');

	}

	public function import(CreateRequest $request) {
		$curriculums = Curriculum::getCurriculumList();
		return view('backend.Question.import')
			->withCurriculums($curriculums)
			->with('formtype', 'create');

	}

	/**
	 * Store Blog Model
	 *
	 * @param StoreRequest $request
	 * @return mixed
	 */
	public function store(StoreRequest $request) {
		
		$postData = $request->all();
		//for option4 image
		$option4_image_name = "";
		if (isset($request->option4_image) && $request->option4_image != null && $request->option4_image != "") {
			$option4_image_name = time() . '.' . $request->option4_image->getClientOriginalExtension();
			$path = public_path('uploads/question/');
			$request->option4_image->move($path, $option4_image_name);
			if (isset($request->old_option4_img) && $request->old_option4_img != "") {
				unlink($path . $request->old_option4_img);
			}

		} else {
			$option4_image_name = $request->old_option4_img;
		}

		//for option3 image
		$option3_image_name = "";
		if (isset($request->option3_image) && $request->option3_image != null && $request->option3_image != "") {
			$option3_image_name = time() . '.' . $request->option3_image->getClientOriginalExtension();
			$path = public_path('uploads/question/');
			$request->option3_image->move($path, $option3_image_name);
			if (isset($request->old_option3_img) && $request->old_option3_img != "") {
				unlink($path . $request->old_option3_img);
			}
		} else {
			$option3_image_name = $request->old_option3_img;
		}

		//for option2 image
		$option2_image_name = "";
		if (isset($request->option2_image) && $request->option2_image != null && $request->option2_image != "") {
			$option2_image_name = time() . '.' . $request->option2_image->getClientOriginalExtension();
			$path = public_path('uploads/question/');
			$request->option2_image->move($path, $option2_image_name);
			if (isset($request->old_option2_img) && $request->old_option2_img != "") {
				unlink($path . $request->old_option2_img);
			}
		} else {
			$option2_image_name = $request->old_option2_img;
		}

		//for option1 image
		$option1_image_name = "";
		if (isset($request->option1_image) && $request->option1_image != null && $request->option1_image != "") {
			$option1_image_name = time() . '.' . $request->option1_image->getClientOriginalExtension();
			$path = public_path('uploads/question/');
			$request->option1_image->move($path, $option1_image_name);
			if (isset($request->old_option1_img) && $request->old_option1_img != "") {
				unlink($path . $request->old_option1_img);
			}
		} else {
			$option1_image_name = $request->old_option1_img;
		}

/*

if(isset($postData['question_id']) && $postData['question_id'] != "" && $postData['question_id'] != 0)
{
$request->option4_image = isset($request->option4_image)?$request->option4_image : $request->old_option4_img;
$request->option3_image = isset($request->option3_image)?$request->option3_image : $request->old_option3_img;
$request->option2_image = isset($request->option2_image)?$request->option2_image : $request->old_option2_img;
$request->option1_image = isset($request->option1_image)?$request->option1_image : $request->old_option1_img;

}*/
		 // dd($request->all());
		if (isset($request->option1) && $request->option1 != "" && isset($option1_image_name) && $option1_image_name != "") {
			return Redirect::back()
				->withInput()
				->withErrors('Image and text both not allowed for option1.please Upload image or add text.');
		}

		if (isset($request->option2) && $request->option2 != "" && isset($option2_image_name) && $option2_image_name != "") {
			return Redirect::back()
				->withInput()
				->withErrors('Image and text both not allowed for option2.please Upload image or add text.');
		}

		if (isset($request->option3) && $request->option3 != "" && isset($option3_image_name) && $option3_image_name != "") {
			return Redirect::back()
				->withInput()
				->withErrors('Image and text both not allowed for option3.please Upload image or add text.');
		}

		if (isset($request->option4) && $request->option4 != "" && isset($option4_image_name) && $option4_image_name != "") {
			return Redirect::back()
				->withInput()
				->withErrors('Image and text both not allowed for option4.please Upload image or add text.');
		}

		if (isset($request->option1) && $request->option1 == "" && $option1_image_name == "") {
			return Redirect::back()
				->withInput()
				->withErrors('Please Upload image or add text for option1');
		}

		if (isset($request->option2) && $request->option2 == "" && $option2_image_name == "") {
			return Redirect::back()
				->withInput()
				->withErrors('Please Upload image or add text for option2');
		}

		if (isset($request->option3) && $request->option3 == "" && $option3_image_name == "") {
			return Redirect::back()
				->withInput()
				->withErrors('Please Upload image or add text for option3');
		}

		if (isset($request->option4) && $request->option4 == "" && $option4_image_name == "") {
			return Redirect::back()
				->withInput()
				->withErrors('Please Upload image or add text for option4');
		}

		// dd($path);
		$imageName = "";
		if (isset($request->question_image) && $request->question_image != null && $request->question_image != "") {
			$imageName = time() . '.' . $request->question_image->getClientOriginalExtension();
			//$path = "/var/www/html/tuteme/public/uploads/question/";
			$path = public_path('uploads/question/');
			$request->question_image->move($path, $imageName);
		} else {
			$imageName = $request->question_img;
		}

		$postData['final_image'] = $imageName;
		$postData['option1_image_name'] = $option1_image_name;
		$postData['option2_image_name'] = $option2_image_name;
		$postData['option3_image_name'] = $option3_image_name;
		$postData['option4_image_name'] = $option4_image_name;

		if (isset($postData['question_id']) && $postData['question_id'] != "" && $postData['question_id'] != 0) {
			$msg = 'Question has been successfully updated';
			if (isset($request->question_image) && $request->question_image != null && $request->question_image != "") {
				unlink($path . $request->question_img);
			}

		} else {
			$msg = 'Question has been successfully added';
		}
		$model = $this->repository->create($postData);

		return redirect()->route('admin.question.index')->withFlashSuccess($msg);

	}

	public function edit($id, EditRequest $request) {

		//Finding the item
		$question = $this->repository->findOrThrowException($id);

		$curriculums = Curriculum::getCurriculumList();

		$selectedCurriculam = Topic::select('curriculum_id')->where('id', '=', $question->topic_id)->first();

		$selectedCurriculam = $selectedCurriculam->curriculum_id;
		// dd($selectedCurriculam);
		//Returning the view with form-type edit
		return view('backend.Question.edit')
			->withCurriculums($curriculums)
			->with('selectedCurriculam', $selectedCurriculam)
			->withQuestion($question)
			->with('formtype', 'edit');

	}

	/**
	 * Exam settings
	 */
	public function examsetting(EditExamSettingRequest $request) {
		$result = $this->repository->findExamSettings(1);
		return view('backend.Question.examsettings')
			->withSettings($result);
	}

	/**
	 * update data in database
	 * @param type $id
	 * @param UpdateExamSettingsRequest $request
	 * @return type
	 */
	public function storeexamsetting($id, UpdateExamSettingsRequest $request) {
		$this->repository->update($id, $request);
		return redirect()->route('admin.exam.settings')->withFlashSuccess(trans('alerts.backend.settings.updated'));
	}

}
