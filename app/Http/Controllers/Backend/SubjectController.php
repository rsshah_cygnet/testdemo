<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Subject\CreateRequest;
use App\Http\Requests\Backend\Subject\DeleteRequest;
use App\Http\Requests\Backend\Subject\EditRequest;
use App\Http\Requests\Backend\Subject\MarkSubjectRequest;
use App\Http\Requests\Backend\Subject\StoreRequest;
use App\Http\Requests\Backend\Subject\ViewRequest;
use App\Library\GridMaster\GridMaster;
use App\Models\Curriculum\Curriculum;
use App\Models\EducationalSystem\EducationalSystem;
use App\Repositories\Backend\Subject\SubjectRepositoryContract;

/**
 * Class FrontusersController
 *
 * @author Nikita Patel
 */
class SubjectController extends Controller {

	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;

	/**
	 * __construct
	 *
	 * @param BlogRepositoryContract $repository
	 */
	function __construct(SubjectRepositoryContract $repository) {

		$this->repository = $repository;

	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function index(ViewRequest $request) {

		return view('backend.Subject.index')->with(['repository' => $this->repository]);
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(ViewRequest $request) {
		$gridMaster = new GridMaster;

		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->getGridData($request, $this->repository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->downloadGridData($request, $this->repository, 'download');
		}
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkSubjectRequest $request) {
		$this->repository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * Delete Blog Model
	 *
	 * @param int $id
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRequest $request) {
		$item = $this->repository->findOrThrowException($id);
		if ($item->id) {
			$this->repository->destroy($id);
			return response()->json(['status' => "OK"]);
		}
	}

	/**
	 * Create Blog Model
	 *
	 * @param CreateRequest $request
	 * @return \Illuminate\View\View
	 */
	public function create(CreateRequest $request) {

		$curriculums = Curriculum::getCurriculumList();
		$educationalsystems = EducationalSystem::getEducatinalSystem();
		//dd($educationalsystems);
		/*$tags       = BlogTag::getSelectData();
        $domains    = Domain::getSelectData('domain');*/

		//Returning to view with a form-type
		return view('backend.Subject.create')
			->withCurriculums($curriculums)
			->withEducationalsystems($educationalsystems)
			->with('formtype', 'create');

	}

	/**
	 * Store Blog Model
	 *
	 * @param StoreRequest $request
	 * @return mixed
	 */
	public function store(StoreRequest $request) {

		// dd($request->all());
		// $categories = array($request->categories);

		$postData = $request->all();
		
		if(isset($postData['subject_id']) && $postData['subject_id'] != "" && $postData['subject_id'] != 0)
        {
            $message = trans('Subject has been successfully updated');
        }else{
            $message = trans('Subject has been successfully created');
        }

		$model = $this->repository->create($postData);

		return redirect()->route('admin.subject.index')->withFlashSuccess($message);

	}

	public function edit($id, EditRequest $request) {

		//Finding the item
		$subject = $this->repository->findOrThrowException($id);

		$curriculums = Curriculum::getCurriculumList();
		$educationalsystems = EducationalSystem::getEducatinalSystem();

		$selectedCurriculam = $subject->curriculum_id;
		$selectedEducation = $subject->eductional_systems_id;

		//Returning the view with form-type edit
		return view('backend.Subject.edit')
			->withCurriculums($curriculums)
			->withEducationalsystems($educationalsystems)
			->withSelectedCurriculam($selectedCurriculam)
			->withSelectedEducation($selectedEducation)
			->withSubject($subject)
			->with('formtype', 'edit');

	}

}
