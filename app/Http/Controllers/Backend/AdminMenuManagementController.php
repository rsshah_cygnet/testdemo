<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\MenuManagement\MenuManagementRepositoryContract;
use App\Http\Requests\Backend\MenuManagement\CreateRequest;
use App\Http\Requests\Backend\MenuManagement\EditRequest;
use App\Http\Requests\Backend\MenuManagement\StoreRequest;
use App\Http\Requests\Backend\MenuManagement\ViewRequest;
use App\Http\Requests\Backend\MenuManagement\UpdateRequest;
use App\Http\Requests\Backend\MenuManagement\DeleteRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Library\GridMaster\GridMaster;
use App\Models\MenuManagement\Menus;
use App\Models\Domain\Domain;
use App\Models\Cmspage\Cmspage;
use Illuminate\Http\Request;

class AdminMenuManagementController extends Controller
{
     /**
     * Repository Object
     * 
     * @var object
     */
    public $repository;

    /**
     * __construct
     * 
     * @param MenuManagementRepositoryContract $repository
     */
    function __construct(MenuManagementRepositoryContract $repository) 
    {
        $this->repository = $repository;
    }
    
    /**
     * Listing 
     * 
     * @return mixed
     */
    public function index(ViewRequest $request)
    {
        return view('backend.menumanagement.index')->with(['repository' => $this->repository]);
    }

    /**
     * show data in grid 
     * @param Request $request
     */
    public function data(ViewRequest $request) 
    {
        $gridMaster = new GridMaster;

        if ($request->ajax()) 
        {
            return $gridMaster->setGridColumns($this->repository->gridColumn)
                ->setRepository($this->repository)
                ->getGridData($request, $this->repository, 'ajax');
        }
        else
        {
            $gridMaster->setGridColumns($this->repository->gridColumn)
                ->setRepository($this->repository)
                ->downloadGridData($request, $this->repository, 'download');
        }
    }

    /**
    * Create Menu Item
    *
    * @param CreateRequest $request
    * @return \Illuminate\View\View
    */
    public function create(CreateRequest $request)
    {
           
        $parentmenu  = Menus::where('parent_menu_id',0)
                            ->where('status', 'Active')
                            ->where('deleted_at', null)
                            ->pluck('name', 'id')
                            ->toArray();

        $website     = Domain::getSelectData('domain', true,'status','Active','deleted_at');
        $contentpage = [];

		$returnHTML =  view('backend.menumanagement.create')
                        ->with([
                                'website'     => $website,
                                'contentpage' => $contentpage,
                                'parentmenu'  => $parentmenu
                            ])
                        ->render();

        return response()->json(array($returnHTML));
    }

    /**
    * Store Menu Item
    *
    * @param StoreRequest $request
    * @return mixed
    */
    public function store(StoreRequest $request)
    {   
        $input = $request->all();
        $menus = Menus::where('parent_menu_id', '=', $input["parent_menu_id"])
                    ->where('cmspage_id', '=', $input["cmspage_id"])
                    ->where('name', '=', $input["name"])
                    ->where('deleted_at', null)
                    ->first();
        if ($menus !== null) 
        {
            return redirect()->back()->withFlashDanger('Menu Item Already Exsist');
        }

        $model = $this->repository->create($request->all());
        
        return response()->json((object) [
                'status' => true,
                'message' => 'Menu Item added Successfully',
            ], 200);
    }

    /**
     * Edit Menu Item
     *
     * @param int $id
     * @param EditRequest $request
     * @return $this
     */
    public function edit($id, EditRequest $request)
    {   
        $item = $this->repository->findOrThrowException($id);
      
        $parentmenu  = Menus::where('parent_menu_id',0)
                            ->where('status', 'Active')
                            ->where('deleted_at', null)
                            ->pluck('name', 'id')
                            ->toArray();

        $website     = Domain::getSelectData('domain', true,'status','Active','deleted_at');
        $contentpage = Cmspage::where('domain_id',$item->domain_id)
                            ->where('is_active', 'Active')
                            ->pluck('title','id')
                            ->all();

        $returnHTML = view('backend.menumanagement.edit')
                        ->with([
                                    "item" => $item,
                                    'menus' => Menus::getSelectData('name', true,'status','Active','deleted_at'),
                                    'website' => $website,
                                    'contentpage' => $contentpage,
                                    'parentmenu' => $parentmenu
                                ])
                        ->render();

        return response()->json(array($returnHTML));
    }

    /**
     * Update Menu Item
     *
     * @param int $id
     * @param UpdateRequest $request
     * @return $this
     */
    public function update($id, UpdateRequest $request)
    { 
        $input = $request->all();
        $status = isset($input["record_status"]) == 1 ? 'Active' : 'InActive';
        $menus = Menus::where('parent_menu_id', '=', $input["parent_menu_id"])
                                ->where('name', '=', $input["name"])
                                ->where('cmspage_id', '=', $input["cmspage_id"])
                                ->where('id', '!=', $id)
                                ->where('deleted_at', null)
                                ->first();

        if($menus != null) 
        {
            return redirect()->back()->withFlashDanger('Menu Item Already Exsist');  
        }


        $model = $this->repository->update($id, $request->all());

        if($model)
        {
            return response()->json((object) [
                'status'    => true,
                'message'   => 'Menu Item Edited Successfully'
            ], 200);  
        }

        return response()->json((object) [
                'status'    => false,
                'message'   => 'Invalid Menu Id, Please Try Again !'
            ], 200);
    }

    /**
     * Delete Menu Item
     *
     * @param int $id
     * @param DeleteRequest $request
     * @return mixed
     */
    public function destroy($id, DeleteRequest $request)
    {
        $item = $this->repository->findOrThrowException($id);

        if($item->id)
        {
            $this->repository->destroy($id);
            return response()->json(['status' => 'OK']);    
        }
    }

    /**
     * get the Content Page from database by domain id
     *
     */
    public function getCmsPage() 
    {
        $type  = $_POST['value'];
        $place = Cmspage::where('domain_id',$type)
                        ->where('is_active', 'Active')
                        ->pluck('title','id')
                        ->all();
        
        return json_encode($place);
    }

    /**
     * change in brand active or inactive 
     * @param type $id
     * @param type $status
     * @param Request $request
     * @return type
     */
    public function mark($id, $status, Request $request) 
    {  
        $this->repository->mark($id, $status);
        return response()->json(['status' => 'OK']);
    }
}
