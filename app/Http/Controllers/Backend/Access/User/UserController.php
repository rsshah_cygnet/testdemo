<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Access\User\ChangeUserPasswordRequest;
use App\Http\Requests\Backend\Access\User\CreateUserRequest;
use App\Http\Requests\Backend\Access\User\DeleteUserRequest;
use App\Http\Requests\Backend\Access\User\EditUserRequest;
use App\Http\Requests\Backend\Access\User\MarkUserRequest;
use App\Http\Requests\Backend\Access\User\PermanentlyDeleteUserRequest;
use App\Http\Requests\Backend\Access\User\ResendConfirmationEmailRequest;
use App\Http\Requests\Backend\Access\User\RestoreUserRequest;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserPasswordRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use App\Http\Requests\Backend\Access\User\ViewUserRequest;
use App\Http\Utilities\Jqgrid;
use App\Models\City\City;
use App\Models\Country\Country;
use App\Repositories\Backend\Permission\Group\PermissionGroupRepositoryContract;
use App\Repositories\Backend\Permission\PermissionRepositoryContract;
use App\Repositories\Backend\Role\RoleRepositoryContract;
use App\Repositories\Backend\User\UserContract;
use App\Repositories\Frontend\User\UserContract as FrontendUserContract;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Response;

/**
 * Class UserController
 */
class UserController extends Controller {

	/**
	 * @var columnsexport
	 */
	public $columnsexport = ['id', 'first_name', 'last_name', 'country_name', 'city_name', 'email', 'contact_no'];

	/**
	 * @var downloadfilename
	 */
	public $downloadfilename = 'users_result';

	/**
	 * @var UserContract
	 */
	protected $users;

	/**
	 * @var RoleRepositoryContract
	 */
	protected $roles;

	/**
	 * @var PermissionRepositoryContract
	 */
	protected $permissions;

	/**
	 * UserController constructor.
	 * @param UserContract $users
	 * @param RoleRepositoryContract $roles
	 * @param PermissionRepositoryContract $permissions
	 * @param Jqgrid $jqgrid
	 * @param UserDealership $userDealership

	 */
	public function __construct(UserContract $users, RoleRepositoryContract $roles, PermissionRepositoryContract $permissions, Jqgrid $jqgrid) {
		$this->users = $users;
		$this->roles = $roles;
		$this->permissions = $permissions;
		$this->jqgrid = $jqgrid;

	}

	/**
	 * @return mixed
	 */
	public function index(ViewUserRequest $request) {

		return view('backend.access.index');
		// ->withUsers($this->users->getUsersPaginated(config('access.users.default_per_page'), 1));
	}

	/**
	 * [data description]
	 * @return [type] [description]
	 */
	public function data(Request $request) {

		if ($request->ajax()) {
			$data = $this->jqgrid->Preparedata($request, $this->users, 'ajax');

			$responce = $data[0];
			$records = $data[1];

			$i = 0;

			//make array for json
			foreach ($records as $record) {
				$responce->rows[$i]['id'] = $record->id;
				// roles check
				$roles = '';
				if ($record->roles()->count() > 0) {
					foreach ($record->roles as $role) {
						$roles .= $role->name . '<br/>';
					}
				} else {
					$roles = trans('labels.general.none');
				}

				$responce->rows[$i]['cell'] = array($record->first_name, $record->last_name, $record->country_name, $record->city_name, $record->email, $record->contact_no, $record->status_label, $roles, $record->action_buttons);
				//$responce->rows[$i]['cell']=array($record->name,$record->email,$record->confirmed_label,$roles,$permissions,$record->created_at->diffForHumans(),$record->updated_at->diffForHumans(),'');
				$i++;
			}
			$responce->records = $responce->records - 1; //Total number of user except super admin

			echo json_encode($responce);
		} else {
			// EXPORT GRID START
			$dtype = $request->get('dtype');
			$records = $this->jqgrid->Preparedata($request, $this->users, 'download');

			$data = array();
			$data[0] = array('FirstName', 'LastName', 'Country', 'City', 'Email', 'ContactNo', 'Status', 'Roles');
			//make array for json
			$i = 1;
			foreach ($records as $record) {
				// roles check
				$roles = '';
				if ($record->roles()->count() > 0) {
					foreach ($record->roles as $role) {
						if (!empty($roles)) {
							$roles .= $role->name . ' & ';
						} else {
							$roles .= $role->name;
						}
					}
				} else {
					$roles = trans('labels.general.none');
				}

				$data[$i] = array($record->first_name, $record->last_name, $record->country_name, $record->city_name, $record->email, $record->contact_no, ($record->status == 1 ? 'Yes' : 'No'), $roles);
				$i++;
			}
			// download file
			$this->jqgrid->Download($this->downloadfilename, $data, $dtype);
		}
	}

	/**
	 * @param PermissionGroupRepositoryContract $group
	 * @param CreateUserRequest $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create(PermissionGroupRepositoryContract $group, CreateUserRequest $request) {
		$roles = getChildRolesFromAuthUser(); //This is helper function
		$permissions = $this->permissions->getUngroupedPermissions();
		$groups = $group->getAllGroups(false, 'backend');
		$country = Country::where('status', 1)->pluck('country_name', 'id')->all();
		//$city = City::where('status', 1)->pluck('city_name', 'id')->all();
		$city = [];
		return view('backend.access.create', compact('roles', 'groups', 'permissions', 'country', 'city'));
	}

	/**
	 * @param  StoreUserRequest $request
	 * @return mixed
	 */
	public function store(StoreUserRequest $request) {

		$this->users->create(
			$request->except('assignees_roles', 'permissions', 'ungrouped'), $request->only('assignees_roles'), $request->only('permissions')
		);

		return redirect()->route('admin.access.users.index')->withFlashSuccess(trans('alerts.backend.users.created'));
	}

	/**
	 * @param $id
	 * @param PermissionGroupRepositoryContract $group
	 * @param EditUserRequest $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id, PermissionGroupRepositoryContract $group, EditUserRequest $request) {
		$user = $this->users->findOrThrowException($id, true);
		$country = Country::where('status', 1)->pluck('country_name', 'id')->all();
		$user->country_id = 1;
		$city = getCityBasedOncountry($user->country_id);
		if(!empty($city)){
			foreach ($city as $key => $value) {
				$final_city[$value['id']] = $value['city_name'];
			}
		}
		$city = $final_city;

		
		$selected_country = $user->country_id;
		$selected_city = $user->city_id;

		$user_roles = $user->roles->lists('id')->all();
		$groups = $group->getAllGroups(false, 'backend');
		$permissions = $this->permissions->getUngroupedPermissions();
		$roles = getChildRolesFromAuthUser(); //This is helper function
		$user_permissions = $user->permissions->lists('id')->all();

		return view('backend.access.edit', compact('user', 'groups', 'user_roles', 'user_permissions', 'roles', 'permissions', 'userRole', 'country', 'city','selected_country','selected_city'));
	}

	/**
	 * @param  $id
	 * @param  UpdateUserRequest $request
	 * @return mixed
	 */
	public function update($id, UpdateUserRequest $request) {

		$this->users->update($id, $request->except('assignees_roles', 'permissions', 'ungrouped'), $request->only('assignees_roles'), $request->only('permissions')
		);
		return redirect()->route('admin.access.users.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * @param  $id
	 * @param  DeleteUserRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteUserRequest $request) {
		$this->users->destroy($id);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted'));
	}

	public function destroy1(Request $request) {
		$id = $request->get('id');
		$this->users->destroy($id);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted'));
	}

	/**
	 * @param  $id
	 * @param  PermanentlyDeleteUserRequest $request
	 * @return mixed
	 */
	public function delete($id, PermanentlyDeleteUserRequest $request) {
		$this->users->delete($id);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted_permanently'));
	}

	/**
	 * @param  $id
	 * @param  RestoreUserRequest $request
	 * @return mixed
	 */
	public function restore($id, RestoreUserRequest $request) {
		$this->users->restore($id);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.restored'));
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkUserRequest $request) {
		//dd($status);
		$this->users->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * @return mixed
	 */
	public function deactivated() {
		return view('backend.access.deactivated-index')
			->withUsers($this->users->getUsersPaginated(25, 0));
	}

	/**
	 * @return mixed
	 */
	public function deleted() {
		return view('backend.access.deleted-index')
			->withUsers($this->users->getDeletedUsersPaginated(25));
	}

	/**
	 * @param  $id
	 * @param  ChangeUserPasswordRequest $request
	 * @return mixed
	 */
	public function changePassword($id, ChangeUserPasswordRequest $request) {
		return view('backend.access.change-password')
			->withUser($this->users->findOrThrowException($id));
	}

	/**
	 * @param  $id
	 * @param  UpdateUserPasswordRequest $request
	 * @return mixed
	 */
	public function updatePassword($id, UpdateUserPasswordRequest $request) {
		$this->users->updatePassword($id, $request->all());
		return redirect()->route('admin.access.users.index')->withFlashSuccess(trans('alerts.backend.users.updated_password'));
	}

	/**
	 * @param  $user_id
	 * @param  FrontendUserContract $user
	 * @param  ResendConfirmationEmailRequest $request
	 * @return mixed
	 */
	public function resendConfirmationEmail($user_id, FrontendUserContract $user, ResendConfirmationEmailRequest $request) {
		$user->sendConfirmationEmail($user_id);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.confirmation_email'));
	}

	/**
	 * @return download .xls file
	 */
	public function exportExcel() {
		$result = $this->users->selectAll($this->columnsexport, "id", "desc");
		Excel::create($this->downloadfilename, function ($excel) use ($result) {
			$excel->sheet('Sheet 1', function ($sheet) use ($result) {
				$sheet->fromArray($result);
			});
		})->download('xls');
	}

	/**
	 * @return download .csv file
	 */
	public function exportCSV() {
		$result = $this->users->selectAll($this->columnsexport, "id", "desc");
		Excel::create($this->downloadfilename, function ($excel) use ($result) {
			$excel->sheet('Sheet 1', function ($sheet) use ($result) {
				$sheet->fromArray($result);
			});
		})->export('csv');
	}

	/**
	 * [deleteAll description]
	 * @param  DeleteUsersRequest $request [description]
	 * @return [type]                        [description]
	 */
	public function deleteAll(DeleteUserRequest $request) {
		$ids = $request->get('params');
		foreach ($ids as $id) {
			$this->users->destroy($id);
		}
		return response()->json(['status' => 'OK']);
	}

	/**
	 * This function is used to get permissions details by role id
	 * @param Request $request
	 * @return mixed
	 */
	public function getPermissionByRole(Request $request) {
		$id = $request->get('id');
		$role = $this->roles->findOrThrowException($id, true);
		$data = $role->permissions->lists('id')->all();
		return Response::json($data);
	}

}
