<?php

namespace App\Http\Controllers\Backend\Access\Role;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Access\Role\CreateRoleRequest;
use App\Http\Requests\Backend\Access\Role\DeleteRoleRequest;
use App\Http\Requests\Backend\Access\Role\EditRoleRequest;
use App\Http\Requests\Backend\Access\Role\MarkRoleRequest;
use App\Http\Requests\Backend\Access\Role\StoreRoleRequest;
use App\Http\Requests\Backend\Access\Role\UpdateRoleRequest;
use App\Http\Utilities\Jqgrid;
use App\Repositories\Backend\Permission\Group\PermissionGroupRepositoryContract;
use App\Repositories\Backend\Permission\PermissionRepositoryContract;
use App\Repositories\Backend\Role\RoleRepositoryContract;
use Illuminate\Http\Request;

/**
 * Class RoleController
 * @package App\Http\Controllers\Access
 */
class RoleController extends Controller {

	/**
	 * @var columnsexport
	 */
	public $columnsexport = ['id', 'name', 'status', 'created_at', 'updated_at'];

	/**
	 * @var downloadfilename
	 */
	public $downloadfilename = 'roles_result';

	/**
	 * @var RoleRepositoryContract
	 */
	protected $roles;

	/**
	 * @var PermissionRepositoryContract
	 */
	protected $permissions;

	/**
	 * @param RoleRepositoryContract       $roles
	 * @param PermissionRepositoryContract $permissions
	 */
	public function __construct(
		RoleRepositoryContract $roles, PermissionRepositoryContract $permissions, Jqgrid $jqgrid
	) {
		$this->roles = $roles;
		$this->permissions = $permissions;
		$this->jqgrid = $jqgrid;
	}

	/**
	 * @return mixed
	 */
	public function index() {
		return view('backend.access.roles.index');
		// ->withRoles($this->roles->getRolesPaginated(50));
	}

	/**
	 * [data description]
	 * @return [type] [description]
	 */
	public function data(Request $request) {
		if ($request->ajax()) {
			$data = $this->jqgrid->Preparedata($request, $this->roles, 'ajax');

			$responce = $data[0];
			$records = $data[1];

			$i = 0;
			//make array for json
			foreach ($records as $record) {
				$responce->rows[$i]['id'] = $record->id;
				$responce->rows[$i]['cell'] = array($record->name, $record->status, $record->users()->count(), $record->action_buttons);
				//$responce->rows[$i]['cell']=array($record->name,$record->email,$record->confirmed_label,$roles,$permissions,$record->created_at->diffForHumans(),$record->updated_at->diffForHumans(),'');
				$i++;
			}
			echo json_encode($responce);
		} else {
			// EXPORT GRID START
			$dtype = $request->get('dtype');
			$records = $this->jqgrid->Preparedata($request, $this->roles, 'download');

			$data = array();
			$data[0] = array(trans('labels.backend.access.roles.table.role'),
				trans('labels.backend.access.roles.table.number_of_users'),
				trans('labels.backend.access.roles.table.sort'));
			//make array for json
			$i = 1;
			foreach ($records as $record) {

				$data[$i] = array($record->name, $record->status, $record->users()->count(), $record->sort);
				$i++;
			}

			// download file
			$this->jqgrid->Download($this->downloadfilename, $data, $dtype);
		}
	}

	/**
	 * @param  PermissionGroupRepositoryContract $group
	 * @param  CreateRoleRequest                 $request
	 * @return mixed
	 */
	public function create(PermissionGroupRepositoryContract $group, CreateRoleRequest $request) {
		return view('backend.access.roles.create')
			->withGroups($group->getAllGroups())
			->withPermissions($this->permissions->getUngroupedPermissions());
	}

	/**
	 * @param  StoreRoleRequest $request
	 * @return mixed
	 */
	public function store(StoreRoleRequest $request) {
		$this->roles->create($request->all());
		return redirect()->route('admin.access.roles.index')->withFlashSuccess(trans('alerts.backend.roles.created'));
	}

	/**
	 * @param  $id
	 * @param  PermissionGroupRepositoryContract $group
	 * @param  EditRoleRequest                   $request
	 * @return mixed
	 */
	public function edit($id, PermissionGroupRepositoryContract $group, EditRoleRequest $request) {
		$role = $this->roles->findOrThrowException($id, true);
		return view('backend.access.roles.edit')
			->withRole($role)
			->withRolePermissions($role->permissions->lists('id')->all())
			->withGroups($group->getAllGroups())
			->withPermissions($this->permissions->getUngroupedPermissions());
	}

	/**
	 * @param  $id
	 * @param  UpdateRoleRequest $request
	 * @return mixed
	 */
	public function update($id, UpdateRoleRequest $request) {
		$this->roles->update($id, $request->all());
		return redirect()->route('admin.access.roles.index')->withFlashSuccess(trans('alerts.backend.roles.updated'));
	}

	/**
	 * @param  $id
	 * @param  DeleteRoleRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRoleRequest $request) {
		$this->roles->destroy($id);
		return response()->json(['status' => 'OK']);
		// return redirect()->route('admin.access.roles.index')->withFlashSuccess(trans('alerts.backend.roles.deleted'));
	}

	/**
	 * [deleteAll description]
	 * @param  DeleteRolesRequest $request [description]
	 * @return [type]                        [description]
	 */
	public function deleteAll(DeleteRoleRequest $request) {
		$ids = $request->get('params');
		foreach ($ids as $id) {
			$this->roles->destroy($id);
		}
		return response()->json(['status' => 'OK']);
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkRoleRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkRoleRequest $request) {
		$this->roles->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}
}
