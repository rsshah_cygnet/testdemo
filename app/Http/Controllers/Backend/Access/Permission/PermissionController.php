<?php

namespace App\Http\Controllers\Backend\Access\Permission;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Access\Permission\CreatePermissionRequest;
use App\Http\Requests\Backend\Access\Permission\DeletePermissionRequest;
use App\Http\Requests\Backend\Access\Permission\EditPermissionRequest;
use App\Http\Requests\Backend\Access\Permission\StorePermissionRequest;
use App\Http\Requests\Backend\Access\Permission\UpdatePermissionRequest;
use App\Http\Utilities\Jqgrid;
use App\Repositories\Backend\Permission\Group\PermissionGroupRepositoryContract;
use App\Repositories\Backend\Permission\PermissionRepositoryContract;
use App\Repositories\Backend\Role\RoleRepositoryContract;
use Illuminate\Http\Request;

/**
 * Class PermissionController
 * @package App\Http\Controllers\Access
 */
class PermissionController extends Controller {

	/**
	 * @var RoleRepositoryContract
	 */
	protected $roles;

	/**
	 * @var PermissionRepositoryContract
	 */
	protected $permissions;

	/**
	 * @var PermissionGroupRepositoryContract
	 */
	protected $groups;

	/**
	 * @param RoleRepositoryContract            $roles
	 * @param PermissionRepositoryContract      $permissions
	 * @param PermissionGroupRepositoryContract $groups
	 */
	public function __construct(
		RoleRepositoryContract $roles, PermissionRepositoryContract $permissions, PermissionGroupRepositoryContract $groups, Jqgrid $jqgrid
	) {
		$this->roles = $roles;
		$this->permissions = $permissions;
		$this->groups = $groups;
		$this->jqgrid = $jqgrid;
	}

	/**
	 * @return mixed
	 */
//    public function index()
	//    {
	//        return view('backend.access.roles.permissions.index')
	//            ->withPermissions($this->permissions->getPermissionsPaginated(50))
	//            ->withGroups($this->groups->getAllGroups());
	//    }

	public function index() {
		return view('backend.access.roles.permissions.index')
			->withGroups($this->groups->getAllGroups());
	}

	/**
	 * data
	 *
	 * @param Request $request
	 */
	public function data(Request $request) {
		if ($request->ajax()) {
			$data = $this->jqgrid->Preparedata($request, $this->permissions, 'ajax');

			$responce = $data[0];
			$records = $data[1];

			$i = 0;
			//make array for json
			foreach ($records as $record) {
				$responce->rows[$i]['id'] = $record->id;
				$responce->rows[$i]['cell'] = array(
					$record->permission,
					$record->name,
					$record->roles,
					$record->action_buttons,
				);
				$i++;
			}

			echo json_encode($responce);
		} else {
			// EXPORT GRID START
			$dtype = $request->get('dtype');
			$records = $this->jqgrid->Preparedata($request, $this->repositorycontract, 'download');

			$data = array();
			$data[0] = array(trans('labels.backend.cmspages.table.title'),
				trans('labels.backend.cmspages.table.created'),
				trans('labels.backend.cmspages.table.last_updated'));
			//make array for json
			$i = 1;
			foreach ($records as $record) {

				$data[$i] = array($record->title, $record->created_at->diffForHumans(), $record->updated_at->diffForHumans());
				$i++;
			}

			// download file
			$this->jqgrid->Download($this->downloadfilename, $data, $dtype);
		}
	}

	/**
	 * @param  CreatePermissionRequest $request
	 * @return mixed
	 */
	public function create(CreatePermissionRequest $request) {
		return view('backend.access.roles.permissions.create')
			->withGroups($this->groups->getAllGroups())
			->withRoles($this->roles->getAllRoles())
			->withPermissions($this->permissions->getUngroupedPermissions());
	}

	/**
	 * @param  StorePermissionRequest $request
	 * @return mixed
	 */
	public function store(StorePermissionRequest $request) {
		$this->permissions->create($request->except('permission_roles'), $request->only('permission_roles'));
		return redirect()->route('admin.access.roles.permissions.index')->withFlashSuccess(trans('alerts.backend.permissions.created'));
	}

	/**
	 * @param  $id
	 * @param  EditPermissionRequest $request
	 * @return mixed
	 */
	public function edit($id, EditPermissionRequest $request) {
		$permission = $this->permissions->findOrThrowException($id, true);
		//$p = $permission->dependencies->lists('dependency_id')->all();
		//echo "<pre>"; print_r($p); die;
		return view('backend.access.roles.permissions.edit')
			->withPermission($permission)
			->withPermissionRoles($permission->roles->lists('id')->all())
			->withGroups($this->groups->getAllGroups())
			->withRoles($this->roles->getAllRoles())
			->withPermissions($this->permissions->getUngroupedPermissions())
			->withPermissionDependencies($permission->dependencies->lists('dependency_id')->all());
	}

	/**
	 * @param  $id
	 * @param  UpdatePermissionRequest $request
	 * @return mixed
	 */
	public function update($id, UpdatePermissionRequest $request) {
		$this->permissions->update($id, $request->except('permission_roles'), $request->only('permission_roles'));
		return redirect()->route('admin.access.roles.permissions.index')->withFlashSuccess(trans('alerts.backend.permissions.updated'));
	}

	/**
	 * @param  $id
	 * @param  DeletePermissionRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeletePermissionRequest $request) {
		$this->permissions->destroy($id);
		return response()->json(['status' => "OK"]);
		/*return redirect()->route('admin.access.roles.permissions.index')->withFlashSuccess(trans('alerts.backend.permissions.deleted'));*/
	}

}
