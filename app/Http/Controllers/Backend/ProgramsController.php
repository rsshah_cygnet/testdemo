<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Programs\ProgramsRepositoryContract;
use App\Http\Requests\Backend\Programs\CreateRequest;
use App\Http\Requests\Backend\Programs\EditRequest;
use App\Http\Requests\Backend\Programs\StoreRequest;
use App\Http\Requests\Backend\Programs\ViewRequest;
use App\Http\Requests\Backend\Programs\UpdateRequest;
use App\Http\Requests\Backend\Programs\DeleteRequest;
use App\Http\Requests\Backend\Programs\MarkLevelsRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Library\GridMaster\GridMaster;
use App\Models\Program\Program;
use App\Models\Curriculum\Curriculum;
use App\Models\EducationalSystem\EducationalSystem;

class ProgramsController extends Controller
{
     /**
     * Repository Object
     *
     * @var object
     */
    public $repository;
    

    /**
     * __construct
     *
     * @param BlogRepositoryContract $repository
     */
    function __construct(ProgramsRepositoryContract $repository)
    {

        $this->repository = $repository;
        
    }

    /**
     * Listing
     *
     * @return mixed
     */
    public function index(ViewRequest $request)
    {

        return view('backend.programs.index')->with(['repository' => $this->repository]);
    }

    /**
     * show data in grid
     * @param Request $request
     */
    public function data(ViewRequest $request)
    {
        $gridMaster = new GridMaster;

        if ($request->ajax())
        {
            return $gridMaster->setGridColumns($this->repository->gridColumn)
                            ->setRepository($this->repository)
                            ->getGridData($request, $this->repository, 'ajax');
        } else
        {
            $gridMaster->setGridColumns($this->repository->gridColumn)
                    ->setRepository($this->repository)
                    ->downloadGridData($request, $this->repository, 'download');
        }
    }

   
    /**
     * @param  $id
     * @param  $status
     * @param  MarkUserRequest $request
     * @return mixed
     */
    public function mark($id, $status, MarkLevelsRequest $request)
    {
        $this->repository->mark($id, $status);
        return response()->json(['status' => 'OK']);
        // return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    
    /**
     * Delete Programs
     *
     * @param int $id
     * @param DeleteRequest $request
     * @return mixed
     */
    public function destroy($id, DeleteRequest $request)
    {
        $item = $this->repository->findOrThrowException($id);
        if ($item->id)
        {
            $this->repository->destroy($id);
            return response()->json(['status' => "OK"]);
        }
    }


     /**
    * Create Programs
    *
    * @param CreateRequest $request
    * @return \Illuminate\View\View
    */
    public function create(CreateRequest $request)
    {

        $curriculums = Curriculum::getCurriculumList('display_in_program');
        $educationalsystems = EducationalSystem::getEducatinalSystem();
        // dd($curriculums);
        /*$tags       = BlogTag::getSelectData();
        $domains    = Domain::getSelectData('domain');*/


        //Returning to view with a form-type
        return view('backend.programs.create')
            ->withCurriculums($curriculums)
            ->withEducationalsystems($educationalsystems)
            ->with('formtype','create');

    }


    /**
    * Store Programs
    *
    * @param StoreRequest $request
    * @return mixed
    */
    public function store(StoreRequest $request)
    {
       
        $postData = $request->all();
        
        $model = $this->repository->create($postData);

        return redirect()->route('admin.programs.index')->withFlashSuccess('Program has been successfully created');
       
    }


    public function edit($id, EditRequest $request)
    {

        //Finding the item
        $programs = $this->repository->findOrThrowException($id);

        $curriculums = Curriculum::getCurriculumList('display_in_level');
        $educationalsystems = EducationalSystem::getEducatinalSystem();

       
        $selectedCurriculam = $programs->curriculum_id;
        $selectedEducation = $programs->eductional_systems_id;
       
        //Returning the view with form-type edit
        return view('backend.programs.edit')
                    ->withCurriculums($curriculums)
                    ->withEducationalsystems($educationalsystems)
                    ->withSelectedCurriculam($selectedCurriculam)
                    ->withSelectedEducation($selectedEducation)
                    ->withPrograms($programs)
                    ->with('formtype','edit');

    }
}
