<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Frontusers\DeleteRequest;
use App\Http\Requests\Backend\Frontusers\MarkUserRequest;
use App\Http\Requests\Backend\Frontusers\ReminderRequest;
use App\Http\Requests\Backend\Frontusers\ViewRequest;
use App\Http\Requests\Backend\UserTestResult\MarkUserTestResultRequest;
use App\Library\GridMaster\GridMaster;
use App\Models\Frontusers\Frontusers;
use App\Models\TutorAnswer\TutorAnswer;
use App\Models\UserTestResult\UserTestResult;
use App\Repositories\Backend\Frontusers\FrontusersRepositoryContract;
use App\Repositories\Backend\UserTestResult\UserTestResultContract;
use Illuminate\Support\Facades\Mail;

/**
 * Class FrontusersController
 *
 * @author Sudhir Virpara
 */
class FrontusersController extends Controller {

	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;
	public $testResultRepository;

	/**
	 * __construct
	 *
	 * @param BlogRepositoryContract $repository
	 */
	function __construct(FrontusersRepositoryContract $repository, UserTestResultContract $testResultRepository) {
		$this->repository = $repository;
		$this->testResultRepository = $testResultRepository;
	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function index(ViewRequest $request) {

		return view('backend.Frontusers.index')->with(['repository' => $this->repository]);
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(ViewRequest $request) {
		$gridMaster = new GridMaster;

		if ($request->ajax()) {

			return $gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->getGridData($request, $this->repository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->downloadGridData($request, $this->repository, 'download');
		}
	}

	/**
	 * show data in grid for TestResult
	 * @param Request $request
	 */
	public function testResultData(ViewRequest $request) {

		$gridMaster = new GridMaster;
		$this->testResultRepository->frontUserId = $request->id;

		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->testResultRepository->gridColumn)
				->setRepository($this->testResultRepository)
				->getGridData($request, $this->testResultRepository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->testResultRepository->gridColumn)
				->setRepository($this->testResultRepository)
				->downloadGridData($request, $this->testResultRepository, 'download');
		}
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkUserRequest $request) {
		$this->repository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function markTestResult($id, $status, MarkUserTestResultRequest $request) {
		$data = $this->testResultRepository->findOrThrowException($id);
		$this->testResultRepository->sendTestResultEmail($data->front_user_id);
		//return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.confirmation_email'));

		$this->testResultRepository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * @param  $id
	 */
	public function testResult($id) {

		return view('backend.userTestResult.index')->with(['repository' => $this->testResultRepository, 'id' => $id]);
	}

	/**
	 * Delete Blog Model
	 *
	 * @param int $id
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRequest $request) {
		$item = $this->repository->findOrThrowException($id);
		if ($item->id) {
			$this->repository->destroy($id);
			return response()->json(['status' => "OK"]);
		}
	}

	public function viewResult($id) {
		$data = UserTestResult::where('id', $id)->first();

		$item = TutorAnswer::with('Que')->where('tutor_detail_id', $id)->get()->all();

		return view('backend.Frontusers.questionlist')->with(["item" => $item, "data" => $data]);
	}

	public function sendReminder(ReminderRequest $request) {
		$post = $request->all();
		if (isset($post['user_id']) && $post['user_id'] == 0) {
			$users = $this->repository->findAllUsers();
			//echo "<pre>";print_r($users);exit;
			if (!empty($users)) {
				foreach ($users as $user) {
					Mail::raw('This is a test reminder mail by admin', function ($message) use ($user) {
						$message->to($user->email, $user->first_name)->subject(app_name() . ': ' . "Gentle reminder");
					});
				}
			}
			return response()->json(['status' => 'OK']);
		} else {
			$user = $this->repository->findOrThrowException($post['user_id']);
			Mail::raw('This is a test reminder mail by admin', function ($message) use ($user) {
				$message->to($user->email, $user->first_name)->subject(app_name() . ': ' . "Gentle reminder");
			});
			return response()->json(['status' => 'OK']);

		}
		/*Mail::send('Test', function ($message) use ($user) {
			                    $message->to('srvirpara@cygnet-infotech.com', $user->first_name)->subject(app_name() . ': ' . "Your result has been changed");
		*/

		//$this->repository->sendReminderEmail($user);
		//return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.confirmation_email'));

	}

}
