<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\SessionSuccess\SessionSuccessRepositoryContract;
use App\Repositories\Backend\SessionUpcoming\SessionUpcomingRepositoryContract;
use App\Repositories\Backend\SessionCancelledByTutor\SessionCancelledByTutorRepositoryContract;
use App\Repositories\Backend\SessionCancelledByTutee\SessionCancelledByTuteeRepositoryContract;
use App\Http\Requests\Backend\Session\ViewRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Library\GridMaster\GridMaster;
use App\Models\Session\Session;

/**
 * Class FrontusersController
 *
 * @author Sudhir Virpara
 */
class SessionController extends Controller
{

    /**
     * Repository Object
     *
     * @var object
     */
    public $repository;
    public $upcoming_repository;
    public $cancelled_tutor_repository;
    public $cancelled_tutee_repository;
    /**
     * __construct
     *
     * @param BlogRepositoryContract $repository
     */
    function __construct(SessionSuccessRepositoryContract $repository,SessionUpcomingRepositoryContract $upcoming_repository,SessionCancelledByTutorRepositoryContract $cancelled_tutor_repository,SessionCancelledByTuteeRepositoryContract $cancelled_tutee_repository)
    {

        $this->repository = $repository;
        $this->upcoming_repository = $upcoming_repository;
        $this->cancelled_tutor_repository = $cancelled_tutor_repository;
        $this->cancelled_tutee_repository = $cancelled_tutee_repository;
    }

    /**
     * Listing
     *
     * @return mixed
     */
    public function index(ViewRequest $request)
    {

        return view('backend.Session.index')->with(['repository' => $this->repository]);
    }


    public function upcoming(ViewRequest $request)
    {

        return view('backend.Session.upcoming')->with(['repository' => $this->upcoming_repository]);
    }

    public function cancelled_tutor(ViewRequest $request)
    {

        return view('backend.Session.tutor')->with(['repository' => $this->cancelled_tutor_repository]);
    }


    public function cancelled_tutee(ViewRequest $request)
    {

        return view('backend.Session.tutee')->with(['repository' => $this->cancelled_tutee_repository]);
    }

    /**
     * show data in grid
     * @param Request $request
     */
    public function data(ViewRequest $request)
    {  
        $gridMaster = new GridMaster;

        if ($request->ajax())
        {
            return $gridMaster->setGridColumns($this->repository->gridColumn)
                            ->setRepository($this->repository)
                            ->getGridData($request, $this->repository, 'ajax');
        } else
        {
            $gridMaster->setGridColumns($this->repository->gridColumn)
                    ->setRepository($this->repository)
                    ->downloadGridData($request, $this->repository, 'download');
        }
    }


     /**
     * show data in grid
     * @param Request $request
     */
    public function data_upcoming(ViewRequest $request)
    {
        $gridMaster = new GridMaster;

        if ($request->ajax())
        {
            return $gridMaster->setGridColumns($this->upcoming_repository->gridColumn)
                            ->setRepository($this->upcoming_repository)
                            ->getGridData($request, $this->upcoming_repository, 'ajax');
        } else
        {
            $gridMaster->setGridColumns($this->upcoming_repository->gridColumn)
                    ->setRepository($this->upcoming_repository)
                    ->downloadGridData($request, $this->upcoming_repository, 'download');
        }
    }


     public function data_cancelled_tutor(ViewRequest $request)
    {
        $gridMaster = new GridMaster;

        if ($request->ajax())
        {
            return $gridMaster->setGridColumns($this->cancelled_tutor_repository->gridColumn)
                            ->setRepository($this->cancelled_tutor_repository)
                            ->getGridData($request, $this->cancelled_tutor_repository, 'ajax');
        } else
        {
            $gridMaster->setGridColumns($this->cancelled_tutor_repository->gridColumn)
                    ->setRepository($this->cancelled_tutor_repository)
                    ->downloadGridData($request, $this->cancelled_tutor_repository, 'download');
        }
    }

    public function data_cancelled_tutee(ViewRequest $request)
    { //    dd($request->all());
        $gridMaster = new GridMaster;

        if ($request->ajax())
        {
            return $gridMaster->setGridColumns($this->cancelled_tutee_repository->gridColumn)
                            ->setRepository($this->cancelled_tutee_repository)
                            ->getGridData($request, $this->cancelled_tutee_repository, 'ajax');
        } else
        {
            $gridMaster->setGridColumns($this->cancelled_tutee_repository->gridColumn)
                    ->setRepository($this->cancelled_tutee_repository)
                    ->downloadGridData($request, $this->cancelled_tutee_repository, 'download');
        }
    }


}
