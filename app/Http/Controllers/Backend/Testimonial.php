<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Testimonial\DeleteRequest;
use App\Http\Requests\Backend\Testimonial\MarkUserRequest;
use App\Http\Requests\Backend\Testimonial\PublishRequest;

use App\Http\Requests\Backend\Testimonial\ViewRequest;
use App\Library\GridMaster\GridMaster;
use App\Repositories\Backend\Testimonial\TestimonialRepositoryContract;

class Testimonial extends Controller {
	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;

	/**
	 * __construct
	 *
	 * @param TestimonialRepositoryContract $repository
	 */
	function __construct(TestimonialRepositoryContract $repository) {
		$this->repository = $repository;
	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function index(ViewRequest $request) {
		return view('backend.Testimonial.index')->with(['repository' => $this->repository]);
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(ViewRequest $request) {
		$gridMaster = new GridMaster;
		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->getGridData($request, $this->repository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->downloadGridData($request, $this->repository, 'download');
		}
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkUserRequest $request) {
		$this->repository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}


	public function publish($id, $publish, PublishRequest $request) {
		$this->repository->publish($id, $publish);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * Delete Blog Model
	 *
	 * @param int $id
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRequest $request) {
		$item = $this->repository->findOrThrowException($id);
		if ($item->id) {
			$this->repository->destroy($id);
			return response()->json(['status' => "OK"]);
		}
	}
}
