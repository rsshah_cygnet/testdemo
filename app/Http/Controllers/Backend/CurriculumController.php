<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Curriculum\CreateRequest;
use App\Http\Requests\Backend\Curriculum\DeleteRequest;
use App\Http\Requests\Backend\Curriculum\EditRequest;
use App\Http\Requests\Backend\Curriculum\MarkUserRequest;
use App\Http\Requests\Backend\Curriculum\StoreRequest;
// use App\Http\Requests\Backend\Frontusers\MarkUserRequest;
// use App\Http\Requests\Backend\UserTestResult\MarkUserTestResultRequest;
use App\Http\Requests\Backend\Curriculum\UpdateRequest;
use App\Http\Requests\Backend\Curriculum\ViewRequest;
use App\Library\GridMaster\GridMaster;
use App\Models\Curriculum\Curriculum;
use App\Repositories\Backend\Curriculum\CurriculumRepositoryContract;

class CurriculumController extends Controller {
	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;

	/**
	 * __construct
	 *
	 * @param CurriculumRepositoryContract $repository
	 */
	function __construct(CurriculumRepositoryContract $repository) {
		$this->repository = $repository;
	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function index(ViewRequest $request) {
		return view('backend.Curriculum.index')->with(['repository' => $this->repository]);
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(ViewRequest $request) {
		$gridMaster = new GridMaster;

		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->getGridData($request, $this->repository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->downloadGridData($request, $this->repository, 'download');
		}
	}

	/**
	 * show data in grid for TestResult
	 * @param Request $request
	 */
	public function testResultData(ViewRequest $request) {

		$gridMaster = new GridMaster;
		$this->testResultRepository->frontUserId = $request->id;

		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->testResultRepository->gridColumn)
				->setRepository($this->testResultRepository)
				->getGridData($request, $this->testResultRepository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->testResultRepository->gridColumn)
				->setRepository($this->testResultRepository)
				->downloadGridData($request, $this->testResultRepository, 'download');
		}
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function mark($id, $status, MarkUserRequest $request) {
		$this->repository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * @param  $id
	 * @param  $status
	 * @param  MarkUserRequest $request
	 * @return mixed
	 */
	public function markTestResult($id, $status, MarkUserTestResultRequest $request) {
		$data = $this->testResultRepository->findOrThrowException($id);
		$this->testResultRepository->sendTestResultEmail($data->front_user_id);
		//return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.confirmation_email'));

		$this->testResultRepository->mark($id, $status);
		return response()->json(['status' => 'OK']);
		// return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.updated'));
	}

	/**
	 * @param  $id
	 */
	public function testResult($id) {

		return view('backend.userTestResult.index')->with(['repository' => $this->testResultRepository, 'id' => $id]);
	}

	/**
	 * Delete Blog Model
	 *
	 * @param int $id
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRequest $request) {
		$item = $this->repository->findOrThrowException($id);
		if ($item->id) {
			$this->repository->destroy($id);
			return response()->json(['status' => "OK"]);
		}
	}
	/**
	 * show curriculum form view
	 * @param CreateRequest $request
	 * @return type
	 */
	public function create(CreateRequest $request) {
		return view('backend.Curriculum.create')->with('formtype', 'create');
	}

	/**
	 * store data in database
	 * @param StoreCmsRequest $request
	 * @return type
	 */
	public function store(StoreRequest $request) {
		$this->repository->create($request);
		// return response()->json((object) [
		// 	'status' => true,
		// 	'message' => 'Curriculum added Successfully',
		// ], 200);
		return redirect()->route('admin.curriculum.index')->withFlashSuccess('Curriculum has been created sucessfully !');
	}
	/**
	 * show edit form view with data
	 * @param type $id
	 * @param EditCurriculumRequest $request
	 * @return type
	 */
	public function edit($id, EditRequest $request) {
		$curriculum = $this->repository->findOrThrowException($id);
		return view('backend.Curriculum.edit', compact('curriculum', $curriculum))->with('formtype', 'create');
		// return view('backend.cmspages.edit' , compact('cmsPage'));
	}

	/**
	 * update data into database
	 * @param type $id
	 * @param UpdateCmsRequest $request
	 * @return type
	 */
	public function update($id, UpdateRequest $request) {
		$curriculum = $this->repository->findOrThrowException($id);
		$this->repository->update($id, $request);
		return redirect()->route('admin.curriculum.index')->withFlashSuccess('Curriculum has been updated sucessfully !');
	}

	public function getLinks() {
		return view('backend.Curriculum.management');
	}
}
