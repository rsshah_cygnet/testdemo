<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Blog\BlogRepositoryContract;
use App\Http\Requests\Backend\Blog\CreateRequest;
use App\Http\Requests\Backend\Blog\EditRequest;
use App\Http\Requests\Backend\Blog\StoreRequest;
use App\Http\Requests\Backend\Blog\ViewRequest;
use App\Http\Requests\Backend\Blog\UpdateRequest;
use App\Http\Requests\Backend\Blog\DeleteRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Library\GridMaster\GridMaster;
use App\Models\BlogCategoryModel\BlogCategoryModel;
use App\Models\BlogTag\BlogTag;
use App\Models\Domain\Domain;
use App\Models\Blog\Blog;
use Carbon\Carbon;
use Image;

/**
 * Class AdminBlogController
 *
 * @author Viral Solani
 */

class AdminBlogController extends Controller
{
    /**
     * Repository Object
     *
     * @var object
     */
    public $repository;

    /**
     * __construct
     *
     * @param BlogRepositoryContract $repository
     */
    function __construct(BlogRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Listing
     *
     * @return mixed
     */
    public function index(ViewRequest $request)
    {
        return view('backend.blog.index')->with(['repository' => $this->repository]);
    }

    /**
     * show data in grid
     * @param Request $request
     */
    public function data(ViewRequest $request)
    {
        $gridMaster = new GridMaster;

        if ($request->ajax())
        {
            return $gridMaster->setGridColumns($this->repository->gridColumn)
                ->setRepository($this->repository)
                ->getGridData($request, $this->repository, 'ajax');
        }
        else
        {
            $gridMaster->setGridColumns($this->repository->gridColumn)
                ->setRepository($this->repository)
                ->downloadGridData($request, $this->repository, 'download');
        }
    }

    /**
    * Create Blog Model
    *
    * @param CreateRequest $request
    * @return \Illuminate\View\View
    */
    public function create(CreateRequest $request)
    {
        //Getting categories
        $categories = BlogCategoryModel::getSelectData();

        //Getting tags 
        $tags       = BlogTag::getSelectData();

        //Getting domains
        $domains    = Domain::getSelectData('domain');


        //Returning to view with a form-type
        return view('backend.blog.create')
            ->withCategories($categories)
            ->withTags($tags)
            ->withDomains($domains)
            ->withStatus($this->repository->record_status)
            ->with('formtype','create');

    }


    /**
    * Store Blog Model
    *
    * @param StoreRequest $request
    * @return mixed
    */
    public function store(StoreRequest $request)
    {

        // dd($request->all());
        $categories = array($request->categories);

        $postData = $request->all();
        
        //Changing the format of publish_datetime field to store it in database
        $postData['publish_datetime'] = Blog::getStorePublishDatetimeAttribute($request->publish_datetime);

        // $postData['slug'] = str_replace('/','',$postData['slug']);
        $postData['slug'] = ltrim($postData['slug'],'/');

        //creating tags Array
        $tags = $this->createTagsArray($postData['tags']);         

        //Storing the request data into database
        $model = $this->repository->create($postData);

        $this->syncCategoriesAndTags($model,$tags,$categories);

        return redirect()->route('admin.blog.index')->withFlashSuccess('Blog has been successfully created');
       
    }

    public function preview($cat,$slug)
    {
        $link = $cat."/".$slug;
        $post = Blog::where('slug','LIKE','%' . $link . '%')->get();
        $categories = BlogCategoryModel::all();
        $this->getDiffForHumans($post);
        return view('frontend.blog.blog')
            ->withCategories($categories)
            ->withPosts($post)
            ->withDefaultPost($post->first());
    }

    public function getDiffForHumans($posts)
    {
        foreach ($posts as $post) {
            $post['publish_datetime'] = Carbon::parse($post['publish_datetime'])->diffForHumans();
        }

    }

    /**
     * Edit Blog Model
     *
     * @param int $id
     * @param EditRequest $request
     * @return $this
     */
    public function edit($id, EditRequest $request)
    {

        //Finding the item
        $item = $this->repository->findOrThrowException($id);

        //Getting categories, tags and domain
        $categories = BlogCategoryModel::getSelectData();

        $tags       = BlogTag::getSelectData();
        
        $domains    = Domain::getSelectData('domain');

        //Chaning the format of date
        $item->publish_datetime = Carbon::parse($item->publish_datetime)->format('d/m/Y h:i a');
        
        $item->slug = '/'.$item->slug;

        //getting the blog specific category,tags and status
        $selectedCategory = $item->categories[0]->id;

        $selectedStatus = array_search($item->status,$this->repository->record_status);

        $selectedTags = $item->tags()->get()->pluck('id')->toArray();

        //Returning the view with form-type edit
        return view('backend.blog.edit')
                    ->with(["item" => $item])
                    ->withTags($tags)
                    ->withDomains($domains)
                    ->withCategories($categories)
                    ->withStatus($this->repository->record_status)
                    ->with('selectedCategory',$selectedCategory)
                    ->with('selectedStatus',$selectedStatus)
                    ->with('selectedTags',$selectedTags)
                    ->with('formtype','edit');

    }

    /**
     * Update Blog Model
     *
     * @param int $id
     * @param UpdateRequest $request
     * @return $this
     */
    public function update($id, UpdateRequest $request)
    {
        //Creating a array of categories as getSelectData() function requires an array
        $categories = array($request->categories);

        //Storing the request data into a variable, so as to make changes to it
        $postData =$request->all();

        //Changing the format of publish_datetime field to store it in database
        $postData['publish_datetime'] = Blog::getStorePublishDatetimeAttribute($request->publish_datetime);

        $postData['slug'] = ltrim($postData['slug'],'/');

        $tags = $this->createTagsArray($postData['tags']); 

        $model = $this->repository->update($id, $postData);

        $this->syncCategoriesAndTags(Blog::find($id),$tags,$categories);

        if($model)
        {
            return redirect()->route('admin.blog.index')->withFlashSuccess('Blog has been edited successfully');   
        }
        
        return redirect()->back()->withFlashWarning('Invalid Blog');
    }

    /**
     * Delete Blog Model
     *
     * @param int $id
     * @param DeleteRequest $request
     * @return mixed
     */
    public function destroy($id, DeleteRequest $request)
    {
        $item = $this->repository->findOrThrowException($id);

        if($item->id)
        {
            $this->repository->destroy($id);
            return response()->json(['status' => "OK"]);
        }
    }

    /**
     * Creating Tags Array
     *
     * @param Array($tags)
     * @return Array
     */
    public function createTagsArray($tags)
    {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach($tags as $tag)
        {
            if(is_numeric($tag))
            {
                $tags_array[] = $tag;
            }
            else
            {
                $newTag = BlogTag::create(['name'=>$tag,'status'=>'Active','created_by'=>1]);

                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Delete Blog Model
     *
     * @param Blog
     * @param Array($tags)
     * @param Array($categories)
     */
    public function syncCategoriesAndTags(Blog $model,$tags,$categories)
    {
        //Syncing the category with the blog
        $model->categories()->sync($categories);

        //Syncing the tags with the blog
        $model->tags()->sync($tags);
    }
}
