<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\EducationalSystem\CreateRequest;
use App\Http\Requests\Backend\EducationalSystem\DeleteRequest;
use App\Http\Requests\Backend\EducationalSystem\EditRequest;
use App\Http\Requests\Backend\EducationalSystem\StoreRequest;
use App\Http\Requests\Backend\EducationalSystem\UpdateRequest;
use App\Http\Requests\Backend\EducationalSystem\ViewRequest;
use App\Library\GridMaster\GridMaster;
use App\Models\Curriculum\Curriculum;
use App\Models\EducationalSystem\EducationalSystem;
use App\Repositories\Backend\EducationalSystem\EducationalSystemContract;
use Illuminate\Http\Request;

class EducationalSystemController extends Controller {
	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;

	/**
	 * __construct
	 *
	 * @param EducationalSystemContract $repository
	 */
	function __construct(EducationalSystemContract $repository) {
		$this->repository = $repository;
	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function index(ViewRequest $request) {
		return view('backend.educationalsystem.index')->with(['repository' => $this->repository]);
	}

	/**
	 * show data in grid
	 * @param Request $request
	 */
	public function data(ViewRequest $request) {
		$gridMaster = new GridMaster;

		if ($request->ajax()) {
			return $gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->getGridData($request, $this->repository, 'ajax');
		} else {
			$gridMaster->setGridColumns($this->repository->gridColumn)
				->setRepository($this->repository)
				->downloadGridData($request, $this->repository, 'download');
		}
	}

	/**
	 * Create EducationalSystem
	 *
	 * @param CreateRequest $request
	 * @return \Illuminate\View\View
	 */
	public function create(CreateRequest $request) {
		//$curriculumData = Curriculum::getCurriculumList('display_in_level');

		//$educationalsystems = EducationalSystem::getEducatinalSystem();
		//dd($educationalsystems);
		//$curriculum = Curriculum::getSelectData('curriculum_name', true, 'status', '1', null);
		$curriculum = Curriculum::getCurriculumList('display_in_eductional_systems');
		return view('backend.educationalsystem.create')
			->with(['curriculum' => $curriculum]);
	}

	/**
	 * Store EducationalSystem
	 *
	 * @param StoreRequest $request
	 * @return mixed
	 */
	public function store(StoreRequest $request) {
		$model = $this->repository->create($request->all());
		if ($model) {
			return $this->saveSuccessAction('admin.educational-system.index', 'admin.educational-system.index', "Educational System added Successfully");
		}
		return redirect()->back()->withFlashDanger('Educational System Already Exist');
	}

	/**
	 * Edit EducationalSystem
	 *
	 * @param int $id
	 * @param EditRequest $request
	 * @return $this
	 */
	public function edit($id, EditRequest $request) {
		$item = $this->repository->findOrThrowException($id);

		$curriculum = Curriculum::getCurriculumList('display_in_eductional_systems');
		//$educationalsystems = EducationalSystem::getEducatinalSystem();
		$selectedCurriculam = $item->curriculum_id;
		return view('backend.educationalsystem.edit')
			->with(
				[
					"item" => $item,
					'curriculum' => $curriculum,
					//'educationalsystems' => $educationalsystems,
				]
			)->withSelectedCurriculam($selectedCurriculam);
		// return view('backend.educationalsystem.edit')->with(["item" => $item,
		// 	'curriculum' => Curriculum::getSelectData('curriculum_name', true, 'status', '1'),
		// ]);
	}

	/**
	 * Update EducationalSystem
	 *
	 * @param int $id
	 * @param UpdateRequest $request
	 * @return $this
	 */
	public function update($id, UpdateRequest $request) {
		$model = $this->repository->update($id, $request->all());

		if ($model) {
			return $this->saveSuccessAction('admin.educational-system.index', 'admin.educational-system.index', 'Educational System Edited Successfully');
		}
		return redirect()->back()->withFlashDanger('Educational System Already Exist');
	}

	/**
	 * Delete Educational System
	 *
	 * @param int $id
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function destroy($id, DeleteRequest $request) {
		$item = $this->repository->findOrThrowException($id);

		if ($item->id) {
			$this->repository->destroy($id);
			return response()->json(['status' => "OK"]);
		}
	}

	/**
	 * change in brand model active or inactive
	 * @param type $id
	 * @param type $status
	 * @param Request $request
	 * @return type
	 */
	public function mark($id, $status, Request $request) {
		$this->repository->mark($id, $status);
		return response()->json(['status' => 'OK']);
	}
}
