<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ShareExperience\CreateRequest;
use App\Http\Requests\Frontend\ShareExperience\StoreRequest;
use App\Models\Notification\Notification;
use App\Models\Session\Session;
use App\Repositories\Frontend\ShareExperience\ShareExperienceRepositoryContract;
use Carbon\Carbon;

class ShareExperienceController extends Controller {

	/**
	 * Repository Object
	 *
	 * @var object
	 */
	public $repository;

	/**
	 * __construct
	 *
	 * @param ShareExperienceRepositoryContract $repository
	 */
	function __construct(ShareExperienceRepositoryContract $repository) {
		$this->repository = $repository;
	}

	/**
	 * Listing
	 *
	 * @return mixed
	 */
	public function create(CreateRequest $request) {
		
		$id = \Session::get('front_user_id');
		if(!$request->ajax()){
        	if (!check_front_login()) {
				return redirect('/');
			}
        }
		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			"current_role" => \Session::get('current_role'),
		]; //dd($sessionData);

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		$upcommingSessions = Session::select('session.*', 'front_user.first_name', 'front_user.last_name')->leftJoin('front_user', 'session.tutee_id', '=', 'front_user.id')->where('session.tutor_id', $sessionData['front_user_id'])->where('session.status', 2)->orderBy('session.id', 'desc')->limit(3)->get()->toArray();

		if($request->ajax())
        {
	          	$returnHTML = view('frontend.shareExperience.middle_content_share_exp', ['repository' => $this->repository,'sessionData' => $sessionData,'notifications' => $notifications,'notificationCount' => $notificationCount])->render();
			return response()->json(array('status' => "success", 'html' => $returnHTML));
        }else{
        	return view('frontend.shareExperience.create')
			->with(['repository' => $this->repository])
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('notificationCount', $notificationCount);
        }
	
	}

	public function store(StoreRequest $request) {
		if (!check_front_login()) {
			return redirect('/');
		}
		$record = $this->repository->create($request->all());
		/*Notification:When Tutor / Tutee posts experience  */
		$first_name = \Session::get('front_user_first_name');
		$last_name = \Session::get('front_user_last_name');
		$name = $first_name." ".$last_name;
		$message = "<b>".$name."</b> Posted an experience with TUTEME";
		$Notification = Notification::insert([
			'from_user_id' => 0,
			'to_user_id' => 0,
			'notification_type_id' => 17,
			'is_read' => 0,
			'is_read_admin' => 0,
			'is_for_admin' => 1,
			'created_at' => Carbon::now()->toDateTimestring(),
			'message' => $message,
		]);
		/*Notification end*/
		return redirect(route('frontuser.share.experience'))->with('status', 'Thank you for sharing your experience !!!');
	}
}
