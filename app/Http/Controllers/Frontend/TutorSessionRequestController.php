<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Notification\Notification;
use App\Models\Session\Session;
use App\Models\UserEducation\UserEducation;
use App\Repositories\Frontend\TutorSessionRequest\TutorSessionRequestRepositoryContract;
use App\Services\Access\Traits\SessionRequest;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class TutorSessionRequestController extends Controller
{

    use SessionRequest;
    /**
     * @var Tutor Repository Contact
     */
    protected $repositorycontract;

    /**
     * @param TutorRepositoryContract
     */
    public function __construct(TutorSessionRequestRepositoryContract $repositorycontract)
    {
        $this->repositorycontract = $repositorycontract;
    }

    /**
     *
     */
    public function showPendingSession(Request $request)
    {

        if(!$request->ajax())
        {
            if (!check_front_login()) {
                return redirect('/');
            }
        }
        

        $sessionData = [
            "front_user_id"          => \Session::get('front_user_id'),
            "front_user_role"        => \Session::get('front_user_role'),
            "front_user_first_name"  => \Session::get('front_user_first_name'),
            "front_user_last_name"   => \Session::get('front_user_last_name'),
            "front_user_username"    => \Session::get('front_user_username'),
            "front_user_email"       => \Session::get('front_user_email'),
            "front_user_country_id"  => \Session::get('front_user_country_id'),
            "front_user_state_id"    => \Session::get('front_user_state_id'),
            "front_user_city_id"     => \Session::get('front_user_city_id'),
            "front_user_nationality" => \Session::get('front_user_nationality'),
            "front_user_lang_id"     => \Session::get('front_user_lang_id'),
            "front_user_photo"       => \Session::get('front_user_photo'),
            "front_user_timezone_id" => \Session::get('front_user_timezone_id'),
        ]; //dd($sessionData);

        $crnt_date         = new \DateTime;
        $current_date      = $crnt_date->format('Y-m-d H:i:s');
        $update_data       = DB::statement("UPDATE `session` SET `status` = '9' where `scheduled_date_time` <= '" . $current_date . "' AND (`status` = '5' OR `status` = '2') AND `tutor_id` = " . $sessionData['front_user_id']); //7 mns none of join, 2 mns upcoming
        // dd($update_data);
        $notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

        $notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

        $upcommingSessions = Session::select('session.*', 'front_user.first_name', 'front_user.last_name')->leftJoin('front_user', 'session.tutee_id', '=', 'front_user.id')->where('session.tutor_id', $sessionData['front_user_id'])->where('session.status', 2)->orderBy('session.id', 'desc')->limit(3)->get()->toArray();
        //  dd($upcommingSessions);
        //$date = getOnlyDate();
        $sessionRequest = $this->repositorycontract->getPendingSession();

       

        if($request->ajax())
        {
                $returnHTML = view('frontend.tutor.mid_section_sessionRequest',
                 [
                    'sessionRequest' => $sessionRequest,
                    'sessionData' => $sessionData,
                    'notifications' => $notifications,
                    'notificationCount' => $notificationCount
                ])->render();
            return response()->json(array('status' => "success", 'html' => $returnHTML));
        }else{
             return view('frontend.tutor.sessionRequest')
            ->with('sessionRequest', $sessionRequest)
            ->with('sessionData', $sessionData)
            ->with('notifications', $notifications)
            ->with('notificationCount', $notificationCount)
        ;
        }
    }

    /**
     * Accept Tutee session request
     *
     * $id as session id
     */
    public function acceptRequest($id)
    {
        if (!check_front_login()) {
            return redirect('/');
        }
        $sessionDetail = Session::find($id);
        if (!empty($sessionDetail)) {
            $tutor_id     = \Session::get('front_user_id');
            $crnt_date    = new \DateTime;
            $current_date = $crnt_date->format('Y-m-d H:i:s');
            
            $chack_tutor_availability = Session::where('scheduled_date_time', '<=', $sessionDetail->scheduled_date_time)
                ->where('end_date_time', '>=', $sessionDetail->scheduled_date_time)
                ->where('tutor_id', $tutor_id)
                ->whereIn('status',array('2'))
                ->get()
                ->toArray();


            if(!empty($chack_tutor_availability)){
               /* \Session::flash('flash_danger', 'You have already accepted one session request for this time slot.');*/
                 $request = Session::where('id', $id)
                    ->update([
                        'status'   => '10'
                    ]);
               // return redirect(route('tutor.pending.session'));
                 $array = array('status' => 'error','msg' => 'You have already accepted one session request for this time slot.');
                return json_encode($array);
            }    

            if (strtotime($sessionDetail->end_date_time) < strtotime($current_date)) {
                $update_data = DB::statement("UPDATE `session` SET `status` = '7' where `end_date_time` <= '" . $current_date . "' AND `status` = '5' AND `tutor_id` = '" . $tutor_id . "' AND `id` = " . $id); //7 mns none of join, 2 mns
                /*\Session::flash('flash_danger', 'Your session is expired.');
                return redirect(route('tutor.pending.session'));*/
                $array = array('status' => 'error','msg' => 'Your session is expired.');
                return json_encode($array);
            }

               if (strtotime($sessionDetail->scheduled_date_time) < strtotime($current_date)) {
                $update_data = DB::statement("UPDATE `session` SET `status` = '9' where `scheduled_date_time` <= '" . $current_date . "' AND `status` = '5' AND `tutor_id` = '" . $tutor_id . "' AND `id` = " . $id); //7 mns none of join, 2 mns
                /*\Session::flash('flash_danger', "You can't accept request which time has gone.");
                return redirect(route('tutor.pending.session'));*/

                 $array = array('status' => 'error','msg' => "You can't accept request which time has gone.");
                return json_encode($array);
            }
        }
        $meetinngId      = $this->generateMeetingId();
        $tuteeDetails    = Session::select(DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutee_name"))->leftJoin('front_user', 'session.tutee_id', '=', 'front_user.id')->where('session.id', $id)->first()->toArray();
        $tuteeName       = $tuteeDetails['tutee_name'];
        $tuteeSessionUrl = $this->getTuteeSessionUrl($meetinngId, $tuteeName);

        $data['meeting_id']        = $meetinngId;
        $data['tutee_session_url'] = $tuteeSessionUrl;

        $generateSessionRequest = $this->repositorycontract->updateMeetingSessionData($id, $data);
        if ($generateSessionRequest == false) {
            /*\Session::flash('message', 'Something is wrong !!!');
            return redirect()->route('tutor.pending.session');*/

                $array = array('status' => 'error','msg' => "Something is wrong !!!");
                return json_encode($array);
        }

        $acceptSessionRequest = $this->repositorycontract->acceptSessionRequest($id);
        if ($acceptSessionRequest == false) {
            /*\Session::flash('message', 'Something is wrong !!!');
            return redirect()->route('tutor.pending.session');*/

            $array = array('status' => 'error','msg' => "Something is wrong !!!");
                return json_encode($array);
        } else {
            //\Session::flash('successmessage', 'Session Request has been accepted !!!');
            $sessionData = [
                "front_user_first_name" => \Session::get('front_user_first_name'),
                "front_user_last_name"  => \Session::get('front_user_last_name'),
            ];
            /*Notification:Session request accepted / declined by Tutor */
            $data           = Session::find($id);
            $subject_id     = $data->subject_id;
            $topic_id       = $data->topic_id;
            $tutee_id       = $data->tutee_id;
            $tutor_id       = $data->tutor_id;
            $from_date_time = $data->scheduled_date_time;
            if (isset($topic_id) && $topic_id != 0 && $topic_id != "") {
                $Hierarchy = getCurriculumHierarchy('', $topic_id, true); //for topic
            } else {
                $Hierarchy = getCurriculumHierarchy($subject_id, '', true); //for subject
            }

            $name           = $sessionData['front_user_first_name']." ".$sessionData['front_user_last_name'];

             $tutee_timezone = getTimezoneName($tutee_id);
            $date = ConverTimeZoneForUser($from_date_time, 'd-m-Y',$tutee_timezone) . " | " . ConverTimeZoneForUser($from_date_time, 'h:i a',$tutee_timezone);
            $message        = "<a href='/tutee-sessions'>Your session request for curriculum ".$Hierarchy." has been accepted by <b>".$name."</b> for the session on ".$date ."</a>";

            // dd($tutee_id);
            $Notification = Notification::insert([
                'from_user_id'         => $tutor_id,
                'to_user_id'           => $tutee_id,
                'notification_type_id' => 2,
                'is_read'              => 0,
                'is_read_admin'        => 0,
                'is_for_admin'         => 0,
                'created_at'           => Carbon::now()->toDateTimestring(),
                'message'              => $message,
            ]);

            //for admin notification
            $tutee_data = getUserData($tutee_id);
            $tutee_name = "";
            if(!empty($tutee_data)){
                $tutee_name = $tutee_data->first_name." ".$tutee_data->last_name;
            }
             $message        = "".$name." accept the session request of ".$tutee_name." for the session ".$Hierarchy."";

            $Notification = Notification::insert([
                'from_user_id'         => $tutor_id,
                'to_user_id'           => $tutee_id,
                'notification_type_id' => 2,
                'is_read'              => 1,
                'is_read_admin'        => 0,
                'is_for_admin'         => 1,
                'created_at'           => Carbon::now()->toDateTimestring(),
                'message'              => $message,
            ]);


            /*Notification end*/
           // return redirect()->route('tutor.pending.session');

            $array = array('status' => 'success','msg' => "Session Request has been accepted successfully.");
                return json_encode($array);
        }
    }

    /**
     * decline Tutee session request
     *
     * $id as session id
     */
    public function declineRequest($id)
    {
        if (!check_front_login()) {
            return redirect('/');
        }
        $sessionDetail = Session::find($id);
        if (!empty($sessionDetail)) {
            $tutor_id     = \Session::get('front_user_id');
            $crnt_date    = new \DateTime;
            $current_date = $crnt_date->format('Y-m-d H:i:s');
            if ($sessionDetail->end_date_time < $current_date) {
                $update_data = DB::statement("UPDATE `session` SET `status` = '7' where `end_date_time` <= '" . $current_date . "' AND `status` = '5' AND `tutor_id` = '" . $tutor_id . "' AND `id` = " . $id); //7 mns none of join, 2 mns
               // \Session::flash('flash_danger', 'Your session is expired.');
                 $array = array('status' => 'error','msg' => 'Your session is expired.');
                return json_encode($array);
            }
        }
        $acceptSessionRequest = $this->repositorycontract->declineSessionRequest($id);
        if ($acceptSessionRequest == false) {
            /*\Session::flash('message', 'Something is wrong !!!');
            return redirect()->route('tutor.pending.session');*/
              $array = array('status' => 'error','msg' => 'Something is wrong !!!');
                return json_encode($array);
        } else {
            //\Session::flash('successmessage', 'Session Request has been declined !!!');
            $sessionData = [
                "front_user_first_name" => \Session::get('front_user_first_name'),
                "front_user_last_name"  => \Session::get('front_user_last_name'),
            ];
            /*Notification:Session request accepted / declined by Tutor */
            $data           = Session::find($id);
            $subject_id     = $data->subject_id;
            $topic_id       = $data->topic_id;
            $tutee_id       = $data->tutee_id;
            $tutor_id       = $data->tutor_id;
            $from_date_time = $data->scheduled_date_time;
            if (isset($topic_id) && $topic_id != 0 && $topic_id != "") {
                $Hierarchy = getCurriculumHierarchy('', $topic_id, true); //for topic
            } else {
                $Hierarchy = getCurriculumHierarchy($subject_id, '', true); //for subject
            }

            $name           = $sessionData['front_user_first_name']." ".$sessionData['front_user_last_name'];

             $tutee_timezone = getTimezoneName($tutee_id);
            $date = ConverTimeZoneForUser($from_date_time, 'd-m-Y',$tutee_timezone) . " | " . ConverTimeZoneForUser($from_date_time, 'h:i a',$tutee_timezone);

            $message        = "<a href='/tutee-sessions'>Your session request for curriculum ".$Hierarchy." has been declined by <b>".$name."</b> for the session on ".$date ."</a>";
            // dd($tutee_id);
            $Notification = Notification::insert([
                'from_user_id'         => $tutor_id,
                'to_user_id'           => $tutee_id,
                'notification_type_id' => 2,
                'is_read'              => 0,
                'is_read_admin'        => 0,
                'is_for_admin'         => 0,
                'created_at'           => Carbon::now()->toDateTimestring(),
                'message'              => $message,
            ]);

            //for admin notification
            $tutee_data = getUserData($tutee_id);
            $tutee_name = "";
            if(!empty($tutee_data)){
                $tutee_name = $tutee_data->first_name." ".$tutee_data->last_name;
            }
             $message        = "".$name." remove the session request of ".$tutee_name." for the session ".$Hierarchy."";

            $Notification = Notification::insert([
                'from_user_id'         => $tutor_id,
                'to_user_id'           => $tutee_id,
                'notification_type_id' => 2,
                'is_read'              => 1,
                'is_read_admin'        => 0,
                'is_for_admin'         => 1,
                'created_at'           => Carbon::now()->toDateTimestring(),
                'message'              => $message,
            ]);
            /*Notification end*/
            $array = array('status' => 'success','msg' => "Session Request has been cancelled successfully !!!");
                return json_encode($array);
        }
    }

    public function declineRequestModel($id)
    {
        if (!check_front_login()) {
            return redirect('/');
        }

        $data = Session::find($id);
        if (!empty($data)) {
            $returnHTML = view('frontend.tutor.model')
                ->with(['id' => $id])
                ->render();
            return response()->json(array($returnHTML));
        } else {
            \Session::flash('message', 'Something is wrong !!!');
            return redirect()->route('tutor.pending.session');
        }
    }

    public function tutorJoinSession(request $request)
    {
        $data = $request->all();
        $id   = $data['id'];

        $session_data = Session::Select('tutor_session_url', 'tutor_id', 'tutee_id', 'scheduled_date_time','topic_id','subject_id')
            ->where('id', $id)
            ->first()->toArray();

        /*Notification:Tutor joins a session */
        $sessionData = [
            "front_user_first_name" => \Session::get('front_user_first_name'),
            "front_user_last_name"  => \Session::get('front_user_last_name'),
            "front_user_role"       => \Session::get('current_role'),
        ];
        $name    = $sessionData['front_user_first_name'] . " " . $sessionData['front_user_last_name'];
        $message = "<a href='/tutee-dashboard'><b>" . $name . "</b> waiting for you to join a session.please join a session as soon as possible.</a>";

        $Notification = Notification::insert([
            'from_user_id'         => $session_data['tutor_id'],
            'to_user_id'           => $session_data['tutee_id'],
            'notification_type_id' => 8,
            'is_read'              => 0,
            'is_read_admin'        => 0,
            'is_for_admin'         => 0,
            'created_at'           => Carbon::now()->toDateTimestring(),
            'message'              => $message,
        ]);

        $topic_id = $session_data['topic_id'];
            $subject_id = $session_data['subject_id'];

            if (isset($topic_id) && $topic_id != 0 && $topic_id != "") {
                $Hierarchy  =  getCurriculumHierarchy('', $topic_id, true); //for topic
            } else {
                $Hierarchy  = getCurriculumHierarchy($subject_id, '', true); //for subject
            }

        //for admin
        $message = "<b>".$name."</b> join session for the curriculum <b>".$Hierarchy."</b>";
        $Notification = Notification::insert([
            'from_user_id' => $session_data['tutor_id'],
            'to_user_id' => $session_data['tutee_id'],
            'notification_type_id' => 8,
            'is_read' => 1,
            'is_read_admin' => 0,
            'is_for_admin' => 1,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
        ]);

        /*Notification end*/

        if (empty($session_data['tutor_session_url']) && $session_data['tutor_session_url'] == '') {
            $data      = SessionRequest::joinSessionBytutor($id);
            $tutor_url = SessionRequest::getTutorSessionUrl($id);

            $find_data = Session::find($id);
            if (!empty($find_data)) {

                /*punctuality_ratings calculation*/
                $final_ratings = 0;
                if (!empty($session_data)) {
                    $schedule_time = ConverTimeZoneForUser($session_data['scheduled_date_time'], 'Y-m-d H:i');
                    $current_time  = ConverTimeZoneForUser(Carbon::now()->toDateTimestring(), 'Y-m-d H:i');
                    // echo "<pre>";print_r($schedule_time);echo "<br>";print_r($current_time);
                    $schedule_time = strtotime($schedule_time);
                    $current_time  = strtotime($current_time);
                    $interval      = abs($current_time - $schedule_time);
                    $minutes       = intval(round($interval / 60));
                //  dd( $interval );
                    

                if($schedule_time >= $current_time){
                        /*$result = DB::select(DB::raw("(SELECT rating FROM `punctuality_ratings` WHERE `status` = '1' order by rating desc limit 1)"));
                        $result = (object_to_array($result[0]));
                       if(!empty($result)){
                             $final_ratings = $result['rating']; 
                       } */
                        $final_ratings = '10';
                    }else{
                        if ($minutes > 0) {
                            $punctuality_ratings = countPunctualityRatingsForSuccessfulSession($minutes);
                            if (!empty($punctuality_ratings)) {
                                $final_ratings = $punctuality_ratings;

                            }

                        }
                    }
                }//dd($final_ratings);
                /*punctuality_ratings calculation end*/
                $request = Session::where('id', $id)
                    ->update([
                        'tutor_session_url'   => $tutor_url,
                        'conducted_date_time' => Carbon::now()->toDateTimestring(),
                        'punctuality_ratings' => $final_ratings,
                    ]);

                $avg_punctuality_ratings = DB::table('session')
                    ->where('tutor_id', $session_data['tutor_id'])
                    ->whereIn('status', array('1', '3'))
                    ->avg('punctuality_ratings');
                $final_avg_punctuality_ratings = round($avg_punctuality_ratings, 1);
                $user_educational              = UserEducation::where('front_user_id', $session_data['tutor_id'])
                    ->update([
                        'punctuality' => $final_avg_punctuality_ratings,
                        'updated_at'  => Carbon::now()->toDateTimestring(),
                    ]);

            }
            echo json_encode($data);
        } else {
            $data = SessionRequest::isMeetingRunning($id);

            if (isset($data['link']) && !empty($data['link'])) {
                if ($data['link']['running'][0] == 'false') {

                    $obj = Session::find($id);
                    $obj->status = 1;
                    $obj->save();

                    if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 2) {
                        \Session::flash('sessionExpired', 'Your session is expired.');
                        \Session::save();

                        $data['link'] = url('/tutee-dashboard');
                        echo json_encode($data);die;

                    } else {
                        \Session::flash('sessionExpired', 'Your session is expired.');
                        \Session::save();

                        $data['link'] = url('/tutor-dashboard');
                        echo json_encode($data);die;
                    }

                }
            }
            $data['link'] = $session_data['tutor_session_url'];
            echo json_encode($data);
        }

    }
}
