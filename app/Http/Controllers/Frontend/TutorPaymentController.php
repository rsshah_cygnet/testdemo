<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Notification\Notification;
use App\Models\Session\Session;
use App\Repositories\Frontend\TutorPayment\TutorPaymentRepositoryContract;
use Illuminate\Http\Request;
class TutorPaymentController extends Controller {
	/**
	 * @var Tutor Repository Contact
	 */
	protected $repositorycontract;

	/**
	 * @param TutorRepositoryContract
	 */
	public function __construct(TutorPaymentRepositoryContract $repositorycontract) {
		$this->repositorycontract = $repositorycontract;
	}

	public function showPayment(Request $request) {
		if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }
		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
		]; //dd($sessionData);

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		$upcommingSessions = Session::select('session.*', 'front_user.first_name', 'front_user.last_name')->leftJoin('front_user', 'session.tutee_id', '=', 'front_user.id')->where('session.tutor_id', $sessionData['front_user_id'])->where('session.status', 2)->orderBy('session.id', 'desc')->limit(3)->get()->toArray();
		//  dd($upcommingSessions);
		//$date = getOnlyDate();

		$getPayments = $this->repositorycontract->getPayments();
		$cancelSessionsByTutee = $this->repositorycontract->getCancelSessionByTutee();
		$cancelSessionByTutor = $this->repositorycontract->getCencelSessionByTutor();

		

		if($request->ajax()){
			 $returnHTML =  view('frontend.tutor.mid_section_payment')
			->with('getPayments', $getPayments)
			->with('cancelSessionsByTutee', $cancelSessionsByTutee)
			->with('cancelSessionByTutor', $cancelSessionByTutor)
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('notificationCount', $notificationCount)->render();
             return response()->json(array('status' => 'success', 'html' => $returnHTML));
        }else{
        	return view('frontend.tutor.payment')
			->with('getPayments', $getPayments)
			->with('cancelSessionsByTutee', $cancelSessionsByTutee)
			->with('cancelSessionByTutor', $cancelSessionByTutor)
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('notificationCount', $notificationCount);
        }	
	}

	public function showPaymentByFilter($startDate, $endDate) {
		if (!check_front_login()) {
			return redirect('/');
		}
		$getPaymentsByFilter = $this->repositorycontract->getPaymentsByFilter($startDate, $endDate);
		$cancelSessionsByTuteeByFilter = $this->repositorycontract->cancelSessionsByFilter($startDate, $endDate);
		$cancelSessionByTutorByFilter = $this->repositorycontract->cancelSessionByTutorByFilter($startDate, $endDate);
		$successfulSessionHTML = view('frontend.tutor.successful-session')
			->with(['getPaymentsByFilter' => $getPaymentsByFilter])
			->render();

		$cancelSessionByTuteeHTML = view('frontend.tutor.session-cancel-by-tutee')
			->with(['cancelSessionsByTuteeByFilter' => $cancelSessionsByTuteeByFilter])
			->render();

		$cancelSessionByTutorHTML = view('frontend.tutor.session-cancel-by-tutor')
			->with(['cancelSessionByTutorByFilter' => $cancelSessionByTutorByFilter])
			->render();

		return response()->json(array('successfulSessionHTML' => $successfulSessionHTML, 'cancelSessionByTuteeHTML' => $cancelSessionByTuteeHTML, 'cancelSessionByTutorHTML' => $cancelSessionByTutorHTML));
	}

	public function getPaymnetType($type, Request $request)
	{
// dd($request->all());
		if($request->startDate != "undefined" && !empty($request->endDate))
		{
			$startDate	=	$request->startDate;
			$endDate	=	$request->endDate;
			
			$getPaymentsByFilter = $this->repositorycontract->getPaymentsByFilter($startDate, $endDate);
			$cancelSessionsByTuteeByFilter = $this->repositorycontract->cancelSessionsByFilter($startDate, $endDate);
			$cancelSessionByTutorByFilter = $this->repositorycontract->cancelSessionByTutorByFilter($startDate, $endDate);
		
		$successfulSessionHTML = view('frontend.tutor.successful-session')
			->with(['getPaymentsByFilter' => $getPaymentsByFilter])
			->render();

		$cancelSessionByTuteeHTML = view('frontend.tutor.session-cancel-by-tutee')
			->with(['cancelSessionsByTuteeByFilter' => $cancelSessionsByTuteeByFilter])
			->render();

		$cancelSessionByTutorHTML = view('frontend.tutor.session-cancel-by-tutor')
			->with(['cancelSessionByTutorByFilter' => $cancelSessionByTutorByFilter])
			->render();

		return response()->json(array('successfulSessionHTML' => $successfulSessionHTML, 'cancelSessionByTuteeHTML' => $cancelSessionByTuteeHTML, 'cancelSessionByTutorHTML' => $cancelSessionByTutorHTML));
		}

		$getPaymentsByFilter = $this->repositorycontract->getPayments();
		$cancelSessionsByTuteeByFilter = $this->repositorycontract->getCancelSessionByTutee();
		$cancelSessionByTutorByFilter = $this->repositorycontract->getCencelSessionByTutor();
		
  //  		$getPaymentsByFilter = $this->repositorycontract->getPaymentsByFilter($startDate, $endDate);
		// $cancelSessionsByTuteeByFilter = $this->repositorycontract->cancelSessionsByFilter($startDate, $endDate);
		// $cancelSessionByTutorByFilter = $this->repositorycontract->cancelSessionByTutorByFilter($startDate, $endDate);
		// dd($getPaymentsByFilter);
		$successfulSessionHTML = view('frontend.tutor.successful-session')
			->with(['getPaymentsByFilter' => $getPaymentsByFilter])
			->render();

		$cancelSessionByTuteeHTML = view('frontend.tutor.session-cancel-by-tutee')
			->with(['cancelSessionsByTuteeByFilter' => $cancelSessionsByTuteeByFilter])
			->render();

		$cancelSessionByTutorHTML = view('frontend.tutor.session-cancel-by-tutor')
			->with(['cancelSessionByTutorByFilter' => $cancelSessionByTutorByFilter])
			->render();
			// return view()
		return response()->json(array('successfulSessionHTML' => $successfulSessionHTML, 'cancelSessionByTuteeHTML' => $cancelSessionByTuteeHTML, 'cancelSessionByTutorHTML' => $cancelSessionByTutorHTML));
	}
}
