<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\LandingPage\ContactusRequest;
use App\Models\Contactus\Contactus;
use App\Models\Curriculum\Curriculum;
use App\Models\EducationalSystem\EducationalSystem;
use App\Models\Grade\Grade;
use App\Models\Language\Language;
use App\Models\Levels\Levels;
use App\Models\Program\Program;
use App\Models\Timezone\Timezone;
use Illuminate\Http\Request;
use App\Models\Notification\Notification;
use Carbon\Carbon;
use DB;

class LandingPageController extends Controller {
	public function showLandingPage(Request $request) {

		$role = \Session::get('front_user_role');
		if (isset($role) && $role != 0 && $role == 2) {
			return redirect()->route('frontend.tuteeDashboard');
		} else if (isset($role) && $role != 0 && ($role == 1 || $role == 3)) {
			return redirect()->route('frontend.tutorDashboard');
		}

		$curriculumn = Curriculum::getCurriculumList();
		$educational = EducationalSystem::getEducationSystemList();
		$level = Levels::getLevelsList();
		$program = Program::getProgramList();
		$grade = Grade::getGradeList();
		$language = Language::getAll();
		$timezone = Timezone::getAll();
		//$pagename = "About Us";
		//$aboutus = Cmspage::select('description')->where('title','=',$pagename)->first();

		 $seo_title = DB::table('settings')->select('seo_title')->first();
		 $meta_title = isset($seo_title->seo_title)?$seo_title->seo_title:"";


		 $seo_description = DB::table('settings')->select('seo_description')->first();
		 $meta_description = isset($seo_description->seo_description)?$seo_description->seo_description:"";


		return view('frontend.landingpage.index', compact('curriculumn', 'educational', 'level', 'program', 'grade', 'language', 'timezone','meta_title','meta_description'));
	}

	public function saveContactUs(ContactusRequest $request) {

		$contactus = Contactus::insert([
			'name' => $request->name,
			'email' => $request->email,
			'description' => $request->message,
		]);

		/*Notification:Contact Us request received  */
		$message = "You have received a Contact Us request from <b>".$request->name."</b>";
		$Notification = Notification::insert([
			'from_user_id' => 0,
			'to_user_id' => 0,
			'notification_type_id' => 16,
			'is_read' => 0,
			'is_read_admin' => 0,
			'is_for_admin' => 1,
			'created_at' => Carbon::now()->toDateTimestring(),
			'message' => $message,
		]);
		/*Notification end*/
		// redirect()->to($request->header('contactUS') . '#contactUS');
		$data = array('status' => 'We will contact you');
		return json_encode($data);

		// $url = URL::route('frontend.indexpage') . '#contactUS';
		// return Redirect::to($url);
		// return Redirect::to(URL::previous() . "#contactUS");
		// return redirect()->route("frontend.indexpage");
	}

	/*public function cal() {
		$from_name = "webmaster";        
		$from_address = "webmaster@example.com";        
		$to_name = "Sudhir";        
		$to_address = "cygnet.svirpara@outlook.com";        
		$startTime = "11/12/2013 18:00:00";        
		$endTime = "11/12/2013 19:00:00";        
		$subject = "My Test Subject";        
		$description = "My Awesome Description";        
		$location = "Joe's House";
		$data = sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject, $description, $location);
		if($data){
		    echo "success";exit;
		}else{
		    echo "fails";exit;
		}
	}*/

}
