<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Notification\Notification;
use App\Models\Session\Session;
use App\Repositories\Frontend\TuteeFreeSesssion\TuteeFreeSesssionRepositoryContract;
use Illuminate\Http\Request;
class TuteeFreeSesssionController extends Controller {
	/**
	 * @var Tutor Repository Contact
	 */
	protected $repositorycontract;

	/**
	 * @param TutorRepositoryContractfront
	 */
	public function __construct(TuteeFreeSesssionRepositoryContract $repositorycontract) {
		$this->repositorycontract = $repositorycontract;
	}

	/**
	 * Get Tutee Free Session
	 */
	public function showFreeSession(Request $request) {
		if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }

		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			 "current_role"           => \Session::get('current_role'),
		]; //dd($sessionData);

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		$upcommingSessions = Session::select('session.*', 'front_user.first_name', 'front_user.last_name')->leftJoin('front_user', 'session.tutee_id', '=', 'front_user.id')->where('session.tutor_id', $sessionData['front_user_id'])->where('session.status', 2)->orderBy('session.id', 'desc')->limit(3)->get()->toArray();

		$getFreeSession = $this->repositorycontract->getTuteeFreeSesssion();
		// dd($getFreeSession);
		

		if($request->ajax())
        {
                $returnHTML = view('frontend.tutee.mid_section_free_session', 
                	[
                		'sessionData' => $sessionData,
                		'getFreeSession' => $getFreeSession,
                		'notifications' => $notifications,
                		'notificationCount' => $notificationCount, 
                		
                	])->render();
            return response()->json(array('status' => "success",'html' => $returnHTML));
        }else{
            return view('frontend.tutee.free-session')
			->with('getFreeSession', $getFreeSession)
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('notificationCount', $notificationCount);
        }	
	}
}
