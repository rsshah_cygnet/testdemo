<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Auth\RegisterTutorCurriculumRequest;
use App\Http\Requests\Frontend\Tutee\ReportTutorRequest;
use App\Http\Requests\Frontend\Tutee\TuteePreferenceRequest;
use App\Models\Curriculum\Curriculum;
use App\Models\Frontusers\Frontusers;
use App\Models\Language\Language;
use App\Models\Notification\Notification;
use App\Models\Session\Session;
use App\Models\TuteeBasicPreference\TuteeBasicPreference;
use App\Models\TuteeSessionPreference\TuteeSessionPreference;
use App\Models\TutorRatings\TutorRatings;
use App\Models\UserReports\UserReports;
use App\Repositories\Frontend\Tutee\TuteeRepositoryContract;
use App\Services\Access\Traits\SessionRequest;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Models\SessionFeedback\SessionFeedback;

class TuteeController extends Controller
{
    use SessionRequest;
    /**
     * @var TuteeRepositoryContract
     */
    protected $repositorycontract;

    /**
     * @param TuteeRepositoryContract $settings
     */
    public function __construct(TuteeRepositoryContract $RepositoryContract)
    {
        $this->repositorycontract = $RepositoryContract;
    }

    public function dashboard(Request $request)
    {
        if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }

        $sessionData = [
            "front_user_id"          => \Session::get('front_user_id'),
            "front_user_role"        => \Session::get('front_user_role'),
            "front_user_first_name"  => \Session::get('front_user_first_name'),
            "front_user_last_name"   => \Session::get('front_user_last_name'),
            "front_user_username"    => \Session::get('front_user_username'),
            "front_user_email"       => \Session::get('front_user_email'),
            "front_user_country_id"  => \Session::get('front_user_country_id'),
            "front_user_state_id"    => \Session::get('front_user_state_id'),
            "front_user_city_id"     => \Session::get('front_user_city_id'),
            "front_user_nationality" => \Session::get('front_user_nationality'),
            "front_user_lang_id"     => \Session::get('front_user_lang_id'),
            "front_user_photo"       => \Session::get('front_user_photo'),
            "front_user_timezone_id" => \Session::get('front_user_timezone_id'),
            "current_role"           => \Session::get('current_role'),
        ];
        \Session::set('current_role', '2');
        if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 1) {
            return redirect('/tutor-dashboard');
        }

        $notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->where('is_for_admin', 0)->count();

        $notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_for_admin', 0)->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

        $upcommingSessions = Session::select('session.*', 'front_user.first_name', 'front_user.last_name')->leftJoin('front_user', 'session.tutor_id', '=', 'front_user.id')->where('session.tutee_id', $sessionData['front_user_id'])->where('session.status', 2)->orderBy('session.id', 'desc')->limit(3)->get()->toArray();

        $preference_where = Frontusers::select('tutee_basic_preference.*', 'tutee_session_preference.front_user_id as user_id', 'tutee_session_preference.deleted_at as deleted_preference',
            DB::raw("GROUP_CONCAT( DISTINCT CONCAT(tutee_session_preference.curriculum_id,':',tutee_session_preference.education_system_id,':',tutee_session_preference.level_id,':',tutee_session_preference.program_id,':',tutee_session_preference.grade_id,':',tutee_session_preference.subject_id,':',tutee_session_preference.topic_id)  SEPARATOR ',') as pre_curri")
        )
            ->leftJoin('tutee_basic_preference', 'tutee_basic_preference.front_user_id', '=', 'front_user.id')
            ->leftJoin('tutee_session_preference', 'tutee_session_preference.front_user_id', '=', 'front_user.id')
            ->where('front_user.id', '=', $sessionData['front_user_id'])
            ->where('tutee_session_preference.deleted_at', '=', null)
        /*->where('tutee_session_preference.front_user_id', '=', $sessionData['front_user_id'])*/
            ->get()->toArray();

        $from_rating          = "";
        $to_rating            = "";
        $from_punctual_rating = "";
        $to_punctual_rating   = "";
        $lang                 = "";
        $pre_curr             = '';
        if ($preference_where['0']['from_rating'] !== null) {
            $from_rating = $preference_where['0']['from_rating'];
        }
        if ($preference_where['0']['to_rating'] !== null) {
            $to_rating = $preference_where['0']['to_rating'];
        }
        if ($preference_where['0']['from_punctual_rating'] !== null) {
            $from_punctual_rating = $preference_where['0']['from_punctual_rating'];
        }
        if ($preference_where['0']['to_punctual_rating'] !== null) {
            $to_punctual_rating = $preference_where['0']['to_punctual_rating'];
        }
        if ($preference_where['0']['lang_id'] !== null) {
            $lang = $preference_where['0']['lang_id'];
        }
        if ($preference_where['0']['pre_curri'] !== null) {
            $pre_curr = $preference_where['0']['pre_curri'];
        }

        $tutors = Frontusers::select('front_user.id as userId', 'front_user.lang_id', 'front_user.first_name', 'front_user.last_name', 'front_user.role', 'front_user.photo', 'user_educational.punctuality', 'user_educational.rating', 'language.name as language', 'qualification.*',
            DB::raw("GROUP_CONCAT( DISTINCT CONCAT(subject.id,':',subject.subject_name)  SEPARATOR ', ') as subjects"),

            DB::raw("GROUP_CONCAT( DISTINCT CONCAT(tutor_details.curriculum_id,':',tutor_details.education_system_id,':',tutor_details.level_id,':',tutor_details.program_id,':',tutor_details.grade_id,':',tutor_details.subject_id,':',tutor_details.topic_id)  SEPARATOR ',') as tutor_curri")

        )
            ->leftJoin('country', 'front_user.country_id', '=', 'country.id')
            ->leftJoin('language', 'front_user.lang_id', '=', 'language.id')
            ->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'front_user.id')
            ->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
            ->leftJoin('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
            ->leftJoin('subject', 'tutor_details.subject_id', '=', 'subject.id')

            ->whereIn('front_user.role', [1, 3])
            ->where('front_user.id', '!=', \Session::get('front_user_id'))
            ->where('tutor_details.passed_status', '=', 1)
            ->where('front_user.status', '=', 1);
        if ($from_rating != "") {
            $tutors->where('user_educational.rating', '>=', $from_rating);
        }
        if ($to_rating != "") {
            $tutors->where('user_educational.rating', '<=', $to_rating);
        }
        if ($from_punctual_rating != "") {
            $tutors->where('user_educational.punctuality', '>=', $from_punctual_rating);
        }
        if ($to_punctual_rating != "") {
            $tutors->where('user_educational.punctuality', '<=', $to_punctual_rating);
        }
        /*if ($lang != "") {
        $tutors->whereRaw("find_in_set(front_user.lang_id,'" . $lang . "')");
        }*/
        if ($pre_curr != "") {
            $tutors->havingRaw("find_in_set(tutor_curri,'" . $pre_curr . "')");
        }
        $tutors->groupBy('front_user.id');
        $tutors->limit(6);
        $result = $tutors->get()->toArray();
        //dd($result);
        $prefered_language = $actual_language = [];
        $prefered_language = explode(',', $lang);
        // dd($prefered_language);
        if (!empty($result) && $lang != "") {
            foreach ($result as $key => $value) {
                $actual_language  = explode(',', $value['lang_id']);
                $intersect_result = array_intersect($prefered_language, $actual_language);
                if (empty($intersect_result)) {
                    unset($result[$key]);
                }
            }
        }
        $tutors = $result;

        $user                            = [];
        $accepted_by_tutor_session_count = DB::table('session')
            ->whereIn('status', [1, 2])
            ->where('tutee_id',$sessionData['front_user_id'])
            ->count();
        $session_cancelled_by_tutor_count = DB::table('session')
            ->where('status', 3)
            ->where('tutee_id',$sessionData['front_user_id'])
            ->count();
        $session_cancelled_by_me_count = DB::table('session')
            ->where('status', 4)
            ->where('tutee_id',$sessionData['front_user_id'])
            ->count();

        $user = array();
        $user = ['Session requests accepted by tutor' => $accepted_by_tutor_session_count, "Sessions cancelled by me" => $session_cancelled_by_me_count, "Sessions cancelled by tutor" => $session_cancelled_by_tutor_count];
        //  dd($upcommingSessions);

        // dd(\Auth::user());
        $curriculumn = Curriculum::getCurriculumList();

        


        if($request->ajax())
        {
                $returnHTML = view('frontend.tutee.mid_section_dashboard', ['sessionData' => $sessionData,'tutors' => $tutors,'user' => $user,'notifications' => $notifications,'notificationCount' => $notificationCount, 'upcommingSessions' => $upcommingSessions, 'curriculumn' => $curriculumn])->render();
            return response()->json(array('status' => "success",'user' => $user ,'html' => $returnHTML));
        }else{
            return view('frontend.tutee.dashboard')
            ->with('sessionData', $sessionData)
            ->with('tutors', $tutors)
            ->with('user', $user)
            ->with('notifications', $notifications)
            ->with('upcommingSessions', $upcommingSessions)
            ->with('notificationCount', $notificationCount)
            ->with('curriculumn', $curriculumn);
                /* ->withEducationalsystems($educationalsystems)
            ->with('formtype','create');*/
        }

    }

    public function my_preference(Request $request)
    {
        if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }


        $request->session()->forget('session_preference_curriculum_last_added');
        $request->session()->forget('session_preference_curriculum');
        // dd(access()->user());
        $sessionData = [
            "front_user_id"          => \Session::get('front_user_id'),
            "front_user_role"        => \Session::get('front_user_role'),
            "front_user_first_name"  => \Session::get('front_user_first_name'),
            "front_user_last_name"   => \Session::get('front_user_last_name'),
            "front_user_username"    => \Session::get('front_user_username'),
            "front_user_email"       => \Session::get('front_user_email'),
            "front_user_country_id"  => \Session::get('front_user_country_id'),
            "front_user_state_id"    => \Session::get('front_user_state_id'),
            "front_user_city_id"     => \Session::get('front_user_city_id'),
            "front_user_nationality" => \Session::get('front_user_nationality'),
            "front_user_lang_id"     => \Session::get('front_user_lang_id'),
            "front_user_photo"       => \Session::get('front_user_photo'),
            "front_user_timezone_id" => \Session::get('front_user_timezone_id'),
             "current_role"           => \Session::get('current_role'),
        ];
        $notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

        $notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

        $curriculum = Curriculum::where('status', 1)->pluck('curriculum_name', 'id')->all();
        $language   = Language::getAll();

        $tutee_basic_preference_data = TuteeBasicPreference::select('*')->where('front_user_id', $sessionData['front_user_id'])->get()->first();
        $tutee_basic_preference_data = json_decode(json_encode($tutee_basic_preference_data), true);

        $tutee_session_preference_data = TuteeSessionPreference::select('tutee_session_preference.*', 'subject.subject_name', 'topic.topic_name')->leftJoin('subject', 'tutee_session_preference.subject_id', '=', 'subject.id')->leftJoin('topic', 'tutee_session_preference.topic_id', '=', 'topic.id')->where('tutee_session_preference.front_user_id', $sessionData['front_user_id'])->get()->all();

//dd($tutee_session_preference_data);

        $tutee_session_preference_data = json_decode(json_encode($tutee_session_preference_data), true);
    

        if($request->ajax())
        {
                $returnHTML = view('frontend.tutee.mid_section_my_preference', 
                    [
                        'sessionData' => $sessionData,
                        'tutee_basic_preference_data' => $tutee_basic_preference_data,
                        'tutee_session_preference_data' => $tutee_session_preference_data,
                        'notifications' => $notifications,
                        'notificationCount' => $notificationCount,
                        'language' => $language,
                        'curriculum' => $curriculum
                    ])->render();
            return response()->json(array('status' => "success",'html' => $returnHTML));
        }else{
            return view('frontend.tutee.my_preference')
            ->with('sessionData', $sessionData)
            ->with('notifications', $notifications)
            ->with('curriculum', $curriculum)
            ->with('language', $language)
            ->with('notificationCount', $notificationCount)
            ->with('tutee_basic_preference_data', $tutee_basic_preference_data)
            ->with('tutee_session_preference_data', $tutee_session_preference_data);
        }

    }

    public function save_preference(TuteePreferenceRequest $request)
    {

        $data            = $request->all();
        $curriculum_data = $request->session()->pull('session_preference_curriculum');
        $request->session()->forget('session_preference_curriculum_last_added');
        //echo $curriculum_data."--";die;
        if (isset($data['lang_id']) && !empty($data['lang_id'])) {
            $languages = implode($data['lang_id'], ',');
        } else {
            $languages = '';
        }

        //save curiculumn data
        if (isset($curriculum_data) && !empty($curriculum_data)) {
            foreach ($curriculum_data as $key => $value) {
                $object                      = new TuteeSessionPreference;
                $object->front_user_id       = \Session::get('front_user_id');
                $object->curriculum_id       = isset($value['cu']) ? $value['cu'] : '';
                $object->education_system_id = isset($value['es']) ? $value['es'] : '';
                $object->level_id            = isset($value['lv']) ? $value['lv'] : '';
                $object->grade_id            = isset($value['gd']) ? $value['gd'] : '';
                $object->program_id          = isset($value['pg']) ? $value['pg'] : '';
                $object->subject_id          = isset($value['sb']) ? $value['sb'] : '';
                $object->topic_id            = isset($value['tp']) ? $value['tp'] : '';
                $object->save();

            }
        }

        if ($languages == '' && $data['from_punctual_rating'] == '' && $data['to_punctual_rating'] == '' && $data['from_rating'] == '' && $data['to_rating'] == '') {
            return $array = array('id' => '');
            return json_encode($array);
        }

        if (isset($data['hidden_id']) && $data['hidden_id'] != '') {
            $obj = TuteeBasicPreference::find($data['hidden_id']);
        } else {
            $obj = new TuteeBasicPreference;
        }

        $obj->front_user_id        = \Session::get('front_user_id');
        $obj->lang_id              = isset($languages) && $languages != '' ? $languages : null;
        $obj->from_punctual_rating = isset($data['from_punctual_rating']) && $data['from_punctual_rating'] != '' ? $data['from_punctual_rating'] : null;
        $obj->to_punctual_rating   = isset($data['to_punctual_rating']) && $data['to_punctual_rating'] != '' ? $data['to_punctual_rating'] : null;
        $obj->from_rating          = isset($data['from_rating']) && $data['from_rating'] != '' ? $data['from_rating'] : null;
        $obj->to_rating            = isset($data['to_rating']) && $data['to_rating'] != '' ? $data['to_rating'] : null;

        if ($obj->save()) {
            $array = array('id' => $obj->id,'msg' => 'Preference Save successfully');
            return json_encode($array);
        }
    }

    public function save_session_preference(RegisterTutorCurriculumRequest $request)
    {
        $error = array();

        if ($request['grade_type'] == 1) {
            if ($request['topic_id_payment'] == '') {
                $error['topic_error'] = 'The Topic field is required';
                return $error;
            }

        }

        $allValue['cu'] = isset($request['curriculum_id_payment']) ? $request['curriculum_id_payment'] : '';
        $allValue['gd'] = isset($request['grade_id_payment']) ? $request['grade_id_payment'] : '';
        $allValue['lv'] = isset($request['level_id_payment']) ? $request['level_id_payment'] : '';
        $allValue['sb'] = isset($request['subject_id_payment']) ? $request['subject_id_payment'] : '';
        $allValue['tp'] = isset($request['topic_id_payment']) ? $request['topic_id_payment'] : '';
        $allValue['es'] = isset($request['education_id_payment']) ? $request['education_id_payment'] : '';
        $allValue['pg'] = isset($request['program_id_payment']) ? $request['program_id_payment'] : '';

        //find from db
        $obj = TuteeSessionPreference::where('curriculum_id', $allValue['cu'])->where('education_system_id', $allValue['es'])->where('level_id', $allValue['lv'])->where('program_id', $allValue['pg'])->where('grade_id', $allValue['gd'])->where('subject_id', $allValue['sb'])->where('topic_id', $allValue['tp'])->where('front_user_id', \Session::get('front_user_id'))->get()->toArray();

        if (!empty($obj)) {
            $error['curriculumn_error'] = 'This heirarchy is already added before';
            return $error;
        }
        //session curriculumn
        $curriculum = ($request->session()->get('session_preference_curriculum'));
        if (isset($curriculum) && !empty($curriculum)) {
            foreach ($curriculum as $key => $value) {
                $array_diff = array();
                $array_diff = (array_diff_assoc($value, $allValue));
                if (empty($array_diff)) {
                    $error['curriculumn_error'] = 'This heirarchy is already added before';
                    return $error;
                }
            }
        }

        //$request->session()->put('session_preference_curriculum_last_added', (int)3);
        //$request->session()->forget('session_preference_curriculum_last_added');
        $count = (int) $request->session()->get('session_preference_curriculum_last_added');
        $count = $count + 1;

        //$request->session()->forget('session_preference_curriculum_last_added');
        $final_array = $allValue;
        $request->session()->put('session_preference_curriculum_last_added', $count);

        $request->session()->put('session_preference_curriculum.curriculum_' . $count, $allValue);
        $array = array('id' => 'curriculum_' . $count);
        return $array;
        //        $request->session()->push('session_preference_curriculum', array('curriculumn_1'=>$allValue));

        /*

     */
    }

    public function delete_preference_curriculum(Request $request)
    {
        $data = $request->all();
        $request->session()->forget('session_preference_curriculum.' . $data['id']);
        $obj = TuteeSessionPreference::find($data['id']);
        if ($obj != '') {
            $obj->delete();
        }

    }

    public function filterPieChart(Request $request)
    {

        $post  = $request->all();
        $month = $post['month'];
        $year  = $post['year'];

        $firstday = date('01-' . $month . '-' . $year);
        $lastday  = date(date('t', strtotime($firstday)) . '-' . $month . '-' . $year);

        $startDate = date("Y-m-d", strtotime($firstday));
        $endDate   = date("Y-m-d", strtotime($lastday));

        $sessionData = [
            "front_user_id" => \Session::get('front_user_id'),
        ];


        /*Pie chart code*/
        $accepted_by_tutor_session_count = DB::table('session')
            ->whereIn('status', [1, 2])
            ->where('tutee_id', $sessionData['front_user_id'])
            ->where('scheduled_date_time', '>=', $startDate)
            ->where('scheduled_date_time', '<=', $endDate)
            ->count();
        $session_cancelled_by_tutor_count = DB::table('session')
            ->where('status', 3)
            ->where('tutee_id', $sessionData['front_user_id'])
            ->where('cancelled_date_time', '>=', $startDate)
            ->where('cancelled_date_time', '<=', $endDate)
            ->count();
        $session_cancelled_by_me_count = DB::table('session')
            ->where('status', 4)
            ->where('tutee_id', $sessionData['front_user_id'])
            ->where('cancelled_date_time', '>=', $startDate)
            ->where('cancelled_date_time', '<=', $endDate)
            ->count();

        $user = array();
        $user = ['Session requests accepted by tutor' => $accepted_by_tutor_session_count, "Sessions cancelled by me" => $session_cancelled_by_me_count, "Sessions cancelled by tutor" => $session_cancelled_by_tutor_count];

        $returnHTML = view('frontend.tutee.piechart', ['user' => $user])->render();
        //dd($returnHTML);
        if ($accepted_by_tutor_session_count == 0 && $session_cancelled_by_tutor_count == 0 && $session_cancelled_by_me_count == 0) {
            return response()->json(array('status' => "error", 'html' => $returnHTML));
        }

        return response()->json(array('status' => "success", 'html' => $returnHTML));

    }

    public function mySessions(Request $request)
    {
        if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }
        $sessionData = [
            "front_user_id"          => \Session::get('front_user_id'),
            "front_user_role"        => \Session::get('front_user_role'),
            "front_user_first_name"  => \Session::get('front_user_first_name'),
            "front_user_last_name"   => \Session::get('front_user_last_name'),
            "front_user_username"    => \Session::get('front_user_username'),
            "front_user_email"       => \Session::get('front_user_email'),
            "front_user_country_id"  => \Session::get('front_user_country_id'),
            "front_user_state_id"    => \Session::get('front_user_state_id'),
            "front_user_city_id"     => \Session::get('front_user_city_id'),
            "front_user_nationality" => \Session::get('front_user_nationality'),
            "front_user_lang_id"     => \Session::get('front_user_lang_id'),
            "front_user_photo"       => \Session::get('front_user_photo'),
            "front_user_timezone_id" => \Session::get('front_user_timezone_id'),
             "current_role"           => \Session::get('current_role'),
        ];

        if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 1) {
            return redirect('/tutor-dashboard');
        }

        $notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

        $notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

        $upcommingSessions = Session::where('tutee_id', $sessionData['front_user_id'])->whereIn('status', [1, 2, 3, 4])->orderBy('id', 'desc')->get()->toArray();

        $currentDate = date('Y-m-d');

        $upcomming_color          = '#499f0f';
        $successful_color         = '#ab8b50';
        $cancelled_by_tutee_color = '#f82617';
        $cancelled_by_tutor_color = '#ff8a00';
        $session                  = '';
        if (!empty($upcommingSessions)) {
            foreach ($upcommingSessions as $key => $value) {
                $scheduled_date_time = ConverTimeZoneForUser($value['scheduled_date_time'], 'Y-m-d H:i:s');

                $start_date              = str_replace(" ", 'T', $scheduled_date_time);
                $session[$key]['title']  = $value['objective'];
                $session[$key]['start']  = $start_date;
                $session[$key]['id']     = $value['id'];
                $session[$key]['status'] = $value['status'];
                if ($value['status'] == 2) {
                    $session[$key]['color'] = $upcomming_color;
                } else if ($value['status'] == 1) {
                    $session[$key]['color'] = $successful_color;
                } else if ($value['status'] == 3) {
                    $session[$key]['color'] = $cancelled_by_tutor_color;
                } else if ($value['status'] == 4) {
                    $session[$key]['color'] = $cancelled_by_tutee_color;
                }

            }
        }
        $finalSessionData = json_encode($session, JSON_NUMERIC_CHECK);
        // dd($finalSessionData);

       

        if($request->ajax())
        {
                $returnHTML = view('frontend.tutee.mid_section_my_sessions', 
                    [
                        'sessionData' => $sessionData,
                        'upcommingSessions' => $upcommingSessions,
                        'notifications' => $notifications,
                        'notificationCount' => $notificationCount,
                        'finalSessionData' => $finalSessionData,
                        'currentDate' => $currentDate,   
                        
                    ])->render();
            return response()->json(array('status' => "success",'currentDate' => $currentDate,'finalSessionData' => $finalSessionData,'html' => $returnHTML));
        }else{
             return view('frontend.tutee.my-sessions')
            ->with('sessionData', $sessionData)
            ->with('notifications', $notifications)
            ->with('finalSessionData', $finalSessionData)
            ->with('currentDate', $currentDate)
            ->with('notificationCount', $notificationCount);
        }      

    }

    public function getSessionModalData(Request $request)
    {
        $post       = $request->all();
        $status     = $post['status'];
        $session_id = $post['id'];

        $session = Session::select('session.*', 'front_user.first_name', 'front_user.last_name', 'qualification.qualification_name','tutee_user.first_name as tutee_first_name', 'tutee_user.last_name as tutee_last_name','user_educational.rating as tutor_ratings','user_educational.punctuality as tutor_punctuality')
            ->leftJoin('user_educational', 'session.tutor_id', '=', 'user_educational.front_user_id')
            ->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
            ->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
            ->leftJoin('front_user as tutee_user', 'tutee_user.id', '=', 'session.tutee_id')

            ->where('session.id', $session_id)

            ->where('session.tutee_id', \Session::get('front_user_id'))
            ->where('session.status', $status)
            ->orderBy('session.id', 'desc')
            ->get()->toArray();

        $total_tutor_ratings = "";
        $session['total_tutor_ratings'] = TutorRatings::where('tutor_id', $session[0]['tutor_id'])->count();
       
       //  dd($session);
        
        $returnHTML = "";
        if ($status == 4) {
            $returnHTML = view('frontend.tutee.cancelled_by_tutee_session_modal', ['session' => $session])->render();
        } else if ($status == 1) {
            $returnHTML = view('frontend.tutee.success_session_modal', ['session' => $session])->render();
        } else if ($status == 2) {
            $returnHTML = view('frontend.tutee.upcomming_session_modal', ['session' => $session])->render();
        } else if ($status == 3) {
            $returnHTML = view('frontend.tutee.cancelled_by_tutor_session_modal', ['session' => $session])->render();
        }

        return response()->json(array('status' => $status, 'html' => $returnHTML));

    }

    public function updateShareCount(Request $request)
    {
        $post       = $request->all();
        $session_id = isset($post['session_id']) ? $post['session_id'] : 0;
        $user_id    = \Session::get('front_user_id');

        DB::table('front_user')->where('id', $user_id)->increment('session_share_count');
        DB::table('session')
            ->where('id', $session_id)
            ->limit(1)
            ->update(array('share_by_tutee' => 1));
        echo "success";exit;
    }

    /*
     * session cancel via tutee or tutor
     */
    public function cancelSessionData(Request $request)
    {

        $sessionId     = $request->id;
        $userId        = $request->userId;
        $role          = Frontusers::select('role')->where('id', $userId)->first();
        $now           = Carbon::now()->toDateTimestring();
        $loggedin_role = $request->role;
        if ($role->role == 1) {
            $status = 3;
        } elseif ($role->role == 2) {
            $status = 4;
        } else if ($loggedin_role == 1) {
            $status = 3;
        } else if ($loggedin_role == 2) {
            $status = 4;
        }

        $cancelDateTime = Session::where('id', $sessionId)
            ->update(['cancelled_date_time' => $now,
                'status'                        => $status,
            ]);
        return response('The session cancelled successfully', 200);

    }

    public function reportTutor(ReportTutorRequest $request)
    {
        $sessionId  = $request->id;
        $reportText = $request->report;
        $tuteeId    = \Session::get('front_user_id');
        if ($reportText == "") {
            return response(array('msg' => 'Feedback Field Is Required', 'res' => 201));
        }
        $sessionInfo = Session::where('id', $sessionId)->get()->toArray();

        $tutorId = $sessionInfo[0]['tutor_id'];

        $getRole = Frontusers::select('role')->where('id', $tutorId)->get()->toArray();

        $reported_role = 2;

        $addReport = UserReports::insert([
            'reported_text'    => $reportText,
            'user_id'          => $tuteeId,
            'reported_user_id' => $tutorId,
            'reported_role'    => $reported_role,
        ]);
        /*Notification:Report on Tutor / Tutee submitted by registered users */
        $sessionData = [
            "front_user_first_name" => \Session::get('front_user_first_name'),
            "front_user_last_name"  => \Session::get('front_user_last_name'),
        ];
        $name         = $sessionData['front_user_first_name'] . " " . $sessionData['front_user_last_name'];
        $tutor_data   = Frontusers::select('*')->where('id', $tutorId)->get()->toArray();
        $tutor_name   = $tutor_data['0']['first_name'] . " " . $tutor_data['0']['last_name'];
        $message      = "<b>" . $name . "</b> wrote a report of <b>" . $tutor_name . "</b>";
        $Notification = Notification::insert([
            'from_user_id'         => 0,
            'to_user_id'           => 0,
            'notification_type_id' => 15,
            'is_read'              => 0,
            'is_read_admin'        => 0,
            'is_for_admin'         => 1,
            'created_at'           => Carbon::now()->toDateTimestring(),
            'message'              => $message,
        ]);
        /*Notification end*/

        return response('The Report Submited successfully', 200);
    }

    public function tuteeJoinSession($id)
    {
        $id = decrypt($id);

        if (!check_front_login()) {
            return redirect('/');
        }

        $sessionData = [
            "front_user_id"          => \Session::get('front_user_id'),
            "front_user_role"        => \Session::get('front_user_role'),
            "front_user_first_name"  => \Session::get('front_user_first_name'),
            "front_user_last_name"   => \Session::get('front_user_last_name'),
            "front_user_username"    => \Session::get('front_user_username'),
            "front_user_email"       => \Session::get('front_user_email'),
            "front_user_country_id"  => \Session::get('front_user_country_id'),
            "front_user_state_id"    => \Session::get('front_user_state_id'),
            "front_user_city_id"     => \Session::get('front_user_city_id'),
            "front_user_nationality" => \Session::get('front_user_nationality'),
            "front_user_lang_id"     => \Session::get('front_user_lang_id'),
            "front_user_photo"       => \Session::get('front_user_photo'),
            "front_user_timezone_id" => \Session::get('front_user_timezone_id'),
            "current_role"           => \Session::get('current_role'),
        ];

        if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 1) {
            return redirect('/tutor-dashboard');
        }

        $notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

        $notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

        $data = $this->joinIfRunningForTutee($id);

        if (isset($data['link']) && $data['link'] != '') {

            return redirect($data['link']);die;
        }

        $session_data = Session::select('session.status')
            ->where('session.id', $id)
            ->first()
            ->toArray();

        if ($session_data['status'] == 1) {

           \Session::flash('sessionExpired', 'Your session is expired.');
            // \Session::save();

            return redirect(route('frontend.tuteeDashboard'));

        }

        return view('frontend.tutee.join_session')
            ->with('sessionData', $sessionData)
            ->with('notifications', $notifications)
            ->with('notificationCount', $notificationCount)
            ->with('data', $data);

    }
    public function tuteeEndSession($id)
    {
        $id   = decrypt($id);
        $data = $this->isMeetingRunning($id);

        if (isset($data['link']) && !empty($data['link'])) {
            if ($data['link']['running'][0] == 'false') {
                $end_meeting_data = $this->endMeeting($id);
                //$recording_data = $this->getRecording($id);
                $obj         = Session::find($id);
                $obj->status = 1;
                $obj->save();

                /*Notification:When Session completes */
                $session_data = Session::Select('tutor_session_url', 'tutor_id', 'tutee_id','punctuality_ratings')
                    ->where('id', $id)
                    ->first()->toArray();

                $tutee_data = Frontusers::Select('first_name', 'last_name')
                    ->where('id', $session_data['tutee_id'])
                    ->first()->toArray();

                $tutee_name = $tutee_data['first_name'] . " " . $tutee_data['last_name'];

                $tutor_data = Frontusers::Select('first_name', 'last_name')
                    ->where('id', $session_data['tutor_id'])
                    ->first()->toArray();

                $tutor_name = $tutor_data['first_name'] . " " . $tutor_data['last_name'];

                $message      = "<a href='/tutee-sessions'> Your session with <b>" . $tutor_name . "</b> is completed successfully.</a>";
                $Notification = Notification::insert([
                    'from_user_id'         => $session_data['tutor_id'],
                    'to_user_id'           => $session_data['tutee_id'],
                    'notification_type_id' => 10,
                    'is_read'              => 0,
                    'is_read_admin'        => 0,
                    'is_for_admin'         => 0,
                    'created_at'           => Carbon::now()->toDateTimestring(),
                    'message'              => $message,
                ]);

                $message      = "<a href='/tutor-sessions'>Your session with <b>" . $tutee_name . "</b> is completed successfully.</a>";
                $Notification = Notification::insert([
                    'from_user_id'         => $session_data['tutee_id'],
                    'to_user_id'           => $session_data['tutor_id'],
                    'notification_type_id' => 10,
                    'is_read'              => 0,
                    'is_read_admin'        => 0,
                    'is_for_admin'         => 0,
                    'created_at'           => Carbon::now()->toDateTimestring(),
                    'message'              => $message,
                ]);

                /*Punctuality ratings given by system after session end*/
                $message      = "<a href='/tutor-sessions'>System post punctuality ratings ".$session_data['punctuality_ratings']." out of 10 for Your session with <b>" . $tutee_name . "</b>.</a>";
                $Notification = Notification::insert([
                    'from_user_id'         => $session_data['tutee_id'],
                    'to_user_id'           => $session_data['tutor_id'],
                    'notification_type_id' => 21,
                    'is_read'              => 0,
                    'is_read_admin'        => 0,
                    'is_for_admin'         => 0,
                    'created_at'           => Carbon::now()->toDateTimestring(),
                    'message'              => $message,
                ]);
                /*end*/

                //for admin
                $message      = "<b>" . $tutee_name . "</b> successfully completed session with <b>" . $tutor_name . "</b> ";
                $Notification = Notification::insert([
                    'from_user_id'         => $session_data['tutor_id'],
                    'to_user_id'           => $session_data['tutee_id'],
                    'notification_type_id' => 10,
                    'is_read'              => 1,
                    'is_read_admin'        => 0,
                    'is_for_admin'         => 1,
                    'created_at'           => Carbon::now()->toDateTimestring(),
                    'message'              => $message,
                ]);
                /*Notification end*/
            }

        }

         $current_role = \Session::get('current_role');
        if ($current_role == 1) {
            return redirect(route('frontend.tutorDashboard'));
        } else {
            return redirect(route('frontend.tuteeDashboard'));
        }

    }

    /*
     *    Tutor Rating Given By Tutee
     */
    public function ratingTutor(Request $request)
    {

        $sessionId  = $request->id;
        $getRecord  = Session::select('tutor_id', 'tutee_id','rated_by_tutee')->where('id', $sessionId)->get()->toArray();
        $tutorId    = isset($getRecord[0]['tutor_id']) ? $getRecord[0]['tutor_id'] : "";
        $tuteeId    = isset($getRecord[0]['tutee_id']) ? $getRecord[0]['tutee_id'] : "";
        $rating     = $request->rating;
        $review     = $request->review;
        $modifiedBy = \Session::get('front_user_id');
        
        if(isset($getRecord['0']['rated_by_tutee']) && $getRecord['0']['rated_by_tutee'] == 1){
            return response(array('msg' => 'You have already submitted you ratings', 'res' => 201));
        }

        if($rating == ""){
            return response(array('msg' => 'Rating Field Is Required', 'res' => 201));
        }
        if ($review == "") {
            return response(array('msg' => 'Review Field Is Required', 'res' => 201));
        }
        $TutorRating              = new TutorRatings;
        $TutorRating->tutor_id    = $tutorId;
        $TutorRating->tutee_id    = $tuteeId;
        $TutorRating->rating      = $rating;
        $TutorRating->review      = $review;
        $TutorRating->modified_by = $modifiedBy;
        $record = $TutorRating->save();

        if($record)
        {
            Session::where('id', $sessionId)->update([
                    'ratings_given_by_tutee' => $rating
                ]);
        }

        DB::table('session')
            ->where('id', $sessionId)
            ->limit(1)
            ->update(array('rated_by_tutee' => 1));

        $getTutorRating = TutorRatings::select('rating')->where('tutor_id', $tutorId)->get()->all();
        $sum            = 0;
        foreach ($getTutorRating as $rating) {
            $sum = $sum + $rating->rating;
        }
        if (!empty($sum)) {
            $totalRatings = TutorRatings::select('rating')->where('tutor_id', $tutorId)->count();
            if (!empty($totalRatings)) {
                $avg       = $sum / $totalRatings;
                $avgRating = round($avg);

                DB::table('user_educational')->where('front_user_id', $tutorId)
                    ->limit(1)
                    ->update(array('rating' => $avgRating));
            }
        }
        /*Notification:Tutee post ratings for Tutor after a session ends */
        $sessionData = [
            "front_user_first_name" => \Session::get('front_user_first_name'),
            "front_user_last_name"  => \Session::get('front_user_last_name'),
        ];
        $name         = $sessionData['front_user_first_name'] . " " . $sessionData['front_user_last_name'];
        $message      = "<a href='/tutor-dashboard'> <b>" . $name . "</b> given you session rating</a>";
        $Notification = Notification::insert([
            'from_user_id'         => $getRecord[0]['tutee_id'],
            'to_user_id'           => $getRecord[0]['tutor_id'],
            'notification_type_id' => 20,
            'is_read'              => 0,
            'is_read_admin'        => 0,
            'is_for_admin'         => 0,
            'created_at'           => Carbon::now()->toDateTimestring(),
            'message'              => $message,
        ]);
        /*Notification end*/
        return response('The Rating Submited successfully', 200);
    }

        /*
         * Session Feedback By Tutee
         */
        public function sessionFeedback(Request $request)
        {   
            $sessionId = $request->id;
            $feedbackText = $request->report;
            $byTutee = 1;
            $posted_by = \Session::get('front_user_id');
            
            if ($feedbackText == "") {
                return response(array('msg' =>'Feedback Field Is Required', 'res' => 201));
            }
            
            $sessionInfo = Session::where('id', $sessionId)->get()->toArray();
            $posted_for = $sessionInfo[0]['tutor_id'];
            $session_end_date_time = $sessionInfo[0]['end_date_time'];

            $sessionFeedback  = new SessionFeedback;
            $sessionFeedback->report_text     = $feedbackText;
            $sessionFeedback->posted_by       = $posted_by;
            $sessionFeedback->posted_for      = $posted_for;
            $sessionFeedback->session_id      = $sessionId;
            $sessionFeedback->modified_by     = $posted_by;
            $sessionFeedback->session_end_date_time = $session_end_date_time;
            $addRecord    = $sessionFeedback->save();
            
            if($addRecord)
            {
                Session::where('id', $sessionId)->update([
                        'session_feedback_tutee' => $feedbackText,
                        'feedback_by_tutee' => $byTutee
                    ]);
            }
            
            return response('The Feedback Submited successfully', 200); 
        }

}
