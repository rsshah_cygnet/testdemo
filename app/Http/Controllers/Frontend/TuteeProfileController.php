<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\TuteeProfile\UpdateTuteeProfileRequest;
use App\Http\Requests\Frontend\TuteeProfile\UploadImageRequest;
use App\Http\Utilities\FileUploads;
use App\Models\City\City;
use App\Models\Country\Country;
use App\Models\Curriculum\Curriculum;
use App\Models\EducationalSystem\EducationalSystem;
use App\Models\Frontusers\Frontusers;
use App\Models\Grade\Grade;
use App\Models\Language\Language;
use App\Models\Levels\Levels;
use App\Models\Notification\Notification;
use App\Models\Program\Program;
use App\Models\Qualification\Qualification;
use App\Models\Session\Session;
use App\Models\State\State;
use App\Models\Timezone\Timezone;
use App\Models\UserEducation\UserEducation;
use App\Repositories\Frontend\TuteeProfile\TuteeProfileRepositoryContract;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TuteeProfileController extends Controller {
	/**
	 * @var TuteeProfileRepositoryContract
	 */
	protected $repositorycontract;

	/**
	 * @var filepath
	 */
	public $filePath = "/user";

	/**
	 * @param TuteeProfileRepositoryContract $settings
	 */
	public function __construct(TuteeProfileRepositoryContract $RepositoryContract) {
		$this->repositorycontract = $RepositoryContract;
		$this->uploadManager = new FileUploads;
	}
	/*
		     * View Login use data
	*/
	public function viewProfile(Request $request) {
		if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }

		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			 "current_role" => \Session::get('current_role'),

		];

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		
		$result = $this->repositorycontract->getTuteeProfile($request->all());
		if (!empty($result->lang_id)) {
			$language_id = $result->lang_id;
			$language_id = explode(",", $language_id);

			$lang = Language::select('name')->whereIn('id', $language_id)->get()->toArray();
			// dd($languageName);
			foreach ($lang as $key => $val) {
				// dd($val);
				$languageName[] = " " . $val['name'];
			}
			$result->language = implode(',', $languageName);
		}

		
		if($request->ajax()){
			 $returnHTML = view('frontend.tuteeprofile.mid_section_tuteeprofile', compact('result', 'sessionData', 'notifications', 'notificationCount'))->render();
             return response()->json(array('status' => 'success', 'html' => $returnHTML));
        }else{
        	return view('frontend.tuteeprofile.tuteeprofile', compact('result', 'sessionData', 'notifications', 'notificationCount'));
        }
	}

	public function edit(Request $request) {
		if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }

		$loggedInUserId = \Session::get('front_user_id');
		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			"current_role" => \Session::get('current_role'),
		]; //dd($sessionData);

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();
		
		$timezone = Timezone::getAll();
		$language = Language::getAll();
		$curriculumn = Curriculum::getCurriculumList();
		$qualification = Qualification::getAll();
		
		$result = $this->repositorycontract->getTuteeProfile($request->all());
		$result->lang_id = explode(",", $result->lang_id);

		$country = Country::where('status', 1)->pluck('country_name', 'id')->all();
		$state = $city = array();

		if(isset($result->country_id) && $result->country_id != ""){
			$state = State::where('status', 1)->where('country_id',$result->country_id)->pluck('state_name', 'id')->all();	
		}
		
		if(isset($result->state_id) && $result->state_id != ""){
			$city = City::where('status', 1)->where('state_id',$result->state_id)->pluck('city_name', 'id')->all();
		}
		
		

		if($request->ajax()){
			 $returnHTML = view('frontend.tuteeprofile.mid_section_tuteeprofileupdate',compact('result', 'country', 'timezone', 'qualification', 'state', 'city', 'curriculumn', 'sessionData', 'notifications', 'notificationCount', 'language'))->render();
             return response()->json(array('status' => 'success', 'html' => $returnHTML));
        }else{
        	return view('frontend.tuteeprofile.tuteeprofileupdate', compact('result', 'country', 'timezone', 'qualification', 'state', 'city', 'curriculumn', 'sessionData', 'notifications', 'notificationCount', 'language'));
        }
	}

	public function update(UpdateTuteeProfileRequest $request) {
		if (!check_front_login()) {
			return redirect('/');
		}

		// dd($request->all());
		//dd($data_second);
		$loginTutee = \Session::get('front_user_id');
		$obj = new UserEducation;
		$obj->front_user_id = isset($user_id) ? $user_id : '';
		$obj->qualification_id = isset($request['qualification_id']) ? $request['qualification_id'] : '';
		if (in_array($obj->qualification_id, array('1', '2', '3'))) {
			$obj->curriculum_id = isset($request['curriculum_id']) ? $request['curriculum_id'] : '';
			$obj->education_system_id = isset($request['education_id']) ? $request['education_id'] : '';
			$obj->level_id = isset($request['level_id']) ? $request['level_id'] : '';
			$obj->grade_id = isset($request['grade_id']) ? $request['grade_id'] : '';
			$obj->program_id = isset($request['program_id']) ? $request['program_id'] : '';
			$obj->subject_id = isset($request['subject_id']) ? $request['subject_id'] : '';
			$obj->topic_id = isset($request['topic_id']) ? $request['topic_id'] : '';
			$obj->subject_name = null;
		} else {
			$obj->curriculum_id = null;
			$obj->education_system_id = null;
			$obj->level_id = null;
			$obj->grade_id = null;
			$obj->program_id = null;
			$obj->subject_id = null;
			$obj->topic_id = null;
			$obj->subject_name = isset($request['subject_name']) ? $request['subject_name'] : '';
		}
		$update1 = UserEducation::where('front_user_id', '=', $loginTutee)
			->update(['qualification_id' => $obj->qualification_id,
				'educational_details' => $request->educational_details,
				'curriculum_id' => $obj->curriculum_id,
				'education_system_id' => $obj->education_system_id,
				'level_id' => $obj->level_id,
				'program_id' => $obj->program_id,
				'grade_id' => $obj->grade_id,
				'subject_id' => $obj->subject_id,
				'topic_id' => $obj->topic_id,
				'occupation_details' => $request->Occupation,
				'subject_name' => $obj->subject_name,
				'paypal_registered_email' => $request->paypal_email,
				'modified_by' => $loginTutee,
				'updated_at' => carbon::now()->toDateTimeString(),
			]);

		$dob = date('Y-m-d', strtotime($request->dob));

		$timezone = $request->timezone_id;
		$timezone_data = Timezone::select('timezone_name')->where('id', '=', $timezone)->get()->first();
		$timezone_name = $timezone_data->timezone_name;

		$result = $this->repositorycontract->getTuteeProfile($request->all());
		$img = $result->photo;

		if (empty($request->tutee_image)) {
			$input['photo'] = $img;
		}

		$tutee_image = $this->uploadLogo($request->tutee_image);

		if ($tutee_image) {
			$input = ['photo' => $tutee_image];
		}

		$language_id = implode(",", $request->language_id);

		$update = Frontusers::where('id', '=', $loginTutee)
			->update(['first_name' => $request->first_name,
				'last_name' => $request->last_name,
				'gender' => $request->gender,
				'nationality' => $request->nationality,
				'dob' => $dob,
				'photo' => $request->photo,
				'timezone_id' => $request->timezone_id,
				'lang_id' => $language_id,
				'country_id' => $request->country_id,
				'state_id' => $request->state_id,
				'city_id' => $request->city_id,
				'email' => $request->email,
				'contact_no' => $request->contact_no,
				'updated_at' => carbon::now()->toDateTimeString(),
			]);
		// dd('update');
		$sessionData = [
			\Session::set('front_user_timezone_id', $request->timezone_id),
			\Session::set('front_user_timezone_name', $timezone_name),
		];

		$array = array('status' => 'success');
			return json_encode($array);
		//return redirect()->route("frontend.profile")->withInput($sessionData);
	}

	/**
	 * Upload profile
	 *
	 * @param array $input
	 * @return mixed
	 */
	public function uploadLogo($input = array()) {

		if (isset($input) && !empty($input)) {
			$fileName = $this->uploadManager->setBasePath($this->filePath)
				->setThumbnailFlag(false)
				->upload($input);

			if ($fileName) {
				return $fileName;
			}
		}

		return false;
	}

	/**
	 * Save user photo
	 */
	public function register_user_photo(UploadImageRequest $request) {

		$imageName = "";

		if (isset($request->file) && $request->file('file') != null && $request->file('file') != "") {
			$extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
			$dir = public_path('uploads/user/');
			$filename = uniqid() . '_' . time() . '.' . $extension;
			$request->file('file')->move($dir, $filename);
			echo $filename;

		}
	}
}
