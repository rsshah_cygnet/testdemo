<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\User\UserContract;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class ProfileController extends Controller {

    /**
     * @return mixed
     */
    public function edit() {
        $user = access()->user();

        return view('frontend.user.profile.edit', compact('user'));
    }

    /**
     * update user profile 
     * @param UserContract $user
     * @param UpdateProfileRequest $request
     * @return type
     */
    public function update(UserContract $user, UpdateProfileRequest $request) {
        $user->updateProfile(access()->id(), $request);
        return redirect()->route('frontend.user.dashboard')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }

}
