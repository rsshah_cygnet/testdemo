<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Tutor\ReserveTutorRequest;
use App\Http\Requests\Frontend\Tutor\TutorAvailablityRequest;
use App\Models\Curriculum\Curriculum;
use App\Models\EducationalSystem\EducationalSystem;
use App\Models\Frontusers\Frontusers;
use App\Models\Grade\Grade;
use App\Models\Language\Language;
use App\Models\Levels\Levels;
use App\Models\Notification\Notification;
use App\Models\Program\Program;
use App\Models\Session\Session;
use App\Models\Setting;
use App\Models\Timezone\Timezone;
use App\Models\TutorDetail\TutorDetail;
use App\Models\TutorPreference\TutorPreference;
use App\Models\TutorRatings\TutorRatings;
use App\Repositories\Frontend\Tutor\TutorRepositoryContract;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Models\UserReports\UserReports;
use App\Http\Requests\Frontend\Tutor\ReportTuteeRequest;
use App\Models\UserEducation\UserEducation;
use App\Models\SessionFeedback\SessionFeedback;

class TutorController extends Controller {
	/**
	 * @var TutorRepositoryContract
	 */
	protected $repositorycontract;

	/**
	 * @param TutorRepositoryContract $settings
	 */
	public function __construct(TutorRepositoryContract $RepositoryContract) {
		$this->repositorycontract = $RepositoryContract;
	}

	public function index(Request $request) {
		
		$curriculumn = Curriculum::getCurriculumList();
		$educational = EducationalSystem::getEducationSystemList();
		$level = Levels::getLevelsList();
		$program = Program::getProgramList();
		$grade = Grade::getGradeList();
		$language = Language::getAll();
		$timezone = Timezone::getAll();
		$searchData = '';

		$post = $request->all();
		if (isset($post['search']['timezone']) && $post['search']['timezone'] != '') { 
		$timezone_data = Timezone::select('timezone_name')->where('id', '=', $post['search']['timezone'])->get()->first();		
		$timezone_name = $timezone_data->timezone_name;
			setcookie("front_user_timezone_id",$post['search']['timezone']);
			setcookie("front_user_timezone_name",$timezone_name);
		}

		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			"current_role" => \Session::get('current_role'),
		];

		if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 1) {
			return redirect('/');
		}

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		if (!empty($request['search'])) {
			$sessionData = [
				"front_user_id" => \Session::get('front_user_id'),
				"front_user_role" => \Session::get('front_user_role'),
				"front_user_first_name" => \Session::get('front_user_first_name'),
				"front_user_last_name" => \Session::get('front_user_last_name'),
				"front_user_username" => \Session::get('front_user_username'),
				"front_user_email" => \Session::get('front_user_email'),
				"front_user_country_id" => \Session::get('front_user_country_id'),
				"front_user_state_id" => \Session::get('front_user_state_id'),
				"front_user_city_id" => \Session::get('front_user_city_id'),
				"front_user_nationality" => \Session::get('front_user_nationality'),
				"front_user_lang_id" => \Session::get('front_user_lang_id'),
				"front_user_photo" => \Session::get('front_user_photo'),
				"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
				"current_role" => \Session::get('current_role'),
			];
			$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();
			$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();
			//$searchData = $request['search'];

			$searchData = $request->all();

			

			if($request->ajax())
	        {
	                $returnHTML = view('frontend.tutor.mid_section_findtutor', 
	                    compact('curriculumn', 'educational', 'level', 'program', 'grade', 'language', 'searchData', 'sessionData', 'notificationCount', 'notifications', 'timezone'))->render();
	            return response()->json(array('status' => "success",'html' => $returnHTML));
	        }else{
	            return view('frontend.tutor.findtutor', compact('curriculumn', 'educational', 'level', 'program', 'grade', 'language', 'searchData', 'sessionData', 'notificationCount', 'notifications', 'timezone'));
	        }
		}

		if($request->ajax())
        {
                $returnHTML = view('frontend.tutor.mid_section_findtutor', 
                    compact('curriculumn', 'educational', 'level', 'program', 'grade', 'language', 'sessionData', 'notificationCount', 'notifications', 'timezone'))->render();
            return response()->json(array('status' => "success",'html' => $returnHTML));
        }else{
            return view('frontend.tutor.findtutor', compact('curriculumn', 'educational', 'level', 'program', 'grade', 'language', 'sessionData', 'notificationCount', 'notifications', 'timezone'));
        }
		
	}

	public function getTutorList(Request $request) {
		$json_string = '';
		if (!empty($request['searchhidden'])) {
			$json_string = $request['searchhidden'];
			$data = json_decode($request['searchhidden'], true);

			// $final_array['page'] =$request['page'];
			// $final_array['search'] = $data;

			$result = $this->repositorycontract->getTutorList($data);
		} else {
			$result = $this->repositorycontract->getTutorList($request->all());

		}
		$html = view('frontend.includes.tutorresult', compact('result', 'json_string'))->render();

		return $html;

	}

	public function searchTutor(Request $request) {
		$json_string = '';
		$post = $request->all();
		if (isset($post['search']['timezone']) && $post['search']['timezone'] != '') { 
		$timezone_data = Timezone::select('timezone_name')->where('id', '=', $post['search']['timezone'])->get()->first();		
		$timezone_name = $timezone_data->timezone_name;
			setcookie("front_user_timezone_id",$post['search']['timezone']);
			setcookie("front_user_timezone_name",$timezone_name);
		}
		$result = $this->repositorycontract->getTutorList($request->all());
		return view('frontend.includes.tutorresult', compact('result', 'json_string'));
	}

	public function tutorDetails($id,Request $request) {

		unset($_COOKIE['tutor_details_url']);
		setcookie('tutor_details_url', '', time() - 3600, '/');

		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			"current_role" => \Session::get('current_role'),
		];

		if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 1) {
			return redirect('/');
		}

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		// $strDateFrom = '2017-05-05';
		// $strDateTo = '2017-05-25';
		$available_dates = $available_dates_specific = [];
		$avalablity_every_day = TutorPreference::where('reference', 0)->where('tutor_id', $id)->where('deleted_at',null)->get()->toArray();
		$avalablity_specific_day = TutorPreference::where('reference', 1)->where('tutor_id',$id)->where('deleted_at',null)->get()->toArray();

		if(!empty($avalablity_every_day)){
			foreach ($avalablity_every_day as $key => $value) {

				$timefrom = $value['start_time'];
	            $timeto   = $value['end_time'];

	            $start_date = $value['start_date'];
                $end_date   = $value['end_date'];
				
				$from_date_final = ConverTimeZoneForUser($start_date . " " . $timefrom, 'Y-m-d H:i');
	            $to_date_final   = ConverTimeZoneForUser($end_date . " " .$timeto, 'Y-m-d H:i');
	            
	            $from_date  = convertDateInSqlFormat($from_date_final);
                $to_date    = convertDateInSqlFormat($to_date_final);

				$available_dates[$key] = createDateRangeArray($from_date,$to_date);
			}
			
		}
		if(!empty($avalablity_specific_day)){
			foreach ($avalablity_specific_day as $key => $value) {

				$timefrom = $value['start_time'];
	            $timeto   = $value['end_time'];

	            $start_date = $value['start_date'];
                $end_date   = $value['end_date'];
				
				$from_date_final = ConverTimeZoneForUser($start_date . " " . $timefrom, 'Y-m-d H:i');
	            $to_date_final   = ConverTimeZoneForUser($end_date . " " .$timeto, 'Y-m-d H:i');
	            
	            $from_date  = convertDateInSqlFormat($from_date_final);
                $to_date    = convertDateInSqlFormat($to_date_final);

				$available_dates_specific[$key] = createDateRangeArray($from_date,$to_date);
			}
			
		}
		$final_dates_multi_array = array_merge($available_dates_specific,$available_dates);
		$dates = array_flatten($final_dates_multi_array);
		

		$curriculumn = Curriculum::getCurriculumList();

		$result = Frontusers::select('front_user.id as userId', 'front_user.*', 'user_educational.*', 'language.name as language', 'country.*', 'subject.*', 'qualification.*', DB::raw('GROUP_CONCAT(DISTINCT subject.subject_name) as subjects', 'tutor_details.*'))
			->leftJoin('country', 'front_user.country_id', '=', 'country.id')
			->leftJoin('language', 'front_user.lang_id', '=', 'language.id')
			->leftJoin('user_educational', 'user_educational.front_user_id', '=', 'front_user.id')
			->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
			->leftjoin('subject', 'subject.id', '=', 'user_educational.subject_id')
			->leftjoin('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
			->whereIn('front_user.role', [1,3])
			->where('front_user.status', '=', 1)
			->where('front_user.id', '=', $id)
			->groupBy('front_user.id')
			->first();
		
		$hierarchy = TutorDetail::with('hierachyName')->where('front_user_id', $id)->where('tutor_details.passed_status',1)->get()->toArray();

		$passed_curri = [];
		if(!empty($hierarchy)){
			foreach ($hierarchy as $key => $value) {
				$passed_curri[$key] =  $value['curriculum_id'];
			}
		}
		$curriculumn_data =  Curriculum::whereIn('id',$passed_curri)->pluck('curriculum_name','id')->all();
		if(empty($curriculumn_data)){
			$curriculumn = Curriculum::getCurriculumList();
		}else{
			$curriculumn = $curriculumn_data;
		}

		$total = TutorRatings::where('tutor_id', $id)->count();

		$ratings = TutorRatings::with('tuteeName')->where('tutor_id', $id)->offset(0)->limit(10)->orderBy('id', 'asc')->get()->toArray();
		$front_user_id = \Session::get('front_user_id');
		if (empty($result)) {
			return "You Have Dont permission to access this page";
		}

		

		if($request->ajax())
        {
                $returnHTML = view('frontend.tutor.mid_section_viewtutordetails', 
                    compact('dates','result', 'ratings', 'total', 'hierarchy', 'front_user_id', 'curriculumn','sessionData', 'notificationCount', 'notifications'))->render();
            return response()->json(array('status' => "success",'html' => $returnHTML));
        }else{
            return view('frontend.tutor.viewtutordetails', compact('dates','result', 'ratings', 'total', 'hierarchy', 'front_user_id', 'curriculumn','sessionData', 'notificationCount', 'notifications'));
        }
	}

	public function getReview(Request $request) {
		$limit = $request->hidden_limit;
		$id = $request->tutorid;

		$ratings = TutorRatings::with('tuteeName')->where('tutor_id', $id)->orderBy('id', 'asc')->offset($limit)->limit(10)->get()->toArray();
		$total = TutorRatings::where('tutor_id', $id)->count();

		return view('frontend.includes.viewreview', compact('ratings', 'total'))->render();

	}

	public function dashboard(Request $request) {
		if(!$request->ajax())
        {
        	if (!check_front_login()) {
				return redirect('/');
			}
	
        }
		
		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			"current_role" => \Session::get('current_role'),
		]; //dd($sessionData['front_user_role']);
		\Session::set('current_role','1');
		if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 2) {
			return redirect('/tutee-dashboard');
		}

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->where('is_for_admin', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->where('is_for_admin', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		$upcommingSessions = Session::select('session.*', 'front_user.first_name', 'front_user.last_name')->leftJoin('front_user', 'session.tutee_id', '=', 'front_user.id')->where('session.tutor_id', $sessionData['front_user_id'])->where('session.status', 2)->orderBy('session.id', 'desc')->limit(3)->get()->toArray();

		$total_tutor_ratings = "";
		$total_tutor_ratings = TutorRatings::where('tutor_id', $sessionData['front_user_id'])->count();
		$punctuality_rating = "";
		/*
		$sum_tutor_ratings = TutorRatings::select(DB::raw('SUM(rating) as total_ratings'))
			->where('tutor_id', $sessionData['front_user_id'])
			->get()->toArray();

		$avg_tutor_ratings = 0;

		if (isset($sum_tutor_ratings[0]['total_ratings']) && $sum_tutor_ratings[0]['total_ratings'] != "" && $total_tutor_ratings != 0) {
			$avg_tutor_ratings = $sum_tutor_ratings[0]['total_ratings'] / $total_tutor_ratings;
		}*/

		$user_ratings = UserEducation::select('rating','punctuality')
			->where('front_user_id', $sessionData['front_user_id'])
			->first();
		$avg_tutor_ratings = $avg_tutor_ratings = "";		
		if(!empty($user_ratings)){
			$avg_tutor_ratings = $user_ratings->rating;
			$punctuality_rating = $user_ratings->punctuality;
		}	
			
		$tutor_ratings_five = TutorRatings::where('tutor_id', $sessionData['front_user_id'])->where('rating', '5')->count();
		$tutor_ratings_four = TutorRatings::where('tutor_id', $sessionData['front_user_id'])->where('rating', '4')->count();
		$tutor_ratings_three = TutorRatings::where('tutor_id', $sessionData['front_user_id'])->where('rating', '3')->count();
		$tutor_ratings_two = TutorRatings::where('tutor_id', $sessionData['front_user_id'])->where('rating', '2')->count();
		$tutor_ratings_one = TutorRatings::where('tutor_id', $sessionData['front_user_id'])->where('rating', '1')->count();

		$tutor_ratings_five_percentage = $tutor_ratings_four_percentage = $tutor_ratings_three_percentage = $tutor_ratings_two_percentage = $tutor_ratings_one_percentage = 0;
		if ($total_tutor_ratings != "" && $total_tutor_ratings != 0) {
			$tutor_ratings_five_percentage = (100 * $tutor_ratings_five) / $total_tutor_ratings;
			$tutor_ratings_four_percentage = (100 * $tutor_ratings_four) / $total_tutor_ratings;
			$tutor_ratings_three_percentage = (100 * $tutor_ratings_three) / $total_tutor_ratings;
			$tutor_ratings_two_percentage = (100 * $tutor_ratings_two) / $total_tutor_ratings;
			$tutor_ratings_one_percentage = (100 * $tutor_ratings_one) / $total_tutor_ratings;
		}

		/*$punctuality_rating = DB::table('session')
			->where('tutor_id', $sessionData['front_user_id'])
			->avg('punctuality_ratings');

		$punctuality_rating = number_format((float) $punctuality_rating, 2, '.', '');*/

		$accepted_by_me_session_count = DB::table('session')
			->whereIn('status', [1, 2])
			->where('tutor_id',$sessionData['front_user_id'])
			->count();
		$session_cancelled_by_tutee_count = DB::table('session')
			->where('status', 4)
			->where('tutor_id',$sessionData['front_user_id'])
			->count();
		$session_cancelled_by_me_count = DB::table('session')
			->where('status', 3)
			->where('tutor_id',$sessionData['front_user_id'])
			->count();

		$user = array();
		$user = ['Session requests accepted by me' => $accepted_by_me_session_count, "Sessions cancelled by me" => $session_cancelled_by_me_count, "Sessions cancelled by tutee" => $session_cancelled_by_tutee_count];

		/*Column chart data*/
		$current_year = date("Y");
		$punctuality_rating_january = $this->getPunctualityRatingsMonthwise($sessionData['front_user_id'], 01, $current_year);
		$punctuality_rating_feb = $this->getPunctualityRatingsMonthwise($sessionData['front_user_id'], 02, $current_year);
		$punctuality_rating_march = $this->getPunctualityRatingsMonthwise($sessionData['front_user_id'], 03, $current_year);
		$punctuality_rating_april = $this->getPunctualityRatingsMonthwise($sessionData['front_user_id'], 04, $current_year);

		$tutee_rating_january = $this->getTuteeRatingsMonthwise($sessionData['front_user_id'], 01, $current_year);
		$tutee_rating_feb = $this->getTuteeRatingsMonthwise($sessionData['front_user_id'], 02, $current_year);
		$tutee_rating_march = $this->getTuteeRatingsMonthwise($sessionData['front_user_id'], 03, $current_year);
		$tutee_rating_april = $this->getTuteeRatingsMonthwise($sessionData['front_user_id'], 04, $current_year);

		$columnChartArray = [
			['Month', 'Average Ratings(Out of 5)', 'Punctuality Ratings by system(Out of 10)'],
			['January', $tutee_rating_january, $punctuality_rating_january],
			['February', $tutee_rating_feb, $punctuality_rating_feb],
			['March', $tutee_rating_march, $punctuality_rating_march],
			['April', $tutee_rating_april, $punctuality_rating_april],
		];

		$columnChartArray = json_encode($columnChartArray, JSON_NUMERIC_CHECK);

		$tutor_overall_ratings = [
			"avg_tutor_ratings" => $avg_tutor_ratings,
			"total_tutor_ratings" => $total_tutor_ratings,
			"tutor_ratings_five" => $tutor_ratings_five,
			"tutor_ratings_four" => $tutor_ratings_four,
			"tutor_ratings_three" => $tutor_ratings_three,
			"tutor_ratings_two" => $tutor_ratings_two,
			"tutor_ratings_one" => $tutor_ratings_one,
			"tutor_ratings_five_percentage" => $tutor_ratings_five_percentage,
			"tutor_ratings_four_percentage" => $tutor_ratings_four_percentage,
			"tutor_ratings_three_percentage" => $tutor_ratings_three_percentage,
			"tutor_ratings_two_percentage" => $tutor_ratings_two_percentage,
			"tutor_ratings_one_percentage" => $tutor_ratings_one_percentage,
			"punctuality_rating" => $punctuality_rating,
		];
		// dd($upcommingSessions);
		//$date = getOnlyDate();
		

    	if($request->ajax())
        {
                $returnHTML = view('frontend.tutor.mid_section_dashboard', 
                	[
                		'sessionData' => $sessionData,
                		'user' => $user,
                		'notifications' => $notifications,
                		'notificationCount' => $notificationCount,
                		'upcommingSessions' => $upcommingSessions, 
                		'tutor_overall_ratings' => $tutor_overall_ratings,
                		'columnChartArray' => $columnChartArray,
                		
                	])->render();
            return response()->json(array('status' => "success",'notificationCount' => $notificationCount,'user' => $user,'columnChartArray' => $columnChartArray,'html' => $returnHTML));
        }else{
            return view('frontend.tutor.dashboard')
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('upcommingSessions', $upcommingSessions)
			->with('tutor_overall_ratings', $tutor_overall_ratings)
			->with('user', $user)
			->with('columnChartArray', $columnChartArray)
			->with('notificationCount', $notificationCount);
		}


	}

	public function filterPieChart(Request $request) {

		$post = $request->all();
		$month = $post['month'];
		$year = $post['year'];

		$firstday = date('01-' . $month . '-' . $year);
		$lastday = date(date('t', strtotime($firstday)) . '-' . $month . '-' . $year);

		$startDate = date("Y-m-d", strtotime($firstday));
		$endDate = date("Y-m-d", strtotime($lastday));

		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
		];

		/*Pie chart code*/
		$accepted_by_me_session_count = DB::table('session')
			->whereIn('status', [1, 2])
			->where('tutor_id', $sessionData['front_user_id'])
			->where('scheduled_date_time', '>=', $startDate)
			->where('scheduled_date_time', '<=', $endDate)
			->count();
		$session_cancelled_by_tutee_count = DB::table('session')
			->where('status', 4)
			->where('tutor_id', $sessionData['front_user_id'])
			->where('cancelled_date_time', '>=', $startDate)
			->where('cancelled_date_time', '<=', $endDate)
			->count();
		$session_cancelled_by_me_count = DB::table('session')
			->where('status', 3)
			->where('tutor_id', $sessionData['front_user_id'])
			->where('cancelled_date_time', '>=', $startDate)
			->where('cancelled_date_time', '<=', $endDate)
			->count();

		$user = array();
		$columnChartArray = array();
		$user = ['Session requests accepted by me' => $accepted_by_me_session_count, "Sessions cancelled by me" => $session_cancelled_by_me_count, "Sessions cancelled by tutee" => $session_cancelled_by_tutee_count];

		$returnHTML = view('frontend.tutor.piechart', ['user' => $user])->render();
		//dd($returnHTML);
		if ($accepted_by_me_session_count == 0 && $session_cancelled_by_me_count == 0 && $session_cancelled_by_tutee_count == 0) {
			return response()->json(array('status' => "error", 'html' => $returnHTML));
		}

		return response()->json(array('status' => "success", 'html' => $returnHTML));

	}

	public function getPunctualityRatingsMonthwise($tutor_id, $month, $year) {

		$firstday = date('01-' . $month . '-' . $year);
		$lastday = date(date('t', strtotime($firstday)) . '-' . $month . '-' . $year);

		$startDate = date("Y-m-d", strtotime($firstday));
		$endDate = date("Y-m-d", strtotime($lastday));

		$punctuality_rating_month_wise = DB::table('session')
			->where('tutor_id', $tutor_id)
			->whereIn('status',array('1','3'))
			->where('scheduled_date_time', '>=', $startDate)
			->where('scheduled_date_time', '<=', $endDate)
			->avg('punctuality_ratings');

		$punctuality_rating = number_format($punctuality_rating_month_wise, 1);
		/* echo $punctuality_rating;
			        dd($punctuality_rating);
		*/
		return $punctuality_rating;
	}

	public function getTuteeRatingsMonthwise($tutor_id, $month, $year) {

		$firstday = date('01-' . $month . '-' . $year);
		$lastday = date(date('t', strtotime($firstday)) . '-' . $month . '-' . $year);

		$startDate = date("Y-m-d", strtotime($firstday));
		$endDate = date("Y-m-d", strtotime($lastday));

		$tutee_rating_month_wise = DB::table('tutor_ratings')
			->where('tutor_id', $tutor_id)
			->where('created_at', '>=', $startDate)
			->where('created_at', '<=', $endDate)
			->avg('rating');

		$tutee_rating_month_wise = number_format((float) $tutee_rating_month_wise, 1);
		return $tutee_rating_month_wise;
	}

	public function filterColumnChart(Request $request) {

		$post = $request->all();
		$quarter = $post['quarter'];
		$current_year = $post['year'];
		$tutor_id = \Session::get('front_user_id');

		if (isset($quarter) && $quarter != "") {
			if ($quarter == 2) {
				$month1 = '05';
				$month2 = '06';
				$month3 = '07';
				$month4 = '08';

				$month_name1 = "May";
				$month_name2 = "June";
				$month_name3 = "July";
				$month_name4 = "August";
			} else if ($quarter == 3) {
				$month1 = '09';
				$month2 = '10';
				$month3 = '11';
				$month4 = '12';
				$month_name1 = "September";
				$month_name2 = "October";
				$month_name3 = "November";
				$month_name4 = "December";
			} else {
				$month1 = '01';
				$month2 = '02';
				$month3 = '03';
				$month4 = '04';

				$month_name1 = "January";
				$month_name2 = "February";
				$month_name3 = "March";
				$month_name4 = "April";
			}
		}

		/*Column chart data*/

		$punctuality_rating_january = $this->getPunctualityRatingsMonthwise($tutor_id, $month1, $current_year);
		$punctuality_rating_feb = $this->getPunctualityRatingsMonthwise($tutor_id, $month2, $current_year);
		$punctuality_rating_march = $this->getPunctualityRatingsMonthwise($tutor_id, $month3, $current_year);
		$punctuality_rating_april = $this->getPunctualityRatingsMonthwise($tutor_id, $month4, $current_year);

		$tutee_rating_january = $this->getTuteeRatingsMonthwise($tutor_id, $month1, $current_year);
		$tutee_rating_feb = $this->getTuteeRatingsMonthwise($tutor_id, $month2, $current_year);
		$tutee_rating_march = $this->getTuteeRatingsMonthwise($tutor_id, $month3, $current_year);
		$tutee_rating_april = $this->getTuteeRatingsMonthwise($tutor_id, $month4, $current_year);

		$columnChartArray = [
			['Month', 'Average Ratings(Out of 5)', 'Punctuality Ratings by system(Out of 10)'],
			[$month_name1, $tutee_rating_january, $punctuality_rating_january],
			[$month_name2, $tutee_rating_feb, $punctuality_rating_feb],
			[$month_name3, $tutee_rating_march, $punctuality_rating_march],
			[$month_name4, $tutee_rating_april, $punctuality_rating_april],
		];

		$columnChartArray = json_encode($columnChartArray, JSON_NUMERIC_CHECK);

		//dd($columnChartArray);
		$returnHTML = view('frontend.tutor.column-chart', ['columnChartArray' => $columnChartArray])->render();

		return response()->json(array('status' => "success", 'html' => $returnHTML));

	}

	public function my_avaliablity(Request $request) {
		 if(!$request->ajax()){
			if (!check_front_login()) {
				return redirect('/');
			} 	
		 }
		

		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			"current_role"           => \Session::get('current_role'),
		]; //dd($sessionData['front_user_role']);

		if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 2) {
			//    return redirect('/tutee-dashboard');
		}

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		$avalablity_every_day = TutorPreference::where('reference', 0)->where('tutor_id', $sessionData['front_user_id'])->get()->toArray();

		$avalablity_every_day = json_decode(json_encode($avalablity_every_day), true);

		$avalablity_specific_day = TutorPreference::where('reference', 1)->where('tutor_id', $sessionData['front_user_id'])->get()->toArray();

		$avalablity_specific_day = json_decode(json_encode($avalablity_specific_day), true);

        $pass_exam_count = TutorDetail::where('front_user_id', $sessionData['front_user_id'])->where('passed_status', 1)->count();

        $front_user_data = Frontusers::where('id', $sessionData['front_user_id'])->where('status', 1)->get()->first()->toArray();
        
        //echo "<pre/>";print_r($front_user_data);die;

		// $save_available_specific_day = DB::select(DB::raw("(SELECT t.start_date,GROUP_CONCAT(DISTINCT CONCAT(tt.start_time,'-',tt.end_time)) as 'time' FROM tutor_preference AS t INNER JOIN tutor_preference AS tt ON t.start_date = tt.start_date where t.`reference` = '1' AND t.`deleted_at` is null AND t.`tutor_id` = ".$sessionData['front_user_id']." GROUP BY t.start_date)"));

		//print_r($save_available_specific_day);die;

		//tutor availablity range in months
		$tutor_availability_range_data = object_to_array(DB::table('settings')
				->select('tutor_availability_range')
				->first());
		$tutor_availability_range =  $tutor_availability_range_data['tutor_availability_range'];
		
		if(isset($tutor_availability_range) && $tutor_availability_range == 0 && $tutor_availability_range != null)
			$tutor_availability_range = 3; //by default


		$tutor_availability_range_data['start_date'] = $front_user_data['created_at'];
		$tutor_availability_range_data['month'] = $tutor_availability_range;

		
		 if($request->ajax()){

		 	 $returnHTML =  view('frontend.tutor.mid_section_my_availablity')
			->with('tutor_availability_range', $tutor_availability_range_data)
			->with('pass_exam_count', $pass_exam_count)
			->with('avalablity_specific_day', $avalablity_specific_day)
			->with('avalablity_every_day', $avalablity_every_day)
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('notificationCount', $notificationCount)->render();
             return response()->json(array('status' => 'success','html' => $returnHTML));
        }else{
        	return view('frontend.tutor.my-availablity')->with('sessionData', $sessionData)
			->with('notifications', $notifications)->with('notificationCount', $notificationCount)
			->with('avalablity_every_day', $avalablity_every_day)
            ->with('pass_exam_count',$pass_exam_count)
            ->with('tutor_availability_range',$tutor_availability_range_data)    
			->with('avalablity_specific_day', $avalablity_specific_day);

        }	
	}

	public function save_available_everyday(TutorAvailablityRequest $request) {

		$from_date_time = ConverTimeZoneForDB($request['from_date'] . " " . ConverTimeIn24Hour($request['from_time']), 'Y-m-d H:i');
		$to_date_time = ConverTimeZoneForDB($request['to_date'] . " " . ConverTimeIn24Hour($request['to_time']), 'Y-m-d H:i');

		$from_date = convertDateInSqlFormat($from_date_time);
		$to_date = convertDateInSqlFormat($to_date_time);
		$start_time = ConverTimeIn24Hour($from_date_time);
		$end_time = ConverTimeIn24Hour($to_date_time);

		$existing_record = TutorPreference::where('start_date', $from_date)->where('end_date', $to_date)->where('start_time', $start_time)->where('end_time', $end_time)->where('tutor_id', \Session::get('front_user_id'))->where('reference', $request['reference'])->get()->toArray();
		if (!empty($existing_record[0])) {
			$array = array('error' => 'This Slot is already exists');
			return json_encode($array);
		}

		if ($request['reference'] == 1) {

			// $from_date_time = ConverTimeZoneForDB(convertDateInSqlFormat($request['from_date']) . " " . ConverTimeIn24Hour($request['from_time']),'H:i');

			// $to_date_time = ConverTimeZoneForDB(convertDateInSqlFormat($request['to_date']) . " " . ConverTimeIn24Hour($request['to_time']),'H:i');

			$scheduled_session = Session::where('tutor_id', \Session::get('front_user_id'));

			$scheduled_session->Where(function ($scheduled_session) use ($from_date_time, $to_date_time) {
				$scheduled_session->where('scheduled_date_time', '<=', $from_date_time)
					->where('end_date_time', '>=', $to_date_time);});

			$scheduled_session->orWhere(function ($scheduled_session) use ($from_date_time, $to_date_time) {
				$scheduled_session->where('scheduled_date_time', '<=', $from_date_time)
					->where('end_date_time', '>=', $from_date_time);});

			$scheduled_session->orWhere(function ($scheduled_session) use ($from_date_time, $to_date_time) {
				$scheduled_session->where('scheduled_date_time', '<=', $to_date_time)
					->where('end_date_time', '>=', $to_date_time);});

			$scheduled_session->where('status', 2);
			$scheduled_session_result = $scheduled_session->get()->toArray();
			if(empty($scheduled_session_result)){
				$scheduled_session = Session::where('tutee_id', \Session::get('front_user_id'));

				$scheduled_session->Where(function ($scheduled_session) use ($from_date_time, $to_date_time) {
					$scheduled_session->where('scheduled_date_time', '<=', $from_date_time)
						->where('end_date_time', '>=', $to_date_time);});

				$scheduled_session->orWhere(function ($scheduled_session) use ($from_date_time, $to_date_time) {
					$scheduled_session->where('scheduled_date_time', '<=', $from_date_time)
						->where('end_date_time', '>=', $from_date_time);});

				$scheduled_session->orWhere(function ($scheduled_session) use ($from_date_time, $to_date_time) {
					$scheduled_session->where('scheduled_date_time', '<=', $to_date_time)
						->where('end_date_time', '>=', $to_date_time);});

				$scheduled_session->where('status', 2);
				$scheduled_session_result = $scheduled_session->get()->toArray();
				$error = "This Slot is already reserved as a tutor by you.";
			}else{
				$error = "This Slot is already reserved as a tutee by you.";
			}
			//print_r($scheduled_session_result);die;

			if (!empty($scheduled_session_result)) {
				$array = array('error' => $error);
				return json_encode($array);
			}

		}

		$obj = new TutorPreference;

		$obj->tutor_id = \Session::get('front_user_id');
		$obj->start_time = isset($request['from_time']) && $request['from_time'] != '' ? $start_time : null;
		$obj->end_time = isset($request['to_time']) && $request['to_time'] != '' ? $end_time : null;
		$obj->start_date = isset($request['from_date']) && $request['from_date'] != '' ? $from_date : null;
		$obj->end_date = isset($request['to_date']) && $request['to_date'] != '' ? $to_date : null;
		$obj->reference = isset($request['reference']) && $request['reference'] != '' ? $request['reference'] : 0;
		if ($obj->save()) {
			$array = array('id' => $obj->id);
			return json_encode($array);
		}

	}

	public function deleteTutorAvailablity(Request $request) {
		$data = $request->all();
		$obj = TutorPreference::find($data['id']);
		$obj->delete();
	}

	public function mySessions(Request $request) {
		if(!$request->ajax())
        {
        	if (!check_front_login()) {
				return redirect('/');
			}
        }
		
		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			"current_role"           => \Session::get('current_role'),
		];

		if (isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 2) {
			return redirect('/tutee-dashboard');
		}

		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

		$upcommingSessions = Session::where('tutor_id', $sessionData['front_user_id'])->whereIn('status',[1,2,3,4])->orderBy('id', 'desc')->get()->toArray();

		$currentDate = date('Y-m-d');

		$upcomming_color = '#499f0f';
		$successful_color = '#ab8b50';
		$cancelled_by_tutee_color = '#f82617';
		$cancelled_by_tutor_color = '#ff8a00';
		$session = array();
		if (!empty($upcommingSessions)) {
			foreach ($upcommingSessions as $key => $value) {
				  $scheduled_date_time = ConverTimeZoneForUser($value['scheduled_date_time'], 'Y-m-d H:i:s');

                $start_date = str_replace(" ", 'T', $scheduled_date_time);

				$session[$key]['title'] = $value['objective'];
				$session[$key]['start'] = $start_date;
				$session[$key]['id'] = $value['id'];
				$session[$key]['status'] = $value['status'];
				if ($value['status'] == 2) {
					$session[$key]['color'] = $upcomming_color;
				} else if ($value['status'] == 1) {
					$session[$key]['color'] = $successful_color;
				} else if ($value['status'] == 3) {
					$session[$key]['color'] = $cancelled_by_tutor_color;
				} else if ($value['status'] == 4) {
					$session[$key]['color'] = $cancelled_by_tutee_color;
				}

			}
		}
		$finalSessionData = json_encode($session, JSON_NUMERIC_CHECK);
		// dd($finalSessionData);

		if($request->ajax())
        {
                $returnHTML = view('frontend.tutor.mid_section_my_sessions', 
                    [
                        'sessionData' => $sessionData,
                        'notifications' => $notifications,
                        'notificationCount' => $notificationCount,
                        'finalSessionData' => $finalSessionData,
                        'currentDate' => $currentDate,   
                        
                    ])->render();
            return response()->json(array('status' => "success",'currentDate' => $currentDate,'finalSessionData' => $finalSessionData,'html' => $returnHTML));
        }else{
            return view('frontend.tutor.my-sessions')
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('finalSessionData', $finalSessionData)
			->with('currentDate', $currentDate)
			->with('notificationCount', $notificationCount);
        }    

	}

	public function getSessionModalData(Request $request) {
		$post = $request->all();
		$status = $post['status'];
		$session_id = $post['id'];

		$session = Session::select('session.*', 'front_user.first_name', 'front_user.last_name', 'qualification.qualification_name', 'user_educational.rating', 'user_educational.rating', 'user_educational.punctuality','ue.punctuality as punctual','tutee_user.first_name as tutee_first_name', 'tutee_user.last_name as tutee_last_name')
			->leftJoin('user_educational', 'session.tutee_id', '=', 'user_educational.front_user_id')
			->leftJoin('user_educational as ue', 'session.tutor_id', '=', 'ue.front_user_id')
			->leftJoin('qualification', 'qualification.id', '=', 'user_educational.qualification_id')
			->leftJoin('front_user', 'front_user.id', '=', 'session.tutee_id')
		    ->leftJoin('front_user as tutee_user', 'tutee_user.id', '=', 'session.tutee_id')
			->where('session.id', $session_id)
			->where('session.tutor_id', \Session::get('front_user_id'))
			->where('session.status', $status)
			->orderBy('session.id', 'desc')
			->get()->toArray();
//dd($session);
		$returnHTML = "";
		if ($status == 4) {
			$returnHTML = view('frontend.tutor.cancelled_by_tutee_session_modal', ['session' => $session])->render();
		} else if ($status == 1) {
			$returnHTML = view('frontend.tutor.success_session_modal', ['session' => $session])->render();
		} else if ($status == 2) {
			$returnHTML = view('frontend.tutor.upcomming_session_modal', ['session' => $session])->render();
		} else if ($status == 3) {
			$returnHTML = view('frontend.tutor.cancelled_by_tutor_session_modal', ['session' => $session])->render();
		}

		return response()->json(array('status' => $status, 'html' => $returnHTML));

	}

	public function updateShareCount(Request $request) {
		$post = $request->all();
		$session_id = isset($post['session_id'])?$post['session_id']:0;
		$user_id = \Session::get('front_user_id');

		DB::table('front_user')->where('id', $user_id)->increment('session_share_count');
		DB::table('session')
        ->where('id', $session_id)
        ->limit(1)
        ->update(array('share_by_tutor' => 1));
        echo "success";exit;
	}

	/*
		     * session cancel via tutee or tutor
	*/
	public function cancelSessionData(Request $request) {

		$sessionId = $request->id;
		$userId = $request->userId;
		$loggedin_role = $request->role;
		$role = Frontusers::select('role')->where('id', $userId)->first();
		$now = Carbon::now()->toDateTimestring();

		$sessionDetail = Session::find($sessionId);
        if (!empty($sessionDetail)) {
            $id     = \Session::get('front_user_id');
            $crnt_date    = new \DateTime;
            $current_date = $crnt_date->format('Y-m-d H:i:s');
            if (strtotime($sessionDetail->scheduled_date_time) < strtotime($current_date)) {
            	if(\Session::get('current_role') == 1){
                	$update_data = DB::statement("UPDATE `session` SET `status` = '9' where `scheduled_date_time` <= '" . $current_date . "' AND `status` = '2' AND `tutor_id` = '" . $id . "' AND `id` = " . $sessionId); //7 mns none of join, 2 mns
            	}else{
            		$update_data = DB::statement("UPDATE `session` SET `status` = '9' where `scheduled_date_time` <= '" . $current_date . "' AND `status` = '2' AND `tutee_id` = '" . $id . "' AND `id` = " . $sessionId); //7 mns none of join, 2 mns
            	}
                \Session::flash('flash_danger', "You can't cancel request which scheduled time has gone.");
                return response("You can't cancel request which scheduled time has gone.", 200);
            }
        }
		if ($role->role == 1) {
			$status = 3;
		} elseif ($role->role == 2) {
			$status = 4;
		}else if($loggedin_role == 1){
			$status = 3;
		}else if($loggedin_role == 2){
			$status = 4;
		}

		$sessionData = [
            "front_user_first_name" 	=> \Session::get('front_user_first_name'),
            "front_user_last_name" 		=> \Session::get('front_user_last_name'),
            "front_user_current_role"	=> \Session::get('current_role'),
            "front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
          ];


			$data = Session::find($sessionId);
            $subject_id        = $data->subject_id;
            $topic_id        = $data->topic_id;
            $tutee_id       = $data->tutee_id;
            $tutor_id       = $data->tutor_id;
            $from_date_time = $data->scheduled_date_time;

            /*penalty amount count*/
			$penalty_amount = $refund_amount = $admin_commission = 0;
			$session_data = Session::select('scheduled_date_time','tutor_id','session_fee','penalty_amount','punctuality_ratings')->where('id', $sessionId)->first();

			if($sessionData['front_user_current_role'] == 2){
			if(!empty($session_data)){
				$schedule_time = ConverTimeZoneForUser($session_data->scheduled_date_time, 'Y-m-d H:i');
				$current_time = ConverTimeZoneForUser($now, 'Y-m-d H:i');
			    //echo "<pre>";print_r($schedule_time);echo "<br>";print_r($current_time);
			  	$hourdiff = round((strtotime($schedule_time) - strtotime($current_time))/3600, 1);
				if($hourdiff > 0){
					$penalty_percentage = countSessionCancelledPenalty(intval($hourdiff));
					//dd($penalty_percentage);
					if(!empty($penalty_percentage)){
						$penalty_amount = ($penalty_percentage * $session_data->session_fee)/100;
						$refund_amount = $session_data->session_fee - $penalty_amount;
						$admin_commission = $penalty_amount;
					}

				}
			}
		 }	
		/*penalty amount count end*/

		/* Punctuality ratings count if tutor cancelled the session*/
		$final_punctuality_rating = "";
		if($sessionData['front_user_current_role'] == 1){
			//countPunctualityRatingsForCancelledSession
			if(!empty($session_data)){
				$schedule_time = ConverTimeZoneForUser($session_data->scheduled_date_time, 'Y-m-d H:i');
				$current_time = ConverTimeZoneForUser($now, 'Y-m-d H:i');
			    //echo "<pre>";print_r($schedule_time);echo "<br>";print_r($current_time);
			  	$hourdiff = round((strtotime($schedule_time) - strtotime($current_time))/3600, 1);
				if($hourdiff > 0){
					$punctuality_rating = countPunctualityRatingsForCancelledSession(intval($hourdiff));
				//	echo $hourdiff;echo "<br>";
				//	dd($punctuality_rating);
					if(!empty($punctuality_rating)){
						$final_punctuality_rating = $punctuality_rating;
					}

				}
			}

		}
		/*puncuality count end*/

		
		$cancelDateTime = Session::where('id', $sessionId)
			->update(['cancelled_date_time' => $now,
				'status' => $status,
				'penalty_amount' => $penalty_amount,
				'refund_amount' => $refund_amount,
				'admin_commission' => $admin_commission,
				'punctuality_ratings' => $final_punctuality_rating
			]);

			$avg_punctuality_ratings = DB::table('session')
                    ->where('tutor_id', $session_data['tutor_id'])
                    ->whereIn('status', array('1', '3'))
                    ->avg('punctuality_ratings');
            $final_avg_punctuality_ratings = round($avg_punctuality_ratings, 1);
          
            $user_educational = UserEducation::where('front_user_id', $session_data['tutor_id'])
                ->update([
                    'punctuality' => $final_avg_punctuality_ratings,
                    'updated_at'  => Carbon::now()->toDateTimestring(),
                ]);
		
            /*Notification:Tutor cancels session */
            
            if (isset($topic_id) && $topic_id != 0 && $topic_id != "") {
                $Hierarchy  =  getCurriculumHierarchy('', $topic_id, true); //for topic
            } else {
                $Hierarchy  = getCurriculumHierarchy($subject_id, '', true); //for subject
            }
            $name           = $sessionData['front_user_first_name']." ".$sessionData['front_user_last_name'];

           
            if($sessionData['front_user_current_role'] == 1){
            	$from_user_id 			= $tutor_id;
            	$to_user_id				= $tutee_id;	
            	$notification_type_id  	= 6;
            	$tutor_timezone = getTimezoneName($tutor_id);
            	$date = ConverTimeZoneForUser($from_date_time, 'd-m-Y',$tutor_timezone) . " | " . ConverTimeZoneForUser($from_date_time, 'h:i a',$tutor_timezone);
            	$tutee_data = getUserData($tutee_id);
				$tutee_name = "";
				if(!empty($tutee_data)){
					$tutee_name = $tutee_data->first_name." ".$tutee_data->last_name;
				}
				$message_admin = "<b>".$name."</b> cancelled the session which is schedule with <b>".$tutee_name."</b> for the curriculum ".$Hierarchy."";
            }
            else{
            	$from_user_id 			= $tutee_id;
            	$to_user_id				= $tutor_id;
            	$notification_type_id  	= 7;
            	$tutee_timezone = getTimezoneName($tutee_id);
            	$date = ConverTimeZoneForUser($from_date_time, 'd-m-Y',$tutee_timezone) . " | " . ConverTimeZoneForUser($from_date_time, 'h:i a',$tutee_timezone);
            	

				$tutor_data = getUserData($tutor_id);
				$tutor_name = "";
				if(!empty($tutor_data)){
					$tutor_name = $tutor_data->first_name." ".$tutor_data->last_name;
				}
				$message_admin = "<b>".$name."</b> cancelled the session which is schedule with <b>".$tutor_name."</b> for the curriculum ".$Hierarchy."";
            }


		if($sessionData['front_user_current_role'] == 1){
			/*Punctuality ratings given by system after session cancelled by tutor*/
	        $message      = "System post punctuality ratings ".$final_punctuality_rating." out of 10 for Your cancelled session with <b>" . $tutee_name . "</b>.";
	        $Notification = Notification::insert([
	            'from_user_id'         => $tutor_id,
	            'to_user_id'           => $tutor_id,
	            'notification_type_id' => 22,
	            'is_read'              => 0,
	            'is_read_admin'        => 0,
	            'is_for_admin'         => 0,
	            'created_at'           => Carbon::now()->toDateTimestring(),
	            'message'              => $message,
	        ]);
	        /*end*/
		}
		

             
            $message        = "<a href='/tutee-sessions'>Your session request for curriculum ".$Hierarchy." has been cancelled by <b>".$name."</b> for the session on ". $date ."</a>";

            $Notification = Notification::insert([
            'from_user_id' => $from_user_id,
            'to_user_id' => $to_user_id,
            'notification_type_id' => $notification_type_id,
            'is_read' => 0,
            'is_read_admin' => 0,
            'is_for_admin' => 0,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
            ]);

            //for admin
			
            $Notification = Notification::insert([
            'from_user_id' => $from_user_id,
            'to_user_id' => $to_user_id,
            'notification_type_id' => $notification_type_id,
            'is_read' => 1,
            'is_read_admin' => 0,
            'is_for_admin' => 1,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message_admin,
            ]);
            /*Notification end*/
		return response('The session cancelled successfully', 200);

	}

	public function checkPreference(ReserveTutorRequest $request) {
		$tutorId = $request->tutorId;
		$ftime = $request->from_time;
		$fdate = $request->fromDate;
		$obj = $request->objective;
		$curriculum = $request->curriculum_id;

		if ($ftime == "") {
			$arr = array('response' => 'error', 'message' => 'Please select from time');
			return json_encode($arr);
		}

		if ($curriculum == "") {
			$arr = array('response' => 'error', 'message' => 'Please select curriculumn');
			return json_encode($arr);
		}

		if ($obj == "") {
			$arr = array('response' => 'error', 'message' => 'Please Enter objective');
			return json_encode($arr);
		}
	
		$now = Carbon::now()->toDateTimestring();
		$from_date_time = ConverTimeZoneForDB($fdate . " " . ConverTimeIn24Hour($ftime), 'Y-m-d H:i:s');
		
		if (strtotime($now) >= strtotime($from_date_time)) {
			$arr = array('response' => 'error', 'message' => 'You have to select future time only');
			return json_encode($arr);
		}

		$from_date = convertDateInSqlFormat($from_date_time);
		$start_time = ConverTimeIn24Hour($from_date_time);

		if (isset($request->topic_id) && $request->topic_id != 0 && $request->topic_id != "") {
			$hierarchy = getCurriculumHierarchy('', $request->topic_id, true);
		} else {
			$hierarchy = getCurriculumHierarchy($request->subject_id, '', true);
		}
		$sessionFees = "";
		$fees = Grade::select('curriculum.*', 'grade.*')
			->leftJoin('curriculum', 'grade.curriculum_id', '=', 'curriculum.id')
			->where('grade.id', '=', $request->grade_id)
			->get()
			->first();

		if (isset($fees->grade_type) && $fees->grade_type == 1) {
			$sessionFees = $fees->higher_level_fee;

			if ($sessionFees == 0) {
				$higerfees = Setting::select('higher_level_grade_fee')->get()->first();
				$sessionFees = $higerfees->higher_level_grade_fee;
			}
		} else {
			$sessionFees = isset($fees->lower_level_fee)?$fees->lower_level_fee:"";

			if ($sessionFees == 0) {
				$lowerfees = Setting::select('lower_level_grade_fee')->get()->first();
				$sessionFees = $lowerfees->lower_level_grade_fee;
			}
		}

		$loggedin_role =  \Session::get('front_user_role');

		$checkDateTime	=	date('Y-m-d H:i:s', strtotime($from_date_time . " +1 hours"));//2017-05-22 20:00:00
		$checkTime 		=	ConverTimeIn24Hour($checkDateTime);

		$existing_record = TutorPreference::where(DB::raw("CONCAT(`start_date`, ' ', `start_time`)"),'<=', $from_date_time)
			->where(DB::raw("CONCAT(`end_date`, ' ', `end_time`)"),'>=', $checkDateTime)
			->where('start_time','<=', $start_time)
			->where('end_time','>=', $checkTime)
			->where('tutor_id', $tutorId)
			->get()
			->toArray();
			
		$tutee_id = \Session::get('front_user_id');	
		$chack_tutee_availability2 = [];	
		$chack_tutee_availability = Session::where('scheduled_date_time', '<=', $from_date_time)
			->where('end_date_time', '>=', $from_date_time)
			->where('tutee_id', $tutee_id)
			->whereIn('status',array('2','5'))
			->get()
			->toArray();
		if(!empty($loggedin_role) && $loggedin_role == '3'){
			$chack_tutee_availability2 = Session::where('scheduled_date_time', '<=', $from_date_time)
			->where('end_date_time', '>=', $from_date_time)
			->where('tutor_id', $tutee_id)
			->whereIn('status',array('2'))
			->get()
			->toArray();

			if(!empty($chack_tutee_availability2)){
				$arr = array('response' => 'error', 'message' => 'You have already booked session as tutor for this time slot.');	
			}
		}	

		if (!empty($existing_record)) {
			$chack_tutor_availability = Session::where('scheduled_date_time', '<=', $from_date_time)
				->where('end_date_time', '>=', $from_date_time)
				->where('tutor_id', $tutorId)
				->whereIn('status',array('2'))
				->get()
				->toArray();

			

			$from_date = getOnlyDate($fdate);
			$start_time = getOnlyTime($ftime);

			if(!empty($chack_tutor_availability)){
				$arr = array('response' => 'error', 'message' => 'This Tutor is already reserved for this time slot.');	
			}else{
				$arr = array('response' => 'success', 'obj' => $obj, 'hierarchy' => $hierarchy, 'from_date' => $from_date, 'start_time' => $start_time, 'sessionFees' => $sessionFees);	
			}

		} else {
			$arr = array('response' => 'error', 'message' => 'This Tutor is not available for this time slot');
		}

		if(!empty($chack_tutee_availability)){
			$arr = array('response' => 'error', 'message' => 'You have already booked session for this time slot.');	
		}

		if(!empty($loggedin_role) && $loggedin_role == '3'){
			if(!empty($chack_tutee_availability2)){
				$arr = array('response' => 'error', 'message' => 'You have already booked session as tutor for this time slot.');	
			}
		}
		

		return json_encode($arr);
	}

	public function conformPreference(Request $request) {
		$sessionFees = $request['gb']['sessionFees'];
		$tutor_id = $request->tutorId;
		$tutee_id = \Session::get('front_user_id');
		$subject_id = $request->subject_id;
		$topic_id = $request->topic_id;
		$objective = $request['gb']['obj'];
		$from_date = $request['gb']['from_date'];
		$from_time = $request['gb']['start_time'];

		$from_date_time = ConverTimeZoneForDB($from_date . " " . ConverTimeIn24Hour($from_time), 'Y-m-d H:i:s');
		
		$found_record = Session::where('scheduled_date_time', '<=', $from_date_time)
			->where('end_date_time', '>=', $from_date_time)
			->where('tutor_id', $tutor_id)
			->whereIn('status',array('2'))
			->get()
			->toArray();

		$end_date_time = date("Y-m-d H:i", strtotime($from_date_time . "+60 minutes"));

		if (!empty($found_record[0])) {
			$arr = array('response' => 'fail', 'message' => 'This Tutor Already Reserved');
		} else {

			$reserveSession = Session::insert([
				'tutor_id' 			  => $tutor_id,
				'tutee_id' 			  => $tutee_id,
				'subject_id' 		  => $subject_id,
				'topic_id' 			  => $topic_id,
				'objective' 		  => $objective,
				'scheduled_date_time' => $from_date_time,
				'end_date_time' 	  => $end_date_time,
				'session_fee' 		  => $sessionFees,
				'reserved_date_time'  => carbon::now()->todateTimeString(),
				'created_at'		  => carbon::now()->toDateTimestring(),
			]);

			$sessionData = [
				"front_user_first_name" => \Session::get('front_user_first_name'),
				"front_user_last_name" => \Session::get('front_user_last_name'),
			];

			/*Notification:Session request made by Tutee */
			$name = $sessionData['front_user_first_name']." ".$sessionData['front_user_last_name'];

			$tutor_timezone = getTimezoneName($tutor_id);
			$from_date_final = ConverTimeZoneForUser($from_date_time, 'Y-m-d h:i a',$tutor_timezone);

			$message = "<a href='/tutor-session-request'>You have received a session request from <b>".$name."</b> for the session on ".$from_date_final ."</a>";
			$Notification = Notification::insert([
				'from_user_id' => $tutee_id,
				'to_user_id' => $tutor_id,
				'notification_type_id' => 1,
				'is_read' => 0,
				'is_read_admin' => 0,
				'is_for_admin' => 0,
				'created_at' => Carbon::now()->toDateTimestring(),
				'message' => $message,
			]);

			$tutor_data = getUserData($tutor_id);
			$tutor_name = "";
			if(!empty($tutor_data)){
				$tutor_name = $tutor_data->first_name." ".$tutor_data->last_name;
			}
			$message = "<b>".$name."</b> Reserved a session on ".$from_date_time ." with <b>".$tutor_name."</b>";
			$Notification = Notification::insert([
				'from_user_id' => $tutee_id,
				'to_user_id' => $tutor_id,
				'notification_type_id' => 1,
				'is_read' => 1,
				'is_read_admin' => 0,
				'is_for_admin' => 1,
				'created_at' => Carbon::now()->toDateTimestring(),
				'message' => $message,
			]);
			/*Notification end*/
			$arr = array('response' => 'success', 'message' => 'Your Session is reserved successfully');
		}
		return json_encode($arr);
	}

	public function checkLogin(Request $request) {
		$post = $request->all();
		$tutor_details_url = url('/tutorDetail/' . $post['tutor_id']);
		setcookie("tutor_details_url", $tutor_details_url);
		//Cookie::queue(Cookie::make('tutor_details_url', $tutor_details_url));
		$url = url('/login');
		echo $url;exit;
	}

	public function reportTutee(ReportTuteeRequest $request)
	{	
		$sessionId = $request->id;
		$reportText = $request->report;
		$loginUser = \Session::get('front_user_id');
		if ($reportText == "") {
			return response(array('msg' =>'Feedback Field Is Required', 'res' => 201));
		}
		$sessionInfo = Session::where('id', $sessionId)->get()->toArray();
		
		$reportedUserId = $sessionInfo[0]['tutee_id'];

		$reported_role = 1;

		$addReport = UserReports::insert([
										'reported_text' 	=> $reportText,
										'user_id' 			=> $loginUser,
										'reported_user_id' 	=> $reportedUserId,
										'reported_role' 	=> $reported_role,
			]);

		 /*Notification:Report on Tutor / Tutee submitted by registered users */
        $sessionData = [
                "front_user_first_name" => \Session::get('front_user_first_name'),
                "front_user_last_name" => \Session::get('front_user_last_name'),
            ];
        $name = $sessionData['front_user_first_name']." ".$sessionData['front_user_last_name'];
        $tutee_data = Frontusers::select('*')->where('id', $reportedUserId)->get()->toArray();
        $tutee_name = $tutee_data['0']['first_name']." ".$tutee_data['0']['last_name'];
        $message = "<b>".$name."</b> wrote a report of <b>".$tutee_name."</b>";
        $Notification = Notification::insert([
            'from_user_id' => 0,
            'to_user_id' => 0,
            'notification_type_id' => 15,
            'is_read' => 0,
            'is_read_admin' => 0,
            'is_for_admin' => 1,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
        ]);
        /*Notification end*/
		return response('The Report Submited successfully', 200);	
	}


	function array_flatten($array) { 
		  if (!is_array($array)) { 
		    return FALSE; 
		  } 
		  $result = array(); 
		  foreach ($array as $key => $value) { 
		    if (is_array($value)) { 
		      $arrayList=array_flatten($value);
		      foreach ($arrayList as $listItem) {
		        $result[] = $listItem; 
		      }
		    } 
		   else { 
		    $result[$key] = $value; 
		   } 
		  } 
		  return $result; 
		} 

		/*
		 * Session Feedback By Tutor
		 */
		public function sessionFeedback(Request $request)
		{	
			$sessionId = $request->id;
			$feedbackText = $request->report;
			$byTutor = 1;
			$posted_by = \Session::get('front_user_id');
			
			if ($feedbackText == "") {
				return response(array('msg' =>'Feedback Field Is Required', 'res' => 201));
			}
			
			$sessionInfo = Session::where('id', $sessionId)->get()->toArray();
			$posted_for = $sessionInfo[0]['tutee_id'];
			$session_end_date_time = $sessionInfo[0]['end_date_time'];

			$sessionFeedback  = new SessionFeedback;
            $sessionFeedback->report_text     = $feedbackText;
            $sessionFeedback->posted_by       = $posted_by;
            $sessionFeedback->posted_for      = $posted_for;
            $sessionFeedback->session_id      = $sessionId;
            $sessionFeedback->modified_by     = $posted_by;
            $sessionFeedback->session_end_date_time = $session_end_date_time;
            $addRecord    = $sessionFeedback->save();
            
            if($addRecord)
            {
				Session::where('id', $sessionId)->update([
	                    'session_feedback_tutor' => $feedbackText,
	                    'feedback_by_tutor' => $byTutor
	                ]);
			}

			return response('The Feedback Submited successfully', 200);	
		}
}