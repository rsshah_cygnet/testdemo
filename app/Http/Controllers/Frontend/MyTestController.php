<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Tutor\TutorAppearTestRequest;
use App\Models\Curriculum\Curriculum;
use App\Models\Notification\Notification;
use App\Models\Session\Session;
use App\Models\TutorDetail\TutorDetail;
use App\Repositories\Frontend\Test\TestContract;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Tutee\becomeTutorRequest;
use App\Models\Frontusers\Frontusers;

class MyTestController extends Controller
{

    /**
     * Repository Object
     *
     * @var object
     */
    public $repository;

    /**
     * __construct
     *
     * @param TestContract $repository
     */
    public function __construct(TestContract $repository)
    {
        $this->repository = $repository;
    }

    public function test($id)
    {
        if (!check_front_login()) {
            $testUrl = \Request::url();
            \Session::set('tutor_test_link', $testUrl);
            return redirect('/');
        }
        \Session::set('tutor_test_link', '');
        \Session::forget('tutor_test_link');
        
        $id = \Crypt::decrypt($id);

        $loggedInUserId = \Session::get('front_user_id');
        $findTest       = TutorDetail::find($id);

        $reAppeareTest = DB::table('settings')
            ->select('reappearing_test')
            ->first();
        $totalGivenTest = DB::table('tutor_details')
            ->where('front_user_id', $loggedInUserId)
            ->where('appeared', 1)
            ->where('is_completed', 1)
            ->where('passed_status', 0)
            ->count();
        if ($reAppeareTest->reappearing_test > $totalGivenTest) {
            $remainingAttemptes = $reAppeareTest->reappearing_test - $totalGivenTest;
        } else {
            $remainingAttemptes = 0;
        }
        if ($remainingAttemptes == 0) {
            \Session::flash('reAttemptMessage', "You can't appear for test as you have used all your reattempt chances. ");
            return redirect(route('tutor.mytest'));
        }
        if ($findTest->appeared == 1) {
            //if ($findTest->appeared == 1 && $findTest->passed_status == 0 && $findTest->is_completed == 1) {
            //$id = \Crypt::encrypt($id);
            //$this->giveTestAgain($id);
            \Session::flash('reAttemptMessage', 'This link is expired');
            return redirect(route('tutor.mytest'));
        } else {
            if ($findTest['appeared'] == 1) {
                //\Session::flash('reAttemptMessage', 'You have already given a test for this');
                return redirect(route('frontend.tutorDashboard'));
            }
            $testAppeared = DB::table('tutor_details')
                ->where('id', $id)
                ->limit(1)
                ->update([
                    'appeared'       => 1,
                    'test_date_time' => carbon::now()->toDateTimeString(),
                ]);
        }

        $getUser = DB::table('front_user')
            ->leftJoin('tutor_details', 'tutor_details.front_user_id', '=', 'front_user.id')
            ->select('tutor_details.*', 'front_user.photo', DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor", "tutor_details.*"))
            ->where('tutor_details.id', $id)
            ->first();
        

        $test_data = $findTest->toArray();
        $topic_id  = $subject_id  = '';

        $number_of_questions_per_topic_data = object_to_array(DB::table('settings')
                ->select('number_of_questions_per_topic')
                ->first());
        $number_of_questions_per_topic = $number_of_questions_per_topic_data['number_of_questions_per_topic'];

        $number_of_minutes_per_questions_data = object_to_array(DB::table('settings')
                ->select('number_of_minutes_per_questions')
                ->first());
        $number_of_minutes_per_questions = $number_of_minutes_per_questions_data['number_of_minutes_per_questions'];

        $min_questions_in_test_data = object_to_array(DB::table('settings')
                ->select('min_questions_in_test')
                ->first());

        $min_questions_in_test = $min_questions_in_test_data['min_questions_in_test'];
        /*$number_of_questions_per_topic
        $number_of_minutes_per_questions*/
        $subject_data = object_to_array(DB::table('subject')
            ->select('number_of_questions_per_topic', 'number_of_max_questions_per_topic','passing_percentage', 'number_of_minutes_per_questions')->where('id',$getUser->subject_id)
                    ->first());

        $passing_percentage = "";
       
         if (isset($subject_data['passing_percentage']) && $subject_data['passing_percentage'] != "" && $subject_data['passing_percentage'] != 0) {
                $passing_percentage = $subject_data['passing_percentage'];
            }
         if (isset($subject_data['number_of_minutes_per_questions']) && $subject_data['number_of_minutes_per_questions'] != "" && $subject_data['number_of_minutes_per_questions'] != 0) {
                $number_of_minutes_per_questions = $subject_data['number_of_minutes_per_questions'];
            }
        //Higher grade
        if ($test_data['topic_id'] != '' && $test_data['topic_id'] != '0') {
            if (isset($subject_data['number_of_max_questions_per_topic']) && $subject_data['number_of_max_questions_per_topic'] != "" && $subject_data['number_of_max_questions_per_topic'] != 0) {
                $min_questions_in_test  = $subject_data['number_of_max_questions_per_topic'];
            }
           
         
            $questions = $this->repository->getQuestions($test_data['topic_id'], $min_questions_in_test, 1);
        } else {
                    
            if (isset($subject_data['number_of_questions_per_topic']) && $subject_data['number_of_questions_per_topic'] != "" && $subject_data['number_of_questions_per_topic'] != 0) {
                $number_of_questions_per_topic = $subject_data['number_of_questions_per_topic'];
            }

            $questions          = $this->repository->getQuestions($test_data['subject_id'], $number_of_questions_per_topic, 0);
            if ($questions == false) {
                $previoudURL = url('/') . '/tutor/mytest';
                $message     = "Sorry for inconvenience.You can't appear test for this subject. Please try agian after some time.";
                \Session::set('ExamInconvienceMessage', $message);
                if ($previoudURL == \URL::previous()) {
                    return redirect(route('tutor.mytest'));
                }
                if (!empty(\Session::get('current_role')) && \Session::get('current_role') == 2) {
                    return redirect(route('frontend.tuteeDashboard'));
                }
            }
        }

        $previoudURL = url('/') . '/tutee-dashboard';
        if ($previoudURL == \URL::previous()) {
            $updateRoll = DB::table('front_user')
                ->where('id', $loggedInUserId)
                ->update([
                    'role' => 3,
                ]);
            \Session::set('becomeTutor', 1);
            \Session::set('current_role', 1);
            \Session::set('front_user_role', 3);
        }

        $num_of_que = count($questions->toArray());
        $time       = $num_of_que * $number_of_minutes_per_questions;
        //$questions = $this->repository->getQuestions($id);
        $getTestDetails = $this->repository->getTestDetails($id);

        $date      = date('Y/m/d H:i:s');
        $startDate = strtotime($date);
        $endDate   = date("Y/m/d H:i:s", strtotime($time . " minutes", $startDate));

        $diff    = strtotime($endDate) - strtotime($date);
        $hours   = date('H', $diff);
        $minutes = date('i', $diff);
        $seconds = date('s', $diff);
        return view('frontend.test.test')
            ->with('getUser', $getUser)
            ->with('questions', $questions)
            ->with('getTestDetails', $getTestDetails)
            ->with('sessionStartTime', $startDate)
            ->with('sessionEndTime', $endDate)
            ->with('num_of_que', $num_of_que)
            /*->with('minutes', $time)*/
            ->with('tutorID', $id)
            ->with('hours', $hours)
            ->with('minutes', $minutes)
            ->with('seconds', $seconds)
            ->with('passing_percentage', $passing_percentage)
            ->with('tutorID', $id);
    }

    public function giveTestAgain($id)
    {
        if (!check_front_login()) {
            return redirect('/');
        }

        $id = \Crypt::decrypt($id);

        $loggedInUserId = \Session::get('front_user_id');
        $findTest       = TutorDetail::find($id);

        $reAppeareTest = DB::table('settings')
            ->select('reappearing_test')
            ->first();
        $totalGivenTest = DB::table('tutor_details')
            ->where('front_user_id', $loggedInUserId)
            ->where('appeared', 1)
            ->where('is_completed', 1)
            ->where('passed_status', 0)
            ->count();
        if ($reAppeareTest->reappearing_test > $totalGivenTest) {
            $remainingAttemptes = $reAppeareTest->reappearing_test - $totalGivenTest;
        } else {
            $remainingAttemptes = 0;
        }
        if ($remainingAttemptes == 0) {
            \Session::flash('reAttemptMessage', "You can't appear for test as you have used all your reattempt chances. ");
            return redirect(route('tutor.mytest'));
        }
        if ($findTest->appeared == 1 && $findTest->passed_status == 0 && $findTest->is_completed == 1) {
            $reAttempTestRecord = DB::table('tutor_details')
                ->where('id', $id)
                ->where('front_user_id', $loggedInUserId)
                ->insertGetId([
                    'front_user_id'       => $findTest->front_user_id,
                    'curriculum_id'       => $findTest->curriculum_id,
                    'education_system_id' => $findTest->education_system_id,
                    'level_id'            => $findTest->level_id,
                    'program_id'          => $findTest->program_id,
                    'grade_id'            => $findTest->grade_id,
                    'subject_id'          => $findTest->subject_id,
                    'topic_id'            => $findTest->topic_id,
                    'appeared'            => 0,
                    'test_date_time'      => carbon::now()->toDateTimeString(),
                    'created_at'          => carbon::now()->toDateTimeString(),
                ]);
            $id         = $reAttempTestRecord;
            $encrypt_id = \Crypt::encrypt($id);
            return redirect(route('tutor.test', $encrypt_id));
        }

        return '';
    }

    public function testAnswer(Request $request)
    {
        // dd($request->all());
        if (!check_front_login()) {
            return redirect('/');
        }
        $answer     = $this->repository->getAnswers($request->all());
        $data       = TutorDetail::find($request->tutorId);
        $subject_id = $data->subject_id;
        $topic_id   = $data->topic_id;
        $tutor_id   = $data->front_user_id;
        $tutor_data = getUserData($tutor_id);
                $tutor_name = "";
                if(!empty($tutor_data)){
                    $tutor_name = $tutor_data->first_name." ".$tutor_data->last_name;
                }

        if (isset($topic_id) && $topic_id != 0 && $topic_id != "") {
            $Hierarchy = getCurriculumHierarchy('', $topic_id, true); //for topic
        } else {
            $Hierarchy = getCurriculumHierarchy($subject_id, '', true); //for subject
        }
        if ($answer == 1) {
            $test_status = 1; //indicate that tutor passed test.
            $message     = "<a href='/tutor/mytest'>You have successfully passed test for curriculum <b>" . $Hierarchy . "</b></a>";
            $message_admin     = "<b>".$tutor_name."</b> successfully passed test for curriculum <b>" . $Hierarchy . "</b>";
        } else {
            $test_status = 0; //indicate that tutor fails test.
            $message     = "<a href='/tutor/mytest'>You have not cleared test for curriculum <b>" . $Hierarchy . "</b></a>";
            $message_admin     = "<b>".$tutor_name."</b> failed the test for curriculum <b>" . $Hierarchy . "</b>";
        }
        $sessionData = [
            "front_user_first_name" => \Session::get('front_user_first_name'),
            "front_user_last_name"  => \Session::get('front_user_last_name'),
        ];
        /*Notification:When Tutor passes / fails a test*/
        $name = $sessionData['front_user_first_name'] . " " . $sessionData['front_user_last_name'];
        $Notification = Notification::insert([
            'from_user_id'         => 0,
            'to_user_id'           => $tutor_id,
            'notification_type_id' => 12,
            'is_read'              => 0,
            'is_read_admin'        => 0,
            'is_for_admin'         => 0,
            'created_at'           => Carbon::now()->toDateTimestring(),
            'message'              => $message,
        ]);

        //for admin
         $Notification = Notification::insert([
            'from_user_id'         => 0,
            'to_user_id'           => $tutor_id,
            'notification_type_id' => 12,
            'is_read'              => 1,
            'is_read_admin'        => 0,
            'is_for_admin'         => 1,
            'created_at'           => Carbon::now()->toDateTimestring(),
            'message'              => $message_admin,
        ]);
        /*Notification end*/
        return redirect('/logout');
        //return redirect(route('auth.register'));
    }

    public function mytest(Request $request, $tabReload = null)
    {

       if(!$request->ajax()){
            if (!check_front_login()) {
                return redirect('/');
            }
        }

        // if(!empty($request->datefilter)){
        //     $date = explode('-', $request->datefilter);
        //     $start_Date = $date[0];
        //     $endDate    = $date[1]; 
        //     $startDate = str_replace(' ', '', $start_Date);
        //     \Session::set('startDate', date("Y-m-d", strtotime($startDate)));
        //     \Session::set('endDate', date("Y-m-d", strtotime($endDate)));
        // }else{
        //     if(empty(\Session::get('startDate')) || $request->removeFilter == 'removeFilter' || $request->removeFilter == ''){
        //         if(empty($request->page) && $tabReload == null){
        //             \Session::forget('startDate');
        //             \Session::forget('startDate');
        //         }
        //     }
        // }
        $id          = \Session::get('front_user_id');
        $sessionData = [
            "front_user_id"          => \Session::get('front_user_id'),
            "front_user_role"        => \Session::get('front_user_role'),
            "front_user_first_name"  => \Session::get('front_user_first_name'),
            "front_user_last_name"   => \Session::get('front_user_last_name'),
            "front_user_username"    => \Session::get('front_user_username'),
            "front_user_email"       => \Session::get('front_user_email'),
            "front_user_country_id"  => \Session::get('front_user_country_id'),
            "front_user_state_id"    => \Session::get('front_user_state_id'),
            "front_user_city_id"     => \Session::get('front_user_city_id'),
            "front_user_nationality" => \Session::get('front_user_nationality'),
            "front_user_lang_id"     => \Session::get('front_user_lang_id'),
            "front_user_photo"       => \Session::get('front_user_photo'),
            "front_user_timezone_id" => \Session::get('front_user_timezone_id'),
            "current_role"           => \Session::get('current_role'),
        ]; //dd($sessionData);

        $notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

        $notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

        $pendingTest   = $this->repository->getPendingTest();
        
        // if(!empty(\Session::get('startDate'))){
        //     $startDate = date("Y-m-d", strtotime(\Session::get('startDate')));
        //     $endDate = date("Y-m-d", strtotime(\Session::get('endDate')));
        //     $appearedTest  = $this->repository->getAppearedTestByFilter($startDate, $endDate);
        // }
        // else{
            $appearedTest  = $this->repository->getAppearedTest();
        // }
        $reAppeareTest = DB::table('settings')
            ->select('reappearing_test')
            ->first();
        $totalGivenTest = DB::table('tutor_details')
            ->where('front_user_id', $id)
            ->where('appeared', 1)
            ->where('is_completed', 1)
            ->where('passed_status', 0)
            ->count();

        if ($reAppeareTest->reappearing_test > $totalGivenTest) {
            $remainingAttemptes = $reAppeareTest->reappearing_test - $totalGivenTest;
        } else {
            $remainingAttemptes = 0;
        }
        $curriculumn = Curriculum::getCurriculumList();

        // dd($curriculumn);

         if($request->ajax()){
             $returnHTML =  view('frontend.test.mid_section_MyTest')
            ->with('pendingTest', $pendingTest)
            ->with('appearedTest', $appearedTest)
            ->with('remainingAttemptes', $remainingAttemptes)
            ->with('curriculumn', $curriculumn)
            ->with('sessionData', $sessionData)
            ->with('notifications', $notifications)
            ->with('notificationCount', $notificationCount)->render();
             return response()->json(array('status' => 'success', 'html' => $returnHTML));
        }else{
            
        return view('frontend.test.MyTest')
            ->with('pendingTest', $pendingTest)
            ->with('appearedTest', $appearedTest)
            ->with('remainingAttemptes', $remainingAttemptes)
            ->with('curriculumn', $curriculumn)
            ->with('sessionData', $sessionData)
            ->with('notifications', $notifications)
            ->with('notificationCount', $notificationCount);
        }      
    }

    /**
     * Show test record by filter
     */
    public function showTestByFilter($startDate, $endDate)
    {
        if (!check_front_login()) {
            return redirect('/');
        }

        $loggedInUserId = \Session::get('front_user_id');
        $reAppeareTest  = DB::table('settings')
            ->select('reappearing_test')
            ->first();
        $totalGivenTest = DB::table('tutor_details')
            ->where('front_user_id', $loggedInUserId)
            ->where('appeared', 1)
            ->where('is_completed', 1)
            ->where('passed_status', 0)
            ->count();

        if ($reAppeareTest->reappearing_test > $totalGivenTest) {
            $remainingAttemptes = $reAppeareTest->reappearing_test - $totalGivenTest;
        } else {
            $remainingAttemptes = 0;
        }

        $getPaymentsByFilter = $this->repository->getAppearedTestByFilter($startDate, $endDate);

        $appearedTestHTML = view('frontend.test.appeared-test')
            ->with([
                'getPaymentsByFilter' => $getPaymentsByFilter,
                'remainingAttemptes'  => $remainingAttemptes,
            ])
            ->render();

        return response()->json(array('appearedTestHTML' => $appearedTestHTML));
    }

    /**
     * Appear for new test
     */
    public function appearForNewTest(TutorAppearTestRequest $request)
    {
        
        $TutorDetails = $this->repository->tutorTestDetails($request->all());

        return redirect()->route('tutor.test', ['id' => \Crypt::encrypt($TutorDetails)]);
    }

    /**
     * Tutee apply for test to become tutor
     */
    public function becomeTutorTest(becomeTutorRequest $request)
    {
       
        $TutorDetails = $this->repository->becomeTutorTest($request->all());
        $id = \Session::get('front_user_id');
        $name = \Session::get('front_user_first_name')." ".\Session::get('front_user_last_name');
            
        $message  = "Your account upgrade request has been submitted successfully";

            $Notification = Notification::insert([
            'from_user_id' => $id,
            'to_user_id' => $id,
            'notification_type_id' => 14,
            'is_read' => 0,
            'is_read_admin' => 0,
            'is_for_admin' => 0,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
            ]);

            $message  = "".$name."'s account has been upgraded ";

            $Notification = Notification::insert([
            'from_user_id' => $id,
            'to_user_id' => $id,
            'notification_type_id' => 14,
            'is_read' => 1,
            'is_read_admin' => 0,
            'is_for_admin' => 1,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
            ]);

            //Send test link to user
            $user = Frontusers::find($id);
                if (!empty($user)) {
                    $userData = \DB::table('tutor_details')
                        ->select('id', 'topic_id', 'subject_id')
                        ->where('front_user_id', $user->id)
                        ->where('appeared', 0)
                        ->where('is_completed', 0)
                        ->where('id', $TutorDetails)
                        ->get();

                    $userEmailId = $user->email;

                    $link = array();
                    foreach ($userData as $id) {
                        if (isset($id->topic_id) && $id->topic_id != 0 && $id->topic_id != "") {
                            $curriculum = getCurriculumHierarchy('',$id->topic_id, true);

                        } else if (isset($id->subject_id) && $id->subject_id != 0 && $id->subject_id != "") {
                            $curriculum = getCurriculumHierarchy($id->subject_id, '', true);
                        }
                        
                        $ExamlId = \Crypt::encrypt($id->id);

                        $link[] = $curriculum . ": " . URL('/') . '/test/' . $ExamlId . " , \n \n";
                    }
                    
                    $makeExamLink = str_replace(array(','), '', $link);
                    $Examlink     = implode(" ", $makeExamLink);
                    $appName      = config('app.name');
                    $mail         = \Mail::raw($Examlink, function ($message) use ($Examlink, $userEmailId) {
                        $message->to($userEmailId);
                        $message->subject('Your Exam links');
                    });
                    
                    $updateRoll = \DB::table('front_user')->where('id', \Session::get('front_user_id'))
                                                          ->update(['role' => 3]);
                }
                \Session::flash('becomeTutorExamLinkMessage', 'We have sent you exam link on your registered email Id. Thank you.');
                \Session::set('front_user_role', 3);
            return redirect()->route('frontend.tuteeDashboard');
            
    }

    public function becomeTutee($id)
    {

        $loggedInUserId = \Session::get('front_user_id');

        if ($id == "2") {
            $updateRoll = \DB::table('front_user')
                ->where('id', $loggedInUserId)
                ->update(['role' => 3]);
            \Session::set('front_user_role', '3');

             $id = \Session::get('front_user_id');
            $name = \Session::get('front_user_first_name')." ".\Session::get('front_user_last_name');
          
        $message  = "Your account upgrade request has been submitted successfully";

            $Notification = Notification::insert([
            'from_user_id' => $id,
            'to_user_id' => $id,
            'notification_type_id' => 14,
            'is_read' => 0,
            'is_read_admin' => 0,
            'is_for_admin' => 0,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
            ]);

            $message  = "".$name."'s account has been upgraded ";

            $Notification = Notification::insert([
            'from_user_id' => $id,
            'to_user_id' => $id,
            'notification_type_id' => 14,
            'is_read' => 1,
            'is_read_admin' => 0,
            'is_for_admin' => 1,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
            ]);


            return redirect('tutee-dashboard');
        }
        return back();
    }

    /**
     * Check test pass or not
     */
    public function checkPassTest(Request $request)
    {

        $curriculum_id  = $request->curriculum_id;
        $education_id   = $request->education_id;
        $level_id       = $request->level_id;
        $program_id     = $request->program_id;
        $grade_id       = $request->grade_id;
        $subject_id     = $request->subject_id;
        $topic_id       = $request->topic_id;

        $loggedInUserId = \Session::get('front_user_id');
        $query          = \DB::table('tutor_details')
            ->where('front_user_id', $loggedInUserId)
            ->where('passed_status', 1);

        if (!empty($curriculum_id)) {
            $query = $query->where('curriculum_id', $curriculum_id);
        }
        if (!empty($education_id)) {
            $query = $query->where('education_system_id', $education_id);
        }
        if (!empty($level_id)) {
            $query = $query->where('level_id', $level_id);
        }
        if (!empty($program_id)) {
            $query = $query->where('program_id', $program_id);
        }
        if (!empty($grade_id)) {
            $query      = $query->where('grade_id', $grade_id);
            $grade      =   '';
        }else{
            $grade      = "grade_required";
        }
        if (!empty($subject_id)) {
            $subject    =   '';
            $query      = $query->where('subject_id', $subject_id);
        }else{
            $subject    = "subject_required";
        }
        if (!empty($topic_id)) {
            $query = $query->where('topic_id', $topic_id);
        }
        
        $checkTestResult = $query->get();

        if(isset($education_id)){
            if(empty($education_id)){
                $data = array('status' => 'appear', 'education_system' => 'education_system_required');
                return json_encode($data);
            }
        }
        if(isset($level_id)){
            if(empty($level_id)){
                $data = array('status' => 'appear', 'level' => 'level_required');
                return json_encode($data);
            }
        }
        if(isset($program_id)){
            if(empty($program_id)){
                $data = array('status' => 'appear', 'program' => 'program_required');
                return json_encode($data);
            }
        }
        if(!empty($grade)){
            $data = array('status' => 'appear', 'grade' => $grade);
            return json_encode($data);
        }
        if(!empty($subject))
        {
            $data = array('status' => 'appear', 'subject' => $subject);
            return json_encode($data);
        }
        if (!empty($grade_id)) {
           $gradeData       =   \DB::table('grade')->where('id', $grade_id)->first();
           $higherGrade     =   $gradeData->grade_type;
           if(empty($topic_id)){
               if($higherGrade == 1){
                    $data = array('status' => 'appear', 'topic' => 'topic_required');
                    return json_encode($data);
               }
            }
        }
        if ($checkTestResult != null) {
            $data = array('status' => 'pass');
            return json_encode($data);
        } else {
            $data = array('status' => 'appear');
            return json_encode($data);
        }
    }

    //After quit test this method is called
    // public function quitTest(Request $request, $id)
    // {
    //     if (!check_front_login()) {
    //         return redirect('/');
    //     }

    //     $tutorDetails = \DB::table('tutor_details')->find($id);

    //     if ($tutorDetails != null) {
    //         $quitTest = \DB::table('tutor_details')->where('id', $id)
    //             ->update([
    //                 'appeared'     => 1,
    //                 'is_completed' => 0,
    //                 'updated_at'   => carbon::now()->todateTimeString(),
    //             ]);
    //     }
    //     return redirect('/logout');
    // }

    public function getMyTest($type, Request $request)
    {

        $loggedInUserId = \Session::get('front_user_id');
        $reAppeareTest  = DB::table('settings')
            ->select('reappearing_test')
            ->first();
        $totalGivenTest = DB::table('tutor_details')
            ->where('front_user_id', $loggedInUserId)
            ->where('appeared', 1)
            ->where('is_completed', 1)
            ->where('passed_status', 0)
            ->count();

        if ($reAppeareTest->reappearing_test > $totalGivenTest) {
            $remainingAttemptes = $reAppeareTest->reappearing_test - $totalGivenTest;
        } else {
            $remainingAttemptes = 0;
        }

        // if($type == 'tab2'){
        //     $getPendingTest   = $this->repository->getPendingTest();

        //     $pendingTestHTML = view('frontend.test.pending-test')
        //         ->with(['getPendingTest' => $getPendingTest])->render();
        //     return response()->json(array('getPendingTest' => $getPendingTest));
        // }
        // dd($request->all());
        if($request->startDate != "undefined" && !empty($request->endDate))
        {
            $startDate  =   $request->startDate;
            $endDate    =   $request->endDate;
            
            $getPaymentsByFilter = $this->repository->getAppearedTestByFilter($startDate, $endDate);
            $getPendingTest   = $this->repository->getPendingTest();

            $appearedTestHTML = view('frontend.test.appeared-test')
                ->with(['getPaymentsByFilter' => $getPaymentsByFilter,'remainingAttemptes'  => $remainingAttemptes,])->render();
            $pendingTestHTML = view('frontend.test.pending-test')
                ->with(['getPendingTest' => $getPendingTest])->render();
            return response()->json(array('appearedTestHTML' => $appearedTestHTML, 'pendingTestHTML' => $pendingTestHTML));
        }

        $getPaymentsByFilter  = $this->repository->getAppearedTest();
        $getPendingTest   = $this->repository->getPendingTest();

        // $getPaymentsByFilter = $this->repositorycontract->getPayments();
        // $cancelSessionsByTuteeByFilter = $this->repositorycontract->getCancelSessionByTutee();
        // $cancelSessionByTutorByFilter = $this->repositorycontract->getCencelSessionByTutor();
        
  //        $getPaymentsByFilter = $this->repositorycontract->getPaymentsByFilter($startDate, $endDate);
        // $cancelSessionsByTuteeByFilter = $this->repositorycontract->cancelSessionsByFilter($startDate, $endDate);
        // $cancelSessionByTutorByFilter = $this->repositorycontract->cancelSessionByTutorByFilter($startDate, $endDate);
        // dd($getPaymentsByFilter);
        $appearedTestHTML = view('frontend.test.appeared-test')
                ->with(['getPaymentsByFilter' => $getPaymentsByFilter,'remainingAttemptes'  => $remainingAttemptes,])->render();
        $pendingTestHTML = view('frontend.test.pending-test')
                ->with(['getPendingTest' => $getPendingTest])->render();

        return response()->json(array('appearedTestHTML' => $appearedTestHTML, 'pendingTestHTML' => $pendingTestHTML));
    }
}
