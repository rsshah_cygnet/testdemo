<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Brand\EloquentBrandRepository;
use App\Repositories\Backend\Cmspages\CmspagesRepositoryContract;
use Illuminate\Http\Request;
use App\Models\CustomerFeedback\CustomerFeedback;
use App\Models\CustomerFeedbackResponse\CustomerFeedbackResponse;
use App\Http\Requests\Frontend\Feedback\FeedbackRequest;
use App\Repositories\Backend\Vehicle\EloquentVehicleRepository;
use App\Repositories\Backend\VehicleModelType\EloquentVehicleModelTypeRepository;

/**
 * Class FrontendController - Home page
 * @package App\Http\Controllers\Frontend
 */
class FrontendController extends Controller
{

    /**
     * FrontendController constructor.
     * @param EloquentBrandRepository $brandRepository
     * @param CmspagesRepositoryContract $cmsPagesRepository
     * @param EloquentVehicleModelTypeRepository $vehicleModelTypeRepository
     * @param EloquentVehicleRepository $vehicleRepository
     */
    public function __construct(EloquentBrandRepository $brandRepository, CmspagesRepositoryContract $cmsPagesRepository, EloquentVehicleModelTypeRepository $vehicleModelTypeRepository, EloquentVehicleRepository $vehicleRepository)
    {
        $this->brand = $brandRepository;
        $this->cmsPages = $cmsPagesRepository;
        $this->vehicleType = $vehicleModelTypeRepository;
        $this->vehicle = $vehicleRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $brands = $this->brand->getBrands();
        $vehicleTypes = $this->vehicleType->getVehicleTypes();
        $vehicleTypesCnt = $this->vehicle->getVehiclesCountsByVehicleTypes();

        return view('frontend.index', compact('brands', 'vehicleTypes', 'vehicleTypesCnt'));
    }

    /**
     * @param $page_slug
     * @return mixed
     */
    public function showCMSPage($page_slug)
    {
        $result = $this->cmsPages->findBySlug($page_slug);
        return view('frontend.cmspages.index')
            ->withCmspages($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showFeedbackForm(Request $request)
    {
        $type = $request->type;
        if(!isset($type))
        {
            return redirect('/');
        }
        return view('frontend.feedback.feedback', compact('type'));
    }

    /**
     * @param FeedbackRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveFeedback(FeedbackRequest $request)
    {   
        $create = \Auth::user()->id;
        $data = $request->all();
        //'first_name'=>$data['first_name'],
        //'surname'=>$data['surname'],
        //'email'=>$data['email'],
        //'phone_no'=>$data['phone_no'];
        $type = $request->type;
        $status = '';
        if($type == "Complaint")
        {
            $status = "Open";
        }
        $lastInsertId = CustomerFeedback::insertGetId([
                'subject'  => $data['subject'],
                'feedback_type' => $request['type'],
                'status' => $status,
                'created_by' => $create
        ]);

        CustomerFeedbackResponse::insert([
                'customer_feedback_id'  => $lastInsertId,
                'description'  => $data['comment'],
                'created_by' => $create
        ]);
        
        return redirect('/');
    }
}
