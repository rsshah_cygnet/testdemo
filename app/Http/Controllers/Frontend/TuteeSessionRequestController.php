<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Notification\Notification;
use App\Models\Session\Session;
use App\Repositories\Frontend\TuteeSessionRequest\TuteeSessionRequestRepositoryContract;
use Illuminate\Http\Request;
use Carbon\Carbon;
class TuteeSessionRequestController extends Controller {
	/**
	 * @var Tutor Repository Contact
	 */
	protected $repositorycontract;

	/**
	 * @param TutorRepositoryContract
	 */
	public function __construct(TuteeSessionRequestRepositoryContract $repositorycontract) {
		$this->repositorycontract = $repositorycontract;
	}

	/**
	 *
	 */
	public function showPendingSession(Request $request) {

		if(!$request->ajax()){
        	if (!check_front_login()) {
				return redirect('/');
			}
        }

		$sessionData = [
			"front_user_id" => \Session::get('front_user_id'),
			"front_user_role" => \Session::get('front_user_role'),
			"front_user_first_name" => \Session::get('front_user_first_name'),
			"front_user_last_name" => \Session::get('front_user_last_name'),
			"front_user_username" => \Session::get('front_user_username'),
			"front_user_email" => \Session::get('front_user_email'),
			"front_user_country_id" => \Session::get('front_user_country_id'),
			"front_user_state_id" => \Session::get('front_user_state_id'),
			"front_user_city_id" => \Session::get('front_user_city_id'),
			"front_user_nationality" => \Session::get('front_user_nationality'),
			"front_user_lang_id" => \Session::get('front_user_lang_id'),
			"front_user_photo" => \Session::get('front_user_photo'),
			"front_user_timezone_id" => \Session::get('front_user_timezone_id'),
			 "current_role"           => \Session::get('current_role'),
		]; //dd($sessionData);
		$crnt_date = new \DateTime;
        $current_date = $crnt_date->format('Y-m-d H:i:s');
        $update_data = \DB::statement("UPDATE `session` SET `status` = '9' where `scheduled_date_time` <= '" . $current_date . "' AND (`status` = '5' OR `status` = '2') AND `tutee_id` = " . $sessionData['front_user_id']); //7 mns none of join, 2 mns upcoming
		$notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

		$notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

			$sessionRequest = $this->repositorycontract->getPendingSession();

		

		if($request->ajax())
        {
	          	$returnHTML = view('frontend.tutee.mid_section_sessionRequest',
	          	 [
	          	 	'sessionRequest' => $sessionRequest,
	          	  	'sessionData' => $sessionData,
	          	  	'notifications' => $notifications,
	          	  	'notificationCount' => $notificationCount
	          	])->render();
			return response()->json(array('status' => "success", 'html' => $returnHTML));
        }else{
        	return view('frontend.tutee.sessionRequest')
			->with('sessionRequest', $sessionRequest)
			->with('sessionData', $sessionData)
			->with('notifications', $notifications)
			->with('notificationCount', $notificationCount);
        }
	}

	// /**
	//  * Accept Tutee session request
	//  *
	//  * $id as session id
	//  */
	// public function acceptRequest($id) {
	// 	if (!check_front_login()) {
	// 		return redirect('/');
	// 	}
	// 	$acceptSessionRequest = $this->repositorycontract->acceptSessionRequest($id);
	// 	if ($acceptSessionRequest == false) {
	// 		\Session::flash('message', 'Something is wrong !!!');
	// 		return redirect()->route('tutor.pending.session');
	// 	} else {
	// 		\Session::flash('successmessage', 'Session Request has been accepted !!!');
	// 		return redirect()->route('tutor.pending.session');
	// 	}
	// }

	/**
	 * Accept Tutee session request
	 *
	 * $id as session id
	 */
	public function declineRequest($id) {
		
		$sessionDetail = Session::find($id);
		if(!empty($sessionDetail)){
            $tutee_id       = \Session::get('front_user_id');
            $crnt_date      = new \DateTime;
            $current_date   = $crnt_date->format('Y-m-d H:i:s');
            if($sessionDetail->end_date_time < $current_date){
                $update_data    = \DB::statement("UPDATE `session` SET `status` = '7' where `end_date_time` <= '" . $current_date . "' AND `status` = '5' AND `tutee_id` = '" . $tutee_id . "' AND `id` = " . $id); //7 mns none of join, 2 mns  
                //\Session::flash('flash_danger', 'Your session is expired.');
                $array = array('status' => 'error','msg' => 'Your session is expired.');
            	return json_encode($array);
            }

            if($sessionDetail->scheduled_date_time < $current_date){
                $update_data    = \DB::statement("UPDATE `session` SET `status` = '9' where `scheduled_date_time` <= '" . $current_date . "' AND `status` = '5' AND `tutee_id` = '" . $tutee_id . "' AND `id` = " . $id); //7 mns none of join, 2 mns  
                /*\Session::flash('flash_danger', "You can't cancel session request which time has gone.");*/
                $array = array('status' => 'error','msg' => "You can't cancel session request which time has gone.");
            	return json_encode($array);
            }
        }
		$acceptSessionRequest = $this->repositorycontract->declineSessionRequest($id);
		if ($acceptSessionRequest == false) {
			//\Session::flash('message', 'Something is wrong !!!');
			$array = array('status' => 'error','msg' => "Something is wrong !!!");
            	return json_encode($array);
		} else {
			$sessionData = [
            "front_user_first_name" => \Session::get('front_user_first_name'),
            "front_user_last_name" => \Session::get('front_user_last_name'),
            ];
            /*Notification:Tutee cancels session*/
            $data           = Session::find($id);
            $subject_id        = $data->subject_id;
            $topic_id        = $data->topic_id;
            $tutee_id       = $data->tutee_id;
            $tutor_id       = $data->tutor_id;
            $from_date_time = $data->scheduled_date_time;
            if (isset($topic_id) && $topic_id != 0 && $topic_id != "") {
                $Hierarchy  =  getCurriculumHierarchy('', $topic_id, true); //for topic
            } else {
                $Hierarchy  = getCurriculumHierarchy($subject_id, '', true); //for subject
            }
            $name           = $sessionData['front_user_first_name']." ".$sessionData['front_user_last_name'];

            $tutor_timezone = getTimezoneName($tutor_id);

            $date = ConverTimeZoneForUser($from_date_time, 'd-m-Y',$tutor_timezone) . " | " . ConverTimeZoneForUser($from_date_time, 'h:i a',$tutor_timezone);
            $message        = "<a href='/tutor-sessions'>Your session request for curriculum ".$Hierarchy." has been cancelled by <b>".$name."</b> for the session on ". $date ."</a>";
            
            $Notification = Notification::insert([
            'from_user_id' => $tutee_id,
            'to_user_id' => $tutor_id,
            'notification_type_id' => 7,
            'is_read' => 0,
            'is_read_admin' => 0,
            'is_for_admin' => 0,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
            ]);
            /*Notification end*/
			//\Session::flash('successmessage', 'Session Request has been cancelled !!!');
			$array = array('status' => 'success','msg' => "Session Request has been cancelled successfully !!!");
            	return json_encode($array);
		}
	}

	public function declineRequestModel($id) {

		if (!check_front_login()) {
			return redirect('/');
		}
		
		$data = Session::find($id);
		if (!empty($data)) {
			$returnHTML = view('frontend.tutee.model')
				->with(['id' => $id])
				->render();
			return response()->json(array($returnHTML));
		} else {
			\Session::flash('message', 'Something is wrong !!!');
			return redirect()->route('tutee.pending.session');
		}
	}
}
