<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Notification\Notification;

class NotificationController extends Controller
{
    public function index(Request $request)
    {

        if(!$request->ajax())
        {
            if (!check_front_login()) {
                return redirect('/');
            }
        }
        
        $sessionData = [
            "front_user_id" => \Session::get('front_user_id'),
            "front_user_role" => \Session::get('front_user_role'),
            "front_user_first_name" => \Session::get('front_user_first_name'),
            "front_user_last_name" => \Session::get('front_user_last_name'),
            "front_user_username" => \Session::get('front_user_username'),
            "front_user_email" => \Session::get('front_user_email'),
            "front_user_country_id" => \Session::get('front_user_country_id'),
            "front_user_state_id" => \Session::get('front_user_state_id'),
            "front_user_city_id" => \Session::get('front_user_city_id'),
            "front_user_nationality" => \Session::get('front_user_nationality'),
            "front_user_lang_id" => \Session::get('front_user_lang_id'),
            "front_user_photo" => \Session::get('front_user_photo'),
            "front_user_timezone_id" => \Session::get('front_user_timezone_id'),
            "current_role" => \Session::get('current_role'),
        ]; //dd($sessionData['front_user_role']);

      
        $notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->count();

        $notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();
        $loginUser = \Session::get('front_user_id'); 
        $total = Notification::where('to_user_id', $loginUser)->count();

       /* if(isset($sessionData['front_user_role']) && $sessionData['front_user_role'] == 2){
            return view('frontend.notification.notification_tutee',compact('total','sessionData','notifications','notificationCount'));
        }else{*/
           /* return view('frontend.notification.notification',compact('total','sessionData','notifications','notificationCount'));    */
       // }

        if($request->ajax())
        {
                $returnHTML = view('frontend.notification.mid_section_notification',compact('total','sessionData','notifications','notificationCount'))->render();
            return response()->json(array('status' => "success",'html' => $returnHTML));
        }else{
                 return view('frontend.notification.notification',compact('total','sessionData','notifications','notificationCount'));   
        }   
    	
    }

    public function getNotificationList(Request $request)
    {  
        $offset = $request->hidden_limit; 

        if(!check_front_login())
        {
               return redirect('/');
        }
        
        $loginUser = \Session::get('front_user_id'); 
       
        $notifications = Notification::where('to_user_id', $loginUser)->where('is_for_admin', 0)->orderBy('id', 'asc')->offset($offset)->limit(10)->get();

        $isNotRead = Notification::where('is_read', 0)->where('to_user_id', $loginUser)->where('is_for_admin', 0)->count();
        if(!empty($notifications) && isset($notifications))
        {   
            foreach ($notifications as $key => $value) {
                \DB::table('notifications')->where('id', $notifications[$key]['id'])->update(['is_read' => 1]);
            }
        }
        

        $total = Notification::where('to_user_id', $loginUser)->where('is_for_admin', 0)->count();
        
        $from = $request->hidden_limit + 1;
        $to = $from + 9;  
        if($to >= $total)
        {
            $to = $total;
        }

        return view('frontend.includes.notificationlist',compact('notifications','total','from', 'to','isNotRead'))->render();
    }
    
    public function getNotificationListAjax(Request $request)
    {
        // if(!empty($notifications) && isset($notifications))
     //    {   
     //        foreach ($notifications as $key => $value) {
     //            \DB::table('notifications')->where('id', $notifications[$key]['id'])->update(['is_read' => 1]);
     //        }
     //    }
     //    $isNotRead = Notification::where('is_read', 0)->where('to_user_id', $loginUser)->count();
    }


    public function getUpdatesNotificationList(){
        $sessionData = [
            "front_user_id" => \Session::get('front_user_id'),
            "current_role" => \Session::get('current_role'),
        ]; 
        
        $notificationCount = Notification::where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->where('is_for_admin', 0)->count();
    /*    $final_count = 0;
        if($notificationCount >= 3){
            $final_count = $notificationCount - 3;
        }else if($notificationCount == 2){
            $final_count = $notificationCount - 2;
        }else if($notificationCount == 1){
            $final_count = $notificationCount - 1;
        }   */

        $notifications = Notification::select('message', 'created_at')->where('to_user_id', $sessionData['front_user_id'])->where('is_read', 0)->where('is_for_admin', 0)->orderBy('id', 'desc')->limit(3)->get()->toArray();

          $returnHTML = view('frontend.includes.new_notificationlist', 
            [
                'notifications' => $notifications,
                'notificationCount' => $notificationCount,
                
            ])->render();
            return response()->json(array('status' => "success",'notificationCount' => $notificationCount,'html' => $returnHTML));
    }
}
