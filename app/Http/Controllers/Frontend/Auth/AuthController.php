<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Events\Frontend\Auth\UserLoggedOut;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Auth\LoginRequest;
use App\Models\Access\User\User;
use App\Repositories\Frontend\User\UserContract;
use App\Services\Access\Traits\AuthenticatesAndRegistersUsers;
use App\Services\Access\Traits\ConfirmUsers;
use App\Services\Access\Traits\UseSocialite;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

/**
 * Class AuthController
 * @package App\Http\Controllers\Frontend\Auth
 */
class AuthController extends Controller {

	use AuthenticatesAndRegistersUsers,
	ConfirmUsers,
	ThrottlesLogins,
		UseSocialite;

	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Where to redirect users after they logout
	 *
	 * @var string
	 */
	protected $redirectAfterLogout = '/';

	/**
	 * @param UserContract $user
	 */
	public function __construct(UserContract $user) {
		$this->user = $user;
	}

	/**
	 * GET METHOD - It will show login form for backend
	 * @return mixed
	 */
	public function showAdminLoginForm(Request $request) {
		//echo 'Back'; exit;
		$AdminloginData = $request->cookie('AdminloginData');

		return view('backend.auth.login', compact('AdminloginData'))
			->withSocialiteLinks($this->getSocialLinks());
	}

	/**
	 * POST METHOD - It will login user at backend
	 * @param LoginRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function adminLogin(LoginRequest $request) {

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		$throttles = in_array(
			ThrottlesLogins::class, class_uses_recursive(get_class($this))
		);

		if ($throttles && $this->hasTooManyLoginAttempts($request)) {
			return $this->sendLockoutResponse($request);
		}

		// Below condition is to check logging user is admin or not, bcz we are using single Auth functionality
		//$User = User::select('is_customer')->where('email', $request->get('email'))->first();
		$isAdmin = true;
		/* if(is_null($User)) {
			            $isAdmin = false;
			        } else if($User->is_customer == 1) {
			            $isAdmin = false;
		*/
		// Above condition is to check logging user is admin or not, bcz we are using single Auth functionality

		if ($isAdmin === true) {
			if (auth()->attempt($request->only($this->loginUsername(), 'password'), $request->has('remember'))) {
				// dd($request->all());
				if ($request->has('remember')) {
					$email = $request->email;
					$password = $request->password;
					$userCredentials = [$email, $password];
					return $this->handleUserWasAuthenticated($request, $throttles)->withCookie(cookie::forever('AdminloginData', $userCredentials));
				}
				return $this->handleUserWasAuthenticated($request, $throttles);
			}
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		if ($throttles) {

			$this->incrementLoginAttempts($request);
		}

		$email = $request->email;
		$emailExist = \DB::table('users')->where('email', $email)->first();
		if ($emailExist == null) {
			$validMsg = 'Email Id is wrong.';
		} else {
			$validMsg = 'Password is wrong.';
		}
		return redirect()->back()
			->withInput($request->only($this->loginUsername(), 'remember'))
			->withErrors([
				$this->loginUsername() => $validMsg,
			]);
	}

	/**
	 * GET METHOD - It will logour user from backend
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function adminLogout() {
		//echo 'Back'; exit;
		/**
		 * Remove the socialite session variable if exists
		 */
		if (app('session')->has(config('access.socialite_session_name'))) {
			app('session')->forget(config('access.socialite_session_name'));
		}

		event(new UserLoggedOut(access()->user()));
		auth()->logout();
		return redirect('/admin/login');
	}

}
