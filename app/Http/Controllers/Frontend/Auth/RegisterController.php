<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Auth\RegisterEducationRequest;
use App\Http\Requests\Frontend\Auth\RegisterPaymentRequest;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Http\Requests\Frontend\Auth\RegisterTutorCurriculumRequest;
use App\Http\Requests\Frontend\Auth\UploadImageRequest;
use App\Models\Country\Country;
use App\Models\Curriculum\Curriculum;
use App\Models\Frontusers\Frontusers;
use App\Models\Language\Language;
use App\Models\Qualification\Qualification;
use App\Models\Session\Session;
use App\Models\Timezone\Timezone;
use App\Models\TutorDetail\TutorDetail;
use App\Models\UserEducation\UserEducation;
use App\Repositories\Backend\Curriculum\CurriculumRepositoryContract;
use App\Repositories\Backend\Frontusers\FrontusersRepositoryContract;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Models\Notification\Notification;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class RegisterController extends Controller
{

    public function __construct(FrontusersRepositoryContract $user, CurriculumRepositoryContract $curriculum_repository)
    {
        $this->curriculum_repository = $curriculum_repository;
        $this->user                  = $user;

    }

    public function showRegistrationForm(Request $request)
    {

        if (check_front_login()) {
            return redirect(url('/'));
        }

        //------
        $request->session()->forget('user_register');

        $curriculum    = Curriculum::where('status', 1)->pluck('curriculum_name', 'id')->all();
        $country       = Country::where('status', 1)->pluck('country_name', 'id')->all();
        $timezone      = Timezone::getAll();
        $language      = Language::getAll();
        $qualification = Qualification::getAll();

        //  $item = State::with('country')->where('country_id',3)->get()->all();
        //  echo "<pre>";print_r($item);die;

        $data = array('curriculum' => $curriculum,
            'country'                  => $country,
            'timezone'                 => $timezone,
            'language'                 => $language,
            'qualification'            => $qualification);
        return view('frontend.auth.register', $data);
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)
    {

        if (config('access.users.confirm_email')) {
            $user = $this->user->create($request->all());
            event(new UserRegistered($user));
            return redirect()->route('frontend.index')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_confirm'));
        } else {
            auth()->login($this->user->create($request->all()));
            event(new UserRegistered(access()->user()));
            return redirect($this->redirectPath());
        }
    }

    public function register_basic(RegisterRequest $request)
    {
        // dd($request->all());
        $user    = $this->user->create_basic($request->all());
        // dd($user);
        $myArray = json_decode($user, true);
        if (isset($myArray['id'])) {
            $languages = ($request['lang_id']);

            $data['first_name']  = isset($request['first_name']) ? $request['first_name'] : '';
            $data['role']        = isset($request['hidden_user_role']) ? $request['hidden_user_role'] : '';
            $data['last_name']   = isset($request['last_name']) ? $request['last_name'] : '';
            $data['gender']      = isset($request['gender']) ? $request['gender'] : '';
            $data['password']    = isset($request['password']) ? md5($request['password']) : '';
            $data['email']       = isset($request['email']) ? $request['email'] : '';
            $data['nationality'] = isset($request['nationality']) ? $request['nationality'] : '';
            $data['dob']         = isset($request['dob']) ? date("Y-m-d", strtotime($request['dob'])) : '';
            $data['photo']       = isset($request['photo']) ? $request['photo'] : '';
            $data['country_id']  = isset($request['country_id']) ? $request['country_id'] : '';
            $data['state_id']    = isset($request['state_id']) ? $request['state_id'] : '';
            $data['city_id']     = isset($request['city_id']) ? $request['city_id'] : '';
            $data['timezone_id'] = isset($request['timezone_id']) ? $request['timezone_id'] : '';
            $data['lang_id']     = isset($languages) ? $languages : '';
            $data['status']      = isset($request['status']) ? $request['status'] : '2';
            $data['status']      = isset($request['status']) ? $request['status'] : '2';
            $data['sign_with']   = isset($request['signup_with']) ? $request['signup_with'] : '';

            $request->session()->put('user_register.first_step', $data);
            //print_r($request->session()->get('user_register'));
        }
        print_r($user);
    }

    public function register_education(RegisterEducationRequest $request)
    {

        $error = array();

        if ($request['hidden_user_role'] == 2) {

            $messages = [
                'qualification_id.required' => 'The Qualification field is required',
               
                'captcha.captcha'           => 'The Captcha validation fail',
                'paypal_registered_email_payment.required' => 'The Paypal Email field is required',
            ];
            $rule = [
                'qualification_id' => 'required',
              
                'captcha'          => 'required|captcha',
                'paypal_registered_email_payment' => 'required|email',
            ];

        } else {
            $messages = [
                'qualification_id.required' => 'The Qualification field is required',
                'paypal_registered_email_payment.required' => 'The Paypal Email field is required',
            ];
            $rule = [
                'qualification_id' => 'required',
                'paypal_registered_email_payment' => 'required|email',
            ];
        }

       /* if ($request['grade_type'] == 1) {
            if ($request['topic_id'] == '') {
                $messages['topic_id.required'] = 'The topic field is required';
                $rule['topic_id']              = 'required';
                 
            }

        }*/

        if(in_array($request['qualification_id'],array('1','2','3')))
        {
            $messages['curriculum_id.required'] = 'The Curriculum field is required';
            $rule['curriculum_id']              = 'required';

            $data['curriculum_id']                   = isset($request['curriculum_id']) ? $request['curriculum_id'] : '';
            $data['education_id']                    = isset($request['education_id']) ? $request['education_id'] : '';
            $data['level_id']                        = isset($request['level_id']) ? $request['level_id'] : '';
            $data['grade_id']                        = isset($request['grade_id']) ? ($request['grade_id']) : '';
            $data['program_id']                      = isset($request['program_id']) ? $request['program_id'] : '';
            $data['subject_id']                      = isset($request['subject_id']) ? $request['subject_id'] : '';
            $data['topic_id']                        = isset($request['topic_id']) ? $request['topic_id'] : '';
        }
        else
        {
            $messages['subject_name.required'] = 'The subject name is required';
                $rule['subject_name']              = 'required';
                $data['subject_name']                        = isset($request['subject_name']) ? $request['subject_name'] : '';
        }

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            $errors['error']            = 'error';
            $errors['validation_error'] = $validator->errors();
            $err                        = object_to_array($errors);
            $err_json                   = json_encode($errors);
            print_r($err_json);die;

        }


        $data['qualification_id']                = isset($request['qualification_id']) ? $request['qualification_id'] : '';
        
        $data['educational_details']             = isset($request['educational_details']) ? $request['educational_details'] : '';
        $data['occupation_details']              = isset($request['occupation_details']) ? $request['occupation_details'] : '';
        $data['paypal_registered_email_payment'] = isset($request['paypal_registered_email_payment']) ? $request['paypal_registered_email_payment'] : '';

        $request->session()->put('user_register.second_step', $data);
        if ($request['hidden_user_role'] == 2) {
            \Session::set('activationLinkSendForTutee', 'Your account activation link has been send to your email id.');
            $user_id      = $this->save_first_step($request);
            $education_id = $this->save_second_step($user_id, $request);
            $this->user->mark($user_id, '0');
            $this->sentActivationLink($user_id);

            /*Notification:Report on Tutor / Tutee submitted by registered users */
        $user_data = Frontusers::select('*')->where('id', $user_id)->get()->toArray();
        $name = $user_data['0']['first_name']." ".$user_data['0']['last_name'];
        $message = "New User registered with name <b>".$name."</b>";
        $Notification = Notification::insert([
            'from_user_id' => 0,
            'to_user_id' => 0,
            'notification_type_id' => 13,
            'is_read' => 0,
            'is_read_admin' => 0,
            'is_for_admin' => 1,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
        ]);
        /*Notification end*/

        }
        $array = array('id' => 'success');
        return json_encode($array);

    }

/***

for tutor learning data

 **/
    public function register_tutor_education(RegisterPaymentRequest $request)
    {

        $messages = [
            'captcha_payment.captcha'  => 'The Captcha validation fail',
            'captcha_payment.required' => 'The Captcha field is required',
            //'paypal_registered_email_payment.required' => 'The Paypal Email field is required',

        ];
        $rule = [
            // 'captcha_payment' => 'required|captcha',
            //'paypal_registered_email_payment' => 'required|email',
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            $errors['error']            = 'error';
            $errors['validation_error'] = $validator->errors();
            return json_encode($errors);
        }

        //save data
        $user_id      = $this->save_first_step($request);
        $education_id = $this->save_second_step($user_id, $request);
        $final_id     = $this->save_third_step_curriculumn($user_id, $request);

        /*Notification:Report on Tutor / Tutee submitted by registered users */
        $user_data = Frontusers::select('*')->where('id', $user_id)->get()->toArray();
        $name = $user_data['0']['first_name']." ".$user_data['0']['last_name'];
        $message = "New User registered with name <b>".$name."</b>";
        $Notification = Notification::insert([
            'from_user_id' => 0,
            'to_user_id' => 0,
            'notification_type_id' => 13,
            'is_read' => 0,
            'is_read_admin' => 0,
            'is_for_admin' => 1,
            'created_at' => Carbon::now()->toDateTimestring(),
            'message' => $message,
        ]);
        /*Notification end*/

        // $obj = UserEducation::find($education_id);
        // $obj->paypal_registered_email = isset($request['paypal_registered_email_payment']) ? $request['paypal_registered_email_payment'] : '';

        //if ($obj->save()) {
        \Session::set('activationLinkSendForTutor', 'Your account activation link has been send to your email id.');
        \Session::set('examLinkSendToEmail', 'Your exams link has been send to your email id.');

        $error['id'] = 'success';
        $this->user->mark($user_id, '0');
        $this->sentActivationLink($user_id);
        $this->sentExamlinks($user_id);
        return json_encode($error);
        // } else {
        //     throw new GeneralException(trans('exceptions.backend.create_error'));
        // }

    }

    public function register_tutor_curriculum(RegisterTutorCurriculumRequest $request)
    {

        $error = array();

        if ($request['grade_type'] == 1) {
            if ($request['topic_id_payment'] == '') {
                $error['topic_error'] = 'The Topic field is required';
                return $error;
            }

        }

        $allValue['cu'] = isset($request['curriculum_id_payment']) ? $request['curriculum_id_payment'] : '';
        $allValue['gd'] = isset($request['grade_id_payment']) ? $request['grade_id_payment'] : '';
        $allValue['lv'] = isset($request['level_id_payment']) ? $request['level_id_payment'] : '';
        $allValue['sb'] = isset($request['subject_id_payment']) ? $request['subject_id_payment'] : '';
        $allValue['tp'] = isset($request['topic_id_payment']) ? $request['topic_id_payment'] : '';
        $allValue['es'] = isset($request['education_id_payment']) ? $request['education_id_payment'] : '';
        $allValue['pg'] = isset($request['program_id_payment']) ? $request['program_id_payment'] : '';

        //session curriculumn
        $curriculum = ($request->session()->get('user_register.register_curriculum'));
        if (isset($curriculum) && !empty($curriculum)) {
            foreach ($curriculum as $key => $value) {
                $array_diff = array();
                $array_diff = (array_diff_assoc($value, $allValue));
                if (empty($array_diff)) {
                    $error['curriculumn_error'] = 'This heirarchy is already added before';
                    return $error;
                }
            }
        }

        //$request->session()->put('session_preference_curriculum_last_added', (int)3);
        //$request->session()->forget('session_preference_curriculum_last_added');
        $count = (int) $request->session()->get('user_register.curriculum_last_added');
        $count = $count + 1;

        //$request->session()->forget('session_preference_curriculum_last_added');
        $request->session()->put('user_register.curriculum_last_added', $count);
        $final_array = $allValue;

        $request->session()->put('user_register.register_curriculum.curriculum_' . $count, $allValue);
        //print_r($request->session()->get('user_register'));
        $array = array('id' => 'curriculum_' . $count);
        return $array;
    }

    public function save_first_step(Request $request)
    {
        //save data
        $obj  = new Frontusers;
        $data = $request->session()->pull('user_register.first_step');

        $obj->first_name  = isset($data['first_name']) ? $data['first_name'] : '';
        $obj->role        = isset($data['role']) ? $data['role'] : '';
        $obj->last_name   = isset($data['last_name']) ? $data['last_name'] : '';
        $obj->gender      = isset($data['gender']) ? $data['gender'] : '';
        $obj->password    = isset($data['password']) ? ($data['password']) : '';
        $obj->email       = isset($data['email']) ? $data['email'] : '';
        $obj->nationality = isset($data['nationality']) ? $data['nationality'] : '';
        $obj->dob         = isset($data['dob']) ? ($data['dob']) : '';
        $obj->photo       = isset($data['photo']) ? $data['photo'] : '';
        $obj->country_id  = isset($data['country_id']) ? $data['country_id'] : '';
        $obj->state_id    = isset($data['state_id']) ? $data['state_id'] : '';
        $obj->city_id     = isset($data['city_id']) ? $data['city_id'] : '';
        $obj->timezone_id = isset($data['timezone_id']) ? $data['timezone_id'] : '';
        $obj->lang_id     = isset($data['lang_id']) ? $data['lang_id'] : '';
        $obj->status      = isset($data['status']) ? $data['status'] : '2';
        $obj->contact_no  = isset($data['contact_no']) ? $data['contact_no'] : '';
        $obj->sign_with   = isset($data['sign_with']) ? $data['sign_with'] : '';
        
        if ($obj->save()) {
            $user_id = $obj->id;
            return $user_id;
        }
    }

    public function save_second_step($user_id, Request $request)
    {
        $data_second              = $request->session()->pull('user_register.second_step');

        $obj                      = new UserEducation;
        $obj->front_user_id       = isset($user_id) ? $user_id : '';
        $obj->qualification_id    = isset($data_second['qualification_id']) ? $data_second['qualification_id'] : '';
        
        if(in_array($obj->qualification_id,array('1','2','3')))
        {
            $obj->curriculum_id       = isset($data_second['curriculum_id']) ? $data_second['curriculum_id'] : '';
        $obj->education_system_id = isset($data_second['education_id']) ? $data_second['education_id'] : '';
        $obj->level_id            = isset($data_second['level_id']) ? $data_second['level_id'] : '';
        $obj->grade_id            = isset($data_second['grade_id']) ? $data_second['grade_id'] : '';
        $obj->program_id          = isset($data_second['program_id']) ? $data_second['program_id'] : '';
        $obj->subject_id          = isset($data_second['subject_id']) ? $data_second['subject_id'] : '';
        $obj->topic_id            = isset($data_second['topic_id']) ? $data_second['topic_id'] : '';
        }
        else
        {
            $obj->subject_name            = isset($data_second['subject_name']) ? $data_second['subject_name'] : '';
        }
        
        $obj->educational_details = isset($data_second['educational_details']) ? $data_second['educational_details'] : '';

        $obj->occupation_details = isset($data_second['occupation_details']) ? $data_second['occupation_details'] : '';

        $obj->paypal_registered_email = isset($data_second['paypal_registered_email_payment']) ? $data_second['paypal_registered_email_payment'] : '';
        if ($obj->save()) {
            $education_id = $obj->id;
            return $education_id;

        }
    }

    public function save_third_step_curriculumn($user_id, Request $request)
    {
        $curriculum_data = $request->session()->pull('user_register.register_curriculum');
        $request->session()->forget('user_register.curriculum_last_added');

        //save curiculumn data
        if (isset($curriculum_data) && !empty($curriculum_data)) {
            foreach ($curriculum_data as $key => $value) {
                $object                      = new TutorDetail;
                $object->front_user_id       = $user_id;
                $object->curriculum_id       = isset($value['cu']) ? $value['cu'] : '';
                $object->education_system_id = isset($value['es']) ? $value['es'] : '';
                $object->level_id            = isset($value['lv']) ? $value['lv'] : '';
                $object->grade_id            = isset($value['gd']) ? $value['gd'] : '';
                $object->program_id          = isset($value['pg']) ? $value['pg'] : '';
                $object->subject_id          = isset($value['sb']) ? $value['sb'] : '';
                $object->topic_id            = isset($value['tp']) ? $value['tp'] : '';
                $object->save();

            }
        }

    }
    public function delete_tutor_curriculum(Request $request)
    {

        $data = $request->all();
        $request->session()->forget('user_register.register_curriculum.' . $data['id']);
        $obj = TutorDetail::find($data['id']);
        if ($obj != '') {
            $obj->delete();
        }

    }

    public function register_user_photo(UploadImageRequest $request)
    {

        $imageName = "";

        if (isset($request->file) && $request->file('file') != null && $request->file('file') != "") {
            $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
            $dir       = public_path('uploads/user/');
            $filename  = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);
            echo $filename;

        }

    }

    /**
     * Send account actiovation link to user's email id
     */
    public function sentActivationLink($id)
    {

        $userData = Frontusers::find($id);
        if (!empty($userData)) {
            $userEmailId = $userData->email;
            $userId      = $userData->id;
            $userEmailId = $userData->email;
            $id          = \Crypt::encrypt($userId);
            $link        = URL('/') . '/active/account/' . $id;
            $mail        = \Mail::raw('Please click following link to active your account. ' . $link, function ($message) use ($userEmailId, $link) {
                $message->to($userEmailId);
                $message->subject('Your account activation link');
            });
            sleep(5);
           // return;
        }
       // return;
    }

    public function sentExamlinks($id)
    {
        $user = Frontusers::find($id);
        if (!empty($user)) {
            $userData = \DB::table('tutor_details')
                ->select('id', 'topic_id', 'subject_id')
                ->where('front_user_id', $user->id)
                ->where('appeared', 0)
                ->where('is_completed', 0)
                ->get();

            $userEmailId = $user->email;

            $link = array();
            foreach ($userData as $id) {
                if (isset($id->topic_id) && $id->topic_id != 0 && $id->topic_id != "") {
                    $curriculum = getCurriculumHierarchy('', $id->topic_id, true);
                } else if (isset($id->subject_id) && $id->subject_id != 0 && $id->subject_id != "") {
                    $curriculum = getCurriculumHierarchy('', $id->subject_id, true);
                }
                $ExamlId = \Crypt::encrypt($id->id);

                $link[] = $curriculum . ": " . URL('/') . '/test/' . $ExamlId . " , \n \n";
            }

            $makeExamLink = str_replace(array(','), '', $link);
            $Examlink     = implode(" ", $makeExamLink);
            $appName      = config('app.name');
            $mail         = \Mail::raw($Examlink, function ($message) use ($Examlink, $userEmailId) {
                $message->to($userEmailId);
                $message->subject('Your Exam links');
            });
            sleep(5);
            //return;
            // if (\Mail::failures()) {
            //     dd('fail');
            // } else {
            //     return;
            // }
        }
    }

    public function activeUser($id)
    {
        $userId = \Crypt::decrypt($id);

        $findUser = Frontusers::find($userId);
        // dd($findUser);

        if (!empty($findUser)) {

            $activeUser = \DB::table('front_user')
                ->where('id', $userId)
                ->update([
                    'status' => 1,
                ]);
            // \Session::set('activeAccount', 'Your account activated successfully.');
            // return redirect('/');
                return view('frontend.auth.activeaccount');
        } else {
            \Session::flash('InvalidActivation', 'Something is wrong. Your account is not activated.');
            return redirect('/');
        }

    }


    public function saveFbUser(Request $request)
    {
        $data = $request->all();
        if(!empty($data)){
            if(!empty($data['dob'])){
                $data['dob'] =  date("Y-m-d", strtotime($data['dob']));    
            }
            
            $user_data = Frontusers::select('*')
                        ->where('email',$data['email'])
                        ->where('sign_with',1)
                        ->whereNull('deleted_at')
                        ->get()->toArray();


        if(!empty($user_data)){
            $first_name = isset($user_data['0']['first_name'])?$user_data['0']['first_name']:"";
            $last_name = isset($user_data['0']['last_name'])?$user_data['0']['last_name']:"";
            $id = isset($user_data['0']['id'])?$user_data['0']['id']:"";
            $role = isset($user_data['0']['role'])?$user_data['0']['role']:"";
            $email = isset($user_data['0']['email'])?$user_data['0']['email']:"";
            $country_id = isset($user_data['0']['country_id'])?$user_data['0']['country_id']:"";
            $state_id = isset($user_data['0']['state_id'])?$user_data['0']['state_id']:"";
            $city_id = isset($user_data['0']['city_id'])?$user_data['0']['city_id']:"";
            $nationality = isset($user_data['0']['nationality'])?$user_data['0']['nationality']:"";
            $lang_id = isset($user_data['0']['lang_id'])?$user_data['0']['lang_id']:"";
            $photo = isset($user_data['0']['photo'])?$user_data['0']['photo']:"";
            $timezone_id = isset($user_data['0']['timezone_id'])?$user_data['0']['timezone_id']:"";
            
            $timezone_name = getTimezoneName($id);


                \Session::set('front_user_id', $id);
                \Session::set('front_user_role', $role);
                \Session::set('front_user_first_name', $first_name);
                \Session::set('front_user_last_name', $last_name);
                \Session::set('front_user_email', $email);
                \Session::set('front_user_country_id', $country_id);
                \Session::set('front_user_state_id', $state_id);
                \Session::set('front_user_city_id', $city_id);
                \Session::set('front_user_nationality', $nationality);
                \Session::set('front_user_lang_id', $lang_id);
                \Session::set('front_user_photo', $photo);
                \Session::set('front_user_timezone_id', $timezone_id);
                \Session::set('front_user_timezone_name', $timezone_name);

                 $array = array('status' => 'success','role' => $role);
                 return json_encode($array);
            }   


             $obj  = new Frontusers;
            $obj->first_name  = isset($data['first_name']) ? $data['first_name'] : '';
            $obj->last_name   = isset($data['last_name']) ? $data['last_name'] : '';
            $obj->gender      = isset($data['gender']) ? $data['gender'] : '';
            $obj->email       = isset($data['email']) ? $data['email'] : '';
            $obj->dob         = isset($data['dob']) ? ($data['dob']) : '';
            $obj->timezone_id = 572;
            $obj->role       = 1;
            $obj->status      = 1;
            $obj->sign_with   = 1;
            
            if ($obj->save()) {
                $user_id = $obj->id;
                 $timezone_name = getTimezoneName($obj->id);

                \Session::set('front_user_id', $user_id);
                \Session::set('front_user_role', $obj->role);
                \Session::set('front_user_first_name', $obj->first_name);
                \Session::set('front_user_last_name', $obj->last_name);
                \Session::set('front_user_email', $obj->email);
                \Session::set('front_user_timezone_id', $obj->timezone_id);
                \Session::set('front_user_timezone_name', $timezone_name);
              
                $array = array('status' => 'success','role' => $obj->role);
                return json_encode($array);
            }
        }
        
    }

}