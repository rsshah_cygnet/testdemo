<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Auth\RegisterEducationRequest;
use App\Http\Requests\Frontend\Auth\RegisterPaymentRequest;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Http\Requests\Frontend\Auth\RegisterTutorCurriculumRequest;
use App\Http\Requests\Frontend\Auth\UploadImageRequest;
use App\Models\Country\Country;
use App\Models\Curriculum\Curriculum;
use App\Models\Language\Language;
use App\Models\Qualification\Qualification;
use App\Models\Timezone\Timezone;
use App\Models\TutorDetail\TutorDetail;
use App\Models\UserEducation\UserEducation;
use App\Repositories\Backend\Curriculum\CurriculumRepositoryContract;
use App\Repositories\Backend\Frontusers\FrontusersRepositoryContract;
use Illuminate\Http\Request;
use Validator;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class RegisterController extends Controller
{

    public function __construct(FrontusersRepositoryContract $user, CurriculumRepositoryContract $curriculum_repository)
    {
        $this->curriculum_repository = $curriculum_repository;
        $this->user                  = $user;

    }

    public function showRegistrationForm()
    {

        $curriculum    = Curriculum::where('status', 1)->pluck('curriculum_name', 'id')->all();
        $country       = Country::where('status', 1)->pluck('country_name', 'id')->all();
        $timezone      = Timezone::getAll();
        $language      = Language::getAll();
        $qualification = Qualification::getAll();

        //  $item = State::with('country')->where('country_id',3)->get()->all();
        //  echo "<pre>";print_r($item);die;

        $data = array('curriculum' => $curriculum,
            'country'                  => $country,
            'timezone'                 => $timezone,
            'language'                 => $language,
            'qualification'            => $qualification);
        return view('frontend.auth.register', $data);
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)
    {

        if (config('access.users.confirm_email')) {
            $user = $this->user->create($request->all());
            event(new UserRegistered($user));
            return redirect()->route('frontend.index')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_confirm'));
        } else {
            auth()->login($this->user->create($request->all()));
            event(new UserRegistered(access()->user()));
            return redirect($this->redirectPath());
        }
    }

    public function register_basic(RegisterRequest $request)
    {
        $user = $this->user->create_basic($request->all());
        $myArray = json_decode($user, true);
        if(isset($myArray['id']))
        {
            $data['first_name'] = isset($request['first_name']) ? $request['first_name'] : '';
            $data['role'] = isset($request['hidden_user_role']) ? $request['hidden_user_role'] : '';
            $data['last_name'] = isset($request['last_name']) ? $request['last_name'] : '';
            $data['username'] = isset($request['username']) ? $request['username'] : '';
            $data['password'] = isset($request['password']) ? md5($request['password']) : '';
            $data['email'] = isset($request['email']) ? $request['email'] : '';
            $data['nationality'] = isset($request['nationality']) ? $request['nationality'] : '';
            $data['dob'] = isset($request['dob']) ? date("Y-m-d", strtotime($request['dob'])) : '';
            $data['photo'] = isset($request['photo']) ? $request['photo'] : '';
            $data['country_id'] = isset($request['country_id']) ? $request['country_id'] : '';
            $data['state_id'] = isset($request['state_id']) ? $request['state_id'] : '';
            $data['city_id'] = isset($request['city_id']) ? $request['city_id'] : '';
            $data['timezone_id'] = isset($request['timezone_id']) ? $request['timezone_id'] : '';
            $data['lang_id'] = isset($request['lang_id']) ? $request['lang_id'] : '';
            $data['status'] = isset($request['status']) ? $request['status'] : '2';

            $request->session()->put('user_register.first_step',$data);
            //print_r($request->session()->get('user_register'));
        }
        print_r($user);
    }

    public function register_education(RegisterEducationRequest $request)
    {

        $error = array();

        if ($request['hidden_user_role'] == 2) {

            $messages = [
                'qualification_id.required' => 'The Qualification field is required',
                'curriculum_id.required'    => 'The Curriculum field is required',
                'captcha.captcha'           => 'The Captcha validation fail',
            ];
            $rule = [
                'qualification_id' => 'required',
                'curriculum_id'    => 'required',
                'captcha'          => 'required|captcha',
            ];

        } else {
            $messages = [
                'qualification_id.required' => 'The Qualification field is required',
                'curriculum_id.required'    => 'The Curriculum field is required',
            ];
            $rule = [
                'qualification_id' => 'required',
                'curriculum_id'    => 'required',
            ];
        }

        if ($request['grade_type'] == 1) {
            if ($request['topic_id'] == '') {
                $messages['topic_id.required'] = 'The topic field is required';
                $rule['topic_id']              = 'required';
            }

        }

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            $errors['error']            = 'error';
            $errors['validation_error'] = $validator->errors();
            $err                        = object_to_array($errors);
            $err_json                   = json_encode($errors);
            print_r($err_json);die;

        }

        if (isset($request['hidden_user_education_id']) && $request['hidden_user_education_id'] != "" && $request['hidden_user_education_id'] != 0) {
            $obj = UserEducation::find($request['hidden_user_education_id']);

        } else {
            $obj = new UserEducation;
        }

        //
        $data['front_user_id']       = isset($request['hidden_user_id']) ? $request['hidden_user_id'] : '';
        $data['qualification_id ']   = isset($request['qualification_id']) ? $request['qualification_id'] : '';
        $data['curriculum_id']       = isset($request['curriculum_id']) ? $request['curriculum_id'] : '';
        $data['education_system_id'] = isset($request['education_id']) ? $request['education_id'] : '';
        $data['level_id'] = isset($request['level_id']) ? $request['level_id'] : '';
        $data['grade_id']            = isset($request['grade_id']) ? $request['grade_id'] : '';
        $data['program_id']          = isset($request['program_id']) ? $request['program_id'] : '';
        $data['subject_id']          = isset($request['subject_id']) ? $request['subject_id'] : '';
        $data['educational_details'] = isset($request['educational_details']) ? $request['educational_details'] : '';

        $data['occupation_details'] = isset($request['occupation_details']) ? $request['occupation_details'] : '';

            if ($request['hidden_user_role'] == 2) {
                $this->user->mark($data['front_user_id'], '1');
            }

            return json_encode($error);
        } else {
            throw new GeneralException(trans('exceptions.backend.create_error'));
        }

    }

/***

for tutor learning data

 **/
    public function register_tutor_education(RegisterPaymentRequest $request)
    {

        $messages = [
            'captcha_payment.captcha'                  => 'The Captcha validation fail',
            'captcha_payment.required'                 => 'The Captcha field is required',
            'paypal_registered_email_payment.required' => 'The Paypal Email field is required',

        ];
        $rule = [
            'captcha_payment'                 => 'required|captcha',
            'paypal_registered_email_payment' => 'required|email',
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            $errors['error']            = 'error';
            $errors['validation_error'] = $validator->errors();
            return json_encode($errors);
        }

        $obj = UserEducation::find($request['hidden_user_education_id']);

        $data['paypal_registered_email = isset($request['paypal_registered_email']) ? $request['paypal_registered_email'] : '';

        if ($data['save()) {
            $error['id'] = $data['id;
            $this->user->mark($request['hidden_user_id'], '1');
            return json_encode($error);
        } else {
            throw new GeneralException(trans('exceptions.backend.create_error'));
        }

    }

    public function register_tutor_curriculum(RegisterTutorCurriculumRequest $request)
    {

        $error = array();

        if ($request['grade_type'] == 1) {
            if ($request['topic_id_payment'] == '') {
                $error['topic_error'] = 'The Topic field is required';
                return $error;
            }

        }

        $obj = new TutorDetail;

        $data['front_user_id       = isset($request['hidden_user_id']) ? $request['hidden_user_id'] : '';
        $data['curriculum_id       = isset($request['curriculum_id_payment']) ? $request['curriculum_id_payment'] : '';
        $data['education_system_id = isset($request['education_id_payment']) ? $request['education_id_payment'] : '';
        $data['level_id            = isset($request['level_id_payment']) ? $request['level_id_payment'] : '';
        $data['grade_id            = isset($request['grade_id_payment']) ? $request['grade_id_payment'] : '';
        $data['program_id          = isset($request['program_id_payment']) ? $request['program_id_payment'] : '';
        $data['subject_id          = isset($request['subject_id_payment']) ? $request['subject_id_payment'] : '';
        $data['topic_id            = isset($request['topic_id_payment']) ? $request['topic_id_payment'] : '';

        if ($data['save()) {
            $error['id'] = $data['id;
            return $error;
        } else {
            throw new GeneralException(trans('exceptions.backend.create_error'));
        }
    }

    public function delete_tutor_curriculum(Request $request)
    {

        $data = $request->all();
        $obj  = TutorDetail::find($data['id']);
        $data['delete();

    }

    public function register_user_photo(UploadImageRequest $request)
    {

        $imageName = "";

        if (isset($request->file) && $request->file('file') != null && $request->file('file') != "") {
            $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
            $dir       = public_path('uploads/user/');
            $filename  = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);
            echo $filename;

        }

    }

}
