<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Auth\ResetPasswordRequest;
use App\Http\Requests\Frontend\Auth\SendResetLinkEmailRequest;
use App\Http\Requests\Frontend\User\ChangePasswordRequest;
use App\Models\Access\User\User;
use App\Repositories\Frontend\User\UserContract;
use App\Services\Access\Traits\ChangePasswords;
use App\Services\Access\Traits\ResetsPasswords;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Password;

/**
 * Class PasswordController
 * @package App\Http\Controllers\Frontend\Auth
 */
class PasswordController extends Controller {

	use ChangePasswords,
		ResetsPasswords;

	/**
	 * Where to redirect the user after their password has been successfully reset
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * @param UserContract $user
	 */
	public function __construct(UserContract $user) {
		$this->user = $user;
		config(['auth.passwords.users.email' => ['frontend.auth.emails.password', 'backend.auth.emails.password']]);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showAdminChangePasswordForm() {
		return view('backend.auth.passwords.change');
	}

	/**
	 * @param ChangePasswordRequest $request
	 * @return mixed
	 */
	public function changeAdminPassword(ChangePasswordRequest $request) {
		$this->user->changePassword($request->all());
		return redirect()->route('backend.user.index')->withFlashSuccess(trans('strings.backend.user.password_updated'));
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showAdminLinkRequestForm() {
		return view('backend.auth.passwords.email');
	}

	/**
	 * @param SendResetLinkEmailRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function sendAdminResetLinkEmail(SendResetLinkEmailRequest $request) {

		// Below condition is to check logging user is customer or not, bcz we are using single Auth functionality
		$User = User::where('email', $request->get('email'))->first();
		$isAdmin = true;
		/*if(is_null($User)) {
			            $isAdmin = false;
			        } else if($User->is_customer == 1) {
			            $isAdmin = false;
		*/
		// Above condition is to check logging user is customer or not, bcz we are using single Auth functionality

		if ($isAdmin === true) {

//            $emailView = 'backend.auth.emails.password';
			//
			//            $passwords = new PasswordBroker(
			//                App::make('TokenRepositoryInterface'),
			//                App::make('UserProvider'),
			//                App::make('MailerContract'),
			//                $emailView
			//            );
			//
			//            $response = $passwords->sendResetLink($request->only('email'), function(Message $message) {
			//                $message->subject(trans('strings.emails.auth.password_reset_subject'));
			//            });

			$response = Password::sendResetLink($request->only('email'), function (Message $message) {
				$message->subject(trans('strings.emails.auth.password_reset_subject'));
			});
		} else {
			$response = Password::INVALID_USER;
		}

		switch ($response) {
		case Password::RESET_LINK_SENT:
			return redirect()->back()->with('status', trans($response));

		case Password::INVALID_USER:
			return redirect()->back()->withErrors(['email' => trans($response)]);
		}
	}

	/**
	 * @param null $token
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showAdminResetForm($token = null) {
		if (is_null($token)) {
			return $this->showAdminLinkRequestForm();
		}

		return view('backend.auth.passwords.reset')
			->with('token', $token);
	}

	/**
	 * @param ResetPasswordRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function adminReset(ResetPasswordRequest $request) {
		$credentials = $request->only(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = Password::reset($credentials, function ($user, $password) {
			$this->resetPassword($user, $password);
		});

		switch ($response) {
		case Password::PASSWORD_RESET:
			return redirect($this->redirectPath())->with('status', trans($response));

		default:
			return redirect()->back()
				->withInput($request->only('email'))
				->withErrors(['email' => trans($response)]);
		}
	}

	/**
	 * @param $user
	 * @param $password
	 */
	protected function adminResetPassword($user, $password) {
		$user->password = bcrypt($password);
		$user->save();
		auth()->login($user);
	}

}
