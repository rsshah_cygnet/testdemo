<?php

namespace App\Http\Controllers;

use App\Models\Session\Session;
use App\Services\Access\Traits\SessionRequest;
use \DateTime;
use DB;
use Carbon\Carbon;
use App\Models\Notification\Notification;

class CronController extends Controller
{
    use SessionRequest;

    public function getRecordingSession()
    {

        $date = new DateTime;
        $date->modify('+15 minutes');
        $formatted_end_date = $date->format('Y-m-d H:i:s');

        $crnt_date    = new DateTime;
        $current_date = $crnt_date->format('Y-m-d H:i:s');

        $session_data = Session::select('*')
            ->where('end_date_time', '>', $formatted_end_date)
            ->where('status', 1)
            ->where('is_get_record_url', 0)
            ->get()
            ->toArray();

        foreach ($session_data as $key => $value) {

            $recording_data = $this->getRecording($value['id']);

            $obj                    = '';
            $obj                    = Session::find($value['id']);
            $obj->is_get_record_url = 1;

            if (empty($recording_data['messageKey'])) {
                $obj->record_url = $recording_data[0]['playbackFormatUrl'][0];

            }
            $obj->save();
        }

    }

    public function sendSessionStartNotification(){
        $crnt_date = new DateTime;
        $current_date = $crnt_date->format('Y-m-d H:i:s');
        $session_data = Session::select('tutee_timezone.timezone_name as timezone_tutee','tutor_timezone.timezone_name as timezone_tutor','session.id','session.tutor_id','session.tutee_id','session.scheduled_date_time',DB::raw('TIMESTAMPDIFF(MINUTE,
                                    "'.$current_date.'",scheduled_date_time) as diff'),
        DB::raw("CONCAT(tutee.first_name, ' ', tutee.last_name) as tutee_name"),DB::raw("CONCAT(front_user.first_name, ' ', front_user.last_name) as tutor_name"))
                ->leftJoin('front_user', 'front_user.id', '=', 'session.tutor_id')
                ->leftJoin('front_user as tutee', 'tutee.id', '=', 'session.tutee_id')
                 ->leftJoin('timezone as tutee_timezone', 'tutee_timezone.id', '=', 'tutee.timezone_id')
                  ->leftJoin('timezone as tutor_timezone', 'tutor_timezone.id', '=', 'front_user.timezone_id')
                ->where('session.status',2)
                ->whereRaw('TIMESTAMPDIFF(MINUTE,
                        "'.$current_date.'",session.scheduled_date_time) <= 278 ')
                ->get()->toArray();

       // dd($session_data);
        if(!empty($session_data)){
            foreach ($session_data as $key => $value) {
                
                $date_tutee = ConverTimeZoneForUser($value['scheduled_date_time'], 'd-m-Y',$value['timezone_tutee']) . " | " . ConverTimeZoneForUser($value['scheduled_date_time'], 'h:i a',$value['timezone_tutee']);

                 $date_tutor = ConverTimeZoneForUser($value['scheduled_date_time'], 'd-m-Y',$value['timezone_tutor']) . " | " . ConverTimeZoneForUser($value['scheduled_date_time'], 'h:i a',$value['timezone_tutor']);


                /*Notification(for tutee):Tutee receive notification for upcoming session starts in X hrs */
                $message = "<a href='/tutee-sessions'>Your session with <b>".$value['tutor_name']."</b> is scheduled on ". $date_tutee .".So,please be ready on time.</a>";
                $Notification = Notification::insert([
                    'from_user_id' => $value['tutor_id'],
                    'to_user_id' => $value['tutee_id'],
                    'notification_type_id' => 19,
                    'is_read' => 0,
                    'is_read_admin' => 0,
                    'is_for_admin' => 0,
                    'created_at' => Carbon::now()->toDateTimestring(),
                    'message' => $message,
                ]);
                /*Notification end*/

                /*Notification(for tutor):Tutor receive notification for upcoming session starts in X hrs */
                $message = "<a href='/tutor-sessions'>Your session with <b>".$value['tutee_name']."</b> is scheduled on ".$date_tutor.".So,please be ready on time.</a>";
                $Notification = Notification::insert([
                    'from_user_id' => $value['tutee_id'],
                    'to_user_id' => $value['tutor_id'],
                    'notification_type_id' => 18,
                    'is_read' => 0,
                    'is_read_admin' => 0,
                    'is_for_admin' => 0,
                    'created_at' => Carbon::now()->toDateTimestring(),
                    'message' => $message,
                ]);
                /*Notification end*/

            }
        }

    }

}
