<?php

Route::group(['middleware' => 'web'], function() {
    /**
     * Frontend Routes
     * Namespaces indicate folder structure
     */
    Route::group(['namespace' => 'Frontend'], function () {
        require(__DIR__ . '/Routes/Frontend/Frontend.php');
        require(__DIR__ . '/Routes/Frontend/Access.php');
    });

    /**
     * Socail login
     */
    Route::get('/redirect', '\App\Http\Controllers\Frontend\Auth\SocialAuthController@redirect');
    Route::get('/callback', '\App\Http\Controllers\Frontend\Auth\SocialAuthController@callback');
});

/**
 * Backend Routes
 * Namespaces indicate folder structure
 * Admin middleware groups web, auth, and routeNeedsPermission
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'middleware' => 'admin'], function () {
    /**
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    require(__DIR__ . '/Routes/Backend/Dashboard.php');
    require(__DIR__ . '/Routes/Backend/Access.php');
    require(__DIR__ . '/Routes/Backend/LogViewer.php');
});

/*
 * Api Routes
 * Namespaces indicate folder structure
 */
require (__DIR__ . '/Routes/Api/Apis.php');