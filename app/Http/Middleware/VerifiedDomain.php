<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Domain\Domain;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VerifiedDomain
{
    protected $Domain;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $domain = $_SERVER['SERVER_NAME'];
        $Domain = Domain::where('domain', $domain)->first();

        if (!$Domain) {
            throw new NotFoundHttpException;
        }

        $Site = $Domain->domain;

        if (!$Site) {
            throw new NotFoundHttpException;
        }

        $this->Domain = $Domain;

        return $next($request);
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->Domain;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return '';
        //return $this->Domain->domain;
    }

}
