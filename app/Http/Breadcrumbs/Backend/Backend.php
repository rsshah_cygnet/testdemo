<?php

require __DIR__ . '/Access.php';
require __DIR__ . '/LogViewer.php';

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
});

Breadcrumbs::register('admin.cmspages.index', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.cmspages.management'), route('admin.cmspages.index'));   
});

Breadcrumbs::register('admin.cmspages.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.cmspages.index');
    $breadcrumbs->push(trans('menus.backend.cmspages.create'), route('admin.cmspages.create'));
});

Breadcrumbs::register('admin.cmspages.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cmspages.index');
    $breadcrumbs->push(trans('menus.backend.cmspages.edit'), route('admin.cmspages.edit' , $id));
});

Breadcrumbs::register('admin.media.index', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Media Management', route('admin.media.index'));
});

/**
 * Admin Special Managment
 */
Breadcrumbs::register('admin.special-management.index', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Special Management', route('admin.special-management.index'));
});

Breadcrumbs::register('admin.special-management.create', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.special-management.index');
    $breadcrumbs->push('Create', route('admin.special-management.create'));
});

Breadcrumbs::register('admin.special-management.edit', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.special-management.index');
    $breadcrumbs->push('Edit');
});



/**
 * Dealer Special Managment
 */
Breadcrumbs::register('admin.dealer.special-management.index', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Manage Special', route('admin.dealer.special-management.index'));
});

Breadcrumbs::register('admin.dealer.special-management.create', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.dealer.special-management.index');
    $breadcrumbs->push('Create', route('admin.dealer.special-management.create'));
});

Breadcrumbs::register('admin.dealer.special-management.edit', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.dealer.special-management.index');
    $breadcrumbs->push('Edit');
});


/**
 * Admin New Car Specification
 */
Breadcrumbs::register('admin.specification.index', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('New Car Specification', route('admin.specification.index'));
});

Breadcrumbs::register('admin.specification.create', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.specification.index');
    $breadcrumbs->push('Create', route('admin.specification.create'));
});

Breadcrumbs::register('admin.specification.edit', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.specification.index');
    $breadcrumbs->push('Edit');
});

Breadcrumbs::register('admin.blog.index', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Blog Management', route('admin.blog.index'));
});

Breadcrumbs::register('admin.blog.create', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.blog.index');
    $breadcrumbs->push('Create Blog', route('admin.blog.create'));
});

Breadcrumbs::register('admin.blog.edit', function ($breadcrumbs,$id) {
	$breadcrumbs->parent('admin.blog.index');
    $breadcrumbs->push('Edit Blog', route('admin.blog.edit',$id));
});

/**
 * Admin Settings
 */
Breadcrumbs::register('admin.settings.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Settings', route('admin.settings.index'));
});

/**
 * Email Template Managment
 */
Breadcrumbs::register('admin.emailtemplate.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Email Template Management', route('admin.emailtemplate.index'));
});

Breadcrumbs::register('admin.emailtemplate.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.emailtemplate.index');
    $breadcrumbs->push('Create', route('admin.emailtemplate.create'));
});

Breadcrumbs::register('admin.emailtemplate.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.emailtemplate.index');
    $breadcrumbs->push('Edit');
});

/**
 * Deal Website Managment
 */
Breadcrumbs::register('admin.deal-website.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Deal Website Management', route('admin.deal-website.index'));
});

Breadcrumbs::register('admin.deal-website.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.deal-website.index');
    $breadcrumbs->push('Create', route('admin.deal-website.create'));
});

Breadcrumbs::register('admin.deal-website.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.deal-website.index');
    $breadcrumbs->push('Edit');
});

/**
 * Admin Old Car Specification
 */
Breadcrumbs::register('admin.old-specification.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Old Car Specification', route('admin.old-specification.index'));
});

Breadcrumbs::register('admin.old-specification.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.old-specification.index');
    $breadcrumbs->push('Create', route('admin.old-specification.create'));
});

Breadcrumbs::register('admin.old-specification.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.old-specification.index');
    $breadcrumbs->push('Edit');
});