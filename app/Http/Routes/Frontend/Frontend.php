<?php
//Route::get('/cal', 'LandingPageController@cal');
/**
 * Find A Tutor
 */
Route::any('/find-a-tutor', 'TutorController@index')->name('frontend.findTutor');
Route::any('/getTutorList', 'TutorController@getTutorList')->name('frontend.getTutorlist');
Route::any('/getTutorListAjax', 'TutorController@getTutorListAjax')->name('frontend.getTutorListAjax');
Route::any('/find', 'TutorController@searchTutor')->name('frontend.find');
Route::any('/tutorDetail/{id}', 'TutorController@tutorDetails')->name('frontend.tutorDetails');
Route::post('/viewreview', 'TutorController@getReview')->name('frontend.getreview');
Route::any('/tutor-dashboard', 'TutorController@dashboard')->name('frontend.tutorDashboard');
Route::any('/tutor-piechart', 'TutorController@filterPieChart');
Route::any('/tutor-columnchart', 'TutorController@filterColumnChart');
Route::any('/tutee-piechart', 'TuteeController@filterPieChart');
Route::any('/tutee-sessions', 'TuteeController@mySessions')->name('frontend.tuteeSessions');
Route::any('/get-session-data', 'TuteeController@getSessionModalData');
Route::post('/update_share_count', 'TuteeController@updateShareCount');
Route::post('/update_share_count_tutor', 'TutorController@updateShareCount');
Route::post('/tutee-cancel-session', 'TutorController@cancelSessionData')->name('decline.tutee.session');
Route::any('/tutor-checkPreference', 'TutorController@checkPreference')->name('frontend.checkPreference');
Route::any('/tutor-conformPreference', 'TutorController@conformPreference')->name('frontend.conformPreference');
Route::post('/tutor-report', 'TutorController@reportTutee')->name('frontend.tutor.reportTutee');
Route::post('/tutor-session-feedback', 'TutorController@sessionFeedback')->name('frontend.tutor.sessionfeedback');
Route::any('/fetch-tutee-events', 'TuteeController@fetchEvents');
/**
 * Fird A Tutee
 */
Route::any('/tutee-dashboard', 'TuteeController@dashboard')->name('frontend.tuteeDashboard');
Route::post('/tutor-check-login', 'TutorController@checkLogin')->name('frontend.tutor.checkLogin');
Route::post('/tutee-report', 'TuteeController@reportTutor')->name('frontend.tutee.reportTutor');
Route::post('/tutor-rating', 'TuteeController@ratingTutor')->name('frontend.tutee.ratingTutor');
Route::post('/tutee-session-feedback', 'TuteeController@sessionFeedback')->name('frontend.tutee.sessionfeedback');

/**
 * tutee preference
 */
Route::any('/tutee-preference', 'TuteeController@my_preference')->name('frontend.tuteePreference');
Route::post('/tutee-preference', 'TuteeController@save_preference')->name('frontend.tuteePreference');
Route::post('/tutee-session-preference', 'TuteeController@save_session_preference')->name('frontend.tuteeSessionPreference');
Route::post('/tutee-delete-preference', 'TuteeController@delete_preference_curriculum')->name('frontend.delete_tutee_preference_curriculum');

/**
 * tutor preference
 */

/* tutor avalilablity
 */
Route::any('/tutor-availablity', 'TutorController@my_avaliablity')->name('frontend.tutorAvalilablity');
Route::post('/tutor-availablity-everyday', 'TutorController@save_available_everyday')->name('frontend.tutorAvailableEveryDay');
Route::post('', 'TutorController@deleteTutorAvailablity')->name('frontend.deleteTutorAvailablity');

/**
 * Blog
 */

Route::get('blog', 'BlogController@index')->name('frontend.blog.index');
Route::get('blog-categories', 'BlogController@fetchByCategory');
Route::get('blog/{category}/{cannonical_link}', 'BlogController@index');
Route::get('blog-sorting', 'BlogController@sorting');

/**
 * Customer Feedback
 */
Route::any('feedback', 'FrontendController@showFeedbackForm')->name('frontend.feedback.feedback');
Route::post('feedback', 'FrontendController@saveFeedback')->name('frontend.feedback');

/**
 * These frontend controllers require the user to be logged in
 */
Route::group(['middleware' => 'auth'], function () {
	Route::group(['namespace' => 'User'], function () {
		Route::get('dashboard', 'DashboardController@index')->name('frontend.user.dashboard');
		Route::get('profile/edit', 'ProfileController@edit')->name('frontend.user.profile.edit');
		Route::patch('profile/update', 'ProfileController@update')->name('frontend.user.profile.update');
	});
});


Route::post('send_notification_for_joinsession', function (\Illuminate\Http\Request $request) {
	  $post = $request->all();
	  $id = $post['id'];
	  sendNotificationToTutorForJoinSession($id);
	  
});

/*
 * Get Educational sysetem based on curriculam
 */

Route::post('getCurriculumsystem', function (\Illuminate\Http\Request $request) {
	$where = array('curriculum_id' => $request->id, 'status' => '1');

	$post = $request->all();

	$curriculum_id = isset($request->curriculum_id) ? $request->curriculum_id : "";
	$eductional_systems_id = isset($request->eductional_systems_id) ? $request->eductional_systems_id : "";
	$program_id = isset($request->program_id) ? $request->program_id : "";
	$subject_id = isset($request->subject_id) ? $request->subject_id : "";
	$grade_id = isset($request->grade_id) ? $request->grade_id : "";
	$level_id = isset($request->level_id) ? $request->level_id : "";
	$topic_id = isset($request->topic_id) ? $request->topic_id : "";
	$step = isset($request->step) ? $request->step : "";

	$table = [];
	$final['education'] = "";
	$final['program'] = "";
	$final['grade'] = "";
	$final['level'] = "";
	$final['subject'] = "";
	$final['topic'] = "";
	//new method
	if ($step == 0) {
		$table['1'] = getCurriculumSteps('0', true, $curriculum_id);
		if (isset($table['1']) && $table['1'] == "program") {
			$final['program'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "grade") {
			$final['grade'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "levels") {
			$final['level'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "eductional_systems") {
			$final['education'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "subject") {
			$final['subject'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "topic") {
			$final['topic'] = getRecordsByTable($table['1'], $curriculum_id, '');
		}
		$final['step'] = $step;
		return json_encode($final);
	} else {

		$table['1'] = getCurriculumSteps($step, true, $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id, $topic_id);
		//dd($table['1']);
		//$table['1'] = "subject";
		$final = [];
		if (isset($table['1']) && $table['1'] == "program") {
			$final['program'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "grade") {
			$final['grade'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "levels") {
			$final['level'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "eductional_systems") {
			$final['education'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "subject") {
			$final['subject'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "topic") {
			$final['topic'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		}

		$final['step'] = $step;
		return json_encode($final);
		//dd($final);
	}

});


Route::post('getCurriculumsystemForSessionReserve', function (\Illuminate\Http\Request $request) {
	$where = array('curriculum_id' => $request->id, 'status' => '1');

	$post = $request->all();

	$tutor_id = isset($request->tutor_id) ? $request->tutor_id : "";
	$curriculum_id = isset($request->curriculum_id) ? $request->curriculum_id : "";
	$eductional_systems_id = isset($request->eductional_systems_id) ? $request->eductional_systems_id : "";
	$program_id = isset($request->program_id) ? $request->program_id : "";
	$subject_id = isset($request->subject_id) ? $request->subject_id : "";
	$grade_id = isset($request->grade_id) ? $request->grade_id : "";
	$level_id = isset($request->level_id) ? $request->level_id : "";
	$topic_id = isset($request->topic_id) ? $request->topic_id : "";
	$step = isset($request->step) ? $request->step : "";

	$table = [];
	$final['education'] = "";
	$final['program'] = "";
	$final['grade'] = "";
	$final['level'] = "";
	$final['subject'] = "";
	$final['topic'] = "";
	//new method
	if ($step == 0) {
		$table['1'] = getCurriculumSteps('0', true, $curriculum_id);
		if (isset($table['1']) && $table['1'] == "program") {
			$final['program'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "grade") {
			$final['grade'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "levels") {
			$final['level'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "eductional_systems") {
			$final['education'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "subject") {
			$final['subject'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "topic") {
			$final['topic'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, '');
		}
		$final['step'] = $step;
		return json_encode($final);
	} else {

		$table['1'] = getCurriculumSteps($step, true, $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id, $topic_id);
		//dd($table['1']);
		//$table['1'] = "subject";
		$final = [];
		if (isset($table['1']) && $table['1'] == "program") {
			$final['program'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "grade") {
			$final['grade'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "levels") {
			$final['level'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "eductional_systems") {
			$final['education'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "subject") {
			$final['subject'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "topic") {
			$final['topic'] = getRecordsByTableForReserveSession($tutor_id,$table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		}

		$final['step'] = $step;
		return json_encode($final);
		//dd($final);
	}

});

Route::post('read_notification', function (\Illuminate\Http\Request $request) {
	$post = $request->all();
	$limit = isset($post['limit'])?$post['limit']:'3';
	readNotifications($limit);
});

// /**
//  * User Test
//  */
// Route::get('test', 'QuestionsController@test')->name('frontuser.test.questions');
// Route::post('test/answer', 'QuestionsController@testAnswer')->name('frontuser.answer');

/**
 * User Test
 */
Route::get('test/{id}', 'MyTestController@test')->name('tutor.test'); //frontuser.test.questions
Route::get('giveTestAgain/{id}', 'MyTestController@giveTestAgain')->name('tutor.giveTestAgain'); 
Route::post('test/answer', 'MyTestController@testAnswer')->name('tutor.test.answer'); //frontuser.answer
Route::get('tutor/mytest', 'MyTestController@mytest')->name('tutor.mytest');
// Route::get('tutor/mytest/{startDate}/{endDate}', 'MyTestController@showTestByFilter')->name('tutor.test.filter');
Route::post('tutor/appear/newtest', 'MyTestController@appearForNewTest')->name('tutor.appear.test');
Route::get('become-tutee/{id}', 'MyTestController@becomeTutee')->name('become.tutee');
Route::post('test/check', 'MyTestController@checkPassTest')->name('check.test.pass');
Route::get('tutor/mytest/{tabReload}', 'MyTestController@mytest')->name('tutor.mytest.tabReload');
Route::post('tutee/become-tutor', 'MyTestController@becomeTutorTest')->name('becometutor.appear.test');
Route::any('tutor/mytest/ajax/{type}', array('as' => 'mytest.type', 'uses'  => 'MyTestController@getMyTest'))
		->where('type', 'tab1|tab2');
Route::any('tutor/mytest/ajax/tab/{tab}', array('as' => 'mytest.type', 'uses'  => 'MyTestController@getMyTest'))
		->where('tab', 'tab1|tab2');
// Route::get('test/quit/{id}', 'MyTestController@quitTest')->name('test.quit');
/**
 * Share Experience
 */

Route::get('share-experience', 'ShareExperienceController@create')->name('frontuser.share.experience');
Route::post('share-experience/store', 'ShareExperienceController@store')->name('frontuser.share.experience.store');

/**
 * Notification
 */
Route::any('/notification', 'NotificationController@index')->name('frontend.notification');
Route::any('/getNotificationList', 'NotificationController@getNotificationList')->name('frontend.getNotificationlist');
Route::any('/getNotificationListAjax', 'NotificationController@getNotificationListAjax')->name('frontend.getNotificationListAjax');
Route::any('/get_notifications', 'NotificationController@getUpdatesNotificationList');
/** Tutor Session Request
 */
Route::get('tutor-session-request', 'TutorSessionRequestController@showPendingSession')->name('tutor.pending.session');
Route::get('tutor-session-request/accept/{id}', 'TutorSessionRequestController@acceptRequest')->name('accept.tutee.session.request');
Route::get('tutor-session-request/decline/model/{id}', 'TutorSessionRequestController@declineRequestModel')->name('decline.tutee.session.request.model');
Route::post('tutor-session-request/decline/{id}', 'TutorSessionRequestController@declineRequest')->name('decline.tutee.session.request');

/**
 * Tutee Session Request
 */
Route::get('tutee-session-request', 'TuteeSessionRequestController@showPendingSession')->name('tutee.pending.session');
// Route::get('tutee-session-request/accept/{id}', 'TuteeSessionRequestController@acceptRequest')->name('accept.tutor.session.request');
Route::get('tutee-session-request/decline/model/{id}', 'TuteeSessionRequestController@declineRequestModel')->name('decline.tutor.session.request.model');
Route::get('tutee-session-request-decline/{id}', 'TuteeSessionRequestController@declineRequest')->name('decline.tutor.session.request');

/**
 * Tutee Profile Update
 */
Route::get('profile', 'TuteeProfileController@viewProfile')->name('frontend.profile');
Route::get('profile/edit', 'TuteeProfileController@edit')->name('frontend.profile.edit');
Route::post('profile/update', 'TuteeProfileController@update')->name('profile.update');
Route::post('tuteeprofile/register_user_photo', 'TuteeProfileController@register_user_photo')->name('tutee.register_user_photo');

/**
 * Tutee payment request
 */
Route::any('tutee/payment', 'TuteePaymentController@showPayment')->name('tutee.payment.request');
Route::get('tutee/payment/filter/{startDate}/{endDate}', 'TuteePaymentController@showPaymentByFilter')->name('tutee.payment.request.filter');
Route::get('tutee/payment/{tabReload}', 'TuteePaymentController@showPayment')->name('tutee.payment.request.tabReload');
Route::any('tutee/payment/ajax/{type}', array('as' => 'payment.type', 'uses'  => 'TuteePaymentController@getPaymnetType'))
		->where('type', 'tab1|tab2|tab3');
Route::any('tutee/payment/ajax/tab/{tab}', array('as' => 'payment.type', 'uses'  => 'TuteePaymentController@getPaymnetType'))
		->where('type', 'tab1|tab2|tab3');

/**
 * Tutor payment request
 */
Route::get('tutor/payment', 'TutorPaymentController@showPayment')->name('tutor.payment.request');
Route::get('tutor/payment/{tabReload}', 'TutorPaymentController@showPayment')->name('tutor.payment.request.tabReload');
Route::get('tutor/payment/filter/{startDate}/{endDate}', 'TutorPaymentController@showPaymentByFilter')->name('tutor.payment.request.filter');

Route::any('tutor/payment/ajax/{type}', array('as' => 'payment.type', 'uses'  => 'TutorPaymentController@getPaymnetType'))
		->where('type', 'tab1|tab2|tab3');
Route::any('tutor/payment/ajax/tab/{tab}', array('as' => 'payment.type', 'uses'  => 'TutorPaymentController@getPaymnetType'))
		->where('type', 'tab1|tab2|tab3');

/**
 *	Tutor Session
 */
Route::any('/tutor-sessions', 'TutorController@mySessions')->name('frontend.tutorSessions');
Route::any('/get-session-data-tutor', 'TutorController@getSessionModalData');
Route::post('/tutor-cancel-session', 'TutorController@cancelSessionData')->name('decline.tutor.session');

/*
 * Tutee Free Sesssion
 */
Route::get('free-session', 'TuteeFreeSesssionController@showFreeSession')->name('tutee.free.session');

/*
 * Landing Page
 */
Route::get('/', 'LandingPageController@showLandingPage')->name('frontend.indexpage');
Route::post('/contactus', 'LandingPageController@saveContactUs')->name('frontend.contactUs');
Route::any('/searchResult', 'TutorController@index')->name('frontend.searchResult');

/**
 * CMS pages
 */
Route::get('cmspages/{page_slug}', '\App\Http\Controllers\Api\CmspagesController@showCMSPage')->name('frontend.cmspages.show');

/**
Tutee Join session
 */

Route::get('tuteeJoinSession/{id}', 'TuteeController@tuteeJoinSession')->name('frontend.tutee.joinSession');
Route::get('tuteeEndSession/{id}', 'TuteeController@tuteeEndSession')->name('frontend.tutee.endSession');

//Route::post('tutorJoinSession', function (\Illuminate\Http\Request $request) {tutorJoinSession($request->id);	});

Route::post('tutorJoinSession', 'TutorSessionRequestController@tutorJoinSession')->name('frontend.tutor.joinSession');

Route::post('checkTutorJoinSession', function (\Illuminate\Http\Request $request) {
	checkTutorTuteeJoinSession($request->id);
});

Route::post('checkTuteeJoinSession', function (\Illuminate\Http\Request $request) {
	checkTutorTuteeJoinSession('', $request->id);
});

Route::any('/joinSessionBytutor', 'TutorSessionRequestController@joinSessionBytutor')->name('joinSessionBytutor');
