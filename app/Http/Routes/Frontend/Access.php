<?php

/**
 * Frontend Access Controllers
 */
Route::group(['namespace' => 'Auth'], function () {
// Change Password Routes - FrontEnd
	Route::get('change-password', 'PasswordController@showChangePasswordForm')->name('auth.password.change');
	Route::post('password/change', 'PasswordController@changePassword')->name('auth.password.update');
	/**
	 * These routes require the user to be logged in
	 */

	// Logout Routes - FrontEnd
	Route::get('logout', 'AuthController@logout')->name('auth.logout');

	Route::group(['middleware' => 'auth'], function () {

		// Logout Routes - BackEnd
		Route::get('admin/logout', 'AuthController@adminLogout')->name('admin.auth.logout');

		// // Change Password Routes - FrontEnd
		// Route::get('password/change', 'PasswordController@showChangePasswordForm')->name('auth.password.change');
		// Route::post('password/change', 'PasswordController@changePassword')->name('auth.password.update');

		// Change Password Routes - BackEnd
		Route::get('admin/password/change', 'PasswordController@showAdminChangePasswordForm')->name('admin.auth.password.change');
		Route::post('admin/password/change', 'PasswordController@changeAdminPassword')->name('admin.auth.password.update');
	});

	/**
	 * These routes require the user NOT be logged in
	 */
	Route::group(['middleware' => 'guest'], function () {

		Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {return $captcha->src($config);});

		Route::get('/login', 'AuthController@showLoginForm')->name('frontend.login');

		Route::post('login', 'AuthController@login')->name('auth.login');

		// Authentication Routes - BackEnd
		Route::get('admin', function () {
			return redirect('/admin/login');
		});
		Route::get('admin/login', 'AuthController@showAdminLoginForm')->name('admin.auth.login');
		Route::post('admin/login', 'AuthController@adminLogin');

		// Socialite Routes
		Route::get('login/{provider}', 'AuthController@loginThirdParty')->name('auth.provider');
		Route::post('login_with_fb', 'AuthController@fbLogin')->name('fb.login');

		// Registration Routes
		Route::post('save_fb_user', 'RegisterController@saveFbUser')->name('fb.register');
		Route::get('register', 'RegisterController@showRegistrationForm')->name('auth.register');
		Route::post('register', 'RegisterController@register');
		Route::post('register_basic', 'RegisterController@register_basic')->name('auth.register_basic');
		Route::post('register_education', 'RegisterController@register_education')->name('auth.register_education');
		Route::post('register_tutor_education', 'RegisterController@register_tutor_education')->name('auth.register_tutor_education');
		Route::post('register_tutor_curriculum', 'RegisterController@register_tutor_curriculum')->name('auth.register_tutor_curriculum');
		Route::post('delete_tutor_curriculum', 'RegisterController@delete_tutor_curriculum')->name('auth.delete_tutor_curriculum');
		Route::post('register_user_photo', 'RegisterController@register_user_photo')->name('auth.register_user_photo');
		Route::get('active/account/{id}', 'RegisterController@activeUser')->name('active.register.user.link');

		// Confirm Account Routes
		Route::get('account/confirm/{token}', 'AuthController@confirmAccount')->name('account.confirm');
		Route::get('account/confirm/resend/{token}', 'AuthController@resendConfirmationEmail')->name('account.confirm.resend');

		// Password Reset Routes - FrontEnd
		Route::get('password/reset/{token?}', 'PasswordController@showResetForm')->name('auth.password.reset');
		Route::post('password/email', 'PasswordController@sendResetLinkEmail');
		Route::post('password/reset', 'PasswordController@reset')->name('password.reset.form');
		Route::get('username/reset', 'PasswordController@showUsernameRequestForm')->name('username.reset');
		Route::post('username/sendmail', 'PasswordController@sendUsernameEmail')->name('username.reset.mail');

		// Password Reset Routes - BackEnd
		Route::get('admin/password/reset/{token?}', 'PasswordController@showAdminResetForm')->name('admin.auth.password.reset');
		Route::post('admin/password/email', 'PasswordController@sendAdminResetLinkEmail');
		Route::post('admin/password/reset', 'PasswordController@adminReset');
	});
});
/*
 * Get state based on country id
 */

Route::POST('getState', function (\Illuminate\Http\Request $request) {
	$where = array('country_id' => $request->id, 'status' => '1');
	$state = getstate($where, true);

	return json_encode($state);
});

/*
 * Get state based on country id
 */

Route::POST('getCity', function (\Illuminate\Http\Request $request) {
	$where = array('state_id' => $request->id, 'status' => '1');
	$state = getcity($where, true);

	return json_encode($state);
});


?>
