<?php

/**
 * These frontend controllers require the user to be logged in
 */
Route::group(['middleware' => 'auth'], function () {
	//Route::group(['namespace' => 'User'], function() {
	Route::get('profile', 'DashboardController@view')->name('backend.user.index');
	Route::get('profile/edit', 'DashboardController@edit')->name('backend.user.profile.edit');
	Route::patch('profile/update', 'DashboardController@update')->name('backend.user.profile.update');
	//});
});

Route::group([
	'prefix' => 'access',
	'namespace' => 'Access',
	'middleware' => 'access.routeNeedsPermission:view-access-management',
], function () {
	/**
	 * User Management
	 */
	Route::group(['namespace' => 'User'], function () {
		Route::resource('users', 'UserController', ['except' => ['show']]);

		Route::get('users/deactivated', 'UserController@deactivated')->name('admin.access.users.deactivated');
		Route::get('users/deleted', 'UserController@deleted')->name('admin.access.users.deleted');
		Route::get('account/confirm/resend/{user_id}', 'UserController@resendConfirmationEmail')->name('admin.account.confirm.resend');
		Route::post('users/search', 'UserController@search')->name('admin.access.users.search');
		Route::any('users/data', 'UserController@data')->name('admin.access.users.data');
		Route::post('users/deleteAll', 'UserController@deleteAll')->name('admin.access.users.deleteAll');
		Route::get('users/export', 'UserController@exportExcel')->name('admin.access.users.export');
		Route::get('users/csv', 'UserController@exportCSV')->name('admin.access.users.csv');
		Route::post('users/get/permission', 'UserController@getPermissionByRole');
		Route::post('users/{id}/get/permission', 'UserController@getPermissionByRole');
		//Route::post('users/destroy1', 'UserController@destroy1')->name('admin.access.users.destroy');
		/**
		 * Specific User
		 */
		Route::group(['prefix' => 'user/{id}', 'where' => ['id' => '[0-9]+']], function () {
			Route::get('delete', 'UserController@delete')->name('admin.access.user.delete-permanently');
			Route::get('restore', 'UserController@restore')->name('admin.access.user.restore');
			Route::get('mark/{status}', 'UserController@mark')->name('admin.access.user.mark')->where(['status' => '[0,1]']);
			Route::get('password/change', 'UserController@changePassword')->name('admin.access.user.change-password');
			Route::post('password/change', 'UserController@updatePassword')->name('admin.access.user.change-password');
		});
	});

	/**
	 * Role Management
	 */
	Route::group(['namespace' => 'Role'], function () {
		Route::resource('roles', 'RoleController', ['except' => ['show']]);
		Route::post('roles/search', 'RoleController@search')->name('admin.access.roles.search');
		Route::any('roles/data', 'RoleController@data')->name('admin.access.roles.data');
		Route::post('roles/deleteAll', 'RoleController@deleteAll')->name('admin.access.roles.deleteAll');
		Route::get('roles/export', 'RoleController@exportExcel')->name('admin.access.roles.export');
		Route::get('roles/csv', 'RoleController@exportCSV')->name('admin.access.roles.csv');
		Route::any('roles/destroy/{id}', 'RoleController@destroy')->name('admin.access.roles.destroy');
		Route::get('roles/mark/{id}/{status}', 'RoleController@mark')->name('admin.access.roles.mark')->where(['status' => '[0,1]']);
	});

	/**
	 * Permission Management
	 */
	Route::group(['prefix' => 'roles', 'namespace' => 'Permission'], function () {
		Route::resource('permission-group', 'PermissionGroupController', ['except' => ['index', 'show']]);
		Route::resource('permissions', 'PermissionController', ['except' => ['show']]);

		// for jqgrid
		Route::any('permissions/data', 'PermissionController@data')->name('admin.access.permissions.data');
		Route::post('permissions/deleteAll', 'PermissionController@deleteAll')->name('admin.access.permissions.deleteAll');
//        Route::get('permissions/export', 'PermissionController@exportExcel')->name('admin.access.permissions.export');
		//        Route::get('permissions/csv', 'PermissionController@exportCSV')->name('admin.access.permissions.csv');

		Route::group(['prefix' => 'groups'], function () {
			Route::post('update-sort', 'PermissionGroupController@updateSort')->name('admin.access.roles.groups.update-sort');
		});
	});
});
