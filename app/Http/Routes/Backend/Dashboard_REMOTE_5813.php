<?php

Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');


/**
 * CMS Pages Management
 */
Route::post('cmspages/search', 'CmspagesController@search')->name('admin.cmspages.search');
Route::any('cmspages/data', 'CmspagesController@data')->name('admin.cmspages.data');
Route::post('cmspages/deleteAll', 'CmspagesController@deleteAll')->name('admin.cmspages.deleteAll');
Route::get('cmspages/export', 'CmspagesController@exportExcel')->name('admin.cmspages.export');
Route::get('cmspages/csv', 'CmspagesController@exportCSV')->name('admin.cmspages.csv');
Route::resource('cmspages', 'CmspagesController', ['except' => ['show']]);
/**
 * Specific CMSpage
 */
Route::group(['prefix' => 'cmspages/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'CmspagesController@mark')->name('admin.cmspages.mark')->where(['is_active' => '[Active,Inactive]']);
});

/**
 * Media Management
 */
Route::any('media/data', 'MediaController@data')->name('admin.media.data');
Route::post('media/addMedia', 'MediaController@addMedia')->name('admin.media.addmedia');
Route::post('media/deleteAll', 'MediaController@deleteAll')->name('admin.media.deleteAll');
Route::group(['prefix' => 'media/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'MediaController@mark')->name('admin.media.mark')->where(['status' => '[0,1]']);
});
Route::resource('media', 'MediaController', ['except' => ['show']]);

/*
 * Notificatons Management
 */
Route::resource('notification', 'NotificationController', ['except' => ['show']]);
Route::get('notification/getlist', 'NotificationController@ajaxNotifications')->name('admin.notification.getlist');
Route::get('notification/clearcurrentnotifications', 'NotificationController@clearCurrentNotifications')->name('admin.notification.clearcurrentnotifications');
Route::any('notification/data', 'NotificationController@data')->name('admin.notification.data');
Route::group(['prefix' => 'notification/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{is_read}', 'NotificationController@mark')->name('admin.notification.mark')->where(['is_read' => '[0,1]']);
});

/**
 * Email Template
 */
Route::post('emailtemplate/search', 'EmailTemplateController@search')->name('admin.emailtemplate.search');
Route::any('emailtemplate/data', 'EmailTemplateController@data')->name('admin.emailtemplate.data');
Route::post('emailtemplate/deleteAll', 'EmailTemplateController@deleteAll')->name('admin.emailtemplate.deleteAll');
Route::get('emailtemplate/export', 'EmailTemplateController@exportExcel')->name('admin.emailtemplate.export');
Route::get('emailtemplate/csv', 'EmailTemplateController@exportCSV')->name('admin.emailtemplate.csv');
Route::resource('emailtemplate', 'EmailTemplateController', ['except' => ['show']]);

/**
 * Settings Management
 */
Route::resource('settings', 'SettingsController', ['except' => ['show']]);

/**
 * Admin Brand Model
 */
Route::resource('brand-model', 'AdminBrandModelController', [
    'except' => ['show']
]);

Route::any('brand-model/data', 'AdminBrandModelController@data')->name('admin.brand-model.data');
Route::group(['prefix' => 'brand-model/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminBrandModelController@mark')->name('admin.brand-model.mark')->where(['is_active' => '[Active,InActive]']);
});

/*
 * Admin Blog Category Model
 */
Route::resource('blogcategory-model', 'AdminBlogCategoryModelController', [
    'except' => ['show'],
]);

Route::any('blogcategory-model/data', 'AdminBlogCategoryModelController@data')->name('admin.blogcategory-model.data');
Route::group(['prefix' => 'blogcategory-model/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminBlogCategoryModelController@mark')->name('admin.blogcategory-model.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Vehicle Model Type 
 */
Route::resource('vehicle-model-type', 'AdminVehicleModelTypeController', [
    'except' => ['show']
]);
Route::post('vehicle-model-type/store', 'AdminVehicleModelTypeController@store');
Route::any('vehicle-model-type/{id}/update', 'AdminVehicleModelTypeController@update')->name('admin.vehicle-model-type.update');
Route::any('vehicle-model-type/data', 'AdminVehicleModelTypeController@data')->name('admin.vehicle-model-type.data');
Route::group(['prefix' => 'vehicle-model-type/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminVehicleModelTypeController@mark')->name('admin.vehicle-model-type.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Aggregator
 */
Route::resource('aggregator', 'AdminAggregatorController', [
    'except' => ['show']
]);

Route::any('aggregator/data', 'AdminAggregatorController@data')->name('admin.aggregator.data');
Route::group(['prefix' => 'aggregator/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminAggregatorController@mark')->name('admin.aggregator.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Dealership
 */
Route::resource('dealership', 'AdminDealershipController', ['except' => ['show']]);

Route::any('dealership/data', 'AdminDealershipController@data')->name('admin.dealership.data');
Route::any('dealership/{id}/edit/data-floor', 'AdminDealershipController@dataFloor')->name('admin.dealership.data-floor');
Route::post('dealership/{id}/edit/store-dealership-floor', 'AdminDealershipController@saveDealershipFloor')->name('admin.dealership.store-floor');
Route::get('dealership/{id}/edit/get-floor-users', 'AdminDealershipController@getFloorManagers')->name('admin.dealership.get-floor-users');
Route::get('dealership/{id}/edit/get-floor-codes', 'AdminDealershipController@getFloorCodes')->name('admin.dealership.get-floor-codes');
Route::delete('dealership/{id}/edit/destroy-floor', 'AdminDealershipController@deleteDealershipFloor')->name('admin.dealership.destroy-floor');

Route::group(['prefix' => 'dealership/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminDealershipController@mark')->name('admin.dealership.mark')->where(['is_active' => '[Active,InActive]']);
});
/*
* Dealer Dealership Update
*/
Route::get('dealership/profile', 'AdminDealershipController@getDealershipProfile')->name('admin.dealership.getdealershipprofile');
Route::any('dealership/profile/editDealershipProfile/{id}', 'AdminDealershipController@editDealershipProfile')->name('admin.dealership.editdealershipprofile');
Route::post('dealership/profile/updateDealershipProfile/{id}', 'AdminDealershipController@updateDealershipProfile')->name('admin.dealership.updatedealershipprofile');
/**
 * Notice Board
 */
Route::resource('notice-board', 'NoticeBoardController', [
    'except' => ['show']
]);

Route::any('notice-board/data', 'NoticeBoardController@data')->name('admin.notice-board.data');
Route::any('notice-board/download/{id}', 'NoticeBoardController@download')->name('admin.notice-board.download');
Route::group(['prefix' => 'notice-board/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'NoticeBoardController@mark')->name('admin.notice-board.mark')->where(['is_active' => '[Active,InActive]']);
});


/*
 * Admin Derivatives*
 */
Route::resource('derivative', 'AdminDerivativeController', [
    'except' => ['show']
]);
Route::any('derivative/data', 'AdminDerivativeController@data')->name('admin.derivative.data');
Route::post('derivative/modelname', 'AdminDerivativeController@getModelName')->name('admin.derivative.modelname');
Route::post('derivative/{id}/modelname', 'AdminDerivativeController@getModelName')->name('admin.derivative.modelname');
Route::group(['prefix' => 'derivative/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('derivative/{status}', 'AdminDerivativeController@mark')->name('admin.derivative.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Special Type
 */
Route::resource('special-type', 'AdminSpecialTypeController', [
    'except' => ['show']
]);

Route::any('special-type/data', 'AdminSpecialTypeController@data')->name('admin.special-type.data');
Route::group(['prefix' => 'special-type/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminSpecialTypeController@mark')->name('admin.special-type.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Brand 
 */
Route::resource('brand', 'AdminBrandController', [
    'except' => ['show']
]);

Route::any('brand-data/data', 'AdminBrandController@data')->name('admin.brand.data');
Route::group(['prefix' => 'brand/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminBrandController@mark')->name('admin.brand.mark')->where(['is_active' => '[Active,InActive]']);
});


/**
 * Admin Customer
 */
Route::resource('customer', 'AdminCustomerController', [
    'except' => ['show']
]);

Route::any('customer-data/data', 'AdminCustomerController@data')->name('admin.customer.data');

/**
 * Admin Blog Module
 */
Route::resource('blog', 'AdminBlogController', [
    'except' => ['show']
]);

Route::any('blog/data', 'AdminBlogController@data')->name('admin.blog.data');

/**
 * Admin New Car Specification
 */
Route::resource('specification', 'AdminSpecificationController', [
    'except' => ['show']
]);

Route::any('specification/data', 'AdminSpecificationController@data')->name('admin.specification.data');
Route::group(['prefix' => 'specification/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminSpecificationController@mark')->name('admin.specification.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Old Car Specification
 */
Route::resource('old-specification', 'AdminOldSpecificationController', [
    'except' => ['show']
]);

Route::any('old-specification/data', 'AdminOldSpecificationController@data')->name('admin.old-specification.data');
Route::group(['prefix' => 'old-specification/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminOldSpecificationController@mark')->name('admin.old-specification.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Price To Go
*/
Route::resource('price-to-go', 'AdminPriceToGoController', [
    'except' => ['show']
]);

Route::any('price-to-go/data', 'AdminPriceToGoController@data')->name('admin.price-to-go.data');
Route::group(['prefix' => 'price-to-go/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminPriceToGoController@mark')->name('admin.price-to-go.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Support
 */
Route::resource('support', 'AdminSupportController', [
    'except' => ['show']
]);

Route::any('support/data', 'AdminSupportController@data')->name('admin.support.data');

Route::patch('support/{id}/add-response', 'AdminSupportController@addResponse')->name('admin.support.add-response');


/*
 * Deal Website
 */
Route::resource('deal-website', 'AdminDealWebsiteController', [
    'except' => ['show']
]);

Route::any('deal-website/data', 'AdminDealWebsiteController@data')->name('admin.deal-website.data');
Route::group(['prefix' => 'deal-website/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminDealWebsiteController@mark')->name('admin.deal-website.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Blog Tag
 */
Route::resource('blogtag', 'AdminBlogTagController', [
    'except' => ['show'],
]);

Route::any('blogtag/data', 'AdminBlogTagController@data')->name('admin.blogtag.data');
Route::group(['prefix' => 'blogtag/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminBlogTagController@mark')->name('admin.blogtag.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Vehicle Management
 */
Route::resource('vehicle-management', 'AdminVehicleController', [
	'except' => ['show'],
]);

Route::any('vehicle-management/data', 'AdminVehicleController@data')->name('admin.vehicle-management.data');
Route::post('vehicle-management/addPhotos', 'AdminVehicleController@addPhotos')->name('admin.vehicle-management.addPhotos');
Route::post('vehicle-management/deletePhotos', 'AdminVehicleController@deletePhotos')->name('admin.vehicle-management.deletePhotos');
Route::get('vehicle-management/{id}/getPhotos', 'AdminVehicleController@getPhotos')->name('admin.vehicle-management.getPhotos');


/**
 * Admin Vehicle Stock 
 */
Route::resource('vehical-stock', 'AdminVehicleStockController', [
    'except' => ['show']
]);

Route::any('vehical-stock/data', 'AdminVehicleStockController@data')->name('admin.vehical-stock.data');

/**
 * Admin Campaign management
 */
Route::resource('campaign', 'AdminCampaignController', [
    'except' => ['show'],
]);

Route::post('campaign/modelname', 'AdminCampaignController@getModelName')->name('admin.campaign.modelname');
Route::post('campaign/{id}/modelname', 'AdminCampaignController@getModelName')->name('admin.campaign.modelname');

Route::post('campaign/cmspages', 'AdminCampaignController@getCmsName')->name('admin.campaign.cmspages');
Route::post('campaign/{id}/cmspages', 'AdminCampaignController@getCmsName')->name('admin.campaign.cmspages');

Route::any('campaign/data', 'AdminCampaignController@data')->name('admin.campaign.data');
Route::group(['prefix' => 'campaign/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminCampaignController@mark')->name('admin.campaign.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Menu Management 
 */
Route::resource('menu-management', 'AdminMenuManagementController', [
	'except' => ['show']
]);

Route::any('menu-management/data', 'AdminMenuManagementController@data')->name('admin.menu-management.data');

Route::post('menu-management/cmspage', 'AdminMenuManagementController@getCmsPage')->name('admin.menu-management.cmspage');

Route::post('menu-management/{id}/cmspage', 'AdminMenuManagementController@getCmsPage')->name('admin.menu-management.cmspage');
Route::group(['prefix' => 'menu-management/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminMenuManagementController@mark')->name('admin.menu-management.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Special Managment
 */
Route::resource('special-management', 'AdminSpecialManagementController', [
    'except' => ['show']
]);

Route::any('special-management/data', 'AdminSpecialManagementController@data')->name('admin.special-management.data');
Route::post('special-management/modelname', 'AdminSpecialManagementController@getModel')->name('admin.special-management.modelname');
Route::post('special-management/{id}/modelname', 'AdminSpecialManagementController@getModel')->name('admin.special-management.modelname');
Route::post('special-management/checkbox', 'AdminSpecialManagementController@getModel')->name('admin.special-management.checkbox');
Route::post('special-management/{id}/checkbox', 'AdminSpecialManagementController@getCheckbox')->name('admin.special-management.checkbox');
Route::post('special-management/derivative', 'AdminSpecialManagementController@getDerivative')->name('admin.special-management.derivative');
Route::post('special-management/{id}/derivative', 'AdminSpecialManagementController@getDerivative')->name('admin.special-management.derivative');
Route::group(['prefix' => 'special-management/{id}', 'where' => ['id' => '[0-9]+']], function() {
    Route::get('mark/{status}', 'AdminSpecialManagementController@mark')->name('admin.special-management.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Customer Feedback Management
 */
Route::resource('customerfeedback', 'AdminCustomerFeedbackController', [
    'except' => ['show']
]);

Route::any('customerfeedback/data', 'AdminCustomerFeedbackController@data')->name('admin.customerfeedback.data');

Route::patch('customerfeedback/{id}/add-response', 'AdminCustomerFeedbackController@addResponse')->name('admin.customerfeedback.add-response');

/*
 * Get Brand Models
 */

Route::post('getBrandModels', function(\Illuminate\Http\Request $request){
    $where = array('brand_id' => $request->brand_id, 'status' => 'Active', 'deleted_at' => null);
    $data = getBrandModels($where, true)->pluck('brand_model_name', 'id');
    return json_encode($data);
});

/*
 * Get Model Derivatives
 */

Route::post('getDerivatives', function(\Illuminate\Http\Request $request){
    $where = array('brand_model_id' => $request->model_id, 'status' => 'Active', 'deleted_at' => null);
    $data = getDerivatives($where, true)->pluck('derivative_name', 'id');
    return json_encode($data);
});


/**
 * Dealer Special Managment
 */
Route::resource('dealer/special-management', 'AdminSpecialManagementController', [
    'except' => ['show']
]);

Route::post('dealer/modelname', 'AdminSpecialManagementController@getModel')->name('admin.dealer.special-management.modelname');
Route::get('dealer/speciallist', 'AdminSpecialManagementController@getSpecialList')->name('admin.dealer.special-management.speciallist');
Route::post('dealer/derivative', 'AdminSpecialManagementController@getDerivative')->name('admin.dealer.special-management.derivative');
Route::get('dealer/{id}/details', 'AdminSpecialManagementController@getSpecialDetails')->name('admin.dealer.special-management.details');
Route::get('dealer/{id}/optin', 'AdminSpecialManagementController@optInSpecial')->name('admin.dealer.special-management.optin');
Route::get('dealer/{id}/optout', 'AdminSpecialManagementController@optOutFromSpecial')->name('admin.dealer.special-management.optout');
Route::post('dealer/list', 'AdminSpecialManagementController@getListData')->name('admin.dealer.special-management.list');
Route::any('dealer/pagination', 'AdminSpecialManagementController@getPaginatedListData')->name('admin.dealer.special-management.pagination');

/**
 * Admin Manage Request
*/
Route::resource('managerequest', 'AdminManageRequestController', [
    'except' => ['show']
]);
Route::any('managerequest/data', 'AdminManageRequestController@data')->name('admin.managerequest.data');


/**
 * Dealer PDF Managment
 */
Route::resource('dealer/pdf', 'AdminVehicleStockController@showPdfGrid', [
    'except' => ['show']
]);

Route::any('dealer/pdf/data', 'AdminVehicleStockController@data')->name('admin.dealer.pdf.data');