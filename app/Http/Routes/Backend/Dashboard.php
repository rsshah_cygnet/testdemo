<?php

Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
Route::post('filterPieChart', 'DashboardController@filterPieChart');

/**
 * Front Users Management
 */
Route::any('users/destroy/{id}', 'FrontusersController@destroy')->name('admin.frontusers.delete');
Route::post('users/search', 'FrontusersController@search')->name('admin.frontusers.search');
Route::any('users/data', 'FrontusersController@data')->name('admin.frontusers.data');
Route::get('users/export', 'FrontusersController@exportExcel')->name('admin.frontusers.export');
Route::get('users/csv', 'FrontusersController@exportCSV')->name('admin.frontusers.csv');
Route::resource('users', 'FrontusersController@index', ['except' => ['show']]);
Route::get('users/mark/{id}/{status}', 'FrontusersController@mark')->name('admin.frontusers.mark')->where(['status' => '[0,1]']);

/**
 * User Test Result Management
 */

Route::get('users/testResult/{id}', 'FrontusersController@testResult')->name('admin.frontusers.test.preview');
Route::any('users/test-result', 'FrontusersController@testResultData')->name('admin.frontusers.testResultData');
Route::get('users/markTestResukt/{id}/{status}', 'FrontusersController@markTestResult')->name('admin.frontusers.testresult.mark')->where(['status' => '[0,1]']);
Route::post('frontusers/send', 'FrontusersController@sendReminder');
Route::get('users/viewQue/{id}', 'FrontusersController@viewResult')->name('admin.frontusers.viewQue');
Route::post('frontusers/send', 'FrontusersController@sendReminder');

/**
 * CMS Pages Management
 */
Route::post('cmspages/search', 'CmspagesController@search')->name('admin.cmspages.search');
Route::any('cmspages/data', 'CmspagesController@data')->name('admin.cmspages.data');
Route::post('cmspages/deleteAll', 'CmspagesController@deleteAll')->name('admin.cmspages.deleteAll');
Route::get('cmspages/export', 'CmspagesController@exportExcel')->name('admin.cmspages.export');
Route::get('cmspages/csv', 'CmspagesController@exportCSV')->name('admin.cmspages.csv');
Route::resource('cmspages', 'CmspagesController', ['except' => ['show']]);
/**
 * Specific CMSpage
 */
Route::group(['prefix' => 'cmspages/{id}', 'where' => ['id' => '[0-9]+']], function () {
	Route::get('mark/{status}', 'CmspagesController@mark')->name('admin.cmspages.mark')->where(['is_active' => '[Active,Inactive]']);
});

/**
 * Media Management
 */
Route::any('media/data', 'MediaController@data')->name('admin.media.data');
Route::post('media/addMedia', 'MediaController@addMedia')->name('admin.media.addmedia');
Route::post('media/deleteAll', 'MediaController@deleteAll')->name('admin.media.deleteAll');
Route::group(['prefix' => 'media/{id}', 'where' => ['id' => '[0-9]+']], function () {
	Route::get('mark/{status}', 'MediaController@mark')->name('admin.media.mark')->where(['status' => '[0,1]']);
});
Route::resource('media', 'MediaController', ['except' => ['show']]);

/*
 * Notificatons Management
 */
Route::resource('notification', 'NotificationController', ['except' => ['show']]);
Route::get('notification/getlist', 'NotificationController@ajaxNotifications')->name('admin.notification.getlist');
Route::get('notification/clearcurrentnotifications', 'NotificationController@clearCurrentNotifications')->name('admin.notification.clearcurrentnotifications');
Route::any('notification/data', 'NotificationController@data')->name('admin.notification.data');
Route::group(['prefix' => 'notification/{id}', 'where' => ['id' => '[0-9]+']], function () {
	Route::get('mark/{is_read}', 'NotificationController@mark')->name('admin.notification.mark')->where(['is_read' => '[0,1]']);
});

/**
 * Email Template
 */
Route::post('emailtemplate/search', 'EmailTemplateController@search')->name('admin.emailtemplate.search');
Route::any('emailtemplate/data', 'EmailTemplateController@data')->name('admin.emailtemplate.data');
Route::post('emailtemplate/deleteAll', 'EmailTemplateController@deleteAll')->name('admin.emailtemplate.deleteAll');
Route::get('emailtemplate/export', 'EmailTemplateController@exportExcel')->name('admin.emailtemplate.export');
Route::get('emailtemplate/csv', 'EmailTemplateController@exportCSV')->name('admin.emailtemplate.csv');
Route::resource('emailtemplate', 'EmailTemplateController', ['except' => ['show']]);

/**
 * Settings Management
 */
Route::resource('settings', 'SettingsController', ['except' => ['show']]);

/**
 * Admin Brand Model
 */
Route::resource('brand-model', 'AdminBrandModelController', [
	'except' => ['show'],
]);

Route::any('brand-model/data', 'AdminBrandModelController@data')->name('admin.brand-model.data');
Route::group(['prefix' => 'brand-model/{id}', 'where' => ['id' => '[0-9]+']], function () {
	Route::get('mark/{status}', 'AdminBrandModelController@mark')->name('admin.brand-model.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Admin Customer
 */
Route::resource('customer', 'AdminCustomerController', [
	'except' => ['show'],
]);

Route::any('customer-data/data', 'AdminCustomerController@data')->name('admin.customer.data');

/**
 * Admin Blog Module
 */
Route::resource('blog', 'AdminBlogController', [
	'except' => ['show'],
]);

Route::any('blog/{cat}/{slug}/preview', 'AdminBlogController@preview')->name('admin.blog.preview');

Route::any('blog/data', 'AdminBlogController@data')->name('admin.blog.data');

/**
 * CSV Import Module
 */
Route::any('upload-csv', 'CSVImportController@index')->name('admin.csv.import');
Route::post('upload-csv/save', 'CSVImportController@store')->name('admin.csv.save');

/**
 * Admin Menu Management
 */
Route::resource('menu-management', 'AdminMenuManagementController', [
	'except' => ['show'],
]);

Route::any('menu-management/data', 'AdminMenuManagementController@data')->name('admin.menu-management.data');

Route::post('menu-management/cmspage', 'AdminMenuManagementController@getCmsPage')->name('admin.menu-management.cmspage');

Route::post('menu-management/{id}/cmspage', 'AdminMenuManagementController@getCmsPage')->name('admin.menu-management.cmspage');
Route::group(['prefix' => 'menu-management/{id}', 'where' => ['id' => '[0-9]+']], function () {
	Route::get('mark/{status}', 'AdminMenuManagementController@mark')->name('admin.menu-management.mark')->where(['is_active' => '[Active,InActive]']);
});

/**
 * Contact Us Management
 */
Route::any('contactus/destroy/{id}', 'ContactusController@destroy')->name('admin.contactus.delete');
Route::post('contactus/search', 'ContactusController@search')->name('admin.contactus.search');
Route::any('contactus/data', 'ContactusController@data')->name('admin.contactus.data');
Route::get('contactus/export', 'ContactusController@exportExcel')->name('admin.contactus.export');
Route::get('contactus/csv', 'ContactusController@exportCSV')->name('admin.contactus.csv');
Route::resource('contactus', 'ContactusController@index', ['except' => ['show']]);
Route::get('contactus/mark/{id}/{status}', 'ContactusController@mark')->name('admin.contactus.mark')->where(['status' => '[0,1]']);

/**
 * Educational System Management
 */

Route::resource('educational-system', 'EducationalSystemController', [
	'except' => ['show'],
]);

Route::any('educational-system/data', 'EducationalSystemController@data')->name('admin.educational-system.data');
Route::group(['prefix' => 'educational-system/{id}', 'where' => ['id' => '[0-9]+']], function () {
	Route::get('mark/{status}', 'EducationalSystemController@mark')->name('admin.educational-system.mark')->where(['status' => '[0,1]']);
});
/**
 * Levels Management
 */

Route::any('levels/destroy/{id}', 'LevelsController@destroy')->name('admin.levels.delete');
Route::post('levels/search', 'LevelsController@search')->name('admin.levels.search');
Route::any('levels/data', 'LevelsController@data')->name('admin.levels.data');
Route::get('levels/create', 'LevelsController@create')->name('admin.levels.create');
Route::get('levels/edit/{id}', 'LevelsController@edit')->name('admin.levels.edit');
Route::get('levels/export', 'LevelsController@exportExcel')->name('admin.levels.export');
Route::post('levels/store', 'LevelsController@store')->name('admin.levels.store');
Route::get('levels/csv', 'LevelsController@exportCSV')->name('admin.levels.csv');
//Route::get('levels/index', 'LevelsController@index')->name('admin.levels.index');
Route::get('levels', 'LevelsController@index')->name('admin.levels.index');
Route::get('levels/mark/{id}/{status}', 'LevelsController@mark')->name('admin.levels.mark')->where(['status' => '[0,1]']);
/**
 * Grade Management
 */

Route::any('grade/destroy/{id}', 'GradeController@destroy')->name('admin.grade.delete');
Route::post('grade/search', 'GradeController@search')->name('admin.grade.search');
Route::any('grade/data', 'GradeController@data')->name('admin.grade.data');
Route::get('grade/create', 'GradeController@create')->name('admin.grade.create');
Route::get('grade/edit/{id}', 'GradeController@edit')->name('admin.grade.edit');
Route::get('grade/export', 'GradeController@exportExcel')->name('admin.grade.export');
Route::post('grade/store', 'GradeController@store')->name('admin.grade.store');
Route::get('grade/csv', 'GradeController@exportCSV')->name('admin.grade.csv');
Route::get('grade', 'GradeController@index')->name('admin.grade.index');
Route::get('grade/mark/{id}/{status}', 'GradeController@mark')->name('admin.grade.mark')->where(['status' => '[0,1]']);

/**
 * Subject Management
 */

Route::any('subject/destroy/{id}', 'SubjectController@destroy')->name('admin.subject.delete');
Route::post('subject/search', 'SubjectController@search')->name('admin.subject.search');
Route::any('subject/data', 'SubjectController@data')->name('admin.subject.data');
Route::get('subject/create', 'SubjectController@create')->name('admin.subject.create');
Route::get('subject/edit/{id}', 'SubjectController@edit')->name('admin.subject.edit');
Route::get('subject/export', 'SubjectController@exportExcel')->name('admin.subject.export');
Route::post('subject/store', 'SubjectController@store')->name('admin.subject.store');
Route::get('subject/csv', 'SubjectController@exportCSV')->name('admin.subject.csv');
Route::get('subject', 'SubjectController@index')->name('admin.subject.index');
Route::get('subject/mark/{id}/{status}', 'SubjectController@mark')->name('admin.subject.mark')->where(['status' => '[0,1]']);

/**
 * Curriculum Management
 */
Route::any('curriculum/destroy/{id}', 'CurriculumController@destroy')->name('admin.curriculum.delete');
Route::post('curriculum/search', 'CurriculumController@search')->name('admin.curriculum.search');
Route::any('curriculum/data', 'CurriculumController@data')->name('admin.curriculum.data');
Route::get('curriculum/export', 'CurriculumController@exportExcel')->name('admin.curriculum.export');
Route::get('curriculum/csv', 'CurriculumController@exportCSV')->name('admin.curriculum.csv');
Route::resource('curriculum', 'CurriculumController', ['except' => ['show']]);
// Route::get('curriculum/create', 'CurriculumController@create')->name('admin.curriculum.create');
Route::get('curriculum/mark/{id}/{status}', 'CurriculumController@mark')->name('admin.curriculum.mark')->where(['status' => '[0,1]']);
Route::get('curriculum-management', 'CurriculumController@getLinks')->name('admin.curriculam.management');

Route::post('getCityBasedOncountry', function (\Illuminate\Http\Request $request) {
	$post = $request->all();
	$city = getCityBasedOncountry($post['country']);
	return json_encode($city);
	
});
/*
 * Get Educational sysetem based on curriculam
 */

Route::post('getCurriculumsystem', function (\Illuminate\Http\Request $request) {
	$where = array('curriculum_id' => $request->id, 'status' => '1');

	$post = $request->all();

	$curriculum_id = isset($request->curriculum_id) ? $request->curriculum_id : "";
	$eductional_systems_id = isset($request->eductional_systems_id) ? $request->eductional_systems_id : "";
	$program_id = isset($request->program_id) ? $request->program_id : "";
	$subject_id = isset($request->subject_id) ? $request->subject_id : "";
	$grade_id = isset($request->grade_id) ? $request->grade_id : "";
	$level_id = isset($request->level_id) ? $request->level_id : "";
	$step = isset($request->step) ? $request->step : "";

	$table = [];
	$final['education'] = "";
	$final['program'] = "";
	$final['grade'] = "";
	$final['level'] = "";
	$final['subject'] = "";
	//new method
	if ($step == 0) {
		$table['1'] = getCurriculumSteps('0', true, $curriculum_id);
		if (isset($table['1']) && $table['1'] == "program") {
			$final['program'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "grade") {
			$final['grade'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "levels") {
			$final['level'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "eductional_systems") {
			$final['education'] = getRecordsByTable($table['1'], $curriculum_id, '');
		} else if (isset($table['1']) && $table['1'] == "subject") {
			$final['subject'] = getRecordsByTable($table['1'], $curriculum_id, '');
		}
		$final['step'] = $step;
		return json_encode($final);
	} else {

		$table['1'] = getCurriculumSteps($step, true, $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		//dd($table['1']);
		//$table['1'] = "subject";
		$final = [];
		if (isset($table['1']) && $table['1'] == "program") {
			$final['program'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "grade") {
			$final['grade'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "levels") {
			$final['level'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "eductional_systems") {
			$final['education'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		} else if (isset($table['1']) && $table['1'] == "subject") {
			$final['subject'] = getRecordsByTable($table['1'], $curriculum_id, $level_id, $program_id, $eductional_systems_id, $subject_id, $grade_id);
		}

		$final['step'] = $step;
		return json_encode($final);
		//dd($final);
	}

});

Route::post('getEducationalsystem', function (\Illuminate\Http\Request $request) {
	$where = array('curriculum_id' => $request->id, 'status' => '1');
	$education = getEducationalsystems($where, true)->pluck('name', 'id');
	$grade = getGrades($where, true)->pluck('grade_name', 'id');
	$program = getPrograms($where, true)->pluck('program_name', 'id');
	$subject = getSubjects($where, true)->pluck('subject_name', 'id');
	$level = getLevels($where, true)->pluck('levels_name', 'id');

	$final = array();
	$final['education'] = $education;
	$final['grade'] = $grade;
	$final['program'] = $program;
	$final['subject'] = $subject;
	$final['level'] = $level;

	return json_encode($final);
});

Route::post('getTopics', function (\Illuminate\Http\Request $request) {
	$where = array('curriculum_id' => $request->id, 'status' => '1');
	$topic = getTopics($where, true)->pluck('topic_name', 'id');

	$final = array();
	$final['topic'] = $topic;

	return json_encode($final);
});

/**
 * Programs Management
 */

Route::any('programs/destroy/{id}', 'ProgramsController@destroy')->name('admin.programs.delete');
Route::post('programs/search', 'ProgramsController@search')->name('admin.programs.search');
Route::any('programs/data', 'ProgramsController@data')->name('admin.programs.data');
Route::get('programs/create', 'ProgramsController@create')->name('admin.programs.create');
Route::get('programs/edit/{id}', 'ProgramsController@edit')->name('admin.programs.edit');
Route::get('programs/export', 'ProgramsController@exportExcel')->name('admin.programs.export');
Route::post('programs/store', 'ProgramsController@store')->name('admin.programs.store');
Route::get('programs/csv', 'ProgramsController@exportCSV')->name('admin.programs.csv');
Route::get('programs', 'ProgramsController@index')->name('admin.programs.index');
Route::get('programs/mark/{id}/{status}', 'ProgramsController@mark')->name('admin.programs.mark')->where(['status' => '[0,1]']);

/**
 * Topic Management
 */

Route::any('topic/destroy/{id}', 'TopicController@destroy')->name('admin.topic.delete');
Route::post('topic/search', 'TopicController@search')->name('admin.topic.search');
Route::any('topic/data', 'TopicController@data')->name('admin.topic.data');
Route::get('topic/create', 'TopicController@create')->name('admin.topic.create');
Route::get('topic/edit/{id}', 'TopicController@edit')->name('admin.topic.edit');
Route::get('topic/export', 'TopicController@exportExcel')->name('admin.topic.export');
Route::post('topic/store', 'TopicController@store')->name('admin.topic.store');
Route::get('topic/csv', 'TopicController@exportCSV')->name('admin.topic.csv');
//Route::get('levels/index', 'LevelsController@index')->name('admin.topic.index');
Route::get('topic', 'TopicController@index')->name('admin.topic.index');
Route::get('topic/mark/{id}/{status}', 'TopicController@mark')->name('admin.topic.mark')->where(['status' => '[0,1]']);

/**
 * Session
 */
Route::post('session/search', 'SessionController@search')->name('admin.session.search');
Route::any('session/data', 'SessionController@data')->name('admin.session.data');
Route::any('session/upcoming-data', 'SessionController@data_upcoming')->name('admin.session.data-upcoming');
Route::any('session/cancelled-tutor-data', 'SessionController@data_cancelled_tutor')->name('admin.session.cancelled-tutor-data');
Route::any('session/cancelled-tutee-data', 'SessionController@data_cancelled_tutee')->name('admin.session.cancelled-tutee-data');
Route::get('session/export', 'SessionController@exportExcel')->name('admin.session.export');
Route::post('session/store', 'SessionController@store')->name('admin.session.store');
Route::get('session/csv', 'SessionController@exportCSV')->name('admin.session.csv');
Route::get('session/success', 'SessionController@index')->name('admin.session.index');
Route::get('session/upcoming', 'SessionController@upcoming')->name('admin.session.upcoming');
Route::get('session/cancelled-tutor', 'SessionController@cancelled_tutor')->name('admin.session.cancelled-tutor');
Route::get('session/cancelled-tutee', 'SessionController@cancelled_tutee')->name('admin.session.cancelled-tutee');

/**
 * Question Management
 */

Route::any('question/destroy/{id}', 'QuestionController@destroy')->name('admin.question.delete');
Route::post('question/search', 'QuestionController@search')->name('admin.question.search');
Route::any('question/data', 'QuestionController@data')->name('admin.question.data');
Route::get('question/create', 'QuestionController@create')->name('admin.question.create');
Route::get('question/edit/{id}', 'QuestionController@edit')->name('admin.question.edit');
Route::get('question/export', 'QuestionController@exportExcel')->name('admin.question.export');
Route::post('question/store', 'QuestionController@store')->name('admin.question.store');
Route::get('question/csv', 'QuestionController@exportCSV')->name('admin.question.csv');
//Route::get('levels/index', 'LevelsController@index')->name('admin.topic.index');
Route::get('question', 'QuestionController@index')->name('admin.question.index');
Route::get('question/mark/{id}/{status}', 'QuestionController@mark')->name('admin.question.mark')->where(['status' => '[0,1]']);
Route::any('question/import', 'QuestionController@import')->name('admin.question.import');
Route::post('question/save', 'QuestionController@save_import')->name('admin.question.save');
Route::get('examsettings', 'QuestionController@examsetting')->name('admin.exam.settings');
Route::patch('examsettings/store/{id}', 'QuestionController@storeexamsetting')->name('admin.exam.settings.update');

/**
 * Testimonial Management
 */
Route::get('testimonial', 'Testimonial@index')->name('admin.testimonial.index');
Route::any('testimonial/data', 'Testimonial@data')->name('admin.testimonial.data');
Route::get('testimonial/mark/{id}/{status}', 'Testimonial@mark')->name('admin.testimonial.mark')->where(['status' => '[0,1]']);
Route::any('testimonial/destroy/{id}', 'Testimonial@destroy')->name('admin.testimonial.delete');
Route::get('testimonial/publish/{id}/{publish_on_home_page}', 'Testimonial@publish')->name('admin.testimonial.publish')->where(['publish_on_home_page' => '[0,1]']);
/**
 * Session Feedback Management
 */
Route::get('session-feedback', 'SessionFeedbackController@index')->name('admin.sessionfeedback.index');
Route::any('session-feedback/data', 'SessionFeedbackController@data')->name('admin.sessionfeedback.data');
Route::any('session-feedback/destroy/{id}', 'SessionFeedbackController@destroy')->name('admin.sessionfeedback.delete');

/**
 * Punctuality Rating for Successful Seesion Management
 */
Route::any('punctuality-ratings/destroy/{id}', 'PunctualityRatingsConctroller@destroy')->name('admin.punctualityratings.delete');
Route::post('punctuality-ratings/search', 'PunctualityRatingsConctroller@search')->name('admin.punctualityratings.search');
Route::any('punctuality-ratings/data', 'PunctualityRatingsConctroller@data')->name('admin.punctualityratings.data');

Route::resource('punctuality-ratings', 'PunctualityRatingsConctroller', ['except' => ['show']]);
// Route::get('curriculum/create', 'CurriculumController@create')->name('admin.curriculum.create');
Route::get('punctuality-ratings/mark/{id}/{status}', 'PunctualityRatingsConctroller@mark')->name('admin.punctualityratings.mark')->where(['status' => '[0,1]']);

/**
 * Punctuality Rating for Cancel Seesion Management
 */
Route::any('punctualityratings-cancelsession/destroy/{id}', 'PunctualityRatingCancelSessionController@destroy')->name('admin.punctualityratings-cancelsession.delete');
Route::post('punctualityratings-cancelsession/search', 'PunctualityRatingCancelSessionController@search')->name('admin.punctualityratings-cancelsession.search');
Route::any('punctualityratings-cancelsession/data', 'PunctualityRatingCancelSessionController@data')->name('admin.punctualityratings-cancelsession.data');
Route::resource('punctualityratings-cancelsession', 'PunctualityRatingCancelSessionController', ['except' => ['show']]);
// Route::get('curriculum/create', 'CurriculumController@create')->name('admin.curriculum.create');
Route::get('punctualityratings-cancelsession/mark/{id}/{status}', 'PunctualityRatingCancelSessionController@mark')->name('admin.punctualityratings-cancelsession.mark')->where(['status' => '[0,1]']);

/**
 * Session cancel penalty management
 */
Route::any('session-cancel-penalty/destroy/{id}', 'SessionCancelPenaltyController@destroy')->name('admin.session-cancel-penalty.delete');
Route::post('session-cancel-penalty/search', 'SessionCancelPenaltyController@search')->name('admin.session-cancel-penalty.search');
Route::any('session-cancel-penalty/data', 'SessionCancelPenaltyController@data')->name('admin.session-cancel-penalty.data');
Route::resource('session-cancel-penalty', 'SessionCancelPenaltyController', ['except' => ['show']]);
// Route::get('curriculum/create', 'CurriculumController@create')->name('admin.curriculum.create');
Route::get('session-cancel-penalty/mark/{id}/{status}', 'SessionCancelPenaltyController@mark')->name('admin.session-cancel-penalty.mark')->where(['status' => '[0,1]']);

/**
 * Report Management
 */
Route::get('report-management/reportedbytutor', 'UserReportsController@index')->name('admin.report.by.tutor');
Route::any('report-management/reportedbytutor/data', 'UserReportsController@data')->name('admin.report.by.tutor.data');
Route::any('report-management/reportedbytutor/destroy/{id}', 'UserReportsController@destroy')->name('admin.report.by.tutor.delete');
Route::get('report-management/reportedbytutee', 'UserReportsByTuteeController@index')->name('admin.report.by.tutee');
Route::any('report-management/reportedbytutee/data', 'UserReportsByTuteeController@data')->name('admin.report.by.tutee.data');
Route::any('report-management/reportedbytutee/destroy/{id}', 'UserReportsByTuteeController@destroy')->name('admin.report.by.tutee.delete');
