<?php

// methods to be accessed without login
Route::group(['namespace' => 'Api', 'prefix' => 'api'], function () {
    Route::post('login', 'UserController@login')->name('api.login');
    Route::post('register', 'UserController@register')->name('api.register');
    Route::post('forgotPassword', 'UserController@forgotPassword')->name('api.forgotPassword');
    Route::post('getCmsContent', 'CmspagesController@getCmsContent')->name('api.getCmsContent');
});

// methods to be accessed after login
Route::group(['namespace' => 'Api', 'prefix' => 'api', 'middleware' => 'authAPI'], function () {
    Route::post('logout', 'UserController@logout')->name('api.logout');
    Route::post('changePassword', 'UserController@changePassword')->name('api.changePassword');
    Route::post('savePasskeyReminder', 'UserController@savePasskeyReminder')->name('api.savePasskeyReminder');
    Route::post('requestPasskey', 'UserController@requestPasskey')->name('api.requestPasskey');
    Route::post('validatePasskey', 'UserController@validatePasskey')->name('api.validatePasskey');
});

Route::get('getRecordingSession', 'CronController@getRecordingSession')->name('admin.cron.getRecordingSession');

Route::get('check_session_start', 'CronController@sendSessionStartNotification');

?>