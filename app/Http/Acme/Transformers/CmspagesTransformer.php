<?php

namespace App\Http\Acme\Transformers;

use App\Http\Acme\Transformers;

class CmspagesTransformer extends Transformer {

    public function transform($data) {
        return [
            //'CmsId' => $data['id'],
            'CmsContent' => $data['description'],
        ];
    }

}
