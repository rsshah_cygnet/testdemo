<?php

namespace App\Http\Acme\Transformers;

use App\Http\Acme\Transformers;

class UserTransformer extends Transformer {

    public function transform($data) {        
        return [
            'UserId' => $data['id'],
            'Name' => $this->nulltoBlank($data['name']),
            //'Confirmed' => $this->nulltoBlank($data['confirmed']),
            'UserToken' => $data['user_token']
        ];
    }

}
