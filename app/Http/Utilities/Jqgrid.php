<?php

namespace App\Http\Utilities;

class Jqgrid {

	public function create_condition($rule) {
		if ($rule['op'] == 'eq') {
			// equal
			return $rule['field'] . ' = "' . $rule['data'] . '"';
		}
		if ($rule['op'] == 'ne') {
			// not equal
			return $rule['field'] . ' <> "' . $rule['data'] . '"';
		}
		if ($rule['op'] == 'bw') { // begin with
			return $rule['field'] . ' LIKE "' . $rule['data'] . '%"'; //(id LIKE "a%")
		}
		if ($rule['op'] == 'bn') {
			// does not begin with (id NOT LIKE "a%")
			return $rule['field'] . ' NOT LIKE "' . $rule['data'] . '%"';
		}
		if ($rule['op'] == 'ew') {
			// end with (id LIKE "%a")
			return $rule['field'] . ' LIKE "%' . $rule['data'] . '"';
		}
		if ($rule['op'] == 'en') {
			// does not end with (id NOT LIKE "%a")
			return $rule['field'] . ' NOT LIKE "%' . $rule['data'] . '"';
		}
		if ($rule['op'] == 'cn') {
			//contains (id LIKE "%a%")
			return $rule['field'] . ' LIKE "%' . $rule['data'] . '%"';
		}
		if ($rule['op'] == 'nc') {
			//does not contains (id NOT LIKE "%a%")
			return $rule['field'] . ' NOT LIKE "%' . $rule['data'] . '%"';
		}
		if ($rule['op'] == 'nu') {
			//is null  (id IS NULL )
			return $rule['field'] . ' IS NULL';
		}
		if ($rule['op'] == 'nn') {
			//not null (id ISNOT NULL )
			return $rule['field'] . ' ISNOT NULL';
		}
		if ($rule['op'] == 'in') {
			//is in (id IN " (1,2)")
			return $rule['field'] . ' IN "(' . $rule['data'] . ')"';
		}
		if ($rule['op'] == 'ni') {
			//is not in (id NOT IN " (1,2)")
			return $rule['field'] . ' NOT IN "(' . $rule['data'] . ')"';
		}

		if ($rule['op'] == 'le') {
			// less or equal then
			return $rule['field'] . ' <= ' . $rule['data'];
		}
		if ($rule['op'] == 'lt') {
			// less then
			return $rule['field'] . ' < ' . $rule['data'];
		}
		if ($rule['op'] == 'ge') {
			// greater or equal then
			return $rule['field'] . ' >= ' . $rule['data'];
		}
		if ($rule['op'] == 'gt') {
			// greater then
			return $rule['field'] . ' > ' . $rule['data'];
		}

	}

	public function sub($datas) {
		if (!empty($datas)) {
			$str = '(';
			$condition = $datas['groupOp'];
			$rules = $datas['rules'];
			foreach ($rules as $rule) {
				$str = $str . $this->create_condition($rule) . ' ' . $condition . ' ';
			}
			if (isset($datas['groups']) && !empty($datas['groups'])) {
				return $str . $this->sub($datas['groups'][0]) . ')';
			} else {
				return $str . ')';
			}
		}
	}

	/**
	 * Prepare where
	 *
	 * @param [type] $request [description]
	 */
	public function Preparewhere($request) {
		$where = '';

		if (!empty($request->get('filters'))) {
			$data = json_decode($request->get('filters'), true);
			$where = $this->remoteAdditionalFilters($data);
		}

		if (!empty($request->get('searchField'))) {
			$rule = $this->getRules($request);
			$where = '(' . $this->create_condition($rule) . ')';
		}

		return $where;
	}

	/**
	 * Get Rules
	 *
	 * @param object $request
	 * @return array
	 */
	public function getRules($request) {
		return [
			'op' => $request->get('searchOper'),
			'field' => $request->get('searchField'),
			'data' => $request->get('searchString'),
		];
	}

	/**
	 * Remote Additional Filters
	 *
	 * @param string $data
	 * @return string
	 */
	public function remoteAdditionalFilters($data = null) {
		$where = '';

		if (!empty($data)) {
			$where = $this->sub($data);
			$where = str_replace('AND )', ')', $where); // remove last AND condition
			$where = str_replace('OR )', ')', $where); // remove last OR condition
		}

		return $where;
	}

	/**
	 * Prepare data
	 * @param array $request
	 * @param object $model
	 * @param string $type    [description]
	 */
	public function Preparedata($request, $model, $type) {
		$where = $this->Preparewhere($request);

		if ($type == 'ajax') {
			return $this->getAjaxResponse($model, $request, $where);
		} else {
			set_time_limit(0);

			if ($request->get('dtotal') == 'current') {
				$limit = $request->get('rows');
				$sidx = $request->get('sidx') ? $request->get('sidx') : 1;
				$sord = $request->get('sord');
				$records = $model->getPaginatedSearch($limit, '', $sidx, $sord, '', $where);
			}

			if ($request->get('dtotal') == 'all') {
				$records = $model->getcAll();
			}

			return $records;
		}
	}

	/**
	 * Get Ajax Response
	 *
	 * @param object $model
	 * @param object $request
	 * @param string $where
	 * @return array
	 */
	public function getAjaxResponse($model, $request, $where) {
		$count = $model->getCount($where);
		$page = $request->get('page');
		$limit = $request->get('rows');
		$sidx = $request->get('sidx') ? $request->get('sidx') : 1;
		$sord = $request->get('sord');
		$totalPages = 0;

		if ($count > 0 && $limit > 0) {
			$totalPages = ceil($count / $limit);
		}

		if ($page > $totalPages) {
			$page = $totalPages;
		}

		ini_set('error_reporting', E_STRICT);

		$response = [
			'page' => $page,
			'total' => $totalPages,
			'records' => $count,

		];

		$records = $model->getPaginatedSearch($limit, '', $sidx, $sord, '', $where, $page);

		return [
			(object) $response,
			$records,
		];
	}

	/**
	 * Download File
	 *
	 * @param string $filename
	 * @param array $data
	 * @param string $dtype
	 */
	public function Download($filename, $data, $dtype) {
		if ($dtype == 'csv') {
			\Excel::create($filename, function ($excel) use ($data) {
				$excel->sheet('One', function ($sheet) use ($data) {
					$sheet->fromArray($data, null, 'A1', false, false);;
				});
			})->export('csv');die;
		}

		// if xls
		if ($dtype == 'xls') {
			\Excel::create($filename, function ($excel) use ($data) {
				$excel->sheet('One', function ($sheet) use ($data) {
					$sheet->fromArray($data, null, 'A1', false, false);;
				});
			})->export('xls');die;
		}

		// if xls
		if ($dtype == 'pdf') {
			\Excel::create($filename, function ($excel) use ($data) {
				$excel->sheet('One', function ($sheet) use ($data) {
					$sheet->fromArray($data);
				});
			})->export('pdf');die;
		}
	}

}
