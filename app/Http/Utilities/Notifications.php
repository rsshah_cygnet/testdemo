<?php

namespace App\Http\Utilities;
use Illuminate\Support\Facades\Auth;
use App\Models\Notification\Notification;

class Notifications {
    /**
     * related model of this repositery
     * @var object
     */
    public $model;
    /*
     * construct
     */
    public function __construct(Notification $model)
    {
        $this->model = $model;       
    }
    
    /**
    * [create description]
    * @param  [type] $result [description]
    * @return [type]         [description]
    */
    public function create($message,$userId,$type='success',$createdBy = NULL)
    {
        $this->model->message = $message;
        $this->model->user_id = $userId;
        $this->model->type = $type;
        if($createdBy){
            $this->model->created_by = $createdBy;   
        } else {
            $this->model->created_by = Auth::user()->id;   
        }
        if ($this->model->save()) {
            return true;
        } else {
            return false;
        }    
    }
    
}
