<?php

namespace App\Http\Requests\Backend\SessionCancelPenalty;

use App\Http\Requests\Request;

class ViewRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
		// return access()->allow('view-curriculam-management');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			//
		];
	}
}
