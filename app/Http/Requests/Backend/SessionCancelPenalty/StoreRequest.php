<?php

namespace App\Http\Requests\Backend\SessionCancelPenalty;

use App\Http\Requests\Request;
use Illuminate\Http\Request as data;

class StoreRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(data $data) {
		$type = $data->type;

		if ($type == 1) {
			return [
				'from' => 'required|regex:/^[0-9]+$/',
				'to' => 'required|regex:/^[0-9]+$/|greater_than:from',
				'penalty_percentage' => 'required|regex:/^[0-9]*(?:\.[0-9]*)?$/|numeric|between:0,100',
				'within' => 'regex:/^[0-9]+$/',
				'type' => 'required',
			];
		} else if ($type == 2) {
			return [
				'from' => 'regex:/^[0-9]+$/',
				'to' => 'regex:/^[0-9]+$/|greater_than:from',
				'penalty_percentage' => 'required|regex:/^[0-9]*(?:\.[0-9]*)?$/|numeric|between:0,100',
				'within' => 'required|regex:/^[0-9]+$/',
				'type' => 'required',
			];
		} else {
			return [
				'type' => 'required',
			];
		}
	}

	public function messages() {
		return [
			'from.regex' => 'From field must be a digits.',
			'to.regex' => 'To field must be a digits.',
			'penalty_percentage.regex' => 'Penalty percentage field must be between 0 to 100.',
			'within.regex' => 'Within field must be a digits.',
			'penalty_percentage.integer' => 'Penalty percentage field must be between 0 to 100.',
			'penalty_percentage.between' => 'Penalty percentage field must be between 0 to 100.',
		];
	}
}
