<?php

namespace App\Http\Requests\Backend\EmailTemplate;

use App\Http\Requests\Request;

class UpdateEmailTemplateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-emailtemplate');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'subject' => 'required',
            'body' => 'required'           
        ];
    }
}
