<?php

namespace App\Http\Requests\Backend\PunctualityRatings;

use App\Http\Requests\Request;

class EditRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
		// return access()->allow('edit-curriculam');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			//
		];
	}
}
