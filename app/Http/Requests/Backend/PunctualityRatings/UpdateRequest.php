<?php

namespace App\Http\Requests\Backend\PunctualityRatings;

use App\Http\Requests\Request;

class UpdateRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
		// return access()->allow('edit-curriculam');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'from' => 'required|max:5|regex:/^[0-9]+$/',
			'to' => 'required|max:5|regex:/^[0-9]+$/|greater_than:from',
			'rating' => 'required|regex:/^[0-9]*(?:\.[0-9]*)?$/|numeric|between:1,10',
		];
	}

	public function messages() {
		return [
			'from.regex' => 'From field must be a digits.',
			'from.max' => 'From field may not be greater than 5 digits.',
			'to.regex' => 'To field must be a digits.',
			'to.max' => 'To field may not be greater than 5 digits.',
			'rating.regex' => 'Rating field must be between 0 and 10.',
			'rating.integer' => 'Rating field must be between 1 and 10.',
			'rating.between' => 'Rating field must be between 1 and 10.',

		];
	}
}
