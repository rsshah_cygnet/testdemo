<?php

namespace App\Http\Requests\Backend\MenuManagement;

use App\Http\Requests\Request;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-menu');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                 'name'       => 'required',
                 'domain_id'  => 'required',
                 'cmspage_id' => 'required',
                 'parent_menu_id' =>  'required'
        ];
    }
}
