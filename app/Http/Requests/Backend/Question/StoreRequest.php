<?php

namespace App\Http\Requests\Backend\Question;

use App\Http\Requests\Request;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question'  => 'required',
            'curriculum_id'  => 'required',
            'topic_id' => 'required',
            'option1_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024*5',
            'option2_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024*5',
            'option3_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024*5',
            'option4_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024*5',
            'correct_answer' => 'required',
            'question_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024*5',
            
            
        ];
    }

    
}
