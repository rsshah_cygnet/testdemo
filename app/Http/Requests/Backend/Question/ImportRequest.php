<?php

namespace App\Http\Requests\Backend\Question;

use App\Http\Requests\Request;

class ImportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'curriculum_id'  => 'required',
            'topic_id' => 'required',
            'imported_file' => 'required',
            
        ];
    }

    
}
