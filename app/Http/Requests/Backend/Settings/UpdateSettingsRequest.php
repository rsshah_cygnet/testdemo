<?php

namespace App\Http\Requests\Backend\Settings;

use App\Http\Requests\Request;

class UpdateSettingsRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return access()->allow('edit-settings');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		return [
			// 'reappearing_test' => 'regex:/^[0-9]+$/|max:3',
			// 'min_questions_in_test' => 'regex:/^[0-9]+$/|max:3',
			// 'passing_percentage' => 'regex:/^[0-9]+$/|max:3',
			// 'number_of_minutes_per_questions' => 'regex:/^[0-9]+$/|max:3',
			// 'number_of_questions_per_topic' => 'regex:/^[0-9]+$/|max:3',
			'higher_level_grade_fee' => 'regex:/^[0-9]+$/',
			'lower_level_grade_fee' => 'regex:/^[0-9]+$/',
			'tutor_availability_range' => 'regex:/^[0-9]+$/',
			'from_email' => 'email',
			// 'no_of_mark_per_question' => 'regex:/^[0-9]+$/|max:3',
			'admin_commission_for_lower_level' => 'regex:/^[0-9]*(?:\.[0-9]*)?$/|numeric|between:0,99.99',
			'admin_commission_for_higher_level' => 'regex:/^[0-9]*(?:\.[0-9]*)?$/|numeric|between:0,99.99',
			'free_session_per_fb_count' => 'regex:/^[0-9]+$/'
		];
	}

	public function messages() {
		return [
			// 'reappearing_test.regex' => 'No. of efforts available for reappearing test must be a digits.',
			// 'reappearing_test.max' => 'No. of efforts available for reappearing test may not be greater than 3 digits.',
			// 'min_questions_in_test.regex' => 'Minimum questions to be asked in a test must be a digits.',
			// 'min_questions_in_test.max' => 'Minimum questions to be asked in a test may not be greater than 3 digits.',
			// 'passing_percentage.regex' => 'Passing percentage must be a digits.',
			// 'passing_percentage.max' => 'Passing percentage may not be greater than 3 digits.',
			// 'number_of_minutes_per_questions.regex' => 'No. of minutes per question must be a digits.',
			// 'number_of_minutes_per_questions.max' => 'No. of minutes per question  may not be greater than 3 digits.',
			// 'number_of_questions_per_topic.regex' => 'No. of questions to be asked per topic must be a digits.',
			// 'number_of_questions_per_topic.max' => 'No. of questions to be asked per topic may not be greater than 3 digits.',
			'higher_level_grade_fee.regex' => 'Higher level grade fees must be a digits.',
			'lower_level_grade_fee.regex' => 'Lower level grade fees must be a digits.',
			'tutor_availability_range.regex' => 'Tutor availability must be a digits.',
			'from_email.required' => 'Email field is required',
			'from_email.email' => 'Enter valid from e-mail address',
			// 'no_of_mark_per_question.regex' => 'No. of mark per questions must be a digits.',
			// 'no_of_mark_per_question.max' => 'No. of mark per questions may not be greater than 3 digits.',
			'admin_commission_for_lower_level.regex' => 'The admin commission for lower level grade must be between 0 and 99.99',
			'admin_commission_for_lower_level.numeric' => 'The admin commission for lower level grade must be between 0 and 99.99',
			'admin_commission_for_lower_level.between' => 'The admin commission for lower level grade must be between 0 and 99.99',
			'admin_commission_for_higher_level.regex' => 'The admin commission for higher level grade must be between 0 and 99.99',
			'admin_commission_for_higher_level.numeric' => 'The admin commission for higher level grade must be between 0 and 99.99',
			'admin_commission_for_higher_level.between' => 'The admin commission for higher level grade must be between 0 and 99.99',

		];
	}
}
