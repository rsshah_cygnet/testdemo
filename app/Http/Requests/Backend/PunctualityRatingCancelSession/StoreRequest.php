<?php

namespace App\Http\Requests\Backend\PunctualityRatingCancelSession;

use App\Http\Requests\Request;
use Illuminate\Http\Request as data;

class StoreRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(data $data) {
		$type = $data->type;
		if ($type == 1) {
			return [
				'from' => 'required|max:5|regex:/^[0-9]+$/',
				'to' => 'required|max:5|regex:/^[0-9]+$/|greater_than:from',
				'rating' => 'required|regex:/^[0-9]*(?:\.[0-9]*)?$/|numeric|between:1,10',
				'within' => 'regex:/^[0-9]+$/|max:5',
				'type' => 'required',
			];
		} else if ($type == 2) {
			return [
				'from' => 'regex:/^[0-9]+$/|max:5',
				'to' => 'regex:/^[0-9]+$/|greater_than:from|max:5',
				'rating' => 'required|regex:/^[0-9]*(?:\.[0-9]*)?$/|numeric|between:1,10',
				'within' => 'required|regex:/^[0-9]+$/|max:5',
				'type' => 'required',
			];
		} else {
			return [
				'type' => 'required',
			];
		}
	}

	public function messages() {
		return [
			'from.regex' => 'From field must be a digits.',
			'from.max' => 'From field may not be greater than 5 digits.',
			'to.regex' => 'To field must be a digits.',
			'to.max' => 'To field may not be greater than 5 digits.',
			'rating.regex' => 'Rating field must be between 1 and 10.',
			'within.regex' => 'Within field must be a digits.',
			'within.max' => 'within field may not be greater than 5 digits.',
			'rating.integer' => 'Rating field must be between 1 and 10.',
			'rating.between' => 'Rating field must be between 1 and 10.',
		];
	}
}
