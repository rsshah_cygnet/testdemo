<?php

namespace App\Http\Requests\Backend\Blog;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{

    // date_default_timezone_set('Asia/Kolkata');
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-blog');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'categories' => 'required',
            'content' => 'required',
            'domain_id' => 'required',
            'featured_image' =>'image|resolution:'.config('constants.BLOG_IMAGE_WIDTH').'x'.config('constants.BLOG_IMAGE_HEIGHT').'|mimes:jpg,jpeg,png',
            'publish_datetime' => 'required|date_format:d/m/Y H:i A|after:'.date('Y-m-d H:i:s'),
            'tags' => 'required',
            'record_status' => 'required'

        ];
    }

    public function messages()
    {
        return [
            'record_status.required' => 'Status is required',
        ];
    }

}
