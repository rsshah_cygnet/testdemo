<?php

namespace App\Http\Requests\Backend\Notification;

use App\Http\Requests\Request;

/**
 * Class MarkNotificationRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class MarkNotificationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //Get the 'mark' id
        switch ((int) request()->segment(6)) {
            case 0:
                return access()->allow('deactivate-notification');
            break;

            case 1:
                return access()->allow('reactivate-notification');
            break;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
