<?php

namespace App\Http\Requests\Backend\Cms;

use App\Http\Requests\Request;
use App\Models\Cmspage\Cmspage;

class UpdateCmsRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return access()->allow('edit-cms-pages');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required',
            'description' => 'required',
        ];
    }

}
