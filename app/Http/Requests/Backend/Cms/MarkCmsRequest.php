<?php

namespace App\Http\Requests\Backend\Cms;

use App\Http\Requests\Request;

/**
 * Class MarkCmsRequest
 * @package App\Http\Requests\Backend\Jobtypes
 */
class MarkCmsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //Get the 'mark' id
        switch ((int) request()->segment(6)) {
            case 'Inactive':
                return access()->allow('deactivate-cmspage');
            break;

            case 'Active':
                return access()->allow('reactivate-cmspage');
            break;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
