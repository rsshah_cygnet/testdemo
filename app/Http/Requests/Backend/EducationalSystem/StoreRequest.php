<?php

namespace App\Http\Requests\Backend\EducationalSystem;

use App\Http\Requests\Request;

class StoreRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'curriculum_id' => 'required',
			'name' => 'required|max:255',
			// 'name' => 'required|max:255|unique:eductional_systems,name,' . $this->education_system_id . ',name,deleted_at,NULL',
		];
	}

	public function messages() {
		return [
			'curriculum_id.required' => 'Curriculum Selection is required',
			'name.required' => 'Name field is required',
		];
	}

}
