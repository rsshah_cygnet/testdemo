<?php

namespace App\Http\Requests\Backend\Levels;

use App\Http\Requests\Request;
use Illuminate\Http\Request as data;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(data $data)
    {
        if(!empty($data->levels_id)){
            return [];
        }else{
            return [
            'levels_name'  => 'required',
            'curriculum_id'  => 'required',
            // 'subject_id' => 'required'
            ];
        }
    }

    
}
