<?php

namespace App\Http\Requests\Backend\Levels;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{

    // date_default_timezone_set('Asia/Kolkata');
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-level');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
     

        ];
    }

    

}
