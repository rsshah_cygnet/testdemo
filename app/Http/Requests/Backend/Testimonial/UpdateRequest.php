<?php

namespace App\Http\Requests\Backend\Testimonial;

use App\Http\Requests\Request;

class UpdateRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return access()->allow('edit-testimonial');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [

		];
	}

	public function messages() {
		return [

		];
	}
}
