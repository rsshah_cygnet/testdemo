<?php

namespace App\Http\Requests\Backend\Testimonial;

use App\Http\Requests\Request;

class PublishRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		// return access()->allow('status-testimonial');
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			//
		];
	}
}
