<?php

namespace App\Http\Requests\Backend\Dealership;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-dealership');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dealership_name' => 'required',
            'dealership_email' => 'required|email|unique:dealerships,dealership_email,' . \Illuminate\Http\Request::segment(3),
            'dealer_principal_email' => 'required|email|unique:dealerships,dealer_principal_email,' . \Illuminate\Http\Request::segment(3),
            'dealer_principal_contact_no' => 'required|regex:/^\+27[0-9]{9}$/'
        ];
    }

}
