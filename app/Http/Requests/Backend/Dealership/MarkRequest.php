<?php

namespace App\Http\Requests\Backend\Dealership;

use App\Http\Requests\Request;

/**
 * Class MarkRequest
 * @package App\Http\Requests\Backend\Dealership
 */
class MarkRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ((int) request()->segment(6)) {
            case 'InActive':
                return access()->allow('deactivate-dealership');
            break;

            case 'Active':
                return access()->allow('reactivate-dealership');
            break;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
