<?php

namespace App\Http\Requests\Backend\Dealership;

use App\Http\Requests\Request;

class UpdateDealershipRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-dealership');        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dealership_name' => 'required',
            'address' => 'required',
             // 'provinence_region_id' => 'required'
             // 'dealership_email' => 'required|email|unique:dealerships,dealership_email,'.\Illuminate\Http\Request::segment(3),
             'dealer_principal_contact_no' => 'required'
        ];
    }

}
