<?php

namespace App\Http\Requests\Backend\Curriculum;

use App\Http\Requests\Request;
use Illuminate\Http\Request as data;

class UpdateRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return access()->allow('edit-curriculam');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(data $data) {
		// dd($data->all());
		if(!empty($data->id)){
			return [
				'curriculum_name'	=>	'required|max:255',
				'higher_level_fee' => 'regex:/^[0-9]+$/|max:9',
				'lower_level_fee' => 'regex:/^[0-9]+$/|max:9',
			];
		}else{
			return [
				'curriculum_name' => 'required|max:255|unique:curriculum,curriculum_name,' . $this->id . ',id,deleted_at,NULL',
				'higher_level_fee' => 'regex:/^[0-9]+$/|max:9',
				'lower_level_fee' => 'regex:/^[0-9]+$/|max:9',
			];
		}
	}

	public function messages() {
		return [
			'higher_level_fee.regex' => 'Higher level fee must be an digits.',
			'higher_level_fee.max' => 'Higher level fee may not be greater than 9 digits.',
			'lower_level_fee.regex' => 'Lower level fee must be an digits.',
			'lower_level_fee.max' => 'Lower level fee may not be greater than 9 digits.',

		];
	}
}
