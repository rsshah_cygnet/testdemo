<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class UpdateUserRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return access()->allow('edit-users');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		$new = array();

		$old = [
			'first_name' => 'required|alpha',
			'last_name' => 'alpha',
			'email' => 'required|email',
			'contact_no' => 'required',
			'contact_no' => 'required|regex:/^[0-9]+$/',
			'assignees_roles' => 'required',
		];

		$rule = array_merge($old + $new);
		return $rule;
	}

	/**
	 * Message displayed when error occured
	 */
	public function messages() {
		return [
			'contact_no.regex' => 'Conact number must be a digits.',
		];
	}
}
