<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;

/**
 * Class StoreUserRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class StoreUserRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return access()->allow('create-users');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		$rule = [
			'first_name' => 'required|alpha',
			'last_name' => 'alpha',
			'email' => 'required|email|unique:users|max:100',
			'contact_no' => 'required|regex:/^[0-9]+$/',
			'password' => 'required|alpha_num|min:6|confirmed|max:60',
			'password_confirmation' => 'required|alpha_num|min:6|max:60',
			'assignees_roles' => 'required',
		];

		return $rule;
	}

	/**
	 * Message displayed when error occured
	 */
	public function messages() {
		return [
			'contact_no.regex' => 'Conact number must be a digits.',
		];
	}
}
