<?php

namespace App\Http\Requests\Backend\Grade;

use App\Http\Requests\Request;
use Illuminate\Http\Request as data;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(data $data)
    {
        if(!empty($data->grade_id)){
            return [];
        }
        else{
            return [
                'grade_name'  => 'required',
                'grade_type'  => 'required',
                'curriculum_id'  => 'required',
                //'eductional_systems_id' => 'required'
                
            ];
        }
    }

    
}
