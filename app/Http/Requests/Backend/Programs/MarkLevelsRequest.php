<?php

namespace App\Http\Requests\Backend\Programs;

use App\Http\Requests\Request;

/**
 * Class MarkUserRequest
 */
class MarkLevelsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('status-program');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
