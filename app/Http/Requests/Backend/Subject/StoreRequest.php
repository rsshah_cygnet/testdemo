<?php

namespace App\Http\Requests\Backend\Subject;

use App\Http\Requests\Request;
use Illuminate\Http\Request as data;

class StoreRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(data $data) {
		if(!empty($data->subject_id)){
			return[];
		}
		else{
			return [
				'subject_name' => 'required',
				'curriculum_id' => 'required',
				//'eductional_systems_id' => 'required'
				'reappearing_test' => 'regex:/^[0-9]+$/|max:3',
				'min_questions_in_test' => 'regex:/^[0-9]+$/|max:3',
				'passing_percentage' => 'regex:/^[0-9]*(?:\.[0-9]*)?$/|between:0,99.99',
				'number_of_minutes_per_questions' => 'regex:/^[0-9]+$/|max:3',
				'number_of_questions_per_topic' => 'regex:/^[0-9]+$/|max:5',
				'grade_id' => 'required',
			];
		}
	}

	public function messages($value = '') {
		return [
			'reappearing_test.regex' => 'No. of efforts available for reappearing test must be a digits.',
			'reappearing_test.max' => 'No. of efforts available for reappearing test may not be greater than 3 digits.',
			'min_questions_in_test.regex' => 'Minimum questions to be asked in a test must be a digits.',
			'min_questions_in_test.max' => 'Minimum questions to be asked in a test may not be greater than 3 digits.',
			'passing_percentage.regex' => 'Passing percentage must be a digits.',
			'passing_percentage.max' => 'Passing percentage may not be greater than 3 digits.',
			'number_of_minutes_per_questions.regex' => 'No. of minutes per question must be a digits.',
			'number_of_minutes_per_questions.max' => 'No. of minutes per question  may not be greater than 3 digits.',
			'number_of_questions_per_topic.regex' => 'No. of questions to be asked per topic must be a digits.',
			'number_of_questions_per_topic.max' => 'No. of questions to be asked per topic may not be greater than 3 digits.',
			'grade_id.required' => 'Please add or select grade for this curriculum'
		];
	}
}
