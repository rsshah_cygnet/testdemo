<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Request
 * @package App\Http\Requests
 */
abstract class Request extends FormRequest {
	/**
	 * Get the response for a forbidden operation.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function forbiddenResponse() {
		// return response(view('errors.403'), 403);
		return redirect()
			->route('admin.dashboard')
			->withFlashDanger(trans('auth.general_error'));
	}
}
