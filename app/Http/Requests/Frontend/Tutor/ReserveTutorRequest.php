<?php

namespace App\Http\Requests\Frontend\Tutor;

use App\Http\Requests\Request;

/**
 * Class ReserveTutorRequest
 * @package App\Http\Requests\Frontend\Tutor
 */
class ReserveTutorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            /*'from_time' => 'required',
            'objective' => 'required',
            'curriculum_id' => 'required', */ 
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
