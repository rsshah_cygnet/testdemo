<?php

namespace App\Http\Requests\Frontend\Tutor;

use App\Http\Requests\Request;
use validator;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class TutorAppearTestRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade_id' => 'required',
            'subject_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'grade_id.required' => 'Grade is required.',
            'subject_id.required' => 'Subject is required.'
        ];
    }
}
