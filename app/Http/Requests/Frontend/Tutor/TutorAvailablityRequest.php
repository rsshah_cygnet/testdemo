<?php

namespace App\Http\Requests\Frontend\Tutor;

use App\Http\Requests\Request;
use validator;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class TutorAvailablityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'lang_id' => 'array|array|min:1',
            'from_time' => 'required',
            'to_time' => 'required|different:from_time',
            //'phone_no' => 'required|regex:/^\+27[0-9]{9}$/',
            
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
