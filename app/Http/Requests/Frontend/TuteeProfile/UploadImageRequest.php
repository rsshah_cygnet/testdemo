<?php

namespace App\Http\Requests\Frontend\TuteeProfile;

use App\Http\Requests\Request;

/**
 * Class UploadImageRequest
 * @package App\Http\Requests\Frontend\TuteeProfile
 */
class UploadImageRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024*5',
			'tutee_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024*5',
		];
	}

	/**
	 * Message displayed when error occured
	 */
	public function messages() {
		return [
			'file.mimes' => 'The file type should be "jpeg/png/jpg/gif/svg." ',
			'tutee_image.mimes' => 'The file type should be "jpeg/png/jpg/gif/svg." ',
		];
	}
}