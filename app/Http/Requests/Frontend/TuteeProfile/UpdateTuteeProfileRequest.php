<?php

namespace App\Http\Requests\Frontend\TuteeProfile;

use App\Http\Requests\Request;
use Illuminate\Http\Request as data;

/**
 * Class UpdateTuteeProfileRequest
 * @package App\Http\Requests\Frontend\TuteeProfile
 */
class UpdateTuteeProfileRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(data $data) {
		if ($data->qualification_id == 1 || $data->qualification_id == 2 || $data->qualification_id == 3) {
			return [
				'first_name' => 'required|alpha',
				'last_name' => 'required|alpha',
				'nationality' => 'required',
				'dob' => 'required',
				'timezone_id' => 'required',
				'country_id' => 'required',
				'language_id' => 'required',
				// 'state_id' => 'required',
				// 'city_id' => 'required',
				'email' => 'required|email|max:255|unique:front_user,email,'.$this->id.',id,deleted_at,NULL',
				'paypal_email' => 'required|email',
				// 'Occupation' => 'required',
				'qualification_id' => 'required',
				// 'educational_details' => 'required',
				'curriculum_id' => 'required',
				'contact_no' => 'regex:/^[0-9]+$/',
			];
		} else {
			return [
				'first_name' => 'required|alpha',
				'last_name' => 'required|alpha',
				'nationality' => 'required',
				'dob' => 'required',
				'timezone_id' => 'required',
				'country_id' => 'required',
				'language_id' => 'required',
				// 'state_id' => 'required',
				// 'city_id' => 'required',
				'email' => 'required|email|max:255|unique:front_user,email,'.$this->id.',id,deleted_at,NULL',
				'paypal_email' => 'required|email',
				// 'Occupation' => 'required',
				'qualification_id' => 'required',
				// 'educational_details' => 'required',
				'subject_name' => 'required|max:255',
				'contact_no' => 'regex:/^[0-9]+$/',
			];
		}
	}

	/**
	 * Message displayed when error occured
	 */
	public function messages() {
		return [
			'first_name.required' => 'First Name is required.',
			'contact_no.regex' => 'Contact no must be a digits.',
			'contact_no.required' => 'Contact no must be a digits.',
			'qualification_id.required'	=> 'The Qualification Status field is required',
			'curriculum_id.required' => 'The curriculum field is required'
		];
	}
}