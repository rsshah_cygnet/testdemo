<?php

namespace App\Http\Requests\Frontend\Tutee;

use App\Http\Requests\Request;


/**
 * Class ReportTutorRequest
 * @package App\Http\Requests\Frontend\Tutee
 */
class ReportTutorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'report' => 'required',
        ];
    }

    public function messages()
    {
        return [
             // 'report.required' =>"Feedback Field Is Required",
        ];
    }
}
