<?php

namespace App\Http\Requests\Frontend\Tutee;

use App\Http\Requests\Request;
use validator;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class TuteePreferenceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'lang_id' => 'array|array|min:1',
            'from_punctual_rating' => 'integer|min:0|max:10|required_with:to_punctual_rating|less_than:to_punctual_rating',
            'to_punctual_rating' => 'integer|min:0|max:10|greater_than:from_punctual_rating|required_with:from_punctual_rating',
            'from_rating' => 'integer|min:0|max:5|required_with:to_rating|less_than:to_rating',
            'to_rating' => 'integer|min:0|max:5|greater_than:from_rating|required_with:from_rating',
            //'phone_no' => 'required|regex:/^\+27[0-9]{9}$/',
            
        ];
    }

    public function messages()
    {
        return [
        'lang_id.required' => 'The Language field is required',
        'lang_id.min' => 'The Language field is required',
        ];
    }
}
