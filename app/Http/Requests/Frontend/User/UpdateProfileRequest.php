<?php

namespace App\Http\Requests\Frontend\User;

use App\Http\Requests\Request;

/**
 * Class UpdateProfileRequest
 * @package App\Http\Requests\Frontend\User
 */
class UpdateProfileRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'first_name' => 'required|alpha',
			'last_name' => 'alpha',
			'email' => 'sometimes|required|email',
			'contact_no' => 'regex:/^[0-9]+$/',
		];
	}

	/**
	 * Message displayed when error occured
	 */
	public function messages() {
		return [
			'contact_no.regex' => 'Conact number must be a digits.',
		];
	}
}