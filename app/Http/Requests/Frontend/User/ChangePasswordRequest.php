<?php

namespace App\Http\Requests\Frontend\User;

use App\Http\Requests\Request;

/**
 * Class ChangePasswordRequest
 * @package App\Http\Requests\Frontend\Access
 */
class ChangePasswordRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		return [
			'old_password' => 'required',
			'password' => 'required|min:6|confirmed',
			'password_confirmation'	=>	'required',
		];
	}

	/**
	 * Validation message
	 * 
	 * @return array
	 */
	public function messages()
	{
		return [
			'password.required' => 'The new password field is required.',
			'password_confirmation.required' => 'The confirm password field is required.'
		];
	}
}