<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255|alpha',
            'last_name' => 'required|max:255|alpha',
            //'username' => 'required|max:255',
            'password' => 'required|min:6',
            'cpassword' => 'required|min:6|same:password',
            'email' => 'required|email|max:255',
            'nationality' => 'required',
            'dob' => 'required',
            'country_id' => 'required',
            'timezone_id' => 'required',
            'lang_id' => 'required',
            'contact_no' => 'required|min:10|regex:/^([0-9\s\-\+\(\)]*)$/',
            //'phone_no' => 'required|regex:/^\+27[0-9]{9}$/',
            
        ];
    }

    public function messages()
    {
        return [
            'cpassword.same' => "Password and Confirm Password do not match. ",
            'cpassword.min' => "Confirm Password must be atleast 6 character. ",
            'cpassword.required' => "confirmed password field is required. ",
            'lang_id.required' => 'Please select at least one Language. ',
            'first_name.alpha' => 'Only alphabetical values are allowed. ',
            'last_name.alpha' => 'Only alphabetical values are allowed.',
            'contact_no.regex' => ' Only digits values are allowed.',
            'contact_no.min' => ' You have to enter minimum 10 digits.',
            'contact_no.required' => 'The contact no. field is required .',
            'country_id.required' => 'The country field is required.',
            'timezone_id.required' => 'The timezone field is required.'
        ];
    }
}
