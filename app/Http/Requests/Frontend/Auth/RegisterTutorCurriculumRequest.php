<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class RegisterTutorCurriculumRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade_id_payment'=>'required',
            'subject_id_payment'=>'required',
            
        ];
    }

    public function messages()
    {
        return [
        'grade_id_payment.required' => 'The Grade field is required',
        'subject_id_payment.required' => 'The Subject field is required',

        ];
    }
}
