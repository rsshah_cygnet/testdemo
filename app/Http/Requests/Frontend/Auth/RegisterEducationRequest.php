<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class RegisterEducationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'qualification_id' => 'required',
            //'curriculum_id' => 'required',
            //'grade_id'=>'required',
           // 'subject_id'=>'required',
        //'paypal_registered_email_payment' => 'required|email',
            
        ];
    }

    public function messages()
    {
        return [
        'qualification_id.required' => 'The Qualification field is required',
        'curriculum_id.required' => 'The Curriculum field is required',
        //'paypal_registered_email_payment.required' => 'The Paypal Email field is required',
       // 'grade_id.required' => 'The Grade field is required',
       // 'subject_id.required' => 'The Subject field is required',

        ];
    }
}
