var elixir = require('laravel-elixir');

elixir(function(mix) {
 mix
     .phpUnit()

    /**
     * Copy needed files from /node directories
     * to /public directory.
     */
     .copy(
       'node_modules/font-awesome/fonts',
       'public/build/fonts/font-awesome'
     )
     .copy(
       'node_modules/bootstrap-sass/assets/fonts/bootstrap',
       'public/build/fonts/bootstrap'
     )
     .copy(
       'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
       'public/js/vendor/bootstrap'
     )


     /**
      * Process frontend SCSS stylesheets
      */
     .sass([
        'frontend/app.scss',
        'plugin/sweetalert/sweetalert.scss'
     ], 'resources/assets/css/frontend/app.css')

     /**
      * Combine pre-processed frontend CSS files
      */
     .styles([
        'frontend/app.css',
        'frontend/bootstrap.min.css',
        'frontend/select2.css',
        'frontend/jquery.mCustomScrollbar.css',
        'frontend/font-awesome.min.css',
        'frontend/owl.carousel.css',
        'frontend/owl.theme.default.css',
        'frontend/owl.theme.css',
        'frontend/style.css',
        'frontend/responsive.css'
     ], 'public/css/frontend.css')

     /**
      * Combine frontend scripts
      */
     .scripts([
        'frontend/jquery.min.js',
        'plugin/sweetalert/sweetalert.min.js',
        'plugins.js',
        'frontend/app.js',
        'frontend/bootstrap.js',
        'frontend/select2.js',
        'frontend/menu.js',
        'frontend/owl.carousel.js',
        'frontend/owl.carousel2.thumbs.js',
        'frontend/easyResponsiveTabs.js',
        'frontend/jquery.mCustomScrollbar.concat.min.js',
        'frontend/icheck.min.js',
        'frontend/common.js'
     ], 'public/js/frontend.js')

     /**
      * Process backend SCSS stylesheets
      */
     .sass([
         'backend/app.scss',
         'backend/plugin/toastr/toastr.scss',
         'plugin/sweetalert/sweetalert.scss'
     ], 'resources/assets/css/backend/app.css')

     /**
      * Combine pre-processed backend CSS files
      */
     .styles([
         'backend/app.css',
         'backend/bootstrap.min.css',
         'backend/AdminLTE.min.css',
         'backend/skins/skin-blue.min.css',
         'backend/jquery-ui.css',
         'backend/ui.jqgrid.css',
         'backend/ui.multiselect.css',
         'backend/select2.css',
         'backend/bootstrap-datetimepicker.min.css',
		 'backend/wizard.css',		 
         'backend/custom.css',		 
         'plugin/dropzone/dropzone.min.css'
     ], 'public/css/backend.css')

     /**
      * Combine backend scripts
      */
     .scripts([
        'jquery-2.2.3.min.js',
        'jqgrid/js/jquery-ui-1.11.1/jquery-ui.min.js',
        'jqgrid/js/addons/ui.multiselect.js',
        'jqgrid/js/i18n/grid.locale-en.js',
        'jqgrid/js/jquery.jqGrid.min.js',
        'jqgrid/js/customJqgrid.js',
        'bootstrap.min.js',
        'select2.js',
        'moment.js',
        'bootstrap-datetimepicker.min.js',
        'plugin/dropzone/dropzone.js',
        'plugin/sweetalert/sweetalert.min.js',
        'tinymce.min.js',
        'plugins.js',
        'backend/app.js',
        'backend/plugin/toastr/toastr.min.js',
        'frontend/icheck.min.js',
        'backend/custom.js',
		'backend/jquery-formwizard.js',
		'backend/formwizard.js'
     ], 'public/js/backend.js')

    /**
      * Apply version control
      */
     .version(["public/css/frontend.css", "public/js/frontend.js", "public/css/backend.css", "public/js/backend.js"]);
});